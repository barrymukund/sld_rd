<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#151" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1409" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004716064453" />
                  <Width Value="9.78389453125" />
                  <Height Value="9.766442871094" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.302443359375" Y="-4.013563720703" />
                  <Point X="-24.4366953125" Y="-3.512524414063" />
                  <Point X="-24.44227734375" Y="-3.497140136719" />
                  <Point X="-24.457634765625" Y="-3.467376953125" />
                  <Point X="-24.5125078125" Y="-3.388315917969" />
                  <Point X="-24.62136328125" Y="-3.2314765625" />
                  <Point X="-24.643248046875" Y="-3.209020751953" />
                  <Point X="-24.66950390625" Y="-3.189776855469" />
                  <Point X="-24.69750390625" Y="-3.175669433594" />
                  <Point X="-24.782416015625" Y="-3.149315673828" />
                  <Point X="-24.95086328125" Y="-3.097036132812" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097035888672" />
                  <Point X="-25.121736328125" Y="-3.123389404297" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.31818359375" Y="-3.189776855469" />
                  <Point X="-25.344439453125" Y="-3.209020751953" />
                  <Point X="-25.36632421875" Y="-3.231477783203" />
                  <Point X="-25.4211953125" Y="-3.310538574219" />
                  <Point X="-25.53005078125" Y="-3.467378173828" />
                  <Point X="-25.5381875" Y="-3.481572021484" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.817318359375" Y="-4.506474609375" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.17421484375" Y="-4.820939453125" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.231123046875" Y="-4.686192871094" />
                  <Point X="-26.2149609375" Y="-4.563438476562" />
                  <Point X="-26.21419921875" Y="-4.5479296875" />
                  <Point X="-26.2165078125" Y="-4.51622265625" />
                  <Point X="-26.236794921875" Y="-4.414240722656" />
                  <Point X="-26.277037109375" Y="-4.211931152344" />
                  <Point X="-26.287939453125" Y="-4.182965332031" />
                  <Point X="-26.30401171875" Y="-4.155127929687" />
                  <Point X="-26.32364453125" Y="-4.131204589844" />
                  <Point X="-26.4018203125" Y="-4.062645507813" />
                  <Point X="-26.556904296875" Y="-3.926639892578" />
                  <Point X="-26.5831875" Y="-3.910295898438" />
                  <Point X="-26.612884765625" Y="-3.897994628906" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.74678515625" Y="-3.884165527344" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708496094" />
                  <Point X="-27.014466796875" Y="-3.882028076172" />
                  <Point X="-27.042658203125" Y="-3.894801757812" />
                  <Point X="-27.12911328125" Y="-3.952570068359" />
                  <Point X="-27.300623046875" Y="-4.067169921875" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.3351015625" Y="-4.099461914062" />
                  <Point X="-27.4801484375" Y="-4.288488769531" />
                  <Point X="-27.515875" Y="-4.266367675781" />
                  <Point X="-27.801712890625" Y="-4.089384521484" />
                  <Point X="-27.933083984375" Y="-3.988233154297" />
                  <Point X="-28.10472265625" Y="-3.856077392578" />
                  <Point X="-27.673255859375" Y="-3.108756103516" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647655273438" />
                  <Point X="-27.406587890625" Y="-2.616129882812" />
                  <Point X="-27.40557421875" Y="-2.585197509766" />
                  <Point X="-27.414556640625" Y="-2.555581054688" />
                  <Point X="-27.428771484375" Y="-2.526752685547" />
                  <Point X="-27.446798828125" Y="-2.501592529297" />
                  <Point X="-27.464146484375" Y="-2.484243896484" />
                  <Point X="-27.4893046875" Y="-2.466215332031" />
                  <Point X="-27.518134765625" Y="-2.451997070313" />
                  <Point X="-27.547751953125" Y="-2.443011962891" />
                  <Point X="-27.5786875" Y="-2.444024169922" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.4964453125" Y="-2.956138183594" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-28.857037109375" Y="-3.090545410156" />
                  <Point X="-29.082857421875" Y="-2.793863037109" />
                  <Point X="-29.17704296875" Y="-2.6359296875" />
                  <Point X="-29.306142578125" Y="-2.419450195312" />
                  <Point X="-28.54248828125" Y="-1.833477416992" />
                  <Point X="-28.105955078125" Y="-1.498513305664" />
                  <Point X="-28.08306640625" Y="-1.475881958008" />
                  <Point X="-28.063474609375" Y="-1.444145874023" />
                  <Point X="-28.0541796875" Y="-1.419109130859" />
                  <Point X="-28.051275390625" Y="-1.40986706543" />
                  <Point X="-28.04615234375" Y="-1.390089355469" />
                  <Point X="-28.042037109375" Y="-1.358610717773" />
                  <Point X="-28.042734375" Y="-1.335738037109" />
                  <Point X="-28.0488828125" Y="-1.313695922852" />
                  <Point X="-28.060888671875" Y="-1.284712402344" />
                  <Point X="-28.07394140625" Y="-1.262393310547" />
                  <Point X="-28.09253125" Y="-1.244420776367" />
                  <Point X="-28.113908203125" Y="-1.228767333984" />
                  <Point X="-28.12184765625" Y="-1.223542724609" />
                  <Point X="-28.139455078125" Y="-1.213179931641" />
                  <Point X="-28.168716796875" Y="-1.201956176758" />
                  <Point X="-28.200603515625" Y="-1.195474731445" />
                  <Point X="-28.231927734375" Y="-1.194383789062" />
                  <Point X="-29.314140625" Y="-1.336859741211" />
                  <Point X="-29.7321015625" Y="-1.391885253906" />
                  <Point X="-29.745755859375" Y="-1.338427734375" />
                  <Point X="-29.834078125" Y="-0.992648620605" />
                  <Point X="-29.858994140625" Y="-0.818428466797" />
                  <Point X="-29.892421875" Y="-0.584698303223" />
                  <Point X="-29.03015234375" Y="-0.353653564453" />
                  <Point X="-28.532875" Y="-0.220408309937" />
                  <Point X="-28.511685546875" Y="-0.211888778687" />
                  <Point X="-28.486818359375" Y="-0.198213821411" />
                  <Point X="-28.478431640625" Y="-0.193016983032" />
                  <Point X="-28.459978515625" Y="-0.180210723877" />
                  <Point X="-28.436021484375" Y="-0.158685028076" />
                  <Point X="-28.41491015625" Y="-0.127851852417" />
                  <Point X="-28.404384765625" Y="-0.103123527527" />
                  <Point X="-28.40106640625" Y="-0.094076515198" />
                  <Point X="-28.394916015625" Y="-0.074259216309" />
                  <Point X="-28.38947265625" Y="-0.045515129089" />
                  <Point X="-28.39053125" Y="-0.011270002365" />
                  <Point X="-28.395876953125" Y="0.013539984703" />
                  <Point X="-28.398015625" Y="0.021688549042" />
                  <Point X="-28.404166015625" Y="0.041505996704" />
                  <Point X="-28.417484375" Y="0.07083265686" />
                  <Point X="-28.440755859375" Y="0.100434516907" />
                  <Point X="-28.461876953125" Y="0.118407867432" />
                  <Point X="-28.469275390625" Y="0.124101829529" />
                  <Point X="-28.4877265625" Y="0.136908233643" />
                  <Point X="-28.501923828125" Y="0.145046478271" />
                  <Point X="-28.532875" Y="0.157848327637" />
                  <Point X="-29.51936328125" Y="0.422177001953" />
                  <Point X="-29.891814453125" Y="0.521975402832" />
                  <Point X="-29.881408203125" Y="0.592306884766" />
                  <Point X="-29.82448828125" Y="0.976969055176" />
                  <Point X="-29.774326171875" Y="1.162084228516" />
                  <Point X="-29.70355078125" Y="1.423268066406" />
                  <Point X="-29.122125" Y="1.346721923828" />
                  <Point X="-28.7656640625" Y="1.29979284668" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228027344" />
                  <Point X="-28.703134765625" Y="1.305263793945" />
                  <Point X="-28.682548828125" Y="1.311754394531" />
                  <Point X="-28.641708984375" Y="1.324631225586" />
                  <Point X="-28.62277734375" Y="1.332961791992" />
                  <Point X="-28.604033203125" Y="1.343783935547" />
                  <Point X="-28.5873515625" Y="1.356015625" />
                  <Point X="-28.573712890625" Y="1.371567993164" />
                  <Point X="-28.561298828125" Y="1.389298095703" />
                  <Point X="-28.551349609375" Y="1.407433959961" />
                  <Point X="-28.54308984375" Y="1.427376464844" />
                  <Point X="-28.526703125" Y="1.466938232422" />
                  <Point X="-28.520916015625" Y="1.486788330078" />
                  <Point X="-28.51715625" Y="1.508103759766" />
                  <Point X="-28.515802734375" Y="1.528750244141" />
                  <Point X="-28.518951171875" Y="1.549200195312" />
                  <Point X="-28.5245546875" Y="1.570106811523" />
                  <Point X="-28.532048828125" Y="1.589377075195" />
                  <Point X="-28.542015625" Y="1.608523925781" />
                  <Point X="-28.5617890625" Y="1.646506835938" />
                  <Point X="-28.573283203125" Y="1.663707763672" />
                  <Point X="-28.5871953125" Y="1.680287231445" />
                  <Point X="-28.60213671875" Y="1.694590087891" />
                  <Point X="-29.16798828125" Y="2.128783935547" />
                  <Point X="-29.351859375" Y="2.269873535156" />
                  <Point X="-29.302326171875" Y="2.354735595703" />
                  <Point X="-29.081146484375" Y="2.733666992188" />
                  <Point X="-28.94828125" Y="2.904447998047" />
                  <Point X="-28.75050390625" Y="3.158662109375" />
                  <Point X="-28.425583984375" Y="2.971069335938" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.11812109375" Y="2.823287841797" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.999013671875" Y="2.826504638672" />
                  <Point X="-27.980462890625" Y="2.835653076172" />
                  <Point X="-27.962208984375" Y="2.847281982422" />
                  <Point X="-27.946078125" Y="2.860228271484" />
                  <Point X="-27.9257265625" Y="2.880579345703" />
                  <Point X="-27.885353515625" Y="2.920951660156" />
                  <Point X="-27.872404296875" Y="2.9370859375" />
                  <Point X="-27.860775390625" Y="2.955340576172" />
                  <Point X="-27.85162890625" Y="2.973889404297" />
                  <Point X="-27.8467109375" Y="2.993978027344" />
                  <Point X="-27.843884765625" Y="3.015437255859" />
                  <Point X="-27.84343359375" Y="3.036119384766" />
                  <Point X="-27.84594140625" Y="3.064791015625" />
                  <Point X="-27.85091796875" Y="3.121668945312" />
                  <Point X="-27.854955078125" Y="3.141963867188" />
                  <Point X="-27.86146484375" Y="3.162605712891" />
                  <Point X="-27.869794921875" Y="3.181532958984" />
                  <Point X="-28.120541015625" Y="3.615837890625" />
                  <Point X="-28.18333203125" Y="3.724596191406" />
                  <Point X="-28.0858359375" Y="3.799346191406" />
                  <Point X="-27.70062109375" Y="4.094686035156" />
                  <Point X="-27.491357421875" Y="4.210947265625" />
                  <Point X="-27.167037109375" Y="4.391133789062" />
                  <Point X="-27.110216796875" Y="4.317083496094" />
                  <Point X="-27.0431953125" Y="4.229740234375" />
                  <Point X="-27.028888671875" Y="4.214796875" />
                  <Point X="-27.01230859375" Y="4.200885253906" />
                  <Point X="-26.995111328125" Y="4.18939453125" />
                  <Point X="-26.96319921875" Y="4.172782714844" />
                  <Point X="-26.89989453125" Y="4.139827636719" />
                  <Point X="-26.8806171875" Y="4.132331542969" />
                  <Point X="-26.8597109375" Y="4.126729492187" />
                  <Point X="-26.839267578125" Y="4.123582519531" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.797314453125" Y="4.128692871094" />
                  <Point X="-26.777451171875" Y="4.134482421875" />
                  <Point X="-26.74421484375" Y="4.148250488281" />
                  <Point X="-26.67827734375" Y="4.175562011719" />
                  <Point X="-26.660146484375" Y="4.185509277344" />
                  <Point X="-26.642416015625" Y="4.197923828125" />
                  <Point X="-26.62686328125" Y="4.2115625" />
                  <Point X="-26.6146328125" Y="4.228244140625" />
                  <Point X="-26.603810546875" Y="4.246988769531" />
                  <Point X="-26.595478515625" Y="4.265921386719" />
                  <Point X="-26.58466015625" Y="4.300232421875" />
                  <Point X="-26.56319921875" Y="4.368297851562" />
                  <Point X="-26.5591640625" Y="4.388583984375" />
                  <Point X="-26.55727734375" Y="4.410145019531" />
                  <Point X="-26.557728515625" Y="4.430826660156" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-26.4483125" Y="4.669996582031" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.6959453125" Y="4.839499023438" />
                  <Point X="-25.2947109375" Y="4.886457519531" />
                  <Point X="-25.19866796875" Y="4.528021972656" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286315429688" />
                  <Point X="-24.725302734375" Y="4.765807617188" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.5913828125" Y="4.877339355469" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.946068359375" Y="4.781064941406" />
                  <Point X="-23.5189765625" Y="4.677951660156" />
                  <Point X="-23.383341796875" Y="4.628756347656" />
                  <Point X="-23.105349609375" Y="4.527926757812" />
                  <Point X="-22.973248046875" Y="4.466146972656" />
                  <Point X="-22.705427734375" Y="4.340895996094" />
                  <Point X="-22.5777734375" Y="4.266524902344" />
                  <Point X="-22.319017578125" Y="4.1157734375" />
                  <Point X="-22.19865625" Y="4.030178222656" />
                  <Point X="-22.05673828125" Y="3.929254150391" />
                  <Point X="-22.563646484375" Y="3.051263671875" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857919921875" Y="2.539936035156" />
                  <Point X="-22.866921875" Y="2.516057128906" />
                  <Point X="-22.8741171875" Y="2.489149414062" />
                  <Point X="-22.888392578125" Y="2.435770751953" />
                  <Point X="-22.891380859375" Y="2.417936523438" />
                  <Point X="-22.892271484375" Y="2.380950195312" />
                  <Point X="-22.88946484375" Y="2.357682861328" />
                  <Point X="-22.883900390625" Y="2.311525390625" />
                  <Point X="-22.87855859375" Y="2.289609375" />
                  <Point X="-22.87029296875" Y="2.26751953125" />
                  <Point X="-22.859927734375" Y="2.247467773438" />
                  <Point X="-22.845529296875" Y="2.226250244141" />
                  <Point X="-22.81696875" Y="2.184159179688" />
                  <Point X="-22.805533203125" Y="2.170328125" />
                  <Point X="-22.77840234375" Y="2.145593261719" />
                  <Point X="-22.757185546875" Y="2.131196044922" />
                  <Point X="-22.71509375" Y="2.102635742188" />
                  <Point X="-22.695044921875" Y="2.092271728516" />
                  <Point X="-22.67295703125" Y="2.084006103516" />
                  <Point X="-22.65103515625" Y="2.078663330078" />
                  <Point X="-22.627767578125" Y="2.075857666016" />
                  <Point X="-22.581611328125" Y="2.070291748047" />
                  <Point X="-22.563533203125" Y="2.069845703125" />
                  <Point X="-22.526794921875" Y="2.074170898438" />
                  <Point X="-22.49988671875" Y="2.081366455078" />
                  <Point X="-22.4465078125" Y="2.095640625" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.419248046875" Y="2.683001953125" />
                  <Point X="-21.032673828125" Y="2.906190673828" />
                  <Point X="-21.030212890625" Y="2.902770263672" />
                  <Point X="-20.87671875" Y="2.689450195312" />
                  <Point X="-20.809625" Y="2.578575927734" />
                  <Point X="-20.73780078125" Y="2.459883789063" />
                  <Point X="-21.389021484375" Y="1.960184204102" />
                  <Point X="-21.76921484375" Y="1.668451171875" />
                  <Point X="-21.7785703125" Y="1.660244506836" />
                  <Point X="-21.79602734375" Y="1.641627075195" />
                  <Point X="-21.815392578125" Y="1.616363647461" />
                  <Point X="-21.85380859375" Y="1.566245849609" />
                  <Point X="-21.86339453125" Y="1.550909179688" />
                  <Point X="-21.8783671875" Y="1.517090209961" />
                  <Point X="-21.88558203125" Y="1.491295898438" />
                  <Point X="-21.899892578125" Y="1.440125732422" />
                  <Point X="-21.90334765625" Y="1.41782421875" />
                  <Point X="-21.9041640625" Y="1.394253173828" />
                  <Point X="-21.902259765625" Y="1.371766723633" />
                  <Point X="-21.896337890625" Y="1.343067260742" />
                  <Point X="-21.88458984375" Y="1.286133911133" />
                  <Point X="-21.8793203125" Y="1.268978881836" />
                  <Point X="-21.86371875" Y="1.235742431641" />
                  <Point X="-21.84761328125" Y="1.211261474609" />
                  <Point X="-21.815662109375" Y="1.162696899414" />
                  <Point X="-21.80110546875" Y="1.14544921875" />
                  <Point X="-21.783861328125" Y="1.129359741211" />
                  <Point X="-21.76565234375" Y="1.116034057617" />
                  <Point X="-21.742310546875" Y="1.102895385742" />
                  <Point X="-21.696009765625" Y="1.076831542969" />
                  <Point X="-21.679478515625" Y="1.069501342773" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.612322265625" Y="1.055267822266" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.569255859375" Y="1.171072265625" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.219984375" Y="1.203590942383" />
                  <Point X="-20.15405859375" Y="0.932787963867" />
                  <Point X="-20.132916015625" Y="0.796993347168" />
                  <Point X="-20.109134765625" Y="0.644238891602" />
                  <Point X="-20.847708984375" Y="0.44633795166" />
                  <Point X="-21.283419921875" Y="0.329589874268" />
                  <Point X="-21.2952109375" Y="0.325585662842" />
                  <Point X="-21.318453125" Y="0.315067810059" />
                  <Point X="-21.34945703125" Y="0.297146820068" />
                  <Point X="-21.410962890625" Y="0.261595367432" />
                  <Point X="-21.4256875" Y="0.25109588623" />
                  <Point X="-21.452466796875" Y="0.225577957153" />
                  <Point X="-21.4710703125" Y="0.201873748779" />
                  <Point X="-21.50797265625" Y="0.154850158691" />
                  <Point X="-21.51969921875" Y="0.135567306519" />
                  <Point X="-21.52947265625" Y="0.11410369873" />
                  <Point X="-21.536318359375" Y="0.092604560852" />
                  <Point X="-21.54251953125" Y="0.06022600174" />
                  <Point X="-21.5548203125" Y="-0.004005921364" />
                  <Point X="-21.556515625" Y="-0.021875597" />
                  <Point X="-21.5548203125" Y="-0.058554225922" />
                  <Point X="-21.548619140625" Y="-0.090932785034" />
                  <Point X="-21.536318359375" Y="-0.155164550781" />
                  <Point X="-21.52947265625" Y="-0.17666217041" />
                  <Point X="-21.51969921875" Y="-0.198126846313" />
                  <Point X="-21.5079765625" Y="-0.217407730103" />
                  <Point X="-21.489375" Y="-0.24111177063" />
                  <Point X="-21.452470703125" Y="-0.288135375977" />
                  <Point X="-21.439998046875" Y="-0.301238830566" />
                  <Point X="-21.410962890625" Y="-0.324155517578" />
                  <Point X="-21.379958984375" Y="-0.342076507568" />
                  <Point X="-21.318453125" Y="-0.377627929688" />
                  <Point X="-21.307291015625" Y="-0.383138244629" />
                  <Point X="-21.283419921875" Y="-0.392149871826" />
                  <Point X="-20.419068359375" Y="-0.623752441406" />
                  <Point X="-20.10852734375" Y="-0.706961669922" />
                  <Point X="-20.14498046875" Y="-0.948747741699" />
                  <Point X="-20.17206640625" Y="-1.067443603516" />
                  <Point X="-20.19882421875" Y="-1.18469921875" />
                  <Point X="-21.07031640625" Y="-1.069965209961" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710144043" />
                  <Point X="-21.62533984375" Y="-1.005508728027" />
                  <Point X="-21.68619140625" Y="-1.018734985352" />
                  <Point X="-21.806904296875" Y="-1.04497253418" />
                  <Point X="-21.8360234375" Y="-1.056596313477" />
                  <Point X="-21.8638515625" Y="-1.073489013672" />
                  <Point X="-21.8876015625" Y="-1.093959472656" />
                  <Point X="-21.9243828125" Y="-1.138194580078" />
                  <Point X="-21.997345703125" Y="-1.225947387695" />
                  <Point X="-22.012068359375" Y="-1.250334838867" />
                  <Point X="-22.02341015625" Y="-1.277720092773" />
                  <Point X="-22.030240234375" Y="-1.305365722656" />
                  <Point X="-22.03551171875" Y="-1.36265234375" />
                  <Point X="-22.04596875" Y="-1.476296142578" />
                  <Point X="-22.04365234375" Y="-1.507562011719" />
                  <Point X="-22.035921875" Y="-1.539182617188" />
                  <Point X="-22.023548828125" Y="-1.567996826172" />
                  <Point X="-21.989873046875" Y="-1.620376953125" />
                  <Point X="-21.923068359375" Y="-1.724287353516" />
                  <Point X="-21.9130625" Y="-1.737241943359" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.08724609375" Y="-2.376400634766" />
                  <Point X="-20.786876953125" Y="-2.606882568359" />
                  <Point X="-20.8751953125" Y="-2.749794677734" />
                  <Point X="-20.931216796875" Y="-2.829392822266" />
                  <Point X="-20.971017578125" Y="-2.885945068359" />
                  <Point X="-21.749005859375" Y="-2.4367734375" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.2457734375" Y="-2.159825195312" />
                  <Point X="-22.3181953125" Y="-2.146745849609" />
                  <Point X="-22.46186328125" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135177001953" />
                  <Point X="-22.615330078125" Y="-2.166841308594" />
                  <Point X="-22.73468359375" Y="-2.229656005859" />
                  <Point X="-22.757615234375" Y="-2.246549072266" />
                  <Point X="-22.77857421875" Y="-2.267508300781" />
                  <Point X="-22.795466796875" Y="-2.290438964844" />
                  <Point X="-22.827130859375" Y="-2.350603759766" />
                  <Point X="-22.889947265625" Y="-2.46995703125" />
                  <Point X="-22.899771484375" Y="-2.499735107422" />
                  <Point X="-22.904728515625" Y="-2.531908691406" />
                  <Point X="-22.90432421875" Y="-2.563259521484" />
                  <Point X="-22.891244140625" Y="-2.635681152344" />
                  <Point X="-22.865296875" Y="-2.779349853516" />
                  <Point X="-22.86101171875" Y="-2.795140136719" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.332732421875" Y="-3.718855957031" />
                  <Point X="-22.13871484375" Y="-4.054904541016" />
                  <Point X="-22.21816015625" Y="-4.111650878906" />
                  <Point X="-22.2807890625" Y="-4.152189941406" />
                  <Point X="-22.298232421875" Y="-4.163480957031" />
                  <Point X="-22.89803125" Y="-3.381810058594" />
                  <Point X="-23.241451171875" Y="-2.934254882812" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.349501953125" Y="-2.854635742188" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.661017578125" Y="-2.748305175781" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-23.95572265625" Y="-2.845615722656" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968861083984" />
                  <Point X="-24.11275" Y="-2.996687744141" />
                  <Point X="-24.124375" Y="-3.025809082031" />
                  <Point X="-24.14241015625" Y="-3.108787109375" />
                  <Point X="-24.178189453125" Y="-3.273396972656" />
                  <Point X="-24.180275390625" Y="-3.289627441406" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-24.03418359375" Y="-4.432656738281" />
                  <Point X="-23.97793359375" Y="-4.859915527344" />
                  <Point X="-24.024318359375" Y="-4.870083007812" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.05842578125" Y="-4.75263671875" />
                  <Point X="-26.141244140625" Y="-4.731328125" />
                  <Point X="-26.136935546875" Y="-4.698591796875" />
                  <Point X="-26.1207734375" Y="-4.575837402344" />
                  <Point X="-26.120076171875" Y="-4.568098632812" />
                  <Point X="-26.11944921875" Y="-4.541030761719" />
                  <Point X="-26.1217578125" Y="-4.509323730469" />
                  <Point X="-26.123333984375" Y="-4.4976875" />
                  <Point X="-26.14362109375" Y="-4.395705566406" />
                  <Point X="-26.18386328125" Y="-4.193395996094" />
                  <Point X="-26.188126953125" Y="-4.178466308594" />
                  <Point X="-26.199029296875" Y="-4.149500488281" />
                  <Point X="-26.20566796875" Y="-4.135464355469" />
                  <Point X="-26.221740234375" Y="-4.107626953125" />
                  <Point X="-26.23057421875" Y="-4.094861572266" />
                  <Point X="-26.25020703125" Y="-4.070938232422" />
                  <Point X="-26.261005859375" Y="-4.059780273438" />
                  <Point X="-26.339181640625" Y="-3.991221191406" />
                  <Point X="-26.494265625" Y="-3.855215576172" />
                  <Point X="-26.50673828125" Y="-3.845965820312" />
                  <Point X="-26.533021484375" Y="-3.829621826172" />
                  <Point X="-26.54683203125" Y="-3.822527587891" />
                  <Point X="-26.576529296875" Y="-3.810226318359" />
                  <Point X="-26.5913125" Y="-3.805476318359" />
                  <Point X="-26.621455078125" Y="-3.798447998047" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.740572265625" Y="-3.789368896484" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166015625" />
                  <Point X="-27.008005859375" Y="-3.781945556641" />
                  <Point X="-27.0390546875" Y="-3.790265136719" />
                  <Point X="-27.053673828125" Y="-3.795496337891" />
                  <Point X="-27.081865234375" Y="-3.808270019531" />
                  <Point X="-27.0954375" Y="-3.8158125" />
                  <Point X="-27.181892578125" Y="-3.873580810547" />
                  <Point X="-27.35340234375" Y="-3.988180664062" />
                  <Point X="-27.35968359375" Y="-3.992759277344" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032770507812" />
                  <Point X="-27.410470703125" Y="-4.041629150391" />
                  <Point X="-27.503201171875" Y="-4.162478027344" />
                  <Point X="-27.747591796875" Y="-4.011158447266" />
                  <Point X="-27.875126953125" Y="-3.912960693359" />
                  <Point X="-27.98086328125" Y="-3.831546875" />
                  <Point X="-27.590984375" Y="-3.156256103516" />
                  <Point X="-27.341490234375" Y="-2.724119628906" />
                  <Point X="-27.3348515625" Y="-2.7100859375" />
                  <Point X="-27.32394921875" Y="-2.681121582031" />
                  <Point X="-27.319685546875" Y="-2.666190917969" />
                  <Point X="-27.3134140625" Y="-2.634665527344" />
                  <Point X="-27.311638671875" Y="-2.619241455078" />
                  <Point X="-27.310625" Y="-2.588309082031" />
                  <Point X="-27.3146640625" Y="-2.557625" />
                  <Point X="-27.323646484375" Y="-2.528008544922" />
                  <Point X="-27.3293515625" Y="-2.513567871094" />
                  <Point X="-27.34356640625" Y="-2.484739501953" />
                  <Point X="-27.351548828125" Y="-2.471421630859" />
                  <Point X="-27.369576171875" Y="-2.446261474609" />
                  <Point X="-27.37962109375" Y="-2.434419189453" />
                  <Point X="-27.39696875" Y="-2.417070556641" />
                  <Point X="-27.408810546875" Y="-2.407024169922" />
                  <Point X="-27.43396875" Y="-2.388995605469" />
                  <Point X="-27.44728515625" Y="-2.381013427734" />
                  <Point X="-27.476115234375" Y="-2.366795166016" />
                  <Point X="-27.4905546875" Y="-2.361088378906" />
                  <Point X="-27.520171875" Y="-2.352103271484" />
                  <Point X="-27.550859375" Y="-2.348062744141" />
                  <Point X="-27.581794921875" Y="-2.349074951172" />
                  <Point X="-27.597220703125" Y="-2.350849365234" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.5439453125" Y="-2.873865722656" />
                  <Point X="-28.793087890625" Y="-3.017708251953" />
                  <Point X="-29.004017578125" Y="-2.740588623047" />
                  <Point X="-29.095451171875" Y="-2.587270996094" />
                  <Point X="-29.181265625" Y="-2.443373535156" />
                  <Point X="-28.48465625" Y="-1.908845825195" />
                  <Point X="-28.048123046875" Y="-1.573881835938" />
                  <Point X="-28.03916015625" Y="-1.566067260742" />
                  <Point X="-28.016271484375" Y="-1.543435791016" />
                  <Point X="-28.002228515625" Y="-1.525785522461" />
                  <Point X="-27.98263671875" Y="-1.494049438477" />
                  <Point X="-27.9744140625" Y="-1.477209716797" />
                  <Point X="-27.965119140625" Y="-1.452172973633" />
                  <Point X="-27.959310546875" Y="-1.433688842773" />
                  <Point X="-27.9541875" Y="-1.413911132812" />
                  <Point X="-27.951953125" Y="-1.402404052734" />
                  <Point X="-27.947837890625" Y="-1.370925415039" />
                  <Point X="-27.94708203125" Y="-1.355716064453" />
                  <Point X="-27.947779296875" Y="-1.332843383789" />
                  <Point X="-27.951228515625" Y="-1.310213134766" />
                  <Point X="-27.957376953125" Y="-1.288171020508" />
                  <Point X="-27.961115234375" Y="-1.27733972168" />
                  <Point X="-27.97312109375" Y="-1.248356201172" />
                  <Point X="-27.9788828125" Y="-1.236753417969" />
                  <Point X="-27.991935546875" Y="-1.214434448242" />
                  <Point X="-28.00791015625" Y="-1.19409387207" />
                  <Point X="-28.0265" Y="-1.17612109375" />
                  <Point X="-28.03640625" Y="-1.167772949219" />
                  <Point X="-28.057783203125" Y="-1.152119628906" />
                  <Point X="-28.073662109375" Y="-1.141670166016" />
                  <Point X="-28.09126953125" Y="-1.131307373047" />
                  <Point X="-28.10543359375" Y="-1.124480957031" />
                  <Point X="-28.1346953125" Y="-1.113257202148" />
                  <Point X="-28.14979296875" Y="-1.108859741211" />
                  <Point X="-28.1816796875" Y="-1.102378417969" />
                  <Point X="-28.197296875" Y="-1.100532348633" />
                  <Point X="-28.22862109375" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-29.326541015625" Y="-1.242672485352" />
                  <Point X="-29.660919921875" Y="-1.286694335938" />
                  <Point X="-29.740763671875" Y="-0.97411138916" />
                  <Point X="-29.764951171875" Y="-0.804979003906" />
                  <Point X="-29.786451171875" Y="-0.654654541016" />
                  <Point X="-29.005564453125" Y="-0.445416503906" />
                  <Point X="-28.508287109375" Y="-0.312171203613" />
                  <Point X="-28.497435546875" Y="-0.308550750732" />
                  <Point X="-28.47624609375" Y="-0.300031097412" />
                  <Point X="-28.465908203125" Y="-0.295132049561" />
                  <Point X="-28.441041015625" Y="-0.281457122803" />
                  <Point X="-28.424267578125" Y="-0.271063781738" />
                  <Point X="-28.405814453125" Y="-0.258257537842" />
                  <Point X="-28.396484375" Y="-0.25087588501" />
                  <Point X="-28.37252734375" Y="-0.229350234985" />
                  <Point X="-28.357634765625" Y="-0.212355819702" />
                  <Point X="-28.3365234375" Y="-0.181522674561" />
                  <Point X="-28.327498046875" Y="-0.165057632446" />
                  <Point X="-28.31697265625" Y="-0.140329315186" />
                  <Point X="-28.3103359375" Y="-0.122235229492" />
                  <Point X="-28.304185546875" Y="-0.102418014526" />
                  <Point X="-28.30157421875" Y="-0.091935493469" />
                  <Point X="-28.296130859375" Y="-0.063191390991" />
                  <Point X="-28.294517578125" Y="-0.042579929352" />
                  <Point X="-28.295576171875" Y="-0.008334697723" />
                  <Point X="-28.297662109375" Y="0.008739985466" />
                  <Point X="-28.3030078125" Y="0.033550041199" />
                  <Point X="-28.30728515625" Y="0.049847133636" />
                  <Point X="-28.313435546875" Y="0.069664505005" />
                  <Point X="-28.31766796875" Y="0.080788032532" />
                  <Point X="-28.330986328125" Y="0.110114738464" />
                  <Point X="-28.34280078125" Y="0.129545837402" />
                  <Point X="-28.366072265625" Y="0.159147644043" />
                  <Point X="-28.379189453125" Y="0.172784225464" />
                  <Point X="-28.400310546875" Y="0.190757644653" />
                  <Point X="-28.415107421875" Y="0.202145721436" />
                  <Point X="-28.43355859375" Y="0.214952102661" />
                  <Point X="-28.440482421875" Y="0.219327407837" />
                  <Point X="-28.46561328125" Y="0.23283366394" />
                  <Point X="-28.496564453125" Y="0.245635437012" />
                  <Point X="-28.508287109375" Y="0.249611236572" />
                  <Point X="-29.494775390625" Y="0.513939941406" />
                  <Point X="-29.7854453125" Y="0.591825073242" />
                  <Point X="-29.73133203125" Y="0.957520690918" />
                  <Point X="-29.6826328125" Y="1.137237426758" />
                  <Point X="-29.6335859375" Y="1.318237182617" />
                  <Point X="-29.134525390625" Y="1.252534667969" />
                  <Point X="-28.778064453125" Y="1.20560559082" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.747056640625" Y="1.204364379883" />
                  <Point X="-28.736705078125" Y="1.20470324707" />
                  <Point X="-28.71514453125" Y="1.206589477539" />
                  <Point X="-28.704890625" Y="1.208053466797" />
                  <Point X="-28.6846015625" Y="1.212089233398" />
                  <Point X="-28.674568359375" Y="1.214660522461" />
                  <Point X="-28.653982421875" Y="1.221151245117" />
                  <Point X="-28.613142578125" Y="1.234028076172" />
                  <Point X="-28.603447265625" Y="1.237677246094" />
                  <Point X="-28.584515625" Y="1.24600793457" />
                  <Point X="-28.57527734375" Y="1.250689819336" />
                  <Point X="-28.556533203125" Y="1.261511962891" />
                  <Point X="-28.547857421875" Y="1.267172119141" />
                  <Point X="-28.53117578125" Y="1.279403808594" />
                  <Point X="-28.51592578125" Y="1.29337878418" />
                  <Point X="-28.502287109375" Y="1.308931152344" />
                  <Point X="-28.495892578125" Y="1.317080200195" />
                  <Point X="-28.483478515625" Y="1.334810302734" />
                  <Point X="-28.478009765625" Y="1.343605834961" />
                  <Point X="-28.468060546875" Y="1.361741699219" />
                  <Point X="-28.463580078125" Y="1.371081665039" />
                  <Point X="-28.4553203125" Y="1.391024169922" />
                  <Point X="-28.43893359375" Y="1.4305859375" />
                  <Point X="-28.4355" Y="1.440348876953" />
                  <Point X="-28.429712890625" Y="1.460198974609" />
                  <Point X="-28.427359375" Y="1.470286376953" />
                  <Point X="-28.423599609375" Y="1.49160168457" />
                  <Point X="-28.422359375" Y="1.501889160156" />
                  <Point X="-28.421005859375" Y="1.522535522461" />
                  <Point X="-28.421908203125" Y="1.543205932617" />
                  <Point X="-28.425056640625" Y="1.563656005859" />
                  <Point X="-28.427189453125" Y="1.573794555664" />
                  <Point X="-28.43279296875" Y="1.594701171875" />
                  <Point X="-28.436013671875" Y="1.604539916992" />
                  <Point X="-28.4435078125" Y="1.623810058594" />
                  <Point X="-28.44778125" Y="1.633241699219" />
                  <Point X="-28.457748046875" Y="1.652388549805" />
                  <Point X="-28.477521484375" Y="1.690371459961" />
                  <Point X="-28.48280078125" Y="1.699288696289" />
                  <Point X="-28.494294921875" Y="1.716489624023" />
                  <Point X="-28.500509765625" Y="1.72477331543" />
                  <Point X="-28.514421875" Y="1.741352783203" />
                  <Point X="-28.521501953125" Y="1.748912841797" />
                  <Point X="-28.536443359375" Y="1.763215698242" />
                  <Point X="-28.5443046875" Y="1.769958618164" />
                  <Point X="-29.11015625" Y="2.20415234375" />
                  <Point X="-29.22761328125" Y="2.294280761719" />
                  <Point X="-29.220279296875" Y="2.306845947266" />
                  <Point X="-29.00228125" Y="2.680325439453" />
                  <Point X="-28.87330078125" Y="2.846114013672" />
                  <Point X="-28.726337890625" Y="3.035013183594" />
                  <Point X="-28.473083984375" Y="2.888796875" />
                  <Point X="-28.25415625" Y="2.7623984375" />
                  <Point X="-28.244916015625" Y="2.757716064453" />
                  <Point X="-28.225982421875" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.126400390625" Y="2.728649414062" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310791016" />
                  <Point X="-27.97643359375" Y="2.734227294922" />
                  <Point X="-27.95699609375" Y="2.741302001953" />
                  <Point X="-27.9384453125" Y="2.750450439453" />
                  <Point X="-27.929419921875" Y="2.755530761719" />
                  <Point X="-27.911166015625" Y="2.767159667969" />
                  <Point X="-27.90274609375" Y="2.773192626953" />
                  <Point X="-27.886615234375" Y="2.786138916016" />
                  <Point X="-27.878904296875" Y="2.793052246094" />
                  <Point X="-27.858552734375" Y="2.813403320312" />
                  <Point X="-27.8181796875" Y="2.853775634766" />
                  <Point X="-27.811265625" Y="2.861488769531" />
                  <Point X="-27.79831640625" Y="2.877623046875" />
                  <Point X="-27.79228125" Y="2.886044189453" />
                  <Point X="-27.78065234375" Y="2.904298828125" />
                  <Point X="-27.7755703125" Y="2.913326171875" />
                  <Point X="-27.766423828125" Y="2.931875" />
                  <Point X="-27.759353515625" Y="2.951299316406" />
                  <Point X="-27.754435546875" Y="2.971387939453" />
                  <Point X="-27.7525234375" Y="2.981573730469" />
                  <Point X="-27.749697265625" Y="3.003032958984" />
                  <Point X="-27.748908203125" Y="3.013365478516" />
                  <Point X="-27.74845703125" Y="3.034047607422" />
                  <Point X="-27.748794921875" Y="3.044397216797" />
                  <Point X="-27.751302734375" Y="3.073068847656" />
                  <Point X="-27.756279296875" Y="3.129946777344" />
                  <Point X="-27.757744140625" Y="3.140203369141" />
                  <Point X="-27.76178125" Y="3.160498291016" />
                  <Point X="-27.764353515625" Y="3.170536621094" />
                  <Point X="-27.77086328125" Y="3.191178466797" />
                  <Point X="-27.774513671875" Y="3.200874023438" />
                  <Point X="-27.78284375" Y="3.219801269531" />
                  <Point X="-27.7875234375" Y="3.229032958984" />
                  <Point X="-28.03826953125" Y="3.663337890625" />
                  <Point X="-28.05938671875" Y="3.699915527344" />
                  <Point X="-28.028033203125" Y="3.723954589844" />
                  <Point X="-27.6483671875" Y="4.015040527344" />
                  <Point X="-27.445220703125" Y="4.127902832031" />
                  <Point X="-27.192525390625" Y="4.268295410156" />
                  <Point X="-27.1855859375" Y="4.259251464844" />
                  <Point X="-27.118564453125" Y="4.171908203125" />
                  <Point X="-27.11181640625" Y="4.16404296875" />
                  <Point X="-27.097509765625" Y="4.149099609375" />
                  <Point X="-27.089951171875" Y="4.142020996094" />
                  <Point X="-27.07337109375" Y="4.128109375" />
                  <Point X="-27.065087890625" Y="4.121895507812" />
                  <Point X="-27.047890625" Y="4.110404785156" />
                  <Point X="-27.0389765625" Y="4.105127929688" />
                  <Point X="-27.007064453125" Y="4.088516113281" />
                  <Point X="-26.943759765625" Y="4.055561035156" />
                  <Point X="-26.93432421875" Y="4.051286132812" />
                  <Point X="-26.915046875" Y="4.043790039063" />
                  <Point X="-26.905205078125" Y="4.040568847656" />
                  <Point X="-26.884298828125" Y="4.034966796875" />
                  <Point X="-26.8741640625" Y="4.032835449219" />
                  <Point X="-26.853720703125" Y="4.029688476562" />
                  <Point X="-26.8330546875" Y="4.028785888672" />
                  <Point X="-26.812416015625" Y="4.030138427734" />
                  <Point X="-26.802134765625" Y="4.031377929688" />
                  <Point X="-26.7808203125" Y="4.035135742188" />
                  <Point X="-26.77073046875" Y="4.037488037109" />
                  <Point X="-26.7508671875" Y="4.043277587891" />
                  <Point X="-26.74109375" Y="4.04671484375" />
                  <Point X="-26.707857421875" Y="4.060482910156" />
                  <Point X="-26.641919921875" Y="4.087794433594" />
                  <Point X="-26.63258203125" Y="4.092273681641" />
                  <Point X="-26.614451171875" Y="4.102220703125" />
                  <Point X="-26.605658203125" Y="4.107688964844" />
                  <Point X="-26.587927734375" Y="4.120103515625" />
                  <Point X="-26.579779296875" Y="4.126497558594" />
                  <Point X="-26.5642265625" Y="4.140136230469" />
                  <Point X="-26.550248046875" Y="4.155391113281" />
                  <Point X="-26.538017578125" Y="4.172072753906" />
                  <Point X="-26.532361328125" Y="4.180744140625" />
                  <Point X="-26.5215390625" Y="4.199488769531" />
                  <Point X="-26.516859375" Y="4.208722167969" />
                  <Point X="-26.50852734375" Y="4.227654785156" />
                  <Point X="-26.504875" Y="4.237354003906" />
                  <Point X="-26.494056640625" Y="4.271665039063" />
                  <Point X="-26.472595703125" Y="4.33973046875" />
                  <Point X="-26.470025390625" Y="4.349764160156" />
                  <Point X="-26.465990234375" Y="4.370050292969" />
                  <Point X="-26.464525390625" Y="4.380302734375" />
                  <Point X="-26.462638671875" Y="4.401863769531" />
                  <Point X="-26.46230078125" Y="4.412216796875" />
                  <Point X="-26.462751953125" Y="4.4328984375" />
                  <Point X="-26.463541015625" Y="4.443227050781" />
                  <Point X="-26.479265625" Y="4.562655761719" />
                  <Point X="-26.422666015625" Y="4.578523925781" />
                  <Point X="-25.931177734375" Y="4.716319824219" />
                  <Point X="-25.68490234375" Y="4.745143066406" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.290431640625" Y="4.503434082031" />
                  <Point X="-25.22566796875" Y="4.261727539062" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.767251953125" Y="4.247105957031" />
                  <Point X="-24.76201953125" Y="4.261727539062" />
                  <Point X="-24.6335390625" Y="4.741219726562" />
                  <Point X="-24.621806640625" Y="4.785005859375" />
                  <Point X="-24.60127734375" Y="4.782855957031" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.96836328125" Y="4.688718261719" />
                  <Point X="-23.546408203125" Y="4.586844726562" />
                  <Point X="-23.415734375" Y="4.53944921875" />
                  <Point X="-23.141734375" Y="4.440067871094" />
                  <Point X="-23.0134921875" Y="4.380092773438" />
                  <Point X="-22.7495546875" Y="4.256658203125" />
                  <Point X="-22.625595703125" Y="4.184439453125" />
                  <Point X="-22.370564453125" Y="4.035857910156" />
                  <Point X="-22.253712890625" Y="3.952759033203" />
                  <Point X="-22.182216796875" Y="3.901915771484" />
                  <Point X="-22.64591796875" Y="3.098763671875" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.93762109375" Y="2.593112060547" />
                  <Point X="-22.9468125" Y="2.573447265625" />
                  <Point X="-22.955814453125" Y="2.549568359375" />
                  <Point X="-22.958697265625" Y="2.540598632812" />
                  <Point X="-22.965892578125" Y="2.513690917969" />
                  <Point X="-22.98016796875" Y="2.460312255859" />
                  <Point X="-22.9820859375" Y="2.451469970703" />
                  <Point X="-22.986353515625" Y="2.420223388672" />
                  <Point X="-22.987244140625" Y="2.383237060547" />
                  <Point X="-22.986587890625" Y="2.369573242188" />
                  <Point X="-22.98378125" Y="2.346305908203" />
                  <Point X="-22.978216796875" Y="2.3001484375" />
                  <Point X="-22.97619921875" Y="2.289028808594" />
                  <Point X="-22.970857421875" Y="2.267112792969" />
                  <Point X="-22.967533203125" Y="2.25631640625" />
                  <Point X="-22.959267578125" Y="2.2342265625" />
                  <Point X="-22.954685546875" Y="2.223895507812" />
                  <Point X="-22.9443203125" Y="2.20384375" />
                  <Point X="-22.938537109375" Y="2.194123046875" />
                  <Point X="-22.924138671875" Y="2.172905517578" />
                  <Point X="-22.895578125" Y="2.130814453125" />
                  <Point X="-22.89018359375" Y="2.123624267578" />
                  <Point X="-22.869537109375" Y="2.100124511719" />
                  <Point X="-22.84240625" Y="2.075389648438" />
                  <Point X="-22.83174609375" Y="2.066983154297" />
                  <Point X="-22.810529296875" Y="2.0525859375" />
                  <Point X="-22.7684375" Y="2.024025756836" />
                  <Point X="-22.75871875" Y="2.018244628906" />
                  <Point X="-22.738669921875" Y="2.007880615234" />
                  <Point X="-22.72833984375" Y="2.003297485352" />
                  <Point X="-22.706251953125" Y="1.995031860352" />
                  <Point X="-22.695451171875" Y="1.991707763672" />
                  <Point X="-22.673529296875" Y="1.986364990234" />
                  <Point X="-22.662408203125" Y="1.984346557617" />
                  <Point X="-22.639140625" Y="1.981540893555" />
                  <Point X="-22.592984375" Y="1.975974975586" />
                  <Point X="-22.583955078125" Y="1.975320678711" />
                  <Point X="-22.55242578125" Y="1.975497314453" />
                  <Point X="-22.5156875" Y="1.979822509766" />
                  <Point X="-22.50225390625" Y="1.982395629883" />
                  <Point X="-22.475345703125" Y="1.989591186523" />
                  <Point X="-22.421966796875" Y="2.003865356445" />
                  <Point X="-22.416005859375" Y="2.005670654297" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572875977" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.371748046875" Y="2.600729492188" />
                  <Point X="-21.059595703125" Y="2.780950683594" />
                  <Point X="-20.956037109375" Y="2.637028076172" />
                  <Point X="-20.89090234375" Y="2.529392333984" />
                  <Point X="-20.863115234375" Y="2.483471923828" />
                  <Point X="-21.446853515625" Y="2.035552856445" />
                  <Point X="-21.827046875" Y="1.743819702148" />
                  <Point X="-21.831861328125" Y="1.739867797852" />
                  <Point X="-21.84787109375" Y="1.725225219727" />
                  <Point X="-21.865328125" Y="1.706607788086" />
                  <Point X="-21.87142578125" Y="1.69942175293" />
                  <Point X="-21.890791015625" Y="1.674158325195" />
                  <Point X="-21.92920703125" Y="1.624040527344" />
                  <Point X="-21.9343671875" Y="1.61659765625" />
                  <Point X="-21.95026171875" Y="1.589367797852" />
                  <Point X="-21.965234375" Y="1.555549072266" />
                  <Point X="-21.96985546875" Y="1.542680175781" />
                  <Point X="-21.9770703125" Y="1.516885864258" />
                  <Point X="-21.991380859375" Y="1.465715698242" />
                  <Point X="-21.9937734375" Y="1.454670166016" />
                  <Point X="-21.997228515625" Y="1.432368652344" />
                  <Point X="-21.998291015625" Y="1.421112670898" />
                  <Point X="-21.999107421875" Y="1.397541625977" />
                  <Point X="-21.998826171875" Y="1.386236694336" />
                  <Point X="-21.996921875" Y="1.363750244141" />
                  <Point X="-21.995298828125" Y="1.352568725586" />
                  <Point X="-21.989376953125" Y="1.323869140625" />
                  <Point X="-21.97762890625" Y="1.266935913086" />
                  <Point X="-21.97540234375" Y="1.258239013672" />
                  <Point X="-21.96531640625" Y="1.228610961914" />
                  <Point X="-21.94971484375" Y="1.195374633789" />
                  <Point X="-21.943083984375" Y="1.183529785156" />
                  <Point X="-21.926978515625" Y="1.159048828125" />
                  <Point X="-21.89502734375" Y="1.11048425293" />
                  <Point X="-21.88826171875" Y="1.101424438477" />
                  <Point X="-21.873705078125" Y="1.084176757813" />
                  <Point X="-21.8659140625" Y="1.075988769531" />
                  <Point X="-21.848669921875" Y="1.059899291992" />
                  <Point X="-21.83996484375" Y="1.052695922852" />
                  <Point X="-21.821755859375" Y="1.039370239258" />
                  <Point X="-21.812251953125" Y="1.033247924805" />
                  <Point X="-21.78891015625" Y="1.020109191895" />
                  <Point X="-21.742609375" Y="0.994045349121" />
                  <Point X="-21.734517578125" Y="0.989986328125" />
                  <Point X="-21.7053203125" Y="0.978083557129" />
                  <Point X="-21.66972265625" Y="0.968020935059" />
                  <Point X="-21.656328125" Y="0.965257751465" />
                  <Point X="-21.62476953125" Y="0.961086791992" />
                  <Point X="-21.562166015625" Y="0.952812927246" />
                  <Point X="-21.555966796875" Y="0.952199707031" />
                  <Point X="-21.53428125" Y="0.951222839355" />
                  <Point X="-21.50878125" Y="0.952032348633" />
                  <Point X="-21.49939453125" Y="0.952797180176" />
                  <Point X="-20.55685546875" Y="1.076885131836" />
                  <Point X="-20.295296875" Y="1.111319824219" />
                  <Point X="-20.247310546875" Y="0.91420916748" />
                  <Point X="-20.22678515625" Y="0.782378356934" />
                  <Point X="-20.216126953125" Y="0.713921447754" />
                  <Point X="-20.872296875" Y="0.538100952148" />
                  <Point X="-21.3080078125" Y="0.421352813721" />
                  <Point X="-21.31396875" Y="0.419544219971" />
                  <Point X="-21.334376953125" Y="0.412135955811" />
                  <Point X="-21.357619140625" Y="0.40161807251" />
                  <Point X="-21.365994140625" Y="0.397316192627" />
                  <Point X="-21.396998046875" Y="0.379395263672" />
                  <Point X="-21.45850390625" Y="0.343843780518" />
                  <Point X="-21.4661171875" Y="0.338945007324" />
                  <Point X="-21.49122265625" Y="0.31987121582" />
                  <Point X="-21.518001953125" Y="0.294353271484" />
                  <Point X="-21.52719921875" Y="0.284229675293" />
                  <Point X="-21.545802734375" Y="0.260525360107" />
                  <Point X="-21.582705078125" Y="0.213501831055" />
                  <Point X="-21.589142578125" Y="0.204211868286" />
                  <Point X="-21.600869140625" Y="0.184928955078" />
                  <Point X="-21.606158203125" Y="0.174936141968" />
                  <Point X="-21.615931640625" Y="0.153472625732" />
                  <Point X="-21.619994140625" Y="0.142927383423" />
                  <Point X="-21.62683984375" Y="0.121428352356" />
                  <Point X="-21.629623046875" Y="0.110474250793" />
                  <Point X="-21.63582421875" Y="0.078095726013" />
                  <Point X="-21.648125" Y="0.013863758087" />
                  <Point X="-21.649396484375" Y="0.004966600418" />
                  <Point X="-21.6514140625" Y="-0.026261882782" />
                  <Point X="-21.64971875" Y="-0.062940513611" />
                  <Point X="-21.648125" Y="-0.076423873901" />
                  <Point X="-21.641923828125" Y="-0.10880255127" />
                  <Point X="-21.629623046875" Y="-0.173034225464" />
                  <Point X="-21.62683984375" Y="-0.183990097046" />
                  <Point X="-21.619994140625" Y="-0.205487808228" />
                  <Point X="-21.615931640625" Y="-0.21602947998" />
                  <Point X="-21.606158203125" Y="-0.237494033813" />
                  <Point X="-21.600873046875" Y="-0.247480300903" />
                  <Point X="-21.589150390625" Y="-0.266761138916" />
                  <Point X="-21.582712890625" Y="-0.276055847168" />
                  <Point X="-21.564111328125" Y="-0.299759857178" />
                  <Point X="-21.52720703125" Y="-0.346783538818" />
                  <Point X="-21.52128125" Y="-0.353633850098" />
                  <Point X="-21.49885546875" Y="-0.375809875488" />
                  <Point X="-21.4698203125" Y="-0.398726623535" />
                  <Point X="-21.45850390625" Y="-0.406403900146" />
                  <Point X="-21.4275" Y="-0.424324981689" />
                  <Point X="-21.365994140625" Y="-0.459876312256" />
                  <Point X="-21.360505859375" Y="-0.46281338501" />
                  <Point X="-21.34084375" Y="-0.472015960693" />
                  <Point X="-21.31697265625" Y="-0.481027557373" />
                  <Point X="-21.3080078125" Y="-0.483912780762" />
                  <Point X="-20.44365625" Y="-0.715515319824" />
                  <Point X="-20.21512109375" Y="-0.776750976562" />
                  <Point X="-20.23838671875" Y="-0.931061645508" />
                  <Point X="-20.264685546875" Y="-1.046308105469" />
                  <Point X="-20.2721953125" Y="-1.079219848633" />
                  <Point X="-21.057916015625" Y="-0.975778015137" />
                  <Point X="-21.563216796875" Y="-0.909253662109" />
                  <Point X="-21.571375" Y="-0.908535705566" />
                  <Point X="-21.59990234375" Y="-0.908042419434" />
                  <Point X="-21.63327734375" Y="-0.910840942383" />
                  <Point X="-21.645517578125" Y="-0.912676147461" />
                  <Point X="-21.706369140625" Y="-0.925902404785" />
                  <Point X="-21.82708203125" Y="-0.952140014648" />
                  <Point X="-21.842125" Y="-0.956742248535" />
                  <Point X="-21.871244140625" Y="-0.968366027832" />
                  <Point X="-21.8853203125" Y="-0.975387695312" />
                  <Point X="-21.9131484375" Y="-0.992280334473" />
                  <Point X="-21.925875" Y="-1.001529602051" />
                  <Point X="-21.949625" Y="-1.022" />
                  <Point X="-21.9606484375" Y="-1.033221313477" />
                  <Point X="-21.9974296875" Y="-1.077456420898" />
                  <Point X="-22.070392578125" Y="-1.165209228516" />
                  <Point X="-22.078673828125" Y="-1.176849365234" />
                  <Point X="-22.093396484375" Y="-1.201236816406" />
                  <Point X="-22.099837890625" Y="-1.213984130859" />
                  <Point X="-22.1111796875" Y="-1.241369384766" />
                  <Point X="-22.11563671875" Y="-1.254934692383" />
                  <Point X="-22.122466796875" Y="-1.282580322266" />
                  <Point X="-22.12483984375" Y="-1.296660644531" />
                  <Point X="-22.130111328125" Y="-1.353947143555" />
                  <Point X="-22.140568359375" Y="-1.467591064453" />
                  <Point X="-22.140708984375" Y="-1.483315185547" />
                  <Point X="-22.138392578125" Y="-1.514580932617" />
                  <Point X="-22.135935546875" Y="-1.530122802734" />
                  <Point X="-22.128205078125" Y="-1.561743530273" />
                  <Point X="-22.12321484375" Y="-1.576666748047" />
                  <Point X="-22.110841796875" Y="-1.605480834961" />
                  <Point X="-22.103458984375" Y="-1.619371826172" />
                  <Point X="-22.069783203125" Y="-1.671752075195" />
                  <Point X="-22.002978515625" Y="-1.775662231445" />
                  <Point X="-21.99825390625" Y="-1.782358520508" />
                  <Point X="-21.980201171875" Y="-1.804454101562" />
                  <Point X="-21.9565078125" Y="-1.828121337891" />
                  <Point X="-21.947201171875" Y="-1.836277709961" />
                  <Point X="-21.145078125" Y="-2.451769287109" />
                  <Point X="-20.912828125" Y="-2.629981445312" />
                  <Point X="-20.954513671875" Y="-2.697434570312" />
                  <Point X="-20.998724609375" Y="-2.760251953125" />
                  <Point X="-21.701505859375" Y="-2.354500976562" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.184970703125" Y="-2.079513916016" />
                  <Point X="-22.216876953125" Y="-2.069326416016" />
                  <Point X="-22.228888671875" Y="-2.066337646484" />
                  <Point X="-22.301310546875" Y="-2.053258300781" />
                  <Point X="-22.444978515625" Y="-2.027312011719" />
                  <Point X="-22.460638671875" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539859375" Y="-2.031461181641" />
                  <Point X="-22.55515625" Y="-2.03513671875" />
                  <Point X="-22.584931640625" Y="-2.044960571289" />
                  <Point X="-22.59941015625" Y="-2.051109130859" />
                  <Point X="-22.65957421875" Y="-2.0827734375" />
                  <Point X="-22.778927734375" Y="-2.145588134766" />
                  <Point X="-22.791029296875" Y="-2.153169433594" />
                  <Point X="-22.8139609375" Y="-2.1700625" />
                  <Point X="-22.824791015625" Y="-2.179374267578" />
                  <Point X="-22.84575" Y="-2.200333496094" />
                  <Point X="-22.855060546875" Y="-2.211162353516" />
                  <Point X="-22.871953125" Y="-2.234093017578" />
                  <Point X="-22.87953515625" Y="-2.246194824219" />
                  <Point X="-22.91119921875" Y="-2.306359619141" />
                  <Point X="-22.974015625" Y="-2.425712890625" />
                  <Point X="-22.9801640625" Y="-2.440193115234" />
                  <Point X="-22.98998828125" Y="-2.469971191406" />
                  <Point X="-22.9936640625" Y="-2.485269042969" />
                  <Point X="-22.99862109375" Y="-2.517442626953" />
                  <Point X="-22.999720703125" Y="-2.533133789062" />
                  <Point X="-22.99931640625" Y="-2.564484619141" />
                  <Point X="-22.9978125" Y="-2.580144287109" />
                  <Point X="-22.984732421875" Y="-2.652565917969" />
                  <Point X="-22.95878515625" Y="-2.796234619141" />
                  <Point X="-22.95698046875" Y="-2.804230957031" />
                  <Point X="-22.94876171875" Y="-2.831540771484" />
                  <Point X="-22.935927734375" Y="-2.862479248047" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.41500390625" Y="-3.766355957031" />
                  <Point X="-22.264103515625" Y="-4.027722412109" />
                  <Point X="-22.2762421875" Y="-4.036082763672" />
                  <Point X="-22.822662109375" Y="-3.323977539062" />
                  <Point X="-23.16608203125" Y="-2.876422363281" />
                  <Point X="-23.17134765625" Y="-2.870141113281" />
                  <Point X="-23.19116796875" Y="-2.849625732422" />
                  <Point X="-23.21674609375" Y="-2.828004638672" />
                  <Point X="-23.22669921875" Y="-2.820646972656" />
                  <Point X="-23.298126953125" Y="-2.774725585938" />
                  <Point X="-23.43982421875" Y="-2.683628173828" />
                  <Point X="-23.453716796875" Y="-2.676245117188" />
                  <Point X="-23.482529296875" Y="-2.663873535156" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.66972265625" Y="-2.653704833984" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920125" Y="-2.699414794922" />
                  <Point X="-23.944505859375" Y="-2.714135742188" />
                  <Point X="-23.956138671875" Y="-2.722413330078" />
                  <Point X="-24.016458984375" Y="-2.772567871094" />
                  <Point X="-24.136123046875" Y="-2.872063720703" />
                  <Point X="-24.147345703125" Y="-2.883087646484" />
                  <Point X="-24.16781640625" Y="-2.906837158203" />
                  <Point X="-24.177064453125" Y="-2.919562744141" />
                  <Point X="-24.19395703125" Y="-2.947389404297" />
                  <Point X="-24.20098046875" Y="-2.961467041016" />
                  <Point X="-24.21260546875" Y="-2.990588378906" />
                  <Point X="-24.21720703125" Y="-3.005632080078" />
                  <Point X="-24.2352421875" Y="-3.088610107422" />
                  <Point X="-24.271021484375" Y="-3.253219970703" />
                  <Point X="-24.2724140625" Y="-3.261287109375" />
                  <Point X="-24.275275390625" Y="-3.289677246094" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520263672" />
                  <Point X="-24.16691015625" Y="-4.152328125" />
                  <Point X="-24.2106796875" Y="-3.988976074219" />
                  <Point X="-24.344931640625" Y="-3.487936767578" />
                  <Point X="-24.347392578125" Y="-3.480121582031" />
                  <Point X="-24.357853515625" Y="-3.453578613281" />
                  <Point X="-24.3732109375" Y="-3.423815429688" />
                  <Point X="-24.37958984375" Y="-3.413209716797" />
                  <Point X="-24.434462890625" Y="-3.334148681641" />
                  <Point X="-24.543318359375" Y="-3.177309326172" />
                  <Point X="-24.553328125" Y="-3.165172119141" />
                  <Point X="-24.575212890625" Y="-3.142716308594" />
                  <Point X="-24.587087890625" Y="-3.132397705078" />
                  <Point X="-24.61334375" Y="-3.113153808594" />
                  <Point X="-24.6267578125" Y="-3.104936767578" />
                  <Point X="-24.6547578125" Y="-3.090829345703" />
                  <Point X="-24.66934375" Y="-3.084938964844" />
                  <Point X="-24.754255859375" Y="-3.058585205078" />
                  <Point X="-24.922703125" Y="-3.006305664062" />
                  <Point X="-24.936623046875" Y="-3.003109619141" />
                  <Point X="-24.964783203125" Y="-2.998840087891" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.051064453125" Y="-3.003109130859" />
                  <Point X="-25.064984375" Y="-3.006305175781" />
                  <Point X="-25.149896484375" Y="-3.032658691406" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.3329296875" Y="-3.090829589844" />
                  <Point X="-25.3609296875" Y="-3.104937255859" />
                  <Point X="-25.37434375" Y="-3.113153808594" />
                  <Point X="-25.400599609375" Y="-3.132397705078" />
                  <Point X="-25.4124765625" Y="-3.142718017578" />
                  <Point X="-25.434361328125" Y="-3.165175048828" />
                  <Point X="-25.444369140625" Y="-3.177311767578" />
                  <Point X="-25.499240234375" Y="-3.256372558594" />
                  <Point X="-25.608095703125" Y="-3.413212158203" />
                  <Point X="-25.61246875" Y="-3.420131347656" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213378906" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.90908203125" Y="-4.48188671875" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.952183529812" Y="-3.781872037086" />
                  <Point X="-27.444994734795" Y="-4.086621810424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.904676330639" Y="-3.699587069454" />
                  <Point X="-27.38319480497" Y="-4.012924781779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.857169131465" Y="-3.617302101822" />
                  <Point X="-27.299672940824" Y="-3.952279608036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.131119447917" Y="-4.654417383151" />
                  <Point X="-25.979692825432" Y="-4.745403677275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.896312581625" Y="-2.882091552365" />
                  <Point X="-28.730626195603" Y="-2.981645976835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.809661932291" Y="-3.53501713419" />
                  <Point X="-27.212339298627" Y="-3.893924781613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.119667807696" Y="-4.550468050048" />
                  <Point X="-25.954113630229" Y="-4.649943035604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.035657203919" Y="-2.68753468362" />
                  <Point X="-28.636559695433" Y="-2.927336659622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.762154733118" Y="-3.452732166559" />
                  <Point X="-27.125005928853" Y="-3.835569791502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.136947066746" Y="-4.429255451023" />
                  <Point X="-25.928534435025" Y="-4.554482393932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.138662174522" Y="-2.514812880485" />
                  <Point X="-28.542493194578" Y="-2.873027342819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.714647533944" Y="-3.370447198927" />
                  <Point X="-27.023077928477" Y="-3.785984140169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.161985840557" Y="-4.303380465156" />
                  <Point X="-25.902955362774" Y="-4.459021678384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.133765175411" Y="-2.406925121667" />
                  <Point X="-28.448426650105" Y="-2.818718052226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.66714033477" Y="-3.288162231295" />
                  <Point X="-26.844307941135" Y="-3.782569812691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.188916444772" Y="-4.176368752901" />
                  <Point X="-25.877376680892" Y="-4.363560728278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.052760121874" Y="-2.344767695544" />
                  <Point X="-28.354360105633" Y="-2.764408761632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.619633135597" Y="-3.205877263664" />
                  <Point X="-26.637271431689" Y="-3.796139725183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.397022660074" Y="-3.940495750816" />
                  <Point X="-25.85179799901" Y="-4.268099778172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.971755068336" Y="-2.282610269422" />
                  <Point X="-28.26029356116" Y="-2.710099471038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.572125920248" Y="-3.123592305751" />
                  <Point X="-25.826219317128" Y="-4.172638828066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.890750014798" Y="-2.220452843299" />
                  <Point X="-28.166227016687" Y="-2.655790180444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.524618680329" Y="-3.041307362602" />
                  <Point X="-25.800640635246" Y="-4.07717787796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.809744961261" Y="-2.158295417177" />
                  <Point X="-28.072160472215" Y="-2.60148088985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.477111440409" Y="-2.959022419453" />
                  <Point X="-25.775061953364" Y="-3.981716927854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.728739907723" Y="-2.096137991054" />
                  <Point X="-27.978093927742" Y="-2.547171599256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.429604200489" Y="-2.876737476304" />
                  <Point X="-25.749483271482" Y="-3.886255977748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.647734854185" Y="-2.033980564932" />
                  <Point X="-27.88402738327" Y="-2.492862308662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.38209696057" Y="-2.794452533155" />
                  <Point X="-25.7239045896" Y="-3.790795027642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.566729800648" Y="-1.97182313881" />
                  <Point X="-27.789960838797" Y="-2.438553018068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.335560391252" Y="-2.711584352267" />
                  <Point X="-25.698325907718" Y="-3.695334077536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.686012046954" Y="-1.188460342692" />
                  <Point X="-29.547396886534" Y="-1.271748733788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.48572474711" Y="-1.909665712687" />
                  <Point X="-27.695894294324" Y="-2.384243727475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.311506468123" Y="-2.615207234672" />
                  <Point X="-25.672747225836" Y="-3.59987312743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.719454335984" Y="-1.057536015468" />
                  <Point X="-29.396095625786" Y="-1.251829530245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.404719686236" Y="-1.847508290973" />
                  <Point X="-27.570583123201" Y="-2.348708102592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.343304348149" Y="-2.485270968061" />
                  <Point X="-25.647168543954" Y="-3.504412177325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.747054569856" Y="-0.930121949123" />
                  <Point X="-29.244794346188" Y="-1.231910338029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.323714625264" Y="-1.785350869317" />
                  <Point X="-25.609857960821" Y="-3.416000464666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.764394291368" Y="-0.808873020586" />
                  <Point X="-29.09349305055" Y="-1.211991155451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.242709564292" Y="-1.723193447662" />
                  <Point X="-25.555695864565" Y="-3.337714162615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.781735898134" Y="-0.687622959275" />
                  <Point X="-28.942191754912" Y="-1.192071972872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.16170450332" Y="-1.661036026006" />
                  <Point X="-25.50141169391" Y="-3.259501210263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.195964989678" Y="-4.043892725075" />
                  <Point X="-24.179916476746" Y="-4.05353564449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.693571272023" Y="-0.62976743836" />
                  <Point X="-28.790890459273" Y="-1.172152790293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.080699442348" Y="-1.598878604351" />
                  <Point X="-25.44712854495" Y="-3.181287644014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.231360116977" Y="-3.91179501424" />
                  <Point X="-24.1957609749" Y="-3.933185136786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.5660057685" Y="-0.595586353038" />
                  <Point X="-28.639589163635" Y="-1.152233607715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.007295433791" Y="-1.532154009636" />
                  <Point X="-25.375121363824" Y="-3.113723750705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.266755305276" Y="-3.779697266753" />
                  <Point X="-24.211605473053" Y="-3.812834629082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.438440264976" Y="-0.561405267717" />
                  <Point X="-28.488287867997" Y="-1.132314425136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.963663425876" Y="-1.447540592186" />
                  <Point X="-25.265744728604" Y="-3.068613690715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.302150493575" Y="-3.647599519266" />
                  <Point X="-24.227449971207" Y="-3.692484121377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.310874761452" Y="-0.527224182395" />
                  <Point X="-28.336986572359" Y="-1.112395242557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.947362833297" Y="-1.346504803598" />
                  <Point X="-25.144116860412" Y="-3.030864914152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.337545681873" Y="-3.515501771779" />
                  <Point X="-24.243294469361" Y="-3.572133613673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.183309257929" Y="-0.493043097073" />
                  <Point X="-28.162827669085" Y="-1.106210296253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.000176811583" Y="-1.203940791176" />
                  <Point X="-25.014071106437" Y="-2.998174113652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.419831838461" Y="-3.355229088059" />
                  <Point X="-24.259138967514" Y="-3.451783105969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.055743754405" Y="-0.458862011752" />
                  <Point X="-24.702177545885" Y="-3.07474849878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.555646538701" Y="-3.162793210464" />
                  <Point X="-24.274703149746" Y="-3.331601029063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.928178285295" Y="-0.424680905752" />
                  <Point X="-24.265208433181" Y="-3.226475857601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.800612838499" Y="-0.390499786345" />
                  <Point X="-24.243901339061" Y="-3.128448278628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.673047391704" Y="-0.356318666938" />
                  <Point X="-24.222594738066" Y="-3.030420403355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.545481944908" Y="-0.32213754753" />
                  <Point X="-24.189245607042" Y="-2.939628410131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.43498173037" Y="-0.277702602105" />
                  <Point X="-24.128201474794" Y="-2.865477252486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.358587680383" Y="-3.928768492435" />
                  <Point X="-22.30139368706" Y="-3.963134110668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.778278015739" Y="0.640261408195" />
                  <Point X="-29.627015550486" Y="0.549373749687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.358203857648" Y="-0.213005229501" />
                  <Point X="-24.05082341305" Y="-2.801140509829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.516384696973" Y="-3.723124306631" />
                  <Point X="-22.399370685802" Y="-3.793433417818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.76321716358" Y="0.742042107979" />
                  <Point X="-29.294103186079" Y="0.460169993063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.312951266011" Y="-0.129365556989" />
                  <Point X="-23.973445664959" Y="-2.736803578711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.674181713562" Y="-3.517480120827" />
                  <Point X="-22.497347370276" Y="-3.623732913799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.74815631142" Y="0.843822807762" />
                  <Point X="-28.961191328491" Y="0.370966540967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.294926048238" Y="-0.029366027763" />
                  <Point X="-23.880482303933" Y="-2.681831428628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.831978592765" Y="-3.311836017573" />
                  <Point X="-22.595323995085" Y="-3.454032445631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.73309545926" Y="0.945603507546" />
                  <Point X="-28.628279470904" Y="0.281763088871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.326624748654" Y="0.100510645727" />
                  <Point X="-23.733129657542" Y="-2.659539658219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.98977328242" Y="-3.106193229933" />
                  <Point X="-22.693300619894" Y="-3.284331977463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.708528765898" Y="1.041672521701" />
                  <Point X="-23.569839210619" Y="-2.646824284502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.147567972074" Y="-2.900550442292" />
                  <Point X="-22.791277244703" Y="-3.114631509294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.682701462477" Y="1.136984084915" />
                  <Point X="-22.889253869511" Y="-2.944931041126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.656874165153" Y="1.232295651792" />
                  <Point X="-22.959577760027" Y="-2.791846012003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.61036455647" Y="1.315180032264" />
                  <Point X="-22.982030793977" Y="-2.667524695389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.374157758925" Y="1.284082842408" />
                  <Point X="-22.999552636808" Y="-2.546166337324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.137950961381" Y="1.252985652552" />
                  <Point X="-22.982034375092" Y="-2.445862198167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.901743966003" Y="1.221888343826" />
                  <Point X="-22.939682817391" Y="-2.360479408609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.69690883364" Y="1.209641152132" />
                  <Point X="-22.895366999973" Y="-2.27627686536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.578258483732" Y="1.249179002173" />
                  <Point X="-22.842580539042" Y="-2.197163998216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.499391575743" Y="1.312621155754" />
                  <Point X="-22.760289742121" Y="-2.135779124659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.453348094251" Y="1.395785613698" />
                  <Point X="-22.66196210142" Y="-2.084030158982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.424055831002" Y="1.489015219005" />
                  <Point X="-22.557561401893" Y="-2.035930255192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.437277548974" Y="1.607789801385" />
                  <Point X="-22.362731401984" Y="-2.042165756806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.622216357884" Y="1.829742421325" />
                  <Point X="-20.99410556313" Y="-2.753688952822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.223269987484" Y="2.30172205001" />
                  <Point X="-20.940639607584" Y="-2.674984367232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.175376320937" Y="2.383774804617" />
                  <Point X="-21.207878047418" Y="-2.403581140111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.127482579095" Y="2.465827513982" />
                  <Point X="-21.873654645226" Y="-1.892712028683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.079588837252" Y="2.547880223346" />
                  <Point X="-22.077866599943" Y="-1.659178934423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.03169509541" Y="2.629932932711" />
                  <Point X="-22.138594168857" Y="-1.511859957038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.979609643797" Y="2.709467008747" />
                  <Point X="-22.134661125521" Y="-1.403392995156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.920852385139" Y="2.784992258673" />
                  <Point X="-22.124997097248" Y="-1.29836955643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.862094980865" Y="2.860517421104" />
                  <Point X="-22.095447061762" Y="-1.205294836309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.803336958663" Y="2.936042212247" />
                  <Point X="-22.03951467565" Y="-1.128072231717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.744578936461" Y="3.011567003389" />
                  <Point X="-21.978063526123" Y="-1.054165634727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.082945334104" Y="2.724847600242" />
                  <Point X="-21.904919287587" Y="-0.987284954436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.939905105903" Y="2.749730532915" />
                  <Point X="-21.791741868066" Y="-0.944458636054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.859629703098" Y="2.812326377429" />
                  <Point X="-21.656288280552" Y="-0.915017189761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.793961772091" Y="2.883699276489" />
                  <Point X="-21.458479253043" Y="-0.923042671744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.754565357852" Y="2.970857695378" />
                  <Point X="-21.222272160007" Y="-0.954140039149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.751917663662" Y="3.080096972943" />
                  <Point X="-20.986065191225" Y="-0.985237331895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.776685742349" Y="3.20580930877" />
                  <Point X="-20.749858506672" Y="-1.016334453858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.87072649372" Y="3.373144865588" />
                  <Point X="-20.51365182212" Y="-1.047431575821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.968703112044" Y="3.54284532986" />
                  <Point X="-21.555702871104" Y="-0.310473964735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.234088361278" Y="-0.503719458197" />
                  <Point X="-20.277445137567" Y="-1.078528697785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.05335545312" Y="3.704539760657" />
                  <Point X="-21.633476446672" Y="-0.15291271324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.901176196824" Y="-0.592923094677" />
                  <Point X="-20.250448716632" Y="-0.983919611244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.972312976732" Y="3.766674700862" />
                  <Point X="-21.651174329672" Y="-0.031448579569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.56826403237" Y="-0.682126731157" />
                  <Point X="-20.231374182693" Y="-0.884550574778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.891270115218" Y="3.82880940966" />
                  <Point X="-21.637187416505" Y="0.070977407862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.235352459839" Y="-0.771330011974" />
                  <Point X="-20.216052225273" Y="-0.782926762863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.810227253704" Y="3.890944118459" />
                  <Point X="-21.610367330505" Y="0.165692447121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.72918439219" Y="3.953078827257" />
                  <Point X="-21.557960324879" Y="0.245033314015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.648100334714" Y="4.015188783025" />
                  <Point X="-21.493866416562" Y="0.317351981324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.552262269961" Y="4.068433636846" />
                  <Point X="-21.404986570393" Y="0.374777754671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.456424205208" Y="4.121678490667" />
                  <Point X="-21.301119767333" Y="0.423198455823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.360586829515" Y="4.174923758519" />
                  <Point X="-21.173554164308" Y="0.457379481358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.264749545038" Y="4.228169081177" />
                  <Point X="-21.045988561282" Y="0.491560506893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.765491817669" Y="4.039014946791" />
                  <Point X="-21.897269092686" Y="1.113891626694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.648088361792" Y="0.964168738479" />
                  <Point X="-20.918422958257" Y="0.525741532428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.653937688296" Y="4.082816636297" />
                  <Point X="-21.979058933925" Y="1.273866094066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.454538749799" Y="0.95870257154" />
                  <Point X="-20.790857452313" Y="0.559922616296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.564492705617" Y="4.139902841371" />
                  <Point X="-21.99908724197" Y="1.396730488372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.303237541334" Y="0.978621806497" />
                  <Point X="-20.663292001354" Y="0.594103733201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.512199002895" Y="4.219311787518" />
                  <Point X="-21.982470528701" Y="1.497576332487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.151936332868" Y="0.998541041454" />
                  <Point X="-20.535726550396" Y="0.628284850107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.481445259938" Y="4.311663247222" />
                  <Point X="-22.925642318846" Y="2.175121290897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.59451422212" Y="1.976159457722" />
                  <Point X="-21.950371630115" Y="1.589119541147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.000635124402" Y="1.018460276412" />
                  <Point X="-20.408161099437" Y="0.662465967013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.462340041802" Y="4.411013846762" />
                  <Point X="-22.980477325805" Y="2.318899659859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.445637558704" Y="1.997535506318" />
                  <Point X="-21.896043404491" Y="1.667306022604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.849333915937" Y="1.038379511369" />
                  <Point X="-20.280595648478" Y="0.696647083919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.4748845654" Y="4.529381529712" />
                  <Point X="-22.984707966549" Y="2.432271858011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.337297975639" Y="2.043268690108" />
                  <Point X="-21.83205670376" Y="1.739689106728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.698032707471" Y="1.058298746326" />
                  <Point X="-20.225542693554" Y="0.77439810408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.388268784299" Y="4.588167690597" />
                  <Point X="-22.961721220194" Y="2.529290200102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.243231501195" Y="2.097578022779" />
                  <Point X="-21.751261023318" Y="1.801972336899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.546731486253" Y="1.078217973621" />
                  <Point X="-20.244579257579" Y="0.896666598459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.262500507726" Y="4.623428658817" />
                  <Point X="-22.923851730984" Y="2.617366088108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.149165026751" Y="2.151887355451" />
                  <Point X="-21.670256006566" Y="1.864129785124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.395430087212" Y="1.098137094069" />
                  <Point X="-20.274380591349" Y="1.025403219052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.136732231152" Y="4.658689627036" />
                  <Point X="-22.876344655599" Y="2.69965113012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.055098552308" Y="2.206196688122" />
                  <Point X="-21.589250989814" Y="1.92628723335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.010963954579" Y="4.693950595256" />
                  <Point X="-25.201289403417" Y="4.207449043234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.997688504789" Y="4.08511328125" />
                  <Point X="-22.828837580215" Y="2.781936172132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.961032077864" Y="2.260506020794" />
                  <Point X="-21.508245973062" Y="1.988444681575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.874734356064" Y="4.722925767098" />
                  <Point X="-25.248406274818" Y="4.346589888486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.870777633228" Y="4.119687709138" />
                  <Point X="-22.781330504831" Y="2.864221214143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.86696560342" Y="2.314815353465" />
                  <Point X="-21.427240959208" Y="2.050602131542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.720352691244" Y="4.740994077143" />
                  <Point X="-25.283800903788" Y="4.478687299894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.799403260404" Y="4.187631832035" />
                  <Point X="-22.733823429446" Y="2.946506256155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.772899128977" Y="2.369124686137" />
                  <Point X="-21.34623595443" Y="2.112759586962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.565970684588" Y="4.759062181793" />
                  <Point X="-25.319196435571" Y="4.610785253768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.758725638968" Y="4.274020423974" />
                  <Point X="-22.686316354062" Y="3.028791298167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.678832654533" Y="2.423434018808" />
                  <Point X="-21.265230949651" Y="2.174917042382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.41158857604" Y="4.777130225219" />
                  <Point X="-25.354592175473" Y="4.742883332692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.733146796336" Y="4.369481277492" />
                  <Point X="-22.638809242289" Y="3.111076318314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.58476618009" Y="2.47774335148" />
                  <Point X="-21.184225944873" Y="2.237074497802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.707567953704" Y="4.46494213101" />
                  <Point X="-22.591301923722" Y="3.193361214208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.490699705646" Y="2.532052684151" />
                  <Point X="-21.103220940095" Y="2.299231953222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.681989111073" Y="4.560402984527" />
                  <Point X="-22.543794605156" Y="3.275646110101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.396633231202" Y="2.586362016823" />
                  <Point X="-21.022215935316" Y="2.361389408642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.656410268441" Y="4.655863838045" />
                  <Point X="-22.49628728659" Y="3.357931005994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.302566729992" Y="2.640671333411" />
                  <Point X="-20.941210930538" Y="2.423546864062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.630831449952" Y="4.75132470607" />
                  <Point X="-22.448779968024" Y="3.440215901888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.208500219154" Y="2.694980644215" />
                  <Point X="-20.866899976015" Y="2.489726510663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.477236348423" Y="4.76986563102" />
                  <Point X="-22.401272649457" Y="3.522500797781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.114433708315" Y="2.749289955018" />
                  <Point X="-20.977696890368" Y="2.667130185942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.253849017213" Y="4.746471153642" />
                  <Point X="-22.353765330891" Y="3.604785693675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.976582101183" Y="4.690702555576" />
                  <Point X="-22.306258012325" Y="3.687070589568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.668230931655" Y="4.616256653711" />
                  <Point X="-22.258750693758" Y="3.769355485461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.264898260747" Y="4.484740108131" />
                  <Point X="-22.211243375192" Y="3.851640381355" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.39420703125" Y="-4.038151611328" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544189453" />
                  <Point X="-24.590552734375" Y="-3.442483154297" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.810576171875" Y="-3.240046142578" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.093576171875" Y="-3.214120117188" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285643798828" />
                  <Point X="-25.343150390625" Y="-3.364704589844" />
                  <Point X="-25.452005859375" Y="-3.521544189453" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.7255546875" Y="-4.5310625" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-25.9024375" Y="-4.976460449219" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.19788671875" Y="-4.912942871094" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.325310546875" Y="-4.673793457031" />
                  <Point X="-26.3091484375" Y="-4.5510390625" />
                  <Point X="-26.309681640625" Y="-4.5347578125" />
                  <Point X="-26.32996875" Y="-4.432775878906" />
                  <Point X="-26.3702109375" Y="-4.230466308594" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.464458984375" Y="-4.134069824219" />
                  <Point X="-26.61954296875" Y="-3.998064208984" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.752998046875" Y="-3.978962158203" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791015625" />
                  <Point X="-27.076333984375" Y="-4.031559326172" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.409255859375" Y="-4.352155761719" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.56588671875" Y="-4.347138183594" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-27.991041015625" Y="-4.063505859375" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.75552734375" Y="-3.061256103516" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597594238281" />
                  <Point X="-27.5139765625" Y="-2.568765869141" />
                  <Point X="-27.53132421875" Y="-2.551417236328" />
                  <Point X="-27.560154296875" Y="-2.537198974609" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.4489453125" Y="-3.038410644531" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-28.932630859375" Y="-3.148083496094" />
                  <Point X="-29.161697265625" Y="-2.847136962891" />
                  <Point X="-29.258634765625" Y="-2.684588134766" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-28.6003203125" Y="-1.758109008789" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.15253515625" Y="-1.41108203125" />
                  <Point X="-28.143240234375" Y="-1.386045288086" />
                  <Point X="-28.1381171875" Y="-1.366267578125" />
                  <Point X="-28.136650390625" Y="-1.350052124023" />
                  <Point X="-28.14865625" Y="-1.321068603516" />
                  <Point X="-28.170033203125" Y="-1.305415283203" />
                  <Point X="-28.187640625" Y="-1.295052490234" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-29.301740234375" Y="-1.43104699707" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.83780078125" Y="-1.361939697266" />
                  <Point X="-29.927392578125" Y="-1.011188293457" />
                  <Point X="-29.953037109375" Y="-0.831878295898" />
                  <Point X="-29.99839453125" Y="-0.5147421875" />
                  <Point X="-29.054740234375" Y="-0.261890563965" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.532595703125" Y="-0.114970451355" />
                  <Point X="-28.514142578125" Y="-0.102164085388" />
                  <Point X="-28.502322265625" Y="-0.09064591217" />
                  <Point X="-28.491796875" Y="-0.065917671204" />
                  <Point X="-28.485646484375" Y="-0.046100421906" />
                  <Point X="-28.483400390625" Y="-0.031280040741" />
                  <Point X="-28.48874609375" Y="-0.006470028877" />
                  <Point X="-28.494896484375" Y="0.013347373962" />
                  <Point X="-28.502322265625" Y="0.028084819794" />
                  <Point X="-28.523443359375" Y="0.046058151245" />
                  <Point X="-28.54189453125" Y="0.058864665985" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.543951171875" Y="0.330414093018" />
                  <Point X="-29.998185546875" Y="0.45212600708" />
                  <Point X="-29.975384765625" Y="0.606212890625" />
                  <Point X="-29.91764453125" Y="0.996414611816" />
                  <Point X="-29.86601953125" Y="1.186930786133" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.109724609375" Y="1.440909179688" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.7111171875" Y="1.402357421875" />
                  <Point X="-28.67027734375" Y="1.415234130859" />
                  <Point X="-28.651533203125" Y="1.426056152344" />
                  <Point X="-28.639119140625" Y="1.443786132813" />
                  <Point X="-28.630859375" Y="1.463728759766" />
                  <Point X="-28.61447265625" Y="1.503290405273" />
                  <Point X="-28.610712890625" Y="1.524605834961" />
                  <Point X="-28.61631640625" Y="1.545512451172" />
                  <Point X="-28.626283203125" Y="1.564659301758" />
                  <Point X="-28.646056640625" Y="1.602642211914" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-29.2258203125" Y="2.053415283203" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.384373046875" Y="2.402625244141" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-29.02326171875" Y="2.962781982422" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.378083984375" Y="3.053341796875" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.109841796875" Y="2.917926269531" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927404296875" />
                  <Point X="-27.992900390625" Y="2.947755371094" />
                  <Point X="-27.95252734375" Y="2.988127685547" />
                  <Point X="-27.9408984375" Y="3.006382324219" />
                  <Point X="-27.938072265625" Y="3.027841552734" />
                  <Point X="-27.940580078125" Y="3.056513183594" />
                  <Point X="-27.945556640625" Y="3.113391113281" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.2028125" Y="3.568337890625" />
                  <Point X="-28.30727734375" Y="3.749276855469" />
                  <Point X="-28.143638671875" Y="3.874737792969" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.537494140625" Y="4.293991699219" />
                  <Point X="-27.141548828125" Y="4.513971679688" />
                  <Point X="-27.03484765625" Y="4.374916015625" />
                  <Point X="-26.967826171875" Y="4.287572753906" />
                  <Point X="-26.95124609375" Y="4.273661132813" />
                  <Point X="-26.919333984375" Y="4.257049316406" />
                  <Point X="-26.856029296875" Y="4.224094238281" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.81380859375" Y="4.22225" />
                  <Point X="-26.780572265625" Y="4.236018066406" />
                  <Point X="-26.714634765625" Y="4.263329589844" />
                  <Point X="-26.696904296875" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.294488769531" />
                  <Point X="-26.675263671875" Y="4.328799804688" />
                  <Point X="-26.653802734375" Y="4.396865234375" />
                  <Point X="-26.651916015625" Y="4.418426269531" />
                  <Point X="-26.680423828125" Y="4.634958984375" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.473958984375" Y="4.761469238281" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.70698828125" Y="4.933854980469" />
                  <Point X="-25.22419921875" Y="4.990358398438" />
                  <Point X="-25.106904296875" Y="4.552609863281" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.81706640625" Y="4.790395507812" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.58148828125" Y="4.971822753906" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.9237734375" Y="4.873411621094" />
                  <Point X="-23.491544921875" Y="4.76905859375" />
                  <Point X="-23.35094921875" Y="4.718063476562" />
                  <Point X="-23.06896484375" Y="4.615785644531" />
                  <Point X="-22.93300390625" Y="4.552201171875" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.529951171875" Y="4.348610351562" />
                  <Point X="-22.26747265625" Y="4.195689453125" />
                  <Point X="-22.143599609375" Y="4.10759765625" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.481375" Y="3.003763671875" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491515625" />
                  <Point X="-22.782341796875" Y="2.464607910156" />
                  <Point X="-22.7966171875" Y="2.411229248047" />
                  <Point X="-22.797955078125" Y="2.392327148438" />
                  <Point X="-22.7951484375" Y="2.369059814453" />
                  <Point X="-22.789583984375" Y="2.32290234375" />
                  <Point X="-22.781318359375" Y="2.3008125" />
                  <Point X="-22.766919921875" Y="2.279594970703" />
                  <Point X="-22.738359375" Y="2.23750390625" />
                  <Point X="-22.72505859375" Y="2.224203369141" />
                  <Point X="-22.703841796875" Y="2.209806152344" />
                  <Point X="-22.66175" Y="2.181245849609" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.61639453125" Y="2.170174560547" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.5513359375" Y="2.165946289062" />
                  <Point X="-22.524427734375" Y="2.173141845703" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.466748046875" Y="2.765274414062" />
                  <Point X="-21.005751953125" Y="3.031430664062" />
                  <Point X="-20.953099609375" Y="2.958256103516" />
                  <Point X="-20.79740234375" Y="2.741874267578" />
                  <Point X="-20.72834765625" Y="2.627759521484" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.331189453125" Y="1.884815673828" />
                  <Point X="-21.7113828125" Y="1.593082641602" />
                  <Point X="-21.72062890625" Y="1.583832885742" />
                  <Point X="-21.739994140625" Y="1.558569213867" />
                  <Point X="-21.77841015625" Y="1.508451538086" />
                  <Point X="-21.78687890625" Y="1.491500244141" />
                  <Point X="-21.79409375" Y="1.465705932617" />
                  <Point X="-21.808404296875" Y="1.414535766602" />
                  <Point X="-21.809220703125" Y="1.39096472168" />
                  <Point X="-21.803298828125" Y="1.362265258789" />
                  <Point X="-21.79155078125" Y="1.30533190918" />
                  <Point X="-21.784353515625" Y="1.287955078125" />
                  <Point X="-21.768248046875" Y="1.263474243164" />
                  <Point X="-21.736296875" Y="1.214909667969" />
                  <Point X="-21.719052734375" Y="1.19882019043" />
                  <Point X="-21.6957109375" Y="1.185681518555" />
                  <Point X="-21.64941015625" Y="1.159617675781" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.599875" Y="1.149448852539" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.58165625" Y="1.265259521484" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.1276796875" Y="1.226061279297" />
                  <Point X="-20.060806640625" Y="0.951366943359" />
                  <Point X="-20.039046875" Y="0.811608337402" />
                  <Point X="-20.002140625" Y="0.57455645752" />
                  <Point X="-20.82312109375" Y="0.354575042725" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819351196" />
                  <Point X="-21.301916015625" Y="0.214898376465" />
                  <Point X="-21.363421875" Y="0.179346984863" />
                  <Point X="-21.377734375" Y="0.166926086426" />
                  <Point X="-21.396337890625" Y="0.143221954346" />
                  <Point X="-21.433240234375" Y="0.096198410034" />
                  <Point X="-21.443013671875" Y="0.074734802246" />
                  <Point X="-21.44921484375" Y="0.042356250763" />
                  <Point X="-21.461515625" Y="-0.021875602722" />
                  <Point X="-21.461515625" Y="-0.040684474945" />
                  <Point X="-21.455314453125" Y="-0.073063026428" />
                  <Point X="-21.443013671875" Y="-0.137294876099" />
                  <Point X="-21.433240234375" Y="-0.158759552002" />
                  <Point X="-21.414638671875" Y="-0.182463531494" />
                  <Point X="-21.377734375" Y="-0.229487228394" />
                  <Point X="-21.363421875" Y="-0.241907058716" />
                  <Point X="-21.33241796875" Y="-0.259828033447" />
                  <Point X="-21.270912109375" Y="-0.295379425049" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.39448046875" Y="-0.531989440918" />
                  <Point X="-20.001931640625" Y="-0.637172546387" />
                  <Point X="-20.01423046875" Y="-0.718754577637" />
                  <Point X="-20.051568359375" Y="-0.966412536621" />
                  <Point X="-20.079447265625" Y="-1.088579589844" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-21.082716796875" Y="-1.16415234375" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.666013671875" Y="-1.111567504883" />
                  <Point X="-21.7867265625" Y="-1.137805053711" />
                  <Point X="-21.8145546875" Y="-1.154697631836" />
                  <Point X="-21.8513359375" Y="-1.198932739258" />
                  <Point X="-21.924298828125" Y="-1.286685546875" />
                  <Point X="-21.935640625" Y="-1.314070800781" />
                  <Point X="-21.940912109375" Y="-1.371357421875" />
                  <Point X="-21.951369140625" Y="-1.485001220703" />
                  <Point X="-21.943638671875" Y="-1.516621826172" />
                  <Point X="-21.909962890625" Y="-1.569001953125" />
                  <Point X="-21.843158203125" Y="-1.672912353516" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-21.0294140625" Y="-2.301032226562" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.690619140625" Y="-2.631833740234" />
                  <Point X="-20.7958671875" Y="-2.802140625" />
                  <Point X="-20.853529296875" Y="-2.884069824219" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.796505859375" Y="-2.519045898438" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.335080078125" Y="-2.240233398438" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.5710859375" Y="-2.250909179688" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.334683105469" />
                  <Point X="-22.7430625" Y="-2.394847900391" />
                  <Point X="-22.80587890625" Y="-2.514201171875" />
                  <Point X="-22.8108359375" Y="-2.546374755859" />
                  <Point X="-22.797755859375" Y="-2.618796386719" />
                  <Point X="-22.77180859375" Y="-2.762465087891" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.2504609375" Y="-3.671355957031" />
                  <Point X="-22.01332421875" Y="-4.082087646484" />
                  <Point X="-22.03981640625" Y="-4.101009277344" />
                  <Point X="-22.16470703125" Y="-4.190216308594" />
                  <Point X="-22.22916796875" Y="-4.231940917969" />
                  <Point X="-22.32022265625" Y="-4.29087890625" />
                  <Point X="-22.973400390625" Y="-3.439642578125" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.400876953125" Y="-2.934545898438" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.6523125" Y="-2.842905517578" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.894986328125" Y="-2.918663574219" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045986083984" />
                  <Point X="-24.049578125" Y="-3.128964111328" />
                  <Point X="-24.085357421875" Y="-3.293573974609" />
                  <Point X="-24.0860703125" Y="-3.310719970703" />
                  <Point X="-23.93999609375" Y="-4.420256835938" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.8875" Y="-4.937348144531" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.0652109375" Y="-4.97406640625" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#150" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.06476507889" Y="4.59683105088" Z="0.85" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.85" />
                  <Point X="-0.718957857503" Y="5.014795969364" Z="0.85" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.85" />
                  <Point X="-1.493614076544" Y="4.840892876226" Z="0.85" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.85" />
                  <Point X="-1.736152903915" Y="4.659713024202" Z="0.85" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.85" />
                  <Point X="-1.729106894088" Y="4.375115128102" Z="0.85" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.85" />
                  <Point X="-1.805861344817" Y="4.313492202684" Z="0.85" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.85" />
                  <Point X="-1.90240392213" Y="4.332679244185" Z="0.85" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.85" />
                  <Point X="-2.001335805635" Y="4.436634312762" Z="0.85" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.85" />
                  <Point X="-2.567935144678" Y="4.368979430084" Z="0.85" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.85" />
                  <Point X="-3.180216675305" Y="3.945697858686" Z="0.85" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.85" />
                  <Point X="-3.252270888989" Y="3.574618046943" Z="0.85" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.85" />
                  <Point X="-2.996548468626" Y="3.083435051646" Z="0.85" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.85" />
                  <Point X="-3.034412322705" Y="3.014391245181" Z="0.85" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.85" />
                  <Point X="-3.111641413961" Y="2.999016230547" Z="0.85" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.85" />
                  <Point X="-3.359241301354" Y="3.127923059757" Z="0.85" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.85" />
                  <Point X="-4.068881907746" Y="3.024764295656" Z="0.85" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.85" />
                  <Point X="-4.433711762955" Y="2.459110406031" Z="0.85" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.85" />
                  <Point X="-4.262414396013" Y="2.045027664976" Z="0.85" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.85" />
                  <Point X="-3.676789617806" Y="1.572851166465" Z="0.85" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.85" />
                  <Point X="-3.68320941337" Y="1.514142709315" Z="0.85" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.85" />
                  <Point X="-3.732309357755" Y="1.48132349399" Z="0.85" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.85" />
                  <Point X="-4.109356951002" Y="1.521761514183" Z="0.85" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.85" />
                  <Point X="-4.920435814624" Y="1.231287993596" Z="0.85" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.85" />
                  <Point X="-5.031003053085" Y="0.644811818872" Z="0.85" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.85" />
                  <Point X="-4.563049190107" Y="0.313397820839" Z="0.85" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.85" />
                  <Point X="-3.558109115856" Y="0.03626255419" Z="0.85" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.85" />
                  <Point X="-3.542657213796" Y="0.009989670713" Z="0.85" />
                  <Point X="-3.539556741714" Y="0" Z="0.85" />
                  <Point X="-3.545707387339" Y="-0.019817280348" Z="0.85" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.85" />
                  <Point X="-3.567259479267" Y="-0.042613428943" Z="0.85" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.85" />
                  <Point X="-4.073838598666" Y="-0.182314235917" Z="0.85" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.85" />
                  <Point X="-5.008690923142" Y="-0.807677398767" Z="0.85" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.85" />
                  <Point X="-4.892402164627" Y="-1.343033623139" Z="0.85" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.85" />
                  <Point X="-4.301371817419" Y="-1.44933931572" Z="0.85" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.85" />
                  <Point X="-3.201551828604" Y="-1.317226056841" Z="0.85" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.85" />
                  <Point X="-3.197798673519" Y="-1.342227468917" Z="0.85" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.85" />
                  <Point X="-3.63691467826" Y="-1.687161380421" Z="0.85" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.85" />
                  <Point X="-4.307735456737" Y="-2.678917876593" Z="0.85" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.85" />
                  <Point X="-3.97857330768" Y="-3.147086560516" Z="0.85" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.85" />
                  <Point X="-3.430102257471" Y="-3.050431848384" Z="0.85" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.85" />
                  <Point X="-2.561305254696" Y="-2.567025214436" Z="0.85" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.85" />
                  <Point X="-2.804985164784" Y="-3.004976125021" Z="0.85" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.85" />
                  <Point X="-3.027701085864" Y="-4.071843033646" Z="0.85" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.85" />
                  <Point X="-2.598366605909" Y="-4.358368736094" Z="0.85" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.85" />
                  <Point X="-2.375745007594" Y="-4.351313927576" Z="0.85" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.85" />
                  <Point X="-2.05471227956" Y="-4.041852694068" Z="0.85" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.85" />
                  <Point X="-1.762424312759" Y="-3.997575303746" Z="0.85" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.85" />
                  <Point X="-1.503582350718" Y="-4.140381574765" Z="0.85" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.85" />
                  <Point X="-1.385164046266" Y="-4.411250175723" Z="0.85" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.85" />
                  <Point X="-1.38103943237" Y="-4.63598637878" Z="0.85" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.85" />
                  <Point X="-1.216503479886" Y="-4.93008558148" Z="0.85" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.85" />
                  <Point X="-0.918122552761" Y="-4.994264383855" Z="0.85" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.85" />
                  <Point X="-0.683414990336" Y="-4.512723562155" Z="0.85" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.85" />
                  <Point X="-0.30823171002" Y="-3.361933676173" Z="0.85" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.85" />
                  <Point X="-0.084912199497" Y="-3.23059298161" Z="0.85" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.85" />
                  <Point X="0.168446879864" Y="-3.256519063678" Z="0.85" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.85" />
                  <Point X="0.362214149412" Y="-3.439712079595" Z="0.85" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.85" />
                  <Point X="0.551339870065" Y="-4.01981246766" Z="0.85" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.85" />
                  <Point X="0.93756929113" Y="-4.991981276846" Z="0.85" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.85" />
                  <Point X="1.117047549347" Y="-4.954908449519" Z="0.85" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.85" />
                  <Point X="1.103419060024" Y="-4.382450251299" Z="0.85" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.85" />
                  <Point X="0.993124408871" Y="-3.108303953805" Z="0.85" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.85" />
                  <Point X="1.130822980883" Y="-2.925830182279" Z="0.85" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.85" />
                  <Point X="1.346112455361" Y="-2.861415078711" Z="0.85" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.85" />
                  <Point X="1.565926497834" Y="-2.945324051681" Z="0.85" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.85" />
                  <Point X="1.980775247744" Y="-3.438800596198" Z="0.85" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.85" />
                  <Point X="2.791844125008" Y="-4.242635228706" Z="0.85" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.85" />
                  <Point X="2.98308996308" Y="-4.110416290426" Z="0.85" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.85" />
                  <Point X="2.786682304073" Y="-3.615075882917" Z="0.85" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.85" />
                  <Point X="2.245290435516" Y="-2.57862995786" Z="0.85" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.85" />
                  <Point X="2.29502594067" Y="-2.386854872922" Z="0.85" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.85" />
                  <Point X="2.446043505451" Y="-2.263875369297" Z="0.85" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.85" />
                  <Point X="2.649876845653" Y="-2.258157573975" Z="0.85" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.85" />
                  <Point X="3.17233771419" Y="-2.531067196201" Z="0.85" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.85" />
                  <Point X="4.181203378329" Y="-2.881566972815" Z="0.85" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.85" />
                  <Point X="4.345757404207" Y="-2.626838861257" Z="0.85" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.85" />
                  <Point X="3.994866778036" Y="-2.230084468654" Z="0.85" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.85" />
                  <Point X="3.12593805044" Y="-1.510682593318" Z="0.85" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.85" />
                  <Point X="3.102719967264" Y="-1.344658770871" Z="0.85" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.85" />
                  <Point X="3.180955277946" Y="-1.199619406464" Z="0.85" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.85" />
                  <Point X="3.338449359837" Y="-1.129146532404" Z="0.85" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.85" />
                  <Point X="3.904601299162" Y="-1.18244461594" Z="0.85" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.85" />
                  <Point X="4.963141487137" Y="-1.068423666151" Z="0.85" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.85" />
                  <Point X="5.029053671714" Y="-0.69492855952" Z="0.85" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.85" />
                  <Point X="4.612304821916" Y="-0.452413079377" Z="0.85" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.85" />
                  <Point X="3.686447035985" Y="-0.185259260811" Z="0.85" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.85" />
                  <Point X="3.618539316485" Y="-0.120314605928" Z="0.85" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.85" />
                  <Point X="3.587635590974" Y="-0.032378601655" Z="0.85" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.85" />
                  <Point X="3.59373585945" Y="0.064231929569" Z="0.85" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.85" />
                  <Point X="3.636840121915" Y="0.143634132647" Z="0.85" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.85" />
                  <Point X="3.716948378367" Y="0.202889583431" Z="0.85" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.85" />
                  <Point X="4.183662995391" Y="0.337558853392" Z="0.85" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.85" />
                  <Point X="5.004199732881" Y="0.850580359781" Z="0.85" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.85" />
                  <Point X="4.914744911502" Y="1.26916795898" Z="0.85" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.85" />
                  <Point X="4.405661490894" Y="1.346111876508" Z="0.85" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.85" />
                  <Point X="3.400518202178" Y="1.230297860237" Z="0.85" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.85" />
                  <Point X="3.322658789508" Y="1.260532498886" Z="0.85" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.85" />
                  <Point X="3.267367277928" Y="1.322235563184" Z="0.85" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.85" />
                  <Point X="3.239513709731" Y="1.403649742164" Z="0.85" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.85" />
                  <Point X="3.247902326301" Y="1.483519384148" Z="0.85" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.85" />
                  <Point X="3.293532713641" Y="1.559431351253" Z="0.85" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.85" />
                  <Point X="3.693092009895" Y="1.876428093179" Z="0.85" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.85" />
                  <Point X="4.308272287284" Y="2.684925720953" Z="0.85" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.85" />
                  <Point X="4.081329672193" Y="3.01873929263" Z="0.85" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.85" />
                  <Point X="3.502095373941" Y="2.839855749216" Z="0.85" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.85" />
                  <Point X="2.456499249959" Y="2.252724616044" Z="0.85" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.85" />
                  <Point X="2.383434232177" Y="2.251094905158" Z="0.85" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.85" />
                  <Point X="2.318075714535" Y="2.28246103712" Z="0.85" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.85" />
                  <Point X="2.268297563115" Y="2.338949145847" Z="0.85" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.85" />
                  <Point X="2.248334797601" Y="2.406324211655" Z="0.85" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.85" />
                  <Point X="2.259803326613" Y="2.482970325923" Z="0.85" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.85" />
                  <Point X="2.555769603643" Y="3.010043708014" Z="0.85" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.85" />
                  <Point X="2.879220615519" Y="4.179624908539" Z="0.85" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.85" />
                  <Point X="2.48906192472" Y="4.423092977446" Z="0.85" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.85" />
                  <Point X="2.082021263578" Y="4.628773126515" Z="0.85" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.85" />
                  <Point X="1.659943201527" Y="4.79634727604" Z="0.85" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.85" />
                  <Point X="1.08180389729" Y="4.953295411807" Z="0.85" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.85" />
                  <Point X="0.417562287376" Y="5.052830975473" Z="0.85" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.85" />
                  <Point X="0.128479482663" Y="4.834616455071" Z="0.85" />
                  <Point X="0" Y="4.355124473572" Z="0.85" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>