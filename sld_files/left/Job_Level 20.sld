<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#155" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1543" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004715820312" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.286671875" Y="-4.072421630859" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497140625" />
                  <Point X="-24.457634765625" Y="-3.467377197266" />
                  <Point X="-24.518953125" Y="-3.379028564453" />
                  <Point X="-24.62136328125" Y="-3.231476806641" />
                  <Point X="-24.643248046875" Y="-3.209020263672" />
                  <Point X="-24.66950390625" Y="-3.189776611328" />
                  <Point X="-24.69750390625" Y="-3.175669433594" />
                  <Point X="-24.792390625" Y="-3.146219970703" />
                  <Point X="-24.95086328125" Y="-3.097036132812" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097035888672" />
                  <Point X="-25.1317109375" Y="-3.126485107422" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.31818359375" Y="-3.18977734375" />
                  <Point X="-25.344439453125" Y="-3.209021484375" />
                  <Point X="-25.36632421875" Y="-3.231477050781" />
                  <Point X="-25.427642578125" Y="-3.319825439453" />
                  <Point X="-25.53005078125" Y="-3.467377441406" />
                  <Point X="-25.5381875" Y="-3.481571289062" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.801546875" Y="-4.447616699219" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.185857421875" Y="-4.817943847656" />
                  <Point X="-26.246416015625" Y="-4.802362304688" />
                  <Point X="-26.233021484375" Y="-4.700612792969" />
                  <Point X="-26.2149609375" Y="-4.563438476562" />
                  <Point X="-26.21419921875" Y="-4.5479296875" />
                  <Point X="-26.2165078125" Y="-4.516223144531" />
                  <Point X="-26.239177734375" Y="-4.402261230469" />
                  <Point X="-26.277037109375" Y="-4.211931640625" />
                  <Point X="-26.287939453125" Y="-4.182965332031" />
                  <Point X="-26.30401171875" Y="-4.155127929687" />
                  <Point X="-26.32364453125" Y="-4.131204589844" />
                  <Point X="-26.41100390625" Y="-4.054591552734" />
                  <Point X="-26.556904296875" Y="-3.926639648438" />
                  <Point X="-26.5831875" Y="-3.910295898438" />
                  <Point X="-26.612884765625" Y="-3.897994628906" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.75897265625" Y="-3.883366699219" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708496094" />
                  <Point X="-27.014466796875" Y="-3.882028076172" />
                  <Point X="-27.042658203125" Y="-3.894801757812" />
                  <Point X="-27.13926953125" Y="-3.959356201172" />
                  <Point X="-27.300623046875" Y="-4.067169921875" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.3351015625" Y="-4.099461914062" />
                  <Point X="-27.475771484375" Y="-4.282784667969" />
                  <Point X="-27.4801484375" Y="-4.288488769531" />
                  <Point X="-27.533173828125" Y="-4.255656738281" />
                  <Point X="-27.80171484375" Y="-4.089382568359" />
                  <Point X="-27.949203125" Y="-3.975821533203" />
                  <Point X="-28.10472265625" Y="-3.856077148438" />
                  <Point X="-27.702564453125" Y="-3.159520019531" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647653808594" />
                  <Point X="-27.406587890625" Y="-2.616125976562" />
                  <Point X="-27.405576171875" Y="-2.585190185547" />
                  <Point X="-27.414560546875" Y="-2.555570556641" />
                  <Point X="-27.428779296875" Y="-2.526740966797" />
                  <Point X="-27.4468046875" Y="-2.501587158203" />
                  <Point X="-27.4641484375" Y="-2.484243408203" />
                  <Point X="-27.489302734375" Y="-2.466218017578" />
                  <Point X="-27.5181328125" Y="-2.451998535156" />
                  <Point X="-27.54775" Y="-2.443012207031" />
                  <Point X="-27.5786875" Y="-2.444024169922" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.445681640625" Y="-2.926829589844" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-28.870703125" Y="-3.072591064453" />
                  <Point X="-29.082859375" Y="-2.793861572266" />
                  <Point X="-29.188599609375" Y="-2.616550537109" />
                  <Point X="-29.306142578125" Y="-2.419449951172" />
                  <Point X="-28.593767578125" Y="-1.872826293945" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.08458203125" Y="-1.475600097656" />
                  <Point X="-28.0666171875" Y="-1.448473876953" />
                  <Point X="-28.053857421875" Y="-1.41983984375" />
                  <Point X="-28.046150390625" Y="-1.390085693359" />
                  <Point X="-28.043345703125" Y="-1.3596484375" />
                  <Point X="-28.045556640625" Y="-1.327978759766" />
                  <Point X="-28.05255859375" Y="-1.298236816406" />
                  <Point X="-28.068640625" Y="-1.272255981445" />
                  <Point X="-28.08947265625" Y="-1.248300415039" />
                  <Point X="-28.112970703125" Y="-1.228767211914" />
                  <Point X="-28.139453125" Y="-1.213180664062" />
                  <Point X="-28.168716796875" Y="-1.201956665039" />
                  <Point X="-28.200603515625" Y="-1.195474975586" />
                  <Point X="-28.231927734375" Y="-1.194383911133" />
                  <Point X="-29.250056640625" Y="-1.328422973633" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.751099609375" Y="-1.317503540039" />
                  <Point X="-29.834078125" Y="-0.992650085449" />
                  <Point X="-29.862052734375" Y="-0.797049072266" />
                  <Point X="-29.892423828125" Y="-0.584698364258" />
                  <Point X="-29.088568359375" Y="-0.369305969238" />
                  <Point X="-28.532875" Y="-0.220408279419" />
                  <Point X="-28.511298828125" Y="-0.211675018311" />
                  <Point X="-28.485337890625" Y="-0.197241119385" />
                  <Point X="-28.4773359375" Y="-0.192256011963" />
                  <Point X="-28.4599765625" Y="-0.180207809448" />
                  <Point X="-28.43601953125" Y="-0.158681350708" />
                  <Point X="-28.414748046875" Y="-0.127464607239" />
                  <Point X="-28.403857421875" Y="-0.101562187195" />
                  <Point X="-28.3990546875" Y="-0.086913360596" />
                  <Point X="-28.3910234375" Y="-0.053451629639" />
                  <Point X="-28.388404296875" Y="-0.032191421509" />
                  <Point X="-28.390615234375" Y="-0.010884907722" />
                  <Point X="-28.396326171875" Y="0.015096289635" />
                  <Point X="-28.40077734375" Y="0.029658296585" />
                  <Point X="-28.41398828125" Y="0.063041549683" />
                  <Point X="-28.42519921875" Y="0.083555023193" />
                  <Point X="-28.441080078125" Y="0.100709541321" />
                  <Point X="-28.463294921875" Y="0.119442443848" />
                  <Point X="-28.47037109375" Y="0.124861816406" />
                  <Point X="-28.48773046875" Y="0.136910018921" />
                  <Point X="-28.501923828125" Y="0.145046447754" />
                  <Point X="-28.532875" Y="0.157848297119" />
                  <Point X="-29.460947265625" Y="0.406524627686" />
                  <Point X="-29.89181640625" Y="0.521975646973" />
                  <Point X="-29.87796484375" Y="0.615586547852" />
                  <Point X="-29.82448828125" Y="0.976966552734" />
                  <Point X="-29.768169921875" Y="1.184797241211" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-29.164" Y="1.352234863281" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228149414" />
                  <Point X="-28.703134765625" Y="1.305263793945" />
                  <Point X="-28.680130859375" Y="1.312517211914" />
                  <Point X="-28.641708984375" Y="1.324631347656" />
                  <Point X="-28.62277734375" Y="1.332962158203" />
                  <Point X="-28.604033203125" Y="1.343784179688" />
                  <Point X="-28.5873515625" Y="1.356016113281" />
                  <Point X="-28.573712890625" Y="1.371568847656" />
                  <Point X="-28.561298828125" Y="1.389299072266" />
                  <Point X="-28.551349609375" Y="1.407432861328" />
                  <Point X="-28.542119140625" Y="1.429718017578" />
                  <Point X="-28.526703125" Y="1.466937011719" />
                  <Point X="-28.520916015625" Y="1.4867890625" />
                  <Point X="-28.51715625" Y="1.508104858398" />
                  <Point X="-28.515802734375" Y="1.528750976562" />
                  <Point X="-28.518951171875" Y="1.549200561523" />
                  <Point X="-28.5245546875" Y="1.570107666016" />
                  <Point X="-28.532048828125" Y="1.589375366211" />
                  <Point X="-28.543185546875" Y="1.610771362305" />
                  <Point X="-28.5617890625" Y="1.646505004883" />
                  <Point X="-28.57328515625" Y="1.66371081543" />
                  <Point X="-28.587197265625" Y="1.680289306641" />
                  <Point X="-28.60213671875" Y="1.694590332031" />
                  <Point X="-29.13448046875" Y="2.103072509766" />
                  <Point X="-29.351859375" Y="2.269873291016" />
                  <Point X="-29.28894140625" Y="2.377667236328" />
                  <Point X="-29.0811484375" Y="2.733665039062" />
                  <Point X="-28.9319765625" Y="2.925404541016" />
                  <Point X="-28.75050390625" Y="3.158662109375" />
                  <Point X="-28.451302734375" Y="2.985917724609" />
                  <Point X="-28.20665625" Y="2.844671142578" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.146794921875" Y="2.825796386719" />
                  <Point X="-28.114755859375" Y="2.822993164062" />
                  <Point X="-28.06124609375" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.999013671875" Y="2.826504638672" />
                  <Point X="-27.980462890625" Y="2.835653076172" />
                  <Point X="-27.962208984375" Y="2.847281982422" />
                  <Point X="-27.946078125" Y="2.860228759766" />
                  <Point X="-27.9233359375" Y="2.882970703125" />
                  <Point X="-27.885353515625" Y="2.920952148438" />
                  <Point X="-27.872404296875" Y="2.937085449219" />
                  <Point X="-27.860775390625" Y="2.95533984375" />
                  <Point X="-27.85162890625" Y="2.973888916016" />
                  <Point X="-27.8467109375" Y="2.993977783203" />
                  <Point X="-27.843884765625" Y="3.015437011719" />
                  <Point X="-27.84343359375" Y="3.036119873047" />
                  <Point X="-27.846236328125" Y="3.068159667969" />
                  <Point X="-27.85091796875" Y="3.121669433594" />
                  <Point X="-27.854955078125" Y="3.141963378906" />
                  <Point X="-27.86146484375" Y="3.16260546875" />
                  <Point X="-27.869794921875" Y="3.181532958984" />
                  <Point X="-28.105693359375" Y="3.590120117188" />
                  <Point X="-28.18333203125" Y="3.724595703125" />
                  <Point X="-28.0625234375" Y="3.817219726562" />
                  <Point X="-27.70062109375" Y="4.094686035156" />
                  <Point X="-27.4656796875" Y="4.225214355469" />
                  <Point X="-27.167037109375" Y="4.391133789062" />
                  <Point X="-27.11808984375" Y="4.32734375" />
                  <Point X="-27.0431953125" Y="4.229740234375" />
                  <Point X="-27.028890625" Y="4.214797851563" />
                  <Point X="-27.012310546875" Y="4.200885742188" />
                  <Point X="-26.995111328125" Y="4.18939453125" />
                  <Point X="-26.959451171875" Y="4.170831054688" />
                  <Point X="-26.89989453125" Y="4.139827636719" />
                  <Point X="-26.8806171875" Y="4.132331054688" />
                  <Point X="-26.8597109375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.12358203125" />
                  <Point X="-26.818626953125" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.777451171875" Y="4.134482421875" />
                  <Point X="-26.740310546875" Y="4.149867675781" />
                  <Point X="-26.67827734375" Y="4.175562011719" />
                  <Point X="-26.660146484375" Y="4.185509277344" />
                  <Point X="-26.642416015625" Y="4.197923828125" />
                  <Point X="-26.626865234375" Y="4.211561523438" />
                  <Point X="-26.6146328125" Y="4.2282421875" />
                  <Point X="-26.603810546875" Y="4.246985839844" />
                  <Point X="-26.595478515625" Y="4.265923339844" />
                  <Point X="-26.583390625" Y="4.304265136719" />
                  <Point X="-26.56319921875" Y="4.368299804688" />
                  <Point X="-26.5591640625" Y="4.388583007812" />
                  <Point X="-26.55727734375" Y="4.410145019531" />
                  <Point X="-26.557728515625" Y="4.430826660156" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-26.4181328125" Y="4.678458007812" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.664814453125" Y="4.843142089844" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.20627734375" Y="4.556416015625" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286315429688" />
                  <Point X="-24.73291015625" Y="4.7374140625" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.56503125" Y="4.874580078125" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.9203125" Y="4.774847167969" />
                  <Point X="-23.5189765625" Y="4.677952148438" />
                  <Point X="-23.36651953125" Y="4.622654296875" />
                  <Point X="-23.105349609375" Y="4.527926269531" />
                  <Point X="-22.957037109375" Y="4.458565917969" />
                  <Point X="-22.705423828125" Y="4.340893554688" />
                  <Point X="-22.562115234375" Y="4.25740234375" />
                  <Point X="-22.31902734375" Y="4.115777832031" />
                  <Point X="-22.18388671875" Y="4.019675292969" />
                  <Point X="-22.05673828125" Y="3.92925390625" />
                  <Point X="-22.52972265625" Y="3.110018798828" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857919921875" Y="2.539935546875" />
                  <Point X="-22.866921875" Y="2.516058105469" />
                  <Point X="-22.874962890625" Y="2.485989501953" />
                  <Point X="-22.888392578125" Y="2.435771728516" />
                  <Point X="-22.891380859375" Y="2.417936767578" />
                  <Point X="-22.892271484375" Y="2.380949707031" />
                  <Point X="-22.889134765625" Y="2.354948974609" />
                  <Point X="-22.883900390625" Y="2.311524902344" />
                  <Point X="-22.87855859375" Y="2.289611083984" />
                  <Point X="-22.87029296875" Y="2.267520507813" />
                  <Point X="-22.859927734375" Y="2.247467773438" />
                  <Point X="-22.843837890625" Y="2.2237578125" />
                  <Point X="-22.81696875" Y="2.184159179688" />
                  <Point X="-22.805533203125" Y="2.170328125" />
                  <Point X="-22.77840234375" Y="2.145593505859" />
                  <Point X="-22.754693359375" Y="2.129505126953" />
                  <Point X="-22.71509375" Y="2.102635986328" />
                  <Point X="-22.695044921875" Y="2.092271728516" />
                  <Point X="-22.67295703125" Y="2.084006103516" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.62503515625" Y="2.075528320312" />
                  <Point X="-22.581611328125" Y="2.070291992188" />
                  <Point X="-22.563533203125" Y="2.069845703125" />
                  <Point X="-22.52679296875" Y="2.074171142578" />
                  <Point X="-22.496724609375" Y="2.082211914062" />
                  <Point X="-22.446505859375" Y="2.095640869141" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.47800390625" Y="2.649079589844" />
                  <Point X="-21.032671875" Y="2.90619140625" />
                  <Point X="-21.020921875" Y="2.889861083984" />
                  <Point X="-20.876724609375" Y="2.6894609375" />
                  <Point X="-20.801392578125" Y="2.564970703125" />
                  <Point X="-20.73780078125" Y="2.459883789063" />
                  <Point X="-21.344359375" Y="1.994454345703" />
                  <Point X="-21.76921484375" Y="1.668451171875" />
                  <Point X="-21.778572265625" Y="1.660243164062" />
                  <Point X="-21.796025390625" Y="1.641627685547" />
                  <Point X="-21.817666015625" Y="1.613396240234" />
                  <Point X="-21.853806640625" Y="1.566246337891" />
                  <Point X="-21.86339453125" Y="1.550909057617" />
                  <Point X="-21.8783671875" Y="1.517090209961" />
                  <Point X="-21.8864296875" Y="1.48826574707" />
                  <Point X="-21.899892578125" Y="1.440125610352" />
                  <Point X="-21.90334765625" Y="1.41782409668" />
                  <Point X="-21.9041640625" Y="1.394254882812" />
                  <Point X="-21.902259765625" Y="1.371763549805" />
                  <Point X="-21.895640625" Y="1.339692749023" />
                  <Point X="-21.88458984375" Y="1.286130859375" />
                  <Point X="-21.8793203125" Y="1.268982421875" />
                  <Point X="-21.86371875" Y="1.235741577148" />
                  <Point X="-21.845720703125" Y="1.208384765625" />
                  <Point X="-21.815662109375" Y="1.162696044922" />
                  <Point X="-21.801107421875" Y="1.145451049805" />
                  <Point X="-21.78386328125" Y="1.129360717773" />
                  <Point X="-21.765654296875" Y="1.116034912109" />
                  <Point X="-21.739572265625" Y="1.101352905273" />
                  <Point X="-21.69601171875" Y="1.076832641602" />
                  <Point X="-21.6794765625" Y="1.069500976563" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.608615234375" Y="1.054777832031" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.6250703125" Y="1.163724487305" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.21599609375" Y="1.187204589844" />
                  <Point X="-20.15405859375" Y="0.932787536621" />
                  <Point X="-20.130322265625" Y="0.780329345703" />
                  <Point X="-20.109134765625" Y="0.644238708496" />
                  <Point X="-20.796525390625" Y="0.460052581787" />
                  <Point X="-21.283419921875" Y="0.329589813232" />
                  <Point X="-21.2952109375" Y="0.325585754395" />
                  <Point X="-21.318453125" Y="0.315068023682" />
                  <Point X="-21.353099609375" Y="0.295041717529" />
                  <Point X="-21.410962890625" Y="0.261595489502" />
                  <Point X="-21.425685546875" Y="0.251098419189" />
                  <Point X="-21.45246875" Y="0.225575775146" />
                  <Point X="-21.473255859375" Y="0.199087158203" />
                  <Point X="-21.507974609375" Y="0.15484815979" />
                  <Point X="-21.51969921875" Y="0.135566833496" />
                  <Point X="-21.52947265625" Y="0.114102310181" />
                  <Point X="-21.536318359375" Y="0.092604545593" />
                  <Point X="-21.543248046875" Y="0.056422428131" />
                  <Point X="-21.5548203125" Y="-0.00400592041" />
                  <Point X="-21.556515625" Y="-0.021875743866" />
                  <Point X="-21.5548203125" Y="-0.05855406189" />
                  <Point X="-21.547890625" Y="-0.094736328125" />
                  <Point X="-21.536318359375" Y="-0.155164672852" />
                  <Point X="-21.52947265625" Y="-0.17666229248" />
                  <Point X="-21.51969921875" Y="-0.198126815796" />
                  <Point X="-21.507974609375" Y="-0.21740814209" />
                  <Point X="-21.4871875" Y="-0.243896774292" />
                  <Point X="-21.45246875" Y="-0.288135772705" />
                  <Point X="-21.439998046875" Y="-0.301238616943" />
                  <Point X="-21.410962890625" Y="-0.324155609131" />
                  <Point X="-21.37631640625" Y="-0.344181762695" />
                  <Point X="-21.318453125" Y="-0.37762802124" />
                  <Point X="-21.307291015625" Y="-0.383138305664" />
                  <Point X="-21.283419921875" Y="-0.392149810791" />
                  <Point X="-20.470251953125" Y="-0.610037719727" />
                  <Point X="-20.108525390625" Y="-0.706961791992" />
                  <Point X="-20.110396484375" Y="-0.719366088867" />
                  <Point X="-20.144974609375" Y="-0.94872467041" />
                  <Point X="-20.175390625" Y="-1.082010253906" />
                  <Point X="-20.19882421875" Y="-1.18469909668" />
                  <Point X="-21.01095703125" Y="-1.077779663086" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710266113" />
                  <Point X="-21.62533984375" Y="-1.005508666992" />
                  <Point X="-21.693337890625" Y="-1.020288513184" />
                  <Point X="-21.806904296875" Y="-1.04497253418" />
                  <Point X="-21.8360234375" Y="-1.056596069336" />
                  <Point X="-21.8638515625" Y="-1.07348840332" />
                  <Point X="-21.8876015625" Y="-1.093959716797" />
                  <Point X="-21.928703125" Y="-1.143391357422" />
                  <Point X="-21.997345703125" Y="-1.225947753906" />
                  <Point X="-22.012068359375" Y="-1.250334838867" />
                  <Point X="-22.02341015625" Y="-1.277720214844" />
                  <Point X="-22.030240234375" Y="-1.305365844727" />
                  <Point X="-22.036130859375" Y="-1.369381958008" />
                  <Point X="-22.04596875" Y="-1.476296142578" />
                  <Point X="-22.04365234375" Y="-1.507561767578" />
                  <Point X="-22.035921875" Y="-1.539182739258" />
                  <Point X="-22.023548828125" Y="-1.567996337891" />
                  <Point X="-21.98591796875" Y="-1.626529663086" />
                  <Point X="-21.923068359375" Y="-1.724286987305" />
                  <Point X="-21.9130625" Y="-1.737243286133" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.134744140625" Y="-2.339953857422" />
                  <Point X="-20.786876953125" Y="-2.606882568359" />
                  <Point X="-20.875197265625" Y="-2.749798095703" />
                  <Point X="-20.93808984375" Y="-2.839160644531" />
                  <Point X="-20.971017578125" Y="-2.885945556641" />
                  <Point X="-21.696138671875" Y="-2.467296142578" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.245775390625" Y="-2.159825195312" />
                  <Point X="-22.326705078125" Y="-2.145209472656" />
                  <Point X="-22.461865234375" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135176757812" />
                  <Point X="-22.6223984375" Y="-2.170560546875" />
                  <Point X="-22.73468359375" Y="-2.229655761719" />
                  <Point X="-22.757615234375" Y="-2.246549804688" />
                  <Point X="-22.77857421875" Y="-2.267509521484" />
                  <Point X="-22.795466796875" Y="-2.290438476562" />
                  <Point X="-22.8308515625" Y="-2.357670898438" />
                  <Point X="-22.889947265625" Y="-2.469956542969" />
                  <Point X="-22.899771484375" Y="-2.499735351563" />
                  <Point X="-22.904728515625" Y="-2.531908203125" />
                  <Point X="-22.90432421875" Y="-2.563260009766" />
                  <Point X="-22.88970703125" Y="-2.644189208984" />
                  <Point X="-22.865296875" Y="-2.779350341797" />
                  <Point X="-22.86101171875" Y="-2.795139648438" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.363255859375" Y="-3.665989257812" />
                  <Point X="-22.138712890625" Y="-4.054906494141" />
                  <Point X="-22.218126953125" Y="-4.111629394531" />
                  <Point X="-22.2884765625" Y="-4.157164550781" />
                  <Point X="-22.298234375" Y="-4.163480957031" />
                  <Point X="-22.857689453125" Y="-3.434385498047" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.357892578125" Y="-2.849241455078" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.6701953125" Y="-2.749149658203" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-23.96280859375" Y="-2.851507568359" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968861083984" />
                  <Point X="-24.11275" Y="-2.996687744141" />
                  <Point X="-24.124375" Y="-3.02580859375" />
                  <Point X="-24.144529296875" Y="-3.118534179688" />
                  <Point X="-24.178189453125" Y="-3.273396484375" />
                  <Point X="-24.180275390625" Y="-3.289627441406" />
                  <Point X="-24.1802578125" Y="-3.323120361328" />
                  <Point X="-24.042833984375" Y="-4.366954101563" />
                  <Point X="-23.977935546875" Y="-4.859915527344" />
                  <Point X="-24.024310546875" Y="-4.870081054688" />
                  <Point X="-24.07068359375" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.05842578125" Y="-4.75263671875" />
                  <Point X="-26.14124609375" Y="-4.731328125" />
                  <Point X="-26.138833984375" Y="-4.71301171875" />
                  <Point X="-26.1207734375" Y="-4.575837402344" />
                  <Point X="-26.120076171875" Y="-4.568098632812" />
                  <Point X="-26.11944921875" Y="-4.541030761719" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497688476562" />
                  <Point X="-26.14600390625" Y="-4.3837265625" />
                  <Point X="-26.18386328125" Y="-4.193396972656" />
                  <Point X="-26.188126953125" Y="-4.178467285156" />
                  <Point X="-26.199029296875" Y="-4.149500976562" />
                  <Point X="-26.20566796875" Y="-4.135464355469" />
                  <Point X="-26.221740234375" Y="-4.107626953125" />
                  <Point X="-26.23057421875" Y="-4.094861572266" />
                  <Point X="-26.25020703125" Y="-4.070938232422" />
                  <Point X="-26.261005859375" Y="-4.059780273438" />
                  <Point X="-26.348365234375" Y="-3.983167236328" />
                  <Point X="-26.494265625" Y="-3.855215332031" />
                  <Point X="-26.50673828125" Y="-3.845965087891" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822527587891" />
                  <Point X="-26.576529296875" Y="-3.810226318359" />
                  <Point X="-26.5913125" Y="-3.805476318359" />
                  <Point X="-26.621455078125" Y="-3.798447998047" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.752759765625" Y="-3.788570068359" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166015625" />
                  <Point X="-27.008005859375" Y="-3.781945556641" />
                  <Point X="-27.0390546875" Y="-3.790265136719" />
                  <Point X="-27.053673828125" Y="-3.795496337891" />
                  <Point X="-27.081865234375" Y="-3.808270019531" />
                  <Point X="-27.0954375" Y="-3.8158125" />
                  <Point X="-27.192048828125" Y="-3.880366943359" />
                  <Point X="-27.35340234375" Y="-3.988180664062" />
                  <Point X="-27.35968359375" Y="-3.992759277344" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032770507812" />
                  <Point X="-27.410470703125" Y="-4.041629150391" />
                  <Point X="-27.503201171875" Y="-4.162478027344" />
                  <Point X="-27.74759375" Y="-4.011156494141" />
                  <Point X="-27.89124609375" Y="-3.900549072266" />
                  <Point X="-27.98086328125" Y="-3.831546386719" />
                  <Point X="-27.62029296875" Y="-3.207020019531" />
                  <Point X="-27.341490234375" Y="-2.724119628906" />
                  <Point X="-27.3348515625" Y="-2.710084472656" />
                  <Point X="-27.32394921875" Y="-2.681118652344" />
                  <Point X="-27.319685546875" Y="-2.666187988281" />
                  <Point X="-27.3134140625" Y="-2.63466015625" />
                  <Point X="-27.311638671875" Y="-2.619231201172" />
                  <Point X="-27.310626953125" Y="-2.588295410156" />
                  <Point X="-27.314666015625" Y="-2.557614990234" />
                  <Point X="-27.323650390625" Y="-2.527995361328" />
                  <Point X="-27.329359375" Y="-2.513549316406" />
                  <Point X="-27.343578125" Y="-2.484719726562" />
                  <Point X="-27.35155859375" Y="-2.471404785156" />
                  <Point X="-27.369583984375" Y="-2.446250976562" />
                  <Point X="-27.37962890625" Y="-2.434412109375" />
                  <Point X="-27.39697265625" Y="-2.417068359375" />
                  <Point X="-27.4088125" Y="-2.407023193359" />
                  <Point X="-27.433966796875" Y="-2.388997802734" />
                  <Point X="-27.44728125" Y="-2.381017578125" />
                  <Point X="-27.476111328125" Y="-2.366798095703" />
                  <Point X="-27.49055078125" Y="-2.361091064453" />
                  <Point X="-27.52016796875" Y="-2.352104736328" />
                  <Point X="-27.55085546875" Y="-2.348062988281" />
                  <Point X="-27.58179296875" Y="-2.349074951172" />
                  <Point X="-27.597220703125" Y="-2.350849365234" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.493181640625" Y="-2.844557128906" />
                  <Point X="-28.793087890625" Y="-3.017708251953" />
                  <Point X="-28.795109375" Y="-3.015052734375" />
                  <Point X="-29.004021484375" Y="-2.740585693359" />
                  <Point X="-29.1070078125" Y="-2.567892333984" />
                  <Point X="-29.181265625" Y="-2.443373535156" />
                  <Point X="-28.535935546875" Y="-1.948194824219" />
                  <Point X="-28.048123046875" Y="-1.573882080078" />
                  <Point X="-28.036486328125" Y="-1.563313110352" />
                  <Point X="-28.01511328125" Y="-1.540399658203" />
                  <Point X="-28.005376953125" Y="-1.528055297852" />
                  <Point X="-27.987412109375" Y="-1.500928833008" />
                  <Point X="-27.97984375" Y="-1.487141845703" />
                  <Point X="-27.967083984375" Y="-1.45850793457" />
                  <Point X="-27.961892578125" Y="-1.443661010742" />
                  <Point X="-27.954185546875" Y="-1.413906860352" />
                  <Point X="-27.95155078125" Y="-1.398802734375" />
                  <Point X="-27.94874609375" Y="-1.368365600586" />
                  <Point X="-27.948576171875" Y="-1.353032470703" />
                  <Point X="-27.950787109375" Y="-1.321362670898" />
                  <Point X="-27.953083984375" Y="-1.306208740234" />
                  <Point X="-27.9600859375" Y="-1.276466796875" />
                  <Point X="-27.97178125" Y="-1.248236206055" />
                  <Point X="-27.98786328125" Y="-1.222255371094" />
                  <Point X="-27.996955078125" Y="-1.209917114258" />
                  <Point X="-28.017787109375" Y="-1.185961547852" />
                  <Point X="-28.028744140625" Y="-1.175245239258" />
                  <Point X="-28.0522421875" Y="-1.155712036133" />
                  <Point X="-28.064783203125" Y="-1.146895141602" />
                  <Point X="-28.091265625" Y="-1.13130859375" />
                  <Point X="-28.10543359375" Y="-1.124481079102" />
                  <Point X="-28.134697265625" Y="-1.113257080078" />
                  <Point X="-28.14979296875" Y="-1.108860595703" />
                  <Point X="-28.1816796875" Y="-1.10237890625" />
                  <Point X="-28.197296875" Y="-1.100532592773" />
                  <Point X="-28.22862109375" Y="-1.09944152832" />
                  <Point X="-28.244328125" Y="-1.100196777344" />
                  <Point X="-29.26245703125" Y="-1.234235717773" />
                  <Point X="-29.66091796875" Y="-1.286694335938" />
                  <Point X="-29.740763671875" Y="-0.974111572266" />
                  <Point X="-29.768009765625" Y="-0.783599121094" />
                  <Point X="-29.786451171875" Y="-0.654654541016" />
                  <Point X="-29.06398046875" Y="-0.461068939209" />
                  <Point X="-28.508287109375" Y="-0.312171203613" />
                  <Point X="-28.497232421875" Y="-0.308468109131" />
                  <Point X="-28.47565625" Y="-0.299734863281" />
                  <Point X="-28.465134765625" Y="-0.294704742432" />
                  <Point X="-28.439173828125" Y="-0.280270935059" />
                  <Point X="-28.423169921875" Y="-0.270300689697" />
                  <Point X="-28.405810546875" Y="-0.258252593994" />
                  <Point X="-28.396482421875" Y="-0.250871810913" />
                  <Point X="-28.372525390625" Y="-0.229345428467" />
                  <Point X="-28.357513671875" Y="-0.212176651001" />
                  <Point X="-28.3362421875" Y="-0.180959884644" />
                  <Point X="-28.327173828125" Y="-0.164284988403" />
                  <Point X="-28.316283203125" Y="-0.13838269043" />
                  <Point X="-28.3135859375" Y="-0.131158569336" />
                  <Point X="-28.306677734375" Y="-0.109084945679" />
                  <Point X="-28.298646484375" Y="-0.07562323761" />
                  <Point X="-28.296736328125" Y="-0.06506729126" />
                  <Point X="-28.2941171875" Y="-0.04380708313" />
                  <Point X="-28.293912109375" Y="-0.022386058807" />
                  <Point X="-28.296123046875" Y="-0.001079624295" />
                  <Point X="-28.297830078125" Y="0.009510205269" />
                  <Point X="-28.303541015625" Y="0.035491424561" />
                  <Point X="-28.3054765625" Y="0.042866542816" />
                  <Point X="-28.312443359375" Y="0.06461542511" />
                  <Point X="-28.325654296875" Y="0.097998664856" />
                  <Point X="-28.330625" Y="0.10860067749" />
                  <Point X="-28.3418359375" Y="0.129114212036" />
                  <Point X="-28.355486328125" Y="0.148092315674" />
                  <Point X="-28.3713671875" Y="0.165246826172" />
                  <Point X="-28.379837890625" Y="0.173334732056" />
                  <Point X="-28.402052734375" Y="0.192067611694" />
                  <Point X="-28.416205078125" Y="0.202906539917" />
                  <Point X="-28.433564453125" Y="0.214954803467" />
                  <Point X="-28.440484375" Y="0.219328186035" />
                  <Point X="-28.46561328125" Y="0.23283354187" />
                  <Point X="-28.496564453125" Y="0.245635482788" />
                  <Point X="-28.508287109375" Y="0.249611297607" />
                  <Point X="-29.436359375" Y="0.498287628174" />
                  <Point X="-29.785447265625" Y="0.591825256348" />
                  <Point X="-29.78398828125" Y="0.601680908203" />
                  <Point X="-29.73133203125" Y="0.957517883301" />
                  <Point X="-29.6764765625" Y="1.159950073242" />
                  <Point X="-29.6335859375" Y="1.318237060547" />
                  <Point X="-29.176400390625" Y="1.258047607422" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767736328125" Y="1.204815673828" />
                  <Point X="-28.747056640625" Y="1.204364379883" />
                  <Point X="-28.736705078125" Y="1.204703369141" />
                  <Point X="-28.71514453125" Y="1.20658972168" />
                  <Point X="-28.704890625" Y="1.208053466797" />
                  <Point X="-28.6846015625" Y="1.212089111328" />
                  <Point X="-28.67456640625" Y="1.214661010742" />
                  <Point X="-28.6515625" Y="1.221914428711" />
                  <Point X="-28.613140625" Y="1.234028442383" />
                  <Point X="-28.6034453125" Y="1.237677978516" />
                  <Point X="-28.584513671875" Y="1.246008789062" />
                  <Point X="-28.57527734375" Y="1.250689819336" />
                  <Point X="-28.556533203125" Y="1.261511962891" />
                  <Point X="-28.547857421875" Y="1.267173095703" />
                  <Point X="-28.53117578125" Y="1.279404907227" />
                  <Point X="-28.51592578125" Y="1.293380126953" />
                  <Point X="-28.502287109375" Y="1.308932861328" />
                  <Point X="-28.495892578125" Y="1.317081420898" />
                  <Point X="-28.483478515625" Y="1.334811645508" />
                  <Point X="-28.47801171875" Y="1.343602783203" />
                  <Point X="-28.4680625" Y="1.361736572266" />
                  <Point X="-28.463580078125" Y="1.371079101563" />
                  <Point X="-28.454349609375" Y="1.393364257812" />
                  <Point X="-28.43893359375" Y="1.430583251953" />
                  <Point X="-28.4355" Y="1.440349975586" />
                  <Point X="-28.429712890625" Y="1.460202026367" />
                  <Point X="-28.427359375" Y="1.470287353516" />
                  <Point X="-28.423599609375" Y="1.491603149414" />
                  <Point X="-28.422359375" Y="1.501890258789" />
                  <Point X="-28.421005859375" Y="1.522536376953" />
                  <Point X="-28.421908203125" Y="1.54320690918" />
                  <Point X="-28.425056640625" Y="1.563656494141" />
                  <Point X="-28.427189453125" Y="1.573794433594" />
                  <Point X="-28.43279296875" Y="1.594701538086" />
                  <Point X="-28.436015625" Y="1.604544677734" />
                  <Point X="-28.443509765625" Y="1.62381237793" />
                  <Point X="-28.44778125" Y="1.633237304688" />
                  <Point X="-28.45891796875" Y="1.654633300781" />
                  <Point X="-28.477521484375" Y="1.690366943359" />
                  <Point X="-28.482798828125" Y="1.699282714844" />
                  <Point X="-28.494294921875" Y="1.716488525391" />
                  <Point X="-28.500513671875" Y="1.724778442383" />
                  <Point X="-28.51442578125" Y="1.741356933594" />
                  <Point X="-28.52150390625" Y="1.748914794922" />
                  <Point X="-28.536443359375" Y="1.763215698242" />
                  <Point X="-28.5443046875" Y="1.769958862305" />
                  <Point X="-29.0766484375" Y="2.178441162109" />
                  <Point X="-29.22761328125" Y="2.294280761719" />
                  <Point X="-29.20689453125" Y="2.329777832031" />
                  <Point X="-29.00228515625" Y="2.680321533203" />
                  <Point X="-28.85699609375" Y="2.867070068359" />
                  <Point X="-28.726337890625" Y="3.035013183594" />
                  <Point X="-28.498802734375" Y="2.903645263672" />
                  <Point X="-28.25415625" Y="2.762398681641" />
                  <Point X="-28.24491796875" Y="2.757716796875" />
                  <Point X="-28.225984375" Y="2.749385742188" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.185615234375" Y="2.736657226562" />
                  <Point X="-28.165330078125" Y="2.732622070312" />
                  <Point X="-28.155076171875" Y="2.731157958984" />
                  <Point X="-28.123037109375" Y="2.728354736328" />
                  <Point X="-28.06952734375" Y="2.723673339844" />
                  <Point X="-28.059173828125" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310791016" />
                  <Point X="-27.97643359375" Y="2.734227294922" />
                  <Point X="-27.95699609375" Y="2.741302001953" />
                  <Point X="-27.9384453125" Y="2.750450439453" />
                  <Point X="-27.929419921875" Y="2.755530761719" />
                  <Point X="-27.911166015625" Y="2.767159667969" />
                  <Point X="-27.90274609375" Y="2.773193847656" />
                  <Point X="-27.886615234375" Y="2.786140625" />
                  <Point X="-27.878904296875" Y="2.793053222656" />
                  <Point X="-27.856162109375" Y="2.815795166016" />
                  <Point X="-27.8181796875" Y="2.853776611328" />
                  <Point X="-27.811265625" Y="2.861487060547" />
                  <Point X="-27.79831640625" Y="2.877620361328" />
                  <Point X="-27.79228125" Y="2.886043212891" />
                  <Point X="-27.78065234375" Y="2.904297607422" />
                  <Point X="-27.7755703125" Y="2.913325683594" />
                  <Point X="-27.766423828125" Y="2.931874755859" />
                  <Point X="-27.759353515625" Y="2.951299072266" />
                  <Point X="-27.754435546875" Y="2.971387939453" />
                  <Point X="-27.7525234375" Y="2.981573486328" />
                  <Point X="-27.749697265625" Y="3.003032714844" />
                  <Point X="-27.748908203125" Y="3.013365234375" />
                  <Point X="-27.74845703125" Y="3.034048095703" />
                  <Point X="-27.748794921875" Y="3.0443984375" />
                  <Point X="-27.75159765625" Y="3.076438232422" />
                  <Point X="-27.756279296875" Y="3.129947998047" />
                  <Point X="-27.757744140625" Y="3.140204833984" />
                  <Point X="-27.76178125" Y="3.160498779297" />
                  <Point X="-27.764353515625" Y="3.170535888672" />
                  <Point X="-27.77086328125" Y="3.191177978516" />
                  <Point X="-27.774513671875" Y="3.200873291016" />
                  <Point X="-27.78284375" Y="3.21980078125" />
                  <Point X="-27.7875234375" Y="3.229032958984" />
                  <Point X="-28.023421875" Y="3.637620117188" />
                  <Point X="-28.05938671875" Y="3.699915283203" />
                  <Point X="-28.004720703125" Y="3.741828369141" />
                  <Point X="-27.6483671875" Y="4.015040283203" />
                  <Point X="-27.41954296875" Y="4.142170410156" />
                  <Point X="-27.192525390625" Y="4.268296386719" />
                  <Point X="-27.118564453125" Y="4.171908203125" />
                  <Point X="-27.111818359375" Y="4.164045410156" />
                  <Point X="-27.097513671875" Y="4.149103027344" />
                  <Point X="-27.089955078125" Y="4.142022949219" />
                  <Point X="-27.073375" Y="4.128110839844" />
                  <Point X="-27.0650859375" Y="4.121894042969" />
                  <Point X="-27.04788671875" Y="4.110402832031" />
                  <Point X="-27.0389765625" Y="4.105128417969" />
                  <Point X="-27.00331640625" Y="4.086564941406" />
                  <Point X="-26.943759765625" Y="4.055561523438" />
                  <Point X="-26.934326171875" Y="4.051286865234" />
                  <Point X="-26.915048828125" Y="4.043790283203" />
                  <Point X="-26.905205078125" Y="4.040568359375" />
                  <Point X="-26.884298828125" Y="4.034966308594" />
                  <Point X="-26.8741640625" Y="4.032834960938" />
                  <Point X="-26.853720703125" Y="4.029687988281" />
                  <Point X="-26.833052734375" Y="4.028785400391" />
                  <Point X="-26.812412109375" Y="4.030138427734" />
                  <Point X="-26.802130859375" Y="4.031378417969" />
                  <Point X="-26.78081640625" Y="4.03513671875" />
                  <Point X="-26.770728515625" Y="4.037488525391" />
                  <Point X="-26.7508671875" Y="4.043277587891" />
                  <Point X="-26.74109375" Y="4.04671484375" />
                  <Point X="-26.703953125" Y="4.062100097656" />
                  <Point X="-26.641919921875" Y="4.087794433594" />
                  <Point X="-26.63258203125" Y="4.092273681641" />
                  <Point X="-26.614451171875" Y="4.102220703125" />
                  <Point X="-26.605658203125" Y="4.107688964844" />
                  <Point X="-26.587927734375" Y="4.120103515625" />
                  <Point X="-26.57977734375" Y="4.126499023438" />
                  <Point X="-26.5642265625" Y="4.14013671875" />
                  <Point X="-26.550255859375" Y="4.155382324219" />
                  <Point X="-26.5380234375" Y="4.172062988281" />
                  <Point X="-26.532361328125" Y="4.180740234375" />
                  <Point X="-26.5215390625" Y="4.199483886719" />
                  <Point X="-26.51685546875" Y="4.208727539062" />
                  <Point X="-26.5085234375" Y="4.227665039062" />
                  <Point X="-26.504875" Y="4.237358886719" />
                  <Point X="-26.492787109375" Y="4.275700683594" />
                  <Point X="-26.472595703125" Y="4.339735351562" />
                  <Point X="-26.470025390625" Y="4.349763671875" />
                  <Point X="-26.465990234375" Y="4.370046875" />
                  <Point X="-26.464525390625" Y="4.380301757812" />
                  <Point X="-26.462638671875" Y="4.401863769531" />
                  <Point X="-26.46230078125" Y="4.412216796875" />
                  <Point X="-26.462751953125" Y="4.4328984375" />
                  <Point X="-26.463541015625" Y="4.443227050781" />
                  <Point X="-26.479265625" Y="4.562655761719" />
                  <Point X="-26.392486328125" Y="4.586985351562" />
                  <Point X="-25.931177734375" Y="4.716319824219" />
                  <Point X="-25.653771484375" Y="4.748786132813" />
                  <Point X="-25.36522265625" Y="4.782556152344" />
                  <Point X="-25.298041015625" Y="4.531828613281" />
                  <Point X="-25.22566796875" Y="4.261728027344" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.767251953125" Y="4.247105957031" />
                  <Point X="-24.76201953125" Y="4.261727539062" />
                  <Point X="-24.641146484375" Y="4.712826171875" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.57492578125" Y="4.780096679688" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.942607421875" Y="4.682500488281" />
                  <Point X="-23.546408203125" Y="4.586845703125" />
                  <Point X="-23.398912109375" Y="4.533347167969" />
                  <Point X="-23.14173828125" Y="4.440068847656" />
                  <Point X="-22.99728125" Y="4.372511230469" />
                  <Point X="-22.749546875" Y="4.256652832031" />
                  <Point X="-22.6099375" Y="4.175316894531" />
                  <Point X="-22.370580078125" Y="4.035865966797" />
                  <Point X="-22.238943359375" Y="3.942255126953" />
                  <Point X="-22.18221875" Y="3.901916015625" />
                  <Point X="-22.61199609375" Y="3.157518798828" />
                  <Point X="-22.934689453125" Y="2.598597900391" />
                  <Point X="-22.93762109375" Y="2.593110595703" />
                  <Point X="-22.9468125" Y="2.573448486328" />
                  <Point X="-22.955814453125" Y="2.549571044922" />
                  <Point X="-22.958697265625" Y="2.540600830078" />
                  <Point X="-22.96673828125" Y="2.510532226562" />
                  <Point X="-22.98016796875" Y="2.460314453125" />
                  <Point X="-22.9820859375" Y="2.451470214844" />
                  <Point X="-22.986353515625" Y="2.420223632812" />
                  <Point X="-22.987244140625" Y="2.383236572266" />
                  <Point X="-22.986587890625" Y="2.369571533203" />
                  <Point X="-22.983451171875" Y="2.343570800781" />
                  <Point X="-22.978216796875" Y="2.300146728516" />
                  <Point X="-22.976197265625" Y="2.289026123047" />
                  <Point X="-22.97085546875" Y="2.267112304688" />
                  <Point X="-22.967533203125" Y="2.256319091797" />
                  <Point X="-22.959267578125" Y="2.234228515625" />
                  <Point X="-22.954685546875" Y="2.223898193359" />
                  <Point X="-22.9443203125" Y="2.203845458984" />
                  <Point X="-22.938537109375" Y="2.194123046875" />
                  <Point X="-22.922447265625" Y="2.170413085938" />
                  <Point X="-22.895578125" Y="2.130814453125" />
                  <Point X="-22.89018359375" Y="2.123624267578" />
                  <Point X="-22.869537109375" Y="2.100124267578" />
                  <Point X="-22.84240625" Y="2.075389648438" />
                  <Point X="-22.83174609375" Y="2.066983642578" />
                  <Point X="-22.808037109375" Y="2.050895263672" />
                  <Point X="-22.7684375" Y="2.024026000977" />
                  <Point X="-22.75871875" Y="2.018245361328" />
                  <Point X="-22.738669921875" Y="2.007881103516" />
                  <Point X="-22.72833984375" Y="2.003297485352" />
                  <Point X="-22.706251953125" Y="1.995031860352" />
                  <Point X="-22.695451171875" Y="1.991707397461" />
                  <Point X="-22.673529296875" Y="1.986365112305" />
                  <Point X="-22.662408203125" Y="1.984346801758" />
                  <Point X="-22.636408203125" Y="1.981211547852" />
                  <Point X="-22.592984375" Y="1.975975219727" />
                  <Point X="-22.583955078125" Y="1.975320922852" />
                  <Point X="-22.55242578125" Y="1.975497314453" />
                  <Point X="-22.515685546875" Y="1.979822875977" />
                  <Point X="-22.50225" Y="1.982395874023" />
                  <Point X="-22.472181640625" Y="1.990436767578" />
                  <Point X="-22.421962890625" Y="2.003865722656" />
                  <Point X="-22.416" Y="2.005671875" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572753906" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.43050390625" Y="2.566807128906" />
                  <Point X="-21.05959375" Y="2.780951660156" />
                  <Point X="-20.95604296875" Y="2.637041748047" />
                  <Point X="-20.882669921875" Y="2.515787841797" />
                  <Point X="-20.863115234375" Y="2.483471679688" />
                  <Point X="-21.40219140625" Y="2.069822753906" />
                  <Point X="-21.827046875" Y="1.743819702148" />
                  <Point X="-21.831859375" Y="1.739869262695" />
                  <Point X="-21.847876953125" Y="1.725219726562" />
                  <Point X="-21.865330078125" Y="1.706604248047" />
                  <Point X="-21.871421875" Y="1.699422973633" />
                  <Point X="-21.8930625" Y="1.67119140625" />
                  <Point X="-21.929203125" Y="1.624041503906" />
                  <Point X="-21.934361328125" Y="1.616604125977" />
                  <Point X="-21.95026171875" Y="1.589367919922" />
                  <Point X="-21.965234375" Y="1.555549072266" />
                  <Point X="-21.96985546875" Y="1.542680419922" />
                  <Point X="-21.97791796875" Y="1.513855957031" />
                  <Point X="-21.991380859375" Y="1.465715942383" />
                  <Point X="-21.9937734375" Y="1.454670043945" />
                  <Point X="-21.997228515625" Y="1.432368530273" />
                  <Point X="-21.998291015625" Y="1.421112792969" />
                  <Point X="-21.999107421875" Y="1.397543579102" />
                  <Point X="-21.998826171875" Y="1.386240112305" />
                  <Point X="-21.996921875" Y="1.363748779297" />
                  <Point X="-21.995298828125" Y="1.352561035156" />
                  <Point X="-21.9886796875" Y="1.320490234375" />
                  <Point X="-21.97762890625" Y="1.266928466797" />
                  <Point X="-21.9753984375" Y="1.258226196289" />
                  <Point X="-21.965318359375" Y="1.228619018555" />
                  <Point X="-21.949716796875" Y="1.195378173828" />
                  <Point X="-21.943083984375" Y="1.183527709961" />
                  <Point X="-21.9250859375" Y="1.156170898438" />
                  <Point X="-21.89502734375" Y="1.110482177734" />
                  <Point X="-21.88826171875" Y="1.101422973633" />
                  <Point X="-21.87370703125" Y="1.084177978516" />
                  <Point X="-21.86591796875" Y="1.0759921875" />
                  <Point X="-21.848673828125" Y="1.059901977539" />
                  <Point X="-21.839966796875" Y="1.052697021484" />
                  <Point X="-21.8217578125" Y="1.039371459961" />
                  <Point X="-21.812255859375" Y="1.03325012207" />
                  <Point X="-21.786173828125" Y="1.018568054199" />
                  <Point X="-21.74261328125" Y="0.994047790527" />
                  <Point X="-21.73451953125" Y="0.989986816406" />
                  <Point X="-21.705318359375" Y="0.978083312988" />
                  <Point X="-21.66972265625" Y="0.968021118164" />
                  <Point X="-21.656328125" Y="0.965257751465" />
                  <Point X="-21.6210625" Y="0.960596801758" />
                  <Point X="-21.562166015625" Y="0.952812988281" />
                  <Point X="-21.555966796875" Y="0.952199768066" />
                  <Point X="-21.53428125" Y="0.951222839355" />
                  <Point X="-21.50878125" Y="0.952032409668" />
                  <Point X="-21.49939453125" Y="0.952797180176" />
                  <Point X="-20.612669921875" Y="1.069537231445" />
                  <Point X="-20.295296875" Y="1.111319824219" />
                  <Point X="-20.247310546875" Y="0.91421295166" />
                  <Point X="-20.22419140625" Y="0.76571484375" />
                  <Point X="-20.216126953125" Y="0.713921203613" />
                  <Point X="-20.82111328125" Y="0.551815490723" />
                  <Point X="-21.3080078125" Y="0.421352813721" />
                  <Point X="-21.313966796875" Y="0.419544525146" />
                  <Point X="-21.334376953125" Y="0.412136260986" />
                  <Point X="-21.357619140625" Y="0.401618530273" />
                  <Point X="-21.365994140625" Y="0.397316619873" />
                  <Point X="-21.400640625" Y="0.377290283203" />
                  <Point X="-21.45850390625" Y="0.343844024658" />
                  <Point X="-21.46611328125" Y="0.338947631836" />
                  <Point X="-21.49122265625" Y="0.319872497559" />
                  <Point X="-21.518005859375" Y="0.294349914551" />
                  <Point X="-21.527203125" Y="0.284224517822" />
                  <Point X="-21.547990234375" Y="0.257735900879" />
                  <Point X="-21.582708984375" Y="0.213496963501" />
                  <Point X="-21.589146484375" Y="0.204206695557" />
                  <Point X="-21.60087109375" Y="0.184925384521" />
                  <Point X="-21.606158203125" Y="0.174934356689" />
                  <Point X="-21.615931640625" Y="0.153469787598" />
                  <Point X="-21.619994140625" Y="0.14292767334" />
                  <Point X="-21.62683984375" Y="0.121429962158" />
                  <Point X="-21.629623046875" Y="0.110474372864" />
                  <Point X="-21.636552734375" Y="0.074292266846" />
                  <Point X="-21.648125" Y="0.013863965034" />
                  <Point X="-21.649396484375" Y="0.004966505051" />
                  <Point X="-21.6514140625" Y="-0.026261997223" />
                  <Point X="-21.64971875" Y="-0.062940353394" />
                  <Point X="-21.648125" Y="-0.076423873901" />
                  <Point X="-21.6411953125" Y="-0.112606124878" />
                  <Point X="-21.629623046875" Y="-0.173034423828" />
                  <Point X="-21.62683984375" Y="-0.183990310669" />
                  <Point X="-21.619994140625" Y="-0.205487869263" />
                  <Point X="-21.615931640625" Y="-0.216029693604" />
                  <Point X="-21.606158203125" Y="-0.237494262695" />
                  <Point X="-21.60087109375" Y="-0.247485290527" />
                  <Point X="-21.589146484375" Y="-0.26676675415" />
                  <Point X="-21.582708984375" Y="-0.276056854248" />
                  <Point X="-21.561921875" Y="-0.302545471191" />
                  <Point X="-21.527203125" Y="-0.346784576416" />
                  <Point X="-21.521283203125" Y="-0.353630462646" />
                  <Point X="-21.49885546875" Y="-0.375809295654" />
                  <Point X="-21.4698203125" Y="-0.398726379395" />
                  <Point X="-21.45850390625" Y="-0.406404388428" />
                  <Point X="-21.423857421875" Y="-0.42643057251" />
                  <Point X="-21.365994140625" Y="-0.459876831055" />
                  <Point X="-21.360505859375" Y="-0.462813476562" />
                  <Point X="-21.34084375" Y="-0.472016052246" />
                  <Point X="-21.31697265625" Y="-0.481027648926" />
                  <Point X="-21.3080078125" Y="-0.483912719727" />
                  <Point X="-20.49483984375" Y="-0.701800598145" />
                  <Point X="-20.21512109375" Y="-0.776750793457" />
                  <Point X="-20.2383828125" Y="-0.931050476074" />
                  <Point X="-20.268009765625" Y="-1.060874389648" />
                  <Point X="-20.2721953125" Y="-1.079219726563" />
                  <Point X="-20.998556640625" Y="-0.983592346191" />
                  <Point X="-21.563216796875" Y="-0.909253540039" />
                  <Point X="-21.571375" Y="-0.908535583496" />
                  <Point X="-21.59990234375" Y="-0.908042419434" />
                  <Point X="-21.63327734375" Y="-0.910840881348" />
                  <Point X="-21.645517578125" Y="-0.912676208496" />
                  <Point X="-21.713515625" Y="-0.927456176758" />
                  <Point X="-21.82708203125" Y="-0.952140197754" />
                  <Point X="-21.842123046875" Y="-0.956742004395" />
                  <Point X="-21.8712421875" Y="-0.968365661621" />
                  <Point X="-21.885318359375" Y="-0.975386901855" />
                  <Point X="-21.913146484375" Y="-0.99227923584" />
                  <Point X="-21.925875" Y="-1.001530273438" />
                  <Point X="-21.949625" Y="-1.022001586914" />
                  <Point X="-21.9606484375" Y="-1.033222045898" />
                  <Point X="-22.00175" Y="-1.082653686523" />
                  <Point X="-22.070392578125" Y="-1.165210083008" />
                  <Point X="-22.078673828125" Y="-1.176849121094" />
                  <Point X="-22.093396484375" Y="-1.201236206055" />
                  <Point X="-22.099837890625" Y="-1.21398425293" />
                  <Point X="-22.1111796875" Y="-1.241369628906" />
                  <Point X="-22.11563671875" Y="-1.254934692383" />
                  <Point X="-22.122466796875" Y="-1.282580444336" />
                  <Point X="-22.12483984375" Y="-1.296660888672" />
                  <Point X="-22.13073046875" Y="-1.360677001953" />
                  <Point X="-22.140568359375" Y="-1.467591064453" />
                  <Point X="-22.140708984375" Y="-1.483315307617" />
                  <Point X="-22.138392578125" Y="-1.514580932617" />
                  <Point X="-22.135935546875" Y="-1.530122314453" />
                  <Point X="-22.128205078125" Y="-1.561743286133" />
                  <Point X="-22.12321484375" Y="-1.576667480469" />
                  <Point X="-22.110841796875" Y="-1.605480957031" />
                  <Point X="-22.103458984375" Y="-1.619370483398" />
                  <Point X="-22.065828125" Y="-1.677903808594" />
                  <Point X="-22.002978515625" Y="-1.775661132812" />
                  <Point X="-21.998255859375" Y="-1.782353271484" />
                  <Point X="-21.98019921875" Y="-1.804457275391" />
                  <Point X="-21.956505859375" Y="-1.828123291016" />
                  <Point X="-21.947201171875" Y="-1.836277587891" />
                  <Point X="-21.192576171875" Y="-2.415322509766" />
                  <Point X="-20.912828125" Y="-2.629981689453" />
                  <Point X="-20.954517578125" Y="-2.697440429688" />
                  <Point X="-20.99872265625" Y="-2.760252685547" />
                  <Point X="-21.648638671875" Y="-2.385023681641" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.18497265625" Y="-2.079513427734" />
                  <Point X="-22.216880859375" Y="-2.069325927734" />
                  <Point X="-22.228892578125" Y="-2.066337646484" />
                  <Point X="-22.309822265625" Y="-2.051721923828" />
                  <Point X="-22.444982421875" Y="-2.027311889648" />
                  <Point X="-22.460640625" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539859375" Y="-2.031461181641" />
                  <Point X="-22.55515625" Y="-2.035136352539" />
                  <Point X="-22.584931640625" Y="-2.044960205078" />
                  <Point X="-22.59941015625" Y="-2.051108642578" />
                  <Point X="-22.666642578125" Y="-2.086492431641" />
                  <Point X="-22.778927734375" Y="-2.145587646484" />
                  <Point X="-22.79103125" Y="-2.153170654297" />
                  <Point X="-22.813962890625" Y="-2.170064697266" />
                  <Point X="-22.824791015625" Y="-2.179375732422" />
                  <Point X="-22.84575" Y="-2.200335449219" />
                  <Point X="-22.85505859375" Y="-2.211160888672" />
                  <Point X="-22.871951171875" Y="-2.23408984375" />
                  <Point X="-22.87953515625" Y="-2.246193359375" />
                  <Point X="-22.914919921875" Y="-2.31342578125" />
                  <Point X="-22.974015625" Y="-2.425711425781" />
                  <Point X="-22.9801640625" Y="-2.440193359375" />
                  <Point X="-22.98998828125" Y="-2.469972167969" />
                  <Point X="-22.9936640625" Y="-2.485269042969" />
                  <Point X="-22.99862109375" Y="-2.517441894531" />
                  <Point X="-22.999720703125" Y="-2.533133056641" />
                  <Point X="-22.99931640625" Y="-2.564484863281" />
                  <Point X="-22.9978125" Y="-2.580145507812" />
                  <Point X="-22.9831953125" Y="-2.661074707031" />
                  <Point X="-22.95878515625" Y="-2.796235839844" />
                  <Point X="-22.95698046875" Y="-2.804232910156" />
                  <Point X="-22.94876171875" Y="-2.831539794922" />
                  <Point X="-22.935927734375" Y="-2.862478759766" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.44552734375" Y="-3.713489257813" />
                  <Point X="-22.264103515625" Y="-4.027723388672" />
                  <Point X="-22.276244140625" Y="-4.036083007813" />
                  <Point X="-22.7823203125" Y="-3.376553222656" />
                  <Point X="-23.16608203125" Y="-2.876422851562" />
                  <Point X="-23.17134765625" Y="-2.870142089844" />
                  <Point X="-23.19116796875" Y="-2.849625732422" />
                  <Point X="-23.21674609375" Y="-2.828004638672" />
                  <Point X="-23.22669921875" Y="-2.820646972656" />
                  <Point X="-23.306517578125" Y="-2.769331298828" />
                  <Point X="-23.43982421875" Y="-2.683628173828" />
                  <Point X="-23.453716796875" Y="-2.676245117188" />
                  <Point X="-23.482529296875" Y="-2.663873535156" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.678900390625" Y="-2.654549316406" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920125" Y="-2.699414794922" />
                  <Point X="-23.944505859375" Y="-2.714135742188" />
                  <Point X="-23.956138671875" Y="-2.722413330078" />
                  <Point X="-24.023544921875" Y="-2.778459716797" />
                  <Point X="-24.136123046875" Y="-2.872063720703" />
                  <Point X="-24.147345703125" Y="-2.883087646484" />
                  <Point X="-24.16781640625" Y="-2.906837158203" />
                  <Point X="-24.177064453125" Y="-2.919562744141" />
                  <Point X="-24.19395703125" Y="-2.947389404297" />
                  <Point X="-24.20098046875" Y="-2.961466552734" />
                  <Point X="-24.21260546875" Y="-2.990587402344" />
                  <Point X="-24.21720703125" Y="-3.005631103516" />
                  <Point X="-24.237361328125" Y="-3.098356689453" />
                  <Point X="-24.271021484375" Y="-3.253218994141" />
                  <Point X="-24.2724140625" Y="-3.261287109375" />
                  <Point X="-24.275275390625" Y="-3.289677246094" />
                  <Point X="-24.2752578125" Y="-3.323170166016" />
                  <Point X="-24.2744453125" Y="-3.335520507812" />
                  <Point X="-24.16691015625" Y="-4.152321289062" />
                  <Point X="-24.194908203125" Y="-4.047833984375" />
                  <Point X="-24.344931640625" Y="-3.487937011719" />
                  <Point X="-24.347392578125" Y="-3.480121337891" />
                  <Point X="-24.357853515625" Y="-3.453579345703" />
                  <Point X="-24.3732109375" Y="-3.423815917969" />
                  <Point X="-24.37958984375" Y="-3.413210449219" />
                  <Point X="-24.440908203125" Y="-3.324861816406" />
                  <Point X="-24.543318359375" Y="-3.177310058594" />
                  <Point X="-24.553328125" Y="-3.165173339844" />
                  <Point X="-24.575212890625" Y="-3.142716796875" />
                  <Point X="-24.587087890625" Y="-3.132396972656" />
                  <Point X="-24.61334375" Y="-3.113153320312" />
                  <Point X="-24.626759765625" Y="-3.104936279297" />
                  <Point X="-24.654759765625" Y="-3.090829101562" />
                  <Point X="-24.66934375" Y="-3.084938964844" />
                  <Point X="-24.76423046875" Y="-3.055489501953" />
                  <Point X="-24.922703125" Y="-3.006305664062" />
                  <Point X="-24.936623046875" Y="-3.003109619141" />
                  <Point X="-24.964783203125" Y="-2.998840087891" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.051064453125" Y="-3.003109130859" />
                  <Point X="-25.064984375" Y="-3.006305175781" />
                  <Point X="-25.15987109375" Y="-3.035754394531" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.332931640625" Y="-3.090830078125" />
                  <Point X="-25.360931640625" Y="-3.104938232422" />
                  <Point X="-25.37434375" Y="-3.113154785156" />
                  <Point X="-25.400599609375" Y="-3.132398925781" />
                  <Point X="-25.412474609375" Y="-3.142716552734" />
                  <Point X="-25.434359375" Y="-3.165172119141" />
                  <Point X="-25.444369140625" Y="-3.177310058594" />
                  <Point X="-25.5056875" Y="-3.265658447266" />
                  <Point X="-25.608095703125" Y="-3.413210449219" />
                  <Point X="-25.61246875" Y="-3.420130615234" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476214111328" />
                  <Point X="-25.64275390625" Y="-3.487936767578" />
                  <Point X="-25.893310546875" Y="-4.423028808594" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.293765637718" Y="0.693117966038" />
                  <Point X="-20.387512337246" Y="1.099179533339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.385584521011" Y="0.668515152981" />
                  <Point X="-20.482135248227" Y="1.086722298563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.477403404304" Y="0.643912339923" />
                  <Point X="-20.576758159209" Y="1.074265063787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.896232376252" Y="2.458059927366" />
                  <Point X="-20.926147838362" Y="2.587638029764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.227681467861" Y="-0.860066293905" />
                  <Point X="-20.249012942449" Y="-0.767669526365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.569222287598" Y="0.619309526866" />
                  <Point X="-20.671381047168" Y="1.061807729293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.979058465667" Y="2.394505044585" />
                  <Point X="-21.06725545662" Y="2.776528183082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.274659705277" Y="-1.078895282768" />
                  <Point X="-20.352940895424" Y="-0.739822196238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.661041170891" Y="0.594706713809" />
                  <Point X="-20.766003921046" Y="1.049350333805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.061884555083" Y="2.330950161804" />
                  <Point X="-21.153287066918" Y="2.726857936664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.375214911162" Y="-1.065656925288" />
                  <Point X="-20.456868848398" Y="-0.711974866111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.752860054184" Y="0.570103900752" />
                  <Point X="-20.860626794924" Y="1.036892938317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.144710644499" Y="2.267395279023" />
                  <Point X="-21.239318677215" Y="2.677187690247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.475770117048" Y="-1.052418567808" />
                  <Point X="-20.560796817565" Y="-0.684127465848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.844678941811" Y="0.545501106467" />
                  <Point X="-20.955249668802" Y="1.024435542829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.227536733914" Y="2.203840396243" />
                  <Point X="-21.325350287512" Y="2.627517443829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.576325322933" Y="-1.039180210328" />
                  <Point X="-20.664724796053" Y="-0.656280025206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.936497841992" Y="0.520898366555" />
                  <Point X="-21.04987254268" Y="1.011978147341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.31036282333" Y="2.140285513462" />
                  <Point X="-21.411381897809" Y="2.577847197411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.676880528818" Y="-1.025941852847" />
                  <Point X="-20.768652774542" Y="-0.628432584565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.028316742172" Y="0.496295626643" />
                  <Point X="-21.144495416558" Y="0.999520751854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.393188912745" Y="2.076730630681" />
                  <Point X="-21.497413478549" Y="2.528176822968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.777435734703" Y="-1.012703495367" />
                  <Point X="-20.872580753031" Y="-0.600585143924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.120135642352" Y="0.471692886732" />
                  <Point X="-21.239118290436" Y="0.987063356366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.476015035736" Y="2.013175893328" />
                  <Point X="-21.583445050842" Y="2.478506411937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.877990940589" Y="-0.999465137887" />
                  <Point X="-20.97650873152" Y="-0.572737703282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.211954542532" Y="0.44709014682" />
                  <Point X="-21.333741164314" Y="0.974605960878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.55884116282" Y="1.949621173709" />
                  <Point X="-21.669476623136" Y="2.428836000906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.978546146474" Y="-0.986226780407" />
                  <Point X="-21.080436710008" Y="-0.544890262641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.303773442712" Y="0.422487406908" />
                  <Point X="-21.428364038192" Y="0.96214856539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.641667289905" Y="1.886066454091" />
                  <Point X="-21.755508195429" Y="2.379165589875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.079101345926" Y="-0.97298845079" />
                  <Point X="-21.184364688497" Y="-0.517042822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.39199189546" Y="0.382289415804" />
                  <Point X="-21.523420133142" Y="0.951567656523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.724493416989" Y="1.822511734472" />
                  <Point X="-21.841539767722" Y="2.329495178844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.198179429537" Y="3.874271269809" />
                  <Point X="-22.208950532526" Y="3.920926042546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.179656543781" Y="-0.959750128096" />
                  <Point X="-21.288292666986" Y="-0.489195381358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.477489340818" Y="0.330305446843" />
                  <Point X="-21.623064657939" Y="0.960861420844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.807319544074" Y="1.758957014854" />
                  <Point X="-21.927571340015" Y="2.279824767813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.267827663554" Y="3.753636824296" />
                  <Point X="-22.32560082069" Y="4.00387986061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.280211741635" Y="-0.946511805402" />
                  <Point X="-21.396651394264" Y="-0.442156259228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.555905703718" Y="0.247649940048" />
                  <Point X="-21.726536375893" Y="0.986732579984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.886525096527" Y="1.679719863568" />
                  <Point X="-22.013602912308" Y="2.230154356781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.337475897572" Y="3.633002378783" />
                  <Point X="-22.439793893524" Y="4.076190309756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.968793674291" Y="-2.717725741763" />
                  <Point X="-21.005461122419" Y="-2.558901574829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.380766939489" Y="-0.933273482708" />
                  <Point X="-21.512607318151" Y="-0.362210063279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.625346842514" Y="0.126118466579" />
                  <Point X="-21.839121551308" Y="1.052078460253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.958742288172" Y="1.57021279604" />
                  <Point X="-22.099634484601" Y="2.18048394575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.407124131589" Y="3.51236793327" />
                  <Point X="-22.55244491583" Y="4.141821404241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.065356139344" Y="-2.721781844874" />
                  <Point X="-21.123950699643" Y="-2.467980920573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.481322137344" Y="-0.920035160014" />
                  <Point X="-22.185666056894" Y="2.130813534719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.476772365606" Y="3.391733487757" />
                  <Point X="-22.665095807561" Y="4.207451933144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.177849460999" Y="-2.656833826951" />
                  <Point X="-21.242440243895" Y="-2.377060409137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.58151638946" Y="-0.908360265076" />
                  <Point X="-22.271697629188" Y="2.081143123688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.546420599624" Y="3.271099042244" />
                  <Point X="-22.776907794044" Y="4.269448764208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.290342782654" Y="-2.591885809029" />
                  <Point X="-21.360929742769" Y="-2.286140094254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.676465844094" Y="-0.919403083896" />
                  <Point X="-22.357729201481" Y="2.031472712657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.61606883606" Y="3.150464607209" />
                  <Point X="-22.8862078751" Y="4.320565337514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.402836104309" Y="-2.526937791107" />
                  <Point X="-21.479419241643" Y="-2.195219779371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.769305975022" Y="-0.939582387464" />
                  <Point X="-22.44729086131" Y="1.997092790623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.685717111448" Y="3.02983034089" />
                  <Point X="-22.995507956157" Y="4.371681910821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.515329425965" Y="-2.461989773184" />
                  <Point X="-21.597908740517" Y="-2.104299464488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.861094869257" Y="-0.964315097405" />
                  <Point X="-22.54013803566" Y="1.976943995468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.755365386835" Y="2.909196074571" />
                  <Point X="-23.104807869078" Y="4.422797755855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.62782274762" Y="-2.397041755262" />
                  <Point X="-21.716398239391" Y="-2.013379149605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.945997620996" Y="-1.018874967432" />
                  <Point X="-22.638685569025" Y="1.981486167864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.825013662223" Y="2.788561808252" />
                  <Point X="-23.212193852001" Y="4.465623459275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.740316074611" Y="-2.33209371423" />
                  <Point X="-21.834887738264" Y="-1.922458834723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.022899544169" Y="-1.108090233366" />
                  <Point X="-22.742767211585" Y="2.009999200724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.89466193761" Y="2.667927541933" />
                  <Point X="-23.318603151054" Y="4.504218680078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.852809402813" Y="-2.26714566795" />
                  <Point X="-21.95357214132" Y="-1.830694297077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.09716989667" Y="-1.208704084178" />
                  <Point X="-22.858816709496" Y="2.090350710303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.960880410653" Y="2.532437169502" />
                  <Point X="-23.425012490886" Y="4.542814077514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.965302731015" Y="-2.20219762167" />
                  <Point X="-22.097841817534" Y="-1.628107766013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.138952977211" Y="-1.450035769707" />
                  <Point X="-23.531421956192" Y="4.581410018437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.077796059217" Y="-2.13724957539" />
                  <Point X="-23.635120484345" Y="4.608263600484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.188914980103" Y="-2.078254741243" />
                  <Point X="-23.738374631143" Y="4.633192355416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.291787507564" Y="-2.054978961266" />
                  <Point X="-23.841628777942" Y="4.658121110348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.393528473053" Y="-2.036604514667" />
                  <Point X="-23.944882924496" Y="4.68304986422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.493587568822" Y="-2.025515046188" />
                  <Point X="-24.048137060193" Y="4.707978571068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.586448479044" Y="-2.045604344744" />
                  <Point X="-24.15139119589" Y="4.732907277915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.673655505092" Y="-2.090183306193" />
                  <Point X="-24.251976294183" Y="4.746275113635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.341559718043" Y="-3.950962286594" />
                  <Point X="-22.363640116413" Y="-3.855321573758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.760591267499" Y="-2.135937239559" />
                  <Point X="-24.351890936446" Y="4.756738885241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.481017689257" Y="-3.769217539646" />
                  <Point X="-22.526104421765" Y="-3.573925445538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.84369671445" Y="-2.198282091916" />
                  <Point X="-24.45180557871" Y="4.767202656847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.620475660472" Y="-3.587472792699" />
                  <Point X="-22.688569013752" Y="-3.292528075768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.914706297541" Y="-2.313019886761" />
                  <Point X="-24.551720220973" Y="4.777666428453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.759933631687" Y="-3.405728045751" />
                  <Point X="-22.851033605739" Y="-3.011130705997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.98174119927" Y="-2.444973918032" />
                  <Point X="-24.637441738976" Y="4.726653024747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.899391957172" Y="-3.223981764292" />
                  <Point X="-24.689815328793" Y="4.531193874641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.038850350401" Y="-3.042235189399" />
                  <Point X="-24.742189009155" Y="4.335735116733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.177566127053" Y="-2.863705240293" />
                  <Point X="-24.803931188417" Y="4.180855785783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.295168447063" Y="-2.776627719255" />
                  <Point X="-24.885492543012" Y="4.111822734639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.409660974909" Y="-2.703020187948" />
                  <Point X="-24.976881695134" Y="4.085358551384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.518539792822" Y="-2.653728305784" />
                  <Point X="-25.078182325867" Y="4.101825698609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.617160777583" Y="-2.64886798044" />
                  <Point X="-25.199295425386" Y="4.204110076387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.712631412249" Y="-2.657653320526" />
                  <Point X="-25.428625993161" Y="4.775135807095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.808102028506" Y="-2.666438740354" />
                  <Point X="-25.523559825861" Y="4.764025322241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.900165023583" Y="-2.689984189109" />
                  <Point X="-25.61849365856" Y="4.752914837387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.984695313535" Y="-2.746157368381" />
                  <Point X="-25.71342747437" Y="4.741804279373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.06649251688" Y="-2.814168846352" />
                  <Point X="-25.808361280191" Y="4.730693678095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.147925067628" Y="-2.883759808254" />
                  <Point X="-25.903295086012" Y="4.719583076817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.217246174938" Y="-3.005811195207" />
                  <Point X="-25.995854264055" Y="4.69818683261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.264525892056" Y="-3.223334332011" />
                  <Point X="-26.087425973517" Y="4.672513392067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.192598839116" Y="-3.957198717376" />
                  <Point X="-26.17899768298" Y="4.646839951525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.433679733688" Y="-3.335276729633" />
                  <Point X="-26.270569392442" Y="4.621166510982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.575740428994" Y="-3.142258346076" />
                  <Point X="-26.362141101904" Y="4.595493070439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.687794605321" Y="-3.07921247555" />
                  <Point X="-26.453712846427" Y="4.569819781761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.792818805257" Y="-3.046616778156" />
                  <Point X="-26.487315296667" Y="4.293053893448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.897842952584" Y="-3.014021308635" />
                  <Point X="-26.55247189998" Y="4.152964057903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.999094537644" Y="-2.997766601563" />
                  <Point X="-26.63562258912" Y="4.090815170993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.092640506696" Y="-3.014888584322" />
                  <Point X="-26.724524706866" Y="4.053578458339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.183620433793" Y="-3.043125315903" />
                  <Point X="-26.816549426336" Y="4.029867219717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.274600341396" Y="-3.071362131923" />
                  <Point X="-26.917481076634" Y="4.044736137088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.363924310074" Y="-3.106771607445" />
                  <Point X="-27.027549265789" Y="4.099179752091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.444946172733" Y="-3.178141454891" />
                  <Point X="-27.151855173195" Y="4.215293700207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.518108463922" Y="-3.283554846547" />
                  <Point X="-27.253739103064" Y="4.234287393565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.591270676434" Y="-3.388968578984" />
                  <Point X="-27.340153965674" Y="4.186277195296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.655198515485" Y="-3.534380777286" />
                  <Point X="-27.426568825642" Y="4.138266985582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.707571838536" Y="-3.729841082883" />
                  <Point X="-27.512983655753" Y="4.090256646542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.759945161586" Y="-3.92530138848" />
                  <Point X="-27.599398485864" Y="4.042246307503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.812318484636" Y="-4.120761694077" />
                  <Point X="-27.684262722908" Y="3.987519612001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.864691807687" Y="-4.316221999674" />
                  <Point X="-27.767099232858" Y="3.924009865513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.917065431006" Y="-4.511681004663" />
                  <Point X="-27.849935742807" Y="3.860500119024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.969439416079" Y="-4.707138442724" />
                  <Point X="-27.74964144334" Y="3.003763689707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.811061039869" Y="3.269801190283" />
                  <Point X="-27.932772252757" Y="3.796990372536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.056340765539" Y="-4.753041434944" />
                  <Point X="-26.173395124525" Y="-4.246023303014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.190363597869" Y="-4.172524770102" />
                  <Point X="-27.81367330361" Y="2.858802056809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.973525217804" Y="3.551196766599" />
                  <Point X="-28.015608711316" Y="3.733480403448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.327317648214" Y="-4.001625695991" />
                  <Point X="-27.893180463569" Y="2.780871311158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.449568297349" Y="-3.894414049495" />
                  <Point X="-27.979724800958" Y="2.733421929767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.565443293999" Y="-3.814818387916" />
                  <Point X="-28.075085311214" Y="2.724159588457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.667715174228" Y="-3.794144296934" />
                  <Point X="-28.17498114128" Y="2.734541875481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.766712097873" Y="-3.787655601381" />
                  <Point X="-28.282718375953" Y="2.778889017387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.865708970803" Y="-3.781167125502" />
                  <Point X="-28.395211689915" Y="2.843837001982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.964315974429" Y="-3.776367359106" />
                  <Point X="-28.507705009697" Y="2.908785011792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.057045852402" Y="-3.797024220686" />
                  <Point X="-27.318960947791" Y="-2.662545303897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.368569637077" Y="-2.447666463101" />
                  <Point X="-28.620198397221" Y="2.973733315024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.142887361583" Y="-3.847517885501" />
                  <Point X="-27.384889849081" Y="-2.799289949389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.485604713993" Y="-2.363045941841" />
                  <Point X="-28.730584640152" Y="3.029554572285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.227355900348" Y="-3.903958538552" />
                  <Point X="-27.454538114955" Y="-2.919924256913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.58621172933" Y="-2.34958317297" />
                  <Point X="-28.451774540987" Y="1.399581263399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.535522698958" Y="1.762334389167" />
                  <Point X="-28.805771798009" Y="2.932911841755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.311824476731" Y="-3.960399028659" />
                  <Point X="-27.52418638083" Y="-3.040558564437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.67789585625" Y="-2.374769680005" />
                  <Point X="-28.523213176874" Y="1.286701900391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.654260731272" Y="1.854331220649" />
                  <Point X="-28.880958971537" Y="2.836269179099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.394539792739" Y="-4.024433723784" />
                  <Point X="-27.593834646705" Y="-3.161192871962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.764114331481" Y="-2.423630525473" />
                  <Point X="-27.98017884867" Y="-1.487752281979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.057726550111" Y="-1.151856284083" />
                  <Point X="-28.302493800631" Y="-0.091652843638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.358626420377" Y="0.15148424455" />
                  <Point X="-28.60891837198" Y="1.235617794449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.772750231003" Y="1.945251539246" />
                  <Point X="-28.956146178561" Y="2.739626661536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.470121460368" Y="-4.119367644754" />
                  <Point X="-27.663482904494" Y="-3.281827214506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.850145930431" Y="-2.473300821041" />
                  <Point X="-28.056337841781" Y="-1.580185531555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.165908043569" Y="-1.105584845968" />
                  <Point X="-28.3691065662" Y="-0.225435347501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.47588735851" Y="0.237083078224" />
                  <Point X="-28.700265909137" Y="1.208973356973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.891239730735" Y="2.036171857842" />
                  <Point X="-29.029277204179" Y="2.634077843815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.566751967234" Y="-4.123129026389" />
                  <Point X="-27.733131157331" Y="-3.402461578504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.936177529381" Y="-2.522971116609" />
                  <Point X="-28.139163933384" Y="-1.643740404861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.264051392324" Y="-1.10279338946" />
                  <Point X="-28.452265266179" Y="-0.287549535653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.580761982355" Y="0.26903089039" />
                  <Point X="-28.797580495057" Y="1.208175047255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.009729230466" Y="2.127092176439" />
                  <Point X="-29.099142278301" Y="2.514382635982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.68051267597" Y="-4.052691351905" />
                  <Point X="-27.802779410167" Y="-3.523095942502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.022209128331" Y="-2.572641412176" />
                  <Point X="-28.221990024987" Y="-1.707295278167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.358674284839" Y="-1.115250704222" />
                  <Point X="-28.541994616157" Y="-0.321203111855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.684689958459" Y="0.296878320701" />
                  <Point X="-28.898135696138" Y="1.221413383928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.12821873385" Y="2.218012510859" />
                  <Point X="-29.169007352422" Y="2.394687428149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.796249769893" Y="-3.973693012665" />
                  <Point X="-27.872427663004" Y="-3.6437303065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.108240727281" Y="-2.622311707744" />
                  <Point X="-28.30481611659" Y="-1.770850151472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.453297177354" Y="-1.127708018983" />
                  <Point X="-28.633813497897" Y="-0.345805931639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.788617934563" Y="0.324725751012" />
                  <Point X="-28.99869089722" Y="1.234651720601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.914827069335" Y="-3.882392391739" />
                  <Point X="-27.94207591584" Y="-3.764364670498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.194272326231" Y="-2.671982003312" />
                  <Point X="-28.387642208193" Y="-1.834405024778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.547920069869" Y="-1.140165333745" />
                  <Point X="-28.725632379637" Y="-0.370408751424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.892545910667" Y="0.352573181323" />
                  <Point X="-29.099246098302" Y="1.247890057275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.280303925181" Y="-2.721652298879" />
                  <Point X="-28.470468299796" Y="-1.897959898083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.642542962384" Y="-1.152622648507" />
                  <Point X="-28.817451261377" Y="-0.395011571208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.99647388677" Y="0.380420611634" />
                  <Point X="-29.199801297731" Y="1.261128386793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.366335524131" Y="-2.771322594447" />
                  <Point X="-28.553294401214" Y="-1.961514728879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.737165854899" Y="-1.165079963268" />
                  <Point X="-28.909270143117" Y="-0.419614390992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.100401862874" Y="0.408268041945" />
                  <Point X="-29.300356491715" Y="1.274366692719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.452367123081" Y="-2.820992890015" />
                  <Point X="-28.636120539645" Y="-2.02506939935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.831788747415" Y="-1.17753727803" />
                  <Point X="-29.001089024857" Y="-0.444217210776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.204329838978" Y="0.436115472257" />
                  <Point X="-29.400911685698" Y="1.287604998646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.538398709215" Y="-2.870663241091" />
                  <Point X="-28.718946678076" Y="-2.088624069821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.92641163993" Y="-1.189994592792" />
                  <Point X="-29.092907906885" Y="-0.468820029317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.308257815082" Y="0.463962902568" />
                  <Point X="-29.501466879681" Y="1.300843304572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.624430283783" Y="-2.920333642271" />
                  <Point X="-28.801772816507" Y="-2.152178740292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.021034532445" Y="-1.202451907553" />
                  <Point X="-29.184726789537" Y="-0.493422845152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.412185791186" Y="0.491810332879" />
                  <Point X="-29.602022073664" Y="1.314081610499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.71046185835" Y="-2.97000404345" />
                  <Point X="-28.884598954938" Y="-2.215733410763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.11565742496" Y="-1.214909222315" />
                  <Point X="-29.276545672189" Y="-0.518025660987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.516113754132" Y="0.5196577062" />
                  <Point X="-29.669705736232" Y="1.184937671149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.798627544527" Y="-3.010430591682" />
                  <Point X="-28.967425093369" Y="-2.279288081234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.210280317475" Y="-1.227366537077" />
                  <Point X="-29.368364554841" Y="-0.542628476823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.620041713091" Y="0.547505062247" />
                  <Point X="-29.722351642972" Y="0.990658055229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.938573972669" Y="-2.826570105337" />
                  <Point X="-29.0502512318" Y="-2.342842751705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.304903201699" Y="-1.239823887751" />
                  <Point X="-29.460183437492" Y="-0.567231292658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.723969672049" Y="0.575352418294" />
                  <Point X="-29.762919093018" Y="0.744060895538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.088709421561" Y="-2.598576121434" />
                  <Point X="-29.133077370232" Y="-2.406397422176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.399526075731" Y="-1.25228128257" />
                  <Point X="-29.552002320144" Y="-0.591834108493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.494148949764" Y="-1.264738677389" />
                  <Point X="-29.643821202796" Y="-0.616436924328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.588771823796" Y="-1.277196072208" />
                  <Point X="-29.735640085448" Y="-0.641039740164" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.378435546875" Y="-4.097009277344" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521543945312" />
                  <Point X="-24.596998046875" Y="-3.4331953125" />
                  <Point X="-24.699408203125" Y="-3.285643554688" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.82055078125" Y="-3.236950439453" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.10355078125" Y="-3.217215820312" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285644042969" />
                  <Point X="-25.34959765625" Y="-3.373992431641" />
                  <Point X="-25.452005859375" Y="-3.521544433594" />
                  <Point X="-25.4592265625" Y="-3.537112060547" />
                  <Point X="-25.709783203125" Y="-4.472204589844" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-25.91423828125" Y="-4.974169921875" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.209529296875" Y="-4.909947265625" />
                  <Point X="-26.351587890625" Y="-4.873396484375" />
                  <Point X="-26.327208984375" Y="-4.688213378906" />
                  <Point X="-26.3091484375" Y="-4.5510390625" />
                  <Point X="-26.309681640625" Y="-4.5347578125" />
                  <Point X="-26.3323515625" Y="-4.420795898438" />
                  <Point X="-26.3702109375" Y="-4.230466308594" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.473642578125" Y="-4.126016113281" />
                  <Point X="-26.61954296875" Y="-3.998064208984" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.765185546875" Y="-3.978163330078" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791015625" />
                  <Point X="-27.086490234375" Y="-4.038345458984" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.40040234375" Y="-4.3406171875" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.583185546875" Y="-4.336427734375" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-28.00716015625" Y="-4.051093994141" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.7848359375" Y="-3.112020019531" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597591796875" />
                  <Point X="-27.51398046875" Y="-2.568762207031" />
                  <Point X="-27.53132421875" Y="-2.551418457031" />
                  <Point X="-27.560154296875" Y="-2.537198974609" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.398181640625" Y="-3.009102050781" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-28.946296875" Y="-3.130129150391" />
                  <Point X="-29.161697265625" Y="-2.847136962891" />
                  <Point X="-29.27019140625" Y="-2.665208984375" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-28.651599609375" Y="-1.797457763672" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.396018676758" />
                  <Point X="-28.138115234375" Y="-1.366264526367" />
                  <Point X="-28.140326171875" Y="-1.334594848633" />
                  <Point X="-28.161158203125" Y="-1.310639282227" />
                  <Point X="-28.187640625" Y="-1.295052734375" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-29.23765625" Y="-1.422610229492" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.84314453125" Y="-1.341014770508" />
                  <Point X="-29.927392578125" Y="-1.011187561035" />
                  <Point X="-29.956095703125" Y="-0.810499389648" />
                  <Point X="-29.99839453125" Y="-0.514742248535" />
                  <Point X="-29.11315625" Y="-0.277543029785" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.531501953125" Y="-0.114211395264" />
                  <Point X="-28.514142578125" Y="-0.10216317749" />
                  <Point X="-28.502322265625" Y="-0.090644317627" />
                  <Point X="-28.491431640625" Y="-0.06474181366" />
                  <Point X="-28.483400390625" Y="-0.031280038834" />
                  <Point X="-28.489111328125" Y="-0.005298885345" />
                  <Point X="-28.502322265625" Y="0.02808423996" />
                  <Point X="-28.524537109375" Y="0.046817058563" />
                  <Point X="-28.541896484375" Y="0.058865276337" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.48553515625" Y="0.314761627197" />
                  <Point X="-29.998185546875" Y="0.452126098633" />
                  <Point X="-29.97194140625" Y="0.629491882324" />
                  <Point X="-29.91764453125" Y="0.996414916992" />
                  <Point X="-29.85986328125" Y="1.209644287109" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.151599609375" Y="1.446422119141" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.70869921875" Y="1.403119995117" />
                  <Point X="-28.67027734375" Y="1.415234130859" />
                  <Point X="-28.651533203125" Y="1.426056274414" />
                  <Point X="-28.639119140625" Y="1.443786499023" />
                  <Point X="-28.629888671875" Y="1.466071777344" />
                  <Point X="-28.61447265625" Y="1.503290771484" />
                  <Point X="-28.610712890625" Y="1.524606567383" />
                  <Point X="-28.61631640625" Y="1.545513427734" />
                  <Point X="-28.627453125" Y="1.566909423828" />
                  <Point X="-28.646056640625" Y="1.602643188477" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-29.1923125" Y="2.027704101563" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.37098828125" Y="2.425556884766" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-29.00695703125" Y="2.983738769531" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.403802734375" Y="3.068189941406" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.106474609375" Y="2.917631591797" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927404296875" />
                  <Point X="-27.990509765625" Y="2.950146240234" />
                  <Point X="-27.95252734375" Y="2.988127685547" />
                  <Point X="-27.9408984375" Y="3.006382080078" />
                  <Point X="-27.938072265625" Y="3.027841308594" />
                  <Point X="-27.940875" Y="3.059881103516" />
                  <Point X="-27.945556640625" Y="3.113390869141" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.18796484375" Y="3.542620117188" />
                  <Point X="-28.30727734375" Y="3.749276611328" />
                  <Point X="-28.120326171875" Y="3.892611328125" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.51181640625" Y="4.308258300781" />
                  <Point X="-27.141548828125" Y="4.513971679688" />
                  <Point X="-27.042720703125" Y="4.385176269531" />
                  <Point X="-26.967826171875" Y="4.287572753906" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.9155859375" Y="4.255097167969" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.218491699219" />
                  <Point X="-26.81380859375" Y="4.22225" />
                  <Point X="-26.77666796875" Y="4.237635253906" />
                  <Point X="-26.714634765625" Y="4.263329589844" />
                  <Point X="-26.696904296875" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.294487792969" />
                  <Point X="-26.673994140625" Y="4.332829589844" />
                  <Point X="-26.653802734375" Y="4.396864257812" />
                  <Point X="-26.651916015625" Y="4.418426269531" />
                  <Point X="-26.678736328125" Y="4.62213671875" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.443779296875" Y="4.769930664062" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.675857421875" Y="4.937498046875" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.114513671875" Y="4.58100390625" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.824673828125" Y="4.762001953125" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.55513671875" Y="4.969063476562" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.898017578125" Y="4.867193847656" />
                  <Point X="-23.491544921875" Y="4.76905859375" />
                  <Point X="-23.334126953125" Y="4.711961425781" />
                  <Point X="-23.06896484375" Y="4.615785644531" />
                  <Point X="-22.91679296875" Y="4.544620117188" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.51429296875" Y="4.339487304688" />
                  <Point X="-22.26747265625" Y="4.195688964844" />
                  <Point X="-22.128830078125" Y="4.097095214844" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.447451171875" Y="3.062518798828" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491515380859" />
                  <Point X="-22.7831875" Y="2.461446777344" />
                  <Point X="-22.7966171875" Y="2.411229003906" />
                  <Point X="-22.797955078125" Y="2.392327880859" />
                  <Point X="-22.794818359375" Y="2.366327148438" />
                  <Point X="-22.789583984375" Y="2.322903076172" />
                  <Point X="-22.781318359375" Y="2.3008125" />
                  <Point X="-22.765228515625" Y="2.277102539062" />
                  <Point X="-22.738359375" Y="2.23750390625" />
                  <Point X="-22.72505859375" Y="2.224203369141" />
                  <Point X="-22.701349609375" Y="2.208114990234" />
                  <Point X="-22.66175" Y="2.181245849609" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.613662109375" Y="2.169844970703" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.5513359375" Y="2.165946289062" />
                  <Point X="-22.521267578125" Y="2.173987060547" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.52550390625" Y="2.731352050781" />
                  <Point X="-21.005751953125" Y="3.031430664062" />
                  <Point X="-20.943810546875" Y="2.945347412109" />
                  <Point X="-20.79740234375" Y="2.741874267578" />
                  <Point X="-20.720115234375" Y="2.614153808594" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.28652734375" Y="1.9190859375" />
                  <Point X="-21.7113828125" Y="1.593082641602" />
                  <Point X="-21.72062890625" Y="1.583832519531" />
                  <Point X="-21.74226953125" Y="1.555601074219" />
                  <Point X="-21.77841015625" Y="1.508451171875" />
                  <Point X="-21.78687890625" Y="1.4915" />
                  <Point X="-21.79494140625" Y="1.462675537109" />
                  <Point X="-21.808404296875" Y="1.414535400391" />
                  <Point X="-21.809220703125" Y="1.390966064453" />
                  <Point X="-21.8026015625" Y="1.358895141602" />
                  <Point X="-21.79155078125" Y="1.305333374023" />
                  <Point X="-21.784353515625" Y="1.287955444336" />
                  <Point X="-21.76635546875" Y="1.260598754883" />
                  <Point X="-21.736296875" Y="1.21491003418" />
                  <Point X="-21.719052734375" Y="1.198819824219" />
                  <Point X="-21.692970703125" Y="1.184137817383" />
                  <Point X="-21.64941015625" Y="1.159617553711" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.59616796875" Y="1.148958862305" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.637470703125" Y="1.257911621094" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.12369140625" Y="1.209674194336" />
                  <Point X="-20.060806640625" Y="0.951366882324" />
                  <Point X="-20.036453125" Y="0.794943847656" />
                  <Point X="-20.002140625" Y="0.574556396484" />
                  <Point X="-20.7719375" Y="0.368289672852" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819351196" />
                  <Point X="-21.30555859375" Y="0.21279309082" />
                  <Point X="-21.363421875" Y="0.179346832275" />
                  <Point X="-21.377734375" Y="0.166927001953" />
                  <Point X="-21.398521484375" Y="0.14043838501" />
                  <Point X="-21.433240234375" Y="0.096199325562" />
                  <Point X="-21.443013671875" Y="0.074734802246" />
                  <Point X="-21.449943359375" Y="0.038552581787" />
                  <Point X="-21.461515625" Y="-0.02187575531" />
                  <Point X="-21.461515625" Y="-0.040684322357" />
                  <Point X="-21.4545859375" Y="-0.076866546631" />
                  <Point X="-21.443013671875" Y="-0.137294876099" />
                  <Point X="-21.433240234375" Y="-0.158759399414" />
                  <Point X="-21.412453125" Y="-0.185248016357" />
                  <Point X="-21.377734375" Y="-0.229487075806" />
                  <Point X="-21.363421875" Y="-0.241906906128" />
                  <Point X="-21.328775390625" Y="-0.261933166504" />
                  <Point X="-21.270912109375" Y="-0.295379425049" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.4456640625" Y="-0.518274780273" />
                  <Point X="-20.001931640625" Y="-0.637172424316" />
                  <Point X="-20.016458984375" Y="-0.733528869629" />
                  <Point X="-20.051568359375" Y="-0.966412780762" />
                  <Point X="-20.082771484375" Y="-1.103146240234" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-21.023357421875" Y="-1.171967041016" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.67316015625" Y="-1.11312097168" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.8145546875" Y="-1.154697387695" />
                  <Point X="-21.85565625" Y="-1.20412902832" />
                  <Point X="-21.924298828125" Y="-1.286685424805" />
                  <Point X="-21.935640625" Y="-1.314070800781" />
                  <Point X="-21.94153125" Y="-1.378086914062" />
                  <Point X="-21.951369140625" Y="-1.485001220703" />
                  <Point X="-21.943638671875" Y="-1.516622192383" />
                  <Point X="-21.9060078125" Y="-1.575155395508" />
                  <Point X="-21.843158203125" Y="-1.672912719727" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-21.076912109375" Y="-2.264585205078" />
                  <Point X="-20.66092578125" Y="-2.583783935547" />
                  <Point X="-20.6968984375" Y="-2.641994140625" />
                  <Point X="-20.795865234375" Y="-2.802138183594" />
                  <Point X="-20.86040234375" Y="-2.893837158203" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.743638671875" Y="-2.549568603516" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.343587890625" Y="-2.238697021484" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.578154296875" Y="-2.254628662109" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.33468359375" />
                  <Point X="-22.746783203125" Y="-2.401916015625" />
                  <Point X="-22.80587890625" Y="-2.514201660156" />
                  <Point X="-22.8108359375" Y="-2.546374511719" />
                  <Point X="-22.79621875" Y="-2.627303710938" />
                  <Point X="-22.77180859375" Y="-2.76246484375" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.280984375" Y="-3.618489257812" />
                  <Point X="-22.01332421875" Y="-4.082088867188" />
                  <Point X="-22.047263671875" Y="-4.106331054688" />
                  <Point X="-22.164685546875" Y="-4.190202148438" />
                  <Point X="-22.23685546875" Y="-4.236916015625" />
                  <Point X="-22.320224609375" Y="-4.29087890625" />
                  <Point X="-22.93305859375" Y="-3.492217773438" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.409267578125" Y="-2.929151611328" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.661490234375" Y="-2.84375" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.902072265625" Y="-2.924555419922" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045986083984" />
                  <Point X="-24.051697265625" Y="-3.138711669922" />
                  <Point X="-24.085357421875" Y="-3.293573974609" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-23.948646484375" Y="-4.354554199219" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.894548828125" Y="-4.938893066406" />
                  <Point X="-24.005650390625" Y="-4.96324609375" />
                  <Point X="-24.072310546875" Y="-4.975356445312" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#154" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.072373132609" Y="4.625224695354" Z="0.95" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.95" />
                  <Point X="-0.687827167114" Y="5.018439361996" Z="0.95" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.95" />
                  <Point X="-1.463434592406" Y="4.849354145221" Z="0.95" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.95" />
                  <Point X="-1.734464829951" Y="4.646890816506" Z="0.95" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.95" />
                  <Point X="-1.727836057336" Y="4.379145697495" Z="0.95" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.95" />
                  <Point X="-1.80195686037" Y="4.315109506691" Z="0.95" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.95" />
                  <Point X="-1.898655254551" Y="4.330727797472" Z="0.95" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.95" />
                  <Point X="-2.009208819016" Y="4.446894628308" Z="0.95" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.95" />
                  <Point X="-2.542256355709" Y="4.383246003837" Z="0.95" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.95" />
                  <Point X="-3.156904143837" Y="3.963571320746" Z="0.95" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.95" />
                  <Point X="-3.237422677147" Y="3.548900191118" Z="0.95" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.95" />
                  <Point X="-2.996843140853" Y="3.086803136521" Z="0.95" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.95" />
                  <Point X="-3.032021631993" Y="3.016781935893" Z="0.95" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.95" />
                  <Point X="-3.108273329085" Y="2.998721558319" Z="0.95" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.95" />
                  <Point X="-3.384959157179" Y="3.142771271599" Z="0.95" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.95" />
                  <Point X="-4.052577615423" Y="3.045721167049" Z="0.95" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.95" />
                  <Point X="-4.420326774766" Y="2.482042096666" Z="0.95" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.95" />
                  <Point X="-4.228906841898" Y="2.019316417288" Z="0.95" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.95" />
                  <Point X="-3.677960478325" Y="1.575100368889" Z="0.95" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.95" />
                  <Point X="-3.682239040352" Y="1.516485397167" Z="0.95" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.95" />
                  <Point X="-3.729891004856" Y="1.482085996979" Z="0.95" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.95" />
                  <Point X="-4.151230960485" Y="1.527274333781" Z="0.95" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.95" />
                  <Point X="-4.914280898113" Y="1.254001521842" Z="0.95" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.95" />
                  <Point X="-5.027558317573" Y="0.668091012806" Z="0.95" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.95" />
                  <Point X="-4.504633192483" Y="0.29774530121" Z="0.95" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.95" />
                  <Point X="-3.559201748797" Y="0.037020903848" Z="0.95" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.95" />
                  <Point X="-3.543021431034" Y="0.011163172741" Z="0.95" />
                  <Point X="-3.539556741714" Y="0" Z="0.95" />
                  <Point X="-3.545343170101" Y="-0.01864377832" Z="0.95" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.95" />
                  <Point X="-3.566166846326" Y="-0.041855079286" Z="0.95" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.95" />
                  <Point X="-4.13225459629" Y="-0.197966755546" Z="0.95" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.95" />
                  <Point X="-5.011748613306" Y="-0.786298351659" Z="0.95" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.95" />
                  <Point X="-4.897746983604" Y="-1.322108856122" Z="0.95" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.95" />
                  <Point X="-4.237287389832" Y="-1.44090244239" Z="0.95" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.95" />
                  <Point X="-3.202594449962" Y="-1.316612415281" Z="0.95" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.95" />
                  <Point X="-3.197495346885" Y="-1.341056312722" Z="0.95" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.95" />
                  <Point X="-3.688194986049" Y="-1.726510142878" Z="0.95" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.95" />
                  <Point X="-4.319292375137" Y="-2.659538688545" Z="0.95" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.95" />
                  <Point X="-3.992239291881" Y="-3.129132270803" Z="0.95" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.95" />
                  <Point X="-3.379338501375" Y="-3.021123380545" Z="0.95" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.95" />
                  <Point X="-2.561988267616" Y="-2.566342201516" Z="0.95" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.95" />
                  <Point X="-2.834293632624" Y="-3.055739881117" Z="0.95" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.95" />
                  <Point X="-3.043821185752" Y="-4.059431093643" Z="0.95" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.95" />
                  <Point X="-2.61566415568" Y="-4.347658522805" Z="0.95" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.95" />
                  <Point X="-2.366890854811" Y="-4.339774975048" Z="0.95" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.95" />
                  <Point X="-2.064868432468" Y="-4.04863882847" Z="0.95" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.95" />
                  <Point X="-1.774612862059" Y="-3.99677642315" Z="0.95" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.95" />
                  <Point X="-1.512765849674" Y="-4.132327851361" Z="0.95" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.95" />
                  <Point X="-1.387547016534" Y="-4.399270186363" Z="0.95" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.95" />
                  <Point X="-1.382937877881" Y="-4.650406497854" Z="0.95" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.95" />
                  <Point X="-1.228145102589" Y="-4.927090293157" Z="0.95" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.95" />
                  <Point X="-0.929923094591" Y="-4.991973857108" Z="0.95" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.95" />
                  <Point X="-0.667644072766" Y="-4.453865692296" Z="0.95" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.95" />
                  <Point X="-0.314677682586" Y="-3.371221093716" Z="0.95" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.95" />
                  <Point X="-0.094886966551" Y="-3.2336887812" Z="0.95" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.95" />
                  <Point X="0.15847211281" Y="-3.253423264088" Z="0.95" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.95" />
                  <Point X="0.355768176847" Y="-3.430424662052" Z="0.95" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.95" />
                  <Point X="0.567110787635" Y="-4.07867033752" Z="0.95" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.95" />
                  <Point X="0.930469209204" Y="-4.993271102605" Z="0.95" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.95" />
                  <Point X="1.109998619471" Y="-4.956453570892" Z="0.95" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.95" />
                  <Point X="1.094769170485" Y="-4.316747825099" Z="0.95" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.95" />
                  <Point X="0.991005738275" Y="-3.11805150044" Z="0.95" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.95" />
                  <Point X="1.123737006955" Y="-2.931721937239" Z="0.95" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.95" />
                  <Point X="1.336935813286" Y="-2.862259521662" Z="0.95" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.95" />
                  <Point X="1.557535811065" Y="-2.939929615357" Z="0.95" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.95" />
                  <Point X="2.02111748354" Y="-3.491375610757" Z="0.95" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.95" />
                  <Point X="2.784158026163" Y="-4.247610292375" Z="0.95" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.95" />
                  <Point X="2.975639607649" Y="-4.11573790583" Z="0.95" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.95" />
                  <Point X="2.756159637975" Y="-3.562209096578" Z="0.95" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.95" />
                  <Point X="2.24682688371" Y="-2.587137455655" Z="0.95" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.95" />
                  <Point X="2.291306291637" Y="-2.393922510208" Z="0.95" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.95" />
                  <Point X="2.438975868164" Y="-2.26759501833" Z="0.95" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.95" />
                  <Point X="2.641369347858" Y="-2.256621125781" Z="0.95" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.95" />
                  <Point X="3.225204500529" Y="-2.561589862299" Z="0.95" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.95" />
                  <Point X="4.174329075985" Y="-2.891334409567" Z="0.95" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.95" />
                  <Point X="4.339478416168" Y="-2.636999209956" Z="0.95" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.95" />
                  <Point X="3.947368126884" Y="-2.193637466852" Z="0.95" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.95" />
                  <Point X="3.129893968534" Y="-1.516835763929" Z="0.95" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.95" />
                  <Point X="3.102100717235" Y="-1.351388309018" Z="0.95" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.95" />
                  <Point X="3.176634645127" Y="-1.204815786407" Z="0.95" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.95" />
                  <Point X="3.33130115772" Y="-1.130700223706" Z="0.95" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.95" />
                  <Point X="3.963959838658" Y="-1.190259315328" Z="0.95" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.95" />
                  <Point X="4.959817406447" Y="-1.082990241349" Z="0.95" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.95" />
                  <Point X="5.026826241412" Y="-0.709702633977" Z="0.95" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.95" />
                  <Point X="4.561121261384" Y="-0.438698486297" Z="0.95" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.95" />
                  <Point X="3.690089161432" Y="-0.187364474603" Z="0.95" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.95" />
                  <Point X="3.620724591754" Y="-0.123099160149" Z="0.95" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.95" />
                  <Point X="3.588364016063" Y="-0.036182165931" Z="0.95" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.95" />
                  <Point X="3.593007434361" Y="0.060428365289" Z="0.95" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.95" />
                  <Point X="3.634654846647" Y="0.140849578426" Z="0.95" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.95" />
                  <Point X="3.71330625292" Y="0.200784369639" Z="0.95" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.95" />
                  <Point X="4.234846555923" Y="0.351273446472" Z="0.95" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.95" />
                  <Point X="5.006794309027" Y="0.833915820735" Z="0.95" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.95" />
                  <Point X="4.918734180038" Y="1.252781243288" Z="0.95" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.95" />
                  <Point X="4.349847944739" Y="1.338763881496" Z="0.95" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.95" />
                  <Point X="3.404225320156" Y="1.229807919436" Z="0.95" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.95" />
                  <Point X="3.325400603209" Y="1.258989095253" Z="0.95" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.95" />
                  <Point X="3.269259307726" Y="1.319359761055" Z="0.95" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.95" />
                  <Point X="3.240209338185" Y="1.400278371993" Z="0.95" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.95" />
                  <Point X="3.247054926584" Y="1.480489285773" Z="0.95" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.95" />
                  <Point X="3.2912578249" Y="1.556463579626" Z="0.95" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.95" />
                  <Point X="3.737753968014" Y="1.9106984178" Z="0.95" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.95" />
                  <Point X="4.316505669415" Y="2.671319970297" Z="0.95" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.95" />
                  <Point X="4.090617972073" Y="3.005830662892" Z="0.95" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.95" />
                  <Point X="3.443340127324" Y="2.805933393701" Z="0.95" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.95" />
                  <Point X="2.459660126558" Y="2.253569875626" Z="0.95" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.95" />
                  <Point X="2.386167494724" Y="2.25076532432" Z="0.95" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.95" />
                  <Point X="2.320568183699" Y="2.280769795192" Z="0.95" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.95" />
                  <Point X="2.269988805043" Y="2.336456676683" Z="0.95" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.95" />
                  <Point X="2.248664378438" Y="2.403590949109" Z="0.95" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.95" />
                  <Point X="2.258958067032" Y="2.479809449324" Z="0.95" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.95" />
                  <Point X="2.589691959159" Y="3.068798954631" Z="0.95" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.95" />
                  <Point X="2.89398946401" Y="4.169122119417" Z="0.95" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.95" />
                  <Point X="2.504720787903" Y="4.413970087498" Z="0.95" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.95" />
                  <Point X="2.09823116467" Y="4.621192297922" Z="0.95" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.95" />
                  <Point X="1.676765786751" Y="4.790245600089" Z="0.95" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.95" />
                  <Point X="1.107558973165" Y="4.947077342532" Z="0.95" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.95" />
                  <Point X="0.443913236643" Y="5.050071327311" Z="0.95" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.95" />
                  <Point X="0.120871428944" Y="4.806222810597" Z="0.95" />
                  <Point X="0" Y="4.355124473572" Z="0.95" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>