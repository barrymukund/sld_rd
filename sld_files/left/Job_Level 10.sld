<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#135" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="873" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000473632812" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766442382812" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.365525390625" Y="-3.778132568359" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497140136719" />
                  <Point X="-24.45763671875" Y="-3.467375732422" />
                  <Point X="-24.4867265625" Y="-3.425464355469" />
                  <Point X="-24.621365234375" Y="-3.231475341797" />
                  <Point X="-24.643248046875" Y="-3.209020751953" />
                  <Point X="-24.66950390625" Y="-3.189776855469" />
                  <Point X="-24.69750390625" Y="-3.175669189453" />
                  <Point X="-24.742517578125" Y="-3.161698730469" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097036132812" />
                  <Point X="-25.0818359375" Y="-3.111006347656" />
                  <Point X="-25.29018359375" Y="-3.175669433594" />
                  <Point X="-25.31818359375" Y="-3.189776855469" />
                  <Point X="-25.344439453125" Y="-3.209020751953" />
                  <Point X="-25.36632421875" Y="-3.231477539062" />
                  <Point X="-25.395412109375" Y="-3.273388671875" />
                  <Point X="-25.53005078125" Y="-3.467377929688" />
                  <Point X="-25.5381875" Y="-3.481572021484" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.88040234375" Y="-4.74190625" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.0793359375" Y="-4.845351074219" />
                  <Point X="-26.1276484375" Y="-4.832920410156" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.223529296875" Y="-4.628512695312" />
                  <Point X="-26.2149609375" Y="-4.563438964844" />
                  <Point X="-26.21419921875" Y="-4.547930175781" />
                  <Point X="-26.2165078125" Y="-4.516223144531" />
                  <Point X="-26.22726171875" Y="-4.462161132812" />
                  <Point X="-26.277037109375" Y="-4.211931640625" />
                  <Point X="-26.287939453125" Y="-4.182964355469" />
                  <Point X="-26.30401171875" Y="-4.155127441406" />
                  <Point X="-26.32364453125" Y="-4.131205078125" />
                  <Point X="-26.3650859375" Y="-4.094860839844" />
                  <Point X="-26.556904296875" Y="-3.926640136719" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.69803125" Y="-3.887361083984" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708740234" />
                  <Point X="-27.014466796875" Y="-3.882028564453" />
                  <Point X="-27.042658203125" Y="-3.894801513672" />
                  <Point X="-27.088490234375" Y="-3.925425292969" />
                  <Point X="-27.300623046875" Y="-4.067169433594" />
                  <Point X="-27.31278515625" Y="-4.076821533203" />
                  <Point X="-27.3351015625" Y="-4.099461425781" />
                  <Point X="-27.4801484375" Y="-4.288489746094" />
                  <Point X="-27.80170703125" Y="-4.089388427734" />
                  <Point X="-27.868603515625" Y="-4.037881103516" />
                  <Point X="-28.10472265625" Y="-3.856077148438" />
                  <Point X="-27.556021484375" Y="-2.905701171875" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.411279296875" Y="-2.646968994141" />
                  <Point X="-27.405865234375" Y="-2.624443603516" />
                  <Point X="-27.406064453125" Y="-2.601277587891" />
                  <Point X="-27.41020703125" Y="-2.569796386719" />
                  <Point X="-27.416625" Y="-2.545839355469" />
                  <Point X="-27.429025390625" Y="-2.524360107422" />
                  <Point X="-27.44169140625" Y="-2.507852539062" />
                  <Point X="-27.4498828125" Y="-2.498509521484" />
                  <Point X="-27.4641484375" Y="-2.484243164062" />
                  <Point X="-27.4893046875" Y="-2.466215087891" />
                  <Point X="-27.5181328125" Y="-2.451997314453" />
                  <Point X="-27.547751953125" Y="-2.44301171875" />
                  <Point X="-27.5786875" Y="-2.444023925781" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.6995" Y="-3.073372070313" />
                  <Point X="-28.8180234375" Y="-3.141801513672" />
                  <Point X="-29.082859375" Y="-2.793862548828" />
                  <Point X="-29.13081640625" Y="-2.713446533203" />
                  <Point X="-29.306142578125" Y="-2.419450439453" />
                  <Point X="-28.3373671875" Y="-1.676082519531" />
                  <Point X="-28.105955078125" Y="-1.498513305664" />
                  <Point X="-28.083064453125" Y="-1.475880615234" />
                  <Point X="-28.0642421875" Y="-1.446142700195" />
                  <Point X="-28.05616015625" Y="-1.425789916992" />
                  <Point X="-28.05248828125" Y="-1.414548706055" />
                  <Point X="-28.04615234375" Y="-1.390086425781" />
                  <Point X="-28.042037109375" Y="-1.358610107422" />
                  <Point X="-28.042734375" Y="-1.335737060547" />
                  <Point X="-28.0488828125" Y="-1.313694458008" />
                  <Point X="-28.060888671875" Y="-1.284711791992" />
                  <Point X="-28.07329296875" Y="-1.263228881836" />
                  <Point X="-28.090837890625" Y="-1.245690063477" />
                  <Point X="-28.108044921875" Y="-1.232491455078" />
                  <Point X="-28.11767578125" Y="-1.225998291016" />
                  <Point X="-28.139453125" Y="-1.213181030273" />
                  <Point X="-28.168716796875" Y="-1.201956787109" />
                  <Point X="-28.200603515625" Y="-1.195474975586" />
                  <Point X="-28.231927734375" Y="-1.194383911133" />
                  <Point X="-29.570478515625" Y="-1.370607421875" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.834076171875" Y="-0.992651916504" />
                  <Point X="-29.846763671875" Y="-0.903944763184" />
                  <Point X="-29.892421875" Y="-0.584698425293" />
                  <Point X="-28.79648828125" Y="-0.291043426514" />
                  <Point X="-28.532875" Y="-0.220408355713" />
                  <Point X="-28.51369140625" Y="-0.212960464478" />
                  <Point X="-28.49319140625" Y="-0.202317977905" />
                  <Point X="-28.482794921875" Y="-0.196047363281" />
                  <Point X="-28.45997265625" Y="-0.180207427979" />
                  <Point X="-28.43601171875" Y="-0.158672317505" />
                  <Point X="-28.4157734375" Y="-0.129816955566" />
                  <Point X="-28.40670703125" Y="-0.109787147522" />
                  <Point X="-28.4025234375" Y="-0.098771713257" />
                  <Point X="-28.394916015625" Y="-0.074260444641" />
                  <Point X="-28.38947265625" Y="-0.045523181915" />
                  <Point X="-28.390126953125" Y="-0.013257699966" />
                  <Point X="-28.394015625" Y="0.006863182068" />
                  <Point X="-28.39655859375" Y="0.016995950699" />
                  <Point X="-28.404166015625" Y="0.041507221222" />
                  <Point X="-28.417482421875" Y="0.070832824707" />
                  <Point X="-28.439087890625" Y="0.098981903076" />
                  <Point X="-28.455837890625" Y="0.113921875" />
                  <Point X="-28.464904296875" Y="0.121069534302" />
                  <Point X="-28.4877265625" Y="0.136909469604" />
                  <Point X="-28.501923828125" Y="0.145047409058" />
                  <Point X="-28.532875" Y="0.157848358154" />
                  <Point X="-29.75302734375" Y="0.484786987305" />
                  <Point X="-29.89181640625" Y="0.521975524902" />
                  <Point X="-29.82448828125" Y="0.976967041016" />
                  <Point X="-29.7989453125" Y="1.071229980469" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-28.95462890625" Y="1.324670532227" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744984375" Y="1.299341674805" />
                  <Point X="-28.723423828125" Y="1.301228149414" />
                  <Point X="-28.7031328125" Y="1.305264282227" />
                  <Point X="-28.692220703125" Y="1.308705200195" />
                  <Point X="-28.64170703125" Y="1.324631835938" />
                  <Point X="-28.622775390625" Y="1.332962402344" />
                  <Point X="-28.60403125" Y="1.343784545898" />
                  <Point X="-28.587349609375" Y="1.356016479492" />
                  <Point X="-28.573712890625" Y="1.371568969727" />
                  <Point X="-28.561298828125" Y="1.389299316406" />
                  <Point X="-28.551349609375" Y="1.407432495117" />
                  <Point X="-28.546970703125" Y="1.418004394531" />
                  <Point X="-28.526703125" Y="1.466936767578" />
                  <Point X="-28.520916015625" Y="1.486788574219" />
                  <Point X="-28.51715625" Y="1.508104003906" />
                  <Point X="-28.515802734375" Y="1.528750610352" />
                  <Point X="-28.518951171875" Y="1.549200805664" />
                  <Point X="-28.5245546875" Y="1.570106933594" />
                  <Point X="-28.532048828125" Y="1.589374755859" />
                  <Point X="-28.53733203125" Y="1.599524902344" />
                  <Point X="-28.5617890625" Y="1.646504638672" />
                  <Point X="-28.573283203125" Y="1.663707763672" />
                  <Point X="-28.5871953125" Y="1.680287231445" />
                  <Point X="-28.60213671875" Y="1.694590209961" />
                  <Point X="-29.302017578125" Y="2.231628662109" />
                  <Point X="-29.351859375" Y="2.269874267578" />
                  <Point X="-29.0811484375" Y="2.733664306641" />
                  <Point X="-29.013498046875" Y="2.820620361328" />
                  <Point X="-28.75050390625" Y="3.158662109375" />
                  <Point X="-28.322712890625" Y="2.911676513672" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.13159375" Y="2.824466552734" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.999013671875" Y="2.826504638672" />
                  <Point X="-27.980462890625" Y="2.835653564453" />
                  <Point X="-27.962208984375" Y="2.847282714844" />
                  <Point X="-27.946078125" Y="2.860227050781" />
                  <Point X="-27.9352890625" Y="2.871015380859" />
                  <Point X="-27.885353515625" Y="2.920950439453" />
                  <Point X="-27.872404296875" Y="2.937085205078" />
                  <Point X="-27.860775390625" Y="2.955339355469" />
                  <Point X="-27.85162890625" Y="2.973888427734" />
                  <Point X="-27.8467109375" Y="2.993977539062" />
                  <Point X="-27.843884765625" Y="3.015436279297" />
                  <Point X="-27.84343359375" Y="3.036122558594" />
                  <Point X="-27.844763671875" Y="3.051321777344" />
                  <Point X="-27.85091796875" Y="3.121672119141" />
                  <Point X="-27.854955078125" Y="3.141962158203" />
                  <Point X="-27.86146484375" Y="3.162604736328" />
                  <Point X="-27.869794921875" Y="3.181532958984" />
                  <Point X="-28.17993359375" Y="3.718709472656" />
                  <Point X="-28.1790859375" Y="3.727852783203" />
                  <Point X="-27.700626953125" Y="4.094681396484" />
                  <Point X="-27.594072265625" Y="4.153881835938" />
                  <Point X="-27.167037109375" Y="4.391133300781" />
                  <Point X="-27.078724609375" Y="4.276041992188" />
                  <Point X="-27.0431953125" Y="4.229740234375" />
                  <Point X="-27.028890625" Y="4.214797851563" />
                  <Point X="-27.012310546875" Y="4.200885742188" />
                  <Point X="-26.99511328125" Y="4.189395019531" />
                  <Point X="-26.978197265625" Y="4.180588867188" />
                  <Point X="-26.899896484375" Y="4.139828125" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.859708984375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.123583007812" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.77744921875" Y="4.134483398438" />
                  <Point X="-26.759830078125" Y="4.141782226563" />
                  <Point X="-26.678275390625" Y="4.175562988281" />
                  <Point X="-26.660146484375" Y="4.185508789062" />
                  <Point X="-26.642416015625" Y="4.197922851562" />
                  <Point X="-26.626859375" Y="4.211563964844" />
                  <Point X="-26.614626953125" Y="4.228249511719" />
                  <Point X="-26.603806640625" Y="4.246993652344" />
                  <Point X="-26.595478515625" Y="4.265923828125" />
                  <Point X="-26.589744140625" Y="4.284112792969" />
                  <Point X="-26.56319921875" Y="4.368300292969" />
                  <Point X="-26.5591640625" Y="4.388583496094" />
                  <Point X="-26.55727734375" Y="4.410145019531" />
                  <Point X="-26.557728515625" Y="4.430826171875" />
                  <Point X="-26.584201171875" Y="4.6318984375" />
                  <Point X="-26.56903125" Y="4.636151367188" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.82046875" Y="4.824925292969" />
                  <Point X="-25.2947109375" Y="4.886457519531" />
                  <Point X="-25.168236328125" Y="4.414447753906" />
                  <Point X="-25.133904296875" Y="4.286315917969" />
                  <Point X="-25.12112890625" Y="4.258122558594" />
                  <Point X="-25.10326953125" Y="4.231395507813" />
                  <Point X="-25.08211328125" Y="4.208808105469" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183885742188" />
                  <Point X="-24.932869140625" Y="4.194217773438" />
                  <Point X="-24.905576171875" Y="4.208806640625" />
                  <Point X="-24.88441796875" Y="4.23139453125" />
                  <Point X="-24.86655859375" Y="4.25812109375" />
                  <Point X="-24.853783203125" Y="4.286314941406" />
                  <Point X="-24.69487109375" Y="4.879381835938" />
                  <Point X="-24.692578125" Y="4.887937011719" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-24.049087890625" Y="4.8059375" />
                  <Point X="-23.51896875" Y="4.677949707031" />
                  <Point X="-23.45062890625" Y="4.653163085938" />
                  <Point X="-23.105357421875" Y="4.527930175781" />
                  <Point X="-23.038087890625" Y="4.496470214844" />
                  <Point X="-22.705431640625" Y="4.340897949219" />
                  <Point X="-22.640408203125" Y="4.303016113281" />
                  <Point X="-22.319009765625" Y="4.115767578125" />
                  <Point X="-22.257728515625" Y="4.072188720703" />
                  <Point X="-22.056736328125" Y="3.929254638672" />
                  <Point X="-22.6993359375" Y="2.816242431641" />
                  <Point X="-22.852416015625" Y="2.55109765625" />
                  <Point X="-22.860234375" Y="2.533744628906" />
                  <Point X="-22.86905078125" Y="2.507398193359" />
                  <Point X="-22.870736328125" Y="2.501794433594" />
                  <Point X="-22.888392578125" Y="2.435772216797" />
                  <Point X="-22.891615234375" Y="2.410771728516" />
                  <Point X="-22.89146484375" Y="2.379530273438" />
                  <Point X="-22.890783203125" Y="2.368615722656" />
                  <Point X="-22.883900390625" Y="2.311530273438" />
                  <Point X="-22.87855859375" Y="2.289609619141" />
                  <Point X="-22.87029296875" Y="2.26751953125" />
                  <Point X="-22.859927734375" Y="2.247467529297" />
                  <Point X="-22.852294921875" Y="2.236219726562" />
                  <Point X="-22.81696875" Y="2.184158935547" />
                  <Point X="-22.799998046875" Y="2.165213867188" />
                  <Point X="-22.77544921875" Y="2.144282226562" />
                  <Point X="-22.76715234375" Y="2.137960693359" />
                  <Point X="-22.715091796875" Y="2.102635253906" />
                  <Point X="-22.695046875" Y="2.092272705078" />
                  <Point X="-22.672958984375" Y="2.084006591797" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.638701171875" Y="2.077176269531" />
                  <Point X="-22.581611328125" Y="2.070291992188" />
                  <Point X="-22.55565625" Y="2.070734619141" />
                  <Point X="-22.522490234375" Y="2.07588671875" />
                  <Point X="-22.51253125" Y="2.077985351562" />
                  <Point X="-22.4465078125" Y="2.095640625" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.1842265625" Y="2.81869140625" />
                  <Point X="-21.032673828125" Y="2.906191162109" />
                  <Point X="-20.87671875" Y="2.689451171875" />
                  <Point X="-20.84255859375" Y="2.632999267578" />
                  <Point X="-20.73780078125" Y="2.459884033203" />
                  <Point X="-21.56766796875" Y="1.823103271484" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.783349609375" Y="1.655098144531" />
                  <Point X="-21.802861328125" Y="1.632455566406" />
                  <Point X="-21.80629296875" Y="1.62823449707" />
                  <Point X="-21.85380859375" Y="1.566245849609" />
                  <Point X="-21.866572265625" Y="1.543840698242" />
                  <Point X="-21.878865234375" Y="1.513215942383" />
                  <Point X="-21.88219140625" Y="1.503414794922" />
                  <Point X="-21.899892578125" Y="1.440124267578" />
                  <Point X="-21.90334765625" Y="1.417824951172" />
                  <Point X="-21.9041640625" Y="1.394254150391" />
                  <Point X="-21.902259765625" Y="1.371759765625" />
                  <Point X="-21.899119140625" Y="1.356545776367" />
                  <Point X="-21.88458984375" Y="1.286126953125" />
                  <Point X="-21.875890625" Y="1.26160949707" />
                  <Point X="-21.860154296875" Y="1.231254882812" />
                  <Point X="-21.8551796875" Y="1.222765380859" />
                  <Point X="-21.815662109375" Y="1.162697509766" />
                  <Point X="-21.801109375" Y="1.145452514648" />
                  <Point X="-21.783865234375" Y="1.129361816406" />
                  <Point X="-21.765654296875" Y="1.116034790039" />
                  <Point X="-21.75328125" Y="1.109069824219" />
                  <Point X="-21.69601171875" Y="1.076832519531" />
                  <Point X="-21.67127734375" Y="1.067168579102" />
                  <Point X="-21.636572265625" Y="1.058959594727" />
                  <Point X="-21.62715234375" Y="1.057227539063" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.346001953125" Y="1.200464355469" />
                  <Point X="-20.22316015625" Y="1.21663659668" />
                  <Point X="-20.15405859375" Y="0.932789001465" />
                  <Point X="-20.143294921875" Y="0.86365222168" />
                  <Point X="-20.109134765625" Y="0.644238830566" />
                  <Point X="-21.052443359375" Y="0.391479614258" />
                  <Point X="-21.283419921875" Y="0.329589782715" />
                  <Point X="-21.30191015625" Y="0.322498443604" />
                  <Point X="-21.33042578125" Y="0.307990631104" />
                  <Point X="-21.334888671875" Y="0.305567749023" />
                  <Point X="-21.410962890625" Y="0.261595733643" />
                  <Point X="-21.431697265625" Y="0.245403030396" />
                  <Point X="-21.45587109375" Y="0.22041696167" />
                  <Point X="-21.462330078125" Y="0.213010955811" />
                  <Point X="-21.507974609375" Y="0.154849121094" />
                  <Point X="-21.51969921875" Y="0.135567489624" />
                  <Point X="-21.52947265625" Y="0.114103569031" />
                  <Point X="-21.536318359375" Y="0.092603668213" />
                  <Point X="-21.53960546875" Y="0.075439361572" />
                  <Point X="-21.5548203125" Y="-0.004006833076" />
                  <Point X="-21.55612109375" Y="-0.030520065308" />
                  <Point X="-21.552833984375" Y="-0.066493637085" />
                  <Point X="-21.551533203125" Y="-0.075717788696" />
                  <Point X="-21.536318359375" Y="-0.155163986206" />
                  <Point X="-21.52947265625" Y="-0.176663574219" />
                  <Point X="-21.51969921875" Y="-0.198127502441" />
                  <Point X="-21.507974609375" Y="-0.21740852356" />
                  <Point X="-21.49811328125" Y="-0.229974334717" />
                  <Point X="-21.45246875" Y="-0.288136169434" />
                  <Point X="-21.432880859375" Y="-0.306841918945" />
                  <Point X="-21.4021328125" Y="-0.328762420654" />
                  <Point X="-21.39452734375" Y="-0.333655853271" />
                  <Point X="-21.318453125" Y="-0.37762802124" />
                  <Point X="-21.307291015625" Y="-0.383138305664" />
                  <Point X="-21.283419921875" Y="-0.392149810791" />
                  <Point X="-20.214333984375" Y="-0.678610778809" />
                  <Point X="-20.108525390625" Y="-0.706961914062" />
                  <Point X="-20.1449765625" Y="-0.948732421875" />
                  <Point X="-20.15876953125" Y="-1.00917767334" />
                  <Point X="-20.198822265625" Y="-1.18469921875" />
                  <Point X="-21.30775" Y="-1.038706298828" />
                  <Point X="-21.5756171875" Y="-1.003440917969" />
                  <Point X="-21.59196484375" Y="-1.002710205078" />
                  <Point X="-21.62533984375" Y="-1.005508789063" />
                  <Point X="-21.65759765625" Y="-1.012520141602" />
                  <Point X="-21.806904296875" Y="-1.044972412109" />
                  <Point X="-21.836021484375" Y="-1.056595825195" />
                  <Point X="-21.863849609375" Y="-1.07348815918" />
                  <Point X="-21.8876015625" Y="-1.093959472656" />
                  <Point X="-21.907099609375" Y="-1.117409179688" />
                  <Point X="-21.997345703125" Y="-1.225947631836" />
                  <Point X="-22.012068359375" Y="-1.250334838867" />
                  <Point X="-22.02341015625" Y="-1.277720092773" />
                  <Point X="-22.030240234375" Y="-1.305364379883" />
                  <Point X="-22.03303515625" Y="-1.335732910156" />
                  <Point X="-22.04596875" Y="-1.476294799805" />
                  <Point X="-22.04365234375" Y="-1.507561889648" />
                  <Point X="-22.035921875" Y="-1.539182617188" />
                  <Point X="-22.023548828125" Y="-1.56799597168" />
                  <Point X="-22.005697265625" Y="-1.595763427734" />
                  <Point X="-21.923068359375" Y="-1.724286621094" />
                  <Point X="-21.9130625" Y="-1.73724206543" />
                  <Point X="-21.889369140625" Y="-1.760909301758" />
                  <Point X="-20.897251953125" Y="-2.522188720703" />
                  <Point X="-20.786876953125" Y="-2.606882568359" />
                  <Point X="-20.87518359375" Y="-2.749775634766" />
                  <Point X="-20.903720703125" Y="-2.790323730469" />
                  <Point X="-20.971017578125" Y="-2.885944824219" />
                  <Point X="-21.96047265625" Y="-2.314682861328" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.2457734375" Y="-2.159825195312" />
                  <Point X="-22.2841640625" Y="-2.152891845703" />
                  <Point X="-22.46186328125" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135176513672" />
                  <Point X="-22.587060546875" Y="-2.151962158203" />
                  <Point X="-22.73468359375" Y="-2.229655517578" />
                  <Point X="-22.757615234375" Y="-2.246549804688" />
                  <Point X="-22.77857421875" Y="-2.267509521484" />
                  <Point X="-22.79546484375" Y="-2.290436035156" />
                  <Point X="-22.812251953125" Y="-2.322330078125" />
                  <Point X="-22.8899453125" Y="-2.469954101562" />
                  <Point X="-22.899771484375" Y="-2.499735351563" />
                  <Point X="-22.904728515625" Y="-2.531908691406" />
                  <Point X="-22.90432421875" Y="-2.563259033203" />
                  <Point X="-22.897390625" Y="-2.601650878906" />
                  <Point X="-22.865296875" Y="-2.779349365234" />
                  <Point X="-22.86101171875" Y="-2.795140869141" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.210642578125" Y="-3.930322998047" />
                  <Point X="-22.13871484375" Y="-4.054906005859" />
                  <Point X="-22.218140625" Y="-4.111637695312" />
                  <Point X="-22.250046875" Y="-4.132291503906" />
                  <Point X="-22.298232421875" Y="-4.163481445312" />
                  <Point X="-23.059400390625" Y="-3.171510253906" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900556884766" />
                  <Point X="-23.315939453125" Y="-2.876213378906" />
                  <Point X="-23.49119921875" Y="-2.763538085938" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.624310546875" Y="-2.744927490234" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-23.92737890625" Y="-2.822048828125" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968861083984" />
                  <Point X="-24.11275" Y="-2.996687744141" />
                  <Point X="-24.124375" Y="-3.025809326172" />
                  <Point X="-24.133935546875" Y="-3.069797119141" />
                  <Point X="-24.178189453125" Y="-3.273397216797" />
                  <Point X="-24.180275390625" Y="-3.289627441406" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-23.999583984375" Y="-4.695466308594" />
                  <Point X="-23.97793359375" Y="-4.859915527344" />
                  <Point X="-24.0243203125" Y="-4.870083007812" />
                  <Point X="-24.053791015625" Y="-4.875437011719" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.058427734375" Y="-4.75263671875" />
                  <Point X="-26.1039765625" Y="-4.740916992188" />
                  <Point X="-26.141244140625" Y="-4.731328125" />
                  <Point X="-26.129341796875" Y="-4.640912109375" />
                  <Point X="-26.1207734375" Y="-4.575838378906" />
                  <Point X="-26.120076171875" Y="-4.568099121094" />
                  <Point X="-26.11944921875" Y="-4.54103125" />
                  <Point X="-26.1217578125" Y="-4.50932421875" />
                  <Point X="-26.123333984375" Y="-4.497688964844" />
                  <Point X="-26.134087890625" Y="-4.443626953125" />
                  <Point X="-26.18386328125" Y="-4.193397460938" />
                  <Point X="-26.188125" Y="-4.178468261719" />
                  <Point X="-26.19902734375" Y="-4.149500976562" />
                  <Point X="-26.20566796875" Y="-4.135462890625" />
                  <Point X="-26.221740234375" Y="-4.107625976562" />
                  <Point X="-26.230576171875" Y="-4.094859619141" />
                  <Point X="-26.250208984375" Y="-4.070937255859" />
                  <Point X="-26.261005859375" Y="-4.05978125" />
                  <Point X="-26.302447265625" Y="-4.023437011719" />
                  <Point X="-26.494265625" Y="-3.855216308594" />
                  <Point X="-26.506736328125" Y="-3.845967041016" />
                  <Point X="-26.53301953125" Y="-3.829622314453" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.691818359375" Y="-3.792564453125" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166259766" />
                  <Point X="-27.008005859375" Y="-3.781946044922" />
                  <Point X="-27.0390546875" Y="-3.790265869141" />
                  <Point X="-27.053673828125" Y="-3.79549609375" />
                  <Point X="-27.081865234375" Y="-3.808269042969" />
                  <Point X="-27.0954375" Y="-3.815811767578" />
                  <Point X="-27.14126953125" Y="-3.846435546875" />
                  <Point X="-27.35340234375" Y="-3.9881796875" />
                  <Point X="-27.3596796875" Y="-3.992755859375" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032771484375" />
                  <Point X="-27.410470703125" Y="-4.04162890625" />
                  <Point X="-27.503203125" Y="-4.162479003906" />
                  <Point X="-27.747583984375" Y="-4.0111640625" />
                  <Point X="-27.810646484375" Y="-3.962608154297" />
                  <Point X="-27.98086328125" Y="-3.831546142578" />
                  <Point X="-27.47375" Y="-2.953201171875" />
                  <Point X="-27.341490234375" Y="-2.724119628906" />
                  <Point X="-27.336205078125" Y="-2.713479980469" />
                  <Point X="-27.32372265625" Y="-2.683829345703" />
                  <Point X="-27.31891015625" Y="-2.669170410156" />
                  <Point X="-27.31349609375" Y="-2.646645019531" />
                  <Point X="-27.310869140625" Y="-2.623626708984" />
                  <Point X="-27.311068359375" Y="-2.600460693359" />
                  <Point X="-27.311876953125" Y="-2.588883544922" />
                  <Point X="-27.31601953125" Y="-2.55740234375" />
                  <Point X="-27.318443359375" Y="-2.545213134766" />
                  <Point X="-27.324861328125" Y="-2.521256103516" />
                  <Point X="-27.3343515625" Y="-2.498341308594" />
                  <Point X="-27.346751953125" Y="-2.476862060547" />
                  <Point X="-27.35365625" Y="-2.466529785156" />
                  <Point X="-27.366322265625" Y="-2.450022216797" />
                  <Point X="-27.382705078125" Y="-2.431336181641" />
                  <Point X="-27.396970703125" Y="-2.417069824219" />
                  <Point X="-27.408810546875" Y="-2.407024658203" />
                  <Point X="-27.433966796875" Y="-2.388996582031" />
                  <Point X="-27.447283203125" Y="-2.381013671875" />
                  <Point X="-27.476111328125" Y="-2.366795898438" />
                  <Point X="-27.4905546875" Y="-2.361088623047" />
                  <Point X="-27.520173828125" Y="-2.352103027344" />
                  <Point X="-27.550859375" Y="-2.3480625" />
                  <Point X="-27.581794921875" Y="-2.349074707031" />
                  <Point X="-27.597220703125" Y="-2.350849121094" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.747" Y="-2.991099609375" />
                  <Point X="-28.793087890625" Y="-3.017708496094" />
                  <Point X="-29.004021484375" Y="-2.740587158203" />
                  <Point X="-29.049224609375" Y="-2.664787841797" />
                  <Point X="-29.181265625" Y="-2.443374023438" />
                  <Point X="-28.27953515625" Y="-1.751451049805" />
                  <Point X="-28.048123046875" Y="-1.573881958008" />
                  <Point X="-28.039162109375" Y="-1.566068115234" />
                  <Point X="-28.016271484375" Y="-1.543435302734" />
                  <Point X="-28.00279296875" Y="-1.526688110352" />
                  <Point X="-27.983970703125" Y="-1.496950073242" />
                  <Point X="-27.97594921875" Y="-1.481203857422" />
                  <Point X="-27.9678671875" Y="-1.460850952148" />
                  <Point X="-27.9605234375" Y="-1.438368408203" />
                  <Point X="-27.9541875" Y="-1.41390612793" />
                  <Point X="-27.951953125" Y="-1.402401977539" />
                  <Point X="-27.947837890625" Y="-1.37092565918" />
                  <Point X="-27.94708203125" Y="-1.355715454102" />
                  <Point X="-27.947779296875" Y="-1.332842407227" />
                  <Point X="-27.9512265625" Y="-1.310212646484" />
                  <Point X="-27.957375" Y="-1.288170043945" />
                  <Point X="-27.961115234375" Y="-1.277337402344" />
                  <Point X="-27.97312109375" Y="-1.248354736328" />
                  <Point X="-27.9786171875" Y="-1.237208496094" />
                  <Point X="-27.991021484375" Y="-1.215725585938" />
                  <Point X="-28.00612890625" Y="-1.196041992188" />
                  <Point X="-28.023673828125" Y="-1.178503173828" />
                  <Point X="-28.03301953125" Y="-1.170311157227" />
                  <Point X="-28.0502265625" Y="-1.157112670898" />
                  <Point X="-28.06948828125" Y="-1.144126098633" />
                  <Point X="-28.091265625" Y="-1.131308837891" />
                  <Point X="-28.105431640625" Y="-1.124481689453" />
                  <Point X="-28.1346953125" Y="-1.113257446289" />
                  <Point X="-28.14979296875" Y="-1.108860717773" />
                  <Point X="-28.1816796875" Y="-1.10237902832" />
                  <Point X="-28.197296875" Y="-1.100532592773" />
                  <Point X="-28.22862109375" Y="-1.09944152832" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-29.58287890625" Y="-1.276420166016" />
                  <Point X="-29.66091796875" Y="-1.286694213867" />
                  <Point X="-29.74076171875" Y="-0.974114501953" />
                  <Point X="-29.752720703125" Y="-0.890494018555" />
                  <Point X="-29.78644921875" Y="-0.654654541016" />
                  <Point X="-28.771900390625" Y="-0.382806335449" />
                  <Point X="-28.508287109375" Y="-0.312171234131" />
                  <Point X="-28.4984921875" Y="-0.308968109131" />
                  <Point X="-28.47930859375" Y="-0.301520172119" />
                  <Point X="-28.469919921875" Y="-0.297275482178" />
                  <Point X="-28.449419921875" Y="-0.286633056641" />
                  <Point X="-28.428626953125" Y="-0.274091674805" />
                  <Point X="-28.4058046875" Y="-0.258251739502" />
                  <Point X="-28.39646875" Y="-0.250863983154" />
                  <Point X="-28.3725078125" Y="-0.229328826904" />
                  <Point X="-28.358234375" Y="-0.213222717285" />
                  <Point X="-28.33799609375" Y="-0.184367294312" />
                  <Point X="-28.3292265625" Y="-0.168991958618" />
                  <Point X="-28.32016015625" Y="-0.148962051392" />
                  <Point X="-28.31179296875" Y="-0.126931243896" />
                  <Point X="-28.304185546875" Y="-0.102419914246" />
                  <Point X="-28.301576171875" Y="-0.091940811157" />
                  <Point X="-28.2961328125" Y="-0.063203536987" />
                  <Point X="-28.2944921875" Y="-0.043597057343" />
                  <Point X="-28.295146484375" Y="-0.011331627846" />
                  <Point X="-28.296853515625" Y="0.004768985271" />
                  <Point X="-28.3007421875" Y="0.024889846802" />
                  <Point X="-28.305828125" Y="0.045155467987" />
                  <Point X="-28.313435546875" Y="0.069666793823" />
                  <Point X="-28.317666015625" Y="0.080785720825" />
                  <Point X="-28.330982421875" Y="0.110111244202" />
                  <Point X="-28.34212109375" Y="0.128675125122" />
                  <Point X="-28.3637265625" Y="0.156824157715" />
                  <Point X="-28.375853515625" Y="0.169878295898" />
                  <Point X="-28.392603515625" Y="0.184818313599" />
                  <Point X="-28.410736328125" Y="0.19911390686" />
                  <Point X="-28.43355859375" Y="0.214953826904" />
                  <Point X="-28.440482421875" Y="0.219329437256" />
                  <Point X="-28.465615234375" Y="0.232835388184" />
                  <Point X="-28.49656640625" Y="0.245636428833" />
                  <Point X="-28.508287109375" Y="0.249611343384" />
                  <Point X="-29.728439453125" Y="0.576549865723" />
                  <Point X="-29.7854453125" Y="0.591824768066" />
                  <Point X="-29.73133203125" Y="0.957520812988" />
                  <Point X="-29.707251953125" Y="1.046383300781" />
                  <Point X="-29.6335859375" Y="1.318237060547" />
                  <Point X="-28.967029296875" Y="1.230483154297" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767736328125" Y="1.204815551758" />
                  <Point X="-28.747056640625" Y="1.204364257812" />
                  <Point X="-28.736703125" Y="1.20470324707" />
                  <Point X="-28.715142578125" Y="1.20658972168" />
                  <Point X="-28.704890625" Y="1.208053588867" />
                  <Point X="-28.684599609375" Y="1.21208984375" />
                  <Point X="-28.663650390625" Y="1.218102905273" />
                  <Point X="-28.61313671875" Y="1.234029541016" />
                  <Point X="-28.6034453125" Y="1.237678100586" />
                  <Point X="-28.584513671875" Y="1.246008544922" />
                  <Point X="-28.575275390625" Y="1.250690429688" />
                  <Point X="-28.55653125" Y="1.261512573242" />
                  <Point X="-28.54785546875" Y="1.267173339844" />
                  <Point X="-28.531173828125" Y="1.279405273438" />
                  <Point X="-28.515919921875" Y="1.293385009766" />
                  <Point X="-28.502283203125" Y="1.3089375" />
                  <Point X="-28.495892578125" Y="1.317081787109" />
                  <Point X="-28.483478515625" Y="1.334812133789" />
                  <Point X="-28.47801171875" Y="1.343601806641" />
                  <Point X="-28.4680625" Y="1.361734985352" />
                  <Point X="-28.459201171875" Y="1.381650268555" />
                  <Point X="-28.43893359375" Y="1.430582641602" />
                  <Point X="-28.4355" Y="1.440349609375" />
                  <Point X="-28.429712890625" Y="1.460201293945" />
                  <Point X="-28.427359375" Y="1.470286499023" />
                  <Point X="-28.423599609375" Y="1.491601806641" />
                  <Point X="-28.422359375" Y="1.501889526367" />
                  <Point X="-28.421005859375" Y="1.522536132812" />
                  <Point X="-28.421908203125" Y="1.543206298828" />
                  <Point X="-28.425056640625" Y="1.56365625" />
                  <Point X="-28.427189453125" Y="1.573795776367" />
                  <Point X="-28.43279296875" Y="1.594701782227" />
                  <Point X="-28.436015625" Y="1.604543701172" />
                  <Point X="-28.443509765625" Y="1.623811523438" />
                  <Point X="-28.453064453125" Y="1.643386962891" />
                  <Point X="-28.477521484375" Y="1.690366699219" />
                  <Point X="-28.482798828125" Y="1.699281860352" />
                  <Point X="-28.49429296875" Y="1.716485107422" />
                  <Point X="-28.500509765625" Y="1.72477331543" />
                  <Point X="-28.514421875" Y="1.741352783203" />
                  <Point X="-28.521501953125" Y="1.748912475586" />
                  <Point X="-28.536443359375" Y="1.763215454102" />
                  <Point X="-28.5443046875" Y="1.769958618164" />
                  <Point X="-29.22761328125" Y="2.294281005859" />
                  <Point X="-29.002283203125" Y="2.680323486328" />
                  <Point X="-28.938517578125" Y="2.762286376953" />
                  <Point X="-28.726337890625" Y="3.035013183594" />
                  <Point X="-28.370212890625" Y="2.829404052734" />
                  <Point X="-28.25415625" Y="2.7623984375" />
                  <Point X="-28.244916015625" Y="2.757716064453" />
                  <Point X="-28.225982421875" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.139873046875" Y="2.729828125" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310791016" />
                  <Point X="-27.97643359375" Y="2.734227294922" />
                  <Point X="-27.956994140625" Y="2.741302978516" />
                  <Point X="-27.938443359375" Y="2.750451904297" />
                  <Point X="-27.929419921875" Y="2.755531738281" />
                  <Point X="-27.911166015625" Y="2.767160888672" />
                  <Point X="-27.902751953125" Y="2.773189208984" />
                  <Point X="-27.88662109375" Y="2.786133544922" />
                  <Point X="-27.868115234375" Y="2.803837890625" />
                  <Point X="-27.8181796875" Y="2.853772949219" />
                  <Point X="-27.811263671875" Y="2.861488525391" />
                  <Point X="-27.798314453125" Y="2.877623291016" />
                  <Point X="-27.79228125" Y="2.886042480469" />
                  <Point X="-27.78065234375" Y="2.904296630859" />
                  <Point X="-27.7755703125" Y="2.913325195312" />
                  <Point X="-27.766423828125" Y="2.931874267578" />
                  <Point X="-27.759353515625" Y="2.951298828125" />
                  <Point X="-27.754435546875" Y="2.971387939453" />
                  <Point X="-27.7525234375" Y="2.981572998047" />
                  <Point X="-27.749697265625" Y="3.003031738281" />
                  <Point X="-27.748908203125" Y="3.013364746094" />
                  <Point X="-27.74845703125" Y="3.034051025391" />
                  <Point X="-27.750125" Y="3.059603515625" />
                  <Point X="-27.756279296875" Y="3.129953857422" />
                  <Point X="-27.757744140625" Y="3.1402109375" />
                  <Point X="-27.76178125" Y="3.160500976562" />
                  <Point X="-27.764353515625" Y="3.170533935547" />
                  <Point X="-27.77086328125" Y="3.191176513672" />
                  <Point X="-27.774513671875" Y="3.200871337891" />
                  <Point X="-27.78284375" Y="3.219799560547" />
                  <Point X="-27.7875234375" Y="3.229032958984" />
                  <Point X="-28.05938671875" Y="3.699915771484" />
                  <Point X="-27.648373046875" Y="4.01503515625" />
                  <Point X="-27.54793359375" Y="4.070838134766" />
                  <Point X="-27.192525390625" Y="4.268294921875" />
                  <Point X="-27.15409375" Y="4.218209960938" />
                  <Point X="-27.118564453125" Y="4.171908203125" />
                  <Point X="-27.111818359375" Y="4.164045410156" />
                  <Point X="-27.097513671875" Y="4.149103027344" />
                  <Point X="-27.089955078125" Y="4.142022949219" />
                  <Point X="-27.073375" Y="4.128110839844" />
                  <Point X="-27.06508984375" Y="4.121895996094" />
                  <Point X="-27.047892578125" Y="4.110405273438" />
                  <Point X="-27.022064453125" Y="4.096323242188" />
                  <Point X="-26.943763671875" Y="4.0555625" />
                  <Point X="-26.934326171875" Y="4.051286621094" />
                  <Point X="-26.915044921875" Y="4.0437890625" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.884294921875" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834472656" />
                  <Point X="-26.85371875" Y="4.029688476562" />
                  <Point X="-26.833056640625" Y="4.028786132812" />
                  <Point X="-26.81241796875" Y="4.030138183594" />
                  <Point X="-26.802134765625" Y="4.031378173828" />
                  <Point X="-26.780818359375" Y="4.035136474609" />
                  <Point X="-26.7707265625" Y="4.037489257812" />
                  <Point X="-26.75086328125" Y="4.043279296875" />
                  <Point X="-26.72347265625" Y="4.054014892578" />
                  <Point X="-26.64191796875" Y="4.087795654297" />
                  <Point X="-26.63258203125" Y="4.092273925781" />
                  <Point X="-26.614453125" Y="4.102219726562" />
                  <Point X="-26.60566015625" Y="4.1076875" />
                  <Point X="-26.5879296875" Y="4.1201015625" />
                  <Point X="-26.579783203125" Y="4.126494140625" />
                  <Point X="-26.5642265625" Y="4.140135253906" />
                  <Point X="-26.5502421875" Y="4.155395507812" />
                  <Point X="-26.538009765625" Y="4.172081054688" />
                  <Point X="-26.5323515625" Y="4.180754882812" />
                  <Point X="-26.52153125" Y="4.199499023438" />
                  <Point X="-26.516849609375" Y="4.208737792969" />
                  <Point X="-26.508521484375" Y="4.22766796875" />
                  <Point X="-26.499140625" Y="4.255548339844" />
                  <Point X="-26.472595703125" Y="4.339735839844" />
                  <Point X="-26.470025390625" Y="4.349764160156" />
                  <Point X="-26.465990234375" Y="4.370047363281" />
                  <Point X="-26.464525390625" Y="4.380302246094" />
                  <Point X="-26.462638671875" Y="4.401863769531" />
                  <Point X="-26.46230078125" Y="4.412216796875" />
                  <Point X="-26.462751953125" Y="4.432897949219" />
                  <Point X="-26.463541015625" Y="4.4432265625" />
                  <Point X="-26.479265625" Y="4.562655761719" />
                  <Point X="-25.931177734375" Y="4.716319824219" />
                  <Point X="-25.80942578125" Y="4.730569335938" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.26" Y="4.389859863281" />
                  <Point X="-25.22566796875" Y="4.261728027344" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912597656" />
                  <Point X="-25.2001171875" Y="4.205341308594" />
                  <Point X="-25.1822578125" Y="4.178614257812" />
                  <Point X="-25.17260546875" Y="4.166452636719" />
                  <Point X="-25.15144921875" Y="4.143865234375" />
                  <Point X="-25.126896484375" Y="4.125025390625" />
                  <Point X="-25.0996015625" Y="4.110436035156" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.978369140625" Y="4.085113037109" />
                  <Point X="-24.94783203125" Y="4.090154541016" />
                  <Point X="-24.93276953125" Y="4.093927246094" />
                  <Point X="-24.90233203125" Y="4.104259277344" />
                  <Point X="-24.8880859375" Y="4.110436035156" />
                  <Point X="-24.86079296875" Y="4.125024902344" />
                  <Point X="-24.8362421875" Y="4.143861328125" />
                  <Point X="-24.815083984375" Y="4.16644921875" />
                  <Point X="-24.8054296875" Y="4.178612792969" />
                  <Point X="-24.7875703125" Y="4.205339355469" />
                  <Point X="-24.78002734375" Y="4.218911621094" />
                  <Point X="-24.767251953125" Y="4.24710546875" />
                  <Point X="-24.76201953125" Y="4.261727050781" />
                  <Point X="-24.621806640625" Y="4.785005859375" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-24.0713828125" Y="4.713590820313" />
                  <Point X="-23.54639453125" Y="4.586841796875" />
                  <Point X="-23.48301953125" Y="4.563855957031" />
                  <Point X="-23.14175" Y="4.44007421875" />
                  <Point X="-23.07833203125" Y="4.410416015625" />
                  <Point X="-22.74955859375" Y="4.256659667969" />
                  <Point X="-22.68823046875" Y="4.220930664062" />
                  <Point X="-22.370560546875" Y="4.035854003906" />
                  <Point X="-22.31278515625" Y="3.994768554688" />
                  <Point X="-22.182216796875" Y="3.901915527344" />
                  <Point X="-22.781607421875" Y="2.863742431641" />
                  <Point X="-22.9346875" Y="2.59859765625" />
                  <Point X="-22.93903125" Y="2.590121826172" />
                  <Point X="-22.95032421875" Y="2.563891601562" />
                  <Point X="-22.959140625" Y="2.537545166016" />
                  <Point X="-22.96251171875" Y="2.526337646484" />
                  <Point X="-22.98016796875" Y="2.460315429688" />
                  <Point X="-22.98261328125" Y="2.447917480469" />
                  <Point X="-22.9858359375" Y="2.422916992188" />
                  <Point X="-22.98661328125" Y="2.410314453125" />
                  <Point X="-22.986462890625" Y="2.379072998047" />
                  <Point X="-22.985099609375" Y="2.357243896484" />
                  <Point X="-22.978216796875" Y="2.300158447266" />
                  <Point X="-22.97619921875" Y="2.289038085938" />
                  <Point X="-22.970857421875" Y="2.267117431641" />
                  <Point X="-22.967533203125" Y="2.256317138672" />
                  <Point X="-22.959267578125" Y="2.234227050781" />
                  <Point X="-22.954685546875" Y="2.223895996094" />
                  <Point X="-22.9443203125" Y="2.203843994141" />
                  <Point X="-22.930904296875" Y="2.182875244141" />
                  <Point X="-22.895578125" Y="2.130814453125" />
                  <Point X="-22.88773046875" Y="2.120772216797" />
                  <Point X="-22.870759765625" Y="2.101827148438" />
                  <Point X="-22.86163671875" Y="2.092924316406" />
                  <Point X="-22.837087890625" Y="2.071992675781" />
                  <Point X="-22.820494140625" Y="2.059349609375" />
                  <Point X="-22.76843359375" Y="2.024023925781" />
                  <Point X="-22.75871875" Y="2.018245117188" />
                  <Point X="-22.738673828125" Y="2.00788269043" />
                  <Point X="-22.72834375" Y="2.003299316406" />
                  <Point X="-22.706255859375" Y="1.995033203125" />
                  <Point X="-22.695453125" Y="1.991708007812" />
                  <Point X="-22.673529296875" Y="1.986364990234" />
                  <Point X="-22.65007421875" Y="1.98285949707" />
                  <Point X="-22.592984375" Y="1.975975219727" />
                  <Point X="-22.5799921875" Y="1.975305786133" />
                  <Point X="-22.554037109375" Y="1.975748413086" />
                  <Point X="-22.54107421875" Y="1.976860473633" />
                  <Point X="-22.507908203125" Y="1.982012573242" />
                  <Point X="-22.487990234375" Y="1.986209960938" />
                  <Point X="-22.421966796875" Y="2.003865234375" />
                  <Point X="-22.416005859375" Y="2.005670654297" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572875977" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.1367265625" Y="2.736418945312" />
                  <Point X="-21.05959375" Y="2.780951660156" />
                  <Point X="-20.956041015625" Y="2.637038818359" />
                  <Point X="-20.9238359375" Y="2.583816650391" />
                  <Point X="-20.86311328125" Y="2.483472167969" />
                  <Point X="-21.6255" Y="1.898471801758" />
                  <Point X="-21.827046875" Y="1.743819946289" />
                  <Point X="-21.834453125" Y="1.737508300781" />
                  <Point X="-21.85531640625" Y="1.71711328125" />
                  <Point X="-21.874828125" Y="1.694470703125" />
                  <Point X="-21.88169140625" Y="1.686028564453" />
                  <Point X="-21.92920703125" Y="1.624039794922" />
                  <Point X="-21.936353515625" Y="1.613269897461" />
                  <Point X="-21.9491171875" Y="1.590864746094" />
                  <Point X="-21.954734375" Y="1.579229614258" />
                  <Point X="-21.96702734375" Y="1.548604858398" />
                  <Point X="-21.9736796875" Y="1.529002563477" />
                  <Point X="-21.991380859375" Y="1.465712036133" />
                  <Point X="-21.993771484375" Y="1.454670166016" />
                  <Point X="-21.9972265625" Y="1.432370727539" />
                  <Point X="-21.998291015625" Y="1.42111340332" />
                  <Point X="-21.999107421875" Y="1.397542602539" />
                  <Point X="-21.998826171875" Y="1.386240478516" />
                  <Point X="-21.996921875" Y="1.36374609375" />
                  <Point X="-21.992158203125" Y="1.33733984375" />
                  <Point X="-21.97762890625" Y="1.266921142578" />
                  <Point X="-21.97412109375" Y="1.254359741211" />
                  <Point X="-21.965421875" Y="1.229842407227" />
                  <Point X="-21.96023046875" Y="1.217886108398" />
                  <Point X="-21.944494140625" Y="1.187531494141" />
                  <Point X="-21.934544921875" Y="1.170552490234" />
                  <Point X="-21.89502734375" Y="1.110484619141" />
                  <Point X="-21.888265625" Y="1.101429199219" />
                  <Point X="-21.873712890625" Y="1.084184204102" />
                  <Point X="-21.865921875" Y="1.075994506836" />
                  <Point X="-21.848677734375" Y="1.059903808594" />
                  <Point X="-21.83996875" Y="1.052697875977" />
                  <Point X="-21.8217578125" Y="1.039370849609" />
                  <Point X="-21.7998828125" Y="1.026284790039" />
                  <Point X="-21.74261328125" Y="0.994047546387" />
                  <Point X="-21.730583984375" Y="0.988346557617" />
                  <Point X="-21.705849609375" Y="0.978682617188" />
                  <Point X="-21.69314453125" Y="0.974719604492" />
                  <Point X="-21.658439453125" Y="0.966510559082" />
                  <Point X="-21.639599609375" Y="0.963046447754" />
                  <Point X="-21.562166015625" Y="0.952812866211" />
                  <Point X="-21.555966796875" Y="0.952199829102" />
                  <Point X="-21.53428125" Y="0.951222900391" />
                  <Point X="-21.50878125" Y="0.952032287598" />
                  <Point X="-21.49939453125" Y="0.952797302246" />
                  <Point X="-20.3336015625" Y="1.106277099609" />
                  <Point X="-20.295296875" Y="1.111320068359" />
                  <Point X="-20.247310546875" Y="0.914207275391" />
                  <Point X="-20.2371640625" Y="0.849038024902" />
                  <Point X="-20.21612890625" Y="0.713921386719" />
                  <Point X="-21.07703125" Y="0.483242492676" />
                  <Point X="-21.3080078125" Y="0.421352661133" />
                  <Point X="-21.3174375" Y="0.418290130615" />
                  <Point X="-21.34498828125" Y="0.407170013428" />
                  <Point X="-21.37350390625" Y="0.392662200928" />
                  <Point X="-21.3824296875" Y="0.387816497803" />
                  <Point X="-21.45850390625" Y="0.343844482422" />
                  <Point X="-21.469435546875" Y="0.33646862793" />
                  <Point X="-21.490169921875" Y="0.320275878906" />
                  <Point X="-21.49997265625" Y="0.311459259033" />
                  <Point X="-21.524146484375" Y="0.286473236084" />
                  <Point X="-21.537064453125" Y="0.27166104126" />
                  <Point X="-21.582708984375" Y="0.213499252319" />
                  <Point X="-21.589146484375" Y="0.204207061768" />
                  <Point X="-21.60087109375" Y="0.184925476074" />
                  <Point X="-21.606158203125" Y="0.174935928345" />
                  <Point X="-21.615931640625" Y="0.153471969604" />
                  <Point X="-21.619994140625" Y="0.142926422119" />
                  <Point X="-21.62683984375" Y="0.121426498413" />
                  <Point X="-21.63291015625" Y="0.093307937622" />
                  <Point X="-21.648125" Y="0.013861732483" />
                  <Point X="-21.64970703125" Y="0.000648416102" />
                  <Point X="-21.6510078125" Y="-0.025864864349" />
                  <Point X="-21.6507265625" Y="-0.039164680481" />
                  <Point X="-21.647439453125" Y="-0.075138252258" />
                  <Point X="-21.644837890625" Y="-0.093586662292" />
                  <Point X="-21.629623046875" Y="-0.173032867432" />
                  <Point X="-21.62683984375" Y="-0.183987121582" />
                  <Point X="-21.619994140625" Y="-0.205486755371" />
                  <Point X="-21.615931640625" Y="-0.216031997681" />
                  <Point X="-21.606158203125" Y="-0.237495803833" />
                  <Point X="-21.600869140625" Y="-0.247486679077" />
                  <Point X="-21.58914453125" Y="-0.266767669678" />
                  <Point X="-21.57284765625" Y="-0.28862387085" />
                  <Point X="-21.527203125" Y="-0.346785797119" />
                  <Point X="-21.518078125" Y="-0.356840576172" />
                  <Point X="-21.498490234375" Y="-0.375546234131" />
                  <Point X="-21.48802734375" Y="-0.384196990967" />
                  <Point X="-21.457279296875" Y="-0.406117523193" />
                  <Point X="-21.442068359375" Y="-0.41590447998" />
                  <Point X="-21.365994140625" Y="-0.459876647949" />
                  <Point X="-21.360505859375" Y="-0.462813568115" />
                  <Point X="-21.34084375" Y="-0.472016174316" />
                  <Point X="-21.31697265625" Y="-0.481027618408" />
                  <Point X="-21.3080078125" Y="-0.483912689209" />
                  <Point X="-20.238921875" Y="-0.770373779297" />
                  <Point X="-20.21512109375" Y="-0.776751342773" />
                  <Point X="-20.2383828125" Y="-0.931050231934" />
                  <Point X="-20.251388671875" Y="-0.98804296875" />
                  <Point X="-20.2721953125" Y="-1.079219848633" />
                  <Point X="-21.295349609375" Y="-0.944519042969" />
                  <Point X="-21.563216796875" Y="-0.909253662109" />
                  <Point X="-21.571375" Y="-0.908535644531" />
                  <Point X="-21.59990234375" Y="-0.908042358398" />
                  <Point X="-21.63327734375" Y="-0.910840942383" />
                  <Point X="-21.645517578125" Y="-0.912676269531" />
                  <Point X="-21.677775390625" Y="-0.919687744141" />
                  <Point X="-21.82708203125" Y="-0.952139953613" />
                  <Point X="-21.842125" Y="-0.956742675781" />
                  <Point X="-21.8712421875" Y="-0.968366027832" />
                  <Point X="-21.88531640625" Y="-0.975386657715" />
                  <Point X="-21.91314453125" Y="-0.992278991699" />
                  <Point X="-21.92587109375" Y="-1.001527526855" />
                  <Point X="-21.949623046875" Y="-1.021998840332" />
                  <Point X="-21.9606484375" Y="-1.033221679688" />
                  <Point X="-21.980146484375" Y="-1.056671386719" />
                  <Point X="-22.070392578125" Y="-1.165209838867" />
                  <Point X="-22.078673828125" Y="-1.176849243164" />
                  <Point X="-22.093396484375" Y="-1.201236572266" />
                  <Point X="-22.099837890625" Y="-1.21398425293" />
                  <Point X="-22.1111796875" Y="-1.241369384766" />
                  <Point X="-22.11563671875" Y="-1.25493371582" />
                  <Point X="-22.122466796875" Y="-1.282577758789" />
                  <Point X="-22.12483984375" Y="-1.296657836914" />
                  <Point X="-22.127634765625" Y="-1.327026489258" />
                  <Point X="-22.140568359375" Y="-1.467588256836" />
                  <Point X="-22.140708984375" Y="-1.483313598633" />
                  <Point X="-22.138392578125" Y="-1.514580688477" />
                  <Point X="-22.135935546875" Y="-1.530122558594" />
                  <Point X="-22.128205078125" Y="-1.561743286133" />
                  <Point X="-22.123212890625" Y="-1.576667602539" />
                  <Point X="-22.11083984375" Y="-1.605480957031" />
                  <Point X="-22.103458984375" Y="-1.619370117188" />
                  <Point X="-22.085607421875" Y="-1.647137573242" />
                  <Point X="-22.002978515625" Y="-1.775660766602" />
                  <Point X="-21.998255859375" Y="-1.78235534668" />
                  <Point X="-21.980201171875" Y="-1.804454223633" />
                  <Point X="-21.9565078125" Y="-1.828121337891" />
                  <Point X="-21.947201171875" Y="-1.836277832031" />
                  <Point X="-20.955083984375" Y="-2.597557128906" />
                  <Point X="-20.912828125" Y="-2.629981445312" />
                  <Point X="-20.9544921875" Y="-2.697400878906" />
                  <Point X="-20.98141015625" Y="-2.735647705078" />
                  <Point X="-20.9987265625" Y="-2.760250976562" />
                  <Point X="-21.91297265625" Y="-2.232410400391" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.184970703125" Y="-2.079513916016" />
                  <Point X="-22.216876953125" Y="-2.069326416016" />
                  <Point X="-22.228888671875" Y="-2.066337646484" />
                  <Point X="-22.267279296875" Y="-2.059404296875" />
                  <Point X="-22.444978515625" Y="-2.027311889648" />
                  <Point X="-22.460638671875" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503295898" />
                  <Point X="-22.539859375" Y="-2.03146105957" />
                  <Point X="-22.555154296875" Y="-2.035136108398" />
                  <Point X="-22.5849296875" Y="-2.044959594727" />
                  <Point X="-22.59941015625" Y="-2.051108154297" />
                  <Point X="-22.6313046875" Y="-2.067893798828" />
                  <Point X="-22.778927734375" Y="-2.145587158203" />
                  <Point X="-22.79103125" Y="-2.153170898438" />
                  <Point X="-22.813962890625" Y="-2.170065185547" />
                  <Point X="-22.824791015625" Y="-2.179375732422" />
                  <Point X="-22.84575" Y="-2.200335449219" />
                  <Point X="-22.85505859375" Y="-2.211161132813" />
                  <Point X="-22.87194921875" Y="-2.234087646484" />
                  <Point X="-22.87953125" Y="-2.246188476562" />
                  <Point X="-22.896318359375" Y="-2.278082519531" />
                  <Point X="-22.97401171875" Y="-2.425706542969" />
                  <Point X="-22.980162109375" Y="-2.440187744141" />
                  <Point X="-22.98998828125" Y="-2.469968994141" />
                  <Point X="-22.9936640625" Y="-2.485269042969" />
                  <Point X="-22.99862109375" Y="-2.517442382812" />
                  <Point X="-22.999720703125" Y="-2.533133789062" />
                  <Point X="-22.99931640625" Y="-2.564484130859" />
                  <Point X="-22.9978125" Y="-2.580143066406" />
                  <Point X="-22.99087890625" Y="-2.618534912109" />
                  <Point X="-22.95878515625" Y="-2.796233398438" />
                  <Point X="-22.95698046875" Y="-2.804228759766" />
                  <Point X="-22.94876171875" Y="-2.831542236328" />
                  <Point X="-22.935927734375" Y="-2.862479980469" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.2929140625" Y="-3.977822998047" />
                  <Point X="-22.264103515625" Y="-4.027723144531" />
                  <Point X="-22.276244140625" Y="-4.036082763672" />
                  <Point X="-22.98403125" Y="-3.113677734375" />
                  <Point X="-23.16608203125" Y="-2.876422607422" />
                  <Point X="-23.17134765625" Y="-2.870142089844" />
                  <Point X="-23.19116796875" Y="-2.849626220703" />
                  <Point X="-23.21674609375" Y="-2.828004882812" />
                  <Point X="-23.22669921875" Y="-2.820646484375" />
                  <Point X="-23.264564453125" Y="-2.796302978516" />
                  <Point X="-23.43982421875" Y="-2.683627685547" />
                  <Point X="-23.453716796875" Y="-2.676244628906" />
                  <Point X="-23.482529296875" Y="-2.663873291016" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.633015625" Y="-2.650327148438" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920125" Y="-2.699414794922" />
                  <Point X="-23.944505859375" Y="-2.714135742188" />
                  <Point X="-23.956138671875" Y="-2.722413330078" />
                  <Point X="-23.988115234375" Y="-2.749000976562" />
                  <Point X="-24.136123046875" Y="-2.872063720703" />
                  <Point X="-24.147345703125" Y="-2.883087646484" />
                  <Point X="-24.16781640625" Y="-2.906837158203" />
                  <Point X="-24.177064453125" Y="-2.919562744141" />
                  <Point X="-24.19395703125" Y="-2.947389404297" />
                  <Point X="-24.20098046875" Y="-2.961467285156" />
                  <Point X="-24.21260546875" Y="-2.990588867188" />
                  <Point X="-24.21720703125" Y="-3.005632568359" />
                  <Point X="-24.226767578125" Y="-3.049620361328" />
                  <Point X="-24.271021484375" Y="-3.253220458984" />
                  <Point X="-24.2724140625" Y="-3.261287353516" />
                  <Point X="-24.275275390625" Y="-3.289677246094" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520263672" />
                  <Point X="-24.16691015625" Y="-4.152321777344" />
                  <Point X="-24.27376171875" Y="-3.753544921875" />
                  <Point X="-24.344931640625" Y="-3.487937011719" />
                  <Point X="-24.347392578125" Y="-3.480122314453" />
                  <Point X="-24.35785546875" Y="-3.453575439453" />
                  <Point X="-24.37321484375" Y="-3.423811035156" />
                  <Point X="-24.37959375" Y="-3.413207275391" />
                  <Point X="-24.40868359375" Y="-3.371295898438" />
                  <Point X="-24.543322265625" Y="-3.177306884766" />
                  <Point X="-24.553330078125" Y="-3.165172119141" />
                  <Point X="-24.575212890625" Y="-3.142717529297" />
                  <Point X="-24.587087890625" Y="-3.132397705078" />
                  <Point X="-24.61334375" Y="-3.113153808594" />
                  <Point X="-24.6267578125" Y="-3.104937255859" />
                  <Point X="-24.6547578125" Y="-3.090829589844" />
                  <Point X="-24.66934375" Y="-3.084938476562" />
                  <Point X="-24.714357421875" Y="-3.070968017578" />
                  <Point X="-24.922703125" Y="-3.006305175781" />
                  <Point X="-24.936623046875" Y="-3.003109130859" />
                  <Point X="-24.964783203125" Y="-2.99883984375" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.998840087891" />
                  <Point X="-25.051064453125" Y="-3.003109619141" />
                  <Point X="-25.064984375" Y="-3.006305664062" />
                  <Point X="-25.10999609375" Y="-3.020275878906" />
                  <Point X="-25.31834375" Y="-3.084938964844" />
                  <Point X="-25.3329296875" Y="-3.090829345703" />
                  <Point X="-25.3609296875" Y="-3.104936767578" />
                  <Point X="-25.37434375" Y="-3.113153808594" />
                  <Point X="-25.400599609375" Y="-3.132397705078" />
                  <Point X="-25.412474609375" Y="-3.142717773438" />
                  <Point X="-25.434359375" Y="-3.165174560547" />
                  <Point X="-25.444369140625" Y="-3.177311279297" />
                  <Point X="-25.47345703125" Y="-3.219222412109" />
                  <Point X="-25.608095703125" Y="-3.413211669922" />
                  <Point X="-25.61246875" Y="-3.420131835938" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213378906" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.972166015625" Y="-4.717318359375" />
                  <Point X="-25.98542578125" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.200486081457" Y="3.870272256762" />
                  <Point X="-23.661260984893" Y="4.614574246046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.248051335877" Y="3.787886971826" />
                  <Point X="-24.058961983231" Y="4.710592033413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.295616590297" Y="3.705501686889" />
                  <Point X="-24.360569013316" Y="4.757647498412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.343181844717" Y="3.623116401952" />
                  <Point X="-24.622012257157" Y="4.784238492172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.390747099137" Y="3.540731117016" />
                  <Point X="-24.647149492051" Y="4.690425560506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.031080655146" Y="2.741325468404" />
                  <Point X="-21.082685649519" Y="2.767619526358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.438312353556" Y="3.458345832079" />
                  <Point X="-24.672286726946" Y="4.596612628839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.920549712387" Y="2.578386147536" />
                  <Point X="-21.180784212316" Y="2.710982248086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.485877607976" Y="3.375960547142" />
                  <Point X="-24.69742396184" Y="4.502799697173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.895201810643" Y="2.458849753931" />
                  <Point X="-21.278882815468" Y="2.654344990377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.533442862396" Y="3.293575262206" />
                  <Point X="-24.722561196735" Y="4.408986765506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.350911693283" Y="4.7291473347" />
                  <Point X="-25.438826898997" Y="4.773942369409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.978704758113" Y="2.394775638199" />
                  <Point X="-21.376981418621" Y="2.597707732668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.581008116816" Y="3.211189977269" />
                  <Point X="-24.747698431629" Y="4.31517383384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.317825579012" Y="4.605668124879" />
                  <Point X="-25.608995628517" Y="4.754026675232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.062207705582" Y="2.330701522468" />
                  <Point X="-21.475080021773" Y="2.541070474959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.628573371236" Y="3.128804692332" />
                  <Point X="-24.7777767198" Y="4.223878494565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.284739464741" Y="4.482188915057" />
                  <Point X="-25.779164358036" Y="4.734110981055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.145710653052" Y="2.266627406736" />
                  <Point X="-21.573178624926" Y="2.484433217249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.676138625656" Y="3.046419407396" />
                  <Point X="-24.83422213543" Y="4.14601787776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.251653561325" Y="4.358709812672" />
                  <Point X="-25.945578677665" Y="4.712282319491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.229213600522" Y="2.202553291004" />
                  <Point X="-21.671277228078" Y="2.42779595954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.723703880076" Y="2.964034122459" />
                  <Point X="-24.938451084337" Y="4.092504187227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.213999820996" Y="4.232903281131" />
                  <Point X="-26.080560864313" Y="4.674438186241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.312716547991" Y="2.138479175273" />
                  <Point X="-21.769375831231" Y="2.371158701831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.771269134496" Y="2.881648837522" />
                  <Point X="-26.21554305096" Y="4.636594052991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.396219495461" Y="2.074405059541" />
                  <Point X="-21.867474434383" Y="2.314521444122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.818834085918" Y="2.799263398201" />
                  <Point X="-26.350525237608" Y="4.598749919741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.479722442931" Y="2.010330943809" />
                  <Point X="-21.965573037535" Y="2.257884186412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.866398953196" Y="2.716877916006" />
                  <Point X="-26.478569793583" Y="4.557370887105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.5632253904" Y="1.946256828078" />
                  <Point X="-22.063671640688" Y="2.201246928703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.913963820474" Y="2.63449243381" />
                  <Point X="-26.463530347051" Y="4.443086913775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.64672838927" Y="1.882182738535" />
                  <Point X="-22.16177024384" Y="2.144609670994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.955325441117" Y="2.548946239585" />
                  <Point X="-26.472289860024" Y="4.340929115985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.288530342952" Y="1.083525275907" />
                  <Point X="-20.333269191167" Y="1.106320857654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.730231538922" Y="1.818108725821" />
                  <Point X="-22.259868846993" Y="2.087972413285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.981123641182" Y="2.455470086494" />
                  <Point X="-26.501311788955" Y="4.249095534793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.25889826406" Y="0.961805985015" />
                  <Point X="-20.499558375275" Y="1.084428436357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.813734688575" Y="1.754034713107" />
                  <Point X="-22.357967450145" Y="2.031335155575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.984281440263" Y="2.350458072915" />
                  <Point X="-26.543831789705" Y="4.164139564712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.236330905762" Y="0.843686349059" />
                  <Point X="-20.665847560099" Y="1.062536015426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.883890356779" Y="1.683159818908" />
                  <Point X="-22.481871161486" Y="1.987846257215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.957471087423" Y="2.230176523257" />
                  <Point X="-26.622691900746" Y="4.097699805663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.218301863615" Y="0.72787910068" />
                  <Point X="-20.832136744924" Y="1.040643594495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.940768594767" Y="1.60551973611" />
                  <Point X="-22.703669161532" Y="1.994236990311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.787437881203" Y="2.036919284854" />
                  <Point X="-26.736324692002" Y="4.048977612129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.336737782321" Y="0.68160422282" />
                  <Point X="-20.998425929749" Y="1.018751173564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.976943995919" Y="1.517331031067" />
                  <Point X="-27.28030323385" Y="4.219527530604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.473875199274" Y="0.64485823426" />
                  <Point X="-21.164715114574" Y="0.996858752632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.998248302421" Y="1.421565124839" />
                  <Point X="-27.380407168902" Y="4.163912040533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.611012616227" Y="0.6081122457" />
                  <Point X="-21.331004299399" Y="0.974966331701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.98627904133" Y="1.308845489126" />
                  <Point X="-27.480511103954" Y="4.108296550461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.748150033179" Y="0.57136625714" />
                  <Point X="-21.497293484224" Y="0.95307391077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.938982660771" Y="1.178125786987" />
                  <Point X="-27.580614708412" Y="4.052680891943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.885287450132" Y="0.534620268581" />
                  <Point X="-27.675367473384" Y="3.994338844531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.022424867085" Y="0.497874280021" />
                  <Point X="-27.758912248153" Y="3.930286040873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.159562453242" Y="0.461128377675" />
                  <Point X="-27.842457022922" Y="3.866233237215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.296700151352" Y="0.424382532372" />
                  <Point X="-27.926001797691" Y="3.802180433556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.40668035816" Y="0.373799254106" />
                  <Point X="-28.009546572459" Y="3.738127629898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.497663241612" Y="0.313536356118" />
                  <Point X="-28.024202108131" Y="3.638974005723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.562106939246" Y="0.239751067546" />
                  <Point X="-27.936988852795" Y="3.487915640021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.613313753062" Y="0.159221249798" />
                  <Point X="-27.849775597458" Y="3.336857274319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.638270827848" Y="0.065316521971" />
                  <Point X="-27.770431919643" Y="3.18980865864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.650816490668" Y="-0.034912136117" />
                  <Point X="-27.751337029102" Y="3.073458333379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.228604120655" Y="-0.8661865258" />
                  <Point X="-20.613775435501" Y="-0.66993193847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.634016235201" Y="-0.150093286411" />
                  <Point X="-27.755082388221" Y="2.968745696592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.245900749622" Y="-0.963994445726" />
                  <Point X="-21.055131507914" Y="-0.551670779862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.573740683647" Y="-0.287426206485" />
                  <Point X="-27.79488702754" Y="2.882406180758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.267697392962" Y="-1.059509493806" />
                  <Point X="-27.861981523585" Y="2.809971541438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.496129918074" Y="-1.049738301344" />
                  <Point X="-27.946563472467" Y="2.746447204386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.778290862564" Y="-1.012591111848" />
                  <Point X="-28.119746288842" Y="2.72806726417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.060451807055" Y="-0.975443922352" />
                  <Point X="-28.784555078918" Y="2.960183269186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.342612751541" Y="-0.938296732858" />
                  <Point X="-28.843957825764" Y="2.883829487898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.609642796586" Y="-0.908859121704" />
                  <Point X="-28.90336057261" Y="2.807475706611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.761801956938" Y="-0.937951149706" />
                  <Point X="-28.962763054641" Y="2.731121790393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.890915558553" Y="-0.978785476382" />
                  <Point X="-29.018338118504" Y="2.652817707214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.970375199288" Y="-1.044919759795" />
                  <Point X="-29.066306033051" Y="2.570637587859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.032645968336" Y="-1.119812210781" />
                  <Point X="-29.114273947598" Y="2.488457468505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.090738418429" Y="-1.19683362161" />
                  <Point X="-29.162241862144" Y="2.40627734915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.123197906972" Y="-1.286915678696" />
                  <Point X="-29.210209776691" Y="2.324097229795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.133281838972" Y="-1.388398651286" />
                  <Point X="-28.964089821979" Y="2.092071856666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.140099084971" Y="-1.491546083529" />
                  <Point X="-28.550514988356" Y="1.774723961089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.105305562843" Y="-1.615895261106" />
                  <Point X="-28.438547209551" Y="1.61105253569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.003940631646" Y="-1.774164265812" />
                  <Point X="-28.423007463272" Y="1.496513646907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.662418786813" Y="-2.054799329888" />
                  <Point X="-28.450074826905" Y="1.403684164953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.248842611859" Y="-2.372147908907" />
                  <Point X="-28.494175815318" Y="1.319533748322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.922225810054" Y="-2.645188474235" />
                  <Point X="-28.572014127896" Y="1.252573356951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.974160761842" Y="-2.725347287155" />
                  <Point X="-21.697885428474" Y="-2.35659115108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.288761911898" Y="-2.055524545267" />
                  <Point X="-28.696982118202" Y="1.209626735809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.543518903094" Y="-2.032340367392" />
                  <Point X="-28.940251875413" Y="1.226957875625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.656732313252" Y="-2.081276246267" />
                  <Point X="-29.222412795317" Y="1.264105052594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.759666109667" Y="-2.135449849956" />
                  <Point X="-29.50457381616" Y="1.301252280993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.844341285509" Y="-2.1989266855" />
                  <Point X="-29.647365094243" Y="1.267387078567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.896725693443" Y="-2.278856489076" />
                  <Point X="-29.672751766597" Y="1.173701241634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.940973912777" Y="-2.362931887805" />
                  <Point X="-29.698138438951" Y="1.080015404701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.982816591015" Y="-2.448232970944" />
                  <Point X="-29.723525361103" Y="0.986329695047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.99955055475" Y="-2.546327583125" />
                  <Point X="-29.741491568684" Y="0.888862942465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.983154710395" Y="-2.661302675665" />
                  <Point X="-28.307885052304" Y="0.051782945233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.904511264536" Y="0.3557791842" />
                  <Point X="-29.756162504841" Y="0.78971716523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.961946440479" Y="-2.778729821502" />
                  <Point X="-28.295942010144" Y="-0.060923331267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.345866225163" Y="0.474039776325" />
                  <Point X="-29.770833440999" Y="0.690571387995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.907692392221" Y="-2.912994632403" />
                  <Point X="-28.322456235366" Y="-0.154034651318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.8204787888" Y="-3.064053175463" />
                  <Point X="-23.151546605234" Y="-2.895365697482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.632546554737" Y="-2.650283982004" />
                  <Point X="-28.376643522701" Y="-0.233045841958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.733265185379" Y="-3.215111718524" />
                  <Point X="-23.017214264311" Y="-3.070432436447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.809791392558" Y="-2.666594218919" />
                  <Point X="-28.464927693022" Y="-0.294683802967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.646051581958" Y="-3.366170261584" />
                  <Point X="-22.88288037626" Y="-3.245499963714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.935918023799" Y="-2.708950483018" />
                  <Point X="-28.594516019449" Y="-0.33527624527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.558837978537" Y="-3.517228804645" />
                  <Point X="-22.748545980659" Y="-3.420567749591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.017925146149" Y="-2.773786759715" />
                  <Point X="-28.731653510936" Y="-0.372022195853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.471624375116" Y="-3.668287347705" />
                  <Point X="-22.614211585057" Y="-3.595635535467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.097434511574" Y="-2.839895707134" />
                  <Point X="-28.868790984622" Y="-0.408768155506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.384410771695" Y="-3.819345890765" />
                  <Point X="-22.479877189456" Y="-3.770703321344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.169841367045" Y="-2.909623564129" />
                  <Point X="-29.005928450913" Y="-0.445514118927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.297197168274" Y="-3.970404433826" />
                  <Point X="-22.345542793855" Y="-3.945771107221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.213628626085" Y="-2.993933833859" />
                  <Point X="-29.143065917203" Y="-0.482260082348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.235424654002" Y="-3.089449195513" />
                  <Point X="-29.280203383494" Y="-0.519006045769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.256288792169" Y="-3.185439378711" />
                  <Point X="-27.953645024209" Y="-1.301542282639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.327355476717" Y="-1.111127296344" />
                  <Point X="-29.417340849785" Y="-0.55575200919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.274576443951" Y="-3.282742347291" />
                  <Point X="-27.953123695342" Y="-1.40842890554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.493644699868" Y="-1.133019697747" />
                  <Point X="-29.554478316075" Y="-0.59249797261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.266837523255" Y="-3.393306516913" />
                  <Point X="-24.46265577288" Y="-3.293532135253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.036866764145" Y="-3.000957021824" />
                  <Point X="-27.985110942208" Y="-1.498751581777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.65993392302" Y="-1.15491209915" />
                  <Point X="-29.691615782366" Y="-0.629243936031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.251791118494" Y="-3.507594035637" />
                  <Point X="-24.357872194147" Y="-3.453543027882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.171029974705" Y="-3.039218444233" />
                  <Point X="-28.04767740954" Y="-1.573493366962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.826223146171" Y="-1.176804500553" />
                  <Point X="-29.781375711789" Y="-0.690129960221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.236744713734" Y="-3.621881554361" />
                  <Point X="-24.320472193889" Y="-3.5792202724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.301073507991" Y="-3.079578947058" />
                  <Point X="-28.131143774014" Y="-1.637586122661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.992512369323" Y="-1.198696901956" />
                  <Point X="-29.764928865203" Y="-0.805131039696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.221698308974" Y="-3.736169073085" />
                  <Point X="-24.287385765465" Y="-3.70269964229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.402841876349" Y="-3.134346366001" />
                  <Point X="-28.214646666432" Y="-1.701660266443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.158801592474" Y="-1.220589303359" />
                  <Point X="-29.748482009141" Y="-0.920132123999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.206651904214" Y="-3.850456591809" />
                  <Point X="-24.25429957326" Y="-3.826178891821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.466158277146" Y="-3.208706040999" />
                  <Point X="-28.298149566222" Y="-1.765734406469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.325090815626" Y="-1.242481704762" />
                  <Point X="-29.724149031974" Y="-1.039151387703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.191605499454" Y="-3.964744110533" />
                  <Point X="-24.221213546413" Y="-3.949658057097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.52082608258" Y="-3.287472395438" />
                  <Point X="-28.381652491712" Y="-1.8298085334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.491380038777" Y="-1.264374106165" />
                  <Point X="-29.692839341842" Y="-1.161725464216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.176559094694" Y="-4.079031629257" />
                  <Point X="-24.188127519566" Y="-4.073137222374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.575494003584" Y="-3.366238690991" />
                  <Point X="-27.36160566498" Y="-2.456169343871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.572394392109" Y="-2.348767122932" />
                  <Point X="-28.465155417202" Y="-1.893882660331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.657669253349" Y="-1.286266511939" />
                  <Point X="-29.66152965171" Y="-1.284299540729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.626616030034" Y="-3.44681171006" />
                  <Point X="-27.311983465845" Y="-2.588074109765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.703455456942" Y="-2.388609167537" />
                  <Point X="-28.548658342692" Y="-1.957956787261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.656256800857" Y="-3.538329975559" />
                  <Point X="-27.325415717439" Y="-2.68785102831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.80155408695" Y="-2.445246411563" />
                  <Point X="-28.632161268182" Y="-2.022030914192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.681393932491" Y="-3.632142959839" />
                  <Point X="-27.369221535373" Y="-2.772151841811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.899652716958" Y="-2.501883655589" />
                  <Point X="-28.715664193672" Y="-2.086105041123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.706531064125" Y="-3.725955944119" />
                  <Point X="-27.416786575953" Y="-2.854537235705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.997751346965" Y="-2.558520899614" />
                  <Point X="-28.799167119162" Y="-2.150179168054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.731668195759" Y="-3.8197689284" />
                  <Point X="-27.464351616532" Y="-2.936922629599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.095849976973" Y="-2.61515814364" />
                  <Point X="-28.882670044652" Y="-2.214253294985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.756805327393" Y="-3.91358191268" />
                  <Point X="-27.511916807643" Y="-3.019307946793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.19394860698" Y="-2.671795387666" />
                  <Point X="-28.966172970142" Y="-2.278327421916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.781942459027" Y="-4.007394896961" />
                  <Point X="-27.559482035822" Y="-3.1016932451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.292047236988" Y="-2.728432631692" />
                  <Point X="-29.049675895632" Y="-2.342401548846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.807079590661" Y="-4.101207881241" />
                  <Point X="-27.607047264001" Y="-3.184078543407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.390145866996" Y="-2.785069875718" />
                  <Point X="-29.133178821122" Y="-2.406475675777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.832216722295" Y="-4.195020865521" />
                  <Point X="-26.487533369648" Y="-3.861120356218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.601322539058" Y="-3.803141878527" />
                  <Point X="-27.65461249218" Y="-3.266463841714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.488244497003" Y="-2.841707119744" />
                  <Point X="-29.142526657584" Y="-2.508333707778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.85735385393" Y="-4.288833849802" />
                  <Point X="-26.226504777512" Y="-4.100742059532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.851933843887" Y="-3.78207003336" />
                  <Point X="-27.702177720359" Y="-3.348849140021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.586343127011" Y="-2.89834436377" />
                  <Point X="-29.051189239411" Y="-2.661493439403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.882490985564" Y="-4.382646834082" />
                  <Point X="-26.175962594104" Y="-4.233115580827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.042608656021" Y="-3.791537356576" />
                  <Point X="-27.749742948538" Y="-3.431234438328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.684441757018" Y="-2.954981607795" />
                  <Point X="-28.939912252956" Y="-2.82481288852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.907628117198" Y="-4.476459818362" />
                  <Point X="-26.152361594912" Y="-4.351761883124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.142502919684" Y="-3.847259679556" />
                  <Point X="-27.797308176717" Y="-3.513619736635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.782540356467" Y="-3.011618867392" />
                  <Point X="-28.807341833146" Y="-2.998981883839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.932765248832" Y="-4.570272802643" />
                  <Point X="-26.128760624912" Y="-4.470408170547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.233035288614" Y="-3.907752126159" />
                  <Point X="-27.844873404896" Y="-3.596005034943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.957902380466" Y="-4.664085786923" />
                  <Point X="-26.121422541962" Y="-4.580768103136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.323567657544" Y="-3.968244572761" />
                  <Point X="-27.892438633075" Y="-3.67839033325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.98303918153" Y="-4.757898939638" />
                  <Point X="-26.134577653298" Y="-4.680686231694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.403810145754" Y="-4.033979975463" />
                  <Point X="-27.940003861253" Y="-3.760775631557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.463196642908" Y="-4.110342036382" />
                  <Point X="-27.923148483288" Y="-3.875984868166" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000162109375" Y="0.001626220703" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.4572890625" Y="-3.802720214844" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544189453" />
                  <Point X="-24.56476953125" Y="-3.4796328125" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.770677734375" Y="-3.252429443359" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.05367578125" Y="-3.201736816406" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285643798828" />
                  <Point X="-25.3173671875" Y="-3.327554931641" />
                  <Point X="-25.452005859375" Y="-3.521544189453" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.788638671875" Y="-4.766494140625" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-25.855236328125" Y="-4.985622558594" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.1513203125" Y="-4.924923828125" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.317716796875" Y="-4.61611328125" />
                  <Point X="-26.3091484375" Y="-4.551039550781" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.320435546875" Y="-4.4806953125" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.427724609375" Y="-4.166284667969" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.704244140625" Y="-3.982157714844" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791259766" />
                  <Point X="-27.0357109375" Y="-4.004415039062" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157293945313" />
                  <Point X="-27.444671875" Y="-4.398311523438" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.496697265625" Y="-4.389979003906" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-27.926560546875" Y="-4.113153808594" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.63829296875" Y="-2.858201171875" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.500251953125" Y="-2.613671630859" />
                  <Point X="-27.50439453125" Y="-2.582190429688" />
                  <Point X="-27.517060546875" Y="-2.565682861328" />
                  <Point X="-27.531326171875" Y="-2.551416503906" />
                  <Point X="-27.560154296875" Y="-2.537198730469" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.652" Y="-3.15564453125" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-28.877966796875" Y="-3.219900634766" />
                  <Point X="-29.16169921875" Y="-2.847135742188" />
                  <Point X="-29.212408203125" Y="-2.762104980469" />
                  <Point X="-29.43101953125" Y="-2.395526855469" />
                  <Point X="-28.39519921875" Y="-1.600713989258" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.15253515625" Y="-1.411081665039" />
                  <Point X="-28.144453125" Y="-1.390728881836" />
                  <Point X="-28.1381171875" Y="-1.366266601562" />
                  <Point X="-28.136650390625" Y="-1.350051513672" />
                  <Point X="-28.14865625" Y="-1.321068847656" />
                  <Point X="-28.16586328125" Y="-1.307870239258" />
                  <Point X="-28.187640625" Y="-1.295052856445" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-29.558078125" Y="-1.464794555664" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.816419921875" Y="-1.445638549805" />
                  <Point X="-29.927392578125" Y="-1.011187744141" />
                  <Point X="-29.940806640625" Y="-0.91739453125" />
                  <Point X="-29.99839453125" Y="-0.5147421875" />
                  <Point X="-28.821076171875" Y="-0.199280441284" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.536962890625" Y="-0.118002891541" />
                  <Point X="-28.514140625" Y="-0.102163024902" />
                  <Point X="-28.5023203125" Y="-0.090641860962" />
                  <Point X="-28.49325390625" Y="-0.070612129211" />
                  <Point X="-28.485646484375" Y="-0.046100879669" />
                  <Point X="-28.483400390625" Y="-0.031284379959" />
                  <Point X="-28.4872890625" Y="-0.011163576126" />
                  <Point X="-28.494896484375" Y="0.013347678185" />
                  <Point X="-28.502322265625" Y="0.028085262299" />
                  <Point X="-28.519072265625" Y="0.043025104523" />
                  <Point X="-28.54189453125" Y="0.058864971161" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.777615234375" Y="0.393024078369" />
                  <Point X="-29.998185546875" Y="0.45212600708" />
                  <Point X="-29.9891640625" Y="0.513096008301" />
                  <Point X="-29.91764453125" Y="0.996414855957" />
                  <Point X="-29.890638671875" Y="1.096076538086" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-28.942228515625" Y="1.418857788086" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.720791015625" Y="1.399307495117" />
                  <Point X="-28.67027734375" Y="1.415234130859" />
                  <Point X="-28.651533203125" Y="1.426056396484" />
                  <Point X="-28.639119140625" Y="1.443786621094" />
                  <Point X="-28.634740234375" Y="1.454358520508" />
                  <Point X="-28.61447265625" Y="1.503290893555" />
                  <Point X="-28.610712890625" Y="1.524606079102" />
                  <Point X="-28.61631640625" Y="1.545512451172" />
                  <Point X="-28.621599609375" Y="1.555662475586" />
                  <Point X="-28.646056640625" Y="1.602642211914" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-29.359849609375" Y="2.156260253906" />
                  <Point X="-29.47610546875" Y="2.245466308594" />
                  <Point X="-29.4379140625" Y="2.3108984375" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-29.088478515625" Y="2.878954345703" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.275212890625" Y="2.993948974609" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.123314453125" Y="2.919104980469" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927404541016" />
                  <Point X="-28.002462890625" Y="2.938192871094" />
                  <Point X="-27.95252734375" Y="2.988127929688" />
                  <Point X="-27.9408984375" Y="3.006382080078" />
                  <Point X="-27.938072265625" Y="3.027840820312" />
                  <Point X="-27.93940234375" Y="3.043040039062" />
                  <Point X="-27.945556640625" Y="3.113390380859" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-28.262205078125" Y="3.671209472656" />
                  <Point X="-28.30727734375" Y="3.749276855469" />
                  <Point X="-28.236888671875" Y="3.803244140625" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.6402109375" Y="4.23692578125" />
                  <Point X="-27.141548828125" Y="4.513971679688" />
                  <Point X="-27.00335546875" Y="4.333874511719" />
                  <Point X="-26.967826171875" Y="4.287572753906" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.934330078125" Y="4.264854492188" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.7961875" Y="4.229549316406" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.69690234375" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.29448828125" />
                  <Point X="-26.68034765625" Y="4.312677246094" />
                  <Point X="-26.653802734375" Y="4.396864746094" />
                  <Point X="-26.651916015625" Y="4.418426269531" />
                  <Point X="-26.68717578125" Y="4.686247558594" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.594677734375" Y="4.727624023438" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.83151171875" Y="4.91928125" />
                  <Point X="-25.22419921875" Y="4.990358398438" />
                  <Point X="-25.07647265625" Y="4.439035644531" />
                  <Point X="-25.042140625" Y="4.310903808594" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176269531" />
                  <Point X="-24.945546875" Y="4.310902832031" />
                  <Point X="-24.786634765625" Y="4.903969726562" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.686892578125" Y="4.982861328125" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-24.02679296875" Y="4.898284179688" />
                  <Point X="-23.491541015625" Y="4.769057128906" />
                  <Point X="-23.41823828125" Y="4.742470703125" />
                  <Point X="-23.068970703125" Y="4.615788085938" />
                  <Point X="-22.99784375" Y="4.582524414062" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.5925859375" Y="4.385102050781" />
                  <Point X="-22.26747265625" Y="4.195688964844" />
                  <Point X="-22.202673828125" Y="4.149608886719" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.617064453125" Y="2.768742431641" />
                  <Point X="-22.77014453125" Y="2.50359765625" />
                  <Point X="-22.7789609375" Y="2.477251220703" />
                  <Point X="-22.7966171875" Y="2.411229003906" />
                  <Point X="-22.796466796875" Y="2.379987548828" />
                  <Point X="-22.789583984375" Y="2.322902099609" />
                  <Point X="-22.781318359375" Y="2.300812011719" />
                  <Point X="-22.773685546875" Y="2.289564208984" />
                  <Point X="-22.738359375" Y="2.237503417969" />
                  <Point X="-22.713810546875" Y="2.216571777344" />
                  <Point X="-22.66175" Y="2.181246337891" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.627328125" Y="2.171492919922" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.537072265625" Y="2.169760742188" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.2317265625" Y="2.900963867188" />
                  <Point X="-21.005751953125" Y="3.031430664062" />
                  <Point X="-20.990251953125" Y="3.009890625" />
                  <Point X="-20.79740234375" Y="2.741874267578" />
                  <Point X="-20.76128125" Y="2.682182373047" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.5098359375" Y="1.747734619141" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.73089453125" Y="1.570440307617" />
                  <Point X="-21.77841015625" Y="1.508451782227" />
                  <Point X="-21.790703125" Y="1.477827026367" />
                  <Point X="-21.808404296875" Y="1.414536499023" />
                  <Point X="-21.809220703125" Y="1.390965698242" />
                  <Point X="-21.806080078125" Y="1.375751708984" />
                  <Point X="-21.79155078125" Y="1.305332885742" />
                  <Point X="-21.775814453125" Y="1.274978271484" />
                  <Point X="-21.736296875" Y="1.214910522461" />
                  <Point X="-21.719052734375" Y="1.198819824219" />
                  <Point X="-21.7066796875" Y="1.191854858398" />
                  <Point X="-21.64941015625" Y="1.159617553711" />
                  <Point X="-21.614705078125" Y="1.151408569336" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.35840234375" Y="1.294651611328" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.14363671875" Y="1.291608032227" />
                  <Point X="-20.060806640625" Y="0.951367248535" />
                  <Point X="-20.04942578125" Y="0.878266479492" />
                  <Point X="-20.002140625" Y="0.574556396484" />
                  <Point X="-21.02785546875" Y="0.29971673584" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.28734765625" Y="0.223319076538" />
                  <Point X="-21.363421875" Y="0.179346984863" />
                  <Point X="-21.387595703125" Y="0.154360824585" />
                  <Point X="-21.433240234375" Y="0.096199020386" />
                  <Point X="-21.443013671875" Y="0.074735107422" />
                  <Point X="-21.44630078125" Y="0.057570766449" />
                  <Point X="-21.461515625" Y="-0.021875450134" />
                  <Point X="-21.458228515625" Y="-0.057848968506" />
                  <Point X="-21.443013671875" Y="-0.137295181274" />
                  <Point X="-21.433240234375" Y="-0.158759094238" />
                  <Point X="-21.42337890625" Y="-0.171324966431" />
                  <Point X="-21.377734375" Y="-0.22948677063" />
                  <Point X="-21.346986328125" Y="-0.251407180786" />
                  <Point X="-21.270912109375" Y="-0.295379425049" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.18974609375" Y="-0.586847900391" />
                  <Point X="-20.0019296875" Y="-0.637172668457" />
                  <Point X="-20.0053203125" Y="-0.659658203125" />
                  <Point X="-20.051568359375" Y="-0.966413024902" />
                  <Point X="-20.066150390625" Y="-1.030313354492" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-21.320150390625" Y="-1.132893554688" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.637419921875" Y="-1.105352539062" />
                  <Point X="-21.7867265625" Y="-1.137804931641" />
                  <Point X="-21.8145546875" Y="-1.154697265625" />
                  <Point X="-21.834052734375" Y="-1.178146972656" />
                  <Point X="-21.924298828125" Y="-1.286685424805" />
                  <Point X="-21.935640625" Y="-1.314070800781" />
                  <Point X="-21.938435546875" Y="-1.344439331055" />
                  <Point X="-21.951369140625" Y="-1.485001220703" />
                  <Point X="-21.943638671875" Y="-1.516621826172" />
                  <Point X="-21.925787109375" Y="-1.544389282227" />
                  <Point X="-21.843158203125" Y="-1.672912475586" />
                  <Point X="-21.831537109375" Y="-1.685540771484" />
                  <Point X="-20.839419921875" Y="-2.4468203125" />
                  <Point X="-20.660923828125" Y="-2.583784423828" />
                  <Point X="-20.665501953125" Y="-2.591192138672" />
                  <Point X="-20.7958671875" Y="-2.802140380859" />
                  <Point X="-20.82603125" Y="-2.845" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-22.00797265625" Y="-2.396955322266" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.301048828125" Y="-2.246379394531" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.54281640625" Y="-2.236030517578" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.33468359375" />
                  <Point X="-22.728185546875" Y="-2.366577636719" />
                  <Point X="-22.80587890625" Y="-2.514201660156" />
                  <Point X="-22.8108359375" Y="-2.546375" />
                  <Point X="-22.80390234375" Y="-2.584766845703" />
                  <Point X="-22.77180859375" Y="-2.762465332031" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.12837109375" Y="-3.882822998047" />
                  <Point X="-22.01332421875" Y="-4.082088867188" />
                  <Point X="-22.1646875" Y="-4.190202636719" />
                  <Point X="-22.198423828125" Y="-4.212041015625" />
                  <Point X="-22.32022265625" Y="-4.29087890625" />
                  <Point X="-23.13476953125" Y="-3.229342529297" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.367314453125" Y="-2.956123779297" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.61560546875" Y="-2.839527832031" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.866642578125" Y="-2.895096679688" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045986083984" />
                  <Point X="-24.041103515625" Y="-3.089973876953" />
                  <Point X="-24.085357421875" Y="-3.293573974609" />
                  <Point X="-24.0860703125" Y="-3.310719970703" />
                  <Point X="-23.905396484375" Y="-4.68306640625" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.036810546875" Y="-4.968907226562" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#134" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.034332864015" Y="4.483256472984" Z="0.45" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.45" />
                  <Point X="-0.843480619063" Y="5.000222398833" Z="0.45" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.45" />
                  <Point X="-1.614332013093" Y="4.807047800245" Z="0.45" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.45" />
                  <Point X="-1.742905199774" Y="4.711001854986" Z="0.45" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.45" />
                  <Point X="-1.734190241094" Y="4.358992850533" Z="0.45" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.45" />
                  <Point X="-1.821479282605" Y="4.307022986653" Z="0.45" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.45" />
                  <Point X="-1.917398592442" Y="4.340485031035" Z="0.45" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.45" />
                  <Point X="-1.969843752107" Y="4.395593050578" Z="0.45" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.45" />
                  <Point X="-2.670650300553" Y="4.311913135075" Z="0.45" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.45" />
                  <Point X="-3.273466801179" Y="3.874204010449" Z="0.45" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.45" />
                  <Point X="-3.311663736356" Y="3.677489470242" Z="0.45" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.45" />
                  <Point X="-2.995369779718" Y="3.069962712144" Z="0.45" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.45" />
                  <Point X="-3.043975085552" Y="3.004828482334" Z="0.45" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.45" />
                  <Point X="-3.125113753463" Y="3.000194919455" Z="0.45" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.45" />
                  <Point X="-3.256369878055" Y="3.068530212389" Z="0.45" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.45" />
                  <Point X="-4.134099077038" Y="2.940936810086" Z="0.45" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.45" />
                  <Point X="-4.487251715715" Y="2.367383643488" Z="0.45" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.45" />
                  <Point X="-4.396444612471" Y="2.147872655727" Z="0.45" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.45" />
                  <Point X="-3.672106175733" Y="1.563854356768" Z="0.45" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.45" />
                  <Point X="-3.687090905442" Y="1.504771957906" Z="0.45" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.45" />
                  <Point X="-3.741982769352" Y="1.478273482031" Z="0.45" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.45" />
                  <Point X="-3.941860913068" Y="1.49971023579" Z="0.45" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.45" />
                  <Point X="-4.945055480665" Y="1.140433880613" Z="0.45" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.45" />
                  <Point X="-5.044781995129" Y="0.55169504314" Z="0.45" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.45" />
                  <Point X="-4.796713180603" Y="0.376007899355" Z="0.45" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.45" />
                  <Point X="-3.553738584093" Y="0.03322915556" Z="0.45" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.45" />
                  <Point X="-3.541200344844" Y="0.005295662601" Z="0.45" />
                  <Point X="-3.539556741714" Y="0" Z="0.45" />
                  <Point X="-3.547164256291" Y="-0.02451128846" Z="0.45" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.45" />
                  <Point X="-3.57163001103" Y="-0.045646827574" Z="0.45" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.45" />
                  <Point X="-3.84017460817" Y="-0.119704157401" Z="0.45" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.45" />
                  <Point X="-4.99646016249" Y="-0.893193587201" Z="0.45" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.45" />
                  <Point X="-4.871022888721" Y="-1.426732691204" Z="0.45" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.45" />
                  <Point X="-4.557709527767" Y="-1.48308680904" Z="0.45" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.45" />
                  <Point X="-3.197381343172" Y="-1.319680623083" Z="0.45" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.45" />
                  <Point X="-3.199011980054" Y="-1.346912093697" Z="0.45" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.45" />
                  <Point X="-3.431793447104" Y="-1.529766330591" Z="0.45" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.45" />
                  <Point X="-4.261507783135" Y="-2.756434628784" Z="0.45" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.45" />
                  <Point X="-3.923909370875" Y="-3.21890371937" Z="0.45" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.45" />
                  <Point X="-3.633157281851" Y="-3.167665719743" Z="0.45" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.45" />
                  <Point X="-2.558573203016" Y="-2.569757266116" Z="0.45" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.45" />
                  <Point X="-2.687751293426" Y="-2.801921100641" Z="0.45" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.45" />
                  <Point X="-2.963220686312" Y="-4.121490793656" Z="0.45" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.45" />
                  <Point X="-2.529176406828" Y="-4.401209589249" Z="0.45" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.45" />
                  <Point X="-2.411161618723" Y="-4.397469737687" Z="0.45" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.45" />
                  <Point X="-2.014087667929" Y="-4.014708156463" Z="0.45" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.45" />
                  <Point X="-1.71367011556" Y="-4.000770826133" Z="0.45" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.45" />
                  <Point X="-1.466848354894" Y="-4.172596468379" Z="0.45" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.45" />
                  <Point X="-1.375632165196" Y="-4.45917013316" Z="0.45" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.45" />
                  <Point X="-1.373445650327" Y="-4.578305902483" Z="0.45" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.45" />
                  <Point X="-1.169936989073" Y="-4.942066734772" Z="0.45" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.45" />
                  <Point X="-0.870920385443" Y="-5.003426490845" Z="0.45" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.45" />
                  <Point X="-0.746498660615" Y="-4.748155041593" Z="0.45" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.45" />
                  <Point X="-0.282447819757" Y="-3.324784005997" Z="0.45" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.45" />
                  <Point X="-0.045013131282" Y="-3.218209783249" Z="0.45" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.45" />
                  <Point X="0.208345948079" Y="-3.268902262039" Z="0.45" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.45" />
                  <Point X="0.387998039675" Y="-3.47686174977" Z="0.45" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.45" />
                  <Point X="0.488256199786" Y="-3.784380988222" Z="0.45" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.45" />
                  <Point X="0.965969618833" Y="-4.986821973809" Z="0.45" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.45" />
                  <Point X="1.145243268851" Y="-4.948727964023" Z="0.45" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.45" />
                  <Point X="1.13801861818" Y="-4.645259956099" Z="0.45" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.45" />
                  <Point X="1.001599091258" Y="-3.069313767264" Z="0.45" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.45" />
                  <Point X="1.159166876598" Y="-2.90226316244" Z="0.45" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.45" />
                  <Point X="1.382819023662" Y="-2.858037306907" Z="0.45" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.45" />
                  <Point X="1.599489244912" Y="-2.966901796977" Z="0.45" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.45" />
                  <Point X="1.819406304562" Y="-3.228500537964" Z="0.45" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.45" />
                  <Point X="2.822588520388" Y="-4.22273497403" Z="0.45" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.45" />
                  <Point X="3.012891384806" Y="-4.089129828809" Z="0.45" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.45" />
                  <Point X="2.908772968465" Y="-3.826543028274" Z="0.45" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.45" />
                  <Point X="2.23914464274" Y="-2.54459996668" Z="0.45" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.45" />
                  <Point X="2.309904536801" Y="-2.358584323774" Z="0.45" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.45" />
                  <Point X="2.474314054598" Y="-2.248996773166" Z="0.45" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.45" />
                  <Point X="2.683906836833" Y="-2.264303366751" Z="0.45" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.45" />
                  <Point X="2.960870568833" Y="-2.408976531809" Z="0.45" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.45" />
                  <Point X="4.208700587705" Y="-2.842497225805" Z="0.45" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.45" />
                  <Point X="4.370873356363" Y="-2.586197466459" Z="0.45" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.45" />
                  <Point X="4.184861382642" Y="-2.375872475861" Z="0.45" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.45" />
                  <Point X="3.110114378065" Y="-1.486069910876" Z="0.45" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.45" />
                  <Point X="3.10519696738" Y="-1.317740618281" Z="0.45" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.45" />
                  <Point X="3.198237809223" Y="-1.178833886695" Z="0.45" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.45" />
                  <Point X="3.367042168307" Y="-1.122931767198" Z="0.45" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.45" />
                  <Point X="3.667167141175" Y="-1.151185818388" Z="0.45" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.45" />
                  <Point X="4.976437809896" Y="-1.01015736536" Z="0.45" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.45" />
                  <Point X="5.037963392922" Y="-0.635832261691" Z="0.45" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.45" />
                  <Point X="4.817039064043" Y="-0.507271451699" Z="0.45" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.45" />
                  <Point X="3.671878534196" Y="-0.176838405643" Z="0.45" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.45" />
                  <Point X="3.609798215412" Y="-0.109176389044" Z="0.45" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.45" />
                  <Point X="3.584721890616" Y="-0.017164344549" Z="0.45" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.45" />
                  <Point X="3.596649559808" Y="0.079446186687" Z="0.45" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.45" />
                  <Point X="3.645581222988" Y="0.154772349531" Z="0.45" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.45" />
                  <Point X="3.731516880156" Y="0.211310438599" Z="0.45" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.45" />
                  <Point X="3.978928753264" Y="0.28270048107" Z="0.45" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.45" />
                  <Point X="4.993821428296" Y="0.917238515969" Z="0.45" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.45" />
                  <Point X="4.89878783736" Y="1.334714821748" Z="0.45" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.45" />
                  <Point X="4.628915675516" Y="1.375503856558" Z="0.45" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.45" />
                  <Point X="3.385689730269" Y="1.23225762344" Z="0.45" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.45" />
                  <Point X="3.311691534704" Y="1.266706113421" Z="0.45" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.45" />
                  <Point X="3.259799158735" Y="1.333738771697" Z="0.45" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.45" />
                  <Point X="3.236731195913" Y="1.417135222847" Z="0.45" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.45" />
                  <Point X="3.251291925168" Y="1.495639777648" Z="0.45" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.45" />
                  <Point X="3.302632268605" Y="1.571302437762" Z="0.45" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.45" />
                  <Point X="3.514444177421" Y="1.739346794695" Z="0.45" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.45" />
                  <Point X="4.27533875876" Y="2.739348723578" Z="0.45" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.45" />
                  <Point X="4.044176472672" Y="3.070373811586" Z="0.45" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.45" />
                  <Point X="3.737116360408" Y="2.975545171278" Z="0.45" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.45" />
                  <Point X="2.443855743561" Y="2.249343577718" Z="0.45" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.45" />
                  <Point X="2.372501181993" Y="2.252413228508" Z="0.45" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.45" />
                  <Point X="2.308105837878" Y="2.289226004833" Z="0.45" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.45" />
                  <Point X="2.261532595402" Y="2.348919022504" Z="0.45" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.45" />
                  <Point X="2.24701647425" Y="2.41725726184" Z="0.45" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.45" />
                  <Point X="2.263184364939" Y="2.495613832321" Z="0.45" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.45" />
                  <Point X="2.420080181581" Y="2.775022721547" Z="0.45" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.45" />
                  <Point X="2.820145221556" Y="4.221636065027" Z="0.45" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.45" />
                  <Point X="2.426426471989" Y="4.459584537241" Z="0.45" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.45" />
                  <Point X="2.01718165921" Y="4.659096440886" Z="0.45" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.45" />
                  <Point X="1.592652860634" Y="4.820753979845" Z="0.45" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.45" />
                  <Point X="0.978783593789" Y="4.978167688907" Z="0.45" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.45" />
                  <Point X="0.312158490307" Y="5.06386956812" Z="0.45" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.45" />
                  <Point X="0.158911697538" Y="4.948191032967" Z="0.45" />
                  <Point X="0" Y="4.355124473572" Z="0.45" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>