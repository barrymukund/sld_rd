<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#121" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="404" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000473632812" Y="0.004716308594" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.420724609375" Y="-3.572129882812" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.445236328125" Y="-3.491298583984" />
                  <Point X="-24.458984375" Y="-3.466324707031" />
                  <Point X="-24.464162109375" Y="-3.457971191406" />
                  <Point X="-24.62136328125" Y="-3.231476318359" />
                  <Point X="-24.642890625" Y="-3.207521972656" />
                  <Point X="-24.673755859375" Y="-3.1863984375" />
                  <Point X="-24.698595703125" Y="-3.175837890625" />
                  <Point X="-24.70760546875" Y="-3.172533935547" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.9796015625" Y="-3.091593017578" />
                  <Point X="-25.0138984375" Y="-3.09266015625" />
                  <Point X="-25.0388203125" Y="-3.098042236328" />
                  <Point X="-25.04692578125" Y="-3.100170898438" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.319505859375" Y="-3.188985107422" />
                  <Point X="-25.349138671875" Y="-3.212289794922" />
                  <Point X="-25.36718359375" Y="-3.233513671875" />
                  <Point X="-25.3728515625" Y="-3.240882324219" />
                  <Point X="-25.53005078125" Y="-3.467377441406" />
                  <Point X="-25.538185546875" Y="-3.481569335938" />
                  <Point X="-25.550990234375" Y="-3.5125234375" />
                  <Point X="-25.916583984375" Y="-4.876941894531" />
                  <Point X="-26.079333984375" Y="-4.8453515625" />
                  <Point X="-26.08690625" Y="-4.843403320312" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.216884765625" Y="-4.578045898438" />
                  <Point X="-26.2149609375" Y="-4.563442382812" />
                  <Point X="-26.21465625" Y="-4.5412421875" />
                  <Point X="-26.217603515625" Y="-4.512825195312" />
                  <Point X="-26.218921875" Y="-4.504091796875" />
                  <Point X="-26.277037109375" Y="-4.211932128906" />
                  <Point X="-26.2870390625" Y="-4.181754882812" />
                  <Point X="-26.3069453125" Y="-4.14992578125" />
                  <Point X="-26.326578125" Y="-4.129177734375" />
                  <Point X="-26.332943359375" Y="-4.123047851562" />
                  <Point X="-26.556904296875" Y="-3.926639160156" />
                  <Point X="-26.583205078125" Y="-3.908789550781" />
                  <Point X="-26.6183828125" Y="-3.89565234375" />
                  <Point X="-26.646591796875" Y="-3.891144287109" />
                  <Point X="-26.65537109375" Y="-3.890157226562" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.984353515625" Y="-3.872525634766" />
                  <Point X="-27.02025390625" Y="-3.883517089844" />
                  <Point X="-27.045376953125" Y="-3.897112304688" />
                  <Point X="-27.052943359375" Y="-3.901673828125" />
                  <Point X="-27.300623046875" Y="-4.067169677734" />
                  <Point X="-27.31278515625" Y="-4.076821533203" />
                  <Point X="-27.3351015625" Y="-4.099461425781" />
                  <Point X="-27.4801484375" Y="-4.288489257813" />
                  <Point X="-27.801712890625" Y="-4.089384033203" />
                  <Point X="-27.812181640625" Y="-4.081322753906" />
                  <Point X="-28.10472265625" Y="-3.856077392578" />
                  <Point X="-27.453443359375" Y="-2.728028564453" />
                  <Point X="-27.42376171875" Y="-2.676620117188" />
                  <Point X="-27.412859375" Y="-2.647659912109" />
                  <Point X="-27.406587890625" Y="-2.616142089844" />
                  <Point X="-27.4056328125" Y="-2.584763916016" />
                  <Point X="-27.41495703125" Y="-2.554787597656" />
                  <Point X="-27.429865234375" Y="-2.525258300781" />
                  <Point X="-27.447494140625" Y="-2.500898925781" />
                  <Point X="-27.464150390625" Y="-2.4842421875" />
                  <Point X="-27.48930859375" Y="-2.466213134766" />
                  <Point X="-27.518138671875" Y="-2.451995849609" />
                  <Point X="-27.547755859375" Y="-2.44301171875" />
                  <Point X="-27.578689453125" Y="-2.444024414062" />
                  <Point X="-27.61021484375" Y="-2.450295166016" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-29.082853515625" Y="-2.793868408203" />
                  <Point X="-29.0903671875" Y="-2.781270751953" />
                  <Point X="-29.306142578125" Y="-2.41944921875" />
                  <Point X="-28.15788671875" Y="-1.538361938477" />
                  <Point X="-28.105955078125" Y="-1.498513305664" />
                  <Point X="-28.083962890625" Y="-1.474653076172" />
                  <Point X="-28.065689453125" Y="-1.446334228516" />
                  <Point X="-28.053546875" Y="-1.418642456055" />
                  <Point X="-28.046150390625" Y="-1.390081298828" />
                  <Point X="-28.04334765625" Y="-1.359650024414" />
                  <Point X="-28.04555859375" Y="-1.327973266602" />
                  <Point X="-28.05278125" Y="-1.297700927734" />
                  <Point X="-28.069400390625" Y="-1.271387329102" />
                  <Point X="-28.09130078125" Y="-1.246809448242" />
                  <Point X="-28.114041015625" Y="-1.228138427734" />
                  <Point X="-28.139453125" Y="-1.213181518555" />
                  <Point X="-28.16871484375" Y="-1.20195715332" />
                  <Point X="-28.2006015625" Y="-1.195475097656" />
                  <Point X="-28.231927734375" Y="-1.194383789062" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.834076171875" Y="-0.992650939941" />
                  <Point X="-29.8360625" Y="-0.978768737793" />
                  <Point X="-29.892421875" Y="-0.584698303223" />
                  <Point X="-28.592033203125" Y="-0.236259490967" />
                  <Point X="-28.532875" Y="-0.220408325195" />
                  <Point X="-28.516419921875" Y="-0.214322296143" />
                  <Point X="-28.486619140625" Y="-0.198701400757" />
                  <Point X="-28.45997265625" Y="-0.180207260132" />
                  <Point X="-28.43684765625" Y="-0.157397872925" />
                  <Point X="-28.417234375" Y="-0.129952362061" />
                  <Point X="-28.403794921875" Y="-0.10287310791" />
                  <Point X="-28.3949140625" Y="-0.074254554749" />
                  <Point X="-28.390654296875" Y="-0.044949760437" />
                  <Point X="-28.39102734375" Y="-0.014110663414" />
                  <Point X="-28.395287109375" Y="0.012896483421" />
                  <Point X="-28.4041640625" Y="0.041501983643" />
                  <Point X="-28.419017578125" Y="0.070507759094" />
                  <Point X="-28.439384765625" Y="0.09754359436" />
                  <Point X="-28.461095703125" Y="0.118425994873" />
                  <Point X="-28.4877265625" Y="0.136909362793" />
                  <Point X="-28.50192578125" Y="0.145047744751" />
                  <Point X="-28.532875" Y="0.157848236084" />
                  <Point X="-29.891814453125" Y="0.521975585938" />
                  <Point X="-29.824486328125" Y="0.976980895996" />
                  <Point X="-29.82048828125" Y="0.991736633301" />
                  <Point X="-29.703552734375" Y="1.423267944336" />
                  <Point X="-28.8080703125" Y="1.305375854492" />
                  <Point X="-28.7656640625" Y="1.29979284668" />
                  <Point X="-28.74497265625" Y="1.299342651367" />
                  <Point X="-28.72340234375" Y="1.301232299805" />
                  <Point X="-28.7031171875" Y="1.30526953125" />
                  <Point X="-28.7006796875" Y="1.306038574219" />
                  <Point X="-28.6417109375" Y="1.324630859375" />
                  <Point X="-28.62277734375" Y="1.332962036133" />
                  <Point X="-28.604033203125" Y="1.343783813477" />
                  <Point X="-28.5873515625" Y="1.356015869141" />
                  <Point X="-28.573712890625" Y="1.371568847656" />
                  <Point X="-28.561298828125" Y="1.389298950195" />
                  <Point X="-28.55134765625" Y="1.407440063477" />
                  <Point X="-28.550365234375" Y="1.4098125" />
                  <Point X="-28.526701171875" Y="1.466944335938" />
                  <Point X="-28.520916015625" Y="1.486788330078" />
                  <Point X="-28.51715625" Y="1.508103515625" />
                  <Point X="-28.515802734375" Y="1.528750244141" />
                  <Point X="-28.518951171875" Y="1.549200195312" />
                  <Point X="-28.5245546875" Y="1.570106689453" />
                  <Point X="-28.532046875" Y="1.589371826172" />
                  <Point X="-28.533232421875" Y="1.591649658203" />
                  <Point X="-28.561787109375" Y="1.646501708984" />
                  <Point X="-28.57328125" Y="1.663706054688" />
                  <Point X="-28.587193359375" Y="1.680286132812" />
                  <Point X="-28.60213671875" Y="1.694590576172" />
                  <Point X="-29.351859375" Y="2.269873046875" />
                  <Point X="-29.081150390625" Y="2.733661132812" />
                  <Point X="-29.070560546875" Y="2.747274414062" />
                  <Point X="-28.75050390625" Y="3.158661376953" />
                  <Point X="-28.232701171875" Y="2.859707519531" />
                  <Point X="-28.20665625" Y="2.844670654297" />
                  <Point X="-28.18772265625" Y="2.836339599609" />
                  <Point X="-28.167080078125" Y="2.829831298828" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.1433828125" Y="2.825498046875" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040568359375" Y="2.818762451172" />
                  <Point X="-28.019115234375" Y="2.8215859375" />
                  <Point X="-27.99902734375" Y="2.826499755859" />
                  <Point X="-27.98048046875" Y="2.835643554688" />
                  <Point X="-27.962228515625" Y="2.847267333984" />
                  <Point X="-27.946107421875" Y="2.860198730469" />
                  <Point X="-27.943654296875" Y="2.862650146484" />
                  <Point X="-27.8853515625" Y="2.920952636719" />
                  <Point X="-27.872400390625" Y="2.937091552734" />
                  <Point X="-27.860771484375" Y="2.955348876953" />
                  <Point X="-27.851625" Y="2.973899902344" />
                  <Point X="-27.8467109375" Y="2.993990966797" />
                  <Point X="-27.84388671875" Y="3.015452880859" />
                  <Point X="-27.84343359375" Y="3.036099853516" />
                  <Point X="-27.843732421875" Y="3.039530761719" />
                  <Point X="-27.85091796875" Y="3.121669433594" />
                  <Point X="-27.854955078125" Y="3.141961669922" />
                  <Point X="-27.86146484375" Y="3.162604736328" />
                  <Point X="-27.869794921875" Y="3.181533447266" />
                  <Point X="-28.18333203125" Y="3.724596679688" />
                  <Point X="-27.700625" Y="4.094683349609" />
                  <Point X="-27.68394921875" Y="4.103947753906" />
                  <Point X="-27.16703515625" Y="4.391133789062" />
                  <Point X="-27.05116796875" Y="4.240132324219" />
                  <Point X="-27.0431953125" Y="4.229741699219" />
                  <Point X="-27.02889453125" Y="4.214802734375" />
                  <Point X="-27.012314453125" Y="4.200889160156" />
                  <Point X="-26.995103515625" Y="4.189390625" />
                  <Point X="-26.991306640625" Y="4.187414550781" />
                  <Point X="-26.89988671875" Y="4.139823730469" />
                  <Point X="-26.88061328125" Y="4.132330078125" />
                  <Point X="-26.859703125" Y="4.126728027344" />
                  <Point X="-26.839255859375" Y="4.123581542969" />
                  <Point X="-26.81861328125" Y="4.124936523438" />
                  <Point X="-26.79729296875" Y="4.128698242188" />
                  <Point X="-26.7774296875" Y="4.134490722656" />
                  <Point X="-26.773490234375" Y="4.136123046875" />
                  <Point X="-26.67827734375" Y="4.175561523437" />
                  <Point X="-26.66014453125" Y="4.185509765625" />
                  <Point X="-26.6424140625" Y="4.197924316406" />
                  <Point X="-26.626859375" Y="4.211565429688" />
                  <Point X="-26.614626953125" Y="4.22825" />
                  <Point X="-26.603806640625" Y="4.246994140625" />
                  <Point X="-26.595478515625" Y="4.265920898438" />
                  <Point X="-26.59419140625" Y="4.270002929688" />
                  <Point X="-26.56319921875" Y="4.368297363281" />
                  <Point X="-26.5591640625" Y="4.388583496094" />
                  <Point X="-26.55727734375" Y="4.410143066406" />
                  <Point X="-26.557728515625" Y="4.430824707031" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-25.9496328125" Y="4.80980859375" />
                  <Point X="-25.92942578125" Y="4.812173339844" />
                  <Point X="-25.2947109375" Y="4.886457519531" />
                  <Point X="-25.141607421875" Y="4.315069824219" />
                  <Point X="-25.133904296875" Y="4.286317871094" />
                  <Point X="-25.1211328125" Y="4.258129882812" />
                  <Point X="-25.103275390625" Y="4.231401855469" />
                  <Point X="-25.082119140625" Y="4.208811523438" />
                  <Point X="-25.05482421875" Y="4.194220703125" />
                  <Point X="-25.02438671875" Y="4.183887207031" />
                  <Point X="-24.99384765625" Y="4.178844238281" />
                  <Point X="-24.963310546875" Y="4.183885253906" />
                  <Point X="-24.93287109375" Y="4.194217285156" />
                  <Point X="-24.905576171875" Y="4.208806152344" />
                  <Point X="-24.88441796875" Y="4.23139453125" />
                  <Point X="-24.86655859375" Y="4.25812109375" />
                  <Point X="-24.853783203125" Y="4.286314941406" />
                  <Point X="-24.692578125" Y="4.887937988281" />
                  <Point X="-24.155958984375" Y="4.831738769531" />
                  <Point X="-24.13923046875" Y="4.827700195313" />
                  <Point X="-23.518970703125" Y="4.677950683594" />
                  <Point X="-23.509509765625" Y="4.674519042969" />
                  <Point X="-23.105359375" Y="4.527930664062" />
                  <Point X="-23.09482421875" Y="4.523003417969" />
                  <Point X="-22.7054296875" Y="4.340896972656" />
                  <Point X="-22.695216796875" Y="4.334946777344" />
                  <Point X="-22.319013671875" Y="4.115770019531" />
                  <Point X="-22.309421875" Y="4.10894921875" />
                  <Point X="-22.05673828125" Y="3.929254150391" />
                  <Point X="-22.8180625" Y="2.610599609375" />
                  <Point X="-22.852416015625" Y="2.551098144531" />
                  <Point X="-22.858849609375" Y="2.537603271484" />
                  <Point X="-22.867779296875" Y="2.512854980469" />
                  <Point X="-22.888392578125" Y="2.43576953125" />
                  <Point X="-22.891515625" Y="2.415624511719" />
                  <Point X="-22.89251953125" Y="2.393954833984" />
                  <Point X="-22.8919375" Y="2.378186279297" />
                  <Point X="-22.883900390625" Y="2.311529296875" />
                  <Point X="-22.878556640625" Y="2.289601318359" />
                  <Point X="-22.8702890625" Y="2.26751171875" />
                  <Point X="-22.85992578125" Y="2.247467773438" />
                  <Point X="-22.858212890625" Y="2.244943603516" />
                  <Point X="-22.816966796875" Y="2.184159179688" />
                  <Point X="-22.80374609375" Y="2.168587646484" />
                  <Point X="-22.787923828125" Y="2.153575195312" />
                  <Point X="-22.775875" Y="2.143879150391" />
                  <Point X="-22.71508984375" Y="2.102634521484" />
                  <Point X="-22.695046875" Y="2.092272216797" />
                  <Point X="-22.672958984375" Y="2.084006347656" />
                  <Point X="-22.65104296875" Y="2.078664550781" />
                  <Point X="-22.648275390625" Y="2.078330566406" />
                  <Point X="-22.581619140625" Y="2.07029296875" />
                  <Point X="-22.56085546875" Y="2.070073242188" />
                  <Point X="-22.538751953125" Y="2.072267089844" />
                  <Point X="-22.52359375" Y="2.075027099609" />
                  <Point X="-22.4465078125" Y="2.095640625" />
                  <Point X="-22.43471875" Y="2.099636962891" />
                  <Point X="-22.41146484375" Y="2.110144775391" />
                  <Point X="-21.032671875" Y="2.90619140625" />
                  <Point X="-20.876720703125" Y="2.689456542969" />
                  <Point X="-20.871375" Y="2.680620605469" />
                  <Point X="-20.737798828125" Y="2.459884277344" />
                  <Point X="-21.723984375" Y="1.703157226563" />
                  <Point X="-21.76921484375" Y="1.668451782227" />
                  <Point X="-21.780521484375" Y="1.658235473633" />
                  <Point X="-21.798328125" Y="1.638622314453" />
                  <Point X="-21.853806640625" Y="1.566246582031" />
                  <Point X="-21.864521484375" Y="1.54857421875" />
                  <Point X="-21.873849609375" Y="1.528554077148" />
                  <Point X="-21.879228515625" Y="1.514017944336" />
                  <Point X="-21.89989453125" Y="1.440122070312" />
                  <Point X="-21.90334765625" Y="1.417815429688" />
                  <Point X="-21.904162109375" Y="1.394236450195" />
                  <Point X="-21.902255859375" Y="1.371749755859" />
                  <Point X="-21.901552734375" Y="1.36834387207" />
                  <Point X="-21.88458984375" Y="1.286134765625" />
                  <Point X="-21.8782109375" Y="1.266408081055" />
                  <Point X="-21.86909765625" Y="1.246118652344" />
                  <Point X="-21.861802734375" Y="1.232829223633" />
                  <Point X="-21.815662109375" Y="1.162696166992" />
                  <Point X="-21.8011015625" Y="1.14544543457" />
                  <Point X="-21.7838515625" Y="1.129352661133" />
                  <Point X="-21.7656484375" Y="1.116033081055" />
                  <Point X="-21.762876953125" Y="1.114473022461" />
                  <Point X="-21.69601171875" Y="1.076833496094" />
                  <Point X="-21.676611328125" Y="1.068595336914" />
                  <Point X="-21.654880859375" Y="1.062101196289" />
                  <Point X="-21.640126953125" Y="1.058942382812" />
                  <Point X="-21.54971875" Y="1.046993896484" />
                  <Point X="-21.537294921875" Y="1.046174926758" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.223162109375" Y="1.216636474609" />
                  <Point X="-20.154060546875" Y="0.932789550781" />
                  <Point X="-20.152376953125" Y="0.921975769043" />
                  <Point X="-20.109134765625" Y="0.644238647461" />
                  <Point X="-21.2315859375" Y="0.34347857666" />
                  <Point X="-21.283419921875" Y="0.329589904785" />
                  <Point X="-21.298017578125" Y="0.324368774414" />
                  <Point X="-21.322140625" Y="0.312936248779" />
                  <Point X="-21.410962890625" Y="0.26159588623" />
                  <Point X="-21.427826171875" Y="0.2491824646" />
                  <Point X="-21.4443515625" Y="0.233942108154" />
                  <Point X="-21.454681640625" Y="0.222756820679" />
                  <Point X="-21.507974609375" Y="0.154848999023" />
                  <Point X="-21.519697265625" Y="0.135572387695" />
                  <Point X="-21.52946875" Y="0.114116065979" />
                  <Point X="-21.536310546875" Y="0.092636360168" />
                  <Point X="-21.537056640625" Y="0.08875113678" />
                  <Point X="-21.5548203125" Y="-0.004007497787" />
                  <Point X="-21.55646484375" Y="-0.024969139099" />
                  <Point X="-21.5557265625" Y="-0.047629619598" />
                  <Point X="-21.55408203125" Y="-0.062404285431" />
                  <Point X="-21.536318359375" Y="-0.155163070679" />
                  <Point X="-21.52947265625" Y="-0.176662811279" />
                  <Point X="-21.51969921875" Y="-0.198127029419" />
                  <Point X="-21.5079765625" Y="-0.217406677246" />
                  <Point X="-21.505763671875" Y="-0.220226638794" />
                  <Point X="-21.452470703125" Y="-0.288134307861" />
                  <Point X="-21.4374609375" Y="-0.30336328125" />
                  <Point X="-21.4194609375" Y="-0.317915588379" />
                  <Point X="-21.407275390625" Y="-0.326287872314" />
                  <Point X="-21.318453125" Y="-0.377628234863" />
                  <Point X="-21.307291015625" Y="-0.383138366699" />
                  <Point X="-21.283419921875" Y="-0.392150024414" />
                  <Point X="-20.108525390625" Y="-0.706961975098" />
                  <Point X="-20.144974609375" Y="-0.948720275879" />
                  <Point X="-20.147134765625" Y="-0.958186279297" />
                  <Point X="-20.19882421875" Y="-1.184698852539" />
                  <Point X="-21.51550390625" Y="-1.011354797363" />
                  <Point X="-21.575615234375" Y="-1.003440917969" />
                  <Point X="-21.596884765625" Y="-1.00304309082" />
                  <Point X="-21.62126953125" Y="-1.005329650879" />
                  <Point X="-21.632578125" Y="-1.007082214355" />
                  <Point X="-21.806904296875" Y="-1.044972290039" />
                  <Point X="-21.8372734375" Y="-1.055693725586" />
                  <Point X="-21.867494140625" Y="-1.07505859375" />
                  <Point X="-21.884369140625" Y="-1.091094970703" />
                  <Point X="-21.891974609375" Y="-1.099222167969" />
                  <Point X="-21.99734375" Y="-1.225947753906" />
                  <Point X="-22.01337109375" Y="-1.250416015625" />
                  <Point X="-22.0252421875" Y="-1.281100585938" />
                  <Point X="-22.02946484375" Y="-1.302468261719" />
                  <Point X="-22.0308671875" Y="-1.312180786133" />
                  <Point X="-22.04596875" Y="-1.476295898438" />
                  <Point X="-22.044888671875" Y="-1.508485473633" />
                  <Point X="-22.03540625" Y="-1.543362670898" />
                  <Point X="-22.024890625" Y="-1.56475769043" />
                  <Point X="-22.01954296875" Y="-1.574228149414" />
                  <Point X="-21.923068359375" Y="-1.724287597656" />
                  <Point X="-21.9130625" Y="-1.73724230957" />
                  <Point X="-21.889369140625" Y="-1.760909301758" />
                  <Point X="-20.786876953125" Y="-2.606881347656" />
                  <Point X="-20.875203125" Y="-2.749805175781" />
                  <Point X="-20.879662109375" Y="-2.756142822266" />
                  <Point X="-20.971017578125" Y="-2.8859453125" />
                  <Point X="-22.145505859375" Y="-2.207853271484" />
                  <Point X="-22.199044921875" Y="-2.176942871094" />
                  <Point X="-22.219111328125" Y="-2.168262451172" />
                  <Point X="-22.24383984375" Y="-2.160803955078" />
                  <Point X="-22.254390625" Y="-2.158269287109" />
                  <Point X="-22.461865234375" Y="-2.120799560547" />
                  <Point X="-22.494025390625" Y="-2.119082275391" />
                  <Point X="-22.529880859375" Y="-2.125611328125" />
                  <Point X="-22.5527109375" Y="-2.134548583984" />
                  <Point X="-22.56232421875" Y="-2.138943847656" />
                  <Point X="-22.73468359375" Y="-2.229656005859" />
                  <Point X="-22.75885546875" Y="-2.246129638672" />
                  <Point X="-22.78185546875" Y="-2.270374267578" />
                  <Point X="-22.794513671875" Y="-2.289600585938" />
                  <Point X="-22.799234375" Y="-2.297596435547" />
                  <Point X="-22.889947265625" Y="-2.46995703125" />
                  <Point X="-22.901265625" Y="-2.500111328125" />
                  <Point X="-22.905814453125" Y="-2.536549560547" />
                  <Point X="-22.904044921875" Y="-2.561666503906" />
                  <Point X="-22.902767578125" Y="-2.571874511719" />
                  <Point X="-22.865296875" Y="-2.779349365234" />
                  <Point X="-22.86101171875" Y="-2.795140136719" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.13871484375" Y="-4.054905273438" />
                  <Point X="-22.218142578125" Y="-4.111639160156" />
                  <Point X="-22.223142578125" Y="-4.114875" />
                  <Point X="-22.298234375" Y="-4.16348046875" />
                  <Point X="-23.20059765625" Y="-2.987498291016" />
                  <Point X="-23.241451171875" Y="-2.934255615234" />
                  <Point X="-23.2570859375" Y="-2.918217041016" />
                  <Point X="-23.278212890625" Y="-2.901133300781" />
                  <Point X="-23.286572265625" Y="-2.895093994141" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.520255859375" Y="-2.749644287109" />
                  <Point X="-23.556392578125" Y="-2.741946777344" />
                  <Point X="-23.582142578125" Y="-2.741581787109" />
                  <Point X="-23.592193359375" Y="-2.741971923828" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.844619140625" Y="-2.768534667969" />
                  <Point X="-23.875544921875" Y="-2.782402099609" />
                  <Point X="-23.895552734375" Y="-2.796116210938" />
                  <Point X="-23.902578125" Y="-2.801427490234" />
                  <Point X="-24.07538671875" Y="-2.945111328125" />
                  <Point X="-24.09738671875" Y="-2.968641113281" />
                  <Point X="-24.115697265625" Y="-3.001061767578" />
                  <Point X="-24.123958984375" Y="-3.026257324219" />
                  <Point X="-24.12651953125" Y="-3.035679443359" />
                  <Point X="-24.178189453125" Y="-3.273395996094" />
                  <Point X="-24.180275390625" Y="-3.289627441406" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-23.977935546875" Y="-4.859915527344" />
                  <Point X="-24.024314453125" Y="-4.87008203125" />
                  <Point X="-24.02894140625" Y="-4.870922363281" />
                  <Point X="-24.07068359375" Y="-4.878505371094" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.058427734375" Y="-4.75263671875" />
                  <Point X="-26.063234375" Y="-4.751399902344" />
                  <Point X="-26.141244140625" Y="-4.731328613281" />
                  <Point X="-26.122697265625" Y="-4.590445800781" />
                  <Point X="-26.119970703125" Y="-4.56474609375" />
                  <Point X="-26.119666015625" Y="-4.542545898438" />
                  <Point X="-26.1201640625" Y="-4.531441894531" />
                  <Point X="-26.123111328125" Y="-4.503024902344" />
                  <Point X="-26.125748046875" Y="-4.485558105469" />
                  <Point X="-26.18386328125" Y="-4.1933984375" />
                  <Point X="-26.186861328125" Y="-4.182043945313" />
                  <Point X="-26.19686328125" Y="-4.151866699219" />
                  <Point X="-26.206494140625" Y="-4.131381347656" />
                  <Point X="-26.226400390625" Y="-4.099552246094" />
                  <Point X="-26.23794140625" Y="-4.084630859375" />
                  <Point X="-26.25757421875" Y="-4.0638828125" />
                  <Point X="-26.2703046875" Y="-4.051623046875" />
                  <Point X="-26.494265625" Y="-3.855214355469" />
                  <Point X="-26.503556640625" Y="-3.848032714844" />
                  <Point X="-26.529857421875" Y="-3.830183105469" />
                  <Point X="-26.54996875" Y="-3.81979296875" />
                  <Point X="-26.585146484375" Y="-3.806655761719" />
                  <Point X="-26.603390625" Y="-3.801842773438" />
                  <Point X="-26.631599609375" Y="-3.797334716797" />
                  <Point X="-26.649158203125" Y="-3.795360595703" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9581484375" Y="-3.7758359375" />
                  <Point X="-26.989884765625" Y="-3.777686767578" />
                  <Point X="-27.0121640625" Y="-3.781687744141" />
                  <Point X="-27.048064453125" Y="-3.792679199219" />
                  <Point X="-27.065466796875" Y="-3.799966064453" />
                  <Point X="-27.09058984375" Y="-3.813561279297" />
                  <Point X="-27.10572265625" Y="-3.822684326172" />
                  <Point X="-27.35340234375" Y="-3.988180175781" />
                  <Point X="-27.359677734375" Y="-3.992755371094" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032771484375" />
                  <Point X="-27.410470703125" Y="-4.04162890625" />
                  <Point X="-27.503203125" Y="-4.162478515625" />
                  <Point X="-27.7475859375" Y="-4.011160888672" />
                  <Point X="-27.754220703125" Y="-4.006052734375" />
                  <Point X="-27.98086328125" Y="-3.831546142578" />
                  <Point X="-27.371171875" Y="-2.775528564453" />
                  <Point X="-27.341490234375" Y="-2.724120117188" />
                  <Point X="-27.334853515625" Y="-2.710090576172" />
                  <Point X="-27.323951171875" Y="-2.681130371094" />
                  <Point X="-27.319685546875" Y="-2.666199707031" />
                  <Point X="-27.3134140625" Y="-2.634681884766" />
                  <Point X="-27.3116328125" Y="-2.619032226562" />
                  <Point X="-27.310677734375" Y="-2.587654052734" />
                  <Point X="-27.314919921875" Y="-2.556547363281" />
                  <Point X="-27.324244140625" Y="-2.526571044922" />
                  <Point X="-27.33015234375" Y="-2.511972900391" />
                  <Point X="-27.345060546875" Y="-2.482443603516" />
                  <Point X="-27.352904296875" Y="-2.469562011719" />
                  <Point X="-27.370533203125" Y="-2.445202636719" />
                  <Point X="-27.380318359375" Y="-2.433724853516" />
                  <Point X="-27.396974609375" Y="-2.417068115234" />
                  <Point X="-27.4088125" Y="-2.407023193359" />
                  <Point X="-27.433970703125" Y="-2.388994140625" />
                  <Point X="-27.447291015625" Y="-2.381010009766" />
                  <Point X="-27.47612109375" Y="-2.366792724609" />
                  <Point X="-27.4905625" Y="-2.361086425781" />
                  <Point X="-27.5201796875" Y="-2.352102294922" />
                  <Point X="-27.550865234375" Y="-2.3480625" />
                  <Point X="-27.581798828125" Y="-2.349075195312" />
                  <Point X="-27.59722265625" Y="-2.350849853516" />
                  <Point X="-27.628748046875" Y="-2.357120605469" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372285888672" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.793087890625" Y="-3.017708251953" />
                  <Point X="-29.004013671875" Y="-2.740596191406" />
                  <Point X="-29.00877734375" Y="-2.732607666016" />
                  <Point X="-29.181265625" Y="-2.443372314453" />
                  <Point X="-28.1000546875" Y="-1.61373046875" />
                  <Point X="-28.048123046875" Y="-1.573881835938" />
                  <Point X="-28.0361015625" Y="-1.56289831543" />
                  <Point X="-28.014109375" Y="-1.539038085938" />
                  <Point X="-28.004138671875" Y="-1.526161499023" />
                  <Point X="-27.985865234375" Y="-1.497842651367" />
                  <Point X="-27.978685546875" Y="-1.48448425293" />
                  <Point X="-27.96654296875" Y="-1.456792602539" />
                  <Point X="-27.961580078125" Y="-1.442458984375" />
                  <Point X="-27.95418359375" Y="-1.413897827148" />
                  <Point X="-27.95155078125" Y="-1.398793945312" />
                  <Point X="-27.948748046875" Y="-1.368362670898" />
                  <Point X="-27.948578125" Y="-1.353035400391" />
                  <Point X="-27.9507890625" Y="-1.321358642578" />
                  <Point X="-27.95315234375" Y="-1.305926025391" />
                  <Point X="-27.960375" Y="-1.275653808594" />
                  <Point X="-27.972458984375" Y="-1.246971557617" />
                  <Point X="-27.989078125" Y="-1.220657958984" />
                  <Point X="-27.99847265625" Y="-1.208186767578" />
                  <Point X="-28.020373046875" Y="-1.183608886719" />
                  <Point X="-28.031017578125" Y="-1.173387207031" />
                  <Point X="-28.0537578125" Y="-1.154716064453" />
                  <Point X="-28.065853515625" Y="-1.146266845703" />
                  <Point X="-28.091265625" Y="-1.131309936523" />
                  <Point X="-28.1054296875" Y="-1.124483032227" />
                  <Point X="-28.13469140625" Y="-1.113258789062" />
                  <Point X="-28.1497890625" Y="-1.108861206055" />
                  <Point X="-28.18167578125" Y="-1.102379150391" />
                  <Point X="-28.197294921875" Y="-1.100532592773" />
                  <Point X="-28.22862109375" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196411133" />
                  <Point X="-29.66091796875" Y="-1.286694335938" />
                  <Point X="-29.740759765625" Y="-0.974118774414" />
                  <Point X="-29.74201953125" Y="-0.965312683105" />
                  <Point X="-29.78644921875" Y="-0.654654296875" />
                  <Point X="-28.5674453125" Y="-0.328022460938" />
                  <Point X="-28.508287109375" Y="-0.312171234131" />
                  <Point X="-28.499919921875" Y="-0.309509399414" />
                  <Point X="-28.472314453125" Y="-0.298463592529" />
                  <Point X="-28.442513671875" Y="-0.28284274292" />
                  <Point X="-28.432451171875" Y="-0.276745788574" />
                  <Point X="-28.4058046875" Y="-0.258251586914" />
                  <Point X="-28.393259765625" Y="-0.247842346191" />
                  <Point X="-28.370134765625" Y="-0.225033035278" />
                  <Point X="-28.3595546875" Y="-0.212632980347" />
                  <Point X="-28.33994140625" Y="-0.185187393188" />
                  <Point X="-28.332138671875" Y="-0.172185714722" />
                  <Point X="-28.31869921875" Y="-0.145106323242" />
                  <Point X="-28.3130625" Y="-0.131028900146" />
                  <Point X="-28.304181640625" Y="-0.102410232544" />
                  <Point X="-28.30090234375" Y="-0.08792024231" />
                  <Point X="-28.296642578125" Y="-0.058615371704" />
                  <Point X="-28.295662109375" Y="-0.043800640106" />
                  <Point X="-28.29603515625" Y="-0.012961535454" />
                  <Point X="-28.2971875" Y="0.000690518022" />
                  <Point X="-28.301447265625" Y="0.027697528839" />
                  <Point X="-28.3045546875" Y="0.041052635193" />
                  <Point X="-28.313431640625" Y="0.069658081055" />
                  <Point X="-28.31960546875" Y="0.08480305481" />
                  <Point X="-28.334458984375" Y="0.113808746338" />
                  <Point X="-28.343138671875" Y="0.127669914246" />
                  <Point X="-28.363505859375" Y="0.154705749512" />
                  <Point X="-28.373529296875" Y="0.166012542725" />
                  <Point X="-28.395240234375" Y="0.186894943237" />
                  <Point X="-28.406927734375" Y="0.196470275879" />
                  <Point X="-28.43355859375" Y="0.214953613281" />
                  <Point X="-28.440486328125" Y="0.219331008911" />
                  <Point X="-28.4656171875" Y="0.232835479736" />
                  <Point X="-28.49656640625" Y="0.245635925293" />
                  <Point X="-28.508287109375" Y="0.24961114502" />
                  <Point X="-29.7854453125" Y="0.591825073242" />
                  <Point X="-29.731328125" Y="0.957540893555" />
                  <Point X="-29.728794921875" Y="0.966892333984" />
                  <Point X="-29.633587890625" Y="1.318237182617" />
                  <Point X="-28.820470703125" Y="1.211188476562" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.76773046875" Y="1.204815307617" />
                  <Point X="-28.7470390625" Y="1.204365112305" />
                  <Point X="-28.736681640625" Y="1.204705200195" />
                  <Point X="-28.715111328125" Y="1.206594726562" />
                  <Point X="-28.704859375" Y="1.208059692383" />
                  <Point X="-28.68457421875" Y="1.212096923828" />
                  <Point X="-28.672095703125" Y="1.215440795898" />
                  <Point X="-28.61314453125" Y="1.234027587891" />
                  <Point X="-28.60344921875" Y="1.237676635742" />
                  <Point X="-28.584515625" Y="1.2460078125" />
                  <Point X="-28.57527734375" Y="1.250689331055" />
                  <Point X="-28.556533203125" Y="1.261511108398" />
                  <Point X="-28.547857421875" Y="1.267172851562" />
                  <Point X="-28.53117578125" Y="1.279404907227" />
                  <Point X="-28.515923828125" Y="1.293380371094" />
                  <Point X="-28.50228515625" Y="1.308933349609" />
                  <Point X="-28.495892578125" Y="1.317081054688" />
                  <Point X="-28.483478515625" Y="1.334811157227" />
                  <Point X="-28.4780078125" Y="1.343609863281" />
                  <Point X="-28.468056640625" Y="1.361750976562" />
                  <Point X="-28.46259375" Y="1.373466186523" />
                  <Point X="-28.4389296875" Y="1.430598022461" />
                  <Point X="-28.435498046875" Y="1.440355712891" />
                  <Point X="-28.429712890625" Y="1.460199707031" />
                  <Point X="-28.427359375" Y="1.470286132812" />
                  <Point X="-28.423599609375" Y="1.491601318359" />
                  <Point X="-28.422359375" Y="1.501888916016" />
                  <Point X="-28.421005859375" Y="1.522535766602" />
                  <Point X="-28.421908203125" Y="1.543206054688" />
                  <Point X="-28.425056640625" Y="1.563655883789" />
                  <Point X="-28.427189453125" Y="1.573794677734" />
                  <Point X="-28.43279296875" Y="1.594701171875" />
                  <Point X="-28.436013671875" Y="1.604539794922" />
                  <Point X="-28.443505859375" Y="1.623804931641" />
                  <Point X="-28.448962890625" Y="1.635509399414" />
                  <Point X="-28.477517578125" Y="1.690361450195" />
                  <Point X="-28.482794921875" Y="1.699276367188" />
                  <Point X="-28.4942890625" Y="1.716480712891" />
                  <Point X="-28.500505859375" Y="1.724770263672" />
                  <Point X="-28.51441796875" Y="1.741350341797" />
                  <Point X="-28.521501953125" Y="1.748912475586" />
                  <Point X="-28.5364453125" Y="1.763216796875" />
                  <Point X="-28.5443046875" Y="1.769959106445" />
                  <Point X="-29.22761328125" Y="2.294280517578" />
                  <Point X="-29.002287109375" Y="2.680317382812" />
                  <Point X="-28.995576171875" Y="2.688944091797" />
                  <Point X="-28.726337890625" Y="3.035011962891" />
                  <Point X="-28.280201171875" Y="2.777435058594" />
                  <Point X="-28.25415625" Y="2.762398193359" />
                  <Point X="-28.24491796875" Y="2.757716308594" />
                  <Point X="-28.225984375" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736083984" />
                  <Point X="-28.195646484375" Y="2.739227783203" />
                  <Point X="-28.185611328125" Y="2.73665625" />
                  <Point X="-28.16532421875" Y="2.732621337891" />
                  <Point X="-28.151662109375" Y="2.730859619141" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059173828125" Y="2.723334228516" />
                  <Point X="-28.038498046875" Y="2.723784912109" />
                  <Point X="-28.028171875" Y="2.724574707031" />
                  <Point X="-28.00671875" Y="2.727398193359" />
                  <Point X="-27.99654296875" Y="2.729306640625" />
                  <Point X="-27.976455078125" Y="2.734220458984" />
                  <Point X="-27.95701953125" Y="2.741292236328" />
                  <Point X="-27.93847265625" Y="2.750436035156" />
                  <Point X="-27.92944921875" Y="2.755513427734" />
                  <Point X="-27.911197265625" Y="2.767137207031" />
                  <Point X="-27.90278515625" Y="2.773162353516" />
                  <Point X="-27.8866640625" Y="2.78609375" />
                  <Point X="-27.876478515625" Y="2.795474853516" />
                  <Point X="-27.81817578125" Y="2.85377734375" />
                  <Point X="-27.8112578125" Y="2.861494628906" />
                  <Point X="-27.798306640625" Y="2.877633544922" />
                  <Point X="-27.7922734375" Y="2.886055175781" />
                  <Point X="-27.78064453125" Y="2.9043125" />
                  <Point X="-27.775564453125" Y="2.913338378906" />
                  <Point X="-27.76641796875" Y="2.931889404297" />
                  <Point X="-27.759345703125" Y="2.951329345703" />
                  <Point X="-27.754431640625" Y="2.971420410156" />
                  <Point X="-27.7525234375" Y="2.981596679688" />
                  <Point X="-27.74969921875" Y="3.00305859375" />
                  <Point X="-27.74891015625" Y="3.013368408203" />
                  <Point X="-27.74845703125" Y="3.034015380859" />
                  <Point X="-27.74909375" Y="3.047809814453" />
                  <Point X="-27.756279296875" Y="3.129948486328" />
                  <Point X="-27.757744140625" Y="3.140206298828" />
                  <Point X="-27.76178125" Y="3.160498535156" />
                  <Point X="-27.764353515625" Y="3.170532958984" />
                  <Point X="-27.77086328125" Y="3.191176025391" />
                  <Point X="-27.77451171875" Y="3.200870361328" />
                  <Point X="-27.782841796875" Y="3.219799072266" />
                  <Point X="-27.7875234375" Y="3.229033447266" />
                  <Point X="-28.05938671875" Y="3.699915527344" />
                  <Point X="-27.64837109375" Y="4.015037109375" />
                  <Point X="-27.6378125" Y="4.020903076172" />
                  <Point X="-27.1925234375" Y="4.268295410156" />
                  <Point X="-27.126537109375" Y="4.182300292969" />
                  <Point X="-27.1118203125" Y="4.164048339844" />
                  <Point X="-27.09751953125" Y="4.149109375" />
                  <Point X="-27.089962890625" Y="4.14203125" />
                  <Point X="-27.0733828125" Y="4.128117675781" />
                  <Point X="-27.06508984375" Y="4.121896484375" />
                  <Point X="-27.04787890625" Y="4.110397949219" />
                  <Point X="-27.0351640625" Y="4.10314453125" />
                  <Point X="-26.943744140625" Y="4.055553466797" />
                  <Point X="-26.9343125" Y="4.051280761719" />
                  <Point X="-26.9150390625" Y="4.043787109375" />
                  <Point X="-26.905197265625" Y="4.040566162109" />
                  <Point X="-26.884287109375" Y="4.034964111328" />
                  <Point X="-26.87415234375" Y="4.032833251953" />
                  <Point X="-26.853705078125" Y="4.029686767578" />
                  <Point X="-26.833033203125" Y="4.028785644531" />
                  <Point X="-26.812390625" Y="4.030140625" />
                  <Point X="-26.802107421875" Y="4.031381591797" />
                  <Point X="-26.780787109375" Y="4.035143310547" />
                  <Point X="-26.770697265625" Y="4.037497070312" />
                  <Point X="-26.750833984375" Y="4.043289550781" />
                  <Point X="-26.737134765625" Y="4.048354492188" />
                  <Point X="-26.641921875" Y="4.08779296875" />
                  <Point X="-26.63258203125" Y="4.092272949219" />
                  <Point X="-26.61444921875" Y="4.102221191406" />
                  <Point X="-26.60565625" Y="4.107689453125" />
                  <Point X="-26.58792578125" Y="4.120104003906" />
                  <Point X="-26.579775390625" Y="4.126499511719" />
                  <Point X="-26.564220703125" Y="4.140140625" />
                  <Point X="-26.550244140625" Y="4.15539453125" />
                  <Point X="-26.53801171875" Y="4.172079101562" />
                  <Point X="-26.5323515625" Y="4.180755371094" />
                  <Point X="-26.52153125" Y="4.199499511719" />
                  <Point X="-26.5168515625" Y="4.208732421875" />
                  <Point X="-26.5085234375" Y="4.227659179688" />
                  <Point X="-26.503587890625" Y="4.241435058594" />
                  <Point X="-26.472595703125" Y="4.339729492188" />
                  <Point X="-26.470025390625" Y="4.349763671875" />
                  <Point X="-26.465990234375" Y="4.370049804688" />
                  <Point X="-26.464525390625" Y="4.380301757812" />
                  <Point X="-26.462638671875" Y="4.401861328125" />
                  <Point X="-26.46230078125" Y="4.41221484375" />
                  <Point X="-26.462751953125" Y="4.432896484375" />
                  <Point X="-26.463541015625" Y="4.443225097656" />
                  <Point X="-26.479265625" Y="4.562654785156" />
                  <Point X="-25.931171875" Y="4.716320800781" />
                  <Point X="-25.9183828125" Y="4.717817382812" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.23337109375" Y="4.290481933594" />
                  <Point X="-25.22566796875" Y="4.261729980469" />
                  <Point X="-25.2204375" Y="4.247111328125" />
                  <Point X="-25.207666015625" Y="4.218923339844" />
                  <Point X="-25.200125" Y="4.205354003906" />
                  <Point X="-25.182267578125" Y="4.178625976563" />
                  <Point X="-25.172615234375" Y="4.166463867188" />
                  <Point X="-25.151458984375" Y="4.143873535156" />
                  <Point X="-25.126904296875" Y="4.125030761719" />
                  <Point X="-25.099609375" Y="4.110439941406" />
                  <Point X="-25.085365234375" Y="4.104263671875" />
                  <Point X="-25.054927734375" Y="4.093930175781" />
                  <Point X="-25.039865234375" Y="4.090156494141" />
                  <Point X="-25.009326171875" Y="4.085113525391" />
                  <Point X="-24.978375" Y="4.085112792969" />
                  <Point X="-24.947837890625" Y="4.090153808594" />
                  <Point X="-24.932775390625" Y="4.093926269531" />
                  <Point X="-24.9023359375" Y="4.104258300781" />
                  <Point X="-24.88808984375" Y="4.110434082031" />
                  <Point X="-24.860794921875" Y="4.125022949219" />
                  <Point X="-24.8362421875" Y="4.143861816406" />
                  <Point X="-24.815083984375" Y="4.166450195312" />
                  <Point X="-24.8054296875" Y="4.178612792969" />
                  <Point X="-24.7875703125" Y="4.205339355469" />
                  <Point X="-24.78002734375" Y="4.218911621094" />
                  <Point X="-24.767251953125" Y="4.24710546875" />
                  <Point X="-24.76201953125" Y="4.261727050781" />
                  <Point X="-24.621806640625" Y="4.785006835938" />
                  <Point X="-24.172126953125" Y="4.737912597656" />
                  <Point X="-24.161525390625" Y="4.735353027344" />
                  <Point X="-23.54639453125" Y="4.586841796875" />
                  <Point X="-23.54190234375" Y="4.585212402344" />
                  <Point X="-23.141748046875" Y="4.440073730469" />
                  <Point X="-23.1350703125" Y="4.436950195312" />
                  <Point X="-22.74955859375" Y="4.256659667969" />
                  <Point X="-22.743041015625" Y="4.252862304688" />
                  <Point X="-22.370556640625" Y="4.035851806641" />
                  <Point X="-22.3644765625" Y="4.031528320312" />
                  <Point X="-22.182216796875" Y="3.901915527344" />
                  <Point X="-22.900333984375" Y="2.658099609375" />
                  <Point X="-22.9346875" Y="2.598598144531" />
                  <Point X="-22.938169921875" Y="2.59198046875" />
                  <Point X="-22.9482109375" Y="2.569846435547" />
                  <Point X="-22.957140625" Y="2.545098144531" />
                  <Point X="-22.9595546875" Y="2.537396484375" />
                  <Point X="-22.98016796875" Y="2.460311035156" />
                  <Point X="-22.982271484375" Y="2.450323242188" />
                  <Point X="-22.98539453125" Y="2.430178222656" />
                  <Point X="-22.9864140625" Y="2.420020996094" />
                  <Point X="-22.98741796875" Y="2.398351318359" />
                  <Point X="-22.987455078125" Y="2.390450683594" />
                  <Point X="-22.98625390625" Y="2.366814208984" />
                  <Point X="-22.978216796875" Y="2.300157226562" />
                  <Point X="-22.97619921875" Y="2.289036376953" />
                  <Point X="-22.97085546875" Y="2.267108398438" />
                  <Point X="-22.967529296875" Y="2.256301269531" />
                  <Point X="-22.95926171875" Y="2.234211669922" />
                  <Point X="-22.954677734375" Y="2.223880859375" />
                  <Point X="-22.944314453125" Y="2.203836914063" />
                  <Point X="-22.936822265625" Y="2.191599609375" />
                  <Point X="-22.895576171875" Y="2.130815185547" />
                  <Point X="-22.889384765625" Y="2.122673339844" />
                  <Point X="-22.8761640625" Y="2.107101806641" />
                  <Point X="-22.869134765625" Y="2.099672119141" />
                  <Point X="-22.8533125" Y="2.084659667969" />
                  <Point X="-22.847482421875" Y="2.079563720703" />
                  <Point X="-22.82921484375" Y="2.065267578125" />
                  <Point X="-22.7684296875" Y="2.024022827148" />
                  <Point X="-22.75871875" Y="2.018245605469" />
                  <Point X="-22.73867578125" Y="2.007883422852" />
                  <Point X="-22.72834375" Y="2.003298339844" />
                  <Point X="-22.706255859375" Y="1.995032592773" />
                  <Point X="-22.695455078125" Y="1.991708496094" />
                  <Point X="-22.6735390625" Y="1.986366699219" />
                  <Point X="-22.65965625" Y="1.984014892578" />
                  <Point X="-22.593" Y="1.975977294922" />
                  <Point X="-22.582625" Y="1.975298339844" />
                  <Point X="-22.561861328125" Y="1.975078613281" />
                  <Point X="-22.55147265625" Y="1.975537719727" />
                  <Point X="-22.529369140625" Y="1.977731445312" />
                  <Point X="-22.521734375" Y="1.978803710938" />
                  <Point X="-22.499052734375" Y="1.983251831055" />
                  <Point X="-22.421966796875" Y="2.003865356445" />
                  <Point X="-22.4160078125" Y="2.005669433594" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572998047" />
                  <Point X="-22.36396484375" Y="2.027872314453" />
                  <Point X="-21.059591796875" Y="2.780952148438" />
                  <Point X="-20.95604296875" Y="2.637041992188" />
                  <Point X="-20.95265234375" Y="2.631436767578" />
                  <Point X="-20.86311328125" Y="2.483471923828" />
                  <Point X="-21.78181640625" Y="1.778525634766" />
                  <Point X="-21.827046875" Y="1.7438203125" />
                  <Point X="-21.83290625" Y="1.738939575195" />
                  <Point X="-21.850857421875" Y="1.722093261719" />
                  <Point X="-21.8686640625" Y="1.702480102539" />
                  <Point X="-21.873724609375" Y="1.696416992188" />
                  <Point X="-21.929203125" Y="1.624041259766" />
                  <Point X="-21.935041015625" Y="1.615499755859" />
                  <Point X="-21.945755859375" Y="1.597827392578" />
                  <Point X="-21.9506328125" Y="1.588696777344" />
                  <Point X="-21.9599609375" Y="1.568676635742" />
                  <Point X="-21.9629453125" Y="1.561522827148" />
                  <Point X="-21.97071875" Y="1.539604248047" />
                  <Point X="-21.991384765625" Y="1.465708374023" />
                  <Point X="-21.993775390625" Y="1.454655151367" />
                  <Point X="-21.997228515625" Y="1.432348510742" />
                  <Point X="-21.998291015625" Y="1.421095092773" />
                  <Point X="-21.99910546875" Y="1.397515991211" />
                  <Point X="-21.998822265625" Y="1.386211914062" />
                  <Point X="-21.996916015625" Y="1.363725219727" />
                  <Point X="-21.99458984375" Y="1.349137817383" />
                  <Point X="-21.97762890625" Y="1.266937011719" />
                  <Point X="-21.97498046875" Y="1.256905395508" />
                  <Point X="-21.9686015625" Y="1.237178710938" />
                  <Point X="-21.96487109375" Y="1.227483642578" />
                  <Point X="-21.9557578125" Y="1.207194335938" />
                  <Point X="-21.952375" Y="1.200405029297" />
                  <Point X="-21.94116796875" Y="1.180615356445" />
                  <Point X="-21.89502734375" Y="1.110482299805" />
                  <Point X="-21.888259765625" Y="1.101420532227" />
                  <Point X="-21.87369921875" Y="1.084169799805" />
                  <Point X="-21.86590625" Y="1.075980834961" />
                  <Point X="-21.84865625" Y="1.059887939453" />
                  <Point X="-21.839951171875" Y="1.052685180664" />
                  <Point X="-21.821748046875" Y="1.039365600586" />
                  <Point X="-21.8094765625" Y="1.03168737793" />
                  <Point X="-21.742611328125" Y="0.994047790527" />
                  <Point X="-21.733142578125" Y="0.989390686035" />
                  <Point X="-21.7137421875" Y="0.981152526855" />
                  <Point X="-21.7038125" Y="0.977573120117" />
                  <Point X="-21.68208203125" Y="0.971078857422" />
                  <Point X="-21.67476953125" Y="0.969206359863" />
                  <Point X="-21.65257421875" Y="0.964761352539" />
                  <Point X="-21.562166015625" Y="0.952812805176" />
                  <Point X="-21.555966796875" Y="0.952199584961" />
                  <Point X="-21.53428125" Y="0.951222839355" />
                  <Point X="-21.50878125" Y="0.952032409668" />
                  <Point X="-21.49939453125" Y="0.952797180176" />
                  <Point X="-20.295298828125" Y="1.111319702148" />
                  <Point X="-20.2473125" Y="0.914206481934" />
                  <Point X="-20.24624609375" Y="0.90736126709" />
                  <Point X="-20.21612890625" Y="0.713920959473" />
                  <Point X="-21.256173828125" Y="0.435241577148" />
                  <Point X="-21.3080078125" Y="0.421352783203" />
                  <Point X="-21.3154140625" Y="0.419040496826" />
                  <Point X="-21.338703125" Y="0.410215881348" />
                  <Point X="-21.362826171875" Y="0.398783355713" />
                  <Point X="-21.369681640625" Y="0.395185028076" />
                  <Point X="-21.45850390625" Y="0.34384475708" />
                  <Point X="-21.46728125" Y="0.338102539062" />
                  <Point X="-21.48414453125" Y="0.325689117432" />
                  <Point X="-21.49223046875" Y="0.319017883301" />
                  <Point X="-21.508755859375" Y="0.303777648926" />
                  <Point X="-21.514142578125" Y="0.29839630127" />
                  <Point X="-21.529416015625" Y="0.281406921387" />
                  <Point X="-21.582708984375" Y="0.213499038696" />
                  <Point X="-21.58914453125" Y="0.204210403442" />
                  <Point X="-21.6008671875" Y="0.18493371582" />
                  <Point X="-21.606154296875" Y="0.17494581604" />
                  <Point X="-21.61592578125" Y="0.153489425659" />
                  <Point X="-21.61998828125" Y="0.142948501587" />
                  <Point X="-21.626830078125" Y="0.121468772888" />
                  <Point X="-21.630361328125" Y="0.106619407654" />
                  <Point X="-21.648125" Y="0.013860736847" />
                  <Point X="-21.649529296875" Y="0.003422801495" />
                  <Point X="-21.651173828125" Y="-0.017538824081" />
                  <Point X="-21.6514140625" Y="-0.028062662125" />
                  <Point X="-21.65067578125" Y="-0.050723049164" />
                  <Point X="-21.650142578125" Y="-0.058138885498" />
                  <Point X="-21.64738671875" Y="-0.080272399902" />
                  <Point X="-21.629623046875" Y="-0.173031219482" />
                  <Point X="-21.62683984375" Y="-0.183986068726" />
                  <Point X="-21.619994140625" Y="-0.205485855103" />
                  <Point X="-21.615931640625" Y="-0.216030807495" />
                  <Point X="-21.606158203125" Y="-0.237494918823" />
                  <Point X="-21.60087109375" Y="-0.247482681274" />
                  <Point X="-21.5891484375" Y="-0.266762329102" />
                  <Point X="-21.5805" Y="-0.278873901367" />
                  <Point X="-21.52720703125" Y="-0.346781494141" />
                  <Point X="-21.520130859375" Y="-0.354820800781" />
                  <Point X="-21.50512109375" Y="-0.370049743652" />
                  <Point X="-21.4971875" Y="-0.377239837646" />
                  <Point X="-21.4791875" Y="-0.391792236328" />
                  <Point X="-21.4732578125" Y="-0.396215423584" />
                  <Point X="-21.45481640625" Y="-0.408536682129" />
                  <Point X="-21.365994140625" Y="-0.459876983643" />
                  <Point X="-21.36050390625" Y="-0.462814239502" />
                  <Point X="-21.34084375" Y="-0.472016052246" />
                  <Point X="-21.31697265625" Y="-0.481027648926" />
                  <Point X="-21.3080078125" Y="-0.483913024902" />
                  <Point X="-20.21512109375" Y="-0.776751220703" />
                  <Point X="-20.238380859375" Y="-0.931032775879" />
                  <Point X="-20.23975390625" Y="-0.937050415039" />
                  <Point X="-20.272197265625" Y="-1.079219482422" />
                  <Point X="-21.503103515625" Y="-0.917167541504" />
                  <Point X="-21.56321484375" Y="-0.909253662109" />
                  <Point X="-21.573837890625" Y="-0.908457458496" />
                  <Point X="-21.595107421875" Y="-0.908059753418" />
                  <Point X="-21.60575390625" Y="-0.908458068848" />
                  <Point X="-21.630138671875" Y="-0.910744628906" />
                  <Point X="-21.652755859375" Y="-0.914249755859" />
                  <Point X="-21.82708203125" Y="-0.952139831543" />
                  <Point X="-21.838529296875" Y="-0.955390930176" />
                  <Point X="-21.8688984375" Y="-0.966112304688" />
                  <Point X="-21.88852734375" Y="-0.975706359863" />
                  <Point X="-21.918748046875" Y="-0.995071166992" />
                  <Point X="-21.932935546875" Y="-1.006194091797" />
                  <Point X="-21.949810546875" Y="-1.022230529785" />
                  <Point X="-21.965021484375" Y="-1.038484863281" />
                  <Point X="-22.070390625" Y="-1.165210571289" />
                  <Point X="-22.0768125" Y="-1.173893310547" />
                  <Point X="-22.09283984375" Y="-1.198361694336" />
                  <Point X="-22.101970703125" Y="-1.216138671875" />
                  <Point X="-22.113841796875" Y="-1.246823242188" />
                  <Point X="-22.118439453125" Y="-1.262682861328" />
                  <Point X="-22.122662109375" Y="-1.28405078125" />
                  <Point X="-22.125466796875" Y="-1.303475830078" />
                  <Point X="-22.140568359375" Y="-1.467590942383" />
                  <Point X="-22.140916015625" Y="-1.479481689453" />
                  <Point X="-22.1398359375" Y="-1.511671264648" />
                  <Point X="-22.136560546875" Y="-1.533409301758" />
                  <Point X="-22.127078125" Y="-1.568286499023" />
                  <Point X="-22.1206640625" Y="-1.585267089844" />
                  <Point X="-22.1101484375" Y="-1.606662109375" />
                  <Point X="-22.099453125" Y="-1.625603149414" />
                  <Point X="-22.002978515625" Y="-1.775662475586" />
                  <Point X="-21.99825390625" Y="-1.782358398438" />
                  <Point X="-21.980201171875" Y="-1.804454833984" />
                  <Point X="-21.9565078125" Y="-1.828121826172" />
                  <Point X="-21.947201171875" Y="-1.836277832031" />
                  <Point X="-20.912828125" Y="-2.629979980469" />
                  <Point X="-20.954515625" Y="-2.697436523438" />
                  <Point X="-20.957357421875" Y="-2.701478515625" />
                  <Point X="-20.998724609375" Y="-2.760252441406" />
                  <Point X="-22.098005859375" Y="-2.125580810547" />
                  <Point X="-22.151544921875" Y="-2.094670410156" />
                  <Point X="-22.161328125" Y="-2.089751220703" />
                  <Point X="-22.18139453125" Y="-2.081070800781" />
                  <Point X="-22.191677734375" Y="-2.077309570312" />
                  <Point X="-22.21640625" Y="-2.069851074219" />
                  <Point X="-22.2375078125" Y="-2.064781738281" />
                  <Point X="-22.444982421875" Y="-2.027311889648" />
                  <Point X="-22.456798828125" Y="-2.025934692383" />
                  <Point X="-22.488958984375" Y="-2.024217407227" />
                  <Point X="-22.511044921875" Y="-2.025619140625" />
                  <Point X="-22.546900390625" Y="-2.03214831543" />
                  <Point X="-22.56451171875" Y="-2.037148193359" />
                  <Point X="-22.587341796875" Y="-2.046085571289" />
                  <Point X="-22.606568359375" Y="-2.054875976563" />
                  <Point X="-22.778927734375" Y="-2.145588134766" />
                  <Point X="-22.788185546875" Y="-2.151153564453" />
                  <Point X="-22.812357421875" Y="-2.167627197266" />
                  <Point X="-22.82777734375" Y="-2.180746826172" />
                  <Point X="-22.85077734375" Y="-2.204991455078" />
                  <Point X="-22.861203125" Y="-2.218133789062" />
                  <Point X="-22.873861328125" Y="-2.237360107422" />
                  <Point X="-22.883302734375" Y="-2.253351806641" />
                  <Point X="-22.974015625" Y="-2.425712402344" />
                  <Point X="-22.978888671875" Y="-2.436573242188" />
                  <Point X="-22.99020703125" Y="-2.466727539062" />
                  <Point X="-22.995533203125" Y="-2.488343261719" />
                  <Point X="-23.00008203125" Y="-2.524781494141" />
                  <Point X="-23.000580078125" Y="-2.543225830078" />
                  <Point X="-22.998810546875" Y="-2.568342773438" />
                  <Point X="-22.996255859375" Y="-2.588758789062" />
                  <Point X="-22.95878515625" Y="-2.796233642578" />
                  <Point X="-22.95698046875" Y="-2.804229736328" />
                  <Point X="-22.94876171875" Y="-2.831540771484" />
                  <Point X="-22.935927734375" Y="-2.862479248047" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.264103515625" Y="-4.027723388672" />
                  <Point X="-22.276244140625" Y="-4.036082275391" />
                  <Point X="-23.125228515625" Y="-2.929666015625" />
                  <Point X="-23.16608203125" Y="-2.876423339844" />
                  <Point X="-23.17342578125" Y="-2.867942382812" />
                  <Point X="-23.189060546875" Y="-2.851903808594" />
                  <Point X="-23.1973515625" Y="-2.844346191406" />
                  <Point X="-23.218478515625" Y="-2.827262451172" />
                  <Point X="-23.235197265625" Y="-2.815183837891" />
                  <Point X="-23.43982421875" Y="-2.683628173828" />
                  <Point X="-23.450216796875" Y="-2.677832519531" />
                  <Point X="-23.4792734375" Y="-2.663938476562" />
                  <Point X="-23.50046484375" Y="-2.656728759766" />
                  <Point X="-23.5366015625" Y="-2.64903125" />
                  <Point X="-23.555046875" Y="-2.646956298828" />
                  <Point X="-23.580796875" Y="-2.646591308594" />
                  <Point X="-23.6008984375" Y="-2.647371582031" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.835373046875" Y="-2.669564941406" />
                  <Point X="-23.864005859375" Y="-2.675533935547" />
                  <Point X="-23.88348828125" Y="-2.681850585938" />
                  <Point X="-23.9144140625" Y="-2.695718017578" />
                  <Point X="-23.929255859375" Y="-2.704042724609" />
                  <Point X="-23.949263671875" Y="-2.717756835938" />
                  <Point X="-23.963314453125" Y="-2.728379394531" />
                  <Point X="-24.136123046875" Y="-2.872063232422" />
                  <Point X="-24.144779296875" Y="-2.880229980469" />
                  <Point X="-24.166779296875" Y="-2.903759765625" />
                  <Point X="-24.18010546875" Y="-2.921923095703" />
                  <Point X="-24.198416015625" Y="-2.95434375" />
                  <Point X="-24.20596875" Y="-2.971461669922" />
                  <Point X="-24.21423046875" Y="-2.996657226562" />
                  <Point X="-24.2193515625" Y="-3.015501464844" />
                  <Point X="-24.271021484375" Y="-3.253218017578" />
                  <Point X="-24.2724140625" Y="-3.261286865234" />
                  <Point X="-24.275275390625" Y="-3.289677246094" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520019531" />
                  <Point X="-24.166912109375" Y="-4.152318847656" />
                  <Point X="-24.3289609375" Y="-3.547542236328" />
                  <Point X="-24.344931640625" Y="-3.487937011719" />
                  <Point X="-24.3485625" Y="-3.477061523438" />
                  <Point X="-24.357103515625" Y="-3.455835449219" />
                  <Point X="-24.362013671875" Y="-3.445484619141" />
                  <Point X="-24.37576171875" Y="-3.420510742188" />
                  <Point X="-24.3861171875" Y="-3.403803710938" />
                  <Point X="-24.543318359375" Y="-3.177308837891" />
                  <Point X="-24.550703125" Y="-3.167976074219" />
                  <Point X="-24.57223046875" Y="-3.144021728516" />
                  <Point X="-24.589236328125" Y="-3.129124023438" />
                  <Point X="-24.6201015625" Y="-3.108000488281" />
                  <Point X="-24.6365859375" Y="-3.098971679688" />
                  <Point X="-24.66142578125" Y="-3.088411132812" />
                  <Point X="-24.6794453125" Y="-3.081803222656" />
                  <Point X="-24.922703125" Y="-3.006305175781" />
                  <Point X="-24.933185546875" Y="-3.0036953125" />
                  <Point X="-24.961923828125" Y="-2.998252441406" />
                  <Point X="-24.982556640625" Y="-2.996638916016" />
                  <Point X="-25.016853515625" Y="-2.997706054688" />
                  <Point X="-25.033953125" Y="-2.99980078125" />
                  <Point X="-25.058875" Y="-3.005182861328" />
                  <Point X="-25.0750859375" Y="-3.009440185547" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.32946484375" Y="-3.089170654297" />
                  <Point X="-25.358787109375" Y="-3.102486572266" />
                  <Point X="-25.378232421875" Y="-3.114311523438" />
                  <Point X="-25.407865234375" Y="-3.137616210938" />
                  <Point X="-25.421515625" Y="-3.150754150391" />
                  <Point X="-25.439560546875" Y="-3.171978027344" />
                  <Point X="-25.450896484375" Y="-3.186715332031" />
                  <Point X="-25.608095703125" Y="-3.413210449219" />
                  <Point X="-25.612470703125" Y="-3.420134277344" />
                  <Point X="-25.625970703125" Y="-3.445255371094" />
                  <Point X="-25.638775390625" Y="-3.476209472656" />
                  <Point X="-25.64275390625" Y="-3.487935791016" />
                  <Point X="-25.98542578125" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.332967519763" Y="-3.962159043309" />
                  <Point X="-22.313057658603" Y="-3.942932313879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.054819581991" Y="-2.727865927261" />
                  <Point X="-20.935467049986" Y="-2.612608526858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.391174067212" Y="-3.886302911655" />
                  <Point X="-22.36201180158" Y="-3.858141239086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.140407461336" Y="-2.678451640457" />
                  <Point X="-21.01167267902" Y="-2.554133906247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.449380614661" Y="-3.81044678" />
                  <Point X="-22.410965944558" Y="-3.773350164294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.225995340681" Y="-2.629037353653" />
                  <Point X="-21.087878308053" Y="-2.495659285635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.50758716211" Y="-3.734590648345" />
                  <Point X="-22.459920087536" Y="-3.688559089501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.311583220025" Y="-2.579623066849" />
                  <Point X="-21.164083937087" Y="-2.437184665023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.56579370956" Y="-3.658734516691" />
                  <Point X="-22.508874230514" Y="-3.603768014708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.39717109937" Y="-2.530208780045" />
                  <Point X="-21.24028956612" Y="-2.378710044411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.624000257009" Y="-3.582878385036" />
                  <Point X="-22.557828373491" Y="-3.518976939916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.482758978715" Y="-2.480794493242" />
                  <Point X="-21.316495195154" Y="-2.3202354238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.682206804458" Y="-3.507022253382" />
                  <Point X="-22.606782516469" Y="-3.434185865123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.56834685806" Y="-2.431380206438" />
                  <Point X="-21.392700824188" Y="-2.261760803188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.740413351907" Y="-3.431166121727" />
                  <Point X="-22.655736659447" Y="-3.34939479033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.653934737404" Y="-2.381965919634" />
                  <Point X="-21.468906453221" Y="-2.203286182576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.300977671899" Y="-1.075430468679" />
                  <Point X="-20.262953001535" Y="-1.038710471343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.798619899357" Y="-3.355309990073" />
                  <Point X="-22.704690802425" Y="-3.264603715538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.739522616749" Y="-2.33255163283" />
                  <Point X="-21.545112082255" Y="-2.144811561965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.421328139633" Y="-1.059586023266" />
                  <Point X="-20.22989057937" Y="-0.874716920244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.856826446806" Y="-3.279453858418" />
                  <Point X="-22.753644945402" Y="-3.179812640745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.825110496094" Y="-2.283137346026" />
                  <Point X="-21.621317711288" Y="-2.086336941353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.541678607367" Y="-1.043741577853" />
                  <Point X="-20.254324301189" Y="-0.766246749984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.915032994255" Y="-3.203597726764" />
                  <Point X="-22.80259908838" Y="-3.095021565952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.910698375439" Y="-2.233723059223" />
                  <Point X="-21.697523340322" Y="-2.027862320741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.662029075101" Y="-1.02789713244" />
                  <Point X="-20.361378020848" Y="-0.737561784214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.973239541704" Y="-3.127741595109" />
                  <Point X="-22.851553231358" Y="-3.01023049116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.996286254784" Y="-2.184308772419" />
                  <Point X="-21.773728969356" Y="-1.969387700129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.782379542835" Y="-1.012052687027" />
                  <Point X="-20.468431740506" Y="-0.708876818443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.167747954601" Y="-4.149199419709" />
                  <Point X="-24.16737074483" Y="-4.148835152468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.031446089154" Y="-3.051885463454" />
                  <Point X="-22.900507374335" Y="-2.925439416367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.081874134128" Y="-2.134894485615" />
                  <Point X="-21.849934598389" Y="-1.910913079518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.902730010569" Y="-0.996208241614" />
                  <Point X="-20.575485460165" Y="-0.680191852672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.195860446916" Y="-4.044281796823" />
                  <Point X="-24.182796310845" Y="-4.031665907267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.089652636603" Y="-2.9760293318" />
                  <Point X="-22.94626441683" Y="-2.837560937526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.168618865665" Y="-2.086597357987" />
                  <Point X="-21.926140227423" Y="-1.852438458906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.023080478303" Y="-0.980363796201" />
                  <Point X="-20.682539179824" Y="-0.651506886901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.223972939231" Y="-3.939364173937" />
                  <Point X="-24.19822187686" Y="-3.914496662066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.147858837281" Y="-2.900172865272" />
                  <Point X="-22.970879729757" Y="-2.729266127761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.275652346265" Y="-2.057892847584" />
                  <Point X="-21.994758472963" Y="-1.786636787225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.143430946037" Y="-0.964519350788" />
                  <Point X="-20.789592899483" Y="-0.622821921131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.252085431546" Y="-3.834446551051" />
                  <Point X="-24.213647442875" Y="-3.797327416865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.213382734976" Y="-2.831383016612" />
                  <Point X="-22.991189140005" Y="-2.616813156114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.390863783906" Y="-2.037085698497" />
                  <Point X="-22.047860696248" Y="-1.705851467021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.263781413771" Y="-0.948674905375" />
                  <Point X="-20.896646619142" Y="-0.59413695536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.280197923861" Y="-3.729528928165" />
                  <Point X="-24.22907300889" Y="-3.680158171664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.294130928293" Y="-2.777295099337" />
                  <Point X="-22.995618282643" Y="-2.489024788295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.516840551417" Y="-2.026674507622" />
                  <Point X="-22.100182114966" Y="-1.624312132613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.384131881505" Y="-0.932830459962" />
                  <Point X="-21.0037003388" Y="-0.565451989589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.308310416176" Y="-3.624611305279" />
                  <Point X="-24.244498574905" Y="-3.562988926463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.376230930259" Y="-2.724512608501" />
                  <Point X="-22.876635438556" Y="-2.242058850219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.774113168757" Y="-2.143054245106" />
                  <Point X="-22.137354035086" Y="-1.528143097464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.504482347775" Y="-0.916986013135" />
                  <Point X="-21.110754058459" Y="-0.536767023818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.33642275562" Y="-3.519693534767" />
                  <Point X="-24.25992414092" Y="-3.445819681263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.459870179782" Y="-2.673216551752" />
                  <Point X="-22.133659536416" Y="-1.392509820423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.635663756447" Y="-0.911600885806" />
                  <Point X="-21.217807778118" Y="-0.508082058048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.373926338927" Y="-3.423844783035" />
                  <Point X="-24.274924283088" Y="-3.328239609028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.56922663264" Y="-2.646755309583" />
                  <Point X="-22.110434567368" Y="-1.238016187372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.810717759757" Y="-0.948583030641" />
                  <Point X="-21.323858246633" Y="-0.478428263909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.427776245692" Y="-3.343781492376" />
                  <Point X="-24.254291619794" Y="-3.176249336544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.717758370847" Y="-2.658125200725" />
                  <Point X="-21.413169163285" Y="-0.432609272441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.229655600087" Y="0.710296490371" />
                  <Point X="-20.217406292381" Y="0.722125509322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.029300901461" Y="-4.758290333882" />
                  <Point X="-25.96703125813" Y="-4.698157238306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.482655264202" Y="-3.264712003376" />
                  <Point X="-24.217487592276" Y="-3.008642559156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.8768587956" Y="-2.679701153829" />
                  <Point X="-21.494685206023" Y="-0.379262858734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.418931912089" Y="0.65958002168" />
                  <Point X="-20.235280500139" Y="0.836930128678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.138792336821" Y="-4.7319594428" />
                  <Point X="-25.919291569434" Y="-4.519990015672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.537534282711" Y="-3.185642514377" />
                  <Point X="-21.557644814321" Y="-0.307996704586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.608208224091" Y="0.60886355299" />
                  <Point X="-20.255821617196" Y="0.94915934366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.121976847816" Y="-4.583655372679" />
                  <Point X="-25.871551880737" Y="-4.341822793039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.604762330873" Y="-3.118498344692" />
                  <Point X="-21.610812088842" Y="-0.227274203632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.797484536094" Y="0.558147084299" />
                  <Point X="-20.281852671834" Y="1.056086987547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.130808415923" Y="-4.460118377716" />
                  <Point X="-25.823812192041" Y="-4.163655570405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.697665510022" Y="-3.076148360794" />
                  <Point X="-21.639264562008" Y="-0.122684896437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.986760848096" Y="0.507430615608" />
                  <Point X="-20.37185190638" Y="1.101241278151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.152845283097" Y="-4.349333591832" />
                  <Point X="-25.776072503344" Y="-3.985488347772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.801161010212" Y="-3.044027262424" />
                  <Point X="-21.649864993286" Y="-0.000856072783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.176037160098" Y="0.456714146917" />
                  <Point X="-20.530197036227" Y="1.080394704859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.174882150272" Y="-4.238548805948" />
                  <Point X="-25.728332814648" Y="-3.807321125138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.904656510402" Y="-3.011906164053" />
                  <Point X="-21.607394345616" Y="0.172222896076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.386691549225" Y="0.38535310912" />
                  <Point X="-20.688542166074" Y="1.059548131567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.204673988418" Y="-4.13525290848" />
                  <Point X="-25.680593125951" Y="-3.629153902505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.028141686994" Y="-2.999088871797" />
                  <Point X="-20.84688729592" Y="1.038701558276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.262556961314" Y="-4.059084304511" />
                  <Point X="-25.624948206661" Y="-3.443352687424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.223231124834" Y="-3.055419010855" />
                  <Point X="-21.005232425767" Y="1.017854984984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.33386605844" Y="-3.995881158001" />
                  <Point X="-21.163577555614" Y="0.997008411692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.405536955489" Y="-3.933027397615" />
                  <Point X="-21.32192268546" Y="0.976161838401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.477207852538" Y="-3.87017363723" />
                  <Point X="-21.480267815307" Y="0.955315265109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.558496970561" Y="-3.816608084871" />
                  <Point X="-21.612700951093" Y="0.959491613614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.671721041416" Y="-3.793881757987" />
                  <Point X="-21.722970401102" Y="0.985071184684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.799786850926" Y="-3.785487931521" />
                  <Point X="-21.810677603586" Y="1.032438864921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.927852660436" Y="-3.777094105055" />
                  <Point X="-21.882755632126" Y="1.094899462996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.125516367252" Y="-3.835910186767" />
                  <Point X="-21.937121383755" Y="1.174464608061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.562454619495" Y="-4.125791011096" />
                  <Point X="-21.977710713562" Y="1.267333489036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.645783480766" Y="-4.074195215896" />
                  <Point X="-21.998258084477" Y="1.379556664738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.729112342036" Y="-4.022599420696" />
                  <Point X="-21.971250397388" Y="1.53770322614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.489984409247" Y="2.002456388585" />
                  <Point X="-20.91060644106" Y="2.561955188833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.806807258244" Y="-3.965562987991" />
                  <Point X="-20.96160499939" Y="2.644771994669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.882897104413" Y="-3.906976557166" />
                  <Point X="-21.017672609743" Y="2.722693673868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.958986950582" Y="-3.848390126341" />
                  <Point X="-21.145409088166" Y="2.731405531668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.858081087214" Y="-3.618880925628" />
                  <Point X="-21.485487298612" Y="2.535061362431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.685753611857" Y="-3.320400675938" />
                  <Point X="-21.825565509057" Y="2.338717193193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.513426136499" Y="-3.021920426248" />
                  <Point X="-22.165643719503" Y="2.142373023956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.341227394604" Y="-2.723564493017" />
                  <Point X="-22.454969202471" Y="1.995040193935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.313769486842" Y="-2.564983158565" />
                  <Point X="-22.609417301828" Y="1.977956939242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.352464239917" Y="-2.470284706107" />
                  <Point X="-22.722282299348" Y="2.001030019214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.417365676899" Y="-2.400893754123" />
                  <Point X="-22.80766326827" Y="2.050644117091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.507561982026" Y="-2.355929772366" />
                  <Point X="-22.880576187518" Y="2.112298470582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.653984645338" Y="-2.365262953558" />
                  <Point X="-22.936186121415" Y="2.190662122796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.980631729866" Y="-2.548636835264" />
                  <Point X="-22.975222942196" Y="2.285030244311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.320709743909" Y="-2.744980814836" />
                  <Point X="-22.987080045473" Y="2.405645513921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.660787757951" Y="-2.941324794409" />
                  <Point X="-22.94385136897" Y="2.579456502716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.828485573183" Y="-2.971203150992" />
                  <Point X="-22.775788926236" Y="2.873818058277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.886422315568" Y="-2.895086471616" />
                  <Point X="-22.60346119562" Y="3.172298554468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.944359057953" Y="-2.818969792239" />
                  <Point X="-22.431133465003" Y="3.470779050659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.002295800338" Y="-2.742853112862" />
                  <Point X="-22.258805734387" Y="3.76925954685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.052508528749" Y="-2.659277439894" />
                  <Point X="-22.225972306659" Y="3.933031960592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.102485472365" Y="-2.575474072198" />
                  <Point X="-28.131777222618" Y="-1.638072011804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.985109603889" Y="-1.49643673877" />
                  <Point X="-22.30473126589" Y="3.989040858893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.152462415982" Y="-2.491670704501" />
                  <Point X="-28.797556875971" Y="-2.148942408395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.950143695157" Y="-1.33060501206" />
                  <Point X="-22.384564186803" Y="4.044012644454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.983005180948" Y="-1.230273438865" />
                  <Point X="-22.469861768966" Y="4.093707267987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.047144212308" Y="-1.16014624033" />
                  <Point X="-22.55515935113" Y="4.14340189152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.135196390559" Y="-1.113111699317" />
                  <Point X="-22.640456933294" Y="4.193096515053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.260829764439" Y="-1.102368897068" />
                  <Point X="-22.725754515457" Y="4.242791138586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.419174889307" Y="-1.123215465551" />
                  <Point X="-22.815982982204" Y="4.287724062228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.577520014174" Y="-1.144062034035" />
                  <Point X="-22.908120386243" Y="4.330813546553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.735865139042" Y="-1.164908602518" />
                  <Point X="-23.000257790283" Y="4.373903030879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.894210263909" Y="-1.185755171002" />
                  <Point X="-23.092395194323" Y="4.416992515205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.052555388777" Y="-1.206601739485" />
                  <Point X="-23.187912632779" Y="4.456817938236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.210900513645" Y="-1.227448307969" />
                  <Point X="-23.287329965081" Y="4.492877277557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.369245638512" Y="-1.248294876452" />
                  <Point X="-23.386747297384" Y="4.528936616878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.52759076338" Y="-1.269141444936" />
                  <Point X="-28.54749912584" Y="-0.322677952282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.300331128447" Y="-0.083990591707" />
                  <Point X="-23.486164629686" Y="4.564995956198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.665193209226" Y="-1.269957041129" />
                  <Point X="-28.736775453366" Y="-0.373394435963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.30525754815" Y="0.043317561232" />
                  <Point X="-23.589518993061" Y="4.597253348806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.692252353254" Y="-1.164022211626" />
                  <Point X="-28.926051878053" Y="-0.424111013472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.347993132666" Y="0.134113828127" />
                  <Point X="-23.698924554989" Y="4.623667166898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.719311497283" Y="-1.058087382124" />
                  <Point X="-29.115328302739" Y="-0.474827590981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.414637715862" Y="0.201821443379" />
                  <Point X="-23.808330116917" Y="4.650080984989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.744201276972" Y="-0.950057621831" />
                  <Point X="-29.304604727426" Y="-0.52554416849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.503566012033" Y="0.24800992715" />
                  <Point X="-23.917735678844" Y="4.676494803081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.760796961916" Y="-0.834018347345" />
                  <Point X="-29.493881152113" Y="-0.576260745999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.610347180825" Y="0.276958092234" />
                  <Point X="-24.027141240772" Y="4.702908621172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.77739264686" Y="-0.717979072859" />
                  <Point X="-29.6831575768" Y="-0.626977323508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.717400878527" Y="0.305643079209" />
                  <Point X="-24.1365468027" Y="4.729322439264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.824454576228" Y="0.334328066183" />
                  <Point X="-24.930464371179" Y="4.094710696408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.764398811632" Y="4.255078343145" />
                  <Point X="-24.255380503443" Y="4.746631609534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.93150827393" Y="0.363013053158" />
                  <Point X="-25.064622270906" Y="4.097221459736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.715852368618" Y="4.434024639366" />
                  <Point X="-24.37875813894" Y="4.759552753019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.038561971631" Y="0.391698040133" />
                  <Point X="-25.152224288607" Y="4.144690715739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.66811235184" Y="4.612192178825" />
                  <Point X="-24.502135774436" Y="4.772473896505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.145615669333" Y="0.420383027107" />
                  <Point X="-25.209019228564" Y="4.221910020903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.252669367034" Y="0.449068014082" />
                  <Point X="-25.241881024002" Y="4.322241295076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.359723064736" Y="0.477753001057" />
                  <Point X="-25.269993687597" Y="4.427158752558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.466776762437" Y="0.506437988031" />
                  <Point X="-28.743946930274" Y="1.204466643046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.421440372075" Y="1.515907606101" />
                  <Point X="-25.298106351192" Y="4.532076210041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.573830460139" Y="0.535122975006" />
                  <Point X="-28.867352718915" Y="1.217360599357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.444425259556" Y="1.625776899418" />
                  <Point X="-25.326219014788" Y="4.636993667523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.68088415784" Y="0.563807961981" />
                  <Point X="-28.987703219204" Y="1.233205013332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.491531199269" Y="1.712352763357" />
                  <Point X="-25.354331678383" Y="4.741911125006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.784914440416" Y="0.595412627003" />
                  <Point X="-29.108053719493" Y="1.249049427307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.557862802605" Y="1.780362619747" />
                  <Point X="-25.460553355583" Y="4.77139958484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.762113702932" Y="0.749496584396" />
                  <Point X="-29.228404219782" Y="1.264893841282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.634068404471" Y="1.838837266593" />
                  <Point X="-25.616171099564" Y="4.753186817463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.739312965447" Y="0.903580541789" />
                  <Point X="-29.348754720071" Y="1.280738255257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.710274006338" Y="1.89731191344" />
                  <Point X="-25.771788843545" Y="4.734974050086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.699832920752" Y="1.073771518927" />
                  <Point X="-29.46910522036" Y="1.296582669232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.786479608204" Y="1.955786560287" />
                  <Point X="-27.981721810089" Y="2.732932132365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.761393354749" Y="2.945700848457" />
                  <Point X="-25.927406429914" Y="4.716761434913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.651362024064" Y="1.25264486091" />
                  <Point X="-29.589455720649" Y="1.312427083207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.862685210071" Y="2.014261207134" />
                  <Point X="-28.123204076206" Y="2.728369837288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.752468348185" Y="3.086385168258" />
                  <Point X="-26.766207253485" Y="4.038806436439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.480477151823" Y="4.314732788238" />
                  <Point X="-26.119214130285" Y="4.66360043289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.938890811937" Y="2.07273585398" />
                  <Point X="-28.234376149004" Y="2.753077755661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.773488565789" Y="3.19815172122" />
                  <Point X="-26.902023399458" Y="4.039715849982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.465868874922" Y="4.460905378407" />
                  <Point X="-26.3119193078" Y="4.609572747263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.015096413804" Y="2.131210500827" />
                  <Point X="-28.321324921367" Y="2.801177843354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.82000632445" Y="3.285295584998" />
                  <Point X="-26.994837759888" Y="4.08215160512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.09130201567" Y="2.189685147674" />
                  <Point X="-28.406912897699" Y="2.850592036498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.868960400809" Y="3.370086724125" />
                  <Point X="-27.079061578622" Y="4.132883149944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.167507617537" Y="2.24815979452" />
                  <Point X="-28.492500874032" Y="2.900006229642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.917914477167" Y="3.454877863251" />
                  <Point X="-27.142757113229" Y="4.203438628315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.190289270521" Y="2.358225349109" />
                  <Point X="-28.578088850365" Y="2.949420422785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.966868553525" Y="3.539669002378" />
                  <Point X="-27.239217965399" Y="4.242353007312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.013624076239" Y="2.660894485272" />
                  <Point X="-28.663676826697" Y="2.998834615929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.015822629883" Y="3.624460141504" />
                  <Point X="-27.561241202423" Y="4.063444323238" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626464844" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978485351562" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.51248828125" Y="-3.596717773438" />
                  <Point X="-24.528458984375" Y="-3.537112548828" />
                  <Point X="-24.54220703125" Y="-3.512138671875" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.71092578125" Y="-3.273825195312" />
                  <Point X="-24.735765625" Y="-3.263264648438" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-24.99384375" Y="-3.18551953125" />
                  <Point X="-25.018765625" Y="-3.190901611328" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.27676171875" Y="-3.273825439453" />
                  <Point X="-25.294806640625" Y="-3.295049316406" />
                  <Point X="-25.452005859375" Y="-3.521544433594" />
                  <Point X="-25.4592265625" Y="-3.537111083984" />
                  <Point X="-25.8438359375" Y="-4.972495605469" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.110576171875" Y="-4.935407226562" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.311072265625" Y="-4.565645996094" />
                  <Point X="-26.3091484375" Y="-4.551042480469" />
                  <Point X="-26.312095703125" Y="-4.522625488281" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.37594921875" Y="-4.215220703125" />
                  <Point X="-26.39558203125" Y="-4.19447265625" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.633375" Y="-3.989461914063" />
                  <Point X="-26.661583984375" Y="-3.984953857422" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.975041015625" Y="-3.967068115234" />
                  <Point X="-27.0001640625" Y="-3.980663330078" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157293945313" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.85583203125" Y="-4.167610839844" />
                  <Point X="-27.870138671875" Y="-4.156595214844" />
                  <Point X="-28.228580078125" Y="-3.880608154297" />
                  <Point X="-27.53571484375" Y="-2.680528564453" />
                  <Point X="-27.506033203125" Y="-2.629120117188" />
                  <Point X="-27.49976171875" Y="-2.597602294922" />
                  <Point X="-27.514669921875" Y="-2.568072998047" />
                  <Point X="-27.531326171875" Y="-2.551416259766" />
                  <Point X="-27.56015625" Y="-2.537198974609" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.829673828125" Y="-3.258224121094" />
                  <Point X="-28.842958984375" Y="-3.26589453125" />
                  <Point X="-29.16169921875" Y="-2.847134765625" />
                  <Point X="-29.171958984375" Y="-2.829932128906" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-28.21571875" Y="-1.462993408203" />
                  <Point X="-28.163787109375" Y="-1.423144897461" />
                  <Point X="-28.145513671875" Y="-1.394825927734" />
                  <Point X="-28.1381171875" Y="-1.366264770508" />
                  <Point X="-28.140328125" Y="-1.334587890625" />
                  <Point X="-28.162228515625" Y="-1.310010009766" />
                  <Point X="-28.187640625" Y="-1.295053100586" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-29.782373046875" Y="-1.494323608398" />
                  <Point X="-29.80328125" Y="-1.497076171875" />
                  <Point X="-29.927392578125" Y="-1.011186279297" />
                  <Point X="-29.93010546875" Y="-0.992221252441" />
                  <Point X="-29.99839453125" Y="-0.5147421875" />
                  <Point X="-28.61662109375" Y="-0.144496612549" />
                  <Point X="-28.557462890625" Y="-0.128645339966" />
                  <Point X="-28.540787109375" Y="-0.120657104492" />
                  <Point X="-28.514140625" Y="-0.102162963867" />
                  <Point X="-28.49452734375" Y="-0.074717384338" />
                  <Point X="-28.485646484375" Y="-0.046098842621" />
                  <Point X="-28.48601953125" Y="-0.015259732246" />
                  <Point X="-28.494896484375" Y="0.013345760345" />
                  <Point X="-28.515263671875" Y="0.040381717682" />
                  <Point X="-28.54189453125" Y="0.058865032196" />
                  <Point X="-28.557462890625" Y="0.066085227966" />
                  <Point X="-29.9820703125" Y="0.447808074951" />
                  <Point X="-29.998185546875" Y="0.452126220703" />
                  <Point X="-29.91764453125" Y="0.996415527344" />
                  <Point X="-29.912181640625" Y="1.016579162598" />
                  <Point X="-29.773515625" Y="1.528298706055" />
                  <Point X="-28.795669921875" Y="1.399563110352" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731693359375" Y="1.395869873047" />
                  <Point X="-28.729255859375" Y="1.396638671875" />
                  <Point X="-28.67027734375" Y="1.415234130859" />
                  <Point X="-28.651533203125" Y="1.426056152344" />
                  <Point X="-28.639119140625" Y="1.443786376953" />
                  <Point X="-28.63813671875" Y="1.446158813477" />
                  <Point X="-28.61447265625" Y="1.503290649414" />
                  <Point X="-28.610712890625" Y="1.524605712891" />
                  <Point X="-28.61631640625" Y="1.545512084961" />
                  <Point X="-28.617501953125" Y="1.547789916992" />
                  <Point X="-28.646056640625" Y="1.602641845703" />
                  <Point X="-28.65996875" Y="1.619221923828" />
                  <Point X="-29.47610546875" Y="2.245465576172" />
                  <Point X="-29.16001171875" Y="2.787008300781" />
                  <Point X="-29.145544921875" Y="2.805605224609" />
                  <Point X="-28.774669921875" Y="3.282310791016" />
                  <Point X="-28.185201171875" Y="2.941979980469" />
                  <Point X="-28.15915625" Y="2.926943115234" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.135103515625" Y="2.920136474609" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.03151171875" Y="2.915773681641" />
                  <Point X="-28.013259765625" Y="2.927397460938" />
                  <Point X="-28.010830078125" Y="2.929825439453" />
                  <Point X="-27.95252734375" Y="2.988127929688" />
                  <Point X="-27.9408984375" Y="3.006385253906" />
                  <Point X="-27.93807421875" Y="3.027847167969" />
                  <Point X="-27.93837109375" Y="3.031251708984" />
                  <Point X="-27.945556640625" Y="3.113390380859" />
                  <Point X="-27.95206640625" Y="3.134033447266" />
                  <Point X="-28.30727734375" Y="3.749277587891" />
                  <Point X="-27.752876953125" Y="4.174331054688" />
                  <Point X="-27.7300859375" Y="4.186992675781" />
                  <Point X="-27.141546875" Y="4.513972167969" />
                  <Point X="-26.975798828125" Y="4.29796484375" />
                  <Point X="-26.967826171875" Y="4.28757421875" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.94744921875" Y="4.271684570312" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835119140625" Y="4.218491699219" />
                  <Point X="-26.813798828125" Y="4.222253417969" />
                  <Point X="-26.809845703125" Y="4.223891601563" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.69690234375" Y="4.275744628906" />
                  <Point X="-26.68608203125" Y="4.294488769531" />
                  <Point X="-26.684794921875" Y="4.298570800781" />
                  <Point X="-26.653802734375" Y="4.396865234375" />
                  <Point X="-26.651916015625" Y="4.418424804688" />
                  <Point X="-26.68913671875" Y="4.701141113281" />
                  <Point X="-25.96809375" Y="4.903296386719" />
                  <Point X="-25.94046875" Y="4.906529296875" />
                  <Point X="-25.22419921875" Y="4.990358398438" />
                  <Point X="-25.04984375" Y="4.339657714844" />
                  <Point X="-25.042140625" Y="4.310905761719" />
                  <Point X="-25.024283203125" Y="4.284177734375" />
                  <Point X="-24.993845703125" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176269531" />
                  <Point X="-24.945546875" Y="4.310902832031" />
                  <Point X="-24.763349609375" Y="4.990869140625" />
                  <Point X="-24.139794921875" Y="4.925565429688" />
                  <Point X="-24.1169375" Y="4.920047363281" />
                  <Point X="-23.49154296875" Y="4.769058105469" />
                  <Point X="-23.4771171875" Y="4.763825683594" />
                  <Point X="-23.06896484375" Y="4.615785644531" />
                  <Point X="-23.054578125" Y="4.609057128906" />
                  <Point X="-22.66130078125" Y="4.425134765625" />
                  <Point X="-22.647392578125" Y="4.417031738281" />
                  <Point X="-22.26747265625" Y="4.195689941406" />
                  <Point X="-22.254365234375" Y="4.186368652344" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.735791015625" Y="2.563099609375" />
                  <Point X="-22.77014453125" Y="2.503598144531" />
                  <Point X="-22.77600390625" Y="2.488313476562" />
                  <Point X="-22.7966171875" Y="2.411228027344" />
                  <Point X="-22.79762109375" Y="2.389558349609" />
                  <Point X="-22.789583984375" Y="2.322901367188" />
                  <Point X="-22.78131640625" Y="2.300811767578" />
                  <Point X="-22.779603515625" Y="2.298287597656" />
                  <Point X="-22.738357421875" Y="2.237503173828" />
                  <Point X="-22.72253515625" Y="2.222490722656" />
                  <Point X="-22.66175" Y="2.18124609375" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.63689453125" Y="2.172646240234" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.548134765625" Y="2.166802490234" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417236328" />
                  <Point X="-21.026083984375" Y="3.019691894531" />
                  <Point X="-21.005751953125" Y="3.031430664062" />
                  <Point X="-20.79740234375" Y="2.741874267578" />
                  <Point X="-20.79009765625" Y="2.729802490234" />
                  <Point X="-20.612486328125" Y="2.436296142578" />
                  <Point X="-21.66615234375" Y="1.627788696289" />
                  <Point X="-21.7113828125" Y="1.593083251953" />
                  <Point X="-21.722931640625" Y="1.580827758789" />
                  <Point X="-21.77841015625" Y="1.508451904297" />
                  <Point X="-21.78773828125" Y="1.488431640625" />
                  <Point X="-21.808404296875" Y="1.414535766602" />
                  <Point X="-21.80921875" Y="1.39095690918" />
                  <Point X="-21.808515625" Y="1.387551025391" />
                  <Point X="-21.79155078125" Y="1.305332519531" />
                  <Point X="-21.7824375" Y="1.28504309082" />
                  <Point X="-21.736296875" Y="1.21491003418" />
                  <Point X="-21.719046875" Y="1.198817138672" />
                  <Point X="-21.716275390625" Y="1.197256958008" />
                  <Point X="-21.64941015625" Y="1.159617553711" />
                  <Point X="-21.6276796875" Y="1.153123413086" />
                  <Point X="-21.537271484375" Y="1.141174926758" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.1630546875" Y="1.320369506836" />
                  <Point X="-20.151025390625" Y="1.32195324707" />
                  <Point X="-20.06080859375" Y="0.951368103027" />
                  <Point X="-20.0585078125" Y="0.936592285156" />
                  <Point X="-20.002140625" Y="0.574556396484" />
                  <Point X="-21.206998046875" Y="0.251715591431" />
                  <Point X="-21.25883203125" Y="0.237826919556" />
                  <Point X="-21.274599609375" Y="0.2306875" />
                  <Point X="-21.363421875" Y="0.179347045898" />
                  <Point X="-21.379947265625" Y="0.164106674194" />
                  <Point X="-21.433240234375" Y="0.096198928833" />
                  <Point X="-21.44301171875" Y="0.074742622375" />
                  <Point X="-21.443751953125" Y="0.070882980347" />
                  <Point X="-21.461515625" Y="-0.021875694275" />
                  <Point X="-21.46077734375" Y="-0.044536151886" />
                  <Point X="-21.443013671875" Y="-0.137294815063" />
                  <Point X="-21.433240234375" Y="-0.158759033203" />
                  <Point X="-21.43102734375" Y="-0.161578964233" />
                  <Point X="-21.377734375" Y="-0.229486709595" />
                  <Point X="-21.359734375" Y="-0.2440390625" />
                  <Point X="-21.270912109375" Y="-0.295379516602" />
                  <Point X="-21.25883203125" Y="-0.300387023926" />
                  <Point X="-20.010603515625" Y="-0.634848876953" />
                  <Point X="-20.001931640625" Y="-0.637172607422" />
                  <Point X="-20.051568359375" Y="-0.96641204834" />
                  <Point X="-20.054517578125" Y="-0.979330444336" />
                  <Point X="-20.125451171875" Y="-1.290178344727" />
                  <Point X="-21.527904296875" Y="-1.105542114258" />
                  <Point X="-21.588015625" Y="-1.097628173828" />
                  <Point X="-21.612400390625" Y="-1.099914672852" />
                  <Point X="-21.7867265625" Y="-1.13780480957" />
                  <Point X="-21.802052734375" Y="-1.143923095703" />
                  <Point X="-21.818927734375" Y="-1.159959472656" />
                  <Point X="-21.924296875" Y="-1.286685180664" />
                  <Point X="-21.932044921875" Y="-1.299518066406" />
                  <Point X="-21.936267578125" Y="-1.320885742188" />
                  <Point X="-21.951369140625" Y="-1.485000976562" />
                  <Point X="-21.9501484375" Y="-1.501458251953" />
                  <Point X="-21.9396328125" Y="-1.522853271484" />
                  <Point X="-21.843158203125" Y="-1.672912475586" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-20.673173828125" Y="-2.574384521484" />
                  <Point X="-20.66092578125" Y="-2.583783203125" />
                  <Point X="-20.7958671875" Y="-2.802140380859" />
                  <Point X="-20.801970703125" Y="-2.810813964844" />
                  <Point X="-20.943310546875" Y="-3.011638183594" />
                  <Point X="-22.193005859375" Y="-2.290125732422" />
                  <Point X="-22.246544921875" Y="-2.259215332031" />
                  <Point X="-22.2712734375" Y="-2.251756835938" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.49525" Y="-2.214074462891" />
                  <Point X="-22.518080078125" Y="-2.22301171875" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7025078125" Y="-2.322614746094" />
                  <Point X="-22.715166015625" Y="-2.341841064453" />
                  <Point X="-22.80587890625" Y="-2.514201660156" />
                  <Point X="-22.811048828125" Y="-2.529873291016" />
                  <Point X="-22.809279296875" Y="-2.554990234375" />
                  <Point X="-22.77180859375" Y="-2.762465087891" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.021541015625" Y="-4.067856933594" />
                  <Point X="-22.01332421875" Y="-4.082087646484" />
                  <Point X="-22.164705078125" Y="-4.19021484375" />
                  <Point X="-22.1715234375" Y="-4.194627929688" />
                  <Point X="-22.320224609375" Y="-4.29087890625" />
                  <Point X="-23.275966796875" Y="-3.045330566406" />
                  <Point X="-23.3168203125" Y="-2.992087890625" />
                  <Point X="-23.337947265625" Y="-2.975004150391" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.55773828125" Y="-2.836937255859" />
                  <Point X="-23.58348828125" Y="-2.836572265625" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.821833984375" Y="-2.860761474609" />
                  <Point X="-23.841841796875" Y="-2.874475585938" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.02542578125" Y="-3.030661865234" />
                  <Point X="-24.0336875" Y="-3.055857421875" />
                  <Point X="-24.085357421875" Y="-3.293573974609" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-23.875123046875" Y="-4.913024902344" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-24.00564453125" Y="-4.963245117188" />
                  <Point X="-24.0119609375" Y="-4.964392578125" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#120" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.007704675999" Y="4.383878717325" Z="0.1" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.1" />
                  <Point X="-0.952438035427" Y="4.987470524619" Z="0.1" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.1" />
                  <Point X="-1.719960207573" Y="4.777433358763" Z="0.1" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.1" />
                  <Point X="-1.74881345865" Y="4.755879581922" Z="0.1" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.1" />
                  <Point X="-1.738638169725" Y="4.34488585766" Z="0.1" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.1" />
                  <Point X="-1.835144978169" Y="4.301362422626" Z="0.1" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.1" />
                  <Point X="-1.930518928965" Y="4.34731509453" Z="0.1" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.1" />
                  <Point X="-1.942288205271" Y="4.359681946167" Z="0.1" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.1" />
                  <Point X="-2.760526061943" Y="4.261980126942" Z="0.1" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.1" />
                  <Point X="-3.355060661319" Y="3.811646893241" Z="0.1" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.1" />
                  <Point X="-3.363632477803" Y="3.767501965628" Z="0.1" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.1" />
                  <Point X="-2.994338426924" Y="3.05817441508" Z="0.1" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.1" />
                  <Point X="-3.052342503043" Y="2.996461064843" Z="0.1" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.1" />
                  <Point X="-3.136902050527" Y="3.001226272249" Z="0.1" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.1" />
                  <Point X="-3.166357382669" Y="3.016561470943" Z="0.1" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.1" />
                  <Point X="-4.191164100168" Y="2.867587760212" Z="0.1" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.1" />
                  <Point X="-4.534099174379" Y="2.287122726264" Z="0.1" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.1" />
                  <Point X="-4.513721051871" Y="2.237862022633" Z="0.1" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.1" />
                  <Point X="-3.668008163918" Y="1.555982148284" Z="0.1" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.1" />
                  <Point X="-3.690487211005" Y="1.496572550423" Z="0.1" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.1" />
                  <Point X="-3.7504470045" Y="1.475604721567" Z="0.1" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.1" />
                  <Point X="-3.795301879876" Y="1.480415367196" Z="0.1" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.1" />
                  <Point X="-4.966597688451" Y="1.060936531753" Z="0.1" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.1" />
                  <Point X="-5.056838569419" Y="0.470217864375" Z="0.1" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.1" />
                  <Point X="-5.001169172287" Y="0.430791718058" Z="0.1" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.1" />
                  <Point X="-3.5499143688" Y="0.030574931759" Z="0.1" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.1" />
                  <Point X="-3.539925584511" Y="0.001188405503" Z="0.1" />
                  <Point X="-3.539556741714" Y="0" Z="0.1" />
                  <Point X="-3.548439016624" Y="-0.028618545557" Z="0.1" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.1" />
                  <Point X="-3.575454226323" Y="-0.048301051375" Z="0.1" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.1" />
                  <Point X="-3.635718616485" Y="-0.064920338698" Z="0.1" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.1" />
                  <Point X="-4.985758246918" Y="-0.968020252081" Z="0.1" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.1" />
                  <Point X="-4.852316022304" Y="-1.49996937576" Z="0.1" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.1" />
                  <Point X="-4.782005024322" Y="-1.512615865694" Z="0.1" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.1" />
                  <Point X="-3.19373216842" Y="-1.321828368544" Z="0.1" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.1" />
                  <Point X="-3.200073623273" Y="-1.351011140379" Z="0.1" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.1" />
                  <Point X="-3.252312369843" Y="-1.392045661991" Z="0.1" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.1" />
                  <Point X="-4.221058568734" Y="-2.824261786951" Z="0.1" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.1" />
                  <Point X="-3.87607842617" Y="-3.281743733367" Z="0.1" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.1" />
                  <Point X="-3.810830428184" Y="-3.270245357181" Z="0.1" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.1" />
                  <Point X="-2.556182657796" Y="-2.572147811336" Z="0.1" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.1" />
                  <Point X="-2.585171655987" Y="-2.624247954308" Z="0.1" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.1" />
                  <Point X="-2.906800336704" Y="-4.164932583666" Z="0.1" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.1" />
                  <Point X="-2.468634982632" Y="-4.43869533576" Z="0.1" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.1" />
                  <Point X="-2.442151153462" Y="-4.437856071534" Z="0.1" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.1" />
                  <Point X="-1.978541132752" Y="-3.990956686059" Z="0.1" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.1" />
                  <Point X="-1.67101019301" Y="-4.003566908221" Z="0.1" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.1" />
                  <Point X="-1.434706108548" Y="-4.200784500292" Z="0.1" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.1" />
                  <Point X="-1.36729176926" Y="-4.501100095918" Z="0.1" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.1" />
                  <Point X="-1.366801091039" Y="-4.527835485724" Z="0.1" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.1" />
                  <Point X="-1.129191309612" Y="-4.952550243903" Z="0.1" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.1" />
                  <Point X="-0.829618489039" Y="-5.011443334461" Z="0.1" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.1" />
                  <Point X="-0.80169687211" Y="-4.954157586102" Z="0.1" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.1" />
                  <Point X="-0.259886915777" Y="-3.292278044594" Z="0.1" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.1" />
                  <Point X="-0.010101446594" Y="-3.207374484683" Z="0.1" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.1" />
                  <Point X="0.243257632767" Y="-3.279737560605" Z="0.1" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.1" />
                  <Point X="0.410558943655" Y="-3.509367711174" Z="0.1" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.1" />
                  <Point X="0.433057988291" Y="-3.578378443714" Z="0.1" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.1" />
                  <Point X="0.990819905574" Y="-4.982307583652" Z="0.1" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.1" />
                  <Point X="1.169914523417" Y="-4.943320039215" Z="0.1" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.1" />
                  <Point X="1.168293231566" Y="-4.8752184478" Z="0.1" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.1" />
                  <Point X="1.009014438347" Y="-3.035197354041" Z="0.1" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.1" />
                  <Point X="1.183967785348" Y="-2.881642020081" Z="0.1" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.1" />
                  <Point X="1.414937270924" Y="-2.855081756579" Z="0.1" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.1" />
                  <Point X="1.628856648605" Y="-2.985782324111" Z="0.1" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.1" />
                  <Point X="1.678208479277" Y="-3.044487987009" Z="0.1" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.1" />
                  <Point X="2.849489866345" Y="-4.205322251188" Z="0.1" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.1" />
                  <Point X="3.038967628816" Y="-4.070504174894" Z="0.1" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.1" />
                  <Point X="3.015602299808" Y="-4.01157678046" Z="0.1" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.1" />
                  <Point X="2.233767074062" Y="-2.514823724397" Z="0.1" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.1" />
                  <Point X="2.322923308416" Y="-2.33384759327" Z="0.1" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.1" />
                  <Point X="2.499050785102" Y="-2.235978001551" Z="0.1" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.1" />
                  <Point X="2.713683079116" Y="-2.269680935429" Z="0.1" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.1" />
                  <Point X="2.775836816647" Y="-2.302147200466" Z="0.1" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.1" />
                  <Point X="4.23276064591" Y="-2.808311197171" Z="0.1" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.1" />
                  <Point X="4.392849814499" Y="-2.550636246012" Z="0.1" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.1" />
                  <Point X="4.351106661673" Y="-2.503436982168" Z="0.1" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.1" />
                  <Point X="3.096268664738" Y="-1.46453381374" Z="0.1" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.1" />
                  <Point X="3.107364342481" Y="-1.294187234764" Z="0.1" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.1" />
                  <Point X="3.213360024091" Y="-1.160646556897" Z="0.1" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.1" />
                  <Point X="3.392060875718" Y="-1.117493847642" Z="0.1" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.1" />
                  <Point X="3.459412252937" Y="-1.12383437053" Z="0.1" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.1" />
                  <Point X="4.98807209231" Y="-0.959174352167" Z="0.1" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.1" />
                  <Point X="5.045759398979" Y="-0.584123001091" Z="0.1" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.1" />
                  <Point X="4.996181525904" Y="-0.555272527481" Z="0.1" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.1" />
                  <Point X="3.659131095131" Y="-0.169470157371" Z="0.1" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.1" />
                  <Point X="3.602149751973" Y="-0.09943044927" Z="0.1" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.1" />
                  <Point X="3.582172402803" Y="-0.003851869581" Z="0.1" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.1" />
                  <Point X="3.599199047621" Y="0.092758661666" Z="0.1" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.1" />
                  <Point X="3.653229686427" Y="0.164518289305" Z="0.1" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.1" />
                  <Point X="3.744264319222" Y="0.218678686871" Z="0.1" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.1" />
                  <Point X="3.799786291402" Y="0.234699405288" Z="0.1" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.1" />
                  <Point X="4.984740411784" Y="0.975564402632" Z="0.1" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.1" />
                  <Point X="4.884825397486" Y="1.39206832667" Z="0.1" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.1" />
                  <Point X="4.82426308706" Y="1.401221839101" Z="0.1" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.1" />
                  <Point X="3.372714817348" Y="1.233972416243" Z="0.1" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.1" />
                  <Point X="3.302095186752" Y="1.272108026139" Z="0.1" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.1" />
                  <Point X="3.253177054441" Y="1.343804079146" Z="0.1" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.1" />
                  <Point X="3.234296496322" Y="1.428935018446" Z="0.1" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.1" />
                  <Point X="3.254257824177" Y="1.50624512196" Z="0.1" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.1" />
                  <Point X="3.310594379199" Y="1.581689638458" Z="0.1" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.1" />
                  <Point X="3.358127324005" Y="1.619400658521" Z="0.1" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.1" />
                  <Point X="4.246521921302" Y="2.786968850875" Z="0.1" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.1" />
                  <Point X="4.011667423091" Y="3.115554015673" Z="0.1" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.1" />
                  <Point X="3.942759723567" Y="3.094273415583" Z="0.1" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.1" />
                  <Point X="2.432792675463" Y="2.246385169183" Z="0.1" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.1" />
                  <Point X="2.362934763081" Y="2.25356676144" Z="0.1" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.1" />
                  <Point X="2.299382195803" Y="2.295145351581" Z="0.1" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.1" />
                  <Point X="2.255613248654" Y="2.357642664579" Z="0.1" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.1" />
                  <Point X="2.245862941319" Y="2.426823680751" Z="0.1" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.1" />
                  <Point X="2.266142773475" Y="2.506676900419" Z="0.1" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.1" />
                  <Point X="2.301351937277" Y="2.569379358388" Z="0.1" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.1" />
                  <Point X="2.768454251838" Y="4.258395826954" Z="0.1" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.1" />
                  <Point X="2.37162045085" Y="4.491514652061" Z="0.1" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.1" />
                  <Point X="1.960447005389" Y="4.685629340961" Z="0.1" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.1" />
                  <Point X="1.533773812352" Y="4.842109845674" Z="0.1" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.1" />
                  <Point X="0.888640828225" Y="4.99993093137" Z="0.1" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.1" />
                  <Point X="0.219930167871" Y="5.073528336686" Z="0.1" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.1" />
                  <Point X="0.185539885554" Y="5.047568788626" Z="0.1" />
                  <Point X="0" Y="4.355124473572" Z="0.1" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>