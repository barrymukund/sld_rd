<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#147" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1275" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004715820312" />
                  <Width Value="9.7838984375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.318212890625" Y="-3.954706054688" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.44227734375" Y="-3.497140136719" />
                  <Point X="-24.45763671875" Y="-3.467376464844" />
                  <Point X="-24.506064453125" Y="-3.397602783203" />
                  <Point X="-24.621365234375" Y="-3.231476074219" />
                  <Point X="-24.643248046875" Y="-3.209020751953" />
                  <Point X="-24.66950390625" Y="-3.189776855469" />
                  <Point X="-24.69750390625" Y="-3.175669189453" />
                  <Point X="-24.77244140625" Y="-3.152411376953" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097035888672" />
                  <Point X="-25.11176171875" Y="-3.120293701172" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.31818359375" Y="-3.189776855469" />
                  <Point X="-25.344439453125" Y="-3.209020751953" />
                  <Point X="-25.36632421875" Y="-3.231477294922" />
                  <Point X="-25.41475" Y="-3.301250732422" />
                  <Point X="-25.53005078125" Y="-3.467377685547" />
                  <Point X="-25.5381875" Y="-3.481572021484" />
                  <Point X="-25.550990234375" Y="-3.512524414063" />
                  <Point X="-25.83308984375" Y="-4.565332519531" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.079341796875" Y="-4.845349609375" />
                  <Point X="-26.16257421875" Y="-4.823934570312" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.229224609375" Y="-4.671772460938" />
                  <Point X="-26.2149609375" Y="-4.563438476562" />
                  <Point X="-26.21419921875" Y="-4.5479296875" />
                  <Point X="-26.2165078125" Y="-4.51622265625" />
                  <Point X="-26.234412109375" Y="-4.426220703125" />
                  <Point X="-26.277037109375" Y="-4.211931152344" />
                  <Point X="-26.287939453125" Y="-4.182965332031" />
                  <Point X="-26.30401171875" Y="-4.155127929687" />
                  <Point X="-26.32364453125" Y="-4.131204589844" />
                  <Point X="-26.39263671875" Y="-4.07069921875" />
                  <Point X="-26.556904296875" Y="-3.926639648438" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.734595703125" Y="-3.884964599609" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708496094" />
                  <Point X="-27.014466796875" Y="-3.882028076172" />
                  <Point X="-27.042658203125" Y="-3.894801757812" />
                  <Point X="-27.11895703125" Y="-3.945783935547" />
                  <Point X="-27.300623046875" Y="-4.067169921875" />
                  <Point X="-27.31278515625" Y="-4.076821533203" />
                  <Point X="-27.3351015625" Y="-4.099461425781" />
                  <Point X="-27.4801484375" Y="-4.288489257813" />
                  <Point X="-27.498578125" Y="-4.277077636719" />
                  <Point X="-27.80171484375" Y="-4.0893828125" />
                  <Point X="-27.916962890625" Y="-4.000645507812" />
                  <Point X="-28.10472265625" Y="-3.856077148438" />
                  <Point X="-27.643947265625" Y="-3.057992431641" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647655029297" />
                  <Point X="-27.406587890625" Y="-2.616129150391" />
                  <Point X="-27.40557421875" Y="-2.585194824219" />
                  <Point X="-27.41455859375" Y="-2.555576416016" />
                  <Point X="-27.428775390625" Y="-2.526747558594" />
                  <Point X="-27.44680078125" Y="-2.501591552734" />
                  <Point X="-27.4641484375" Y="-2.484242919922" />
                  <Point X="-27.489306640625" Y="-2.466214599609" />
                  <Point X="-27.518134765625" Y="-2.451997070313" />
                  <Point X="-27.547751953125" Y="-2.44301171875" />
                  <Point X="-27.5786875" Y="-2.444023925781" />
                  <Point X="-27.61021484375" Y="-2.450294921875" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.547208984375" Y="-2.985446533203" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-28.84337109375" Y="-3.108499511719" />
                  <Point X="-29.082857421875" Y="-2.793862548828" />
                  <Point X="-29.165486328125" Y="-2.65530859375" />
                  <Point X="-29.306142578125" Y="-2.419450195312" />
                  <Point X="-28.49120703125" Y="-1.79412890625" />
                  <Point X="-28.105955078125" Y="-1.498513427734" />
                  <Point X="-28.083064453125" Y="-1.475879516602" />
                  <Point X="-28.06363671875" Y="-1.444576293945" />
                  <Point X="-28.05464453125" Y="-1.420709960938" />
                  <Point X="-28.051578125" Y="-1.411034179688" />
                  <Point X="-28.04615234375" Y="-1.390085449219" />
                  <Point X="-28.042037109375" Y="-1.358609619141" />
                  <Point X="-28.042734375" Y="-1.335736572266" />
                  <Point X="-28.0488828125" Y="-1.313694213867" />
                  <Point X="-28.060888671875" Y="-1.284711303711" />
                  <Point X="-28.073802734375" Y="-1.262571411133" />
                  <Point X="-28.0921640625" Y="-1.244689453125" />
                  <Point X="-28.112498046875" Y="-1.229649902344" />
                  <Point X="-28.1208046875" Y="-1.224155395508" />
                  <Point X="-28.139455078125" Y="-1.213179199219" />
                  <Point X="-28.16871875" Y="-1.201955810547" />
                  <Point X="-28.20060546875" Y="-1.195474609375" />
                  <Point X="-28.231927734375" Y="-1.194383911133" />
                  <Point X="-29.378224609375" Y="-1.34529675293" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.74041015625" Y="-1.359352783203" />
                  <Point X="-29.834078125" Y="-0.992649658203" />
                  <Point X="-29.8559375" Y="-0.839807067871" />
                  <Point X="-29.892423828125" Y="-0.584698364258" />
                  <Point X="-28.971736328125" Y="-0.33800088501" />
                  <Point X="-28.532875" Y="-0.220408279419" />
                  <Point X="-28.512111328125" Y="-0.212121124268" />
                  <Point X="-28.4883359375" Y="-0.199203918457" />
                  <Point X="-28.479521484375" Y="-0.193773162842" />
                  <Point X="-28.4599765625" Y="-0.180208267212" />
                  <Point X="-28.436021484375" Y="-0.158682266235" />
                  <Point X="-28.41509375" Y="-0.128276382446" />
                  <Point X="-28.404931640625" Y="-0.104720947266" />
                  <Point X="-28.401427734375" Y="-0.095244522095" />
                  <Point X="-28.3949140625" Y="-0.074253890991" />
                  <Point X="-28.38947265625" Y="-0.045516941071" />
                  <Point X="-28.390443359375" Y="-0.011683779716" />
                  <Point X="-28.39542578125" Y="0.011950889587" />
                  <Point X="-28.397650390625" Y="0.020509588242" />
                  <Point X="-28.4041640625" Y="0.041500526428" />
                  <Point X="-28.417482421875" Y="0.070828399658" />
                  <Point X="-28.440396484375" Y="0.100128334045" />
                  <Point X="-28.46042578125" Y="0.117344398499" />
                  <Point X="-28.468185546875" Y="0.123345733643" />
                  <Point X="-28.48773046875" Y="0.136910629272" />
                  <Point X="-28.501923828125" Y="0.145046905518" />
                  <Point X="-28.532875" Y="0.157848297119" />
                  <Point X="-29.577779296875" Y="0.437829559326" />
                  <Point X="-29.89181640625" Y="0.521975524902" />
                  <Point X="-29.884853515625" Y="0.569027893066" />
                  <Point X="-29.82448828125" Y="0.976968017578" />
                  <Point X="-29.78048046875" Y="1.139370483398" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-29.080251953125" Y="1.341209106445" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301228149414" />
                  <Point X="-28.703134765625" Y="1.305263671875" />
                  <Point X="-28.684966796875" Y="1.3109921875" />
                  <Point X="-28.641708984375" Y="1.324631225586" />
                  <Point X="-28.62277734375" Y="1.332961914062" />
                  <Point X="-28.604033203125" Y="1.343783935547" />
                  <Point X="-28.5873515625" Y="1.356015380859" />
                  <Point X="-28.573712890625" Y="1.371567382813" />
                  <Point X="-28.561298828125" Y="1.38929699707" />
                  <Point X="-28.551349609375" Y="1.407435424805" />
                  <Point X="-28.544060546875" Y="1.42503527832" />
                  <Point X="-28.526703125" Y="1.466939697266" />
                  <Point X="-28.520916015625" Y="1.486788452148" />
                  <Point X="-28.51715625" Y="1.508104614258" />
                  <Point X="-28.515802734375" Y="1.528750488281" />
                  <Point X="-28.518951171875" Y="1.549199829102" />
                  <Point X="-28.5245546875" Y="1.570107055664" />
                  <Point X="-28.532046875" Y="1.589373901367" />
                  <Point X="-28.540841796875" Y="1.606271484375" />
                  <Point X="-28.561787109375" Y="1.646503662109" />
                  <Point X="-28.57328515625" Y="1.663710449219" />
                  <Point X="-28.587197265625" Y="1.6802890625" />
                  <Point X="-28.60213671875" Y="1.694590332031" />
                  <Point X="-29.20149609375" Y="2.154495117188" />
                  <Point X="-29.351859375" Y="2.269873291016" />
                  <Point X="-29.3157109375" Y="2.331803955078" />
                  <Point X="-29.081146484375" Y="2.733665771484" />
                  <Point X="-28.9645859375" Y="2.883490966797" />
                  <Point X="-28.75050390625" Y="3.158662109375" />
                  <Point X="-28.3998671875" Y="2.956221191406" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.146794921875" Y="2.825796386719" />
                  <Point X="-28.1214921875" Y="2.823582519531" />
                  <Point X="-28.06124609375" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.999013671875" Y="2.826504638672" />
                  <Point X="-27.980462890625" Y="2.835653076172" />
                  <Point X="-27.962208984375" Y="2.847281982422" />
                  <Point X="-27.946078125" Y="2.860228271484" />
                  <Point X="-27.9281171875" Y="2.878188720703" />
                  <Point X="-27.885353515625" Y="2.920951660156" />
                  <Point X="-27.872404296875" Y="2.937084960938" />
                  <Point X="-27.860775390625" Y="2.955339111328" />
                  <Point X="-27.85162890625" Y="2.973887939453" />
                  <Point X="-27.8467109375" Y="2.993977050781" />
                  <Point X="-27.843884765625" Y="3.015435302734" />
                  <Point X="-27.84343359375" Y="3.036123779297" />
                  <Point X="-27.8456484375" Y="3.061427246094" />
                  <Point X="-27.85091796875" Y="3.121673339844" />
                  <Point X="-27.854955078125" Y="3.1419609375" />
                  <Point X="-27.86146484375" Y="3.162604003906" />
                  <Point X="-27.869794921875" Y="3.181532958984" />
                  <Point X="-28.135390625" Y="3.641555908203" />
                  <Point X="-28.18333203125" Y="3.724595947266" />
                  <Point X="-28.1091484375" Y="3.781473144531" />
                  <Point X="-27.70062109375" Y="4.094686035156" />
                  <Point X="-27.517037109375" Y="4.196680664062" />
                  <Point X="-27.167037109375" Y="4.391133789062" />
                  <Point X="-27.10234375" Y="4.306823242188" />
                  <Point X="-27.0431953125" Y="4.229740722656" />
                  <Point X="-27.028890625" Y="4.214797851563" />
                  <Point X="-27.012310546875" Y="4.200885742188" />
                  <Point X="-26.99511328125" Y="4.189395019531" />
                  <Point X="-26.966951171875" Y="4.174734375" />
                  <Point X="-26.899896484375" Y="4.139828125" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.859708984375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.123583007812" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.77744921875" Y="4.134482421875" />
                  <Point X="-26.7481171875" Y="4.1466328125" />
                  <Point X="-26.678275390625" Y="4.175562011719" />
                  <Point X="-26.660146484375" Y="4.185508789062" />
                  <Point X="-26.642416015625" Y="4.197922851562" />
                  <Point X="-26.626859375" Y="4.211563964844" />
                  <Point X="-26.614626953125" Y="4.228249511719" />
                  <Point X="-26.603806640625" Y="4.246993652344" />
                  <Point X="-26.595478515625" Y="4.265922851563" />
                  <Point X="-26.585931640625" Y="4.296203613281" />
                  <Point X="-26.56319921875" Y="4.368299316406" />
                  <Point X="-26.5591640625" Y="4.388583496094" />
                  <Point X="-26.55727734375" Y="4.410145507812" />
                  <Point X="-26.557728515625" Y="4.430826660156" />
                  <Point X="-26.58419921875" Y="4.631897949219" />
                  <Point X="-26.4784921875" Y="4.66153515625" />
                  <Point X="-25.949634765625" Y="4.809808105469" />
                  <Point X="-25.727076171875" Y="4.83585546875" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.191060546875" Y="4.499628417969" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183885742188" />
                  <Point X="-24.932869140625" Y="4.194217773438" />
                  <Point X="-24.905576171875" Y="4.208806640625" />
                  <Point X="-24.88441796875" Y="4.23139453125" />
                  <Point X="-24.86655859375" Y="4.25812109375" />
                  <Point X="-24.853783203125" Y="4.286314941406" />
                  <Point X="-24.7176953125" Y="4.794200683594" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.617732421875" Y="4.880099121094" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.97182421875" Y="4.787283203125" />
                  <Point X="-23.5189765625" Y="4.677952148438" />
                  <Point X="-23.4001640625" Y="4.634857910156" />
                  <Point X="-23.105349609375" Y="4.527926269531" />
                  <Point X="-22.98945703125" Y="4.473727539062" />
                  <Point X="-22.705427734375" Y="4.340895996094" />
                  <Point X="-22.593431640625" Y="4.275647949219" />
                  <Point X="-22.3190234375" Y="4.115775390625" />
                  <Point X="-22.213423828125" Y="4.040680664062" />
                  <Point X="-22.05673828125" Y="3.92925390625" />
                  <Point X="-22.597568359375" Y="2.992508300781" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857921875" Y="2.539933105469" />
                  <Point X="-22.866919921875" Y="2.516061279297" />
                  <Point X="-22.873271484375" Y="2.492314453125" />
                  <Point X="-22.888390625" Y="2.435774902344" />
                  <Point X="-22.891380859375" Y="2.417935546875" />
                  <Point X="-22.892271484375" Y="2.380951416016" />
                  <Point X="-22.889794921875" Y="2.360417236328" />
                  <Point X="-22.883900390625" Y="2.311526611328" />
                  <Point X="-22.87855859375" Y="2.289608642578" />
                  <Point X="-22.87029296875" Y="2.267519042969" />
                  <Point X="-22.859927734375" Y="2.247467529297" />
                  <Point X="-22.847220703125" Y="2.228742431641" />
                  <Point X="-22.81696875" Y="2.184158935547" />
                  <Point X="-22.805533203125" Y="2.170326660156" />
                  <Point X="-22.778400390625" Y="2.145593017578" />
                  <Point X="-22.75967578125" Y="2.132887207031" />
                  <Point X="-22.715091796875" Y="2.102635498047" />
                  <Point X="-22.695046875" Y="2.092272216797" />
                  <Point X="-22.672958984375" Y="2.084006347656" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.630501953125" Y="2.0761875" />
                  <Point X="-22.581611328125" Y="2.070291992188" />
                  <Point X="-22.563533203125" Y="2.069845703125" />
                  <Point X="-22.526794921875" Y="2.074170898438" />
                  <Point X="-22.503046875" Y="2.080521240234" />
                  <Point X="-22.4465078125" Y="2.095640625" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.3604921875" Y="2.716924316406" />
                  <Point X="-21.032671875" Y="2.906191650391" />
                  <Point X="-20.8767265625" Y="2.689463378906" />
                  <Point X="-20.817859375" Y="2.592181884766" />
                  <Point X="-20.73780078125" Y="2.459883544922" />
                  <Point X="-21.433681640625" Y="1.92591394043" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.7785703125" Y="1.660244873047" />
                  <Point X="-21.79602734375" Y="1.641626342773" />
                  <Point X="-21.8131171875" Y="1.619330444336" />
                  <Point X="-21.85380859375" Y="1.566244995117" />
                  <Point X="-21.86339453125" Y="1.550909423828" />
                  <Point X="-21.8783671875" Y="1.51708996582" />
                  <Point X="-21.884734375" Y="1.494325683594" />
                  <Point X="-21.899892578125" Y="1.440125366211" />
                  <Point X="-21.90334765625" Y="1.417824584961" />
                  <Point X="-21.9041640625" Y="1.394253417969" />
                  <Point X="-21.902259765625" Y="1.371766235352" />
                  <Point X="-21.897033203125" Y="1.34643762207" />
                  <Point X="-21.88458984375" Y="1.286133178711" />
                  <Point X="-21.8793203125" Y="1.268978271484" />
                  <Point X="-21.863716796875" Y="1.235739990234" />
                  <Point X="-21.849501953125" Y="1.214134765625" />
                  <Point X="-21.81566015625" Y="1.162694580078" />
                  <Point X="-21.801107421875" Y="1.145451416016" />
                  <Point X="-21.78386328125" Y="1.129361206055" />
                  <Point X="-21.76565234375" Y="1.116034179688" />
                  <Point X="-21.745052734375" Y="1.104438964844" />
                  <Point X="-21.696009765625" Y="1.076831542969" />
                  <Point X="-21.679478515625" Y="1.069501342773" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.616029296875" Y="1.0557578125" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.51344140625" Y="1.178420166016" />
                  <Point X="-20.22316015625" Y="1.216636230469" />
                  <Point X="-20.15405859375" Y="0.932788269043" />
                  <Point X="-20.13551171875" Y="0.813658691406" />
                  <Point X="-20.109134765625" Y="0.644238708496" />
                  <Point X="-20.898892578125" Y="0.432623443604" />
                  <Point X="-21.283419921875" Y="0.329589813232" />
                  <Point X="-21.295208984375" Y="0.32558605957" />
                  <Point X="-21.318455078125" Y="0.315067443848" />
                  <Point X="-21.34581640625" Y="0.299251647949" />
                  <Point X="-21.41096484375" Y="0.26159487915" />
                  <Point X="-21.4256875" Y="0.251096298218" />
                  <Point X="-21.452466796875" Y="0.225577758789" />
                  <Point X="-21.468884765625" Y="0.204658309937" />
                  <Point X="-21.50797265625" Y="0.154850128174" />
                  <Point X="-21.51969921875" Y="0.135567443848" />
                  <Point X="-21.52947265625" Y="0.11410382843" />
                  <Point X="-21.536318359375" Y="0.092604545593" />
                  <Point X="-21.541791015625" Y="0.064029403687" />
                  <Point X="-21.5548203125" Y="-0.004006072044" />
                  <Point X="-21.556515625" Y="-0.021875591278" />
                  <Point X="-21.5548203125" Y="-0.058553909302" />
                  <Point X="-21.54934765625" Y="-0.087129051208" />
                  <Point X="-21.536318359375" Y="-0.155164520264" />
                  <Point X="-21.52947265625" Y="-0.176663955688" />
                  <Point X="-21.51969921875" Y="-0.198127578735" />
                  <Point X="-21.50797265625" Y="-0.217410263062" />
                  <Point X="-21.4915546875" Y="-0.238329711914" />
                  <Point X="-21.452466796875" Y="-0.288137878418" />
                  <Point X="-21.44" Y="-0.301235565186" />
                  <Point X="-21.41096484375" Y="-0.324154998779" />
                  <Point X="-21.383603515625" Y="-0.339970794678" />
                  <Point X="-21.318455078125" Y="-0.377627563477" />
                  <Point X="-21.3072890625" Y="-0.383138763428" />
                  <Point X="-21.283419921875" Y="-0.392149810791" />
                  <Point X="-20.367884765625" Y="-0.63746697998" />
                  <Point X="-20.108525390625" Y="-0.706961853027" />
                  <Point X="-20.144974609375" Y="-0.948724182129" />
                  <Point X="-20.1687421875" Y="-1.052877197266" />
                  <Point X="-20.19882421875" Y="-1.18469909668" />
                  <Point X="-21.129673828125" Y="-1.062150268555" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710266113" />
                  <Point X="-21.62533984375" Y="-1.005508666992" />
                  <Point X="-21.67904296875" Y="-1.017181335449" />
                  <Point X="-21.806904296875" Y="-1.04497253418" />
                  <Point X="-21.8360234375" Y="-1.056597045898" />
                  <Point X="-21.863849609375" Y="-1.073489013672" />
                  <Point X="-21.8876015625" Y="-1.093960693359" />
                  <Point X="-21.920060546875" Y="-1.132999633789" />
                  <Point X="-21.997345703125" Y="-1.225948608398" />
                  <Point X="-22.012064453125" Y="-1.250329589844" />
                  <Point X="-22.023408203125" Y="-1.277715454102" />
                  <Point X="-22.030240234375" Y="-1.305365478516" />
                  <Point X="-22.034892578125" Y="-1.355922485352" />
                  <Point X="-22.04596875" Y="-1.476295898438" />
                  <Point X="-22.04365234375" Y="-1.507561767578" />
                  <Point X="-22.035921875" Y="-1.539182739258" />
                  <Point X="-22.023548828125" Y="-1.56799597168" />
                  <Point X="-21.993830078125" Y="-1.614222900391" />
                  <Point X="-21.923068359375" Y="-1.724286376953" />
                  <Point X="-21.9130625" Y="-1.737242919922" />
                  <Point X="-21.889369140625" Y="-1.760909301758" />
                  <Point X="-21.039748046875" Y="-2.41284765625" />
                  <Point X="-20.786876953125" Y="-2.606883300781" />
                  <Point X="-20.87519140625" Y="-2.749788574219" />
                  <Point X="-20.924341796875" Y="-2.819625488281" />
                  <Point X="-20.971017578125" Y="-2.8859453125" />
                  <Point X="-21.801873046875" Y="-2.406250732422" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.245775390625" Y="-2.159825195312" />
                  <Point X="-22.309689453125" Y="-2.148282470703" />
                  <Point X="-22.461865234375" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135176513672" />
                  <Point X="-22.608263671875" Y="-2.16312109375" />
                  <Point X="-22.73468359375" Y="-2.229655517578" />
                  <Point X="-22.757615234375" Y="-2.246549804688" />
                  <Point X="-22.77857421875" Y="-2.267509521484" />
                  <Point X="-22.795466796875" Y="-2.290438476562" />
                  <Point X="-22.823412109375" Y="-2.343535644531" />
                  <Point X="-22.889947265625" Y="-2.469956542969" />
                  <Point X="-22.899771484375" Y="-2.499735351563" />
                  <Point X="-22.904728515625" Y="-2.531908691406" />
                  <Point X="-22.90432421875" Y="-2.563259033203" />
                  <Point X="-22.89278125" Y="-2.627173339844" />
                  <Point X="-22.865296875" Y="-2.779349365234" />
                  <Point X="-22.86101171875" Y="-2.795140869141" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.3022109375" Y="-3.77172265625" />
                  <Point X="-22.13871484375" Y="-4.054905029297" />
                  <Point X="-22.218150390625" Y="-4.111645019531" />
                  <Point X="-22.273103515625" Y="-4.147215332031" />
                  <Point X="-22.298232421875" Y="-4.163480957031" />
                  <Point X="-22.938373046875" Y="-3.329235351562" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.341111328125" Y="-2.860030273438" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.651841796875" Y="-2.747460693359" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.89540234375" Y="-2.795461181641" />
                  <Point X="-23.94863671875" Y="-2.839724121094" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968860839844" />
                  <Point X="-24.11275" Y="-2.996687255859" />
                  <Point X="-24.124375" Y="-3.025809326172" />
                  <Point X="-24.140291015625" Y="-3.099039794922" />
                  <Point X="-24.178189453125" Y="-3.273397216797" />
                  <Point X="-24.180275390625" Y="-3.289627197266" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-24.025533203125" Y="-4.498358886719" />
                  <Point X="-23.97793359375" Y="-4.859916015625" />
                  <Point X="-24.024306640625" Y="-4.870080566406" />
                  <Point X="-24.07068359375" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.05843359375" Y="-4.752635253906" />
                  <Point X="-26.13890234375" Y="-4.731931152344" />
                  <Point X="-26.141244140625" Y="-4.731328613281" />
                  <Point X="-26.135037109375" Y="-4.684171875" />
                  <Point X="-26.1207734375" Y="-4.575837890625" />
                  <Point X="-26.120076171875" Y="-4.568098632812" />
                  <Point X="-26.11944921875" Y="-4.541030761719" />
                  <Point X="-26.1217578125" Y="-4.509323730469" />
                  <Point X="-26.123333984375" Y="-4.4976875" />
                  <Point X="-26.14123828125" Y="-4.407685546875" />
                  <Point X="-26.18386328125" Y="-4.193395996094" />
                  <Point X="-26.188126953125" Y="-4.178466308594" />
                  <Point X="-26.199029296875" Y="-4.149500488281" />
                  <Point X="-26.20566796875" Y="-4.135464355469" />
                  <Point X="-26.221740234375" Y="-4.107626953125" />
                  <Point X="-26.23057421875" Y="-4.094861572266" />
                  <Point X="-26.25020703125" Y="-4.070938232422" />
                  <Point X="-26.261005859375" Y="-4.059780273438" />
                  <Point X="-26.329998046875" Y="-3.999274902344" />
                  <Point X="-26.494265625" Y="-3.855215332031" />
                  <Point X="-26.506736328125" Y="-3.845965820312" />
                  <Point X="-26.53301953125" Y="-3.829621582031" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.7283828125" Y="-3.79016796875" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166015625" />
                  <Point X="-27.008005859375" Y="-3.781945556641" />
                  <Point X="-27.0390546875" Y="-3.790265136719" />
                  <Point X="-27.053673828125" Y="-3.795496337891" />
                  <Point X="-27.081865234375" Y="-3.808270019531" />
                  <Point X="-27.0954375" Y="-3.8158125" />
                  <Point X="-27.171736328125" Y="-3.866794677734" />
                  <Point X="-27.35340234375" Y="-3.988180664062" />
                  <Point X="-27.359677734375" Y="-3.992754882812" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032771484375" />
                  <Point X="-27.410470703125" Y="-4.04162890625" />
                  <Point X="-27.503203125" Y="-4.162478515625" />
                  <Point X="-27.747587890625" Y="-4.011160644531" />
                  <Point X="-27.859005859375" Y="-3.925373046875" />
                  <Point X="-27.98086328125" Y="-3.831546386719" />
                  <Point X="-27.56167578125" Y="-3.105492431641" />
                  <Point X="-27.341490234375" Y="-2.724119628906" />
                  <Point X="-27.3348515625" Y="-2.710085693359" />
                  <Point X="-27.32394921875" Y="-2.68112109375" />
                  <Point X="-27.319685546875" Y="-2.666190429688" />
                  <Point X="-27.3134140625" Y="-2.634664550781" />
                  <Point X="-27.311638671875" Y="-2.619240478516" />
                  <Point X="-27.310625" Y="-2.588306152344" />
                  <Point X="-27.3146640625" Y="-2.557618652344" />
                  <Point X="-27.3236484375" Y="-2.528000244141" />
                  <Point X="-27.32935546875" Y="-2.513559082031" />
                  <Point X="-27.343572265625" Y="-2.484730224609" />
                  <Point X="-27.351552734375" Y="-2.471414550781" />
                  <Point X="-27.369578125" Y="-2.446258544922" />
                  <Point X="-27.379623046875" Y="-2.434418212891" />
                  <Point X="-27.396970703125" Y="-2.417069580078" />
                  <Point X="-27.4088125" Y="-2.407022705078" />
                  <Point X="-27.433970703125" Y="-2.388994384766" />
                  <Point X="-27.447287109375" Y="-2.381012939453" />
                  <Point X="-27.476115234375" Y="-2.366795410156" />
                  <Point X="-27.4905546875" Y="-2.361088623047" />
                  <Point X="-27.520171875" Y="-2.352103271484" />
                  <Point X="-27.550859375" Y="-2.3480625" />
                  <Point X="-27.581794921875" Y="-2.349074707031" />
                  <Point X="-27.597220703125" Y="-2.350849121094" />
                  <Point X="-27.628748046875" Y="-2.357120117188" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372286132812" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.594708984375" Y="-2.903174072266" />
                  <Point X="-28.793087890625" Y="-3.017708251953" />
                  <Point X="-29.004017578125" Y="-2.740589111328" />
                  <Point X="-29.08389453125" Y="-2.606649658203" />
                  <Point X="-29.181265625" Y="-2.443373535156" />
                  <Point X="-28.433375" Y="-1.869497680664" />
                  <Point X="-28.048123046875" Y="-1.573882080078" />
                  <Point X="-28.03916015625" Y="-1.56606628418" />
                  <Point X="-28.01626953125" Y="-1.543432373047" />
                  <Point X="-28.002345703125" Y="-1.525975585938" />
                  <Point X="-27.98291796875" Y="-1.494672363281" />
                  <Point X="-27.97473828125" Y="-1.478071166992" />
                  <Point X="-27.96574609375" Y="-1.454204833984" />
                  <Point X="-27.95961328125" Y="-1.434853515625" />
                  <Point X="-27.9541875" Y="-1.413904785156" />
                  <Point X="-27.951953125" Y="-1.402401245117" />
                  <Point X="-27.947837890625" Y="-1.370925415039" />
                  <Point X="-27.94708203125" Y="-1.35571496582" />
                  <Point X="-27.947779296875" Y="-1.332841918945" />
                  <Point X="-27.951228515625" Y="-1.310211914062" />
                  <Point X="-27.957376953125" Y="-1.288169555664" />
                  <Point X="-27.961115234375" Y="-1.277337524414" />
                  <Point X="-27.97312109375" Y="-1.248354492188" />
                  <Point X="-27.978828125" Y="-1.236845947266" />
                  <Point X="-27.9917421875" Y="-1.214706054688" />
                  <Point X="-28.007521484375" Y="-1.194513671875" />
                  <Point X="-28.0258828125" Y="-1.176631835938" />
                  <Point X="-28.035671875" Y="-1.168310791016" />
                  <Point X="-28.056005859375" Y="-1.153271240234" />
                  <Point X="-28.07262109375" Y="-1.142282104492" />
                  <Point X="-28.091271484375" Y="-1.131305786133" />
                  <Point X="-28.105435546875" Y="-1.124479003906" />
                  <Point X="-28.13469921875" Y="-1.113255615234" />
                  <Point X="-28.149796875" Y="-1.10885949707" />
                  <Point X="-28.18168359375" Y="-1.102378173828" />
                  <Point X="-28.197298828125" Y="-1.100532104492" />
                  <Point X="-28.22862109375" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196655273" />
                  <Point X="-29.390625" Y="-1.25110949707" />
                  <Point X="-29.66091796875" Y="-1.286694335938" />
                  <Point X="-29.740763671875" Y="-0.974108032227" />
                  <Point X="-29.76189453125" Y="-0.826357177734" />
                  <Point X="-29.786451171875" Y="-0.654654541016" />
                  <Point X="-28.9471484375" Y="-0.429763793945" />
                  <Point X="-28.508287109375" Y="-0.312171295166" />
                  <Point X="-28.49766015625" Y="-0.308640441895" />
                  <Point X="-28.476896484375" Y="-0.300353240967" />
                  <Point X="-28.466759765625" Y="-0.295596710205" />
                  <Point X="-28.442984375" Y="-0.282679443359" />
                  <Point X="-28.42535546875" Y="-0.271818237305" />
                  <Point X="-28.405810546875" Y="-0.258253265381" />
                  <Point X="-28.39648046875" Y="-0.250870437622" />
                  <Point X="-28.372525390625" Y="-0.229344482422" />
                  <Point X="-28.357765625" Y="-0.21254385376" />
                  <Point X="-28.336837890625" Y="-0.182137985229" />
                  <Point X="-28.327865234375" Y="-0.165907913208" />
                  <Point X="-28.317703125" Y="-0.142352523804" />
                  <Point X="-28.3106953125" Y="-0.123399841309" />
                  <Point X="-28.304181640625" Y="-0.102409233093" />
                  <Point X="-28.301572265625" Y="-0.091928344727" />
                  <Point X="-28.296130859375" Y="-0.063191364288" />
                  <Point X="-28.29451171875" Y="-0.042792423248" />
                  <Point X="-28.295482421875" Y="-0.008959312439" />
                  <Point X="-28.297486328125" Y="0.007912508011" />
                  <Point X="-28.30246875" Y="0.031547117233" />
                  <Point X="-28.30691796875" Y="0.048664463043" />
                  <Point X="-28.313431640625" Y="0.069655517578" />
                  <Point X="-28.317666015625" Y="0.080781272888" />
                  <Point X="-28.330984375" Y="0.110109184265" />
                  <Point X="-28.3426484375" Y="0.129351837158" />
                  <Point X="-28.3655625" Y="0.158651794434" />
                  <Point X="-28.378470703125" Y="0.172172180176" />
                  <Point X="-28.3985" Y="0.189388198853" />
                  <Point X="-28.41401953125" Y="0.201390838623" />
                  <Point X="-28.433564453125" Y="0.214955657959" />
                  <Point X="-28.440484375" Y="0.219329177856" />
                  <Point X="-28.465615234375" Y="0.23283454895" />
                  <Point X="-28.49656640625" Y="0.245635879517" />
                  <Point X="-28.508287109375" Y="0.249611251831" />
                  <Point X="-29.55319140625" Y="0.529592590332" />
                  <Point X="-29.7854453125" Y="0.591824768066" />
                  <Point X="-29.73133203125" Y="0.957522644043" />
                  <Point X="-29.688787109375" Y="1.1145234375" />
                  <Point X="-29.6335859375" Y="1.318236938477" />
                  <Point X="-29.09265234375" Y="1.247021972656" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767736328125" Y="1.204815551758" />
                  <Point X="-28.747056640625" Y="1.204364257812" />
                  <Point X="-28.736705078125" Y="1.204703369141" />
                  <Point X="-28.71514453125" Y="1.20658984375" />
                  <Point X="-28.704890625" Y="1.208053344727" />
                  <Point X="-28.6846015625" Y="1.212088867188" />
                  <Point X="-28.6563984375" Y="1.220389282227" />
                  <Point X="-28.613140625" Y="1.234028198242" />
                  <Point X="-28.6034453125" Y="1.237677490234" />
                  <Point X="-28.584513671875" Y="1.246008300781" />
                  <Point X="-28.57527734375" Y="1.250689575195" />
                  <Point X="-28.556533203125" Y="1.26151171875" />
                  <Point X="-28.547859375" Y="1.267171630859" />
                  <Point X="-28.531177734375" Y="1.279403076172" />
                  <Point X="-28.51592578125" Y="1.293377685547" />
                  <Point X="-28.502287109375" Y="1.3089296875" />
                  <Point X="-28.495892578125" Y="1.317078613281" />
                  <Point X="-28.483478515625" Y="1.334808227539" />
                  <Point X="-28.478005859375" Y="1.34360949707" />
                  <Point X="-28.468056640625" Y="1.361748046875" />
                  <Point X="-28.456291015625" Y="1.388684814453" />
                  <Point X="-28.43893359375" Y="1.430589233398" />
                  <Point X="-28.4355" Y="1.440348632813" />
                  <Point X="-28.429712890625" Y="1.460197387695" />
                  <Point X="-28.427359375" Y="1.470286987305" />
                  <Point X="-28.423599609375" Y="1.491603149414" />
                  <Point X="-28.422359375" Y="1.501889770508" />
                  <Point X="-28.421005859375" Y="1.522535766602" />
                  <Point X="-28.421908203125" Y="1.543206665039" />
                  <Point X="-28.425056640625" Y="1.563656005859" />
                  <Point X="-28.427189453125" Y="1.573793579102" />
                  <Point X="-28.43279296875" Y="1.594700805664" />
                  <Point X="-28.436013671875" Y="1.604537597656" />
                  <Point X="-28.443505859375" Y="1.623804443359" />
                  <Point X="-28.456572265625" Y="1.650132080078" />
                  <Point X="-28.477517578125" Y="1.690364379883" />
                  <Point X="-28.482798828125" Y="1.699285522461" />
                  <Point X="-28.494296875" Y="1.716492431641" />
                  <Point X="-28.500513671875" Y="1.724777832031" />
                  <Point X="-28.51442578125" Y="1.741356445312" />
                  <Point X="-28.52150390625" Y="1.748913818359" />
                  <Point X="-28.536443359375" Y="1.763215209961" />
                  <Point X="-28.5443046875" Y="1.769958862305" />
                  <Point X="-29.1436640625" Y="2.229863769531" />
                  <Point X="-29.22761328125" Y="2.294280517578" />
                  <Point X="-29.00228125" Y="2.680325927734" />
                  <Point X="-28.88960546875" Y="2.825157226562" />
                  <Point X="-28.726337890625" Y="3.035013427734" />
                  <Point X="-28.4473671875" Y="2.873948730469" />
                  <Point X="-28.25415625" Y="2.7623984375" />
                  <Point X="-28.244916015625" Y="2.757716064453" />
                  <Point X="-28.225982421875" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.185615234375" Y="2.736657226562" />
                  <Point X="-28.165330078125" Y="2.732622070312" />
                  <Point X="-28.155076171875" Y="2.731157958984" />
                  <Point X="-28.1297734375" Y="2.728944091797" />
                  <Point X="-28.06952734375" Y="2.723673339844" />
                  <Point X="-28.059173828125" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310791016" />
                  <Point X="-27.97643359375" Y="2.734227294922" />
                  <Point X="-27.95699609375" Y="2.741302001953" />
                  <Point X="-27.9384453125" Y="2.750450439453" />
                  <Point X="-27.929419921875" Y="2.755530761719" />
                  <Point X="-27.911166015625" Y="2.767159667969" />
                  <Point X="-27.90274609375" Y="2.773192626953" />
                  <Point X="-27.886615234375" Y="2.786138916016" />
                  <Point X="-27.878904296875" Y="2.793052246094" />
                  <Point X="-27.860943359375" Y="2.811012695312" />
                  <Point X="-27.8181796875" Y="2.853775634766" />
                  <Point X="-27.811265625" Y="2.861486572266" />
                  <Point X="-27.79831640625" Y="2.877619873047" />
                  <Point X="-27.79228125" Y="2.886042236328" />
                  <Point X="-27.78065234375" Y="2.904296386719" />
                  <Point X="-27.7755703125" Y="2.913324707031" />
                  <Point X="-27.766423828125" Y="2.931873535156" />
                  <Point X="-27.759353515625" Y="2.951298339844" />
                  <Point X="-27.754435546875" Y="2.971387451172" />
                  <Point X="-27.7525234375" Y="2.981572265625" />
                  <Point X="-27.749697265625" Y="3.003030517578" />
                  <Point X="-27.748908203125" Y="3.013364013672" />
                  <Point X="-27.74845703125" Y="3.034052490234" />
                  <Point X="-27.748794921875" Y="3.044407470703" />
                  <Point X="-27.751009765625" Y="3.0697109375" />
                  <Point X="-27.756279296875" Y="3.12995703125" />
                  <Point X="-27.757744140625" Y="3.140214111328" />
                  <Point X="-27.76178125" Y="3.160501708984" />
                  <Point X="-27.764353515625" Y="3.170532226562" />
                  <Point X="-27.77086328125" Y="3.191175292969" />
                  <Point X="-27.77451171875" Y="3.200869384766" />
                  <Point X="-27.782841796875" Y="3.219798339844" />
                  <Point X="-27.7875234375" Y="3.229033203125" />
                  <Point X="-28.053119140625" Y="3.689056152344" />
                  <Point X="-28.051345703125" Y="3.70608203125" />
                  <Point X="-27.648369140625" Y="4.015039306641" />
                  <Point X="-27.470900390625" Y="4.113636230469" />
                  <Point X="-27.192525390625" Y="4.268295898437" />
                  <Point X="-27.177712890625" Y="4.248991210937" />
                  <Point X="-27.118564453125" Y="4.171908691406" />
                  <Point X="-27.1118203125" Y="4.164046875" />
                  <Point X="-27.097515625" Y="4.149104003906" />
                  <Point X="-27.089955078125" Y="4.142022949219" />
                  <Point X="-27.073375" Y="4.128110839844" />
                  <Point X="-27.06508984375" Y="4.121895996094" />
                  <Point X="-27.047892578125" Y="4.110405273438" />
                  <Point X="-27.03898046875" Y="4.105129394531" />
                  <Point X="-27.010818359375" Y="4.09046875" />
                  <Point X="-26.943763671875" Y="4.0555625" />
                  <Point X="-26.934326171875" Y="4.051286621094" />
                  <Point X="-26.915044921875" Y="4.0437890625" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.884294921875" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834472656" />
                  <Point X="-26.85371875" Y="4.029688476562" />
                  <Point X="-26.833056640625" Y="4.028786132812" />
                  <Point X="-26.81241796875" Y="4.030138183594" />
                  <Point X="-26.802134765625" Y="4.031378173828" />
                  <Point X="-26.780818359375" Y="4.035136474609" />
                  <Point X="-26.77073046875" Y="4.037488037109" />
                  <Point X="-26.7508671875" Y="4.043277099609" />
                  <Point X="-26.741091796875" Y="4.046714599609" />
                  <Point X="-26.711759765625" Y="4.058864990234" />
                  <Point X="-26.64191796875" Y="4.087794189453" />
                  <Point X="-26.632578125" Y="4.092274658203" />
                  <Point X="-26.61444921875" Y="4.102221679688" />
                  <Point X="-26.60566015625" Y="4.1076875" />
                  <Point X="-26.5879296875" Y="4.1201015625" />
                  <Point X="-26.579783203125" Y="4.126494140625" />
                  <Point X="-26.5642265625" Y="4.140135253906" />
                  <Point X="-26.5502421875" Y="4.155395507812" />
                  <Point X="-26.538009765625" Y="4.172081054688" />
                  <Point X="-26.5323515625" Y="4.180754882812" />
                  <Point X="-26.52153125" Y="4.199499023438" />
                  <Point X="-26.516849609375" Y="4.208736328125" />
                  <Point X="-26.508521484375" Y="4.227665527344" />
                  <Point X="-26.504875" Y="4.237357421875" />
                  <Point X="-26.495328125" Y="4.267638183594" />
                  <Point X="-26.472595703125" Y="4.339733886719" />
                  <Point X="-26.470025390625" Y="4.349764160156" />
                  <Point X="-26.465990234375" Y="4.370048339844" />
                  <Point X="-26.464525390625" Y="4.380302246094" />
                  <Point X="-26.462638671875" Y="4.401864257812" />
                  <Point X="-26.46230078125" Y="4.412217285156" />
                  <Point X="-26.462751953125" Y="4.4328984375" />
                  <Point X="-26.463541015625" Y="4.443226074219" />
                  <Point X="-26.479263671875" Y="4.562655761719" />
                  <Point X="-26.452845703125" Y="4.5700625" />
                  <Point X="-25.931177734375" Y="4.716319824219" />
                  <Point X="-25.716033203125" Y="4.741499511719" />
                  <Point X="-25.36522265625" Y="4.782556152344" />
                  <Point X="-25.28282421875" Y="4.475040527344" />
                  <Point X="-25.22566796875" Y="4.261727539062" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.978369140625" Y="4.085113037109" />
                  <Point X="-24.94783203125" Y="4.090154541016" />
                  <Point X="-24.93276953125" Y="4.093927246094" />
                  <Point X="-24.90233203125" Y="4.104259277344" />
                  <Point X="-24.8880859375" Y="4.110436035156" />
                  <Point X="-24.86079296875" Y="4.125024902344" />
                  <Point X="-24.8362421875" Y="4.143861328125" />
                  <Point X="-24.815083984375" Y="4.16644921875" />
                  <Point X="-24.8054296875" Y="4.178612792969" />
                  <Point X="-24.7875703125" Y="4.205339355469" />
                  <Point X="-24.78002734375" Y="4.218911621094" />
                  <Point X="-24.767251953125" Y="4.24710546875" />
                  <Point X="-24.76201953125" Y="4.261727050781" />
                  <Point X="-24.625931640625" Y="4.769612792969" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.994119140625" Y="4.694936523438" />
                  <Point X="-23.546408203125" Y="4.586845703125" />
                  <Point X="-23.432556640625" Y="4.54555078125" />
                  <Point X="-23.141740234375" Y="4.440069335938" />
                  <Point X="-23.029701171875" Y="4.387672851562" />
                  <Point X="-22.74955078125" Y="4.256654785156" />
                  <Point X="-22.64125390625" Y="4.1935625" />
                  <Point X="-22.370564453125" Y="4.035856445312" />
                  <Point X="-22.268478515625" Y="3.963260498047" />
                  <Point X="-22.182216796875" Y="3.901916015625" />
                  <Point X="-22.67983984375" Y="3.040008300781" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.937619140625" Y="2.593115234375" />
                  <Point X="-22.94681640625" Y="2.573440429688" />
                  <Point X="-22.955814453125" Y="2.549568603516" />
                  <Point X="-22.958693359375" Y="2.540608154297" />
                  <Point X="-22.965044921875" Y="2.516861328125" />
                  <Point X="-22.9801640625" Y="2.460321777344" />
                  <Point X="-22.982083984375" Y="2.451479736328" />
                  <Point X="-22.986353515625" Y="2.42022265625" />
                  <Point X="-22.987244140625" Y="2.383238525391" />
                  <Point X="-22.986587890625" Y="2.369576171875" />
                  <Point X="-22.984111328125" Y="2.349041992188" />
                  <Point X="-22.978216796875" Y="2.300151367188" />
                  <Point X="-22.97619921875" Y="2.289031982422" />
                  <Point X="-22.970857421875" Y="2.267114013672" />
                  <Point X="-22.967533203125" Y="2.256315429688" />
                  <Point X="-22.959267578125" Y="2.234225830078" />
                  <Point X="-22.95468359375" Y="2.22389453125" />
                  <Point X="-22.944318359375" Y="2.203843017578" />
                  <Point X="-22.938537109375" Y="2.194122802734" />
                  <Point X="-22.925830078125" Y="2.175397705078" />
                  <Point X="-22.895578125" Y="2.130814208984" />
                  <Point X="-22.8901875" Y="2.123627197266" />
                  <Point X="-22.869533203125" Y="2.100119384766" />
                  <Point X="-22.842400390625" Y="2.075385742188" />
                  <Point X="-22.8317421875" Y="2.066982421875" />
                  <Point X="-22.813017578125" Y="2.054276611328" />
                  <Point X="-22.76843359375" Y="2.024025024414" />
                  <Point X="-22.758720703125" Y="2.018246459961" />
                  <Point X="-22.73867578125" Y="2.007883300781" />
                  <Point X="-22.72834375" Y="2.003298339844" />
                  <Point X="-22.706255859375" Y="1.995032470703" />
                  <Point X="-22.695451171875" Y="1.991707519531" />
                  <Point X="-22.67352734375" Y="1.986364868164" />
                  <Point X="-22.662408203125" Y="1.984346801758" />
                  <Point X="-22.641875" Y="1.981870727539" />
                  <Point X="-22.592984375" Y="1.975975219727" />
                  <Point X="-22.583955078125" Y="1.975320922852" />
                  <Point X="-22.55242578125" Y="1.975497314453" />
                  <Point X="-22.5156875" Y="1.979822631836" />
                  <Point X="-22.50225390625" Y="1.982395507812" />
                  <Point X="-22.478505859375" Y="1.988745849609" />
                  <Point X="-22.421966796875" Y="2.003865112305" />
                  <Point X="-22.416005859375" Y="2.005670654297" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572875977" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.3129921875" Y="2.634651855469" />
                  <Point X="-21.05959375" Y="2.780951660156" />
                  <Point X="-20.956041015625" Y="2.637036865234" />
                  <Point X="-20.89913671875" Y="2.542999023438" />
                  <Point X="-20.863115234375" Y="2.483471435547" />
                  <Point X="-21.491513671875" Y="2.001282592773" />
                  <Point X="-21.827046875" Y="1.743819946289" />
                  <Point X="-21.831861328125" Y="1.739868530273" />
                  <Point X="-21.847873046875" Y="1.725223510742" />
                  <Point X="-21.865330078125" Y="1.706604980469" />
                  <Point X="-21.87142578125" Y="1.699419677734" />
                  <Point X="-21.888515625" Y="1.677123657227" />
                  <Point X="-21.92920703125" Y="1.624038208008" />
                  <Point X="-21.934365234375" Y="1.616599487305" />
                  <Point X="-21.95026171875" Y="1.589367675781" />
                  <Point X="-21.965234375" Y="1.555548217773" />
                  <Point X="-21.96985546875" Y="1.542679321289" />
                  <Point X="-21.97622265625" Y="1.519915161133" />
                  <Point X="-21.991380859375" Y="1.46571484375" />
                  <Point X="-21.9937734375" Y="1.454670288086" />
                  <Point X="-21.997228515625" Y="1.432369506836" />
                  <Point X="-21.998291015625" Y="1.421113037109" />
                  <Point X="-21.999107421875" Y="1.397541870117" />
                  <Point X="-21.998826171875" Y="1.386237182617" />
                  <Point X="-21.996921875" Y="1.36375" />
                  <Point X="-21.995298828125" Y="1.352567504883" />
                  <Point X="-21.990072265625" Y="1.327238891602" />
                  <Point X="-21.97762890625" Y="1.266934448242" />
                  <Point X="-21.97540234375" Y="1.258238037109" />
                  <Point X="-21.96531640625" Y="1.228608154297" />
                  <Point X="-21.949712890625" Y="1.195369750977" />
                  <Point X="-21.943080078125" Y="1.183524169922" />
                  <Point X="-21.928865234375" Y="1.161918945312" />
                  <Point X="-21.8950234375" Y="1.110478759766" />
                  <Point X="-21.888259765625" Y="1.101422485352" />
                  <Point X="-21.87370703125" Y="1.084179321289" />
                  <Point X="-21.86591796875" Y="1.075992431641" />
                  <Point X="-21.848673828125" Y="1.05990222168" />
                  <Point X="-21.839966796875" Y="1.052697265625" />
                  <Point X="-21.821755859375" Y="1.039370361328" />
                  <Point X="-21.812251953125" Y="1.033248291016" />
                  <Point X="-21.79165234375" Y="1.021652832031" />
                  <Point X="-21.742609375" Y="0.994045532227" />
                  <Point X="-21.734517578125" Y="0.989986328125" />
                  <Point X="-21.7053203125" Y="0.978083557129" />
                  <Point X="-21.66972265625" Y="0.968020935059" />
                  <Point X="-21.656328125" Y="0.96525769043" />
                  <Point X="-21.6284765625" Y="0.961576782227" />
                  <Point X="-21.562166015625" Y="0.952812927246" />
                  <Point X="-21.555966796875" Y="0.952199707031" />
                  <Point X="-21.53428125" Y="0.951222961426" />
                  <Point X="-21.50878125" Y="0.952032348633" />
                  <Point X="-21.49939453125" Y="0.952797180176" />
                  <Point X="-20.501041015625" Y="1.084232910156" />
                  <Point X="-20.295296875" Y="1.111319580078" />
                  <Point X="-20.247310546875" Y="0.91420489502" />
                  <Point X="-20.229380859375" Y="0.799044555664" />
                  <Point X="-20.21612890625" Y="0.713921203613" />
                  <Point X="-20.92348046875" Y="0.524386474609" />
                  <Point X="-21.3080078125" Y="0.421352722168" />
                  <Point X="-21.31396875" Y="0.419543701172" />
                  <Point X="-21.334373046875" Y="0.412137817383" />
                  <Point X="-21.357619140625" Y="0.401619171143" />
                  <Point X="-21.365998046875" Y="0.39731552124" />
                  <Point X="-21.393359375" Y="0.381499664307" />
                  <Point X="-21.4585078125" Y="0.343842926025" />
                  <Point X="-21.46612109375" Y="0.338943267822" />
                  <Point X="-21.491224609375" Y="0.319870941162" />
                  <Point X="-21.51800390625" Y="0.294352386475" />
                  <Point X="-21.52719921875" Y="0.284229370117" />
                  <Point X="-21.5436171875" Y="0.263309967041" />
                  <Point X="-21.582705078125" Y="0.213501831055" />
                  <Point X="-21.589140625" Y="0.204212142944" />
                  <Point X="-21.6008671875" Y="0.184929519653" />
                  <Point X="-21.606158203125" Y="0.174936264038" />
                  <Point X="-21.615931640625" Y="0.153472732544" />
                  <Point X="-21.619994140625" Y="0.142927352905" />
                  <Point X="-21.62683984375" Y="0.121428153992" />
                  <Point X="-21.629623046875" Y="0.110474052429" />
                  <Point X="-21.635095703125" Y="0.081898918152" />
                  <Point X="-21.648125" Y="0.013863368988" />
                  <Point X="-21.649396484375" Y="0.004966505051" />
                  <Point X="-21.6514140625" Y="-0.02626184082" />
                  <Point X="-21.64971875" Y="-0.062940189362" />
                  <Point X="-21.648125" Y="-0.076423408508" />
                  <Point X="-21.64265234375" Y="-0.104998542786" />
                  <Point X="-21.629623046875" Y="-0.173033935547" />
                  <Point X="-21.62683984375" Y="-0.18398789978" />
                  <Point X="-21.619994140625" Y="-0.205487380981" />
                  <Point X="-21.615931640625" Y="-0.216032775879" />
                  <Point X="-21.606158203125" Y="-0.237496444702" />
                  <Point X="-21.6008671875" Y="-0.247489562988" />
                  <Point X="-21.589140625" Y="-0.266772338867" />
                  <Point X="-21.582705078125" Y="-0.276061859131" />
                  <Point X="-21.566287109375" Y="-0.296981292725" />
                  <Point X="-21.52719921875" Y="-0.346789581299" />
                  <Point X="-21.521279296875" Y="-0.353635284424" />
                  <Point X="-21.498861328125" Y="-0.375803283691" />
                  <Point X="-21.469826171875" Y="-0.398722717285" />
                  <Point X="-21.4585078125" Y="-0.406402954102" />
                  <Point X="-21.431146484375" Y="-0.422218811035" />
                  <Point X="-21.365998046875" Y="-0.459875549316" />
                  <Point X="-21.360501953125" Y="-0.462816192627" />
                  <Point X="-21.340841796875" Y="-0.472016265869" />
                  <Point X="-21.31697265625" Y="-0.481027252197" />
                  <Point X="-21.3080078125" Y="-0.483912780762" />
                  <Point X="-20.39247265625" Y="-0.729229858398" />
                  <Point X="-20.215119140625" Y="-0.776751220703" />
                  <Point X="-20.238380859375" Y="-0.931036987305" />
                  <Point X="-20.261361328125" Y="-1.031741699219" />
                  <Point X="-20.2721953125" Y="-1.079219726563" />
                  <Point X="-21.1172734375" Y="-0.967963012695" />
                  <Point X="-21.563216796875" Y="-0.909253540039" />
                  <Point X="-21.571375" Y="-0.908535522461" />
                  <Point X="-21.59990234375" Y="-0.908042541504" />
                  <Point X="-21.63327734375" Y="-0.910840820312" />
                  <Point X="-21.645517578125" Y="-0.912676147461" />
                  <Point X="-21.699220703125" Y="-0.924348876953" />
                  <Point X="-21.82708203125" Y="-0.952140014648" />
                  <Point X="-21.842125" Y="-0.956743164062" />
                  <Point X="-21.871244140625" Y="-0.968367553711" />
                  <Point X="-21.8853203125" Y="-0.975388916016" />
                  <Point X="-21.913146484375" Y="-0.992280944824" />
                  <Point X="-21.92587109375" Y="-1.001528869629" />
                  <Point X="-21.949623046875" Y="-1.022000488281" />
                  <Point X="-21.960650390625" Y="-1.033224365234" />
                  <Point X="-21.993109375" Y="-1.072263305664" />
                  <Point X="-22.07039453125" Y="-1.165212280273" />
                  <Point X="-22.078673828125" Y="-1.176850585938" />
                  <Point X="-22.093392578125" Y="-1.201231567383" />
                  <Point X="-22.09983203125" Y="-1.213974121094" />
                  <Point X="-22.11117578125" Y="-1.241360229492" />
                  <Point X="-22.115634765625" Y="-1.254927246094" />
                  <Point X="-22.122466796875" Y="-1.282577270508" />
                  <Point X="-22.12483984375" Y="-1.29666027832" />
                  <Point X="-22.1294921875" Y="-1.347217163086" />
                  <Point X="-22.140568359375" Y="-1.467590576172" />
                  <Point X="-22.140708984375" Y="-1.483314941406" />
                  <Point X="-22.138392578125" Y="-1.514580810547" />
                  <Point X="-22.135935546875" Y="-1.530122314453" />
                  <Point X="-22.128205078125" Y="-1.561743286133" />
                  <Point X="-22.123212890625" Y="-1.57666784668" />
                  <Point X="-22.11083984375" Y="-1.605481079102" />
                  <Point X="-22.103458984375" Y="-1.619369750977" />
                  <Point X="-22.073740234375" Y="-1.665596679688" />
                  <Point X="-22.002978515625" Y="-1.77566015625" />
                  <Point X="-21.9982578125" Y="-1.782352050781" />
                  <Point X="-21.98019921875" Y="-1.804456176758" />
                  <Point X="-21.956505859375" Y="-1.828122680664" />
                  <Point X="-21.947201171875" Y="-1.836277832031" />
                  <Point X="-21.097580078125" Y="-2.488216064453" />
                  <Point X="-20.912828125" Y="-2.629981933594" />
                  <Point X="-20.9545" Y="-2.697411865234" />
                  <Point X="-20.998724609375" Y="-2.760251708984" />
                  <Point X="-21.754373046875" Y="-2.323978271484" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.18497265625" Y="-2.079513427734" />
                  <Point X="-22.216880859375" Y="-2.069325927734" />
                  <Point X="-22.228892578125" Y="-2.066337646484" />
                  <Point X="-22.292806640625" Y="-2.054794921875" />
                  <Point X="-22.444982421875" Y="-2.027312011719" />
                  <Point X="-22.460640625" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503417969" />
                  <Point X="-22.539859375" Y="-2.031461181641" />
                  <Point X="-22.555154296875" Y="-2.035136108398" />
                  <Point X="-22.5849296875" Y="-2.044959594727" />
                  <Point X="-22.59941015625" Y="-2.051108154297" />
                  <Point X="-22.6525078125" Y="-2.079052734375" />
                  <Point X="-22.778927734375" Y="-2.145587158203" />
                  <Point X="-22.79103125" Y="-2.153170898438" />
                  <Point X="-22.813962890625" Y="-2.170065185547" />
                  <Point X="-22.824791015625" Y="-2.179375732422" />
                  <Point X="-22.84575" Y="-2.200335449219" />
                  <Point X="-22.85505859375" Y="-2.211160888672" />
                  <Point X="-22.871951171875" Y="-2.23408984375" />
                  <Point X="-22.87953515625" Y="-2.246193359375" />
                  <Point X="-22.90748046875" Y="-2.299290527344" />
                  <Point X="-22.974015625" Y="-2.425711425781" />
                  <Point X="-22.9801640625" Y="-2.440193359375" />
                  <Point X="-22.98998828125" Y="-2.469972167969" />
                  <Point X="-22.9936640625" Y="-2.485269042969" />
                  <Point X="-22.99862109375" Y="-2.517442382812" />
                  <Point X="-22.999720703125" Y="-2.533133789062" />
                  <Point X="-22.99931640625" Y="-2.564484130859" />
                  <Point X="-22.9978125" Y="-2.580143066406" />
                  <Point X="-22.98626953125" Y="-2.644057373047" />
                  <Point X="-22.95878515625" Y="-2.796233398438" />
                  <Point X="-22.95698046875" Y="-2.804228759766" />
                  <Point X="-22.94876171875" Y="-2.831542236328" />
                  <Point X="-22.935927734375" Y="-2.862479980469" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.384482421875" Y="-3.81922265625" />
                  <Point X="-22.264103515625" Y="-4.027722900391" />
                  <Point X="-22.2762421875" Y="-4.036083007813" />
                  <Point X="-22.86300390625" Y="-3.271402832031" />
                  <Point X="-23.16608203125" Y="-2.876422607422" />
                  <Point X="-23.17134765625" Y="-2.870142089844" />
                  <Point X="-23.19116796875" Y="-2.849625732422" />
                  <Point X="-23.21674609375" Y="-2.828004638672" />
                  <Point X="-23.22669921875" Y="-2.820646972656" />
                  <Point X="-23.289736328125" Y="-2.780120117188" />
                  <Point X="-23.43982421875" Y="-2.683628173828" />
                  <Point X="-23.453716796875" Y="-2.676245117188" />
                  <Point X="-23.482529296875" Y="-2.663873535156" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.660546875" Y="-2.652860351562" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920125" Y="-2.699414794922" />
                  <Point X="-23.944505859375" Y="-2.714135742188" />
                  <Point X="-23.956138671875" Y="-2.722413330078" />
                  <Point X="-24.009373046875" Y="-2.766676269531" />
                  <Point X="-24.136123046875" Y="-2.872063720703" />
                  <Point X="-24.147345703125" Y="-2.883087158203" />
                  <Point X="-24.16781640625" Y="-2.906836425781" />
                  <Point X="-24.177064453125" Y="-2.919562255859" />
                  <Point X="-24.19395703125" Y="-2.947388671875" />
                  <Point X="-24.20098046875" Y="-2.961467285156" />
                  <Point X="-24.21260546875" Y="-2.990589355469" />
                  <Point X="-24.21720703125" Y="-3.0056328125" />
                  <Point X="-24.233123046875" Y="-3.07886328125" />
                  <Point X="-24.271021484375" Y="-3.253220703125" />
                  <Point X="-24.2724140625" Y="-3.261287109375" />
                  <Point X="-24.275275390625" Y="-3.289677001953" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520263672" />
                  <Point X="-24.16691015625" Y="-4.152323242188" />
                  <Point X="-24.22644921875" Y="-3.930118408203" />
                  <Point X="-24.344931640625" Y="-3.487937011719" />
                  <Point X="-24.347392578125" Y="-3.480122314453" />
                  <Point X="-24.35785546875" Y="-3.453574707031" />
                  <Point X="-24.37321484375" Y="-3.423811035156" />
                  <Point X="-24.37959375" Y="-3.413208740234" />
                  <Point X="-24.428021484375" Y="-3.343435058594" />
                  <Point X="-24.543322265625" Y="-3.177308349609" />
                  <Point X="-24.553328125" Y="-3.165173828125" />
                  <Point X="-24.5752109375" Y="-3.142718505859" />
                  <Point X="-24.587087890625" Y="-3.132397705078" />
                  <Point X="-24.61334375" Y="-3.113153808594" />
                  <Point X="-24.6267578125" Y="-3.104937255859" />
                  <Point X="-24.6547578125" Y="-3.090829589844" />
                  <Point X="-24.66934375" Y="-3.084938476562" />
                  <Point X="-24.74428125" Y="-3.061680664062" />
                  <Point X="-24.922703125" Y="-3.006305175781" />
                  <Point X="-24.936623046875" Y="-3.003109130859" />
                  <Point X="-24.964783203125" Y="-2.99883984375" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.051064453125" Y="-3.003109130859" />
                  <Point X="-25.064984375" Y="-3.006305175781" />
                  <Point X="-25.139921875" Y="-3.029562988281" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.3329296875" Y="-3.090829589844" />
                  <Point X="-25.3609296875" Y="-3.104937255859" />
                  <Point X="-25.37434375" Y="-3.113153808594" />
                  <Point X="-25.400599609375" Y="-3.132397705078" />
                  <Point X="-25.412474609375" Y="-3.142717285156" />
                  <Point X="-25.434359375" Y="-3.165173828125" />
                  <Point X="-25.444369140625" Y="-3.177310791016" />
                  <Point X="-25.492794921875" Y="-3.247084228516" />
                  <Point X="-25.608095703125" Y="-3.413211181641" />
                  <Point X="-25.61246875" Y="-3.420132080078" />
                  <Point X="-25.625974609375" Y="-3.445260986328" />
                  <Point X="-25.63877734375" Y="-3.476213378906" />
                  <Point X="-25.64275390625" Y="-3.487936523438" />
                  <Point X="-25.924853515625" Y="-4.540744628906" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.361987571079" Y="4.770482631675" />
                  <Point X="-26.467622438252" Y="4.474228661892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.33739979684" Y="4.678719668787" />
                  <Point X="-26.465059398765" Y="4.376564189114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.137371870393" Y="4.19641860528" />
                  <Point X="-27.493799443493" Y="4.100914124907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.587204735772" Y="4.781382592427" />
                  <Point X="-24.625529496196" Y="4.771113503821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.312812022602" Y="4.586956705899" />
                  <Point X="-26.494479407603" Y="4.270329884365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.060324734556" Y="4.118712085967" />
                  <Point X="-27.756428206345" Y="3.932191722853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.323299427649" Y="4.753744569478" />
                  <Point X="-24.653921155371" Y="4.665154744534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.288224248363" Y="4.495193743011" />
                  <Point X="-26.548744553014" Y="4.157438345336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.938258699686" Y="4.053068344295" />
                  <Point X="-27.953628113632" Y="3.781000929809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.089647370746" Y="4.718000212296" />
                  <Point X="-24.682312783051" Y="4.559195993687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.263636695138" Y="4.403430720902" />
                  <Point X="-28.036586568946" Y="3.660421041563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.896567368331" Y="4.671384605879" />
                  <Point X="-24.710704410731" Y="4.453237242839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.239049204112" Y="4.311667682127" />
                  <Point X="-27.987410737186" Y="3.575246428832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.703486747624" Y="4.624769165132" />
                  <Point X="-24.739096038411" Y="4.347278491992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.208797607398" Y="4.221422335898" />
                  <Point X="-27.938234905425" Y="3.4900718161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.517329499826" Y="4.576298612206" />
                  <Point X="-24.770203346162" Y="4.240592076862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.146206717366" Y="4.139842277197" />
                  <Point X="-27.889059073664" Y="3.404897203369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.361379168072" Y="4.51973414052" />
                  <Point X="-24.887267196196" Y="4.110873675643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.983407059734" Y="4.085113076848" />
                  <Point X="-27.839883241904" Y="3.319722590637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.205428781866" Y="4.463169683424" />
                  <Point X="-27.790707410143" Y="3.234547977905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.66580704752" Y="3.000065736774" />
                  <Point X="-28.776629588307" Y="2.970370926467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.062642003786" Y="4.403078048161" />
                  <Point X="-27.758653814543" Y="3.144785475822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.549456722245" Y="2.932890475331" />
                  <Point X="-28.873298576937" Y="2.846117311891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.92894316545" Y="4.340551306783" />
                  <Point X="-27.749194192639" Y="3.048968936733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.433106335154" Y="2.865715230452" />
                  <Point X="-28.969965229971" Y="2.721864323137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.795244677738" Y="4.278024471456" />
                  <Point X="-27.760725938902" Y="2.947527777495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.316755505543" Y="2.798540104146" />
                  <Point X="-29.04758174123" Y="2.602715804486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.673436495472" Y="4.212311638387" />
                  <Point X="-27.845492337839" Y="2.826463452216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.182767701931" Y="2.736090790781" />
                  <Point X="-29.115631695756" Y="2.486130636987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.557804216229" Y="4.14494397709" />
                  <Point X="-29.183681650282" Y="2.369545469488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.44217278463" Y="4.077576088668" />
                  <Point X="-29.193943394981" Y="2.268444606144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.332319989482" Y="4.008659819275" />
                  <Point X="-29.098943558222" Y="2.195548498546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.231867441354" Y="3.937224761285" />
                  <Point X="-29.003943601595" Y="2.122652423066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.216188952675" Y="3.843074562526" />
                  <Point X="-28.908943644967" Y="2.049756347586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.283364160079" Y="3.726723782812" />
                  <Point X="-28.813943688339" Y="1.976860272107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.350539367483" Y="3.610373003097" />
                  <Point X="-28.718943731712" Y="1.903964196627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.417714574887" Y="3.494022223383" />
                  <Point X="-28.623943775084" Y="1.831068121148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.484889782291" Y="3.377671443669" />
                  <Point X="-28.530687210455" Y="1.75770490519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.552064989695" Y="3.261320663955" />
                  <Point X="-28.469858521946" Y="1.675652666014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.619240197098" Y="3.14496988424" />
                  <Point X="-28.430925597722" Y="1.58773347448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.501565969632" Y="1.300856251442" />
                  <Point X="-29.649000500376" Y="1.261351287993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.686415375203" Y="3.028619112377" />
                  <Point X="-28.423647358114" Y="1.491332435766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.25544246007" Y="1.268453609929" />
                  <Point X="-29.677737716229" Y="1.155299937073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.753590283287" Y="2.912268412865" />
                  <Point X="-28.458492580643" Y="1.38364444939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.009319048984" Y="1.236050942029" />
                  <Point X="-29.706475696166" Y="1.049248381418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.820765191371" Y="2.795917713353" />
                  <Point X="-28.564575587803" Y="1.256868356152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.759509343192" Y="1.204636013818" />
                  <Point X="-29.73337912193" Y="0.943688393072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.887940099455" Y="2.679567013842" />
                  <Point X="-29.748533263258" Y="0.841276616002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.950170845873" Y="2.564541098456" />
                  <Point X="-29.763687404586" Y="0.738864838932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.98066402567" Y="2.458019238415" />
                  <Point X="-29.778841545914" Y="0.636453061863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.985244860905" Y="2.358440570175" />
                  <Point X="-29.681894994854" Y="0.564078574789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.056646532997" Y="2.776855697523" />
                  <Point X="-21.075384444207" Y="2.771834889346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.969956053499" Y="2.264185956633" />
                  <Point X="-29.498369024068" Y="0.514902973313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.99731739013" Y="2.694401656303" />
                  <Point X="-21.393260201446" Y="2.588309099762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.927128078903" Y="2.177310440701" />
                  <Point X="-29.314843141977" Y="0.46572734807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.94045845351" Y="2.611285725313" />
                  <Point X="-21.711136444886" Y="2.4047831799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.864677285787" Y="2.095692843144" />
                  <Point X="-29.131317259886" Y="0.416551722828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.889247450407" Y="2.5266564351" />
                  <Point X="-22.029012688326" Y="2.221257260038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.767396379143" Y="2.023407946379" />
                  <Point X="-28.947791377795" Y="0.367376097585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.959562976596" Y="2.409464209503" />
                  <Point X="-22.346888931766" Y="2.037731340176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.579719341908" Y="1.975344619805" />
                  <Point X="-28.764265495704" Y="0.318200472343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.156509428036" Y="2.258341329748" />
                  <Point X="-28.580739613613" Y="0.2690248471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.353455879477" Y="2.107218449994" />
                  <Point X="-28.428526331832" Y="0.211459035892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.550403025641" Y="1.956095384088" />
                  <Point X="-28.347023652413" Y="0.134946375884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.747351800508" Y="1.804971881773" />
                  <Point X="-28.306596469776" Y="0.047427569685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.89654904931" Y="1.666643362305" />
                  <Point X="-28.294908545827" Y="-0.047791897671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.967483309991" Y="1.549285347301" />
                  <Point X="-28.322531581283" Y="-0.153544704852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.995516025096" Y="1.443422766788" />
                  <Point X="-28.448401847054" Y="-0.285622778056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.993844456013" Y="1.345519425235" />
                  <Point X="-29.774350890726" Y="-0.739260990652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.973490650002" Y="1.252621973979" />
                  <Point X="-29.760804003225" Y="-0.833982350225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.931275460837" Y="1.165582262686" />
                  <Point X="-29.747257249187" Y="-0.928703745559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.872552479713" Y="1.082965800916" />
                  <Point X="-29.728521477456" Y="-1.022034747793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.773164319048" Y="1.011245541165" />
                  <Point X="-29.705008384967" Y="-1.114085670788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.603699785823" Y="0.958302188849" />
                  <Point X="-29.681495292478" Y="-1.206136593783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.023244188896" Y="1.015483560149" />
                  <Point X="-29.570821512585" Y="-1.274832880976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.301647989893" Y="1.110483441794" />
                  <Point X="-28.849224426427" Y="-1.179832761619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.273017890329" Y="1.019803616712" />
                  <Point X="-28.18961064676" Y="-1.101441019179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.250541076114" Y="0.927475023791" />
                  <Point X="-28.04536475042" Y="-1.161141684883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.234723853604" Y="0.83336199865" />
                  <Point X="-27.976718897627" Y="-1.241099321202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.220025255983" Y="0.738949238873" />
                  <Point X="-27.94794707068" Y="-1.331741170546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.51986084269" Y="0.292308105983" />
                  <Point X="-27.959158187788" Y="-1.43309641746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.608285985155" Y="0.17026342333" />
                  <Point X="-28.020872825056" Y="-1.547984041816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.63856181395" Y="0.063799802315" />
                  <Point X="-28.207830391746" Y="-1.696430407968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.650878369314" Y="-0.037851645887" />
                  <Point X="-28.404776536833" Y="-1.847553205636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.63736966442" Y="-0.132583236459" />
                  <Point X="-28.601724252668" Y="-1.998676424184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.612215273318" Y="-0.224194374816" />
                  <Point X="-28.798672235337" Y="-2.14979971423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.557690434428" Y="-0.307935725407" />
                  <Point X="-28.995620218005" Y="-2.300923004276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.484938862417" Y="-0.386793237577" />
                  <Point X="-29.178363391745" Y="-2.448240127241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.373801183723" Y="-0.455365223462" />
                  <Point X="-29.127791822808" Y="-2.533040753323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.210649092361" Y="-0.509999989477" />
                  <Point X="-29.077220179663" Y="-2.617841359521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.027123249658" Y="-0.559175625273" />
                  <Point X="-29.026648048436" Y="-2.702641834939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.843597406955" Y="-0.60835126107" />
                  <Point X="-27.451502352565" Y="-2.378934054907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.890383184475" Y="-2.496531819391" />
                  <Point X="-28.969663085752" Y="-2.785723997346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.660071564252" Y="-0.657526896866" />
                  <Point X="-21.595305166464" Y="-0.908121985313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.915339834172" Y="-0.993875016076" />
                  <Point X="-27.364122749261" Y="-2.453871997906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.208260017774" Y="-2.680057897305" />
                  <Point X="-28.907484251725" Y="-2.867414466121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.47654572155" Y="-0.706702532663" />
                  <Point X="-21.34144177971" Y="-0.938450732984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.036759828456" Y="-1.124760642628" />
                  <Point X="-27.31989544804" Y="-2.5403725654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.526136851073" Y="-2.863583975219" />
                  <Point X="-28.845305417699" Y="-2.949104934896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.293019457905" Y="-0.755878055668" />
                  <Point X="-21.095318501836" Y="-0.970853436578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.111785346096" Y="-1.24321490663" />
                  <Point X="-27.313902108913" Y="-2.63711789216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.224012748772" Y="-0.835739000823" />
                  <Point X="-20.849195375295" Y="-1.003256180721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.129406662111" Y="-1.346287761165" />
                  <Point X="-27.354291054818" Y="-2.746291314737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.240058733765" Y="-0.938389746682" />
                  <Point X="-20.603072248753" Y="-1.035658924865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.138685234248" Y="-1.447125184215" />
                  <Point X="-27.421466147259" Y="-2.862642063647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.263963740067" Y="-1.043146310955" />
                  <Point X="-20.356949122212" Y="-1.068061669009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.132581704412" Y="-1.543840985464" />
                  <Point X="-27.4886412397" Y="-2.978992812557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.095222272851" Y="-1.632181793086" />
                  <Point X="-27.55581633214" Y="-3.095343561467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.041283513958" Y="-1.716080183339" />
                  <Point X="-27.622991481111" Y="-3.211694325524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.984483645638" Y="-1.799211941631" />
                  <Point X="-27.690166635483" Y="-3.328045091028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.897653545395" Y="-1.874297123531" />
                  <Point X="-22.462985116688" Y="-2.025777261515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.642857562917" Y="-2.073973938223" />
                  <Point X="-27.757341789855" Y="-3.444395856533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.802653657464" Y="-1.947193217418" />
                  <Point X="-22.239892667448" Y="-2.064351057043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.871603067941" Y="-2.233617348705" />
                  <Point X="-27.824516944227" Y="-3.560746622037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.707653769533" Y="-2.020089311305" />
                  <Point X="-22.099067792231" Y="-2.124968382593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.933392518502" Y="-2.348525019223" />
                  <Point X="-27.891692098599" Y="-3.677097387541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.612653881602" Y="-2.092985405192" />
                  <Point X="-21.982717143204" Y="-2.192143557286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.987118028286" Y="-2.461271963321" />
                  <Point X="-23.723889558952" Y="-2.658689299969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.952773049983" Y="-2.720018446552" />
                  <Point X="-24.989343276191" Y="-2.997766601562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.325578786985" Y="-3.087860635146" />
                  <Point X="-27.958867252971" Y="-3.793448153046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.51765399367" Y="-2.165881499078" />
                  <Point X="-21.866366494178" Y="-2.25931873198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.999336871075" Y="-2.562897229518" />
                  <Point X="-23.443205198778" Y="-2.681831389472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.128021054885" Y="-2.86532724508" />
                  <Point X="-24.798278063441" Y="-3.044922069244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.479096262872" Y="-3.227346755974" />
                  <Point X="-27.917133109266" Y="-3.880616760082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.422654105739" Y="-2.238777592965" />
                  <Point X="-21.750015838222" Y="-2.326493904817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.983911175385" Y="-2.657115163954" />
                  <Point X="-23.334814283048" Y="-2.751139368274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.210707294666" Y="-2.985834193393" />
                  <Point X="-24.63663435751" Y="-3.099961005917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.562951622746" Y="-3.348166969072" />
                  <Point X="-27.82237478496" Y="-3.953577580747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.327654217808" Y="-2.311673686852" />
                  <Point X="-21.633665004162" Y="-2.393669029931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.966967996225" Y="-2.75092648992" />
                  <Point X="-23.226837233649" Y="-2.820558242225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.235737682453" Y="-3.090892302726" />
                  <Point X="-24.546023036342" Y="-3.174033012724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.634414978979" Y="-3.465666754802" />
                  <Point X="-26.822458211001" Y="-3.784001979395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.19871084698" Y="-3.884818569356" />
                  <Point X="-27.724221380837" Y="-4.025628692517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.232654329877" Y="-2.384569780739" />
                  <Point X="-21.517314170103" Y="-2.460844155045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.943961702184" Y="-2.84311320915" />
                  <Point X="-23.149401833299" Y="-2.898160726375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.258437438621" Y="-3.195325921198" />
                  <Point X="-24.488105509408" Y="-3.256865295293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.665361798244" Y="-3.572310167171" />
                  <Point X="-26.567395218826" Y="-3.814009293762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.408519165443" Y="-4.039387775992" />
                  <Point X="-27.613356617657" Y="-4.094273805893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.137654441945" Y="-2.457465874625" />
                  <Point X="-21.400963336043" Y="-2.528019280159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.898316685067" Y="-2.929233900814" />
                  <Point X="-23.086804711664" Y="-2.979739115323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.275270923958" Y="-3.298187677139" />
                  <Point X="-24.43054845716" Y="-3.339794166763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.693753412084" Y="-3.67826891431" />
                  <Point X="-26.461468710007" Y="-3.883977608406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.042654692941" Y="-2.530362005737" />
                  <Point X="-21.284612501983" Y="-2.595194405273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.84914114808" Y="-3.01440859253" />
                  <Point X="-23.02420759003" Y="-3.061317504272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.266713843665" Y="-3.394246051524" />
                  <Point X="-24.373747541974" Y="-3.422925644549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.722145025923" Y="-3.784227661449" />
                  <Point X="-26.375567207438" Y="-3.959311607303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.947655045298" Y="-2.603258164009" />
                  <Point X="-21.168261667923" Y="-2.662369530387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.799965611094" Y="-3.099583284246" />
                  <Point X="-22.961610468396" Y="-3.14289589322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.254206743591" Y="-3.489246021298" />
                  <Point X="-24.338526958699" Y="-3.511839554842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.750536639763" Y="-3.890186408588" />
                  <Point X="-26.28966597183" Y="-4.034645677732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.95973136484" Y="-2.704845241217" />
                  <Point X="-21.051910833864" Y="-2.729544655501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.750790074107" Y="-3.184757975962" />
                  <Point X="-22.899013346761" Y="-3.224474282169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.241699643516" Y="-3.584245991073" />
                  <Point X="-24.313939096404" Y="-3.603602494136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.778928253603" Y="-3.996145155728" />
                  <Point X="-26.218155512258" Y="-4.113835744978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.701614537121" Y="-3.269932667678" />
                  <Point X="-22.836416087952" Y="-3.306052634362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.229192543442" Y="-3.679245960847" />
                  <Point X="-24.28935123411" Y="-3.695365433429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.807319867443" Y="-4.102103902867" />
                  <Point X="-26.182049887342" Y="-4.202512509078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.652439000134" Y="-3.355107359394" />
                  <Point X="-22.773818643358" Y="-3.387630936773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.216685443367" Y="-3.774245930622" />
                  <Point X="-24.264763371816" Y="-3.787128372723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.835711481282" Y="-4.208062650006" />
                  <Point X="-26.163476474363" Y="-4.295887015209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.603263463148" Y="-3.44028205111" />
                  <Point X="-22.711221198764" Y="-3.469209239185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.204178343293" Y="-3.869245900396" />
                  <Point X="-24.240175509521" Y="-3.878891312017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.864103095122" Y="-4.314021397145" />
                  <Point X="-26.144903061383" Y="-4.389261521339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.554087926161" Y="-3.525456742826" />
                  <Point X="-22.648623754169" Y="-3.550787541597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.191671243218" Y="-3.96424587017" />
                  <Point X="-24.21558776312" Y="-3.970654282364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.892494708962" Y="-4.419980144284" />
                  <Point X="-26.126328281508" Y="-4.482635661211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.504912389175" Y="-3.610631434542" />
                  <Point X="-22.586026309575" Y="-3.632365844008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.179164143144" Y="-4.059245839945" />
                  <Point X="-24.191000163178" Y="-4.062417291954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.920886322802" Y="-4.525938891423" />
                  <Point X="-26.121273030439" Y="-4.579632347909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.455736852188" Y="-3.695806126258" />
                  <Point X="-22.523428864981" Y="-3.71394414642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.949278415707" Y="-4.631897766927" />
                  <Point X="-26.134695883038" Y="-4.681580227562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.406561315202" Y="-3.780980817974" />
                  <Point X="-22.460831420387" Y="-3.795522448832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.977670586425" Y="-4.737856663282" />
                  <Point X="-26.043583014467" Y="-4.755517845147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.357385492673" Y="-3.86615543318" />
                  <Point X="-22.398233975792" Y="-3.877100751243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.308209437479" Y="-3.951329986042" />
                  <Point X="-22.335636531198" Y="-3.958679053655" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.4099765625" Y="-3.979293701172" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544189453" />
                  <Point X="-24.584107421875" Y="-3.451770507813" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.8006015625" Y="-3.243142089844" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.0836015625" Y="-3.211024414062" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285643798828" />
                  <Point X="-25.336705078125" Y="-3.355417236328" />
                  <Point X="-25.452005859375" Y="-3.521544189453" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.741326171875" Y="-4.589920410156" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-25.890638671875" Y="-4.978750976562" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.18624609375" Y="-4.915937988281" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.323412109375" Y="-4.659373046875" />
                  <Point X="-26.3091484375" Y="-4.5510390625" />
                  <Point X="-26.309681640625" Y="-4.5347578125" />
                  <Point X="-26.3275859375" Y="-4.444755859375" />
                  <Point X="-26.3702109375" Y="-4.230466308594" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.455275390625" Y="-4.142123535156" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.74080859375" Y="-3.979761230469" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791015625" />
                  <Point X="-27.066177734375" Y="-4.024773193359" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157293945313" />
                  <Point X="-27.418109375" Y="-4.363694335938" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.54858984375" Y="-4.357848144531" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-27.974919921875" Y="-4.07591796875" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.72621875" Y="-3.010492431641" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.59759375" />
                  <Point X="-27.513978515625" Y="-2.568764892578" />
                  <Point X="-27.531326171875" Y="-2.551416259766" />
                  <Point X="-27.560154296875" Y="-2.537198730469" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.499708984375" Y="-3.067718994141" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-28.91896484375" Y="-3.166037597656" />
                  <Point X="-29.161697265625" Y="-2.847135742188" />
                  <Point X="-29.247078125" Y="-2.703967529297" />
                  <Point X="-29.43101953125" Y="-2.395526855469" />
                  <Point X="-28.5490390625" Y="-1.718760253906" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.15253515625" Y="-1.411081420898" />
                  <Point X="-28.14354296875" Y="-1.387215087891" />
                  <Point X="-28.1381171875" Y="-1.366266235352" />
                  <Point X="-28.136650390625" Y="-1.350051025391" />
                  <Point X="-28.14865625" Y="-1.321068115234" />
                  <Point X="-28.168990234375" Y="-1.306028564453" />
                  <Point X="-28.187640625" Y="-1.295052246094" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-29.36582421875" Y="-1.439484008789" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.832455078125" Y="-1.382864257812" />
                  <Point X="-29.927392578125" Y="-1.011187866211" />
                  <Point X="-29.94998046875" Y="-0.853257446289" />
                  <Point X="-29.99839453125" Y="-0.514742248535" />
                  <Point X="-28.99632421875" Y="-0.246237960815" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.5336875" Y="-0.115728294373" />
                  <Point X="-28.514142578125" Y="-0.102163330078" />
                  <Point X="-28.502322265625" Y="-0.090644638062" />
                  <Point X="-28.49216015625" Y="-0.067089271545" />
                  <Point X="-28.485646484375" Y="-0.04609859848" />
                  <Point X="-28.483400390625" Y="-0.031280040741" />
                  <Point X="-28.4883828125" Y="-0.007645431042" />
                  <Point X="-28.494896484375" Y="0.013345396042" />
                  <Point X="-28.502322265625" Y="0.028084554672" />
                  <Point X="-28.5223515625" Y="0.045300613403" />
                  <Point X="-28.541896484375" Y="0.058865428925" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.6023671875" Y="0.346066558838" />
                  <Point X="-29.998185546875" Y="0.452126037598" />
                  <Point X="-29.978830078125" Y="0.58293347168" />
                  <Point X="-29.91764453125" Y="0.996415039063" />
                  <Point X="-29.872173828125" Y="1.164217163086" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.0678515625" Y="1.435396362305" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866577148" />
                  <Point X="-28.71353515625" Y="1.401595092773" />
                  <Point X="-28.67027734375" Y="1.415234130859" />
                  <Point X="-28.651533203125" Y="1.426056152344" />
                  <Point X="-28.639119140625" Y="1.443785766602" />
                  <Point X="-28.631830078125" Y="1.461385742188" />
                  <Point X="-28.61447265625" Y="1.503289916992" />
                  <Point X="-28.610712890625" Y="1.524606079102" />
                  <Point X="-28.61631640625" Y="1.545513305664" />
                  <Point X="-28.625111328125" Y="1.562410888672" />
                  <Point X="-28.646056640625" Y="1.602643066406" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-29.259328125" Y="2.079126464844" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.3977578125" Y="2.379693603516" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-29.03956640625" Y="2.941824951172" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.3523671875" Y="3.038493652344" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.1132109375" Y="2.918220947266" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927404296875" />
                  <Point X="-27.995291015625" Y="2.945364746094" />
                  <Point X="-27.95252734375" Y="2.988127685547" />
                  <Point X="-27.9408984375" Y="3.006381835938" />
                  <Point X="-27.938072265625" Y="3.027840087891" />
                  <Point X="-27.940287109375" Y="3.053143554688" />
                  <Point X="-27.945556640625" Y="3.113389648438" />
                  <Point X="-27.95206640625" Y="3.134032714844" />
                  <Point X="-28.217662109375" Y="3.594055664062" />
                  <Point X="-28.30727734375" Y="3.749276611328" />
                  <Point X="-28.166951171875" Y="3.856864501953" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.563173828125" Y="4.279725097656" />
                  <Point X="-27.141548828125" Y="4.513971679688" />
                  <Point X="-27.026974609375" Y="4.364655273438" />
                  <Point X="-26.967826171875" Y="4.287572753906" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.923083984375" Y="4.259" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.784474609375" Y="4.234400878906" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.69690234375" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.29448828125" />
                  <Point X="-26.67653515625" Y="4.324769042969" />
                  <Point X="-26.653802734375" Y="4.396864746094" />
                  <Point X="-26.651916015625" Y="4.418426757812" />
                  <Point X="-26.682111328125" Y="4.647781738281" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.504138671875" Y="4.7530078125" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.738119140625" Y="4.930211425781" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.099296875" Y="4.524216308594" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176269531" />
                  <Point X="-24.945546875" Y="4.310902832031" />
                  <Point X="-24.809458984375" Y="4.818788574219" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.607837890625" Y="4.974582519531" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.949529296875" Y="4.879629882812" />
                  <Point X="-23.491544921875" Y="4.76905859375" />
                  <Point X="-23.367771484375" Y="4.724165039062" />
                  <Point X="-23.06896484375" Y="4.615785644531" />
                  <Point X="-22.949212890625" Y="4.559781738281" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.545609375" Y="4.357733398438" />
                  <Point X="-22.26747265625" Y="4.195688964844" />
                  <Point X="-22.1583671875" Y="4.118100585938" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.515296875" Y="2.945008300781" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491514404297" />
                  <Point X="-22.781498046875" Y="2.467767578125" />
                  <Point X="-22.7966171875" Y="2.411228027344" />
                  <Point X="-22.797955078125" Y="2.392326660156" />
                  <Point X="-22.795478515625" Y="2.371792480469" />
                  <Point X="-22.789583984375" Y="2.322901855469" />
                  <Point X="-22.781318359375" Y="2.300812255859" />
                  <Point X="-22.768611328125" Y="2.282087158203" />
                  <Point X="-22.738359375" Y="2.237503662109" />
                  <Point X="-22.72505859375" Y="2.224203613281" />
                  <Point X="-22.706333984375" Y="2.211497802734" />
                  <Point X="-22.66175" Y="2.18124609375" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.61912890625" Y="2.170504150391" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.5513359375" Y="2.165946289062" />
                  <Point X="-22.527587890625" Y="2.172296630859" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.4079921875" Y="2.799196777344" />
                  <Point X="-21.005751953125" Y="3.031430908203" />
                  <Point X="-20.96238671875" Y="2.971165039062" />
                  <Point X="-20.797404296875" Y="2.741876464844" />
                  <Point X="-20.73658203125" Y="2.641365234375" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.375849609375" Y="1.850545410156" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583833129883" />
                  <Point X="-21.73771875" Y="1.561537231445" />
                  <Point X="-21.77841015625" Y="1.508451782227" />
                  <Point X="-21.78687890625" Y="1.491500488281" />
                  <Point X="-21.79324609375" Y="1.468736206055" />
                  <Point X="-21.808404296875" Y="1.414536010742" />
                  <Point X="-21.809220703125" Y="1.390965087891" />
                  <Point X="-21.803994140625" Y="1.36563671875" />
                  <Point X="-21.79155078125" Y="1.30533215332" />
                  <Point X="-21.784353515625" Y="1.287955810547" />
                  <Point X="-21.770138671875" Y="1.266350585938" />
                  <Point X="-21.736296875" Y="1.214910400391" />
                  <Point X="-21.719052734375" Y="1.19882019043" />
                  <Point X="-21.698453125" Y="1.187224975586" />
                  <Point X="-21.64941015625" Y="1.159617675781" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.60358203125" Y="1.149938842773" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.525841796875" Y="1.272607421875" />
                  <Point X="-20.1510234375" Y="1.321953125" />
                  <Point X="-20.131669921875" Y="1.242447631836" />
                  <Point X="-20.060806640625" Y="0.951366882324" />
                  <Point X="-20.041642578125" Y="0.828272827148" />
                  <Point X="-20.002140625" Y="0.574556396484" />
                  <Point X="-20.8743046875" Y="0.340860534668" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819503784" />
                  <Point X="-21.2982734375" Y="0.217003662109" />
                  <Point X="-21.363421875" Y="0.179346984863" />
                  <Point X="-21.377734375" Y="0.166926239014" />
                  <Point X="-21.39415234375" Y="0.146006744385" />
                  <Point X="-21.433240234375" Y="0.096198562622" />
                  <Point X="-21.443013671875" Y="0.074734954834" />
                  <Point X="-21.448486328125" Y="0.046159915924" />
                  <Point X="-21.461515625" Y="-0.021875602722" />
                  <Point X="-21.461515625" Y="-0.040684474945" />
                  <Point X="-21.45604296875" Y="-0.069259513855" />
                  <Point X="-21.443013671875" Y="-0.137295028687" />
                  <Point X="-21.433240234375" Y="-0.158758636475" />
                  <Point X="-21.416822265625" Y="-0.179678131104" />
                  <Point X="-21.377734375" Y="-0.229486312866" />
                  <Point X="-21.363421875" Y="-0.241907058716" />
                  <Point X="-21.336060546875" Y="-0.257722900391" />
                  <Point X="-21.270912109375" Y="-0.295379577637" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.343296875" Y="-0.545704101563" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.01200390625" Y="-0.70398059082" />
                  <Point X="-20.051568359375" Y="-0.966412597656" />
                  <Point X="-20.076123046875" Y="-1.074012939453" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-21.14207421875" Y="-1.156337646484" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.658865234375" Y="-1.110013793945" />
                  <Point X="-21.7867265625" Y="-1.137805053711" />
                  <Point X="-21.814552734375" Y="-1.154697021484" />
                  <Point X="-21.84701171875" Y="-1.193735961914" />
                  <Point X="-21.924296875" Y="-1.286685058594" />
                  <Point X="-21.935640625" Y="-1.314070800781" />
                  <Point X="-21.94029296875" Y="-1.364627807617" />
                  <Point X="-21.951369140625" Y="-1.485001220703" />
                  <Point X="-21.943638671875" Y="-1.516622192383" />
                  <Point X="-21.913919921875" Y="-1.562849121094" />
                  <Point X="-21.843158203125" Y="-1.672912719727" />
                  <Point X="-21.831537109375" Y="-1.685540771484" />
                  <Point X="-20.981916015625" Y="-2.337479248047" />
                  <Point X="-20.66092578125" Y="-2.583784179688" />
                  <Point X="-20.68433984375" Y="-2.621673339844" />
                  <Point X="-20.795865234375" Y="-2.802138183594" />
                  <Point X="-20.846654296875" Y="-2.874302246094" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.849373046875" Y="-2.488523193359" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.326572265625" Y="-2.241770019531" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.56401953125" Y="-2.247189453125" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.33468359375" />
                  <Point X="-22.73934375" Y="-2.387780761719" />
                  <Point X="-22.80587890625" Y="-2.514201660156" />
                  <Point X="-22.8108359375" Y="-2.546375" />
                  <Point X="-22.79929296875" Y="-2.610289306641" />
                  <Point X="-22.77180859375" Y="-2.762465332031" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.219939453125" Y="-3.72422265625" />
                  <Point X="-22.01332421875" Y="-4.082087646484" />
                  <Point X="-22.032365234375" Y="-4.0956875" />
                  <Point X="-22.16470703125" Y="-4.190216308594" />
                  <Point X="-22.221482421875" Y="-4.226966308594" />
                  <Point X="-22.32022265625" Y="-4.29087890625" />
                  <Point X="-23.0137421875" Y="-3.387067626953" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.392486328125" Y="-2.939940429688" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.64313671875" Y="-2.842061035156" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.887900390625" Y="-2.912771972656" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045985839844" />
                  <Point X="-24.047458984375" Y="-3.119216308594" />
                  <Point X="-24.085357421875" Y="-3.293573730469" />
                  <Point X="-24.0860703125" Y="-3.310719970703" />
                  <Point X="-23.931345703125" Y="-4.485958984375" />
                  <Point X="-23.872357421875" Y="-4.934028808594" />
                  <Point X="-23.880451171875" Y="-4.935803222656" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.058111328125" Y="-4.972776855469" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#146" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.057157025171" Y="4.568437406406" Z="0.75" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.75" />
                  <Point X="-0.750088547893" Y="5.011152576731" Z="0.75" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.75" />
                  <Point X="-1.523793560681" Y="4.832431607231" Z="0.75" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.75" />
                  <Point X="-1.73784097788" Y="4.672535231898" Z="0.75" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.75" />
                  <Point X="-1.730377730839" Y="4.37108455871" Z="0.75" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.75" />
                  <Point X="-1.809765829264" Y="4.311874898676" Z="0.75" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.75" />
                  <Point X="-1.906152589708" Y="4.334630690897" Z="0.75" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.75" />
                  <Point X="-1.993462792253" Y="4.426373997216" Z="0.75" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.75" />
                  <Point X="-2.593613933646" Y="4.354712856332" Z="0.75" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.75" />
                  <Point X="-3.203529206774" Y="3.927824396627" Z="0.75" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.75" />
                  <Point X="-3.267119100831" Y="3.600335902768" Z="0.75" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.75" />
                  <Point X="-2.996253796399" Y="3.08006696677" Z="0.75" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.75" />
                  <Point X="-3.036803013417" Y="3.01200055447" Z="0.75" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.75" />
                  <Point X="-3.115009498836" Y="2.999310902774" Z="0.75" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.75" />
                  <Point X="-3.333523445529" Y="3.113074847915" Z="0.75" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.75" />
                  <Point X="-4.085186200069" Y="3.003807424264" Z="0.75" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.75" />
                  <Point X="-4.447096751145" Y="2.436178715395" Z="0.75" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.75" />
                  <Point X="-4.295921950127" Y="2.070738912664" Z="0.75" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.75" />
                  <Point X="-3.675618757288" Y="1.57060196404" Z="0.75" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.75" />
                  <Point X="-3.684179786388" Y="1.511800021463" Z="0.75" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.75" />
                  <Point X="-3.734727710654" Y="1.480560991" Z="0.75" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.75" />
                  <Point X="-4.067482941519" Y="1.516248694585" Z="0.75" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.75" />
                  <Point X="-4.926590731134" Y="1.20857446535" Z="0.75" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.75" />
                  <Point X="-5.034447788596" Y="0.621532624939" Z="0.75" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.75" />
                  <Point X="-4.621465187731" Y="0.329050340468" Z="0.75" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.75" />
                  <Point X="-3.557016482916" Y="0.035504204533" Z="0.75" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.75" />
                  <Point X="-3.542292996558" Y="0.008816168685" Z="0.75" />
                  <Point X="-3.539556741714" Y="0" Z="0.75" />
                  <Point X="-3.546071604577" Y="-0.020990782376" Z="0.75" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.75" />
                  <Point X="-3.568352112208" Y="-0.043371778601" Z="0.75" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.75" />
                  <Point X="-4.015422601042" Y="-0.166661716288" Z="0.75" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.75" />
                  <Point X="-5.005633232979" Y="-0.829056445876" Z="0.75" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.75" />
                  <Point X="-4.887057345651" Y="-1.363958390155" Z="0.75" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.75" />
                  <Point X="-4.365456245006" Y="-1.45777618905" Z="0.75" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.75" />
                  <Point X="-3.200509207246" Y="-1.317839698402" Z="0.75" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.75" />
                  <Point X="-3.198102000153" Y="-1.343398625112" Z="0.75" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.75" />
                  <Point X="-3.585634370471" Y="-1.647812617964" Z="0.75" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.75" />
                  <Point X="-4.296178538336" Y="-2.698297064641" Z="0.75" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.75" />
                  <Point X="-3.964907323479" Y="-3.16504085023" Z="0.75" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.75" />
                  <Point X="-3.480866013566" Y="-3.079740316224" Z="0.75" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.75" />
                  <Point X="-2.560622241776" Y="-2.567708227356" Z="0.75" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.75" />
                  <Point X="-2.775676696944" Y="-2.954212368926" Z="0.75" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.75" />
                  <Point X="-3.011580985976" Y="-4.084254973649" Z="0.75" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.75" />
                  <Point X="-2.581069056139" Y="-4.369078949383" Z="0.75" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.75" />
                  <Point X="-2.384599160376" Y="-4.362852880104" Z="0.75" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.75" />
                  <Point X="-2.044556126652" Y="-4.035066559667" Z="0.75" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.75" />
                  <Point X="-1.750235763459" Y="-3.998374184343" Z="0.75" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.75" />
                  <Point X="-1.494398851762" Y="-4.148435298168" Z="0.75" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.75" />
                  <Point X="-1.382781075999" Y="-4.423230165082" Z="0.75" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.75" />
                  <Point X="-1.37914098686" Y="-4.621566259706" Z="0.75" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.75" />
                  <Point X="-1.204861857182" Y="-4.933080869803" Z="0.75" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.75" />
                  <Point X="-0.906322010932" Y="-4.996554910603" Z="0.75" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.75" />
                  <Point X="-0.699185907906" Y="-4.571581432015" Z="0.75" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.75" />
                  <Point X="-0.301785737454" Y="-3.352646258629" Z="0.75" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.75" />
                  <Point X="-0.074937432443" Y="-3.22749718202" Z="0.75" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.75" />
                  <Point X="0.178421646918" Y="-3.259614863269" Z="0.75" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.75" />
                  <Point X="0.368660121978" Y="-3.448999497139" Z="0.75" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.75" />
                  <Point X="0.535568952495" Y="-3.960954597801" Z="0.75" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.75" />
                  <Point X="0.944669373056" Y="-4.990691451087" Z="0.75" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.75" />
                  <Point X="1.124096479223" Y="-4.953363328145" Z="0.75" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.75" />
                  <Point X="1.112068949563" Y="-4.448152677499" Z="0.75" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.75" />
                  <Point X="0.995243079468" Y="-3.09855640717" Z="0.75" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.75" />
                  <Point X="1.137908954812" Y="-2.91993842732" Z="0.75" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.75" />
                  <Point X="1.355289097436" Y="-2.86057063576" Z="0.75" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.75" />
                  <Point X="1.574317184604" Y="-2.950718488005" Z="0.75" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.75" />
                  <Point X="1.940433011949" Y="-3.386225581639" Z="0.75" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.75" />
                  <Point X="2.799530223853" Y="-4.237660165037" Z="0.75" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.75" />
                  <Point X="2.990540318512" Y="-4.105094675021" Z="0.75" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.75" />
                  <Point X="2.817204970171" Y="-3.667942669256" Z="0.75" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.75" />
                  <Point X="2.243753987322" Y="-2.570122460065" Z="0.75" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.75" />
                  <Point X="2.298745589703" Y="-2.379787235635" Z="0.75" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.75" />
                  <Point X="2.453111142738" Y="-2.260155720264" Z="0.75" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.75" />
                  <Point X="2.658384343448" Y="-2.259694022169" Z="0.75" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.75" />
                  <Point X="3.11947092785" Y="-2.500544530103" Z="0.75" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.75" />
                  <Point X="4.188077680673" Y="-2.871799536062" Z="0.75" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.75" />
                  <Point X="4.352036392246" Y="-2.616678512558" Z="0.75" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.75" />
                  <Point X="4.042365429188" Y="-2.266531470456" Z="0.75" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.75" />
                  <Point X="3.121982132346" Y="-1.504529422708" Z="0.75" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.75" />
                  <Point X="3.103339217293" Y="-1.337929232723" Z="0.75" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.75" />
                  <Point X="3.185275910766" Y="-1.194423026522" Z="0.75" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.75" />
                  <Point X="3.345597561955" Y="-1.127592841103" Z="0.75" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.75" />
                  <Point X="3.845242759665" Y="-1.174629916552" Z="0.75" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.75" />
                  <Point X="4.966465567826" Y="-1.053857090953" Z="0.75" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.75" />
                  <Point X="5.031281102016" Y="-0.680154485062" Z="0.75" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.75" />
                  <Point X="4.663488382447" Y="-0.466127672458" Z="0.75" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.75" />
                  <Point X="3.682804910538" Y="-0.183154047019" Z="0.75" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.75" />
                  <Point X="3.616354041217" Y="-0.117530051707" Z="0.75" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.75" />
                  <Point X="3.586907165884" Y="-0.028575037378" Z="0.75" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.75" />
                  <Point X="3.59446428454" Y="0.068035493849" Z="0.75" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.75" />
                  <Point X="3.639025397183" Y="0.146418686868" Z="0.75" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.75" />
                  <Point X="3.720590503815" Y="0.204994797223" Z="0.75" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.75" />
                  <Point X="4.132479434859" Y="0.323844260311" Z="0.75" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.75" />
                  <Point X="5.001605156735" Y="0.867244898828" Z="0.75" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.75" />
                  <Point X="4.910755642967" Y="1.285554674672" Z="0.75" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.75" />
                  <Point X="4.461475037049" Y="1.35345987152" Z="0.75" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.75" />
                  <Point X="3.396811084201" Y="1.230787801038" Z="0.75" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.75" />
                  <Point X="3.319916975807" Y="1.26207590252" Z="0.75" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.75" />
                  <Point X="3.265475248129" Y="1.325111365312" Z="0.75" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.75" />
                  <Point X="3.238818081276" Y="1.407021112334" Z="0.75" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.75" />
                  <Point X="3.248749726018" Y="1.486549482523" Z="0.75" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.75" />
                  <Point X="3.295807602382" Y="1.562399122881" Z="0.75" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.75" />
                  <Point X="3.648430051777" Y="1.842157768558" Z="0.75" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.75" />
                  <Point X="4.300038905153" Y="2.698531471609" Z="0.75" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.75" />
                  <Point X="4.072041372313" Y="3.031647922369" Z="0.75" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.75" />
                  <Point X="3.560850620557" Y="2.873778104732" Z="0.75" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.75" />
                  <Point X="2.453338373359" Y="2.251879356463" Z="0.75" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.75" />
                  <Point X="2.380700969631" Y="2.251424485995" Z="0.75" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.75" />
                  <Point X="2.31558324537" Y="2.284152279049" Z="0.75" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.75" />
                  <Point X="2.266606321186" Y="2.341441615012" Z="0.75" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.75" />
                  <Point X="2.248005216763" Y="2.409057474201" Z="0.75" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.75" />
                  <Point X="2.260648586195" Y="2.486131202523" Z="0.75" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.75" />
                  <Point X="2.521847248128" Y="2.951288461398" Z="0.75" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.75" />
                  <Point X="2.864451767028" Y="4.190127697661" Z="0.75" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.75" />
                  <Point X="2.473403061537" Y="4.432215867395" Z="0.75" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.75" />
                  <Point X="2.065811362486" Y="4.636353955108" Z="0.75" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.75" />
                  <Point X="1.643120616304" Y="4.802448951991" Z="0.75" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.75" />
                  <Point X="1.056048821415" Y="4.959513481082" Z="0.75" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.75" />
                  <Point X="0.391211338109" Y="5.055590623635" Z="0.75" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.75" />
                  <Point X="0.136087536382" Y="4.863010099545" Z="0.75" />
                  <Point X="0" Y="4.355124473572" Z="0.75" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>