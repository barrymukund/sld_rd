<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#217" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3620" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000475585938" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.4366953125" Y="-3.512524658203" />
                  <Point X="-24.442275390625" Y="-3.497142822266" />
                  <Point X="-24.4576328125" Y="-3.467379882812" />
                  <Point X="-24.618865234375" Y="-3.235076416016" />
                  <Point X="-24.621361328125" Y="-3.231479492188" />
                  <Point X="-24.6432421875" Y="-3.209026123047" />
                  <Point X="-24.6695" Y="-3.189779052734" />
                  <Point X="-24.69750390625" Y="-3.175668945312" />
                  <Point X="-24.947" Y="-3.098234619141" />
                  <Point X="-24.95086328125" Y="-3.097035644531" />
                  <Point X="-24.97902734375" Y="-3.092766357422" />
                  <Point X="-25.008669921875" Y="-3.092767333984" />
                  <Point X="-25.036826171875" Y="-3.097036621094" />
                  <Point X="-25.286322265625" Y="-3.174470703125" />
                  <Point X="-25.290185546875" Y="-3.175669921875" />
                  <Point X="-25.31819140625" Y="-3.189782470703" />
                  <Point X="-25.344447265625" Y="-3.209029785156" />
                  <Point X="-25.366326171875" Y="-3.231480957031" />
                  <Point X="-25.527556640625" Y="-3.463784179688" />
                  <Point X="-25.534244140625" Y="-3.474991455078" />
                  <Point X="-25.543958984375" Y="-3.494152587891" />
                  <Point X="-25.550990234375" Y="-3.512526611328" />
                  <Point X="-25.55709765625" Y="-3.535322021484" />
                  <Point X="-25.916583984375" Y="-4.876941894531" />
                  <Point X="-26.07904296875" Y="-4.845408203125" />
                  <Point X="-26.079330078125" Y="-4.845352539063" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.2149609375" Y="-4.563438476562" />
                  <Point X="-26.21419921875" Y="-4.547929199219" />
                  <Point X="-26.2165078125" Y="-4.516223632812" />
                  <Point X="-26.27611328125" Y="-4.216571777344" />
                  <Point X="-26.27704296875" Y="-4.211902832031" />
                  <Point X="-26.2879453125" Y="-4.182947753906" />
                  <Point X="-26.304015625" Y="-4.155120117188" />
                  <Point X="-26.32364453125" Y="-4.131204101562" />
                  <Point X="-26.55334765625" Y="-3.929758789062" />
                  <Point X="-26.556904296875" Y="-3.926639648438" />
                  <Point X="-26.5831875" Y="-3.910295898438" />
                  <Point X="-26.612884765625" Y="-3.897994628906" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.947896484375" Y="-3.870984130859" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708496094" />
                  <Point X="-27.014466796875" Y="-3.882028076172" />
                  <Point X="-27.042658203125" Y="-3.894801513672" />
                  <Point X="-27.29669140625" Y="-4.064541259766" />
                  <Point X="-27.29670703125" Y="-4.064552246094" />
                  <Point X="-27.315654296875" Y="-4.079621582031" />
                  <Point X="-27.330962890625" Y="-4.095219238281" />
                  <Point X="-27.33853125" Y="-4.103930664062" />
                  <Point X="-27.4801484375" Y="-4.288489257813" />
                  <Point X="-27.80128515625" Y="-4.089648925781" />
                  <Point X="-27.801708984375" Y="-4.089386962891" />
                  <Point X="-28.104720703125" Y="-3.856076904297" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647654541016" />
                  <Point X="-27.406587890625" Y="-2.616128173828" />
                  <Point X="-27.40557421875" Y="-2.585194091797" />
                  <Point X="-27.41455859375" Y="-2.555575927734" />
                  <Point X="-27.428775390625" Y="-2.526747070312" />
                  <Point X="-27.446802734375" Y="-2.501589355469" />
                  <Point X="-27.463845703125" Y="-2.484546386719" />
                  <Point X="-27.464044921875" Y="-2.484346923828" />
                  <Point X="-27.489125" Y="-2.466336669922" />
                  <Point X="-27.51797265625" Y="-2.452060546875" />
                  <Point X="-27.54762109375" Y="-2.443029052734" />
                  <Point X="-27.578599609375" Y="-2.444021728516" />
                  <Point X="-27.610171875" Y="-2.450286621094" />
                  <Point X="-27.6391796875" Y="-2.461195800781" />
                  <Point X="-27.658841796875" Y="-2.472546875" />
                  <Point X="-28.8180234375" Y="-3.141801025391" />
                  <Point X="-29.082525390625" Y="-2.794298828125" />
                  <Point X="-29.08286328125" Y="-2.793854003906" />
                  <Point X="-29.306142578125" Y="-2.419450439453" />
                  <Point X="-28.105955078125" Y="-1.498513305664" />
                  <Point X="-28.084578125" Y="-1.475594848633" />
                  <Point X="-28.06661328125" Y="-1.448465087891" />
                  <Point X="-28.053857421875" Y="-1.419836425781" />
                  <Point X="-28.04626953125" Y="-1.390542480469" />
                  <Point X="-28.0461171875" Y="-1.389952148438" />
                  <Point X="-28.043341796875" Y="-1.35958190918" />
                  <Point X="-28.045564453125" Y="-1.327927490234" />
                  <Point X="-28.05257421875" Y="-1.298199951172" />
                  <Point X="-28.068654296875" Y="-1.272233032227" />
                  <Point X="-28.08948046875" Y="-1.248290771484" />
                  <Point X="-28.112970703125" Y="-1.228768066406" />
                  <Point X="-28.139033203125" Y="-1.213428710938" />
                  <Point X="-28.139421875" Y="-1.21319921875" />
                  <Point X="-28.168763671875" Y="-1.201938354492" />
                  <Point X="-28.20062890625" Y="-1.195469848633" />
                  <Point X="-28.231927734375" Y="-1.194383789062" />
                  <Point X="-28.256748046875" Y="-1.197651367188" />
                  <Point X="-29.732099609375" Y="-1.391885253906" />
                  <Point X="-29.8339453125" Y="-0.993169555664" />
                  <Point X="-29.834078125" Y="-0.992652648926" />
                  <Point X="-29.892423828125" Y="-0.584698669434" />
                  <Point X="-28.532875" Y="-0.220408309937" />
                  <Point X="-28.517494140625" Y="-0.214828353882" />
                  <Point X="-28.487728515625" Y="-0.199469894409" />
                  <Point X="-28.4603984375" Y="-0.180501403809" />
                  <Point X="-28.45988671875" Y="-0.180145614624" />
                  <Point X="-28.437572265625" Y="-0.158374160767" />
                  <Point X="-28.418296875" Y="-0.132097732544" />
                  <Point X="-28.404166015625" Y="-0.104064933777" />
                  <Point X="-28.395056640625" Y="-0.074712463379" />
                  <Point X="-28.394833984375" Y="-0.073988578796" />
                  <Point X="-28.390634765625" Y="-0.046021652222" />
                  <Point X="-28.390646484375" Y="-0.016422374725" />
                  <Point X="-28.394916015625" Y="0.011698008537" />
                  <Point X="-28.404025390625" Y="0.041050323486" />
                  <Point X="-28.404185546875" Y="0.041563831329" />
                  <Point X="-28.418236328125" Y="0.069431480408" />
                  <Point X="-28.4374921875" Y="0.095728408813" />
                  <Point X="-28.459974609375" Y="0.117647544861" />
                  <Point X="-28.4873046875" Y="0.136616027832" />
                  <Point X="-28.501072265625" Y="0.14455406189" />
                  <Point X="-28.532873046875" Y="0.157847717285" />
                  <Point X="-28.55549609375" Y="0.163910064697" />
                  <Point X="-29.891814453125" Y="0.521975646973" />
                  <Point X="-29.8245703125" Y="0.976414245605" />
                  <Point X="-29.82448828125" Y="0.97696472168" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-28.7656640625" Y="1.29979284668" />
                  <Point X="-28.744982421875" Y="1.299341796875" />
                  <Point X="-28.723421875" Y="1.301228271484" />
                  <Point X="-28.703134765625" Y="1.305263793945" />
                  <Point X="-28.64266015625" Y="1.324331542969" />
                  <Point X="-28.64176171875" Y="1.324614868164" />
                  <Point X="-28.622806640625" Y="1.332947998047" />
                  <Point X="-28.6040546875" Y="1.34376940918" />
                  <Point X="-28.5873671875" Y="1.356001342773" />
                  <Point X="-28.57372265625" Y="1.371555908203" />
                  <Point X="-28.561302734375" Y="1.389290649414" />
                  <Point X="-28.551349609375" Y="1.407431274414" />
                  <Point X="-28.527078125" Y="1.466028076172" />
                  <Point X="-28.526681640625" Y="1.466989746094" />
                  <Point X="-28.520890625" Y="1.486886230469" />
                  <Point X="-28.5171484375" Y="1.508184204102" />
                  <Point X="-28.515806640625" Y="1.528803466797" />
                  <Point X="-28.518955078125" Y="1.549225097656" />
                  <Point X="-28.5245546875" Y="1.570111450195" />
                  <Point X="-28.532046875" Y="1.589375610352" />
                  <Point X="-28.56133203125" Y="1.645634277344" />
                  <Point X="-28.56192578125" Y="1.646768310547" />
                  <Point X="-28.573359375" Y="1.663823852539" />
                  <Point X="-28.58723828125" Y="1.68033972168" />
                  <Point X="-28.6021328125" Y="1.694588378906" />
                  <Point X="-28.615109375" Y="1.704546264648" />
                  <Point X="-29.351859375" Y="2.269874511719" />
                  <Point X="-29.081478515625" Y="2.733103515625" />
                  <Point X="-29.08115625" Y="2.733654052734" />
                  <Point X="-28.750505859375" Y="3.158661865234" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.187724609375" Y="2.836340576172" />
                  <Point X="-28.167083984375" Y="2.829832275391" />
                  <Point X="-28.146794921875" Y="2.825796630859" />
                  <Point X="-28.062548828125" Y="2.818426025391" />
                  <Point X="-28.06124609375" Y="2.818312011719" />
                  <Point X="-28.040568359375" Y="2.818762939453" />
                  <Point X="-28.019109375" Y="2.821587646484" />
                  <Point X="-27.999017578125" Y="2.826503662109" />
                  <Point X="-27.980466796875" Y="2.835651611328" />
                  <Point X="-27.9622109375" Y="2.84728125" />
                  <Point X="-27.946078125" Y="2.860229003906" />
                  <Point X="-27.886279296875" Y="2.920026611328" />
                  <Point X="-27.885353515625" Y="2.920952392578" />
                  <Point X="-27.872404296875" Y="2.9370859375" />
                  <Point X="-27.860775390625" Y="2.955340332031" />
                  <Point X="-27.85162890625" Y="2.973889160156" />
                  <Point X="-27.8467109375" Y="2.993978027344" />
                  <Point X="-27.843884765625" Y="3.015437011719" />
                  <Point X="-27.84343359375" Y="3.036122070312" />
                  <Point X="-27.8508046875" Y="3.1203671875" />
                  <Point X="-27.850916015625" Y="3.121639404297" />
                  <Point X="-27.854943359375" Y="3.141898925781" />
                  <Point X="-27.861455078125" Y="3.162572998047" />
                  <Point X="-27.869798828125" Y="3.181541015625" />
                  <Point X="-27.87555078125" Y="3.191501464844" />
                  <Point X="-28.18333203125" Y="3.724595703125" />
                  <Point X="-27.701173828125" Y="4.094260986328" />
                  <Point X="-27.70061328125" Y="4.094690429687" />
                  <Point X="-27.16703515625" Y="4.391133300781" />
                  <Point X="-27.0431953125" Y="4.229741210938" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.99511328125" Y="4.189395019531" />
                  <Point X="-26.901349609375" Y="4.140583984375" />
                  <Point X="-26.899865234375" Y="4.139811523438" />
                  <Point X="-26.880580078125" Y="4.132317382813" />
                  <Point X="-26.8596796875" Y="4.12672265625" />
                  <Point X="-26.839244140625" Y="4.123581542969" />
                  <Point X="-26.818611328125" Y="4.124936035156" />
                  <Point X="-26.797302734375" Y="4.128694824219" />
                  <Point X="-26.777451171875" Y="4.134481933594" />
                  <Point X="-26.679830078125" Y="4.174918457031" />
                  <Point X="-26.67831640625" Y="4.175544921875" />
                  <Point X="-26.660173828125" Y="4.185492675781" />
                  <Point X="-26.642435546875" Y="4.197907226562" />
                  <Point X="-26.626875" Y="4.211548828125" />
                  <Point X="-26.61463671875" Y="4.228235839844" />
                  <Point X="-26.603810546875" Y="4.246985839844" />
                  <Point X="-26.595478515625" Y="4.265921386719" />
                  <Point X="-26.56369140625" Y="4.366736816406" />
                  <Point X="-26.56319921875" Y="4.368297851562" />
                  <Point X="-26.5591640625" Y="4.388583984375" />
                  <Point X="-26.55727734375" Y="4.410145507812" />
                  <Point X="-26.55773046875" Y="4.430835449219" />
                  <Point X="-26.558384765625" Y="4.435801757812" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-25.9503515625" Y="4.809606933594" />
                  <Point X="-25.9496328125" Y="4.80980859375" />
                  <Point X="-25.2947109375" Y="4.886457519531" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.121763671875" Y="4.256521484375" />
                  <Point X="-25.109662109375" Y="4.236764160156" />
                  <Point X="-25.093138671875" Y="4.2205234375" />
                  <Point X="-25.067947265625" Y="4.201192871094" />
                  <Point X="-25.040650390625" Y="4.186602539062" />
                  <Point X="-25.01011328125" Y="4.181560546875" />
                  <Point X="-24.97757421875" Y="4.181560546875" />
                  <Point X="-24.947037109375" Y="4.186602539062" />
                  <Point X="-24.919740234375" Y="4.201192871094" />
                  <Point X="-24.894548828125" Y="4.2205234375" />
                  <Point X="-24.8760703125" Y="4.239307617188" />
                  <Point X="-24.86346484375" Y="4.2624453125" />
                  <Point X="-24.853681640625" Y="4.288454589844" />
                  <Point X="-24.8508359375" Y="4.2973125" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.15659375" Y="4.831805175781" />
                  <Point X="-24.155958984375" Y="4.831738769531" />
                  <Point X="-23.521109375" Y="4.678466796875" />
                  <Point X="-23.518974609375" Y="4.677951171875" />
                  <Point X="-23.10576953125" Y="4.528079101562" />
                  <Point X="-23.1053515625" Y="4.527927246094" />
                  <Point X="-22.70578125" Y="4.341061523438" />
                  <Point X="-22.705421875" Y="4.340893066406" />
                  <Point X="-22.31940234375" Y="4.115997558594" />
                  <Point X="-22.3190234375" Y="4.115776855469" />
                  <Point X="-22.05673828125" Y="3.929254394531" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857919921875" Y="2.539934326172" />
                  <Point X="-22.866921875" Y="2.516057128906" />
                  <Point X="-22.888064453125" Y="2.436995117188" />
                  <Point X="-22.89096484375" Y="2.420289550781" />
                  <Point X="-22.892630859375" Y="2.400162109375" />
                  <Point X="-22.892271484375" Y="2.380951904297" />
                  <Point X="-22.8840390625" Y="2.3126796875" />
                  <Point X="-22.883912109375" Y="2.31162109375" />
                  <Point X="-22.878572265625" Y="2.289666992188" />
                  <Point X="-22.870302734375" Y="2.267546386719" />
                  <Point X="-22.8599296875" Y="2.247471191406" />
                  <Point X="-22.817626953125" Y="2.185127929688" />
                  <Point X="-22.806919921875" Y="2.172031494141" />
                  <Point X="-22.778400390625" Y="2.145593505859" />
                  <Point X="-22.71605859375" Y="2.103290771484" />
                  <Point X="-22.715033203125" Y="2.102595214844" />
                  <Point X="-22.694974609375" Y="2.092234130859" />
                  <Point X="-22.672923828125" Y="2.083992675781" />
                  <Point X="-22.651037109375" Y="2.078663818359" />
                  <Point X="-22.582671875" Y="2.070419921875" />
                  <Point X="-22.5655546875" Y="2.069910400391" />
                  <Point X="-22.526796875" Y="2.074170410156" />
                  <Point X="-22.447734375" Y="2.095312744141" />
                  <Point X="-22.436962890625" Y="2.098894775391" />
                  <Point X="-22.41146484375" Y="2.110145263672" />
                  <Point X="-22.388708984375" Y="2.123283447266" />
                  <Point X="-21.032671875" Y="2.90619140625" />
                  <Point X="-20.876953125" Y="2.689776611328" />
                  <Point X="-20.8767265625" Y="2.689462402344" />
                  <Point X="-20.73780078125" Y="2.4598828125" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.7785703125" Y="1.660244628906" />
                  <Point X="-21.79602734375" Y="1.641627075195" />
                  <Point X="-21.852927734375" Y="1.567395141602" />
                  <Point X="-21.861935546875" Y="1.553197875977" />
                  <Point X="-21.87128515625" Y="1.535096557617" />
                  <Point X="-21.878369140625" Y="1.517086181641" />
                  <Point X="-21.899544921875" Y="1.441367675781" />
                  <Point X="-21.899833984375" Y="1.440336791992" />
                  <Point X="-21.903326171875" Y="1.417956420898" />
                  <Point X="-21.90416015625" Y="1.394309082031" />
                  <Point X="-21.9022578125" Y="1.371762451172" />
                  <Point X="-21.884857421875" Y="1.287435302734" />
                  <Point X="-21.880033203125" Y="1.271377685547" />
                  <Point X="-21.872568359375" Y="1.252699462891" />
                  <Point X="-21.86371875" Y="1.235741210938" />
                  <Point X="-21.816400390625" Y="1.163818359375" />
                  <Point X="-21.8156796875" Y="1.162724975586" />
                  <Point X="-21.801119140625" Y="1.145465698242" />
                  <Point X="-21.783869140625" Y="1.129366943359" />
                  <Point X="-21.765654296875" Y="1.116034912109" />
                  <Point X="-21.69707421875" Y="1.077430175781" />
                  <Point X="-21.681544921875" Y="1.07043737793" />
                  <Point X="-21.66251953125" Y="1.063849121094" />
                  <Point X="-21.643880859375" Y="1.059438476562" />
                  <Point X="-21.55115625" Y="1.047183837891" />
                  <Point X="-21.53997265625" Y="1.046373291016" />
                  <Point X="-21.511794921875" Y="1.046984619141" />
                  <Point X="-21.4901796875" Y="1.049830566406" />
                  <Point X="-20.22316015625" Y="1.21663659668" />
                  <Point X="-20.154162109375" Y="0.933210449219" />
                  <Point X="-20.154060546875" Y="0.932794250488" />
                  <Point X="-20.1091328125" Y="0.644238586426" />
                  <Point X="-21.283419921875" Y="0.329589874268" />
                  <Point X="-21.2952109375" Y="0.325585662842" />
                  <Point X="-21.318453125" Y="0.315068084717" />
                  <Point X="-21.409552734375" Y="0.262410949707" />
                  <Point X="-21.42319140625" Y="0.252839508057" />
                  <Point X="-21.4389140625" Y="0.239603988647" />
                  <Point X="-21.45246875" Y="0.225577194214" />
                  <Point X="-21.50712890625" Y="0.155928024292" />
                  <Point X="-21.50801171875" Y="0.154800674438" />
                  <Point X="-21.51971875" Y="0.135536651611" />
                  <Point X="-21.52948046875" Y="0.11408821106" />
                  <Point X="-21.536318359375" Y="0.092605171204" />
                  <Point X="-21.5545390625" Y="-0.002532188416" />
                  <Point X="-21.556232421875" Y="-0.019138664246" />
                  <Point X="-21.5565078125" Y="-0.039393573761" />
                  <Point X="-21.5548203125" Y="-0.05855437851" />
                  <Point X="-21.53660546875" Y="-0.153663345337" />
                  <Point X="-21.536333984375" Y="-0.155082138062" />
                  <Point X="-21.52948828125" Y="-0.176612686157" />
                  <Point X="-21.519708984375" Y="-0.198104690552" />
                  <Point X="-21.507974609375" Y="-0.217409698486" />
                  <Point X="-21.453326171875" Y="-0.28704385376" />
                  <Point X="-21.441783203125" Y="-0.299329589844" />
                  <Point X="-21.42661328125" Y="-0.312843048096" />
                  <Point X="-21.410962890625" Y="-0.324155670166" />
                  <Point X="-21.31986328125" Y="-0.376812683105" />
                  <Point X="-21.309970703125" Y="-0.381785797119" />
                  <Point X="-21.283419921875" Y="-0.392150024414" />
                  <Point X="-21.263595703125" Y="-0.397461791992" />
                  <Point X="-20.10852734375" Y="-0.706961853027" />
                  <Point X="-20.144921875" Y="-0.948363769531" />
                  <Point X="-20.1449765625" Y="-0.948724060059" />
                  <Point X="-20.19882421875" Y="-1.18469909668" />
                  <Point X="-21.5756171875" Y="-1.003441040039" />
                  <Point X="-21.591966796875" Y="-1.002710449219" />
                  <Point X="-21.62534375" Y="-1.005509338379" />
                  <Point X="-21.804140625" Y="-1.044371337891" />
                  <Point X="-21.806908203125" Y="-1.044973022461" />
                  <Point X="-21.83603125" Y="-1.056598999023" />
                  <Point X="-21.86385546875" Y="-1.073490844727" />
                  <Point X="-21.887603515625" Y="-1.093960449219" />
                  <Point X="-21.995673828125" Y="-1.223936035156" />
                  <Point X="-21.997341796875" Y="-1.225942016602" />
                  <Point X="-22.012060546875" Y="-1.250319702148" />
                  <Point X="-22.023408203125" Y="-1.277711669922" />
                  <Point X="-22.030240234375" Y="-1.305365722656" />
                  <Point X="-22.0457265625" Y="-1.473667602539" />
                  <Point X="-22.04596875" Y="-1.476273803711" />
                  <Point X="-22.04365234375" Y="-1.507548706055" />
                  <Point X="-22.035921875" Y="-1.539177368164" />
                  <Point X="-22.023548828125" Y="-1.567996337891" />
                  <Point X="-21.9246015625" Y="-1.721903686523" />
                  <Point X="-21.916140625" Y="-1.733139038086" />
                  <Point X="-21.889369140625" Y="-1.760909545898" />
                  <Point X="-21.87097265625" Y="-1.775025634766" />
                  <Point X="-20.786876953125" Y="-2.606882568359" />
                  <Point X="-20.875037109375" Y="-2.7495390625" />
                  <Point X="-20.8751953125" Y="-2.749795166016" />
                  <Point X="-20.971017578125" Y="-2.885944824219" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.213869140625" Y="-2.170011962891" />
                  <Point X="-22.245775390625" Y="-2.159824951172" />
                  <Point X="-22.4585703125" Y="-2.121394287109" />
                  <Point X="-22.461865234375" Y="-2.120799316406" />
                  <Point X="-22.49321875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353271484" />
                  <Point X="-22.555166015625" Y="-2.135176757812" />
                  <Point X="-22.731947265625" Y="-2.228215087891" />
                  <Point X="-22.734716796875" Y="-2.229673828125" />
                  <Point X="-22.757625" Y="-2.246557128906" />
                  <Point X="-22.778578125" Y="-2.267513183594" />
                  <Point X="-22.795466796875" Y="-2.290438964844" />
                  <Point X="-22.888505859375" Y="-2.467219726562" />
                  <Point X="-22.88994140625" Y="-2.469947265625" />
                  <Point X="-22.899763671875" Y="-2.499713134766" />
                  <Point X="-22.9047265625" Y="-2.531895996094" />
                  <Point X="-22.90432421875" Y="-2.563259033203" />
                  <Point X="-22.865892578125" Y="-2.776054443359" />
                  <Point X="-22.862490234375" Y="-2.789331054688" />
                  <Point X="-22.8559921875" Y="-2.808739501953" />
                  <Point X="-22.8481796875" Y="-2.826075195312" />
                  <Point X="-22.836359375" Y="-2.846550537109" />
                  <Point X="-22.138712890625" Y="-4.054905273438" />
                  <Point X="-22.217962890625" Y="-4.111510742188" />
                  <Point X="-22.21814453125" Y="-4.111640625" />
                  <Point X="-22.298234375" Y="-4.163481933594" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.252494140625" Y="-2.922179443359" />
                  <Point X="-23.27807421875" Y="-2.900556640625" />
                  <Point X="-23.48794921875" Y="-2.765627197266" />
                  <Point X="-23.49119921875" Y="-2.763537841797" />
                  <Point X="-23.52001171875" Y="-2.751166015625" />
                  <Point X="-23.5516328125" Y="-2.743435058594" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.812431640625" Y="-2.762238525391" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.843638671875" Y="-2.769398193359" />
                  <Point X="-23.8710234375" Y="-2.780741943359" />
                  <Point X="-23.89540234375" Y="-2.795461669922" />
                  <Point X="-24.072642578125" Y="-2.942830322266" />
                  <Point X="-24.07538671875" Y="-2.945112060547" />
                  <Point X="-24.095857421875" Y="-2.968861572266" />
                  <Point X="-24.11275" Y="-2.996687988281" />
                  <Point X="-24.124375" Y="-3.025809326172" />
                  <Point X="-24.17736328125" Y="-3.269601074219" />
                  <Point X="-24.179275390625" Y="-3.282815429688" />
                  <Point X="-24.180814453125" Y="-3.303757324219" />
                  <Point X="-24.1802578125" Y="-3.323125488281" />
                  <Point X="-24.17690625" Y="-3.348572021484" />
                  <Point X="-23.977935546875" Y="-4.859915039062" />
                  <Point X="-24.02415234375" Y="-4.870045898438" />
                  <Point X="-24.02433984375" Y="-4.870086914062" />
                  <Point X="-24.07068359375" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.058423828125" Y="-4.752637695312" />
                  <Point X="-26.141244140625" Y="-4.731328613281" />
                  <Point X="-26.1207734375" Y="-4.575838378906" />
                  <Point X="-26.120076171875" Y="-4.568098632812" />
                  <Point X="-26.11944921875" Y="-4.541030273438" />
                  <Point X="-26.1217578125" Y="-4.509324707031" />
                  <Point X="-26.123333984375" Y="-4.497689941406" />
                  <Point X="-26.182939453125" Y="-4.198038085938" />
                  <Point X="-26.182943359375" Y="-4.19801953125" />
                  <Point X="-26.18813671875" Y="-4.178427246094" />
                  <Point X="-26.1990390625" Y="-4.149472167969" />
                  <Point X="-26.205677734375" Y="-4.135438964844" />
                  <Point X="-26.221748046875" Y="-4.107611328125" />
                  <Point X="-26.23058203125" Y="-4.094849853516" />
                  <Point X="-26.2502109375" Y="-4.070933837891" />
                  <Point X="-26.261005859375" Y="-4.059779541016" />
                  <Point X="-26.490708984375" Y="-3.858334228516" />
                  <Point X="-26.50673828125" Y="-3.845965087891" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822527587891" />
                  <Point X="-26.576529296875" Y="-3.810226318359" />
                  <Point X="-26.5913125" Y="-3.805476318359" />
                  <Point X="-26.621455078125" Y="-3.798447998047" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.94168359375" Y="-3.7761875" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166015625" />
                  <Point X="-27.008005859375" Y="-3.781945556641" />
                  <Point X="-27.0390546875" Y="-3.790265136719" />
                  <Point X="-27.053673828125" Y="-3.79549609375" />
                  <Point X="-27.081865234375" Y="-3.80826953125" />
                  <Point X="-27.0954375" Y="-3.815812011719" />
                  <Point X="-27.349470703125" Y="-3.985551757812" />
                  <Point X="-27.355841796875" Y="-3.990200683594" />
                  <Point X="-27.3747890625" Y="-4.005270019531" />
                  <Point X="-27.383455078125" Y="-4.013077636719" />
                  <Point X="-27.398763671875" Y="-4.028675292969" />
                  <Point X="-27.413900390625" Y="-4.046098144531" />
                  <Point X="-27.503203125" Y="-4.162478515625" />
                  <Point X="-27.74758984375" Y="-4.011158691406" />
                  <Point X="-27.98086328125" Y="-3.831546386719" />
                  <Point X="-27.341490234375" Y="-2.724119628906" />
                  <Point X="-27.3348515625" Y="-2.710085205078" />
                  <Point X="-27.32394921875" Y="-2.681120117188" />
                  <Point X="-27.319685546875" Y="-2.666189453125" />
                  <Point X="-27.3134140625" Y="-2.634663085938" />
                  <Point X="-27.311638671875" Y="-2.619239501953" />
                  <Point X="-27.310625" Y="-2.588305419922" />
                  <Point X="-27.3146640625" Y="-2.557617675781" />
                  <Point X="-27.3236484375" Y="-2.527999511719" />
                  <Point X="-27.32935546875" Y="-2.51355859375" />
                  <Point X="-27.343572265625" Y="-2.484729736328" />
                  <Point X="-27.3515546875" Y="-2.471412597656" />
                  <Point X="-27.36958203125" Y="-2.446254882812" />
                  <Point X="-27.379626953125" Y="-2.434414306641" />
                  <Point X="-27.39662890625" Y="-2.417412353516" />
                  <Point X="-27.4086328125" Y="-2.407182128906" />
                  <Point X="-27.433712890625" Y="-2.389171875" />
                  <Point X="-27.44698828125" Y="-2.381192382812" />
                  <Point X="-27.4758359375" Y="-2.366916259766" />
                  <Point X="-27.4902890625" Y="-2.361183349609" />
                  <Point X="-27.5199375" Y="-2.352151855469" />
                  <Point X="-27.5506640625" Y="-2.348077880859" />
                  <Point X="-27.581642578125" Y="-2.349070556641" />
                  <Point X="-27.59708984375" Y="-2.350838623047" />
                  <Point X="-27.628662109375" Y="-2.357103515625" />
                  <Point X="-27.64361328125" Y="-2.361366943359" />
                  <Point X="-27.67262109375" Y="-2.372276123047" />
                  <Point X="-27.686677734375" Y="-2.378921875" />
                  <Point X="-27.70633984375" Y="-2.390272949219" />
                  <Point X="-28.793087890625" Y="-3.017707763672" />
                  <Point X="-29.004021484375" Y="-2.740583740234" />
                  <Point X="-29.181265625" Y="-2.443373779297" />
                  <Point X="-28.048123046875" Y="-1.573881958008" />
                  <Point X="-28.036484375" Y="-1.563311523438" />
                  <Point X="-28.015107421875" Y="-1.540393066406" />
                  <Point X="-28.005369140625" Y="-1.528045166016" />
                  <Point X="-27.987404296875" Y="-1.500915405273" />
                  <Point X="-27.979837890625" Y="-1.487129272461" />
                  <Point X="-27.96708203125" Y="-1.458500610352" />
                  <Point X="-27.961892578125" Y="-1.443657714844" />
                  <Point X="-27.9543046875" Y="-1.414363769531" />
                  <Point X="-27.95151171875" Y="-1.39859777832" />
                  <Point X="-27.948736328125" Y="-1.368227539062" />
                  <Point X="-27.94857421875" Y="-1.352927734375" />
                  <Point X="-27.950796875" Y="-1.32127331543" />
                  <Point X="-27.953099609375" Y="-1.306124389648" />
                  <Point X="-27.960109375" Y="-1.276396850586" />
                  <Point X="-27.971806640625" Y="-1.248184448242" />
                  <Point X="-27.98788671875" Y="-1.222217407227" />
                  <Point X="-27.9969765625" Y="-1.209884399414" />
                  <Point X="-28.017802734375" Y="-1.185942138672" />
                  <Point X="-28.028759765625" Y="-1.175229492188" />
                  <Point X="-28.05225" Y="-1.155706787109" />
                  <Point X="-28.064783203125" Y="-1.146895996094" />
                  <Point X="-28.090845703125" Y="-1.131556640625" />
                  <Point X="-28.1053828125" Y="-1.124506713867" />
                  <Point X="-28.134724609375" Y="-1.113245849609" />
                  <Point X="-28.149865234375" Y="-1.108837280273" />
                  <Point X="-28.18173046875" Y="-1.102368652344" />
                  <Point X="-28.197333984375" Y="-1.100526977539" />
                  <Point X="-28.2286328125" Y="-1.099441040039" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-28.2691484375" Y="-1.103464111328" />
                  <Point X="-29.66091796875" Y="-1.286694213867" />
                  <Point X="-29.740763671875" Y="-0.974107543945" />
                  <Point X="-29.786451171875" Y="-0.654654846191" />
                  <Point X="-28.508287109375" Y="-0.312171234131" />
                  <Point X="-28.5004765625" Y="-0.309713012695" />
                  <Point X="-28.47393359375" Y="-0.299252471924" />
                  <Point X="-28.44416796875" Y="-0.283893951416" />
                  <Point X="-28.4335625" Y="-0.277514434814" />
                  <Point X="-28.406232421875" Y="-0.25854586792" />
                  <Point X="-28.393544921875" Y="-0.248143005371" />
                  <Point X="-28.37123046875" Y="-0.226371536255" />
                  <Point X="-28.36097265625" Y="-0.214565078735" />
                  <Point X="-28.341697265625" Y="-0.188288711548" />
                  <Point X="-28.33346484375" Y="-0.174859893799" />
                  <Point X="-28.319333984375" Y="-0.146827087402" />
                  <Point X="-28.313435546875" Y="-0.132222808838" />
                  <Point X="-28.304326171875" Y="-0.102870384216" />
                  <Point X="-28.30088671875" Y="-0.088094596863" />
                  <Point X="-28.2966875" Y="-0.060127784729" />
                  <Point X="-28.295634765625" Y="-0.045984088898" />
                  <Point X="-28.295646484375" Y="-0.016384805679" />
                  <Point X="-28.29672265625" Y="-0.002161893845" />
                  <Point X="-28.3009921875" Y="0.025958444595" />
                  <Point X="-28.304185546875" Y="0.039856021881" />
                  <Point X="-28.313294921875" Y="0.069208297729" />
                  <Point X="-28.319357421875" Y="0.084333648682" />
                  <Point X="-28.333408203125" Y="0.112201324463" />
                  <Point X="-28.341587890625" Y="0.125556877136" />
                  <Point X="-28.36084375" Y="0.151853759766" />
                  <Point X="-28.371173828125" Y="0.163750274658" />
                  <Point X="-28.39365625" Y="0.18566947937" />
                  <Point X="-28.40580859375" Y="0.195692001343" />
                  <Point X="-28.433138671875" Y="0.214660598755" />
                  <Point X="-28.439853515625" Y="0.218916107178" />
                  <Point X="-28.464431640625" Y="0.232203887939" />
                  <Point X="-28.496232421875" Y="0.245497619629" />
                  <Point X="-28.508283203125" Y="0.249610153198" />
                  <Point X="-28.53090625" Y="0.255672485352" />
                  <Point X="-29.7854453125" Y="0.591825195312" />
                  <Point X="-29.73133203125" Y="0.957513366699" />
                  <Point X="-29.6335859375" Y="1.318237060547" />
                  <Point X="-28.778064453125" Y="1.20560546875" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.7470546875" Y="1.204364379883" />
                  <Point X="-28.736701171875" Y="1.204703369141" />
                  <Point X="-28.715140625" Y="1.20658984375" />
                  <Point X="-28.70488671875" Y="1.208053833008" />
                  <Point X="-28.684599609375" Y="1.212089355469" />
                  <Point X="-28.674568359375" Y="1.214660766602" />
                  <Point X="-28.61409375" Y="1.233728515625" />
                  <Point X="-28.603529296875" Y="1.237647827148" />
                  <Point X="-28.58457421875" Y="1.245980957031" />
                  <Point X="-28.575322265625" Y="1.250666015625" />
                  <Point X="-28.5565703125" Y="1.261487426758" />
                  <Point X="-28.547892578125" Y="1.267148803711" />
                  <Point X="-28.531205078125" Y="1.279380737305" />
                  <Point X="-28.515951171875" Y="1.293354248047" />
                  <Point X="-28.502306640625" Y="1.308908813477" />
                  <Point X="-28.49590625" Y="1.317060546875" />
                  <Point X="-28.483486328125" Y="1.334795166016" />
                  <Point X="-28.478015625" Y="1.34359375" />
                  <Point X="-28.4680625" Y="1.36173449707" />
                  <Point X="-28.463580078125" Y="1.371076538086" />
                  <Point X="-28.43930859375" Y="1.429673339844" />
                  <Point X="-28.435466796875" Y="1.440441162109" />
                  <Point X="-28.42967578125" Y="1.460337524414" />
                  <Point X="-28.42732421875" Y="1.470446044922" />
                  <Point X="-28.42358203125" Y="1.491743896484" />
                  <Point X="-28.422349609375" Y="1.502015136719" />
                  <Point X="-28.4210078125" Y="1.522634521484" />
                  <Point X="-28.421916015625" Y="1.543278808594" />
                  <Point X="-28.425064453125" Y="1.563700439453" />
                  <Point X="-28.4271953125" Y="1.573825683594" />
                  <Point X="-28.432794921875" Y="1.594712036133" />
                  <Point X="-28.436015625" Y="1.604546142578" />
                  <Point X="-28.4435078125" Y="1.623810424805" />
                  <Point X="-28.447779296875" Y="1.633240234375" />
                  <Point X="-28.477064453125" Y="1.689498901367" />
                  <Point X="-28.483015625" Y="1.699667236328" />
                  <Point X="-28.49444921875" Y="1.71672265625" />
                  <Point X="-28.50062890625" Y="1.72494152832" />
                  <Point X="-28.5145078125" Y="1.741457397461" />
                  <Point X="-28.521568359375" Y="1.748986694336" />
                  <Point X="-28.536462890625" Y="1.763235473633" />
                  <Point X="-28.557275390625" Y="1.779913085938" />
                  <Point X="-29.22761328125" Y="2.294281982422" />
                  <Point X="-29.002296875" Y="2.680306396484" />
                  <Point X="-28.72633984375" Y="3.035012939453" />
                  <Point X="-28.25415625" Y="2.7623984375" />
                  <Point X="-28.24491796875" Y="2.757716552734" />
                  <Point X="-28.225986328125" Y="2.749386230469" />
                  <Point X="-28.21629296875" Y="2.745737792969" />
                  <Point X="-28.19565234375" Y="2.739229492188" />
                  <Point X="-28.1856171875" Y="2.736657470703" />
                  <Point X="-28.165328125" Y="2.732621826172" />
                  <Point X="-28.15507421875" Y="2.731158203125" />
                  <Point X="-28.070828125" Y="2.723787597656" />
                  <Point X="-28.05917578125" Y="2.723334472656" />
                  <Point X="-28.038498046875" Y="2.723785400391" />
                  <Point X="-28.028169921875" Y="2.724575439453" />
                  <Point X="-28.0067109375" Y="2.727400146484" />
                  <Point X="-27.99653125" Y="2.729309814453" />
                  <Point X="-27.976439453125" Y="2.734225830078" />
                  <Point X="-27.957001953125" Y="2.741300292969" />
                  <Point X="-27.938451171875" Y="2.750448242188" />
                  <Point X="-27.92942578125" Y="2.755528076172" />
                  <Point X="-27.911169921875" Y="2.767157714844" />
                  <Point X="-27.902748046875" Y="2.773191894531" />
                  <Point X="-27.886615234375" Y="2.786139648438" />
                  <Point X="-27.878904296875" Y="2.793053222656" />
                  <Point X="-27.81910546875" Y="2.852850830078" />
                  <Point X="-27.811265625" Y="2.861487792969" />
                  <Point X="-27.79831640625" Y="2.877621337891" />
                  <Point X="-27.79228125" Y="2.886043701172" />
                  <Point X="-27.78065234375" Y="2.904298095703" />
                  <Point X="-27.7755703125" Y="2.913325927734" />
                  <Point X="-27.766423828125" Y="2.931874755859" />
                  <Point X="-27.759353515625" Y="2.951299316406" />
                  <Point X="-27.754435546875" Y="2.971388183594" />
                  <Point X="-27.7525234375" Y="2.981573486328" />
                  <Point X="-27.749697265625" Y="3.003032470703" />
                  <Point X="-27.748908203125" Y="3.013365478516" />
                  <Point X="-27.74845703125" Y="3.034050537109" />
                  <Point X="-27.748794921875" Y="3.044402587891" />
                  <Point X="-27.756166015625" Y="3.128647705078" />
                  <Point X="-27.75773828125" Y="3.140161865234" />
                  <Point X="-27.761765625" Y="3.160421386719" />
                  <Point X="-27.76433203125" Y="3.170438964844" />
                  <Point X="-27.77084375" Y="3.191113037109" />
                  <Point X="-27.77449609375" Y="3.200824707031" />
                  <Point X="-27.78283984375" Y="3.219792724609" />
                  <Point X="-27.793283203125" Y="3.239009521484" />
                  <Point X="-28.05938671875" Y="3.699914794922" />
                  <Point X="-27.648359375" Y="4.015045166016" />
                  <Point X="-27.1925234375" Y="4.268295410156" />
                  <Point X="-27.118564453125" Y="4.171909179688" />
                  <Point X="-27.1118203125" Y="4.164048828125" />
                  <Point X="-27.097517578125" Y="4.149107421875" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.065091796875" Y="4.121897460938" />
                  <Point X="-27.047892578125" Y="4.110405273438" />
                  <Point X="-27.03898046875" Y="4.105129394531" />
                  <Point X="-26.945216796875" Y="4.056318115234" />
                  <Point X="-26.934275390625" Y="4.051262451172" />
                  <Point X="-26.914990234375" Y="4.043768310547" />
                  <Point X="-26.90514453125" Y="4.040548339844" />
                  <Point X="-26.884244140625" Y="4.034953613281" />
                  <Point X="-26.87411328125" Y="4.032825439453" />
                  <Point X="-26.853677734375" Y="4.029684326172" />
                  <Point X="-26.833021484375" Y="4.028785644531" />
                  <Point X="-26.812388671875" Y="4.030140136719" />
                  <Point X="-26.802107421875" Y="4.031380371094" />
                  <Point X="-26.780798828125" Y="4.035139160156" />
                  <Point X="-26.77071484375" Y="4.037491210938" />
                  <Point X="-26.75086328125" Y="4.043278320312" />
                  <Point X="-26.741095703125" Y="4.046713623047" />
                  <Point X="-26.6435" Y="4.087139648438" />
                  <Point X="-26.632642578125" Y="4.092245117188" />
                  <Point X="-26.6145" Y="4.102192871094" />
                  <Point X="-26.605701171875" Y="4.107661132812" />
                  <Point X="-26.587962890625" Y="4.120075683594" />
                  <Point X="-26.579810546875" Y="4.126471679688" />
                  <Point X="-26.56425" Y="4.14011328125" />
                  <Point X="-26.55026953125" Y="4.155365722656" />
                  <Point X="-26.53803125" Y="4.172052734375" />
                  <Point X="-26.532365234375" Y="4.180732910156" />
                  <Point X="-26.5215390625" Y="4.199482910156" />
                  <Point X="-26.51685546875" Y="4.208724121094" />
                  <Point X="-26.5085234375" Y="4.227659667969" />
                  <Point X="-26.504875" Y="4.237354003906" />
                  <Point X="-26.473087890625" Y="4.338169433594" />
                  <Point X="-26.470025390625" Y="4.349764160156" />
                  <Point X="-26.465990234375" Y="4.370050292969" />
                  <Point X="-26.464525390625" Y="4.380302734375" />
                  <Point X="-26.462638671875" Y="4.401864257812" />
                  <Point X="-26.46230078125" Y="4.412225585938" />
                  <Point X="-26.46275390625" Y="4.432915527344" />
                  <Point X="-26.46419921875" Y="4.448210449219" />
                  <Point X="-26.479265625" Y="4.562654785156" />
                  <Point X="-25.931171875" Y="4.716320800781" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.22566796875" Y="4.261727539062" />
                  <Point X="-25.221880859375" Y="4.250466308594" />
                  <Point X="-25.209740234375" Y="4.220672363281" />
                  <Point X="-25.202775390625" Y="4.206901367188" />
                  <Point X="-25.190673828125" Y="4.187144042969" />
                  <Point X="-25.176255859375" Y="4.16901171875" />
                  <Point X="-25.159732421875" Y="4.152770996094" />
                  <Point X="-25.15097265625" Y="4.145155761719" />
                  <Point X="-25.12578125" Y="4.125825195313" />
                  <Point X="-25.11273046875" Y="4.11741015625" />
                  <Point X="-25.08543359375" Y="4.102819824219" />
                  <Point X="-25.056126953125" Y="4.092871582031" />
                  <Point X="-25.02558984375" Y="4.087829589844" />
                  <Point X="-25.01011328125" Y="4.086560546875" />
                  <Point X="-24.97757421875" Y="4.086560546875" />
                  <Point X="-24.96209765625" Y="4.087829589844" />
                  <Point X="-24.931560546875" Y="4.092871582031" />
                  <Point X="-24.90225390625" Y="4.102819824219" />
                  <Point X="-24.87495703125" Y="4.11741015625" />
                  <Point X="-24.86190625" Y="4.125825195313" />
                  <Point X="-24.83671484375" Y="4.145155761719" />
                  <Point X="-24.82682421875" Y="4.153901367188" />
                  <Point X="-24.808345703125" Y="4.172685546875" />
                  <Point X="-24.792646484375" Y="4.193858398438" />
                  <Point X="-24.780041015625" Y="4.21699609375" />
                  <Point X="-24.774546875" Y="4.228999511719" />
                  <Point X="-24.764763671875" Y="4.255008789062" />
                  <Point X="-24.759072265625" Y="4.272724609375" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.172123046875" Y="4.737911621094" />
                  <Point X="-23.546408203125" Y="4.586845214844" />
                  <Point X="-23.14173828125" Y="4.440069335938" />
                  <Point X="-22.749546875" Y="4.256654296875" />
                  <Point X="-22.370572265625" Y="4.035863037109" />
                  <Point X="-22.18221875" Y="3.901915527344" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.937623046875" Y="2.593106933594" />
                  <Point X="-22.9468125" Y="2.573447753906" />
                  <Point X="-22.955814453125" Y="2.549570556641" />
                  <Point X="-22.958697265625" Y="2.540599365234" />
                  <Point X="-22.97983984375" Y="2.461537353516" />
                  <Point X="-22.9816640625" Y="2.453245849609" />
                  <Point X="-22.985640625" Y="2.428126220703" />
                  <Point X="-22.987306640625" Y="2.407998779297" />
                  <Point X="-22.98761328125" Y="2.398385253906" />
                  <Point X="-22.986587890625" Y="2.369578857422" />
                  <Point X="-22.97836328125" Y="2.301367675781" />
                  <Point X="-22.976220703125" Y="2.289168945312" />
                  <Point X="-22.970880859375" Y="2.26721484375" />
                  <Point X="-22.967556640625" Y="2.256400878906" />
                  <Point X="-22.959287109375" Y="2.234280273438" />
                  <Point X="-22.954701171875" Y="2.223936523438" />
                  <Point X="-22.944328125" Y="2.203861328125" />
                  <Point X="-22.938541015625" Y="2.194129882813" />
                  <Point X="-22.89623828125" Y="2.131786621094" />
                  <Point X="-22.89117578125" Y="2.124998046875" />
                  <Point X="-22.87150390625" Y="2.102362060547" />
                  <Point X="-22.842984375" Y="2.075924072266" />
                  <Point X="-22.8317421875" Y="2.066982910156" />
                  <Point X="-22.769400390625" Y="2.024680175781" />
                  <Point X="-22.758630859375" Y="2.018190429688" />
                  <Point X="-22.738572265625" Y="2.007829345703" />
                  <Point X="-22.728234375" Y="2.003246337891" />
                  <Point X="-22.70618359375" Y="1.995004882812" />
                  <Point X="-22.695396484375" Y="1.991689086914" />
                  <Point X="-22.673509765625" Y="1.986360473633" />
                  <Point X="-22.66241015625" Y="1.984347045898" />
                  <Point X="-22.594044921875" Y="1.976103149414" />
                  <Point X="-22.585498046875" Y="1.975462036133" />
                  <Point X="-22.55517578125" Y="1.975479125977" />
                  <Point X="-22.51641796875" Y="1.979739135742" />
                  <Point X="-22.502255859375" Y="1.982395141602" />
                  <Point X="-22.423193359375" Y="2.003537475586" />
                  <Point X="-22.417755859375" Y="2.005166503906" />
                  <Point X="-22.39861328125" Y="2.011979248047" />
                  <Point X="-22.373115234375" Y="2.023229736328" />
                  <Point X="-22.36396484375" Y="2.027872802734" />
                  <Point X="-22.341208984375" Y="2.041011108398" />
                  <Point X="-21.05959375" Y="2.780951416016" />
                  <Point X="-20.956041015625" Y="2.637034912109" />
                  <Point X="-20.863115234375" Y="2.483470947266" />
                  <Point X="-21.827046875" Y="1.743819946289" />
                  <Point X="-21.83186328125" Y="1.739867797852" />
                  <Point X="-21.84787109375" Y="1.725225097656" />
                  <Point X="-21.865328125" Y="1.706607543945" />
                  <Point X="-21.87142578125" Y="1.699421020508" />
                  <Point X="-21.928326171875" Y="1.625189208984" />
                  <Point X="-21.93314453125" Y="1.618290405273" />
                  <Point X="-21.946341796875" Y="1.596794799805" />
                  <Point X="-21.95569140625" Y="1.578693359375" />
                  <Point X="-21.95969140625" Y="1.569869628906" />
                  <Point X="-21.969859375" Y="1.542672485352" />
                  <Point X="-21.99103125" Y="1.466965332031" />
                  <Point X="-21.993697265625" Y="1.454983154297" />
                  <Point X="-21.997189453125" Y="1.432602783203" />
                  <Point X="-21.998267578125" Y="1.421304931641" />
                  <Point X="-21.9991015625" Y="1.397657470703" />
                  <Point X="-21.99882421875" Y="1.386321899414" />
                  <Point X="-21.996921875" Y="1.363775390625" />
                  <Point X="-21.995296875" Y="1.352564208984" />
                  <Point X="-21.977896484375" Y="1.268236938477" />
                  <Point X="-21.97583984375" Y="1.260101196289" />
                  <Point X="-21.968248046875" Y="1.236121826172" />
                  <Point X="-21.960783203125" Y="1.217443603516" />
                  <Point X="-21.956791015625" Y="1.208748657227" />
                  <Point X="-21.943083984375" Y="1.183527099609" />
                  <Point X="-21.895765625" Y="1.111604248047" />
                  <Point X="-21.888291015625" Y="1.101466918945" />
                  <Point X="-21.87373046875" Y="1.084207641602" />
                  <Point X="-21.8659375" Y="1.076012939453" />
                  <Point X="-21.8486875" Y="1.05991418457" />
                  <Point X="-21.839978515625" Y="1.052707275391" />
                  <Point X="-21.821763671875" Y="1.039375244141" />
                  <Point X="-21.812255859375" Y="1.033249755859" />
                  <Point X="-21.74367578125" Y="0.994645141602" />
                  <Point X="-21.736080078125" Y="0.990807250977" />
                  <Point X="-21.712630859375" Y="0.980667480469" />
                  <Point X="-21.69360546875" Y="0.974079162598" />
                  <Point X="-21.684396484375" Y="0.97140234375" />
                  <Point X="-21.656328125" Y="0.965257385254" />
                  <Point X="-21.563603515625" Y="0.953002807617" />
                  <Point X="-21.5580234375" Y="0.952432434082" />
                  <Point X="-21.537912109375" Y="0.951395629883" />
                  <Point X="-21.509734375" Y="0.952006896973" />
                  <Point X="-21.49939453125" Y="0.952797424316" />
                  <Point X="-21.477779296875" Y="0.955643432617" />
                  <Point X="-20.295296875" Y="1.111319946289" />
                  <Point X="-20.2473125" Y="0.914211181641" />
                  <Point X="-20.216126953125" Y="0.713920898438" />
                  <Point X="-21.3080078125" Y="0.421352813721" />
                  <Point X="-21.31396875" Y="0.419544219971" />
                  <Point X="-21.334376953125" Y="0.412136413574" />
                  <Point X="-21.357619140625" Y="0.401618804932" />
                  <Point X="-21.365994140625" Y="0.397316772461" />
                  <Point X="-21.45709375" Y="0.344659545898" />
                  <Point X="-21.464125" Y="0.340172607422" />
                  <Point X="-21.48437109375" Y="0.325516601562" />
                  <Point X="-21.50009375" Y="0.312281005859" />
                  <Point X="-21.507228515625" Y="0.305619567871" />
                  <Point X="-21.527203125" Y="0.28422769165" />
                  <Point X="-21.58186328125" Y="0.214578414917" />
                  <Point X="-21.5891953125" Y="0.204137496948" />
                  <Point X="-21.60090234375" Y="0.184873458862" />
                  <Point X="-21.606185546875" Y="0.174889419556" />
                  <Point X="-21.615947265625" Y="0.153441055298" />
                  <Point X="-21.620005859375" Y="0.142901611328" />
                  <Point X="-21.62684375" Y="0.121418617249" />
                  <Point X="-21.629623046875" Y="0.110474777222" />
                  <Point X="-21.64784375" Y="0.015337409019" />
                  <Point X="-21.649048828125" Y="0.007105039597" />
                  <Point X="-21.651224609375" Y="-0.017847105026" />
                  <Point X="-21.6515" Y="-0.038102024078" />
                  <Point X="-21.651140625" Y="-0.047728031158" />
                  <Point X="-21.648125" Y="-0.076423538208" />
                  <Point X="-21.62991015625" Y="-0.171532516479" />
                  <Point X="-21.6268671875" Y="-0.183867630005" />
                  <Point X="-21.620021484375" Y="-0.205398178101" />
                  <Point X="-21.61595703125" Y="-0.215957977295" />
                  <Point X="-21.606177734375" Y="-0.237450027466" />
                  <Point X="-21.600888671875" Y="-0.247449081421" />
                  <Point X="-21.589154296875" Y="-0.266753997803" />
                  <Point X="-21.582708984375" Y="-0.276060180664" />
                  <Point X="-21.528060546875" Y="-0.34569430542" />
                  <Point X="-21.5225625" Y="-0.352093414307" />
                  <Point X="-21.504974609375" Y="-0.370265960693" />
                  <Point X="-21.4898046875" Y="-0.38377935791" />
                  <Point X="-21.482265625" Y="-0.389835296631" />
                  <Point X="-21.45850390625" Y="-0.40640435791" />
                  <Point X="-21.367404296875" Y="-0.459061309814" />
                  <Point X="-21.344515625" Y="-0.470282318115" />
                  <Point X="-21.31796484375" Y="-0.48064654541" />
                  <Point X="-21.3080078125" Y="-0.483913116455" />
                  <Point X="-21.28818359375" Y="-0.489224914551" />
                  <Point X="-20.21512109375" Y="-0.776751281738" />
                  <Point X="-20.2383828125" Y="-0.931035583496" />
                  <Point X="-20.272197265625" Y="-1.079219726563" />
                  <Point X="-21.563216796875" Y="-0.90925378418" />
                  <Point X="-21.571376953125" Y="-0.908535827637" />
                  <Point X="-21.59990625" Y="-0.908042663574" />
                  <Point X="-21.633283203125" Y="-0.910841674805" />
                  <Point X="-21.645521484375" Y="-0.912676879883" />
                  <Point X="-21.824318359375" Y="-0.951538818359" />
                  <Point X="-21.84212890625" Y="-0.956743408203" />
                  <Point X="-21.871251953125" Y="-0.968369445801" />
                  <Point X="-21.88533203125" Y="-0.975392333984" />
                  <Point X="-21.91315625" Y="-0.992284179688" />
                  <Point X="-21.92587890625" Y="-1.001532714844" />
                  <Point X="-21.949626953125" Y="-1.022002258301" />
                  <Point X="-21.96065234375" Y="-1.033223510742" />
                  <Point X="-22.06872265625" Y="-1.16319921875" />
                  <Point X="-22.07866796875" Y="-1.176839233398" />
                  <Point X="-22.09338671875" Y="-1.201216796875" />
                  <Point X="-22.099828125" Y="-1.213960571289" />
                  <Point X="-22.11117578125" Y="-1.241352539062" />
                  <Point X="-22.115634765625" Y="-1.254926635742" />
                  <Point X="-22.122466796875" Y="-1.282580688477" />
                  <Point X="-22.12483984375" Y="-1.296661010742" />
                  <Point X="-22.140318359375" Y="-1.464877319336" />
                  <Point X="-22.140708984375" Y="-1.483290893555" />
                  <Point X="-22.138392578125" Y="-1.514565795898" />
                  <Point X="-22.135935546875" Y="-1.530104003906" />
                  <Point X="-22.128205078125" Y="-1.561732788086" />
                  <Point X="-22.123216796875" Y="-1.57665612793" />
                  <Point X="-22.11084375" Y="-1.605475097656" />
                  <Point X="-22.103458984375" Y="-1.61937097168" />
                  <Point X="-22.00451171875" Y="-1.773278198242" />
                  <Point X="-22.000490234375" Y="-1.779052368164" />
                  <Point X="-21.98453515625" Y="-1.799072753906" />
                  <Point X="-21.957763671875" Y="-1.826843261719" />
                  <Point X="-21.947201171875" Y="-1.836278320312" />
                  <Point X="-21.9288046875" Y="-1.850394287109" />
                  <Point X="-20.912828125" Y="-2.629981689453" />
                  <Point X="-20.954513671875" Y="-2.697434814453" />
                  <Point X="-20.998724609375" Y="-2.760251953125" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.15880859375" Y="-2.090885009766" />
                  <Point X="-22.184974609375" Y="-2.079512695312" />
                  <Point X="-22.216880859375" Y="-2.069325683594" />
                  <Point X="-22.228890625" Y="-2.066337402344" />
                  <Point X="-22.441685546875" Y="-2.027906616211" />
                  <Point X="-22.460640625" Y="-2.025807250977" />
                  <Point X="-22.491994140625" Y="-2.025403198242" />
                  <Point X="-22.5076875" Y="-2.026503662109" />
                  <Point X="-22.539859375" Y="-2.031461669922" />
                  <Point X="-22.555154296875" Y="-2.035136352539" />
                  <Point X="-22.5849296875" Y="-2.044959838867" />
                  <Point X="-22.59941015625" Y="-2.051108642578" />
                  <Point X="-22.77619140625" Y="-2.144146972656" />
                  <Point X="-22.791078125" Y="-2.15319921875" />
                  <Point X="-22.813986328125" Y="-2.170082519531" />
                  <Point X="-22.8248046875" Y="-2.17938671875" />
                  <Point X="-22.8457578125" Y="-2.200342773438" />
                  <Point X="-22.855064453125" Y="-2.21116796875" />
                  <Point X="-22.871953125" Y="-2.23409375" />
                  <Point X="-22.87953515625" Y="-2.246194335938" />
                  <Point X="-22.97257421875" Y="-2.422975097656" />
                  <Point X="-22.98015625" Y="-2.440177734375" />
                  <Point X="-22.989978515625" Y="-2.469943603516" />
                  <Point X="-22.993654296875" Y="-2.485234375" />
                  <Point X="-22.9986171875" Y="-2.517417236328" />
                  <Point X="-22.99971875" Y="-2.533114501953" />
                  <Point X="-22.99931640625" Y="-2.564477539062" />
                  <Point X="-22.9978125" Y="-2.580143310547" />
                  <Point X="-22.959380859375" Y="-2.792938720703" />
                  <Point X="-22.95791796875" Y="-2.799637695312" />
                  <Point X="-22.952576171875" Y="-2.819491943359" />
                  <Point X="-22.946078125" Y="-2.838900390625" />
                  <Point X="-22.942603515625" Y="-2.847771728516" />
                  <Point X="-22.930453125" Y="-2.873571777344" />
                  <Point X="-22.9186328125" Y="-2.894047119141" />
                  <Point X="-22.264103515625" Y="-4.027721923828" />
                  <Point X="-22.276244140625" Y="-4.036083984375" />
                  <Point X="-23.16608203125" Y="-2.876422851562" />
                  <Point X="-23.171345703125" Y="-2.87014453125" />
                  <Point X="-23.191166015625" Y="-2.849627197266" />
                  <Point X="-23.21674609375" Y="-2.828004394531" />
                  <Point X="-23.22669921875" Y="-2.820646484375" />
                  <Point X="-23.43657421875" Y="-2.685717041016" />
                  <Point X="-23.453716796875" Y="-2.676244873047" />
                  <Point X="-23.482529296875" Y="-2.663873046875" />
                  <Point X="-23.49744921875" Y="-2.658884033203" />
                  <Point X="-23.5290703125" Y="-2.651153076172" />
                  <Point X="-23.544607421875" Y="-2.648695068359" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.82113671875" Y="-2.667638183594" />
                  <Point X="-23.8387734375" Y="-2.670339355469" />
                  <Point X="-23.86642578125" Y="-2.677171875" />
                  <Point X="-23.87999609375" Y="-2.681630371094" />
                  <Point X="-23.907380859375" Y="-2.692974121094" />
                  <Point X="-23.920126953125" Y="-2.699416503906" />
                  <Point X="-23.944505859375" Y="-2.714136230469" />
                  <Point X="-23.956138671875" Y="-2.722413574219" />
                  <Point X="-24.13337890625" Y="-2.869782226562" />
                  <Point X="-24.147345703125" Y="-2.883088134766" />
                  <Point X="-24.16781640625" Y="-2.906837646484" />
                  <Point X="-24.177064453125" Y="-2.919562988281" />
                  <Point X="-24.19395703125" Y="-2.947389404297" />
                  <Point X="-24.20098046875" Y="-2.961467285156" />
                  <Point X="-24.21260546875" Y="-2.990588623047" />
                  <Point X="-24.21720703125" Y="-3.005632080078" />
                  <Point X="-24.2701953125" Y="-3.249423828125" />
                  <Point X="-24.271384765625" Y="-3.255996337891" />
                  <Point X="-24.27401953125" Y="-3.275852539062" />
                  <Point X="-24.27555859375" Y="-3.296794433594" />
                  <Point X="-24.275775390625" Y="-3.306486572266" />
                  <Point X="-24.2744453125" Y="-3.335530761719" />
                  <Point X="-24.27109375" Y="-3.360977294922" />
                  <Point X="-24.166912109375" Y="-4.152315917969" />
                  <Point X="-24.344931640625" Y="-3.487937011719" />
                  <Point X="-24.347390625" Y="-3.480127441406" />
                  <Point X="-24.3578515625" Y="-3.453580810547" />
                  <Point X="-24.373208984375" Y="-3.423817871094" />
                  <Point X="-24.379587890625" Y="-3.413212646484" />
                  <Point X="-24.5408203125" Y="-3.180909179688" />
                  <Point X="-24.55332421875" Y="-3.165177246094" />
                  <Point X="-24.575205078125" Y="-3.142723876953" />
                  <Point X="-24.587078125" Y="-3.132405517578" />
                  <Point X="-24.6133359375" Y="-3.113158447266" />
                  <Point X="-24.626751953125" Y="-3.104939941406" />
                  <Point X="-24.654755859375" Y="-3.090829833984" />
                  <Point X="-24.66934375" Y="-3.084938232422" />
                  <Point X="-24.91883984375" Y="-3.00750390625" />
                  <Point X="-24.936625" Y="-3.003108642578" />
                  <Point X="-24.9647890625" Y="-2.998839355469" />
                  <Point X="-24.97903125" Y="-2.997766357422" />
                  <Point X="-25.008673828125" Y="-2.997767333984" />
                  <Point X="-25.022912109375" Y="-2.998840820312" />
                  <Point X="-25.051068359375" Y="-3.003110107422" />
                  <Point X="-25.064986328125" Y="-3.006305908203" />
                  <Point X="-25.314482421875" Y="-3.083739990234" />
                  <Point X="-25.332935546875" Y="-3.090832519531" />
                  <Point X="-25.36094140625" Y="-3.104945068359" />
                  <Point X="-25.374357421875" Y="-3.113164306641" />
                  <Point X="-25.40061328125" Y="-3.132411621094" />
                  <Point X="-25.412484375" Y="-3.142727294922" />
                  <Point X="-25.43436328125" Y="-3.165178466797" />
                  <Point X="-25.44437109375" Y="-3.177313964844" />
                  <Point X="-25.6056015625" Y="-3.4096171875" />
                  <Point X="-25.6189765625" Y="-3.432031738281" />
                  <Point X="-25.62869140625" Y="-3.451192871094" />
                  <Point X="-25.63268359375" Y="-3.460199707031" />
                  <Point X="-25.64275390625" Y="-3.487940917969" />
                  <Point X="-25.648861328125" Y="-3.510736328125" />
                  <Point X="-25.985423828125" Y="-4.766807128906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.33688295481" Y="-3.957057819394" />
                  <Point X="-22.310455092308" Y="-3.947438864088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.397521768996" Y="-3.878031654414" />
                  <Point X="-22.358687968614" Y="-3.863897306992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.458160583181" Y="-3.799005489433" />
                  <Point X="-22.406920844921" Y="-3.780355749895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.518799397366" Y="-3.719979324453" />
                  <Point X="-22.455153721227" Y="-3.696814192798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.579438211551" Y="-3.640953159472" />
                  <Point X="-22.503386597534" Y="-3.613272635702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.175375531288" Y="-4.120729951226" />
                  <Point X="-24.171267333008" Y="-4.119234689335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.640077025737" Y="-3.561926994492" />
                  <Point X="-22.55161947384" Y="-3.529731078605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.135091984873" Y="-4.732911519547" />
                  <Point X="-25.959186281117" Y="-4.668887079342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.200057229146" Y="-4.028616466192" />
                  <Point X="-24.183968379337" Y="-4.022760603758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.700715839922" Y="-3.482900829511" />
                  <Point X="-22.599852350147" Y="-3.446189521509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.127793115394" Y="-4.629158059928" />
                  <Point X="-25.929170173245" Y="-4.556865221143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.224738927005" Y="-3.936502981158" />
                  <Point X="-24.196669425666" Y="-3.92628651818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.761354654107" Y="-3.40387466453" />
                  <Point X="-22.648085226453" Y="-3.362647964412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.120584584786" Y="-4.525437480968" />
                  <Point X="-25.899154065374" Y="-4.444843362944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.249420624863" Y="-3.844389496125" />
                  <Point X="-24.209370471995" Y="-3.829812432603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.821993468292" Y="-3.32484849955" />
                  <Point X="-22.69631810276" Y="-3.279106407315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.103916231569" Y="-2.699519525212" />
                  <Point X="-20.912839215933" Y="-2.629973179068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.13675369029" Y="-4.430225665701" />
                  <Point X="-25.869137957503" Y="-4.332821504746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.274102322721" Y="-3.752276011091" />
                  <Point X="-24.222071518324" Y="-3.733338347026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.882632282478" Y="-3.245822334569" />
                  <Point X="-22.744550979066" Y="-3.195564850219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.211315221758" Y="-2.637512672446" />
                  <Point X="-21.002202810017" Y="-2.561401978957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.155505809726" Y="-4.33595399062" />
                  <Point X="-25.839121849632" Y="-4.220799646547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.29878402058" Y="-3.660162526057" />
                  <Point X="-24.234772564653" Y="-3.636864261448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.943271096663" Y="-3.166796169589" />
                  <Point X="-22.792783855373" Y="-3.112023293122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.318714211947" Y="-2.57550581968" />
                  <Point X="-21.091566404101" Y="-2.492830778845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.174257929163" Y="-4.241682315539" />
                  <Point X="-25.809105741761" Y="-4.108777788348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.323465718438" Y="-3.568049041024" />
                  <Point X="-24.247473610982" Y="-3.540390175871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.003909910848" Y="-3.087770004608" />
                  <Point X="-22.841016731679" Y="-3.028481736026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.426113202136" Y="-2.513498966914" />
                  <Point X="-21.180929998184" Y="-2.424259578733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.198995068578" Y="-4.149589009582" />
                  <Point X="-25.77908963389" Y="-3.996755930149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.348930203188" Y="-3.476220467119" />
                  <Point X="-24.260174657311" Y="-3.443916090293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.064548725034" Y="-3.008743839628" />
                  <Point X="-22.889249607986" Y="-2.944940178929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.533512192325" Y="-2.451492114148" />
                  <Point X="-21.270293592268" Y="-2.355688378622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.252931019096" Y="-4.068123201742" />
                  <Point X="-25.749073526019" Y="-3.88473407195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.394509790757" Y="-3.391713191899" />
                  <Point X="-24.27287644722" Y="-3.347442275357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.125187539219" Y="-2.929717674647" />
                  <Point X="-22.93637571627" Y="-2.860995791216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.640911182515" Y="-2.389485261382" />
                  <Point X="-21.359657186352" Y="-2.28711717851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.333381156851" Y="-3.996307768843" />
                  <Point X="-25.719057418147" Y="-3.772712213751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.450526317547" Y="-3.311004651892" />
                  <Point X="-24.269238409421" Y="-3.245021249502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.189016269069" Y="-2.851852544018" />
                  <Point X="-22.963560967575" Y="-2.769793525117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.748310172704" Y="-2.327478408615" />
                  <Point X="-21.449020780436" Y="-2.218545978398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.414848399675" Y="-3.924862531913" />
                  <Point X="-25.689041310276" Y="-3.660690355553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.506542844337" Y="-3.230296111885" />
                  <Point X="-24.245377309421" Y="-3.13523963096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.282491058237" Y="-2.784777696544" />
                  <Point X="-22.980693260182" Y="-2.674932281285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.855709162893" Y="-2.265471555849" />
                  <Point X="-21.53838437452" Y="-2.149974778287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.496835626798" Y="-3.853606553791" />
                  <Point X="-25.659025202405" Y="-3.548668497354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.566956702944" Y="-3.15118806977" />
                  <Point X="-24.221516209421" Y="-3.025458012417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.382897743639" Y="-2.720225852967" />
                  <Point X="-22.997819644618" Y="-2.580068887056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.963108153082" Y="-2.203464703083" />
                  <Point X="-21.627747968604" Y="-2.081403578175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.547632706331" Y="-4.134968524609" />
                  <Point X="-27.456696410519" Y="-4.101870419719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.62258831401" Y="-3.79827990043" />
                  <Point X="-25.619575521847" Y="-3.433213099494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.66619363038" Y="-3.086210469112" />
                  <Point X="-24.165277281667" Y="-2.903891828323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.494673760625" Y="-2.659812107669" />
                  <Point X="-22.991605128095" Y="-2.476710099635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.070507143271" Y="-2.141457850317" />
                  <Point X="-21.717111562688" Y="-2.012832378063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.650462053381" Y="-4.071298457759" />
                  <Point X="-27.271217983609" Y="-3.93326490484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.855221400745" Y="-3.781854531122" />
                  <Point X="-25.528907956771" Y="-3.299115916208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.815678105901" Y="-3.039521480301" />
                  <Point X="-23.965253549211" Y="-2.729992255177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.78473339574" Y="-2.664288292628" />
                  <Point X="-22.937232400032" Y="-2.355823156679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.181830303235" Y="-2.080879278543" />
                  <Point X="-21.806475156771" Y="-1.944261177952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.752533246947" Y="-4.007352445608" />
                  <Point X="-25.431991006504" Y="-3.16274414273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.978771941741" Y="-2.997785893554" />
                  <Point X="-22.868805453984" Y="-2.229820896711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.356379124288" Y="-2.043312965448" />
                  <Point X="-21.895838750855" Y="-1.87568997784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.841689060256" Y="-3.938705619479" />
                  <Point X="-21.978966570109" Y="-1.804849141303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.930844873564" Y="-3.87005879335" />
                  <Point X="-22.03589013445" Y="-1.724470735966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.948424210657" Y="-3.775360260405" />
                  <Point X="-22.088560750857" Y="-1.642544384173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.469479552832" Y="-1.053247021232" />
                  <Point X="-20.247864205858" Y="-0.972585631477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.874527342029" Y="-3.647367111433" />
                  <Point X="-22.129519419776" Y="-1.55635523211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.673459076804" Y="-1.026392607973" />
                  <Point X="-20.228333027883" Y="-0.864379975669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.800630473401" Y="-3.519373962462" />
                  <Point X="-22.139776792939" Y="-1.458991722238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.877438600775" Y="-0.999538194713" />
                  <Point X="-20.244043992389" Y="-0.769001410716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.726733604774" Y="-3.391380813491" />
                  <Point X="-22.130151963061" Y="-1.354391682267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.081418124747" Y="-0.972683781453" />
                  <Point X="-20.404027787326" Y="-0.726133861652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.652836736146" Y="-3.26338766452" />
                  <Point X="-22.11305452529" Y="-1.247071835451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.285397648719" Y="-0.945829368193" />
                  <Point X="-20.564011582262" Y="-0.683266312589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.578939867518" Y="-3.135394515549" />
                  <Point X="-22.028948318982" Y="-1.115362791453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.48937717269" Y="-0.918974954933" />
                  <Point X="-20.723995377198" Y="-0.640398763525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.505042998891" Y="-3.007401366578" />
                  <Point X="-20.883979172135" Y="-0.597531214462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.431146130263" Y="-2.879408217607" />
                  <Point X="-21.043962967071" Y="-0.554663665398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.357249261636" Y="-2.751415068635" />
                  <Point X="-21.203946762007" Y="-0.511796116335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.31337806625" Y="-2.634350370988" />
                  <Point X="-21.354372893733" Y="-0.465449862354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.321191988101" Y="-2.53609751757" />
                  <Point X="-21.462415061347" Y="-0.403677107026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.821089882614" Y="-2.980918817226" />
                  <Point X="-28.572913921482" Y="-2.890590154513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.365968346854" Y="-2.451297890969" />
                  <Point X="-21.539793668703" Y="-0.330743728487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.881346667664" Y="-2.901753605011" />
                  <Point X="-28.099127073121" Y="-2.617048955938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.448737755109" Y="-2.380326603497" />
                  <Point X="-21.598681486534" Y="-0.251080252953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.941603452713" Y="-2.822588392797" />
                  <Point X="-21.63173324868" Y="-0.162013222179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.001860237763" Y="-2.743423180582" />
                  <Point X="-21.649090739031" Y="-0.067233943623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.051782341775" Y="-2.660496452089" />
                  <Point X="-21.643936447411" Y="0.035738953491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.101319979183" Y="-2.577429789196" />
                  <Point X="-21.618821610255" Y="0.145976895039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.150857616592" Y="-2.494363126304" />
                  <Point X="-21.531386032342" Y="0.2788977312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.671149880052" Y="0.591998085074" />
                  <Point X="-20.22256502146" Y="0.755269621143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.084478225765" Y="-2.369106115489" />
                  <Point X="-20.237461772707" Y="0.850944535488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.833839852744" Y="-2.176784319759" />
                  <Point X="-20.254970563918" Y="0.945668745034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.583201479723" Y="-1.984462524029" />
                  <Point X="-20.277578517082" Y="1.03853701141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.332563106703" Y="-1.7921407283" />
                  <Point X="-20.389413574813" Y="1.098929267634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.081924733682" Y="-1.59981893257" />
                  <Point X="-20.824579643646" Y="1.041638660001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.966449922437" Y="-1.456692650084" />
                  <Point X="-21.259745712479" Y="0.984348052368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.948837006753" Y="-1.349185184651" />
                  <Point X="-21.607638108849" Y="0.958822463747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.968829494882" Y="-1.255364966854" />
                  <Point X="-21.760679592802" Y="1.004216807366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.028125165982" Y="-1.17584993777" />
                  <Point X="-21.858991947391" Y="1.06953092502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.134315083337" Y="-1.113403018481" />
                  <Point X="-21.919990838559" Y="1.148426032697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.450291195503" Y="-1.127312029664" />
                  <Point X="-21.966795532766" Y="1.232487405567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.885457886865" Y="-1.184602863878" />
                  <Point X="-21.98966307244" Y="1.325261190179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.320624578227" Y="-1.241893698093" />
                  <Point X="-21.998077629963" Y="1.423295430092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.666069053038" Y="-1.266528316131" />
                  <Point X="-21.972353116186" Y="1.533755275783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.689696146082" Y="-1.174030986336" />
                  <Point X="-21.901017167194" Y="1.660816326234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.713323239126" Y="-1.081533656541" />
                  <Point X="-21.715442879593" Y="1.829456731551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.73695033217" Y="-0.989036326746" />
                  <Point X="-21.46480414496" Y="2.021778658897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.752288830817" Y="-0.893522195306" />
                  <Point X="-21.214165410328" Y="2.214100586243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.76603211297" Y="-0.797427452546" />
                  <Point X="-20.963526675696" Y="2.406422513588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.779775395123" Y="-0.701332709786" />
                  <Point X="-29.275022840687" Y="-0.517617804301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.333511264115" Y="-0.174935615212" />
                  <Point X="-20.893164723832" Y="2.533129058077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.296736449557" Y="-0.060453788957" />
                  <Point X="-22.67327665797" Y="1.986318188774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.031105122874" Y="2.220049512842" />
                  <Point X="-20.943299195723" Y="2.615978490983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.303777537032" Y="0.03808035317" />
                  <Point X="-22.796017064958" Y="2.042741222474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.557316149961" Y="2.493591484691" />
                  <Point X="-20.999035972235" Y="2.696788851764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.341517035741" Y="0.12544118737" />
                  <Point X="-22.88074769645" Y="2.112998683065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.083527177048" Y="2.76713345654" />
                  <Point X="-21.056681775059" Y="2.776904383791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.412846096808" Y="0.200576420688" />
                  <Point X="-22.937959118191" Y="2.193272316876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.535701864837" Y="0.256957466403" />
                  <Point X="-22.974270950181" Y="2.281152779266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.695685695845" Y="0.299825002337" />
                  <Point X="-22.986875615673" Y="2.377661944599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.855669526852" Y="0.342692538272" />
                  <Point X="-22.973979289966" Y="2.483452711673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.01565335786" Y="0.385560074207" />
                  <Point X="-22.934502758833" Y="2.598917882342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.175637188867" Y="0.428427610141" />
                  <Point X="-22.860606129796" Y="2.72691094411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.335621019875" Y="0.471295146076" />
                  <Point X="-22.786709500758" Y="2.854904005877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.495604850882" Y="0.514162682011" />
                  <Point X="-22.712812871721" Y="2.982897067645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.65558868189" Y="0.557030217945" />
                  <Point X="-22.638916242684" Y="3.110890129412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.782467772551" Y="0.611946893979" />
                  <Point X="-22.565019613646" Y="3.23888319118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.766656205537" Y="0.718798722114" />
                  <Point X="-22.491122984609" Y="3.366876252947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.750844638524" Y="0.825650550249" />
                  <Point X="-28.694552052423" Y="1.210109610266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.561997376983" Y="1.258355566539" />
                  <Point X="-22.417226355572" Y="3.494869314714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.73503307151" Y="0.932502378385" />
                  <Point X="-28.929802837709" Y="1.225582215219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.451778181328" Y="1.399568961387" />
                  <Point X="-22.343329726534" Y="3.622862376482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.708054038165" Y="1.043418831857" />
                  <Point X="-29.133782335774" Y="1.252436637908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.421725612685" Y="1.511604090222" />
                  <Point X="-22.269433097497" Y="3.750855438249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.677662168753" Y="1.155577456072" />
                  <Point X="-29.337761833839" Y="1.279291060597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.437021920037" Y="1.607133578037" />
                  <Point X="-22.195536468459" Y="3.878848500017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.64727029934" Y="1.267736080287" />
                  <Point X="-29.541741331904" Y="1.306145483286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.479070281445" Y="1.69292611447" />
                  <Point X="-22.259305153486" Y="3.956735485174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.544957144214" Y="1.770042145978" />
                  <Point X="-22.353338296504" Y="4.023607108466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.633950564663" Y="1.838748078274" />
                  <Point X="-22.457801036216" Y="4.086682669006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.723314185602" Y="1.907319268611" />
                  <Point X="-22.564604209273" Y="4.148906381473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.812677806541" Y="1.975890458948" />
                  <Point X="-22.67140738233" Y="4.211130093941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.90204142748" Y="2.044461649286" />
                  <Point X="-22.782171954582" Y="4.271911975015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.99140504842" Y="2.113032839623" />
                  <Point X="-22.903735618897" Y="4.328763308021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.080768669359" Y="2.18160402996" />
                  <Point X="-23.025299283212" Y="4.385614641028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.170132290298" Y="2.250175220297" />
                  <Point X="-23.147603161179" Y="4.442196558297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.200881517255" Y="2.340080305343" />
                  <Point X="-28.131959904461" Y="2.729135955164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.834585072348" Y="2.837371542473" />
                  <Point X="-23.286725643782" Y="4.492657004098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.125955225866" Y="2.468448133558" />
                  <Point X="-28.278970982817" Y="2.77672518692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.755442774286" Y="2.967273871624" />
                  <Point X="-23.425848126386" Y="4.543117449898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.051028934476" Y="2.596815961773" />
                  <Point X="-28.386370189791" Y="2.838731960783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.75103248568" Y="3.069975973787" />
                  <Point X="-24.948196649719" Y="4.090124789611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.878641461249" Y="4.115440807853" />
                  <Point X="-23.568689069163" Y="4.592224486878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.963938366319" Y="2.729611224653" />
                  <Point X="-28.493769396765" Y="2.900738734646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.763346226908" Y="3.166591026892" />
                  <Point X="-25.126453893938" Y="4.126341347058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.763680626673" Y="4.25838001813" />
                  <Point X="-23.735680860006" Y="4.63254133403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.854217233506" Y="2.870643339452" />
                  <Point X="-28.601168603739" Y="2.962745508509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.801766997127" Y="3.25370389854" />
                  <Point X="-25.199148275109" Y="4.200979644499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.732817042108" Y="4.37071033262" />
                  <Point X="-23.902672650849" Y="4.672858181181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.744496100692" Y="3.011675454251" />
                  <Point X="-28.708567810713" Y="3.024752282372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.849999721041" Y="3.337245511103" />
                  <Point X="-25.233162169594" Y="4.28969648774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.702800833738" Y="4.482732227397" />
                  <Point X="-24.069664441692" Y="4.713175028333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.898232444954" Y="3.420787123666" />
                  <Point X="-25.25784373117" Y="4.381810022376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.672784625368" Y="4.594754122174" />
                  <Point X="-24.255478095939" Y="4.746641277452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.946465168868" Y="3.504328736229" />
                  <Point X="-25.282525292745" Y="4.473923557013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.642768416999" Y="4.706776016952" />
                  <Point X="-24.471175008723" Y="4.76923090996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.994697892782" Y="3.587870348792" />
                  <Point X="-25.307206854321" Y="4.566037091649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.042930616695" Y="3.671411961356" />
                  <Point X="-26.96175519038" Y="4.064927634554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.507550612273" Y="4.230244581253" />
                  <Point X="-25.331888415897" Y="4.658150626286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.893999760286" Y="3.826715248437" />
                  <Point X="-27.070940179307" Y="4.126284436941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.471417716512" Y="4.344492768173" />
                  <Point X="-25.356569977473" Y="4.750264160922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.637019472006" Y="4.02134531255" />
                  <Point X="-27.14144000317" Y="4.20172148792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.46420003856" Y="4.448216676493" />
                  <Point X="-25.631102406801" Y="4.751439416691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.476900709273" Y="4.544690898783" />
                  <Point X="-26.254105957506" Y="4.625781556778" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.0001640625" Y="0.001626220703" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.535677734375" Y="-3.521547119141" />
                  <Point X="-24.69691015625" Y="-3.289243652344" />
                  <Point X="-24.69940625" Y="-3.285646728516" />
                  <Point X="-24.7256640625" Y="-3.266399658203" />
                  <Point X="-24.97516015625" Y="-3.188965332031" />
                  <Point X="-24.9790234375" Y="-3.187766357422" />
                  <Point X="-25.008666015625" Y="-3.187767333984" />
                  <Point X="-25.258162109375" Y="-3.265201416016" />
                  <Point X="-25.262025390625" Y="-3.266400634766" />
                  <Point X="-25.28828125" Y="-3.285647949219" />
                  <Point X="-25.44951171875" Y="-3.517951171875" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.465333984375" Y="-3.559907714844" />
                  <Point X="-25.847744140625" Y="-4.987077148438" />
                  <Point X="-26.09714453125" Y="-4.938667480469" />
                  <Point X="-26.10023828125" Y="-4.938066894531" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.3091484375" Y="-4.551038574219" />
                  <Point X="-26.309681640625" Y="-4.534757324219" />
                  <Point X="-26.369287109375" Y="-4.23510546875" />
                  <Point X="-26.370212890625" Y="-4.230456542969" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.615986328125" Y="-4.001183349609" />
                  <Point X="-26.61954296875" Y="-3.998064208984" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.954109375" Y="-3.965780761719" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791015625" />
                  <Point X="-27.243912109375" Y="-4.143530761719" />
                  <Point X="-27.247853515625" Y="-4.146165527344" />
                  <Point X="-27.263162109375" Y="-4.161763183594" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.851296875" Y="-4.170419433594" />
                  <Point X="-27.855830078125" Y="-4.167612304688" />
                  <Point X="-28.228580078125" Y="-3.880607421875" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597593261719" />
                  <Point X="-27.513978515625" Y="-2.568764404297" />
                  <Point X="-27.5310625" Y="-2.551680419922" />
                  <Point X="-27.53126171875" Y="-2.551480957031" />
                  <Point X="-27.560109375" Y="-2.537204833984" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-27.61134375" Y="-2.554820800781" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-29.158119140625" Y="-2.851836914062" />
                  <Point X="-29.161703125" Y="-2.847128173828" />
                  <Point X="-29.43101953125" Y="-2.395527099609" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.145822265625" Y="-1.396014892578" />
                  <Point X="-28.138234375" Y="-1.366721069336" />
                  <Point X="-28.138109375" Y="-1.366236206055" />
                  <Point X="-28.14033203125" Y="-1.334581665039" />
                  <Point X="-28.161158203125" Y="-1.310639526367" />
                  <Point X="-28.187236328125" Y="-1.295290527344" />
                  <Point X="-28.187662109375" Y="-1.295039550781" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-28.24434765625" Y="-1.291838623047" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.925990234375" Y="-1.016680786133" />
                  <Point X="-29.927392578125" Y="-1.011191772461" />
                  <Point X="-29.998396484375" Y="-0.514742553711" />
                  <Point X="-28.557462890625" Y="-0.128645339966" />
                  <Point X="-28.54189453125" Y="-0.121425285339" />
                  <Point X="-28.514564453125" Y="-0.102456840515" />
                  <Point X="-28.514171875" Y="-0.102183486938" />
                  <Point X="-28.494896484375" Y="-0.075906921387" />
                  <Point X="-28.485787109375" Y="-0.0465545578" />
                  <Point X="-28.485634765625" Y="-0.046059268951" />
                  <Point X="-28.485646484375" Y="-0.01646002388" />
                  <Point X="-28.494755859375" Y="0.012892336845" />
                  <Point X="-28.494884765625" Y="0.013306093216" />
                  <Point X="-28.514140625" Y="0.039603046417" />
                  <Point X="-28.541470703125" Y="0.058571491241" />
                  <Point X="-28.557462890625" Y="0.066085273743" />
                  <Point X="-28.5800859375" Y="0.072147567749" />
                  <Point X="-29.998185546875" Y="0.452126098633" />
                  <Point X="-29.918546875" Y="0.9903203125" />
                  <Point X="-29.91764453125" Y="0.996415039063" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866699219" />
                  <Point X="-28.671228515625" Y="1.414934570312" />
                  <Point X="-28.670291015625" Y="1.415229736328" />
                  <Point X="-28.6515390625" Y="1.426051269531" />
                  <Point X="-28.639119140625" Y="1.443786010742" />
                  <Point X="-28.61484765625" Y="1.50238293457" />
                  <Point X="-28.61445703125" Y="1.503326538086" />
                  <Point X="-28.61071484375" Y="1.524624511719" />
                  <Point X="-28.616314453125" Y="1.545510986328" />
                  <Point X="-28.645599609375" Y="1.60176965332" />
                  <Point X="-28.64608984375" Y="1.602706054688" />
                  <Point X="-28.65996875" Y="1.619221801758" />
                  <Point X="-28.6729453125" Y="1.62917980957" />
                  <Point X="-29.47610546875" Y="2.245466308594" />
                  <Point X="-29.163525390625" Y="2.780992919922" />
                  <Point X="-29.1600078125" Y="2.787013183594" />
                  <Point X="-28.774671875" Y="3.282311035156" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138515625" Y="2.920435058594" />
                  <Point X="-28.05426953125" Y="2.913064453125" />
                  <Point X="-28.052966796875" Y="2.912950439453" />
                  <Point X="-28.0315078125" Y="2.915775146484" />
                  <Point X="-28.013251953125" Y="2.927404785156" />
                  <Point X="-27.953453125" Y="2.987202392578" />
                  <Point X="-27.95252734375" Y="2.988128173828" />
                  <Point X="-27.9408984375" Y="3.006382568359" />
                  <Point X="-27.938072265625" Y="3.027841552734" />
                  <Point X="-27.945443359375" Y="3.112086669922" />
                  <Point X="-27.9455546875" Y="3.113358886719" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-27.957818359375" Y="3.143993408203" />
                  <Point X="-28.30727734375" Y="3.749276367188" />
                  <Point X="-27.7589765625" Y="4.169652832031" />
                  <Point X="-27.752873046875" Y="4.174333007812" />
                  <Point X="-27.141546875" Y="4.513971191406" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.857482421875" Y="4.224849609375" />
                  <Point X="-26.856015625" Y="4.224086425781" />
                  <Point X="-26.835115234375" Y="4.218491699219" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.71616015625" Y="4.262697753906" />
                  <Point X="-26.714646484375" Y="4.26332421875" />
                  <Point X="-26.696908203125" Y="4.275738769531" />
                  <Point X="-26.68608203125" Y="4.294488769531" />
                  <Point X="-26.654294921875" Y="4.395304199219" />
                  <Point X="-26.653802734375" Y="4.396865234375" />
                  <Point X="-26.651916015625" Y="4.418426757812" />
                  <Point X="-26.6525703125" Y="4.423393066406" />
                  <Point X="-26.68913671875" Y="4.701141113281" />
                  <Point X="-25.975998046875" Y="4.901080078125" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.22419921875" Y="4.990358398438" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.0353046875" Y="4.295891113281" />
                  <Point X="-25.01011328125" Y="4.276560546875" />
                  <Point X="-24.97757421875" Y="4.276560546875" />
                  <Point X="-24.9523828125" Y="4.295891113281" />
                  <Point X="-24.942599609375" Y="4.321900390625" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.14669921875" Y="4.926288574219" />
                  <Point X="-24.13979296875" Y="4.925564941406" />
                  <Point X="-23.498814453125" Y="4.770813476562" />
                  <Point X="-23.491541015625" Y="4.769057128906" />
                  <Point X="-23.073376953125" Y="4.617386230469" />
                  <Point X="-23.068966796875" Y="4.615786621094" />
                  <Point X="-22.665537109375" Y="4.427115722656" />
                  <Point X="-22.66130078125" Y="4.425134765625" />
                  <Point X="-22.271580078125" Y="4.198082519531" />
                  <Point X="-22.267474609375" Y="4.195690917969" />
                  <Point X="-21.9312578125" Y="3.956593261719" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491514892578" />
                  <Point X="-22.7962890625" Y="2.412452880859" />
                  <Point X="-22.7962890625" Y="2.412452636719" />
                  <Point X="-22.797955078125" Y="2.392325439453" />
                  <Point X="-22.78971484375" Y="2.323991699219" />
                  <Point X="-22.789587890625" Y="2.322933105469" />
                  <Point X="-22.781318359375" Y="2.3008125" />
                  <Point X="-22.739015625" Y="2.238469238281" />
                  <Point X="-22.72505859375" Y="2.224204101562" />
                  <Point X="-22.662716796875" Y="2.181901367187" />
                  <Point X="-22.66171484375" Y="2.181221923828" />
                  <Point X="-22.6396640625" Y="2.17298046875" />
                  <Point X="-22.571298828125" Y="2.164736572266" />
                  <Point X="-22.551337890625" Y="2.165945556641" />
                  <Point X="-22.472275390625" Y="2.187087890625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-22.436208984375" Y="2.205555664062" />
                  <Point X="-21.00575" Y="3.031431152344" />
                  <Point X="-20.79983984375" Y="2.745262207031" />
                  <Point X="-20.79741015625" Y="2.741885986328" />
                  <Point X="-20.612486328125" Y="2.436294921875" />
                  <Point X="-21.7113828125" Y="1.593082763672" />
                  <Point X="-21.72062890625" Y="1.583833007812" />
                  <Point X="-21.777529296875" Y="1.509601074219" />
                  <Point X="-21.77753125" Y="1.509596923828" />
                  <Point X="-21.78687890625" Y="1.491499755859" />
                  <Point X="-21.8080546875" Y="1.41578125" />
                  <Point X="-21.808384765625" Y="1.414608032227" />
                  <Point X="-21.80921875" Y="1.390960693359" />
                  <Point X="-21.791818359375" Y="1.306633544922" />
                  <Point X="-21.784353515625" Y="1.287955322266" />
                  <Point X="-21.73703515625" Y="1.216032470703" />
                  <Point X="-21.736302734375" Y="1.214918579102" />
                  <Point X="-21.719052734375" Y="1.198819946289" />
                  <Point X="-21.65047265625" Y="1.160215209961" />
                  <Point X="-21.650458984375" Y="1.160207519531" />
                  <Point X="-21.63143359375" Y="1.153619506836" />
                  <Point X="-21.538708984375" Y="1.141364868164" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-21.502580078125" Y="1.144017700195" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.061857421875" Y="0.955681152344" />
                  <Point X="-20.06080859375" Y="0.951371887207" />
                  <Point X="-20.002138671875" Y="0.574556274414" />
                  <Point X="-21.25883203125" Y="0.237826843262" />
                  <Point X="-21.270912109375" Y="0.232819335938" />
                  <Point X="-21.36201171875" Y="0.180162307739" />
                  <Point X="-21.362017578125" Y="0.180158798218" />
                  <Point X="-21.377734375" Y="0.166926818848" />
                  <Point X="-21.43239453125" Y="0.097277633667" />
                  <Point X="-21.433251953125" Y="0.096184066772" />
                  <Point X="-21.443013671875" Y="0.074735519409" />
                  <Point X="-21.461234375" Y="-0.020401813507" />
                  <Point X="-21.461240234375" Y="-0.020430257797" />
                  <Point X="-21.461515625" Y="-0.040685150146" />
                  <Point X="-21.44330078125" Y="-0.1357940979" />
                  <Point X="-21.44301953125" Y="-0.137267288208" />
                  <Point X="-21.433240234375" Y="-0.158759185791" />
                  <Point X="-21.378591796875" Y="-0.228393447876" />
                  <Point X="-21.378591796875" Y="-0.228393463135" />
                  <Point X="-21.363421875" Y="-0.241907012939" />
                  <Point X="-21.272322265625" Y="-0.294564056396" />
                  <Point X="-21.25883203125" Y="-0.300386901855" />
                  <Point X="-21.2390078125" Y="-0.305698669434" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.050984375" Y="-0.962526611328" />
                  <Point X="-20.051568359375" Y="-0.966403686523" />
                  <Point X="-20.125453125" Y="-1.290178466797" />
                  <Point X="-21.588017578125" Y="-1.097628295898" />
                  <Point X="-21.605166015625" Y="-1.098341796875" />
                  <Point X="-21.783962890625" Y="-1.137203857422" />
                  <Point X="-21.78673046875" Y="-1.137805541992" />
                  <Point X="-21.8145546875" Y="-1.154697387695" />
                  <Point X="-21.922625" Y="-1.284672973633" />
                  <Point X="-21.92429296875" Y="-1.286679077148" />
                  <Point X="-21.935640625" Y="-1.314070922852" />
                  <Point X="-21.95112890625" Y="-1.482386962891" />
                  <Point X="-21.951369140625" Y="-1.484993286133" />
                  <Point X="-21.943638671875" Y="-1.516621826172" />
                  <Point X="-21.84469140625" Y="-1.670529174805" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-21.813140625" Y="-1.699656616211" />
                  <Point X="-20.66092578125" Y="-2.583783447266" />
                  <Point X="-20.794224609375" Y="-2.799480957031" />
                  <Point X="-20.795873046875" Y="-2.802148925781" />
                  <Point X="-20.943310546875" Y="-3.011637695312" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.26266015625" Y="-2.2533125" />
                  <Point X="-22.475455078125" Y="-2.214881835938" />
                  <Point X="-22.47875" Y="-2.214286865234" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.687703125" Y="-2.312283203125" />
                  <Point X="-22.6904453125" Y="-2.313727539062" />
                  <Point X="-22.7113984375" Y="-2.33468359375" />
                  <Point X="-22.8044375" Y="-2.511464355469" />
                  <Point X="-22.805873046875" Y="-2.514191894531" />
                  <Point X="-22.8108359375" Y="-2.546374755859" />
                  <Point X="-22.772404296875" Y="-2.759170166016" />
                  <Point X="-22.772404296875" Y="-2.759179199219" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.7540859375" Y="-2.799053955078" />
                  <Point X="-22.01332421875" Y="-4.082087890625" />
                  <Point X="-22.16274609375" Y="-4.188815917969" />
                  <Point X="-22.1646953125" Y="-4.190208496094" />
                  <Point X="-22.320224609375" Y="-4.290879394531" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980466796875" />
                  <Point X="-23.53932421875" Y="-2.845537353516" />
                  <Point X="-23.54257421875" Y="-2.843447998047" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.8037265625" Y="-2.856838867188" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509765625" />
                  <Point X="-24.01190625" Y="-3.015878417969" />
                  <Point X="-24.014650390625" Y="-3.01816015625" />
                  <Point X="-24.03154296875" Y="-3.045986572266" />
                  <Point X="-24.08453125" Y="-3.289778320312" />
                  <Point X="-24.08453125" Y="-3.289778076172" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-24.08271875" Y="-3.336166748047" />
                  <Point X="-23.872357421875" Y="-4.934028320312" />
                  <Point X="-24.003810546875" Y="-4.962842773438" />
                  <Point X="-24.005666015625" Y="-4.963249511719" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#216" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.190297965249" Y="5.0653261847" Z="2.5" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.5" />
                  <Point X="-0.205301466071" Y="5.074911947802" Z="2.5" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.5" />
                  <Point X="-0.995652588278" Y="4.980503814645" Z="2.5" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.5" />
                  <Point X="-1.7082996835" Y="4.448146597217" Z="2.5" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.5" />
                  <Point X="-1.708138087686" Y="4.441619523077" Z="2.5" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.5" />
                  <Point X="-1.741437351441" Y="4.340177718811" Z="2.5" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.5" />
                  <Point X="-1.840550907092" Y="4.300480373425" Z="2.5" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.5" />
                  <Point X="-2.131240526435" Y="4.605929519272" Z="2.5" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.5" />
                  <Point X="-2.144235126693" Y="4.604377896997" Z="2.5" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.5" />
                  <Point X="-2.795559906076" Y="4.240609982665" Z="2.5" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.5" />
                  <Point X="-3.007275393598" Y="3.150273425835" Z="2.5" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.5" />
                  <Point X="-3.001410560372" Y="3.139008452092" Z="2.5" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.5" />
                  <Point X="-2.994965925961" Y="3.053837641926" Z="2.5" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.5" />
                  <Point X="-3.056068013515" Y="2.994154138801" Z="2.5" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.5" />
                  <Point X="-3.783585922462" Y="3.372918555147" Z="2.5" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.5" />
                  <Point X="-3.799861084417" Y="3.370552673632" Z="2.5" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.5" />
                  <Point X="-4.212859457823" Y="2.837483301517" Z="2.5" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.5" />
                  <Point X="-3.709539753124" Y="1.62079207813" Z="2.5" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.5" />
                  <Point X="-3.696108816359" Y="1.609963006463" Z="2.5" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.5" />
                  <Point X="-3.667198258572" Y="1.552797058877" Z="2.5" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.5" />
                  <Point X="-3.692406534916" Y="1.493904793318" Z="2.5" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.5" />
                  <Point X="-4.800278107478" Y="1.612723037554" Z="2.5" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.5" />
                  <Point X="-4.818879692203" Y="1.606061209653" Z="2.5" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.5" />
                  <Point X="-4.97416491715" Y="1.028918518768" Z="2.5" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.5" />
                  <Point X="-3.59918522931" Y="0.055131246957" Z="2.5" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.5" />
                  <Point X="-3.576137559381" Y="0.04877532354" Z="2.5" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.5" />
                  <Point X="-3.548666798223" Y="0.029352454173" Z="2.5" />
                  <Point X="-3.539556741714" Y="0" Z="2.5" />
                  <Point X="-3.539697802912" Y="-0.000454496888" Z="2.5" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.5" />
                  <Point X="-3.549231035742" Y="-0.030100659594" Z="2.5" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.5" />
                  <Point X="-5.037702559463" Y="-0.440580809799" Z="2.5" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.5" />
                  <Point X="-5.059142810836" Y="-0.454923121477" Z="2.5" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.5" />
                  <Point X="-4.980591677739" Y="-0.997774967371" Z="2.5" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.5" />
                  <Point X="-3.243978762234" Y="-1.310130905777" Z="2.5" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.5" />
                  <Point X="-3.218755081009" Y="-1.307100971095" Z="2.5" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.5" />
                  <Point X="-3.192793784061" Y="-1.3229033917" Z="2.5" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.5" />
                  <Point X="-4.483039756779" Y="-2.336415960968" Z="2.5" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.5" />
                  <Point X="-4.498424610343" Y="-2.359161273806" Z="2.5" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.5" />
                  <Point X="-4.204062047001" Y="-2.850840780243" Z="2.5" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.5" />
                  <Point X="-2.5925002819" Y="-2.566842129031" Z="2.5" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.5" />
                  <Point X="-2.572574967876" Y="-2.555755501255" Z="2.5" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.5" />
                  <Point X="-3.288574884137" Y="-3.842578100592" Z="2.5" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.5" />
                  <Point X="-3.293682734016" Y="-3.867046023603" Z="2.5" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.5" />
                  <Point X="-2.88377617712" Y="-4.181650216827" Z="2.5" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.5" />
                  <Point X="-2.229651486684" Y="-4.160921210869" Z="2.5" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.5" />
                  <Point X="-2.222288802539" Y="-4.153823911689" Z="2.5" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.5" />
                  <Point X="-1.963535376207" Y="-3.984393773903" Z="2.5" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.5" />
                  <Point X="-1.655110083492" Y="-4.007495138604" Z="2.5" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.5" />
                  <Point X="-1.42448305568" Y="-4.213580351293" Z="2.5" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.5" />
                  <Point X="-1.412363783299" Y="-4.873918343503" Z="2.5" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.5" />
                  <Point X="-1.408590254488" Y="-4.880663324148" Z="2.5" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.5" />
                  <Point X="-1.112831492951" Y="-4.956470692524" Z="2.5" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.5" />
                  <Point X="-0.423194850433" Y="-3.541568709474" Z="2.5" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.5" />
                  <Point X="-0.414590257354" Y="-3.515176065645" Z="2.5" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.5" />
                  <Point X="-0.249495855884" Y="-3.281673674848" Z="2.5" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.5" />
                  <Point X="0.003863223476" Y="-3.20543837044" Z="2.5" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.5" />
                  <Point X="0.255855602078" Y="-3.286469690123" Z="2.5" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.5" />
                  <Point X="0.811560009968" Y="-4.990967320342" Z="2.5" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.5" />
                  <Point X="0.820417939353" Y="-5.013263401872" Z="2.5" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.5" />
                  <Point X="1.000740206392" Y="-4.980402952187" Z="2.5" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.5" />
                  <Point X="0.960695882631" Y="-3.298360218998" Z="2.5" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.5" />
                  <Point X="0.958166344025" Y="-3.269138473286" Z="2.5" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.5" />
                  <Point X="1.013904411061" Y="-3.023044139116" Z="2.5" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.5" />
                  <Point X="1.194697861121" Y="-2.875348387402" Z="2.5" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.5" />
                  <Point X="1.427480166139" Y="-2.856315852334" Z="2.5" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.5" />
                  <Point X="2.646422138372" Y="-4.306288336414" Z="2.5" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.5" />
                  <Point X="2.665023494066" Y="-4.324723779245" Z="2.5" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.5" />
                  <Point X="2.860159098462" Y="-4.198222944594" Z="2.5" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.5" />
                  <Point X="2.283058313457" Y="-2.742773908323" Z="2.5" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.5" />
                  <Point X="2.270641830715" Y="-2.719003671479" Z="2.5" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.5" />
                  <Point X="2.233651731628" Y="-2.503470888155" Z="2.5" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.5" />
                  <Point X="2.329427490217" Y="-2.325249578339" Z="2.5" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.5" />
                  <Point X="2.509503132034" Y="-2.232806178776" Z="2.5" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.5" />
                  <Point X="4.044639688783" Y="-3.034691186818" Z="2.5" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.5" />
                  <Point X="4.067777389652" Y="-3.04272967923" Z="2.5" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.5" />
                  <Point X="4.242154101566" Y="-2.794484614797" Z="2.5" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.5" />
                  <Point X="3.211139034035" Y="-1.628708938924" Z="2.5" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.5" />
                  <Point X="3.191210698985" Y="-1.612209908391" Z="2.5" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.5" />
                  <Point X="3.092502341785" Y="-1.455696150305" Z="2.5" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.5" />
                  <Point X="3.109664836429" Y="-1.285359675513" Z="2.5" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.5" />
                  <Point X="3.220504024901" Y="-1.154782438881" Z="2.5" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.5" />
                  <Point X="4.884017200855" Y="-1.311387155841" Z="2.5" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.5" />
                  <Point X="4.908294155754" Y="-1.308772156914" Z="2.5" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.5" />
                  <Point X="4.992301071731" Y="-0.938700788063" Z="2.5" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.5" />
                  <Point X="3.76777607314" Y="-0.226122293549" Z="2.5" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.5" />
                  <Point X="3.746542105864" Y="-0.219995288379" Z="2.5" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.5" />
                  <Point X="3.654596358413" Y="-0.166259750576" Z="2.5" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.5" />
                  <Point X="3.59965460495" Y="-0.095137412217" Z="2.5" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.5" />
                  <Point X="3.581716845474" Y="0.001473118956" Z="2.5" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.5" />
                  <Point X="3.600783079987" Y="0.097688987999" Z="2.5" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.5" />
                  <Point X="3.656853308488" Y="0.168153555863" Z="2.5" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.5" />
                  <Point X="5.028191744166" Y="0.56384963922" Z="2.5" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.5" />
                  <Point X="5.047010239293" Y="0.57561546551" Z="2.5" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.5" />
                  <Point X="4.980567842339" Y="0.998787150061" Z="2.5" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.5" />
                  <Point X="3.48473797933" Y="1.224869958804" Z="2.5" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.5" />
                  <Point X="3.461685648805" Y="1.222213837023" Z="2.5" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.5" />
                  <Point X="3.367898715572" Y="1.235066338931" Z="2.5" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.5" />
                  <Point X="3.298585769598" Y="1.274784828066" Z="2.5" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.5" />
                  <Point X="3.25099157923" Y="1.348022134343" Z="2.5" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.5" />
                  <Point X="3.233920230973" Y="1.433522760962" Z="2.5" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.5" />
                  <Point X="3.255997049412" Y="1.510463119403" Z="2.5" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.5" />
                  <Point X="4.430014318854" Y="2.441888449425" Z="2.5" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.5" />
                  <Point X="4.444123092444" Y="2.460430835126" Z="2.5" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.5" />
                  <Point X="4.234586620218" Y="2.805746901938" Z="2.5" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.5" />
                  <Point X="2.532633804763" Y="2.28013688321" Z="2.5" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.5" />
                  <Point X="2.508653713849" Y="2.266671399139" Z="2.5" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.5" />
                  <Point X="2.428533064189" Y="2.245656821337" Z="2.5" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.5" />
                  <Point X="2.359201455744" Y="2.254555545308" Z="2.5" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.5" />
                  <Point X="2.296203054927" Y="2.297823404638" Z="2.5" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.5" />
                  <Point X="2.253772881421" Y="2.361225379643" Z="2.5" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.5" />
                  <Point X="2.245856543518" Y="2.430815862033" Z="2.5" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.5" />
                  <Point X="3.115488469649" Y="3.979505277192" Z="2.5" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.5" />
                  <Point X="3.122906615617" Y="4.006328888027" Z="2.5" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.5" />
                  <Point X="2.747433167234" Y="4.272565293294" Z="2.5" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.5" />
                  <Point X="2.349484631596" Y="4.503689454734" Z="2.5" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.5" />
                  <Point X="1.937515857713" Y="4.695669622845" Z="2.5" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.5" />
                  <Point X="1.506762649233" Y="4.850697268768" Z="2.5" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.5" />
                  <Point X="0.852352950287" Y="5.007296780804" Z="2.5" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.5" />
                  <Point X="0.002946596304" Y="4.366121321251" Z="2.5" />
                  <Point X="0" Y="4.355124473572" Z="2.5" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>