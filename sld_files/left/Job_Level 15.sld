<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#145" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1208" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000474609375" Y="0.004715820312" />
                  <Width Value="9.78389453125" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.326099609375" Y="-3.925276855469" />
                  <Point X="-24.4366953125" Y="-3.512524414063" />
                  <Point X="-24.44227734375" Y="-3.497139648438" />
                  <Point X="-24.45763671875" Y="-3.467376220703" />
                  <Point X="-24.502841796875" Y="-3.402246337891" />
                  <Point X="-24.621365234375" Y="-3.231475830078" />
                  <Point X="-24.643248046875" Y="-3.209021484375" />
                  <Point X="-24.66950390625" Y="-3.18977734375" />
                  <Point X="-24.69750390625" Y="-3.175669433594" />
                  <Point X="-24.767453125" Y="-3.153959472656" />
                  <Point X="-24.95086328125" Y="-3.097036132812" />
                  <Point X="-24.9790234375" Y="-3.092766601562" />
                  <Point X="-25.0086640625" Y="-3.092766601562" />
                  <Point X="-25.03682421875" Y="-3.097035888672" />
                  <Point X="-25.1067734375" Y="-3.118745605469" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.31818359375" Y="-3.18977734375" />
                  <Point X="-25.344439453125" Y="-3.209021484375" />
                  <Point X="-25.36632421875" Y="-3.231477294922" />
                  <Point X="-25.41152734375" Y="-3.296606933594" />
                  <Point X="-25.53005078125" Y="-3.467377685547" />
                  <Point X="-25.5381875" Y="-3.481571289062" />
                  <Point X="-25.550990234375" Y="-3.512524169922" />
                  <Point X="-25.840974609375" Y="-4.59476171875" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.079333984375" Y="-4.845351074219" />
                  <Point X="-26.15675390625" Y="-4.825432128906" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.228275390625" Y="-4.6645625" />
                  <Point X="-26.2149609375" Y="-4.563438476562" />
                  <Point X="-26.21419921875" Y="-4.5479296875" />
                  <Point X="-26.2165078125" Y="-4.516222167969" />
                  <Point X="-26.233220703125" Y="-4.432209960938" />
                  <Point X="-26.277037109375" Y="-4.211930664062" />
                  <Point X="-26.287939453125" Y="-4.182965332031" />
                  <Point X="-26.30401171875" Y="-4.155127929687" />
                  <Point X="-26.32364453125" Y="-4.131204589844" />
                  <Point X="-26.388044921875" Y="-4.074726074219" />
                  <Point X="-26.556904296875" Y="-3.926639648438" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.728501953125" Y="-3.885364013672" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.98341796875" Y="-3.873708496094" />
                  <Point X="-27.014466796875" Y="-3.882028076172" />
                  <Point X="-27.042658203125" Y="-3.894802001953" />
                  <Point X="-27.11387890625" Y="-3.942391113281" />
                  <Point X="-27.300623046875" Y="-4.067170166016" />
                  <Point X="-27.31278515625" Y="-4.076822998047" />
                  <Point X="-27.3351015625" Y="-4.099461914062" />
                  <Point X="-27.4801484375" Y="-4.288489257813" />
                  <Point X="-27.4899296875" Y="-4.282432617188" />
                  <Point X="-27.80171484375" Y="-4.0893828125" />
                  <Point X="-27.90890234375" Y="-4.0068515625" />
                  <Point X="-28.10472265625" Y="-3.856077148438" />
                  <Point X="-27.62929296875" Y="-3.032610595703" />
                  <Point X="-27.42376171875" Y="-2.676619628906" />
                  <Point X="-27.412859375" Y="-2.647654785156" />
                  <Point X="-27.406587890625" Y="-2.616128662109" />
                  <Point X="-27.40557421875" Y="-2.585194335938" />
                  <Point X="-27.41455859375" Y="-2.555576171875" />
                  <Point X="-27.428775390625" Y="-2.526747314453" />
                  <Point X="-27.44680078125" Y="-2.501591308594" />
                  <Point X="-27.4641484375" Y="-2.484242675781" />
                  <Point X="-27.48930859375" Y="-2.466212890625" />
                  <Point X="-27.518138671875" Y="-2.451995605469" />
                  <Point X="-27.547755859375" Y="-2.443011474609" />
                  <Point X="-27.578689453125" Y="-2.444024169922" />
                  <Point X="-27.61021484375" Y="-2.450295166016" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-28.572591796875" Y="-3.000100830078" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-28.8365390625" Y="-3.117476318359" />
                  <Point X="-29.082861328125" Y="-2.793859130859" />
                  <Point X="-29.15970703125" Y="-2.664998535156" />
                  <Point X="-29.306142578125" Y="-2.419449951172" />
                  <Point X="-28.465568359375" Y="-1.774454467773" />
                  <Point X="-28.105955078125" Y="-1.498513305664" />
                  <Point X="-28.08306640625" Y="-1.475882446289" />
                  <Point X="-28.06372265625" Y="-1.444805908203" />
                  <Point X="-28.0548828125" Y="-1.421525878906" />
                  <Point X="-28.05173046875" Y="-1.411624145508" />
                  <Point X="-28.04615234375" Y="-1.390089599609" />
                  <Point X="-28.042037109375" Y="-1.358610351562" />
                  <Point X="-28.042734375" Y="-1.335737792969" />
                  <Point X="-28.0488828125" Y="-1.313695922852" />
                  <Point X="-28.060888671875" Y="-1.284712280273" />
                  <Point X="-28.0737265625" Y="-1.262668457031" />
                  <Point X="-28.09196875" Y="-1.244835693359" />
                  <Point X="-28.11178125" Y="-1.230102661133" />
                  <Point X="-28.120283203125" Y="-1.224462036133" />
                  <Point X="-28.139455078125" Y="-1.213178833008" />
                  <Point X="-28.16871875" Y="-1.201955566406" />
                  <Point X="-28.20060546875" Y="-1.195474609375" />
                  <Point X="-28.231927734375" Y="-1.194383911133" />
                  <Point X="-29.410267578125" Y="-1.349515136719" />
                  <Point X="-29.732099609375" Y="-1.391885375977" />
                  <Point X="-29.73773828125" Y="-1.369814819336" />
                  <Point X="-29.834078125" Y="-0.99264855957" />
                  <Point X="-29.854408203125" Y="-0.850496887207" />
                  <Point X="-29.892421875" Y="-0.584698364258" />
                  <Point X="-28.942529296875" Y="-0.330174713135" />
                  <Point X="-28.532875" Y="-0.220408279419" />
                  <Point X="-28.51233984375" Y="-0.212245132446" />
                  <Point X="-28.489111328125" Y="-0.199707565308" />
                  <Point X="-28.480068359375" Y="-0.194153549194" />
                  <Point X="-28.4599765625" Y="-0.180209625244" />
                  <Point X="-28.436017578125" Y="-0.158679229736" />
                  <Point X="-28.41518359375" Y="-0.128485855103" />
                  <Point X="-28.40520703125" Y="-0.105513748169" />
                  <Point X="-28.401615234375" Y="-0.095834228516" />
                  <Point X="-28.394919921875" Y="-0.07426512146" />
                  <Point X="-28.389474609375" Y="-0.045534244537" />
                  <Point X="-28.39039453125" Y="-0.011917840958" />
                  <Point X="-28.3951953125" Y="0.011137292862" />
                  <Point X="-28.397470703125" Y="0.019931875229" />
                  <Point X="-28.40416796875" Y="0.04150933075" />
                  <Point X="-28.417484375" Y="0.070833404541" />
                  <Point X="-28.440205078125" Y="0.099963188171" />
                  <Point X="-28.459685546875" Y="0.116798866272" />
                  <Point X="-28.467638671875" Y="0.122967475891" />
                  <Point X="-28.48773046875" Y="0.1369115448" />
                  <Point X="-28.50192578125" Y="0.145047805786" />
                  <Point X="-28.532875" Y="0.157848297119" />
                  <Point X="-29.606986328125" Y="0.445655700684" />
                  <Point X="-29.89181640625" Y="0.521975646973" />
                  <Point X="-29.886576171875" Y="0.557388427734" />
                  <Point X="-29.82448828125" Y="0.976968505859" />
                  <Point X="-29.78355859375" Y="1.128013916016" />
                  <Point X="-29.70355078125" Y="1.423268066406" />
                  <Point X="-29.059314453125" Y="1.338452758789" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.744982421875" Y="1.299341796875" />
                  <Point X="-28.723421875" Y="1.301228271484" />
                  <Point X="-28.70313671875" Y="1.305263671875" />
                  <Point X="-28.686177734375" Y="1.310610961914" />
                  <Point X="-28.6417109375" Y="1.324631225586" />
                  <Point X="-28.62277734375" Y="1.332962402344" />
                  <Point X="-28.604033203125" Y="1.343784423828" />
                  <Point X="-28.5873515625" Y="1.356016113281" />
                  <Point X="-28.573712890625" Y="1.371568603516" />
                  <Point X="-28.561298828125" Y="1.389298706055" />
                  <Point X="-28.551349609375" Y="1.407432617188" />
                  <Point X="-28.544544921875" Y="1.423861450195" />
                  <Point X="-28.526703125" Y="1.466937011719" />
                  <Point X="-28.520916015625" Y="1.486788330078" />
                  <Point X="-28.51715625" Y="1.508103271484" />
                  <Point X="-28.515802734375" Y="1.528750244141" />
                  <Point X="-28.518951171875" Y="1.549200439453" />
                  <Point X="-28.5245546875" Y="1.570106689453" />
                  <Point X="-28.53205078125" Y="1.589378295898" />
                  <Point X="-28.54026171875" Y="1.605151245117" />
                  <Point X="-28.561791015625" Y="1.64650793457" />
                  <Point X="-28.57328125" Y="1.663705810547" />
                  <Point X="-28.587193359375" Y="1.680285888672" />
                  <Point X="-28.60213671875" Y="1.694590332031" />
                  <Point X="-29.21825" Y="2.167350830078" />
                  <Point X="-29.351859375" Y="2.269873779297" />
                  <Point X="-29.322404296875" Y="2.320338378906" />
                  <Point X="-29.081146484375" Y="2.733666503906" />
                  <Point X="-28.97273828125" Y="2.873012695312" />
                  <Point X="-28.75050390625" Y="3.158662109375" />
                  <Point X="-28.3870078125" Y="2.948796875" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.18772265625" Y="2.836340087891" />
                  <Point X="-28.167080078125" Y="2.829831542969" />
                  <Point X="-28.14679296875" Y="2.825796386719" />
                  <Point X="-28.123173828125" Y="2.823729980469" />
                  <Point X="-28.061244140625" Y="2.818311767578" />
                  <Point X="-28.040564453125" Y="2.818762939453" />
                  <Point X="-28.01910546875" Y="2.821588134766" />
                  <Point X="-27.999013671875" Y="2.826504638672" />
                  <Point X="-27.980462890625" Y="2.835653076172" />
                  <Point X="-27.962208984375" Y="2.847281982422" />
                  <Point X="-27.946078125" Y="2.860228271484" />
                  <Point X="-27.9293125" Y="2.876993408203" />
                  <Point X="-27.885353515625" Y="2.920951660156" />
                  <Point X="-27.872404296875" Y="2.937085449219" />
                  <Point X="-27.860775390625" Y="2.95533984375" />
                  <Point X="-27.85162890625" Y="2.973888671875" />
                  <Point X="-27.8467109375" Y="2.993977539062" />
                  <Point X="-27.843884765625" Y="3.015436523438" />
                  <Point X="-27.84343359375" Y="3.036120849609" />
                  <Point X="-27.8455" Y="3.059740234375" />
                  <Point X="-27.85091796875" Y="3.121670410156" />
                  <Point X="-27.854955078125" Y="3.141963134766" />
                  <Point X="-27.86146484375" Y="3.162605224609" />
                  <Point X="-27.869794921875" Y="3.181532958984" />
                  <Point X="-28.142814453125" Y="3.654414794922" />
                  <Point X="-28.18333203125" Y="3.724595947266" />
                  <Point X="-28.1208046875" Y="3.772536376953" />
                  <Point X="-27.70062109375" Y="4.094686035156" />
                  <Point X="-27.529876953125" Y="4.189547851562" />
                  <Point X="-27.16703515625" Y="4.391134277344" />
                  <Point X="-27.09840625" Y="4.301693847656" />
                  <Point X="-27.0431953125" Y="4.229741210938" />
                  <Point X="-27.028892578125" Y="4.214799804688" />
                  <Point X="-27.0123125" Y="4.200887207031" />
                  <Point X="-26.995111328125" Y="4.18939453125" />
                  <Point X="-26.968822265625" Y="4.175709472656" />
                  <Point X="-26.89989453125" Y="4.139827636719" />
                  <Point X="-26.880615234375" Y="4.132330566406" />
                  <Point X="-26.859708984375" Y="4.126729003906" />
                  <Point X="-26.839267578125" Y="4.123583007812" />
                  <Point X="-26.81862890625" Y="4.124935058594" />
                  <Point X="-26.7973125" Y="4.128693359375" />
                  <Point X="-26.777451171875" Y="4.134482421875" />
                  <Point X="-26.7500703125" Y="4.14582421875" />
                  <Point X="-26.67827734375" Y="4.175562011719" />
                  <Point X="-26.66014453125" Y="4.185509765625" />
                  <Point X="-26.6424140625" Y="4.197924316406" />
                  <Point X="-26.626859375" Y="4.211565429688" />
                  <Point X="-26.614626953125" Y="4.22825" />
                  <Point X="-26.603806640625" Y="4.246994140625" />
                  <Point X="-26.595478515625" Y="4.265921386719" />
                  <Point X="-26.58656640625" Y="4.294186523438" />
                  <Point X="-26.56319921875" Y="4.368297851562" />
                  <Point X="-26.5591640625" Y="4.388583984375" />
                  <Point X="-26.55727734375" Y="4.410145507812" />
                  <Point X="-26.557728515625" Y="4.430826660156" />
                  <Point X="-26.58419921875" Y="4.631897949219" />
                  <Point X="-26.49358203125" Y="4.657304199219" />
                  <Point X="-25.949630859375" Y="4.80980859375" />
                  <Point X="-25.742640625" Y="4.834033691406" />
                  <Point X="-25.2947109375" Y="4.88645703125" />
                  <Point X="-25.187255859375" Y="4.485431640625" />
                  <Point X="-25.133904296875" Y="4.286315917969" />
                  <Point X="-25.12112890625" Y="4.258122558594" />
                  <Point X="-25.10326953125" Y="4.231395507813" />
                  <Point X="-25.08211328125" Y="4.208808105469" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286315429688" />
                  <Point X="-24.713890625" Y="4.808397949219" />
                  <Point X="-24.692578125" Y="4.8879375" />
                  <Point X="-24.630908203125" Y="4.881479003906" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.984701171875" Y="4.790392089844" />
                  <Point X="-23.518970703125" Y="4.677950195312" />
                  <Point X="-23.40857421875" Y="4.637909179688" />
                  <Point X="-23.105359375" Y="4.527930175781" />
                  <Point X="-22.9975625" Y="4.477517578125" />
                  <Point X="-22.705423828125" Y="4.340894042969" />
                  <Point X="-22.60126171875" Y="4.280208984375" />
                  <Point X="-22.3190234375" Y="4.115775390625" />
                  <Point X="-22.22080859375" Y="4.045932128906" />
                  <Point X="-22.05673828125" Y="3.92925390625" />
                  <Point X="-22.614529296875" Y="2.963130859375" />
                  <Point X="-22.852416015625" Y="2.551097900391" />
                  <Point X="-22.857919921875" Y="2.539935546875" />
                  <Point X="-22.866921875" Y="2.516058105469" />
                  <Point X="-22.872849609375" Y="2.493891845703" />
                  <Point X="-22.888392578125" Y="2.435771728516" />
                  <Point X="-22.8915859375" Y="2.408797607422" />
                  <Point X="-22.890611328125" Y="2.370728515625" />
                  <Point X="-22.889958984375" Y="2.361789794922" />
                  <Point X="-22.883900390625" Y="2.311532470703" />
                  <Point X="-22.87855859375" Y="2.289610351562" />
                  <Point X="-22.87029296875" Y="2.267520019531" />
                  <Point X="-22.859927734375" Y="2.247467773438" />
                  <Point X="-22.84806640625" Y="2.229988769531" />
                  <Point X="-22.81696875" Y="2.184159179688" />
                  <Point X="-22.805533203125" Y="2.170326660156" />
                  <Point X="-22.778400390625" Y="2.145593261719" />
                  <Point X="-22.760921875" Y="2.133732910156" />
                  <Point X="-22.715091796875" Y="2.102635742188" />
                  <Point X="-22.695046875" Y="2.092272216797" />
                  <Point X="-22.672958984375" Y="2.084006347656" />
                  <Point X="-22.65103515625" Y="2.078663330078" />
                  <Point X="-22.6318671875" Y="2.076352050781" />
                  <Point X="-22.581611328125" Y="2.070291748047" />
                  <Point X="-22.563533203125" Y="2.069845703125" />
                  <Point X="-22.52679296875" Y="2.074171386719" />
                  <Point X="-22.504626953125" Y="2.080099121094" />
                  <Point X="-22.446505859375" Y="2.095641113281" />
                  <Point X="-22.43471875" Y="2.099637451172" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-21.331115234375" Y="2.733885498047" />
                  <Point X="-21.032671875" Y="2.906191650391" />
                  <Point X="-20.8767265625" Y="2.689462158203" />
                  <Point X="-20.821974609375" Y="2.598984375" />
                  <Point X="-20.73780078125" Y="2.459884033203" />
                  <Point X="-21.456013671875" Y="1.908778808594" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.778572265625" Y="1.660243408203" />
                  <Point X="-21.796025390625" Y="1.641627441406" />
                  <Point X="-21.811978515625" Y="1.620815429688" />
                  <Point X="-21.853806640625" Y="1.56624609375" />
                  <Point X="-21.86339453125" Y="1.55091003418" />
                  <Point X="-21.8783671875" Y="1.517090087891" />
                  <Point X="-21.884310546875" Y="1.495841186523" />
                  <Point X="-21.899892578125" Y="1.440125366211" />
                  <Point X="-21.90334765625" Y="1.417824584961" />
                  <Point X="-21.9041640625" Y="1.394253417969" />
                  <Point X="-21.902259765625" Y="1.371765258789" />
                  <Point X="-21.897380859375" Y="1.348122802734" />
                  <Point X="-21.88458984375" Y="1.286132446289" />
                  <Point X="-21.8793203125" Y="1.268979614258" />
                  <Point X="-21.86371875" Y="1.235742553711" />
                  <Point X="-21.850451171875" Y="1.215575317383" />
                  <Point X="-21.815662109375" Y="1.162697143555" />
                  <Point X="-21.801107421875" Y="1.145450073242" />
                  <Point X="-21.78386328125" Y="1.129360107422" />
                  <Point X="-21.76565234375" Y="1.116033935547" />
                  <Point X="-21.746423828125" Y="1.105210449219" />
                  <Point X="-21.696009765625" Y="1.076831542969" />
                  <Point X="-21.6794765625" Y="1.069500976563" />
                  <Point X="-21.643880859375" Y="1.059438720703" />
                  <Point X="-21.6178828125" Y="1.056002563477" />
                  <Point X="-21.54971875" Y="1.046994018555" />
                  <Point X="-21.537294921875" Y="1.046175048828" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.48553515625" Y="1.182094238281" />
                  <Point X="-20.22316015625" Y="1.216636474609" />
                  <Point X="-20.15405859375" Y="0.932787963867" />
                  <Point X="-20.13680859375" Y="0.821990783691" />
                  <Point X="-20.109134765625" Y="0.644238708496" />
                  <Point X="-20.924484375" Y="0.425766021729" />
                  <Point X="-21.283419921875" Y="0.329589813232" />
                  <Point X="-21.2952109375" Y="0.325585754395" />
                  <Point X="-21.318453125" Y="0.315068023682" />
                  <Point X="-21.343994140625" Y="0.300304748535" />
                  <Point X="-21.410962890625" Y="0.261595489502" />
                  <Point X="-21.425685546875" Y="0.251097961426" />
                  <Point X="-21.45246875" Y="0.225575637817" />
                  <Point X="-21.46779296875" Y="0.206048400879" />
                  <Point X="-21.507974609375" Y="0.154848007202" />
                  <Point X="-21.51969921875" Y="0.135568191528" />
                  <Point X="-21.52947265625" Y="0.114104736328" />
                  <Point X="-21.536318359375" Y="0.09260181427" />
                  <Point X="-21.54142578125" Y="0.065928604126" />
                  <Point X="-21.5548203125" Y="-0.004008804321" />
                  <Point X="-21.556515625" Y="-0.021874832153" />
                  <Point X="-21.5548203125" Y="-0.058551330566" />
                  <Point X="-21.549712890625" Y="-0.085224693298" />
                  <Point X="-21.536318359375" Y="-0.155161941528" />
                  <Point X="-21.52947265625" Y="-0.176664871216" />
                  <Point X="-21.51969921875" Y="-0.198128326416" />
                  <Point X="-21.507974609375" Y="-0.217408447266" />
                  <Point X="-21.492650390625" Y="-0.236935516357" />
                  <Point X="-21.45246875" Y="-0.288136077881" />
                  <Point X="-21.439998046875" Y="-0.30123815918" />
                  <Point X="-21.410962890625" Y="-0.324155303955" />
                  <Point X="-21.385421875" Y="-0.338918609619" />
                  <Point X="-21.318453125" Y="-0.377627716064" />
                  <Point X="-21.307291015625" Y="-0.383138305664" />
                  <Point X="-21.283419921875" Y="-0.392149810791" />
                  <Point X="-20.34229296875" Y="-0.644324279785" />
                  <Point X="-20.10852734375" Y="-0.706961791992" />
                  <Point X="-20.144974609375" Y="-0.948725769043" />
                  <Point X="-20.167080078125" Y="-1.045594116211" />
                  <Point X="-20.19882421875" Y="-1.18469909668" />
                  <Point X="-21.159353515625" Y="-1.058243041992" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.59196484375" Y="-1.002710266113" />
                  <Point X="-21.62533984375" Y="-1.005508666992" />
                  <Point X="-21.67546875" Y="-1.01640447998" />
                  <Point X="-21.806904296875" Y="-1.04497253418" />
                  <Point X="-21.836025390625" Y="-1.056597167969" />
                  <Point X="-21.8638515625" Y="-1.073489257812" />
                  <Point X="-21.8876015625" Y="-1.093960571289" />
                  <Point X="-21.917900390625" Y="-1.130401245117" />
                  <Point X="-21.997345703125" Y="-1.225948608398" />
                  <Point X="-22.012064453125" Y="-1.250329101562" />
                  <Point X="-22.023408203125" Y="-1.277714355469" />
                  <Point X="-22.030240234375" Y="-1.305366943359" />
                  <Point X="-22.03458203125" Y="-1.352559326172" />
                  <Point X="-22.04596875" Y="-1.476297241211" />
                  <Point X="-22.04365234375" Y="-1.507560791016" />
                  <Point X="-22.035921875" Y="-1.539182373047" />
                  <Point X="-22.02355078125" Y="-1.567995239258" />
                  <Point X="-21.995810546875" Y="-1.611145629883" />
                  <Point X="-21.9230703125" Y="-1.724285766602" />
                  <Point X="-21.9130625" Y="-1.737243652344" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.015998046875" Y="-2.431071289062" />
                  <Point X="-20.786876953125" Y="-2.606882324219" />
                  <Point X="-20.8751953125" Y="-2.749794189453" />
                  <Point X="-20.920904296875" Y="-2.814741699219" />
                  <Point X="-20.971017578125" Y="-2.8859453125" />
                  <Point X="-21.8283046875" Y="-2.390989501953" />
                  <Point X="-22.199044921875" Y="-2.176943115234" />
                  <Point X="-22.2138671875" Y="-2.170012695312" />
                  <Point X="-22.2457734375" Y="-2.159825195312" />
                  <Point X="-22.30543359375" Y="-2.149050537109" />
                  <Point X="-22.46186328125" Y="-2.120799560547" />
                  <Point X="-22.493216796875" Y="-2.120395263672" />
                  <Point X="-22.525390625" Y="-2.125353027344" />
                  <Point X="-22.555166015625" Y="-2.135177001953" />
                  <Point X="-22.604728515625" Y="-2.16126171875" />
                  <Point X="-22.73468359375" Y="-2.229656005859" />
                  <Point X="-22.757615234375" Y="-2.246549804688" />
                  <Point X="-22.77857421875" Y="-2.267509521484" />
                  <Point X="-22.79546484375" Y="-2.290437744141" />
                  <Point X="-22.82155078125" Y="-2.340000976562" />
                  <Point X="-22.8899453125" Y="-2.469955810547" />
                  <Point X="-22.899771484375" Y="-2.499735351563" />
                  <Point X="-22.904728515625" Y="-2.531908447266" />
                  <Point X="-22.90432421875" Y="-2.563259765625" />
                  <Point X="-22.893548828125" Y="-2.622920166016" />
                  <Point X="-22.865296875" Y="-2.779350097656" />
                  <Point X="-22.86101171875" Y="-2.795140136719" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.28694921875" Y="-3.798156005859" />
                  <Point X="-22.13871484375" Y="-4.054904541016" />
                  <Point X="-22.21815234375" Y="-4.111645019531" />
                  <Point X="-22.26926171875" Y="-4.144727050781" />
                  <Point X="-22.298234375" Y="-4.163480957031" />
                  <Point X="-22.958544921875" Y="-3.302947753906" />
                  <Point X="-23.241451171875" Y="-2.934255126953" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.336916015625" Y="-2.862727539062" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.647251953125" Y="-2.747038574219" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397216797" />
                  <Point X="-23.871021484375" Y="-2.780740234375" />
                  <Point X="-23.895404296875" Y="-2.795461181641" />
                  <Point X="-23.945095703125" Y="-2.836778320312" />
                  <Point X="-24.075388671875" Y="-2.945111572266" />
                  <Point X="-24.095857421875" Y="-2.968861083984" />
                  <Point X="-24.11275" Y="-2.996687744141" />
                  <Point X="-24.124375" Y="-3.025808837891" />
                  <Point X="-24.139232421875" Y="-3.094165527344" />
                  <Point X="-24.178189453125" Y="-3.273396728516" />
                  <Point X="-24.180275390625" Y="-3.289627441406" />
                  <Point X="-24.1802578125" Y="-3.323120117188" />
                  <Point X="-24.021208984375" Y="-4.531209960938" />
                  <Point X="-23.97793359375" Y="-4.859915039062" />
                  <Point X="-24.0243203125" Y="-4.870083007812" />
                  <Point X="-24.07068359375" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.058419921875" Y="-4.752637695312" />
                  <Point X="-26.13308203125" Y="-4.733428222656" />
                  <Point X="-26.141244140625" Y="-4.731328125" />
                  <Point X="-26.134087890625" Y="-4.676961914062" />
                  <Point X="-26.1207734375" Y="-4.575837890625" />
                  <Point X="-26.120076171875" Y="-4.568098632812" />
                  <Point X="-26.11944921875" Y="-4.54103125" />
                  <Point X="-26.1217578125" Y="-4.509323730469" />
                  <Point X="-26.123333984375" Y="-4.497686523438" />
                  <Point X="-26.140046875" Y="-4.413674316406" />
                  <Point X="-26.18386328125" Y="-4.193395019531" />
                  <Point X="-26.188126953125" Y="-4.178465332031" />
                  <Point X="-26.199029296875" Y="-4.1495" />
                  <Point X="-26.20566796875" Y="-4.135464355469" />
                  <Point X="-26.221740234375" Y="-4.107626953125" />
                  <Point X="-26.23057421875" Y="-4.094861572266" />
                  <Point X="-26.25020703125" Y="-4.070938232422" />
                  <Point X="-26.261005859375" Y="-4.059780273438" />
                  <Point X="-26.32540625" Y="-4.003301757813" />
                  <Point X="-26.494265625" Y="-3.855215332031" />
                  <Point X="-26.506736328125" Y="-3.845965820312" />
                  <Point X="-26.53301953125" Y="-3.829621582031" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.7222890625" Y="-3.790567382812" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.99273046875" Y="-3.779166015625" />
                  <Point X="-27.008005859375" Y="-3.781945556641" />
                  <Point X="-27.0390546875" Y="-3.790265136719" />
                  <Point X="-27.05367578125" Y="-3.795496582031" />
                  <Point X="-27.0818671875" Y="-3.808270507812" />
                  <Point X="-27.0954375" Y="-3.815812988281" />
                  <Point X="-27.166658203125" Y="-3.863402099609" />
                  <Point X="-27.35340234375" Y="-3.988181152344" />
                  <Point X="-27.359681640625" Y="-3.992758789062" />
                  <Point X="-27.38044140625" Y="-4.010131591797" />
                  <Point X="-27.4027578125" Y="-4.032770507812" />
                  <Point X="-27.410470703125" Y="-4.041629394531" />
                  <Point X="-27.503201171875" Y="-4.162478027344" />
                  <Point X="-27.74759375" Y="-4.01115625" />
                  <Point X="-27.8509453125" Y="-3.931579101562" />
                  <Point X="-27.98086328125" Y="-3.831546386719" />
                  <Point X="-27.547021484375" Y="-3.080110595703" />
                  <Point X="-27.341490234375" Y="-2.724119628906" />
                  <Point X="-27.3348515625" Y="-2.710085449219" />
                  <Point X="-27.32394921875" Y="-2.681120605469" />
                  <Point X="-27.319685546875" Y="-2.666189941406" />
                  <Point X="-27.3134140625" Y="-2.634663818359" />
                  <Point X="-27.311638671875" Y="-2.619239990234" />
                  <Point X="-27.310625" Y="-2.588305664062" />
                  <Point X="-27.3146640625" Y="-2.557617919922" />
                  <Point X="-27.3236484375" Y="-2.527999755859" />
                  <Point X="-27.32935546875" Y="-2.513558837891" />
                  <Point X="-27.343572265625" Y="-2.484729980469" />
                  <Point X="-27.351552734375" Y="-2.471414306641" />
                  <Point X="-27.369578125" Y="-2.446258300781" />
                  <Point X="-27.379623046875" Y="-2.43441796875" />
                  <Point X="-27.396970703125" Y="-2.417069335938" />
                  <Point X="-27.4088125" Y="-2.407022705078" />
                  <Point X="-27.43397265625" Y="-2.388992919922" />
                  <Point X="-27.447291015625" Y="-2.381009765625" />
                  <Point X="-27.47612109375" Y="-2.366792480469" />
                  <Point X="-27.4905625" Y="-2.361086181641" />
                  <Point X="-27.5201796875" Y="-2.352102050781" />
                  <Point X="-27.550865234375" Y="-2.348062255859" />
                  <Point X="-27.581798828125" Y="-2.349074951172" />
                  <Point X="-27.59722265625" Y="-2.350849609375" />
                  <Point X="-27.628748046875" Y="-2.357120605469" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372285888672" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-28.620091796875" Y="-2.917828369141" />
                  <Point X="-28.79308984375" Y="-3.017708740234" />
                  <Point X="-29.00401171875" Y="-2.740600585938" />
                  <Point X="-29.07811328125" Y="-2.616340820313" />
                  <Point X="-29.181265625" Y="-2.443372558594" />
                  <Point X="-28.407736328125" Y="-1.849822998047" />
                  <Point X="-28.048123046875" Y="-1.573881958008" />
                  <Point X="-28.039162109375" Y="-1.566067871094" />
                  <Point X="-28.0162734375" Y="-1.543437011719" />
                  <Point X="-28.0024140625" Y="-1.526084594727" />
                  <Point X="-27.9830703125" Y="-1.495008178711" />
                  <Point X="-27.97491015625" Y="-1.478529663086" />
                  <Point X="-27.9660703125" Y="-1.455249633789" />
                  <Point X="-27.959765625" Y="-1.435445922852" />
                  <Point X="-27.9541875" Y="-1.413911376953" />
                  <Point X="-27.951953125" Y="-1.402404052734" />
                  <Point X="-27.947837890625" Y="-1.370924804688" />
                  <Point X="-27.94708203125" Y="-1.355715698242" />
                  <Point X="-27.947779296875" Y="-1.332843139648" />
                  <Point X="-27.951228515625" Y="-1.310212646484" />
                  <Point X="-27.957376953125" Y="-1.288170776367" />
                  <Point X="-27.961115234375" Y="-1.27733984375" />
                  <Point X="-27.97312109375" Y="-1.248356201172" />
                  <Point X="-27.978794921875" Y="-1.236902832031" />
                  <Point X="-27.9916328125" Y="-1.214859130859" />
                  <Point X="-28.007318359375" Y="-1.194735351562" />
                  <Point X="-28.025560546875" Y="-1.176902587891" />
                  <Point X="-28.03528125" Y="-1.168603149414" />
                  <Point X="-28.05509375" Y="-1.153869995117" />
                  <Point X="-28.07209765625" Y="-1.142588745117" />
                  <Point X="-28.09126953125" Y="-1.131305541992" />
                  <Point X="-28.1054375" Y="-1.124478393555" />
                  <Point X="-28.134701171875" Y="-1.113255249023" />
                  <Point X="-28.149796875" Y="-1.108859130859" />
                  <Point X="-28.18168359375" Y="-1.102378051758" />
                  <Point X="-28.197298828125" Y="-1.100531982422" />
                  <Point X="-28.22862109375" Y="-1.09944140625" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-29.42266796875" Y="-1.25532800293" />
                  <Point X="-29.660919921875" Y="-1.286694580078" />
                  <Point X="-29.74076171875" Y="-0.974118225098" />
                  <Point X="-29.760365234375" Y="-0.837047180176" />
                  <Point X="-29.786451171875" Y="-0.654654602051" />
                  <Point X="-28.91794140625" Y="-0.421937683105" />
                  <Point X="-28.508287109375" Y="-0.312171234131" />
                  <Point X="-28.49778125" Y="-0.308688842773" />
                  <Point X="-28.47724609375" Y="-0.300525726318" />
                  <Point X="-28.467216796875" Y="-0.295844848633" />
                  <Point X="-28.44398828125" Y="-0.283307312012" />
                  <Point X="-28.425904296875" Y="-0.272199554443" />
                  <Point X="-28.4058125" Y="-0.258255584717" />
                  <Point X="-28.396478515625" Y="-0.250870529175" />
                  <Point X="-28.37251953125" Y="-0.229340133667" />
                  <Point X="-28.357826171875" Y="-0.212633132935" />
                  <Point X="-28.3369921875" Y="-0.182439651489" />
                  <Point X="-28.328046875" Y="-0.166328781128" />
                  <Point X="-28.3180703125" Y="-0.1433565979" />
                  <Point X="-28.31088671875" Y="-0.123997718811" />
                  <Point X="-28.30419140625" Y="-0.10242868042" />
                  <Point X="-28.30158203125" Y="-0.091955375671" />
                  <Point X="-28.29613671875" Y="-0.063224491119" />
                  <Point X="-28.294509765625" Y="-0.042935539246" />
                  <Point X="-28.2954296875" Y="-0.009319125175" />
                  <Point X="-28.297388671875" Y="0.007448655605" />
                  <Point X="-28.302189453125" Y="0.030503778458" />
                  <Point X="-28.306740234375" Y="0.048092849731" />
                  <Point X="-28.3134375" Y="0.06967036438" />
                  <Point X="-28.317669921875" Y="0.080789581299" />
                  <Point X="-28.330986328125" Y="0.110113616943" />
                  <Point X="-28.342576171875" Y="0.129260559082" />
                  <Point X="-28.365296875" Y="0.158390335083" />
                  <Point X="-28.3780859375" Y="0.171840118408" />
                  <Point X="-28.39756640625" Y="0.188675811768" />
                  <Point X="-28.41347265625" Y="0.201013153076" />
                  <Point X="-28.433564453125" Y="0.214957244873" />
                  <Point X="-28.440490234375" Y="0.219332855225" />
                  <Point X="-28.4656171875" Y="0.232835540771" />
                  <Point X="-28.49656640625" Y="0.245635986328" />
                  <Point X="-28.508287109375" Y="0.249611190796" />
                  <Point X="-29.5823984375" Y="0.537418640137" />
                  <Point X="-29.7854453125" Y="0.591825012207" />
                  <Point X="-29.73133203125" Y="0.957521972656" />
                  <Point X="-29.691865234375" Y="1.103167236328" />
                  <Point X="-29.6335859375" Y="1.318237182617" />
                  <Point X="-29.07171484375" Y="1.24426550293" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.767736328125" Y="1.204815551758" />
                  <Point X="-28.7470546875" Y="1.204364379883" />
                  <Point X="-28.736701171875" Y="1.204703369141" />
                  <Point X="-28.715140625" Y="1.20658984375" />
                  <Point X="-28.70488671875" Y="1.208054077148" />
                  <Point X="-28.6846015625" Y="1.212089477539" />
                  <Point X="-28.657609375" Y="1.220008178711" />
                  <Point X="-28.613142578125" Y="1.234028320312" />
                  <Point X="-28.60344921875" Y="1.237677001953" />
                  <Point X="-28.584515625" Y="1.246008178711" />
                  <Point X="-28.57527734375" Y="1.250690185547" />
                  <Point X="-28.556533203125" Y="1.261512207031" />
                  <Point X="-28.547857421875" Y="1.267172607422" />
                  <Point X="-28.53117578125" Y="1.279404418945" />
                  <Point X="-28.51592578125" Y="1.293379516602" />
                  <Point X="-28.502287109375" Y="1.308932006836" />
                  <Point X="-28.495892578125" Y="1.317080810547" />
                  <Point X="-28.483478515625" Y="1.334810913086" />
                  <Point X="-28.47801171875" Y="1.343602661133" />
                  <Point X="-28.4680625" Y="1.361736572266" />
                  <Point X="-28.456775390625" Y="1.387508178711" />
                  <Point X="-28.43893359375" Y="1.430583740234" />
                  <Point X="-28.4355" Y="1.440349121094" />
                  <Point X="-28.429712890625" Y="1.460200317383" />
                  <Point X="-28.427359375" Y="1.470285888672" />
                  <Point X="-28.423599609375" Y="1.491600830078" />
                  <Point X="-28.422359375" Y="1.501888916016" />
                  <Point X="-28.421005859375" Y="1.522535888672" />
                  <Point X="-28.421908203125" Y="1.543205810547" />
                  <Point X="-28.425056640625" Y="1.563656005859" />
                  <Point X="-28.427189453125" Y="1.573795166016" />
                  <Point X="-28.43279296875" Y="1.594701416016" />
                  <Point X="-28.436017578125" Y="1.604545410156" />
                  <Point X="-28.443513671875" Y="1.623817016602" />
                  <Point X="-28.45599609375" Y="1.649017578125" />
                  <Point X="-28.477525390625" Y="1.690374267578" />
                  <Point X="-28.482798828125" Y="1.699283691406" />
                  <Point X="-28.4942890625" Y="1.716481689453" />
                  <Point X="-28.500505859375" Y="1.724770019531" />
                  <Point X="-28.51441796875" Y="1.741350097656" />
                  <Point X="-28.521501953125" Y="1.748911987305" />
                  <Point X="-28.5364453125" Y="1.763216552734" />
                  <Point X="-28.5443046875" Y="1.769958862305" />
                  <Point X="-29.16041796875" Y="2.242719482422" />
                  <Point X="-29.22761328125" Y="2.29428125" />
                  <Point X="-29.002283203125" Y="2.680324707031" />
                  <Point X="-28.8977578125" Y="2.814678955078" />
                  <Point X="-28.726337890625" Y="3.035013427734" />
                  <Point X="-28.4345078125" Y="2.866524414062" />
                  <Point X="-28.25415625" Y="2.7623984375" />
                  <Point X="-28.244916015625" Y="2.757716064453" />
                  <Point X="-28.225982421875" Y="2.749385253906" />
                  <Point X="-28.2162890625" Y="2.745736816406" />
                  <Point X="-28.195646484375" Y="2.739228271484" />
                  <Point X="-28.18561328125" Y="2.736656738281" />
                  <Point X="-28.165326171875" Y="2.732621582031" />
                  <Point X="-28.155072265625" Y="2.731157958984" />
                  <Point X="-28.131453125" Y="2.729091552734" />
                  <Point X="-28.0695234375" Y="2.723673339844" />
                  <Point X="-28.059171875" Y="2.723334472656" />
                  <Point X="-28.0384921875" Y="2.723785644531" />
                  <Point X="-28.0281640625" Y="2.724575683594" />
                  <Point X="-28.006705078125" Y="2.727400878906" />
                  <Point X="-27.996525390625" Y="2.729310791016" />
                  <Point X="-27.97643359375" Y="2.734227294922" />
                  <Point X="-27.95699609375" Y="2.741302001953" />
                  <Point X="-27.9384453125" Y="2.750450439453" />
                  <Point X="-27.929419921875" Y="2.755530761719" />
                  <Point X="-27.911166015625" Y="2.767159667969" />
                  <Point X="-27.90274609375" Y="2.773192626953" />
                  <Point X="-27.886615234375" Y="2.786138916016" />
                  <Point X="-27.878904296875" Y="2.793052246094" />
                  <Point X="-27.862138671875" Y="2.809817382812" />
                  <Point X="-27.8181796875" Y="2.853775634766" />
                  <Point X="-27.811265625" Y="2.861487548828" />
                  <Point X="-27.79831640625" Y="2.877621337891" />
                  <Point X="-27.79228125" Y="2.886043212891" />
                  <Point X="-27.78065234375" Y="2.904297607422" />
                  <Point X="-27.7755703125" Y="2.913325439453" />
                  <Point X="-27.766423828125" Y="2.931874267578" />
                  <Point X="-27.759353515625" Y="2.951298828125" />
                  <Point X="-27.754435546875" Y="2.971387695312" />
                  <Point X="-27.7525234375" Y="2.981572998047" />
                  <Point X="-27.749697265625" Y="3.003031982422" />
                  <Point X="-27.748908203125" Y="3.013364746094" />
                  <Point X="-27.74845703125" Y="3.034049072266" />
                  <Point X="-27.748794921875" Y="3.044400634766" />
                  <Point X="-27.750861328125" Y="3.068020019531" />
                  <Point X="-27.756279296875" Y="3.129950195312" />
                  <Point X="-27.757744140625" Y="3.140206787109" />
                  <Point X="-27.76178125" Y="3.160499511719" />
                  <Point X="-27.764353515625" Y="3.170535644531" />
                  <Point X="-27.77086328125" Y="3.191177734375" />
                  <Point X="-27.774513671875" Y="3.200872558594" />
                  <Point X="-27.78284375" Y="3.219800292969" />
                  <Point X="-27.7875234375" Y="3.229033203125" />
                  <Point X="-28.059388671875" Y="3.699915771484" />
                  <Point X="-27.648369140625" Y="4.015039550781" />
                  <Point X="-27.483740234375" Y="4.10650390625" />
                  <Point X="-27.1925234375" Y="4.268296875" />
                  <Point X="-27.173775390625" Y="4.243862304688" />
                  <Point X="-27.118564453125" Y="4.171909667969" />
                  <Point X="-27.1118203125" Y="4.164048828125" />
                  <Point X="-27.097517578125" Y="4.149107421875" />
                  <Point X="-27.089958984375" Y="4.142026367188" />
                  <Point X="-27.07337890625" Y="4.128113769531" />
                  <Point X="-27.06508984375" Y="4.121895996094" />
                  <Point X="-27.047888671875" Y="4.110403320312" />
                  <Point X="-27.0389765625" Y="4.105128417969" />
                  <Point X="-27.0126875" Y="4.091443115234" />
                  <Point X="-26.943759765625" Y="4.055561279297" />
                  <Point X="-26.93432421875" Y="4.051286621094" />
                  <Point X="-26.915044921875" Y="4.043789550781" />
                  <Point X="-26.905201171875" Y="4.040567382812" />
                  <Point X="-26.884294921875" Y="4.034965820313" />
                  <Point X="-26.87416015625" Y="4.032834472656" />
                  <Point X="-26.85371875" Y="4.029688476562" />
                  <Point X="-26.833056640625" Y="4.028786132812" />
                  <Point X="-26.81241796875" Y="4.030138183594" />
                  <Point X="-26.802134765625" Y="4.031378173828" />
                  <Point X="-26.780818359375" Y="4.035136474609" />
                  <Point X="-26.770728515625" Y="4.037488525391" />
                  <Point X="-26.7508671875" Y="4.043277587891" />
                  <Point X="-26.741095703125" Y="4.046714111328" />
                  <Point X="-26.71371484375" Y="4.058055908203" />
                  <Point X="-26.641921875" Y="4.087793701172" />
                  <Point X="-26.632583984375" Y="4.092272460938" />
                  <Point X="-26.614451171875" Y="4.102220214844" />
                  <Point X="-26.60565625" Y="4.107689453125" />
                  <Point X="-26.58792578125" Y="4.120104003906" />
                  <Point X="-26.579775390625" Y="4.126499511719" />
                  <Point X="-26.564220703125" Y="4.140140625" />
                  <Point X="-26.550244140625" Y="4.15539453125" />
                  <Point X="-26.53801171875" Y="4.172079101562" />
                  <Point X="-26.5323515625" Y="4.180755371094" />
                  <Point X="-26.52153125" Y="4.199499511719" />
                  <Point X="-26.5168515625" Y="4.208733398438" />
                  <Point X="-26.5085234375" Y="4.227660644531" />
                  <Point X="-26.504875" Y="4.237354003906" />
                  <Point X="-26.495962890625" Y="4.265619140625" />
                  <Point X="-26.472595703125" Y="4.33973046875" />
                  <Point X="-26.470025390625" Y="4.349764160156" />
                  <Point X="-26.465990234375" Y="4.370050292969" />
                  <Point X="-26.464525390625" Y="4.380302734375" />
                  <Point X="-26.462638671875" Y="4.401864257812" />
                  <Point X="-26.46230078125" Y="4.412217285156" />
                  <Point X="-26.462751953125" Y="4.4328984375" />
                  <Point X="-26.463541015625" Y="4.443226074219" />
                  <Point X="-26.479263671875" Y="4.562655273438" />
                  <Point X="-26.467935546875" Y="4.565831542969" />
                  <Point X="-25.931173828125" Y="4.7163203125" />
                  <Point X="-25.73159765625" Y="4.739677734375" />
                  <Point X="-25.36522265625" Y="4.782556152344" />
                  <Point X="-25.27901953125" Y="4.46084375" />
                  <Point X="-25.22566796875" Y="4.261728027344" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912597656" />
                  <Point X="-25.2001171875" Y="4.205341308594" />
                  <Point X="-25.1822578125" Y="4.178614257812" />
                  <Point X="-25.17260546875" Y="4.166452636719" />
                  <Point X="-25.15144921875" Y="4.143865234375" />
                  <Point X="-25.126896484375" Y="4.125025390625" />
                  <Point X="-25.0996015625" Y="4.110436035156" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.767251953125" Y="4.247105957031" />
                  <Point X="-24.76201953125" Y="4.261727539062" />
                  <Point X="-24.622126953125" Y="4.783810058594" />
                  <Point X="-24.621806640625" Y="4.785006347656" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-24.00699609375" Y="4.698045410156" />
                  <Point X="-23.54640234375" Y="4.58684375" />
                  <Point X="-23.440966796875" Y="4.548602050781" />
                  <Point X="-23.141748046875" Y="4.440072753906" />
                  <Point X="-23.037806640625" Y="4.391462890625" />
                  <Point X="-22.74954296875" Y="4.256651367187" />
                  <Point X="-22.649083984375" Y="4.198124023438" />
                  <Point X="-22.370564453125" Y="4.035857177734" />
                  <Point X="-22.27586328125" Y="3.968511962891" />
                  <Point X="-22.182216796875" Y="3.901916015625" />
                  <Point X="-22.69680078125" Y="3.010630859375" />
                  <Point X="-22.9346875" Y="2.598597900391" />
                  <Point X="-22.93762109375" Y="2.593110595703" />
                  <Point X="-22.9468125" Y="2.573448486328" />
                  <Point X="-22.955814453125" Y="2.549571044922" />
                  <Point X="-22.958697265625" Y="2.540600830078" />
                  <Point X="-22.964625" Y="2.518434570312" />
                  <Point X="-22.98016796875" Y="2.460314453125" />
                  <Point X="-22.982734375" Y="2.446940429688" />
                  <Point X="-22.985927734375" Y="2.419966308594" />
                  <Point X="-22.9865546875" Y="2.406366210938" />
                  <Point X="-22.985580078125" Y="2.368297119141" />
                  <Point X="-22.984275390625" Y="2.350419677734" />
                  <Point X="-22.978216796875" Y="2.300162353516" />
                  <Point X="-22.97619921875" Y="2.289041748047" />
                  <Point X="-22.970857421875" Y="2.267119628906" />
                  <Point X="-22.967533203125" Y="2.256318115234" />
                  <Point X="-22.959267578125" Y="2.234227783203" />
                  <Point X="-22.954685546875" Y="2.223896728516" />
                  <Point X="-22.9443203125" Y="2.203844482422" />
                  <Point X="-22.938537109375" Y="2.194123291016" />
                  <Point X="-22.92667578125" Y="2.176644287109" />
                  <Point X="-22.895578125" Y="2.130814697266" />
                  <Point X="-22.8901875" Y="2.123628173828" />
                  <Point X="-22.869533203125" Y="2.100118896484" />
                  <Point X="-22.842400390625" Y="2.075385498047" />
                  <Point X="-22.8317421875" Y="2.066982910156" />
                  <Point X="-22.814263671875" Y="2.055122558594" />
                  <Point X="-22.76843359375" Y="2.024025512695" />
                  <Point X="-22.75872265625" Y="2.018247314453" />
                  <Point X="-22.738677734375" Y="2.007883789062" />
                  <Point X="-22.72834375" Y="2.003298339844" />
                  <Point X="-22.706255859375" Y="1.995032470703" />
                  <Point X="-22.695453125" Y="1.991707763672" />
                  <Point X="-22.673529296875" Y="1.986364746094" />
                  <Point X="-22.643240234375" Y="1.982035400391" />
                  <Point X="-22.592984375" Y="1.975974975586" />
                  <Point X="-22.583955078125" Y="1.975320678711" />
                  <Point X="-22.55242578125" Y="1.975497436523" />
                  <Point X="-22.515685546875" Y="1.979823120117" />
                  <Point X="-22.50225" Y="1.982396362305" />
                  <Point X="-22.480083984375" Y="1.98832421875" />
                  <Point X="-22.421962890625" Y="2.003866088867" />
                  <Point X="-22.416001953125" Y="2.005671630859" />
                  <Point X="-22.395599609375" Y="2.013065307617" />
                  <Point X="-22.372345703125" Y="2.023572875977" />
                  <Point X="-22.36396484375" Y="2.027872558594" />
                  <Point X="-21.283615234375" Y="2.651613037109" />
                  <Point X="-21.05959375" Y="2.780951660156" />
                  <Point X="-20.956046875" Y="2.637043457031" />
                  <Point X="-20.903251953125" Y="2.549800292969" />
                  <Point X="-20.863115234375" Y="2.483471923828" />
                  <Point X="-21.513845703125" Y="1.984147216797" />
                  <Point X="-21.827046875" Y="1.743819946289" />
                  <Point X="-21.831859375" Y="1.739869506836" />
                  <Point X="-21.847876953125" Y="1.725219238281" />
                  <Point X="-21.865330078125" Y="1.706603149414" />
                  <Point X="-21.871421875" Y="1.699422119141" />
                  <Point X="-21.887375" Y="1.678610107422" />
                  <Point X="-21.929203125" Y="1.624040771484" />
                  <Point X="-21.934359375" Y="1.616606811523" />
                  <Point X="-21.95026171875" Y="1.589367797852" />
                  <Point X="-21.965234375" Y="1.555547851562" />
                  <Point X="-21.96985546875" Y="1.5426796875" />
                  <Point X="-21.975798828125" Y="1.521430786133" />
                  <Point X="-21.991380859375" Y="1.46571496582" />
                  <Point X="-21.9937734375" Y="1.454670288086" />
                  <Point X="-21.997228515625" Y="1.432369506836" />
                  <Point X="-21.998291015625" Y="1.421113037109" />
                  <Point X="-21.999107421875" Y="1.397541870117" />
                  <Point X="-21.998826171875" Y="1.386237548828" />
                  <Point X="-21.996921875" Y="1.363749389648" />
                  <Point X="-21.995298828125" Y="1.352565429688" />
                  <Point X="-21.990419921875" Y="1.328922973633" />
                  <Point X="-21.97762890625" Y="1.266932617188" />
                  <Point X="-21.975400390625" Y="1.25823425293" />
                  <Point X="-21.96531640625" Y="1.228612548828" />
                  <Point X="-21.94971484375" Y="1.195375366211" />
                  <Point X="-21.943083984375" Y="1.183530029297" />
                  <Point X="-21.92981640625" Y="1.163362548828" />
                  <Point X="-21.89502734375" Y="1.11048449707" />
                  <Point X="-21.888265625" Y="1.101428344727" />
                  <Point X="-21.8737109375" Y="1.084181274414" />
                  <Point X="-21.86591796875" Y="1.075990600586" />
                  <Point X="-21.848673828125" Y="1.059900512695" />
                  <Point X="-21.83996484375" Y="1.052694458008" />
                  <Point X="-21.82175390625" Y="1.039368286133" />
                  <Point X="-21.812251953125" Y="1.033247924805" />
                  <Point X="-21.7930234375" Y="1.022424560547" />
                  <Point X="-21.742609375" Y="0.994045654297" />
                  <Point X="-21.734515625" Y="0.989985229492" />
                  <Point X="-21.705318359375" Y="0.978083374023" />
                  <Point X="-21.66972265625" Y="0.968020996094" />
                  <Point X="-21.656328125" Y="0.9652578125" />
                  <Point X="-21.630330078125" Y="0.961821655273" />
                  <Point X="-21.562166015625" Y="0.952813049316" />
                  <Point X="-21.555966796875" Y="0.952199829102" />
                  <Point X="-21.53428125" Y="0.951222900391" />
                  <Point X="-21.50878125" Y="0.952032287598" />
                  <Point X="-21.49939453125" Y="0.952797302246" />
                  <Point X="-20.473134765625" Y="1.087906860352" />
                  <Point X="-20.295296875" Y="1.111319824219" />
                  <Point X="-20.247310546875" Y="0.91420703125" />
                  <Point X="-20.230677734375" Y="0.807376342773" />
                  <Point X="-20.216126953125" Y="0.713921264648" />
                  <Point X="-20.949072265625" Y="0.517529052734" />
                  <Point X="-21.3080078125" Y="0.421352813721" />
                  <Point X="-21.313966796875" Y="0.419544647217" />
                  <Point X="-21.334376953125" Y="0.412136260986" />
                  <Point X="-21.357619140625" Y="0.401618499756" />
                  <Point X="-21.365994140625" Y="0.397316467285" />
                  <Point X="-21.39153515625" Y="0.382553161621" />
                  <Point X="-21.45850390625" Y="0.343843902588" />
                  <Point X="-21.466115234375" Y="0.338946472168" />
                  <Point X="-21.49122265625" Y="0.319872497559" />
                  <Point X="-21.518005859375" Y="0.294350097656" />
                  <Point X="-21.527203125" Y="0.284224578857" />
                  <Point X="-21.54252734375" Y="0.26469744873" />
                  <Point X="-21.582708984375" Y="0.213497024536" />
                  <Point X="-21.58914453125" Y="0.204209442139" />
                  <Point X="-21.600869140625" Y="0.184929626465" />
                  <Point X="-21.606158203125" Y="0.174937271118" />
                  <Point X="-21.615931640625" Y="0.153473892212" />
                  <Point X="-21.61999609375" Y="0.142923904419" />
                  <Point X="-21.626841796875" Y="0.12142099762" />
                  <Point X="-21.629623046875" Y="0.110467933655" />
                  <Point X="-21.63473046875" Y="0.08379473877" />
                  <Point X="-21.648125" Y="0.013857274055" />
                  <Point X="-21.64939453125" Y="0.004965464592" />
                  <Point X="-21.6514140625" Y="-0.026261388779" />
                  <Point X="-21.64971875" Y="-0.062937797546" />
                  <Point X="-21.648125" Y="-0.076417449951" />
                  <Point X="-21.643017578125" Y="-0.103090797424" />
                  <Point X="-21.629623046875" Y="-0.17302796936" />
                  <Point X="-21.626841796875" Y="-0.183981018066" />
                  <Point X="-21.61999609375" Y="-0.205483932495" />
                  <Point X="-21.615931640625" Y="-0.216033920288" />
                  <Point X="-21.606158203125" Y="-0.237497436523" />
                  <Point X="-21.600869140625" Y="-0.247489212036" />
                  <Point X="-21.58914453125" Y="-0.266769317627" />
                  <Point X="-21.582708984375" Y="-0.276057800293" />
                  <Point X="-21.567384765625" Y="-0.295584899902" />
                  <Point X="-21.527203125" Y="-0.346785339355" />
                  <Point X="-21.52128125" Y="-0.35363269043" />
                  <Point X="-21.49885546875" Y="-0.37580871582" />
                  <Point X="-21.4698203125" Y="-0.398725769043" />
                  <Point X="-21.45850390625" Y="-0.406403778076" />
                  <Point X="-21.432962890625" Y="-0.42116708374" />
                  <Point X="-21.365994140625" Y="-0.459876190186" />
                  <Point X="-21.3605078125" Y="-0.462812255859" />
                  <Point X="-21.34084375" Y="-0.472016174316" />
                  <Point X="-21.31697265625" Y="-0.481027618408" />
                  <Point X="-21.3080078125" Y="-0.483912689209" />
                  <Point X="-20.366880859375" Y="-0.736087219238" />
                  <Point X="-20.215123046875" Y="-0.776750793457" />
                  <Point X="-20.23838671875" Y="-0.931063415527" />
                  <Point X="-20.25969921875" Y="-1.024458374023" />
                  <Point X="-20.2721953125" Y="-1.079219726563" />
                  <Point X="-21.146953125" Y="-0.964055786133" />
                  <Point X="-21.563216796875" Y="-0.909253662109" />
                  <Point X="-21.571375" Y="-0.908535644531" />
                  <Point X="-21.59990234375" Y="-0.908042541504" />
                  <Point X="-21.63327734375" Y="-0.910840942383" />
                  <Point X="-21.645517578125" Y="-0.912676147461" />
                  <Point X="-21.695646484375" Y="-0.923571960449" />
                  <Point X="-21.82708203125" Y="-0.952140136719" />
                  <Point X="-21.842125" Y="-0.956742370605" />
                  <Point X="-21.87124609375" Y="-0.96836706543" />
                  <Point X="-21.88532421875" Y="-0.975389343262" />
                  <Point X="-21.913150390625" Y="-0.99228137207" />
                  <Point X="-21.925875" Y="-1.001531066895" />
                  <Point X="-21.949625" Y="-1.022002380371" />
                  <Point X="-21.960650390625" Y="-1.033223999023" />
                  <Point X="-21.99094921875" Y="-1.069664672852" />
                  <Point X="-22.07039453125" Y="-1.165211914063" />
                  <Point X="-22.078673828125" Y="-1.176849853516" />
                  <Point X="-22.093392578125" Y="-1.20123046875" />
                  <Point X="-22.09983203125" Y="-1.213973022461" />
                  <Point X="-22.11117578125" Y="-1.241358276367" />
                  <Point X="-22.115634765625" Y="-1.254928100586" />
                  <Point X="-22.122466796875" Y="-1.282580810547" />
                  <Point X="-22.12483984375" Y="-1.296663452148" />
                  <Point X="-22.129181640625" Y="-1.343855712891" />
                  <Point X="-22.140568359375" Y="-1.46759387207" />
                  <Point X="-22.140708984375" Y="-1.483316772461" />
                  <Point X="-22.138392578125" Y="-1.514580322266" />
                  <Point X="-22.135935546875" Y="-1.530120849609" />
                  <Point X="-22.128205078125" Y="-1.561742431641" />
                  <Point X="-22.12321484375" Y="-1.576662841797" />
                  <Point X="-22.11084375" Y="-1.605475708008" />
                  <Point X="-22.103462890625" Y="-1.619368286133" />
                  <Point X="-22.07572265625" Y="-1.662518554688" />
                  <Point X="-22.002982421875" Y="-1.775658691406" />
                  <Point X="-21.998255859375" Y="-1.782354736328" />
                  <Point X="-21.980197265625" Y="-1.804458251953" />
                  <Point X="-21.95650390625" Y="-1.828123779297" />
                  <Point X="-21.947201171875" Y="-1.836277709961" />
                  <Point X="-21.073830078125" Y="-2.506439697266" />
                  <Point X="-20.912828125" Y="-2.629981201172" />
                  <Point X="-20.95450390625" Y="-2.697419677734" />
                  <Point X="-20.99859375" Y="-2.760065673828" />
                  <Point X="-20.998724609375" Y="-2.760251953125" />
                  <Point X="-21.7808046875" Y="-2.308717041016" />
                  <Point X="-22.151544921875" Y="-2.094670654297" />
                  <Point X="-22.158806640625" Y="-2.090885498047" />
                  <Point X="-22.184970703125" Y="-2.079513916016" />
                  <Point X="-22.216876953125" Y="-2.069326416016" />
                  <Point X="-22.228888671875" Y="-2.066337646484" />
                  <Point X="-22.288548828125" Y="-2.055562988281" />
                  <Point X="-22.444978515625" Y="-2.027311889648" />
                  <Point X="-22.460638671875" Y="-2.025807495117" />
                  <Point X="-22.4919921875" Y="-2.025403198242" />
                  <Point X="-22.507685546875" Y="-2.026503295898" />
                  <Point X="-22.539859375" Y="-2.03146105957" />
                  <Point X="-22.55515625" Y="-2.03513659668" />
                  <Point X="-22.584931640625" Y="-2.044960571289" />
                  <Point X="-22.59941015625" Y="-2.051109130859" />
                  <Point X="-22.64897265625" Y="-2.077193847656" />
                  <Point X="-22.778927734375" Y="-2.145588134766" />
                  <Point X="-22.79103125" Y="-2.153170654297" />
                  <Point X="-22.813962890625" Y="-2.170064453125" />
                  <Point X="-22.824791015625" Y="-2.179375732422" />
                  <Point X="-22.84575" Y="-2.200335449219" />
                  <Point X="-22.855060546875" Y="-2.2111640625" />
                  <Point X="-22.871951171875" Y="-2.234092285156" />
                  <Point X="-22.87953125" Y="-2.246191894531" />
                  <Point X="-22.9056171875" Y="-2.295755126953" />
                  <Point X="-22.97401171875" Y="-2.425709960938" />
                  <Point X="-22.98016015625" Y="-2.440187988281" />
                  <Point X="-22.989986328125" Y="-2.469967529297" />
                  <Point X="-22.9936640625" Y="-2.485269042969" />
                  <Point X="-22.99862109375" Y="-2.517442138672" />
                  <Point X="-22.999720703125" Y="-2.533133544922" />
                  <Point X="-22.99931640625" Y="-2.564484863281" />
                  <Point X="-22.9978125" Y="-2.580144775391" />
                  <Point X="-22.987037109375" Y="-2.639805175781" />
                  <Point X="-22.95878515625" Y="-2.796235107422" />
                  <Point X="-22.95698046875" Y="-2.804231445312" />
                  <Point X="-22.94876171875" Y="-2.831540771484" />
                  <Point X="-22.935927734375" Y="-2.862479248047" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.369220703125" Y="-3.845656005859" />
                  <Point X="-22.264103515625" Y="-4.027722412109" />
                  <Point X="-22.276244140625" Y="-4.036082763672" />
                  <Point X="-22.88317578125" Y="-3.245115478516" />
                  <Point X="-23.16608203125" Y="-2.876422851562" />
                  <Point X="-23.17134765625" Y="-2.870142089844" />
                  <Point X="-23.19116796875" Y="-2.849625732422" />
                  <Point X="-23.21674609375" Y="-2.828004638672" />
                  <Point X="-23.22669921875" Y="-2.820646972656" />
                  <Point X="-23.285541015625" Y="-2.782817382812" />
                  <Point X="-23.43982421875" Y="-2.683628173828" />
                  <Point X="-23.453716796875" Y="-2.676245117188" />
                  <Point X="-23.482529296875" Y="-2.663873535156" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.65595703125" Y="-2.652438232422" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.8387734375" Y="-2.670338867188" />
                  <Point X="-23.866423828125" Y="-2.677170410156" />
                  <Point X="-23.8799921875" Y="-2.681628417969" />
                  <Point X="-23.907376953125" Y="-2.692971435547" />
                  <Point X="-23.920123046875" Y="-2.699413085938" />
                  <Point X="-23.944505859375" Y="-2.714134033203" />
                  <Point X="-23.956142578125" Y="-2.722413330078" />
                  <Point X="-24.005833984375" Y="-2.76373046875" />
                  <Point X="-24.136126953125" Y="-2.872063720703" />
                  <Point X="-24.147349609375" Y="-2.883091064453" />
                  <Point X="-24.167818359375" Y="-2.906840576172" />
                  <Point X="-24.177064453125" Y="-2.919562744141" />
                  <Point X="-24.19395703125" Y="-2.947389404297" />
                  <Point X="-24.20098046875" Y="-2.961466796875" />
                  <Point X="-24.21260546875" Y="-2.990587890625" />
                  <Point X="-24.21720703125" Y="-3.005631591797" />
                  <Point X="-24.232064453125" Y="-3.07398828125" />
                  <Point X="-24.271021484375" Y="-3.253219482422" />
                  <Point X="-24.2724140625" Y="-3.261287109375" />
                  <Point X="-24.275275390625" Y="-3.289677246094" />
                  <Point X="-24.2752578125" Y="-3.323169921875" />
                  <Point X="-24.2744453125" Y="-3.335520263672" />
                  <Point X="-24.16691015625" Y="-4.152326660156" />
                  <Point X="-24.2343359375" Y="-3.900689208984" />
                  <Point X="-24.344931640625" Y="-3.487936767578" />
                  <Point X="-24.347392578125" Y="-3.480122558594" />
                  <Point X="-24.35785546875" Y="-3.453573974609" />
                  <Point X="-24.37321484375" Y="-3.423810546875" />
                  <Point X="-24.37959375" Y="-3.413208007812" />
                  <Point X="-24.424798828125" Y="-3.348078125" />
                  <Point X="-24.543322265625" Y="-3.177307617188" />
                  <Point X="-24.553330078125" Y="-3.165172119141" />
                  <Point X="-24.575212890625" Y="-3.142717773438" />
                  <Point X="-24.587087890625" Y="-3.132398925781" />
                  <Point X="-24.61334375" Y="-3.113154785156" />
                  <Point X="-24.6267578125" Y="-3.104937988281" />
                  <Point X="-24.6547578125" Y="-3.090830078125" />
                  <Point X="-24.66934375" Y="-3.084938964844" />
                  <Point X="-24.73929296875" Y="-3.063229003906" />
                  <Point X="-24.922703125" Y="-3.006305664062" />
                  <Point X="-24.936623046875" Y="-3.003109619141" />
                  <Point X="-24.964783203125" Y="-2.998840087891" />
                  <Point X="-24.9790234375" Y="-2.997766601562" />
                  <Point X="-25.0086640625" Y="-2.997766601562" />
                  <Point X="-25.022904296875" Y="-2.99883984375" />
                  <Point X="-25.051064453125" Y="-3.003109130859" />
                  <Point X="-25.064984375" Y="-3.006305175781" />
                  <Point X="-25.13493359375" Y="-3.028014892578" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.332931640625" Y="-3.090830078125" />
                  <Point X="-25.360931640625" Y="-3.104938232422" />
                  <Point X="-25.37434375" Y="-3.113154785156" />
                  <Point X="-25.400599609375" Y="-3.132398925781" />
                  <Point X="-25.412474609375" Y="-3.142717041016" />
                  <Point X="-25.434359375" Y="-3.165172851562" />
                  <Point X="-25.444369140625" Y="-3.177310546875" />
                  <Point X="-25.489572265625" Y="-3.242440185547" />
                  <Point X="-25.608095703125" Y="-3.4132109375" />
                  <Point X="-25.61246875" Y="-3.420130371094" />
                  <Point X="-25.625974609375" Y="-3.445260742188" />
                  <Point X="-25.63877734375" Y="-3.476213623047" />
                  <Point X="-25.64275390625" Y="-3.487936279297" />
                  <Point X="-25.93273828125" Y="-4.570173828125" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.916018277111" Y="2.442877856724" />
                  <Point X="-21.142674749643" Y="2.732984912122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.991389620637" Y="2.385043198855" />
                  <Point X="-21.225755749286" Y="2.685018164089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.066760964163" Y="2.327208540987" />
                  <Point X="-21.308836730021" Y="2.637051391854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.231018100677" Y="3.817389720589" />
                  <Point X="-22.427719439528" Y="4.069155953293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.142132307688" Y="2.269373883119" />
                  <Point X="-21.391917667381" Y="2.589084564103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.28224851581" Y="3.728656083432" />
                  <Point X="-22.648997873356" Y="4.198073854735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.266952222763" Y="0.994888878436" />
                  <Point X="-20.352077639464" Y="1.10384444323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.217503651214" Y="2.211539225251" />
                  <Point X="-21.474998604742" Y="2.541117736351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.333478930943" Y="3.639922446274" />
                  <Point X="-22.853191754427" Y="4.305124525837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.22810668023" Y="0.790863273002" />
                  <Point X="-20.46139056154" Y="1.08945302481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.29287499474" Y="2.153704567383" />
                  <Point X="-21.558079542102" Y="2.493150908599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.384709346076" Y="3.551188809117" />
                  <Point X="-23.043159090275" Y="4.393966049425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.276012903211" Y="0.697874863915" />
                  <Point X="-20.570703553298" Y="1.07506169558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.368246338265" Y="2.095869909515" />
                  <Point X="-21.641160479462" Y="2.445184080847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.435939761209" Y="3.462455171959" />
                  <Point X="-23.222669898367" Y="4.469423827809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.375700530573" Y="0.67116363007" />
                  <Point X="-20.680016553444" Y="1.060670377086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.443617681791" Y="2.038035251647" />
                  <Point X="-21.724241416822" Y="2.397217253095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.487170176342" Y="3.373721534802" />
                  <Point X="-23.390899366291" Y="4.530442149246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.475388157935" Y="0.644452396224" />
                  <Point X="-20.78932955359" Y="1.046279058592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.518989035252" Y="1.980200606495" />
                  <Point X="-21.807322354183" Y="2.349250425344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.538400591475" Y="3.284987897644" />
                  <Point X="-23.557641945123" Y="4.589557339431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.575075785297" Y="0.617741162379" />
                  <Point X="-20.898642553736" Y="1.031887740098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.594360524371" Y="1.922366134978" />
                  <Point X="-21.890403291543" Y="2.301283597592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.589631006608" Y="3.196254260487" />
                  <Point X="-23.706225493359" Y="4.625430030356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.674763412659" Y="0.591029928533" />
                  <Point X="-21.007955553882" Y="1.017496421604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.669732013489" Y="1.864531663461" />
                  <Point X="-21.973484228903" Y="2.25331676984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.640861421741" Y="3.107520623329" />
                  <Point X="-23.854809041595" Y="4.661302721281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.774451040021" Y="0.564318694688" />
                  <Point X="-21.117268554028" Y="1.003105103109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.745103502608" Y="1.806697191943" />
                  <Point X="-22.056565166263" Y="2.205349942088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.692091836874" Y="3.018786986171" />
                  <Point X="-24.00339258983" Y="4.697175412206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.874138667384" Y="0.537607460842" />
                  <Point X="-21.226581554174" Y="0.988713784615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.820474991727" Y="1.748862720426" />
                  <Point X="-22.139646103624" Y="2.157383114336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.743322168677" Y="2.930053242356" />
                  <Point X="-24.151976465455" Y="4.733048522171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.973826308267" Y="0.510896244303" />
                  <Point X="-21.335894554319" Y="0.974322466121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.88676554683" Y="1.679405183403" />
                  <Point X="-22.222727040984" Y="2.109416286584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.794552492044" Y="2.841319487744" />
                  <Point X="-24.285619699454" Y="4.749798482906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.073513990079" Y="0.484185080151" />
                  <Point X="-21.445207554465" Y="0.959931147627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.944580415534" Y="1.599099262496" />
                  <Point X="-22.305807978344" Y="2.061449458833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.845782815412" Y="2.752585733132" />
                  <Point X="-24.41691961688" Y="4.763549135202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.173201671891" Y="0.457473915998" />
                  <Point X="-21.560038602067" Y="0.952602607799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.983352879737" Y="1.494420175291" />
                  <Point X="-22.390398894425" Y="2.015415315709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.89701313878" Y="2.663851978521" />
                  <Point X="-24.548219534306" Y="4.777299787498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.233619564172" Y="-0.899441918856" />
                  <Point X="-20.359754334755" Y="-0.73799677472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.272889353704" Y="0.430762751846" />
                  <Point X="-21.699138138463" Y="0.97633631713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.995804869226" Y="1.35605241672" />
                  <Point X="-22.488112308724" Y="1.986177204373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.946853647855" Y="2.573339342735" />
                  <Point X="-24.63533906715" Y="4.734502126242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.258976641361" Y="-1.021291918412" />
                  <Point X="-20.512231399902" Y="-0.697140609405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.366904061296" Y="0.396790511811" />
                  <Point X="-22.601500546939" Y="1.977001952744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.979897072761" Y="2.461327419622" />
                  <Point X="-24.666126501925" Y="4.619602667438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.341392722396" Y="-1.070109723454" />
                  <Point X="-20.664708458606" Y="-0.656284452336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.449955027757" Y="0.348785323057" />
                  <Point X="-22.751272158418" Y="2.014395295277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.978888022924" Y="2.305730316405" />
                  <Point X="-24.6969139367" Y="4.504703208634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.475771387765" Y="-1.05241845349" />
                  <Point X="-20.817185517311" Y="-0.615428295266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.523369137222" Y="0.288445519831" />
                  <Point X="-24.727701371475" Y="4.38980374983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.610150053134" Y="-1.034727183527" />
                  <Point X="-20.969662576015" Y="-0.574572138197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.583959591692" Y="0.211692186699" />
                  <Point X="-24.75848880625" Y="4.274904291026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.744528718503" Y="-1.017035913563" />
                  <Point X="-21.12213963472" Y="-0.533715981128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.628594854711" Y="0.114517139781" />
                  <Point X="-24.804685202448" Y="4.179727403456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.878907383872" Y="-0.9993446436" />
                  <Point X="-21.274616693424" Y="-0.492859824059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.650477504168" Y="-0.011779924477" />
                  <Point X="-24.876107913656" Y="4.116838726694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.349040878126" Y="4.722165317155" />
                  <Point X="-25.393626227569" Y="4.779231962094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.013286049241" Y="-0.981653373637" />
                  <Point X="-21.467476660762" Y="-0.400315901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.62020242075" Y="-0.204835842484" />
                  <Point X="-24.972619457166" Y="4.086062290899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.286112575822" Y="4.487315184874" />
                  <Point X="-25.504083095084" Y="4.766304727067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.147664714647" Y="-0.963962103626" />
                  <Point X="-25.121266322185" Y="4.122016023611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.221650997055" Y="4.250502548211" />
                  <Point X="-25.614539962599" Y="4.753377492041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.2820433869" Y="-0.946270824852" />
                  <Point X="-25.724996830115" Y="4.740450257014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.416422059153" Y="-0.928579546077" />
                  <Point X="-25.835453631618" Y="4.727522937497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.550800731406" Y="-0.910888267302" />
                  <Point X="-25.944367836861" Y="4.712621184804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.666412331727" Y="-0.917217745208" />
                  <Point X="-26.04326228127" Y="4.684894723075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.769468431192" Y="-0.939617531372" />
                  <Point X="-26.14215672568" Y="4.657168261346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.868439024856" Y="-0.967246526499" />
                  <Point X="-26.241051170089" Y="4.629441799617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.947587742401" Y="-1.0202463661" />
                  <Point X="-26.339945614498" Y="4.601715337888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.010842352662" Y="-1.093589735319" />
                  <Point X="-26.438840058907" Y="4.573988876158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.920870685975" Y="-2.642995427745" />
                  <Point X="-20.958297635421" Y="-2.595091116981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.072799976842" Y="-1.16859317102" />
                  <Point X="-26.46481918849" Y="4.452935067359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.975546743986" Y="-2.727318843132" />
                  <Point X="-21.25931518033" Y="-2.364111807553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.118102103096" Y="-1.264914671923" />
                  <Point X="-26.479592719114" Y="4.317538745939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.129250383752" Y="-2.684892733897" />
                  <Point X="-21.560332421466" Y="-2.133132886936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.13421862515" Y="-1.3985920427" />
                  <Point X="-26.516235423748" Y="4.210133690795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.348874196753" Y="-2.558092650537" />
                  <Point X="-21.861349662603" Y="-1.902153966318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.128613472477" Y="-1.560071889282" />
                  <Point X="-26.574834988005" Y="4.130832134395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.568498009754" Y="-2.431292567177" />
                  <Point X="-26.656914485889" Y="4.081583522565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.788121796021" Y="-2.304492518034" />
                  <Point X="-26.748259777125" Y="4.044194585403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.007744806604" Y="-2.177693461722" />
                  <Point X="-26.857997590853" Y="4.030347003498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.211667632891" Y="-2.070989724925" />
                  <Point X="-27.035611242856" Y="4.103376532822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.353432052475" Y="-2.043845120656" />
                  <Point X="-27.257021587125" Y="4.232463271929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.4883606054" Y="-2.025450026716" />
                  <Point X="-27.341088121199" Y="4.185757950443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.591496044764" Y="-2.047748262441" />
                  <Point X="-27.425154655272" Y="4.139052628957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.677370583859" Y="-2.092139443028" />
                  <Point X="-27.509221143843" Y="4.092347249231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.762800063115" Y="-2.137100274233" />
                  <Point X="-27.593287527797" Y="4.0456417356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.83912682921" Y="-2.193712046979" />
                  <Point X="-27.6743641428" Y="3.995109492219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.895289545518" Y="-2.276132626519" />
                  <Point X="-27.749759108807" Y="3.937305069748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.943813291463" Y="-2.368330642256" />
                  <Point X="-27.825154074814" Y="3.879500647277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.988494625023" Y="-2.465446721572" />
                  <Point X="-27.900549040821" Y="3.821696224806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.991309661947" Y="-2.616149216936" />
                  <Point X="-27.975944006828" Y="3.763891802336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.95200573573" Y="-2.820761526732" />
                  <Point X="-28.051338972835" Y="3.706087379865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.644945122809" Y="-3.368086767137" />
                  <Point X="-27.772034561124" Y="3.19428845694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.303642094366" Y="-3.959240300755" />
                  <Point X="-27.749115665424" Y="3.01064802985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.401633260145" Y="-2.708181274215" />
                  <Point X="-27.783315813314" Y="2.900116644639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.570145043178" Y="-2.646801605917" />
                  <Point X="-27.845961673851" Y="2.825994111304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.684262916476" Y="-2.655042967227" />
                  <Point X="-27.917432759574" Y="2.763167351098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.796733605478" Y="-2.665392628292" />
                  <Point X="-28.009734093014" Y="2.727002092155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.89858773731" Y="-2.68933086287" />
                  <Point X="-28.131957784245" Y="2.72913570468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.978665979747" Y="-2.741140964863" />
                  <Point X="-28.298508340863" Y="2.78800511764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.051747874963" Y="-2.801905982938" />
                  <Point X="-28.518132139093" Y="2.914805182094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.124829970954" Y="-2.862670744032" />
                  <Point X="-28.729465260891" Y="3.030993664624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.187503218571" Y="-2.936758223503" />
                  <Point X="-28.78961655904" Y="2.953678237034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.225269399365" Y="-3.042725294737" />
                  <Point X="-28.849767857188" Y="2.876362809444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.251508679522" Y="-3.163446125986" />
                  <Point X="-28.909919036837" Y="2.79904723018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.275066003538" Y="-3.287599704556" />
                  <Point X="-28.970069748869" Y="2.721731052396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.257475186798" Y="-3.464420501566" />
                  <Point X="-29.026226956527" Y="2.639303422104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.233043407824" Y="-3.649997330945" />
                  <Point X="-24.337244421" Y="-3.516626116064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.676170864582" Y="-3.082820050672" />
                  <Point X="-29.077779396905" Y="2.550981958464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.20861162885" Y="-3.835574160323" />
                  <Point X="-24.274317574838" Y="-3.75147438457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.835317995103" Y="-3.033426590996" />
                  <Point X="-28.424591354534" Y="1.560633811063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.653741710721" Y="1.853932891978" />
                  <Point X="-29.129331837283" Y="2.462660494825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.184179849875" Y="-4.021150989702" />
                  <Point X="-24.2113905593" Y="-3.986322869867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.98373536272" Y="-2.997766601562" />
                  <Point X="-28.440530205192" Y="1.426729031268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.954758688754" Y="2.084911475838" />
                  <Point X="-29.180884277661" Y="2.374339031185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.091251660241" Y="-3.014457594547" />
                  <Point X="-28.486216945119" Y="1.330899813418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.188280603311" Y="-3.044571789105" />
                  <Point X="-28.553902903806" Y="1.263228311537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.285309594171" Y="-3.074685922494" />
                  <Point X="-28.644035971969" Y="1.224287799594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.375276833604" Y="-3.113838685532" />
                  <Point X="-28.749061016453" Y="1.204408148132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.445251097839" Y="-3.178581289875" />
                  <Point X="-28.881157330488" Y="1.219178141604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.501965190457" Y="-3.260296139923" />
                  <Point X="-29.015536003727" Y="1.23686942164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.558679334799" Y="-3.342010923766" />
                  <Point X="-29.149914674539" Y="1.25456069857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.614805475025" Y="-3.424478318558" />
                  <Point X="-28.297849417025" Y="0.009661323812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.475360705572" Y="0.236865412208" />
                  <Point X="-29.284293343608" Y="1.272251973269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.65374259285" Y="-3.528946658737" />
                  <Point X="-28.311882878307" Y="-0.126682243171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.631714418112" Y="0.282683459914" />
                  <Point X="-29.418672012677" Y="1.289943247969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.684529841718" Y="-3.64384635549" />
                  <Point X="-28.36177735513" Y="-0.217125803389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.784191451256" Y="0.323539584268" />
                  <Point X="-29.553050681746" Y="1.307634522668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.715317090587" Y="-3.758746052243" />
                  <Point X="-28.434960006792" Y="-0.277761859093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.936668484401" Y="0.364395708623" />
                  <Point X="-29.646025456495" Y="1.272331229292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.746104339455" Y="-3.873645748997" />
                  <Point X="-28.52511116128" Y="-0.316679221595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.089145517546" Y="0.405251832977" />
                  <Point X="-29.677071148131" Y="1.157762324197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.776891588324" Y="-3.98854544575" />
                  <Point X="-28.624798812073" Y="-0.343390425451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.241622550691" Y="0.446107957332" />
                  <Point X="-29.708116865817" Y="1.043193452443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.807678837193" Y="-4.103445142503" />
                  <Point X="-27.947522103491" Y="-1.3645706596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.146572144057" Y="-1.10979822579" />
                  <Point X="-28.724486462865" Y="-0.370101629306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.394099583836" Y="0.486964081687" />
                  <Point X="-29.736174078723" Y="0.924799469004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.838466086061" Y="-4.218344839257" />
                  <Point X="-27.976513956916" Y="-1.481768357728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.271804379167" Y="-1.103813852701" />
                  <Point X="-28.824174113658" Y="-0.396812833162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.546576616981" Y="0.527820206041" />
                  <Point X="-29.755371255836" Y="0.79506515689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.86925333493" Y="-4.33324453601" />
                  <Point X="-28.03467495248" Y="-1.561631256456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.381117365854" Y="-1.118205188422" />
                  <Point X="-28.923861761732" Y="-0.423524040498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.699053701591" Y="0.568676396269" />
                  <Point X="-29.774568432948" Y="0.665330844776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.900040583798" Y="-4.448144232763" />
                  <Point X="-27.314118830058" Y="-2.638206614197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.539650978296" Y="-2.349538628268" />
                  <Point X="-28.109102875697" Y="-1.620673437254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.490430352541" Y="-1.132596524143" />
                  <Point X="-29.023549366742" Y="-0.450235302953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.930827832667" Y="-4.563043929516" />
                  <Point X="-26.171647207964" Y="-4.254809185234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.461163426909" Y="-3.884245323412" />
                  <Point X="-27.352565541783" Y="-2.74330264556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.649300026301" Y="-2.363499825118" />
                  <Point X="-28.184474306375" Y="-1.678507983572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.599743339227" Y="-1.146987859864" />
                  <Point X="-29.123236971751" Y="-0.476946565409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.961615912262" Y="-4.677942562988" />
                  <Point X="-26.130468953382" Y="-4.461820525936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.651272776657" Y="-3.795222030321" />
                  <Point X="-27.40379590902" Y="-2.832036344022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.735696213332" Y="-2.407223326795" />
                  <Point X="-28.259845737054" Y="-1.736342529889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.709056325914" Y="-1.161379195585" />
                  <Point X="-29.222924576761" Y="-0.503657827864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.017628142385" Y="-4.760555756063" />
                  <Point X="-26.126711231222" Y="-4.620935769291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.778336170309" Y="-3.786893881179" />
                  <Point X="-27.455026276256" Y="-2.920770042484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.818777214666" Y="-2.455190072665" />
                  <Point X="-28.335217167733" Y="-1.794177076206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.818369312601" Y="-1.175770531306" />
                  <Point X="-29.32261218177" Y="-0.53036909032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.905399547966" Y="-3.778565752509" />
                  <Point X="-27.506256643493" Y="-3.009503740946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.901858215999" Y="-2.503156818534" />
                  <Point X="-28.410588599281" Y="-1.85201162141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.927682299288" Y="-1.190161867027" />
                  <Point X="-29.42229978678" Y="-0.557080352776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.020665444252" Y="-3.785337711401" />
                  <Point X="-27.557487019431" Y="-3.098237428271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.984939217333" Y="-2.551123564404" />
                  <Point X="-28.485960052939" Y="-1.909846138315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.036995285975" Y="-1.204553202748" />
                  <Point X="-29.521987391789" Y="-0.583791615231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.109875130164" Y="-3.825460098729" />
                  <Point X="-27.608717429259" Y="-3.186971072219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.068020218666" Y="-2.599090310274" />
                  <Point X="-28.561331506597" Y="-1.96768065522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.146308272661" Y="-1.218944538469" />
                  <Point X="-29.621674996799" Y="-0.610502877687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.1890821192" Y="-3.878385354222" />
                  <Point X="-27.659947839087" Y="-3.275704716166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.15110122" Y="-2.647057056143" />
                  <Point X="-28.636702960256" Y="-2.025515172125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.255621259348" Y="-1.23333587419" />
                  <Point X="-29.721362601808" Y="-0.637214140142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.268289405906" Y="-3.931310228715" />
                  <Point X="-27.711178248916" Y="-3.364438360113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.234182221334" Y="-2.695023802013" />
                  <Point X="-28.712074413914" Y="-2.08334968903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.364934246035" Y="-1.247727209911" />
                  <Point X="-29.777075278097" Y="-0.720210744639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.347496692612" Y="-3.984235103208" />
                  <Point X="-27.762408658744" Y="-3.453172004061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.317263222667" Y="-2.742990547883" />
                  <Point X="-28.787445867572" Y="-2.141184205935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.474247218149" Y="-1.262118564284" />
                  <Point X="-29.750061539239" Y="-0.909092331966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.416783974283" Y="-4.049857005136" />
                  <Point X="-27.813639068572" Y="-3.541905648008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.400344224001" Y="-2.790957293752" />
                  <Point X="-28.86281732123" Y="-2.19901872284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.583560173952" Y="-1.276509939534" />
                  <Point X="-29.702362200856" Y="-1.124450279312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.476519028623" Y="-4.127705200506" />
                  <Point X="-27.8648694784" Y="-3.630639291955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.483425225335" Y="-2.838924039622" />
                  <Point X="-28.938188774888" Y="-2.256853239745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.63241648453" Y="-4.082471134659" />
                  <Point X="-27.916099888229" Y="-3.719372935903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.566506226668" Y="-2.886890785492" />
                  <Point X="-29.013560228546" Y="-2.314687756649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.900935113962" Y="-3.89308854015" />
                  <Point X="-27.967330298057" Y="-3.80810657985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.64958721928" Y="-2.934857542525" />
                  <Point X="-29.088931682204" Y="-2.372522273554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.732668196046" Y="-2.982824319839" />
                  <Point X="-29.164303135862" Y="-2.430356790459" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.41786328125" Y="-3.949864746094" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521544433594" />
                  <Point X="-24.580884765625" Y="-3.456414550781" />
                  <Point X="-24.699408203125" Y="-3.285644042969" />
                  <Point X="-24.7256640625" Y="-3.266399902344" />
                  <Point X="-24.79561328125" Y="-3.244689941406" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-25.0086640625" Y="-3.187766601562" />
                  <Point X="-25.07861328125" Y="-3.209476318359" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.288279296875" Y="-3.285644042969" />
                  <Point X="-25.333482421875" Y="-3.350773681641" />
                  <Point X="-25.452005859375" Y="-3.521544433594" />
                  <Point X="-25.4592265625" Y="-3.537112060547" />
                  <Point X="-25.7492109375" Y="-4.619349609375" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-25.88473828125" Y="-4.979895996094" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.18042578125" Y="-4.917435546875" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.322462890625" Y="-4.652163085938" />
                  <Point X="-26.3091484375" Y="-4.5510390625" />
                  <Point X="-26.309681640625" Y="-4.5347578125" />
                  <Point X="-26.32639453125" Y="-4.450745605469" />
                  <Point X="-26.3702109375" Y="-4.230466308594" />
                  <Point X="-26.386283203125" Y="-4.20262890625" />
                  <Point X="-26.45068359375" Y="-4.146150390625" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.73471484375" Y="-3.980160644531" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.98987890625" Y="-3.973791015625" />
                  <Point X="-27.061099609375" Y="-4.021380126953" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.422537109375" Y="-4.369464355469" />
                  <Point X="-27.45709375" Y="-4.4145" />
                  <Point X="-27.53994140625" Y="-4.363203125" />
                  <Point X="-27.855833984375" Y="-4.167610351562" />
                  <Point X="-27.966859375" Y="-4.082124023438" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-27.711564453125" Y="-2.985110595703" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597593505859" />
                  <Point X="-27.513978515625" Y="-2.568764648438" />
                  <Point X="-27.531326171875" Y="-2.551416015625" />
                  <Point X="-27.56015625" Y="-2.537198730469" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-28.525091796875" Y="-3.082373291016" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-28.9121328125" Y="-3.175014892578" />
                  <Point X="-29.161697265625" Y="-2.847136962891" />
                  <Point X="-29.241298828125" Y="-2.713656738281" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-28.523400390625" Y="-1.6990859375" />
                  <Point X="-28.163787109375" Y="-1.423144775391" />
                  <Point X="-28.15253515625" Y="-1.41108215332" />
                  <Point X="-28.1436953125" Y="-1.387802124023" />
                  <Point X="-28.1381171875" Y="-1.366267700195" />
                  <Point X="-28.136650390625" Y="-1.350052001953" />
                  <Point X="-28.14865625" Y="-1.321068359375" />
                  <Point X="-28.16846875" Y="-1.306335327148" />
                  <Point X="-28.187640625" Y="-1.295052124023" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-29.3978671875" Y="-1.443702392578" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.829783203125" Y="-1.393326782227" />
                  <Point X="-29.927392578125" Y="-1.011188110352" />
                  <Point X="-29.948451171875" Y="-0.863946960449" />
                  <Point X="-29.99839453125" Y="-0.514742248535" />
                  <Point X="-28.9671171875" Y="-0.238411727905" />
                  <Point X="-28.557462890625" Y="-0.128645401001" />
                  <Point X="-28.534234375" Y="-0.116107826233" />
                  <Point X="-28.514142578125" Y="-0.102163780212" />
                  <Point X="-28.5023203125" Y="-0.090642791748" />
                  <Point X="-28.49234375" Y="-0.06767074585" />
                  <Point X="-28.4856484375" Y="-0.046101638794" />
                  <Point X="-28.483400390625" Y="-0.031284263611" />
                  <Point X="-28.488201171875" Y="-0.008229101181" />
                  <Point X="-28.4948984375" Y="0.013348438263" />
                  <Point X="-28.50232421875" Y="0.028086112976" />
                  <Point X="-28.5218046875" Y="0.044921691895" />
                  <Point X="-28.541896484375" Y="0.058865730286" />
                  <Point X="-28.557462890625" Y="0.066085319519" />
                  <Point X="-29.63157421875" Y="0.353892791748" />
                  <Point X="-29.998185546875" Y="0.452126098633" />
                  <Point X="-29.980552734375" Y="0.571293884277" />
                  <Point X="-29.91764453125" Y="0.996414794922" />
                  <Point X="-29.875251953125" Y="1.152860351562" />
                  <Point X="-29.773515625" Y="1.528298828125" />
                  <Point X="-29.0469140625" Y="1.432640014648" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866699219" />
                  <Point X="-28.714744140625" Y="1.401213867188" />
                  <Point X="-28.67027734375" Y="1.41523425293" />
                  <Point X="-28.651533203125" Y="1.426056396484" />
                  <Point X="-28.639119140625" Y="1.443786499023" />
                  <Point X="-28.632314453125" Y="1.460215087891" />
                  <Point X="-28.61447265625" Y="1.503290771484" />
                  <Point X="-28.610712890625" Y="1.524605712891" />
                  <Point X="-28.61631640625" Y="1.545511962891" />
                  <Point X="-28.62452734375" Y="1.561284912109" />
                  <Point X="-28.646056640625" Y="1.602641601562" />
                  <Point X="-28.65996875" Y="1.619221679688" />
                  <Point X="-29.27608203125" Y="2.091982177734" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.404451171875" Y="2.368227783203" />
                  <Point X="-29.16001171875" Y="2.787007324219" />
                  <Point X="-29.04771875" Y="2.931346435547" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.3395078125" Y="3.031069335938" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.11489453125" Y="2.918368408203" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031505859375" Y="2.915775390625" />
                  <Point X="-28.013251953125" Y="2.927404296875" />
                  <Point X="-27.996486328125" Y="2.944169433594" />
                  <Point X="-27.95252734375" Y="2.988127685547" />
                  <Point X="-27.9408984375" Y="3.006382080078" />
                  <Point X="-27.938072265625" Y="3.027841064453" />
                  <Point X="-27.940138671875" Y="3.051460449219" />
                  <Point X="-27.945556640625" Y="3.113390625" />
                  <Point X="-27.95206640625" Y="3.134032714844" />
                  <Point X="-28.2250859375" Y="3.606914550781" />
                  <Point X="-28.30727734375" Y="3.749276611328" />
                  <Point X="-28.178607421875" Y="3.847927734375" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.576013671875" Y="4.272591796875" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-27.023037109375" Y="4.359525878906" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.92495703125" Y="4.259975585938" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835123046875" Y="4.2184921875" />
                  <Point X="-26.813806640625" Y="4.222250488281" />
                  <Point X="-26.78642578125" Y="4.233592285156" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.69690234375" Y="4.275744628906" />
                  <Point X="-26.68608203125" Y="4.294488769531" />
                  <Point X="-26.677169921875" Y="4.32275390625" />
                  <Point X="-26.653802734375" Y="4.396865234375" />
                  <Point X="-26.651916015625" Y="4.418426757812" />
                  <Point X="-26.682955078125" Y="4.654192871094" />
                  <Point X="-26.68913671875" Y="4.701141113281" />
                  <Point X="-26.519228515625" Y="4.74877734375" />
                  <Point X="-25.968091796875" Y="4.903296386719" />
                  <Point X="-25.75368359375" Y="4.928389648438" />
                  <Point X="-25.22419921875" Y="4.990357910156" />
                  <Point X="-25.0954921875" Y="4.51001953125" />
                  <Point X="-25.042140625" Y="4.310903808594" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.805654296875" Y="4.832985839844" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.621013671875" Y="4.975962402344" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.96240625" Y="4.882738769531" />
                  <Point X="-23.491541015625" Y="4.769057128906" />
                  <Point X="-23.376181640625" Y="4.727216308594" />
                  <Point X="-23.068970703125" Y="4.615788085938" />
                  <Point X="-22.957318359375" Y="4.563572265625" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.553439453125" Y="4.362294433594" />
                  <Point X="-22.26747265625" Y="4.195688964844" />
                  <Point X="-22.165751953125" Y="4.123352050781" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-22.5322578125" Y="2.915630859375" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491515380859" />
                  <Point X="-22.78107421875" Y="2.469349121094" />
                  <Point X="-22.7966171875" Y="2.411229003906" />
                  <Point X="-22.795642578125" Y="2.373159912109" />
                  <Point X="-22.789583984375" Y="2.322902587891" />
                  <Point X="-22.781318359375" Y="2.300812255859" />
                  <Point X="-22.76945703125" Y="2.283333251953" />
                  <Point X="-22.738359375" Y="2.237503662109" />
                  <Point X="-22.72505859375" Y="2.224203613281" />
                  <Point X="-22.707580078125" Y="2.212343261719" />
                  <Point X="-22.66175" Y="2.18124609375" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.620494140625" Y="2.170668945312" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.5513359375" Y="2.165946289062" />
                  <Point X="-22.529169921875" Y="2.171874023438" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-21.378615234375" Y="2.816157958984" />
                  <Point X="-21.005751953125" Y="3.031430664062" />
                  <Point X="-20.96703125" Y="2.977619140625" />
                  <Point X="-20.79740234375" Y="2.741874267578" />
                  <Point X="-20.740697265625" Y="2.64816796875" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.398181640625" Y="1.83341027832" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.72062890625" Y="1.583832763672" />
                  <Point X="-21.73658203125" Y="1.563020751953" />
                  <Point X="-21.77841015625" Y="1.508451416016" />
                  <Point X="-21.78687890625" Y="1.491500610352" />
                  <Point X="-21.792822265625" Y="1.470251586914" />
                  <Point X="-21.808404296875" Y="1.414536010742" />
                  <Point X="-21.809220703125" Y="1.390965087891" />
                  <Point X="-21.804341796875" Y="1.367322631836" />
                  <Point X="-21.79155078125" Y="1.305332275391" />
                  <Point X="-21.784353515625" Y="1.287955200195" />
                  <Point X="-21.7710859375" Y="1.267787963867" />
                  <Point X="-21.736296875" Y="1.214909790039" />
                  <Point X="-21.719052734375" Y="1.198819946289" />
                  <Point X="-21.69982421875" Y="1.187996459961" />
                  <Point X="-21.64941015625" Y="1.159617553711" />
                  <Point X="-21.63143359375" Y="1.153619750977" />
                  <Point X="-21.605435546875" Y="1.15018371582" />
                  <Point X="-21.537271484375" Y="1.141175048828" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.497935546875" Y="1.276281494141" />
                  <Point X="-20.1510234375" Y="1.32195324707" />
                  <Point X="-20.1336640625" Y="1.250641235352" />
                  <Point X="-20.060806640625" Y="0.95136730957" />
                  <Point X="-20.042939453125" Y="0.836605163574" />
                  <Point X="-20.002140625" Y="0.574556396484" />
                  <Point X="-20.899896484375" Y="0.334003143311" />
                  <Point X="-21.25883203125" Y="0.237826858521" />
                  <Point X="-21.270912109375" Y="0.232819351196" />
                  <Point X="-21.296453125" Y="0.218056167603" />
                  <Point X="-21.363421875" Y="0.179346832275" />
                  <Point X="-21.377734375" Y="0.166926849365" />
                  <Point X="-21.39305859375" Y="0.147399520874" />
                  <Point X="-21.433240234375" Y="0.096199172974" />
                  <Point X="-21.443013671875" Y="0.074735717773" />
                  <Point X="-21.44812109375" Y="0.04806243515" />
                  <Point X="-21.461515625" Y="-0.02187484169" />
                  <Point X="-21.461515625" Y="-0.04068523407" />
                  <Point X="-21.456408203125" Y="-0.067358520508" />
                  <Point X="-21.443013671875" Y="-0.137295791626" />
                  <Point X="-21.433240234375" Y="-0.158759246826" />
                  <Point X="-21.417916015625" Y="-0.178286407471" />
                  <Point X="-21.377734375" Y="-0.229486923218" />
                  <Point X="-21.363421875" Y="-0.241906906128" />
                  <Point X="-21.337880859375" Y="-0.256670257568" />
                  <Point X="-21.270912109375" Y="-0.295379425049" />
                  <Point X="-21.25883203125" Y="-0.300386932373" />
                  <Point X="-20.317705078125" Y="-0.552561340332" />
                  <Point X="-20.001931640625" Y="-0.637172424316" />
                  <Point X="-20.010890625" Y="-0.696593688965" />
                  <Point X="-20.051568359375" Y="-0.966412414551" />
                  <Point X="-20.0744609375" Y="-1.066729858398" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-21.17175390625" Y="-1.152430297852" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341186523" />
                  <Point X="-21.655291015625" Y="-1.109236938477" />
                  <Point X="-21.7867265625" Y="-1.137805053711" />
                  <Point X="-21.814552734375" Y="-1.154697143555" />
                  <Point X="-21.8448515625" Y="-1.191137817383" />
                  <Point X="-21.924296875" Y="-1.286685180664" />
                  <Point X="-21.935640625" Y="-1.31407043457" />
                  <Point X="-21.939982421875" Y="-1.361262695312" />
                  <Point X="-21.951369140625" Y="-1.485000732422" />
                  <Point X="-21.943638671875" Y="-1.516622314453" />
                  <Point X="-21.9158984375" Y="-1.559772705078" />
                  <Point X="-21.843158203125" Y="-1.672912841797" />
                  <Point X="-21.831537109375" Y="-1.685540649414" />
                  <Point X="-20.958166015625" Y="-2.355702636719" />
                  <Point X="-20.66092578125" Y="-2.583783691406" />
                  <Point X="-20.681201171875" Y="-2.616593261719" />
                  <Point X="-20.795865234375" Y="-2.802138183594" />
                  <Point X="-20.843216796875" Y="-2.869418457031" />
                  <Point X="-20.943310546875" Y="-3.011638427734" />
                  <Point X="-21.8758046875" Y="-2.473261962891" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.262658203125" Y="-2.253312744141" />
                  <Point X="-22.322318359375" Y="-2.242538085938" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219244873047" />
                  <Point X="-22.560484375" Y="-2.245329589844" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.7113984375" Y="-2.33468359375" />
                  <Point X="-22.737484375" Y="-2.384246826172" />
                  <Point X="-22.80587890625" Y="-2.514201660156" />
                  <Point X="-22.8108359375" Y="-2.546374755859" />
                  <Point X="-22.800060546875" Y="-2.60603515625" />
                  <Point X="-22.77180859375" Y="-2.762465087891" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.204677734375" Y="-3.750656005859" />
                  <Point X="-22.01332421875" Y="-4.082087646484" />
                  <Point X="-22.028640625" Y="-4.093027099609" />
                  <Point X="-22.16470703125" Y="-4.190215820312" />
                  <Point X="-22.217640625" Y="-4.224478515625" />
                  <Point X="-22.320224609375" Y="-4.29087890625" />
                  <Point X="-23.0339140625" Y="-3.360780029297" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.388291015625" Y="-2.942637695312" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.638546875" Y="-2.841638916016" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509033203" />
                  <Point X="-23.884357421875" Y="-2.909826171875" />
                  <Point X="-24.014650390625" Y="-3.018159423828" />
                  <Point X="-24.03154296875" Y="-3.045986083984" />
                  <Point X="-24.046400390625" Y="-3.114342773438" />
                  <Point X="-24.085357421875" Y="-3.293573974609" />
                  <Point X="-24.0860703125" Y="-3.310719970703" />
                  <Point X="-23.927021484375" Y="-4.518810058594" />
                  <Point X="-23.872357421875" Y="-4.934028320312" />
                  <Point X="-23.876927734375" Y="-4.935030273438" />
                  <Point X="-24.005650390625" Y="-4.96324609375" />
                  <Point X="-24.054560546875" Y="-4.972131835938" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#144" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.053352998312" Y="4.554240584169" Z="0.7" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.7" />
                  <Point X="-0.765653893088" Y="5.009330880415" Z="0.7" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.7" />
                  <Point X="-1.53888330275" Y="4.828200972733" Z="0.7" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.7" />
                  <Point X="-1.738685014862" Y="4.678946335746" Z="0.7" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.7" />
                  <Point X="-1.731013149215" Y="4.369069274014" Z="0.7" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.7" />
                  <Point X="-1.811718071487" Y="4.311066246672" Z="0.7" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.7" />
                  <Point X="-1.908026923497" Y="4.335606414254" Z="0.7" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.7" />
                  <Point X="-1.989526285562" Y="4.421243839443" Z="0.7" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.7" />
                  <Point X="-2.606453328131" Y="4.347579569456" Z="0.7" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.7" />
                  <Point X="-3.215185472508" Y="3.918887665597" Z="0.7" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.7" />
                  <Point X="-3.274543206752" Y="3.61319483068" Z="0.7" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.7" />
                  <Point X="-2.996106460286" Y="3.078382924333" Z="0.7" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.7" />
                  <Point X="-3.037998358773" Y="3.010805209114" Z="0.7" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.7" />
                  <Point X="-3.116693541274" Y="2.999458238887" Z="0.7" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.7" />
                  <Point X="-3.320664517617" Y="3.105650741994" Z="0.7" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.7" />
                  <Point X="-4.093338346231" Y="2.993328988567" Z="0.7" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.7" />
                  <Point X="-4.45378924524" Y="2.424712870077" Z="0.7" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.7" />
                  <Point X="-4.312675727185" Y="2.083594536508" Z="0.7" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.7" />
                  <Point X="-3.675033327029" Y="1.569477362828" Z="0.7" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.7" />
                  <Point X="-3.684664972897" Y="1.510628677537" Z="0.7" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.7" />
                  <Point X="-3.735936887104" Y="1.480179739505" Z="0.7" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.7" />
                  <Point X="-4.046545936777" Y="1.513492284786" Z="0.7" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.7" />
                  <Point X="-4.929668189389" Y="1.197217701228" Z="0.7" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.7" />
                  <Point X="-5.036170156351" Y="0.609893027973" Z="0.7" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.7" />
                  <Point X="-4.650673186543" Y="0.336876600282" Z="0.7" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.7" />
                  <Point X="-3.556470166445" Y="0.035125029704" Z="0.7" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.7" />
                  <Point X="-3.542110887939" Y="0.008229417671" Z="0.7" />
                  <Point X="-3.539556741714" Y="0" Z="0.7" />
                  <Point X="-3.546253713196" Y="-0.02157753339" Z="0.7" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.7" />
                  <Point X="-3.568898428678" Y="-0.04375095343" Z="0.7" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.7" />
                  <Point X="-3.98621460223" Y="-0.158835456474" Z="0.7" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.7" />
                  <Point X="-5.004104387898" Y="-0.83974596943" Z="0.7" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.7" />
                  <Point X="-4.884384936162" Y="-1.374420773663" Z="0.7" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.7" />
                  <Point X="-4.3974984588" Y="-1.461994625715" Z="0.7" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.7" />
                  <Point X="-3.199987896567" Y="-1.318146519182" Z="0.7" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.7" />
                  <Point X="-3.19825366347" Y="-1.343984203209" Z="0.7" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.7" />
                  <Point X="-3.559994216577" Y="-1.628138236735" Z="0.7" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.7" />
                  <Point X="-4.290400079136" Y="-2.707986658665" Z="0.7" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.7" />
                  <Point X="-3.958074331378" Y="-3.174017995086" Z="0.7" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.7" />
                  <Point X="-3.506247891613" Y="-3.094394550144" Z="0.7" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.7" />
                  <Point X="-2.560280735316" Y="-2.568049733816" Z="0.7" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.7" />
                  <Point X="-2.761022463025" Y="-2.928830490879" Z="0.7" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.7" />
                  <Point X="-3.003520936032" Y="-4.09046094365" Z="0.7" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.7" />
                  <Point X="-2.572420281254" Y="-4.374434056027" Z="0.7" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.7" />
                  <Point X="-2.389026236767" Y="-4.368622356368" Z="0.7" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.7" />
                  <Point X="-2.039478050199" Y="-4.031673492466" Z="0.7" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.7" />
                  <Point X="-1.744141488809" Y="-3.998773624641" Z="0.7" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.7" />
                  <Point X="-1.489807102284" Y="-4.15246215987" Z="0.7" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.7" />
                  <Point X="-1.381589590865" Y="-4.429220159762" Z="0.7" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.7" />
                  <Point X="-1.378191764104" Y="-4.614356200169" Z="0.7" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.7" />
                  <Point X="-1.199041045831" Y="-4.934578513964" Z="0.7" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.7" />
                  <Point X="-0.900421740017" Y="-4.997700173976" Z="0.7" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.7" />
                  <Point X="-0.707071366691" Y="-4.601010366945" Z="0.7" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.7" />
                  <Point X="-0.298562751171" Y="-3.348002549857" Z="0.7" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.7" />
                  <Point X="-0.069950048917" Y="-3.225949282224" Z="0.7" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.7" />
                  <Point X="0.183409030445" Y="-3.261162763064" Z="0.7" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.7" />
                  <Point X="0.371883108261" Y="-3.453643205911" Z="0.7" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.7" />
                  <Point X="0.52768349371" Y="-3.931525662871" Z="0.7" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.7" />
                  <Point X="0.948219414019" Y="-4.990046538207" Z="0.7" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.7" />
                  <Point X="1.127620944161" Y="-4.952590767458" Z="0.7" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.7" />
                  <Point X="1.116393894333" Y="-4.481003890599" Z="0.7" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.7" />
                  <Point X="0.996302414766" Y="-3.093682633852" Z="0.7" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.7" />
                  <Point X="1.141451941776" Y="-2.91699254984" Z="0.7" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.7" />
                  <Point X="1.359877418474" Y="-2.860148414285" Z="0.7" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.7" />
                  <Point X="1.578512527988" Y="-2.953415706167" Z="0.7" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.7" />
                  <Point X="1.920261894051" Y="-3.35993807436" Z="0.7" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.7" />
                  <Point X="2.803373273275" Y="-4.235172633202" Z="0.7" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.7" />
                  <Point X="2.994265496227" Y="-4.102433867319" Z="0.7" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.7" />
                  <Point X="2.83246630322" Y="-3.694376062426" Z="0.7" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.7" />
                  <Point X="2.242985763225" Y="-2.565868711167" Z="0.7" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.7" />
                  <Point X="2.300605414219" Y="-2.376253416991" Z="0.7" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.7" />
                  <Point X="2.456644961381" Y="-2.258295895748" Z="0.7" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.7" />
                  <Point X="2.662638092345" Y="-2.260462246266" Z="0.7" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.7" />
                  <Point X="3.093037534681" Y="-2.485283197054" Z="0.7" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.7" />
                  <Point X="4.191514831845" Y="-2.866915817686" Z="0.7" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.7" />
                  <Point X="4.355175886266" Y="-2.611598338208" Z="0.7" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.7" />
                  <Point X="4.066114754763" Y="-2.284754971357" Z="0.7" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.7" />
                  <Point X="3.1200041733" Y="-1.501452837403" Z="0.7" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.7" />
                  <Point X="3.103648842307" Y="-1.334564463649" Z="0.7" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.7" />
                  <Point X="3.187436227175" Y="-1.191824836551" Z="0.7" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.7" />
                  <Point X="3.349171663013" Y="-1.126815995452" Z="0.7" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.7" />
                  <Point X="3.815563489917" Y="-1.170722566858" Z="0.7" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.7" />
                  <Point X="4.968127608171" Y="-1.046573803354" Z="0.7" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.7" />
                  <Point X="5.032394817167" Y="-0.672767447834" Z="0.7" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.7" />
                  <Point X="4.689080162713" Y="-0.472984968998" Z="0.7" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.7" />
                  <Point X="3.680983847814" Y="-0.182101440123" Z="0.7" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.7" />
                  <Point X="3.615261403583" Y="-0.116137774597" Z="0.7" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.7" />
                  <Point X="3.58654295334" Y="-0.02667325524" Z="0.7" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.7" />
                  <Point X="3.594828497084" Y="0.069937275988" Z="0.7" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.7" />
                  <Point X="3.640118034817" Y="0.147810963978" Z="0.7" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.7" />
                  <Point X="3.722411566538" Y="0.206047404119" Z="0.7" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.7" />
                  <Point X="4.106887654593" Y="0.316986963771" Z="0.7" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.7" />
                  <Point X="5.000307868661" Y="0.875577168352" Z="0.7" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.7" />
                  <Point X="4.908761008699" Y="1.293748032518" Z="0.7" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.7" />
                  <Point X="4.489381810127" Y="1.357133869027" Z="0.7" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.7" />
                  <Point X="3.394957525212" Y="1.231032771438" Z="0.7" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.7" />
                  <Point X="3.318546068957" Y="1.262847604337" Z="0.7" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.7" />
                  <Point X="3.26452923323" Y="1.326549266376" Z="0.7" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.7" />
                  <Point X="3.238470267049" Y="1.40870679742" Z="0.7" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.7" />
                  <Point X="3.249173425876" Y="1.48806453171" Z="0.7" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.7" />
                  <Point X="3.296945046753" Y="1.563883008694" Z="0.7" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.7" />
                  <Point X="3.626099072717" Y="1.825022606247" Z="0.7" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.7" />
                  <Point X="4.295922214088" Y="2.705334346938" Z="0.7" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.7" />
                  <Point X="4.067397222373" Y="3.038102237239" Z="0.7" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.7" />
                  <Point X="3.590228243866" Y="2.890739282489" Z="0.7" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.7" />
                  <Point X="2.451757935059" Y="2.251456726672" Z="0.7" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.7" />
                  <Point X="2.379334338358" Y="2.251589276414" Z="0.7" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.7" />
                  <Point X="2.314337010788" Y="2.284997900013" Z="0.7" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.7" />
                  <Point X="2.265760700222" Y="2.342687849594" Z="0.7" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.7" />
                  <Point X="2.247840426344" Y="2.410424105474" Z="0.7" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.7" />
                  <Point X="2.261071215986" Y="2.487711640823" Z="0.7" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.7" />
                  <Point X="2.50488607037" Y="2.921910838089" Z="0.7" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.7" />
                  <Point X="2.857067342783" Y="4.195379092222" Z="0.7" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.7" />
                  <Point X="2.465573629946" Y="4.436777312369" Z="0.7" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.7" />
                  <Point X="2.05770641194" Y="4.640144369404" Z="0.7" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.7" />
                  <Point X="1.634709323692" Y="4.805499789967" Z="0.7" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.7" />
                  <Point X="1.043171283477" Y="4.962622515719" Z="0.7" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.7" />
                  <Point X="0.378035863475" Y="5.056970447716" Z="0.7" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.7" />
                  <Point X="0.139891563241" Y="4.877206921782" Z="0.7" />
                  <Point X="0" Y="4.355124473572" Z="0.7" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>