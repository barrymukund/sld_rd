<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#119" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="337" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000473632812" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.428609375" Y="-3.542700439453" />
                  <Point X="-24.4366953125" Y="-3.512524169922" />
                  <Point X="-24.444095703125" Y="-3.493433105469" />
                  <Point X="-24.45462109375" Y="-3.473103759766" />
                  <Point X="-24.460939453125" Y="-3.462615234375" />
                  <Point X="-24.62136328125" Y="-3.2314765625" />
                  <Point X="-24.642890625" Y="-3.207521972656" />
                  <Point X="-24.67165234375" Y="-3.187322753906" />
                  <Point X="-24.69150390625" Y="-3.178310058594" />
                  <Point X="-24.7026171875" Y="-3.174081787109" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.9796015625" Y="-3.091593017578" />
                  <Point X="-25.011787109375" Y="-3.092229736328" />
                  <Point X="-25.031720703125" Y="-3.096063964844" />
                  <Point X="-25.0419375" Y="-3.098623046875" />
                  <Point X="-25.29018359375" Y="-3.175669189453" />
                  <Point X="-25.319505859375" Y="-3.188985107422" />
                  <Point X="-25.3475859375" Y="-3.210509765625" />
                  <Point X="-25.362408203125" Y="-3.22708984375" />
                  <Point X="-25.36962890625" Y="-3.236238769531" />
                  <Point X="-25.53005078125" Y="-3.467377685547" />
                  <Point X="-25.5381875" Y="-3.481570800781" />
                  <Point X="-25.550990234375" Y="-3.512523925781" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.079337890625" Y="-4.845350585938" />
                  <Point X="-26.0810859375" Y="-4.844900878906" />
                  <Point X="-26.246416015625" Y="-4.802362304688" />
                  <Point X="-26.215935546875" Y="-4.570831542969" />
                  <Point X="-26.214439453125" Y="-4.543641601562" />
                  <Point X="-26.216193359375" Y="-4.521209960938" />
                  <Point X="-26.21773046875" Y="-4.510081542969" />
                  <Point X="-26.277037109375" Y="-4.211932128906" />
                  <Point X="-26.28703125" Y="-4.181770996094" />
                  <Point X="-26.30531640625" Y="-4.151690917969" />
                  <Point X="-26.320361328125" Y="-4.134965820312" />
                  <Point X="-26.3283515625" Y="-4.127074707031" />
                  <Point X="-26.556904296875" Y="-3.926639160156" />
                  <Point X="-26.583205078125" Y="-3.908789550781" />
                  <Point X="-26.6160234375" Y="-3.896060058594" />
                  <Point X="-26.638138671875" Y="-3.891951416016" />
                  <Point X="-26.64927734375" Y="-3.890556640625" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.984353515625" Y="-3.872525634766" />
                  <Point X="-27.0181328125" Y="-3.882403320312" />
                  <Point X="-27.038177734375" Y="-3.89260546875" />
                  <Point X="-27.047865234375" Y="-3.898280761719" />
                  <Point X="-27.300623046875" Y="-4.067169677734" />
                  <Point X="-27.312787109375" Y="-4.076823242188" />
                  <Point X="-27.335103515625" Y="-4.099463867187" />
                  <Point X="-27.4801484375" Y="-4.288489257813" />
                  <Point X="-27.80171875" Y="-4.089380126953" />
                  <Point X="-27.80412890625" Y="-4.087524658203" />
                  <Point X="-28.10472265625" Y="-3.856077880859" />
                  <Point X="-27.4387890625" Y="-2.702647216797" />
                  <Point X="-27.42376171875" Y="-2.676620849609" />
                  <Point X="-27.41286328125" Y="-2.647681396484" />
                  <Point X="-27.406587890625" Y="-2.616180664062" />
                  <Point X="-27.40559765625" Y="-2.585017089844" />
                  <Point X="-27.41475" Y="-2.555210693359" />
                  <Point X="-27.4293203125" Y="-2.526004882812" />
                  <Point X="-27.44715234375" Y="-2.501239746094" />
                  <Point X="-27.464150390625" Y="-2.484241455078" />
                  <Point X="-27.489306640625" Y="-2.466214111328" />
                  <Point X="-27.518134765625" Y="-2.451996826172" />
                  <Point X="-27.547751953125" Y="-2.44301171875" />
                  <Point X="-27.578685546875" Y="-2.444023681641" />
                  <Point X="-27.610212890625" Y="-2.450294189453" />
                  <Point X="-27.639181640625" Y="-2.461196777344" />
                  <Point X="-28.8180234375" Y="-3.141801269531" />
                  <Point X="-29.08285546875" Y="-2.7938671875" />
                  <Point X="-29.0845859375" Y="-2.790965087891" />
                  <Point X="-29.306142578125" Y="-2.419449707031" />
                  <Point X="-28.13224609375" Y="-1.518687133789" />
                  <Point X="-28.105955078125" Y="-1.498512939453" />
                  <Point X="-28.084259765625" Y="-1.475110595703" />
                  <Point X="-28.066138671875" Y="-1.44737878418" />
                  <Point X="-28.053701171875" Y="-1.419231201172" />
                  <Point X="-28.04615234375" Y="-1.390084228516" />
                  <Point X="-28.04334765625" Y="-1.359646728516" />
                  <Point X="-28.045560546875" Y="-1.327963623047" />
                  <Point X="-28.05268359375" Y="-1.297937011719" />
                  <Point X="-28.069052734375" Y="-1.27177722168" />
                  <Point X="-28.090439453125" Y="-1.247505615234" />
                  <Point X="-28.113529296875" Y="-1.228439331055" />
                  <Point X="-28.139453125" Y="-1.213181396484" />
                  <Point X="-28.168716796875" Y="-1.20195703125" />
                  <Point X="-28.20060546875" Y="-1.195474975586" />
                  <Point X="-28.2319296875" Y="-1.194383911133" />
                  <Point X="-29.732099609375" Y="-1.391885131836" />
                  <Point X="-29.834076171875" Y="-0.992649414063" />
                  <Point X="-29.834533203125" Y="-0.98946081543" />
                  <Point X="-29.892421875" Y="-0.584698364258" />
                  <Point X="-28.56282421875" Y="-0.228433273315" />
                  <Point X="-28.532875" Y="-0.220408279419" />
                  <Point X="-28.51692578125" Y="-0.214562667847" />
                  <Point X="-28.487169921875" Y="-0.199081420898" />
                  <Point X="-28.4599765625" Y="-0.180208267212" />
                  <Point X="-28.437125" Y="-0.157781234741" />
                  <Point X="-28.41766796875" Y="-0.130838150024" />
                  <Point X="-28.403955078125" Y="-0.103378517151" />
                  <Point X="-28.39491796875" Y="-0.074259506226" />
                  <Point X="-28.390650390625" Y="-0.045518306732" />
                  <Point X="-28.3908359375" Y="-0.015274533272" />
                  <Point X="-28.395103515625" Y="0.012302587509" />
                  <Point X="-28.40416796875" Y="0.041507965088" />
                  <Point X="-28.418654296875" Y="0.070021026611" />
                  <Point X="-28.438458984375" Y="0.09666493988" />
                  <Point X="-28.460537109375" Y="0.118036712646" />
                  <Point X="-28.48773046875" Y="0.136910171509" />
                  <Point X="-28.501923828125" Y="0.14504675293" />
                  <Point X="-28.532875" Y="0.157848144531" />
                  <Point X="-29.891814453125" Y="0.521975280762" />
                  <Point X="-29.824486328125" Y="0.976974914551" />
                  <Point X="-29.823564453125" Y="0.980378540039" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-28.7871328125" Y="1.302619506836" />
                  <Point X="-28.7656640625" Y="1.29979296875" />
                  <Point X="-28.74496484375" Y="1.299343505859" />
                  <Point X="-28.723384765625" Y="1.301236083984" />
                  <Point X="-28.703083984375" Y="1.305280273438" />
                  <Point X="-28.7018984375" Y="1.305654296875" />
                  <Point X="-28.6417109375" Y="1.324630859375" />
                  <Point X="-28.6227578125" Y="1.332973388672" />
                  <Point X="-28.603994140625" Y="1.343812744141" />
                  <Point X="-28.587294921875" Y="1.356066162109" />
                  <Point X="-28.573650390625" Y="1.371646972656" />
                  <Point X="-28.561236328125" Y="1.389406982422" />
                  <Point X="-28.55130078125" Y="1.407553588867" />
                  <Point X="-28.55085546875" Y="1.408631103516" />
                  <Point X="-28.526705078125" Y="1.466934082031" />
                  <Point X="-28.52091796875" Y="1.486778686523" />
                  <Point X="-28.517158203125" Y="1.50808215332" />
                  <Point X="-28.515802734375" Y="1.528713500977" />
                  <Point X="-28.51894140625" Y="1.549149658203" />
                  <Point X="-28.524533203125" Y="1.57004699707" />
                  <Point X="-28.532025390625" Y="1.589332519531" />
                  <Point X="-28.532650390625" Y="1.590533081055" />
                  <Point X="-28.561791015625" Y="1.646509643555" />
                  <Point X="-28.573283203125" Y="1.663707519531" />
                  <Point X="-28.5871953125" Y="1.680287109375" />
                  <Point X="-28.60213671875" Y="1.694590332031" />
                  <Point X="-29.351859375" Y="2.269873291016" />
                  <Point X="-29.0811484375" Y="2.733666503906" />
                  <Point X="-29.07870703125" Y="2.736804199219" />
                  <Point X="-28.75050390625" Y="3.158662109375" />
                  <Point X="-28.219841796875" Y="2.852283935547" />
                  <Point X="-28.20665625" Y="2.844671142578" />
                  <Point X="-28.187724609375" Y="2.836340820312" />
                  <Point X="-28.16708203125" Y="2.82983203125" />
                  <Point X="-28.146796875" Y="2.825796630859" />
                  <Point X="-28.1450703125" Y="2.825645507812" />
                  <Point X="-28.061248046875" Y="2.818312011719" />
                  <Point X="-28.04057421875" Y="2.81876171875" />
                  <Point X="-28.019125" Y="2.821583496094" />
                  <Point X="-27.999041015625" Y="2.826494628906" />
                  <Point X="-27.98049609375" Y="2.835634033203" />
                  <Point X="-27.96224609375" Y="2.847252685547" />
                  <Point X="-27.946109375" Y="2.860195556641" />
                  <Point X="-27.944849609375" Y="2.861454833984" />
                  <Point X="-27.8853515625" Y="2.920952636719" />
                  <Point X="-27.872408203125" Y="2.937079101562" />
                  <Point X="-27.860783203125" Y="2.955323974609" />
                  <Point X="-27.851638671875" Y="2.973864746094" />
                  <Point X="-27.84671875" Y="2.9939453125" />
                  <Point X="-27.843888671875" Y="3.015393554688" />
                  <Point X="-27.843421875" Y="3.035966308594" />
                  <Point X="-27.843583984375" Y="3.037848876953" />
                  <Point X="-27.85091796875" Y="3.121671386719" />
                  <Point X="-27.854955078125" Y="3.141960205078" />
                  <Point X="-27.861462890625" Y="3.162598632812" />
                  <Point X="-27.86979296875" Y="3.181529785156" />
                  <Point X="-28.183333984375" Y="3.724595703125" />
                  <Point X="-27.700623046875" Y="4.094684570313" />
                  <Point X="-27.696787109375" Y="4.096815917969" />
                  <Point X="-27.16703515625" Y="4.391134277344" />
                  <Point X="-27.04723046875" Y="4.235" />
                  <Point X="-27.043193359375" Y="4.229739257812" />
                  <Point X="-27.028888671875" Y="4.214796875" />
                  <Point X="-27.012310546875" Y="4.200886230469" />
                  <Point X="-26.99511328125" Y="4.189395019531" />
                  <Point X="-26.99319140625" Y="4.18839453125" />
                  <Point X="-26.899896484375" Y="4.139828125" />
                  <Point X="-26.88061328125" Y="4.132330078125" />
                  <Point X="-26.859703125" Y="4.126728027344" />
                  <Point X="-26.839255859375" Y="4.12358203125" />
                  <Point X="-26.818611328125" Y="4.124937011719" />
                  <Point X="-26.79729296875" Y="4.128698730469" />
                  <Point X="-26.777451171875" Y="4.134482421875" />
                  <Point X="-26.775455078125" Y="4.135309082031" />
                  <Point X="-26.678283203125" Y="4.175559082031" />
                  <Point X="-26.6601328125" Y="4.185517578125" />
                  <Point X="-26.64239453125" Y="4.19794140625" />
                  <Point X="-26.6268359375" Y="4.211591308594" />
                  <Point X="-26.614603515625" Y="4.228287109375" />
                  <Point X="-26.603783203125" Y="4.247045898438" />
                  <Point X="-26.59545703125" Y="4.265991210938" />
                  <Point X="-26.5948203125" Y="4.268011230469" />
                  <Point X="-26.56319921875" Y="4.368296875" />
                  <Point X="-26.5591640625" Y="4.388583007812" />
                  <Point X="-26.55727734375" Y="4.410143554688" />
                  <Point X="-26.557728515625" Y="4.430825195312" />
                  <Point X="-26.584201171875" Y="4.631897949219" />
                  <Point X="-25.94963671875" Y="4.809807617188" />
                  <Point X="-25.9449921875" Y="4.810351074219" />
                  <Point X="-25.2947109375" Y="4.886457519531" />
                  <Point X="-25.1378046875" Y="4.300873046875" />
                  <Point X="-25.133904296875" Y="4.28631640625" />
                  <Point X="-25.12112890625" Y="4.258124023438" />
                  <Point X="-25.10326953125" Y="4.231396484375" />
                  <Point X="-25.08211328125" Y="4.208808105469" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.853783203125" Y="4.286315429688" />
                  <Point X="-24.692580078125" Y="4.8879375" />
                  <Point X="-24.15595703125" Y="4.83173828125" />
                  <Point X="-24.15210546875" Y="4.83080859375" />
                  <Point X="-23.51896875" Y="4.677949707031" />
                  <Point X="-23.517919921875" Y="4.677569824219" />
                  <Point X="-23.105361328125" Y="4.527931640625" />
                  <Point X="-23.1029296875" Y="4.526793945312" />
                  <Point X="-22.705431640625" Y="4.3408984375" />
                  <Point X="-22.703044921875" Y="4.339508300781" />
                  <Point X="-22.319025390625" Y="4.115778808594" />
                  <Point X="-22.31680859375" Y="4.114201660156" />
                  <Point X="-22.056740234375" Y="3.929254638672" />
                  <Point X="-22.83502734375" Y="2.581220214844" />
                  <Point X="-22.85241796875" Y="2.551096435547" />
                  <Point X="-22.85845703125" Y="2.538616210938" />
                  <Point X="-22.86735546875" Y="2.514437255859" />
                  <Point X="-22.888392578125" Y="2.435771484375" />
                  <Point X="-22.891462890625" Y="2.416657470703" />
                  <Point X="-22.892626953125" Y="2.3963203125" />
                  <Point X="-22.89209765625" Y="2.379520263672" />
                  <Point X="-22.883900390625" Y="2.311530517578" />
                  <Point X="-22.878556640625" Y="2.289602294922" />
                  <Point X="-22.8702890625" Y="2.267512207031" />
                  <Point X="-22.859923828125" Y="2.247465576172" />
                  <Point X="-22.859056640625" Y="2.246187744141" />
                  <Point X="-22.81696484375" Y="2.184156982422" />
                  <Point X="-22.804568359375" Y="2.169378173828" />
                  <Point X="-22.7899921875" Y="2.155211425781" />
                  <Point X="-22.77712109375" Y="2.144724853516" />
                  <Point X="-22.71508984375" Y="2.102634521484" />
                  <Point X="-22.695056640625" Y="2.092276123047" />
                  <Point X="-22.67298046875" Y="2.08401171875" />
                  <Point X="-22.651072265625" Y="2.07866796875" />
                  <Point X="-22.649634765625" Y="2.078494140625" />
                  <Point X="-22.581611328125" Y="2.070291748047" />
                  <Point X="-22.5620703125" Y="2.069960449219" />
                  <Point X="-22.541546875" Y="2.071731689453" />
                  <Point X="-22.525173828125" Y="2.074604492188" />
                  <Point X="-22.4465078125" Y="2.095640625" />
                  <Point X="-22.434716796875" Y="2.099638183594" />
                  <Point X="-22.41146484375" Y="2.110145263672" />
                  <Point X="-21.032673828125" Y="2.906190429688" />
                  <Point X="-20.87671875" Y="2.689447265625" />
                  <Point X="-20.87548828125" Y="2.687415771484" />
                  <Point X="-20.73780078125" Y="2.459883789063" />
                  <Point X="-21.74631640625" Y="1.686021850586" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.7796875" Y="1.659108764648" />
                  <Point X="-21.797193359375" Y="1.64010559082" />
                  <Point X="-21.85380859375" Y="1.566245849609" />
                  <Point X="-21.864015625" Y="1.549643432617" />
                  <Point X="-21.872919921875" Y="1.531138427734" />
                  <Point X="-21.8788046875" Y="1.515533081055" />
                  <Point X="-21.89989453125" Y="1.440122192383" />
                  <Point X="-21.90334765625" Y="1.417805664062" />
                  <Point X="-21.90416015625" Y="1.394214355469" />
                  <Point X="-21.902248046875" Y="1.371707397461" />
                  <Point X="-21.901890625" Y="1.369978881836" />
                  <Point X="-21.88458984375" Y="1.286134521484" />
                  <Point X="-21.878720703125" Y="1.267563476563" />
                  <Point X="-21.870552734375" Y="1.248712158203" />
                  <Point X="-21.862748046875" Y="1.234267822266" />
                  <Point X="-21.815662109375" Y="1.162697021484" />
                  <Point X="-21.8011015625" Y="1.145446044922" />
                  <Point X="-21.783849609375" Y="1.129351196289" />
                  <Point X="-21.765625" Y="1.116018798828" />
                  <Point X="-21.764240234375" Y="1.115239257812" />
                  <Point X="-21.69601171875" Y="1.076832519531" />
                  <Point X="-21.67790234375" Y="1.068990844727" />
                  <Point X="-21.658025390625" Y="1.062741699219" />
                  <Point X="-21.64198046875" Y="1.05918737793" />
                  <Point X="-21.54971875" Y="1.046993896484" />
                  <Point X="-21.537294921875" Y="1.046174926758" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-20.22316015625" Y="1.21663671875" />
                  <Point X="-20.15405859375" Y="0.932778930664" />
                  <Point X="-20.153673828125" Y="0.930309753418" />
                  <Point X="-20.109134765625" Y="0.644238830566" />
                  <Point X="-21.257177734375" Y="0.336621368408" />
                  <Point X="-21.283419921875" Y="0.329589813232" />
                  <Point X="-21.29683203125" Y="0.324896148682" />
                  <Point X="-21.3203203125" Y="0.313988647461" />
                  <Point X="-21.410962890625" Y="0.261595489502" />
                  <Point X="-21.4268671875" Y="0.250055465698" />
                  <Point X="-21.44230078125" Y="0.236207336426" />
                  <Point X="-21.45358984375" Y="0.224148193359" />
                  <Point X="-21.507974609375" Y="0.15484815979" />
                  <Point X="-21.519703125" Y="0.135558792114" />
                  <Point X="-21.529478515625" Y="0.114085159302" />
                  <Point X="-21.53632421875" Y="0.092575401306" />
                  <Point X="-21.5366953125" Y="0.0906354599" />
                  <Point X="-21.5548203125" Y="-0.004006072044" />
                  <Point X="-21.5565" Y="-0.023582019806" />
                  <Point X="-21.556126953125" Y="-0.044341777802" />
                  <Point X="-21.554447265625" Y="-0.060504112244" />
                  <Point X="-21.536318359375" Y="-0.155164672852" />
                  <Point X="-21.52946484375" Y="-0.176678527832" />
                  <Point X="-21.519681640625" Y="-0.198154281616" />
                  <Point X="-21.507947265625" Y="-0.217441833496" />
                  <Point X="-21.506853515625" Y="-0.218835876465" />
                  <Point X="-21.45246875" Y="-0.288135925293" />
                  <Point X="-21.438595703125" Y="-0.302430175781" />
                  <Point X="-21.422416015625" Y="-0.315930084229" />
                  <Point X="-21.409095703125" Y="-0.325234832764" />
                  <Point X="-21.318453125" Y="-0.37762802124" />
                  <Point X="-21.307291015625" Y="-0.3831378479" />
                  <Point X="-21.283419921875" Y="-0.392149963379" />
                  <Point X="-20.108525390625" Y="-0.706961853027" />
                  <Point X="-20.144974609375" Y="-0.948725769043" />
                  <Point X="-20.145474609375" Y="-0.950911987305" />
                  <Point X="-20.19882421875" Y="-1.18469909668" />
                  <Point X="-21.54518359375" Y="-1.007447509766" />
                  <Point X="-21.5756171875" Y="-1.003440856934" />
                  <Point X="-21.594890625" Y="-1.002877258301" />
                  <Point X="-21.615701171875" Y="-1.004386962891" />
                  <Point X="-21.629005859375" Y="-1.006305419922" />
                  <Point X="-21.806904296875" Y="-1.04497265625" />
                  <Point X="-21.8385546875" Y="-1.058188842773" />
                  <Point X="-21.868595703125" Y="-1.077745117188" />
                  <Point X="-21.889814453125" Y="-1.096624023438" />
                  <Point X="-21.99734375" Y="-1.225947875977" />
                  <Point X="-22.013154296875" Y="-1.253079467773" />
                  <Point X="-22.02481640625" Y="-1.283915527344" />
                  <Point X="-22.03055859375" Y="-1.308816894531" />
                  <Point X="-22.04596875" Y="-1.47629675293" />
                  <Point X="-22.044890625" Y="-1.508471679688" />
                  <Point X="-22.036259765625" Y="-1.541582275391" />
                  <Point X="-22.027720703125" Y="-1.559905517578" />
                  <Point X="-22.021521484375" Y="-1.571152099609" />
                  <Point X="-21.923068359375" Y="-1.724287841797" />
                  <Point X="-21.91306640625" Y="-1.737238647461" />
                  <Point X="-21.88937109375" Y="-1.760908569336" />
                  <Point X="-20.786875" Y="-2.606883056641" />
                  <Point X="-20.87519140625" Y="-2.749791015625" />
                  <Point X="-20.876220703125" Y="-2.751253173828" />
                  <Point X="-20.971017578125" Y="-2.8859453125" />
                  <Point X="-22.171939453125" Y="-2.192592041016" />
                  <Point X="-22.199044921875" Y="-2.176942871094" />
                  <Point X="-22.2170390625" Y="-2.168913818359" />
                  <Point X="-22.237513671875" Y="-2.162223632813" />
                  <Point X="-22.250134765625" Y="-2.159037597656" />
                  <Point X="-22.46186328125" Y="-2.120799560547" />
                  <Point X="-22.494025390625" Y="-2.119082275391" />
                  <Point X="-22.52796484375" Y="-2.124885009766" />
                  <Point X="-22.547259765625" Y="-2.131962402344" />
                  <Point X="-22.5587890625" Y="-2.137083740234" />
                  <Point X="-22.73468359375" Y="-2.229655761719" />
                  <Point X="-22.759998046875" Y="-2.249020507812" />
                  <Point X="-22.782865234375" Y="-2.273603759766" />
                  <Point X="-22.797375" Y="-2.294062744141" />
                  <Point X="-22.889947265625" Y="-2.469957275391" />
                  <Point X="-22.90126953125" Y="-2.50012109375" />
                  <Point X="-22.90594140625" Y="-2.534453857422" />
                  <Point X="-22.904935546875" Y="-2.555323974609" />
                  <Point X="-22.903533203125" Y="-2.567635009766" />
                  <Point X="-22.865296875" Y="-2.779349121094" />
                  <Point X="-22.86101171875" Y="-2.795140380859" />
                  <Point X="-22.848177734375" Y="-2.826078125" />
                  <Point X="-22.13871484375" Y="-4.054904541016" />
                  <Point X="-22.21817578125" Y="-4.111661132813" />
                  <Point X="-22.21930078125" Y="-4.112389160156" />
                  <Point X="-22.298234375" Y="-4.163481445312" />
                  <Point X="-23.22076953125" Y="-2.961211669922" />
                  <Point X="-23.241451171875" Y="-2.934256591797" />
                  <Point X="-23.2553046875" Y="-2.9196953125" />
                  <Point X="-23.272236328125" Y="-2.905307861328" />
                  <Point X="-23.282376953125" Y="-2.897791259766" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.520255859375" Y="-2.749644287109" />
                  <Point X="-23.55420703125" Y="-2.742002929688" />
                  <Point X="-23.575369140625" Y="-2.741215820312" />
                  <Point X="-23.58760546875" Y="-2.741549804688" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.847201171875" Y="-2.770960693359" />
                  <Point X="-23.87821875" Y="-2.785324462891" />
                  <Point X="-23.89903515625" Y="-2.798481689453" />
                  <Point X="-24.07538671875" Y="-2.945111572266" />
                  <Point X="-24.097384765625" Y="-2.968638427734" />
                  <Point X="-24.114962890625" Y="-2.998916748047" />
                  <Point X="-24.12216796875" Y="-3.019240722656" />
                  <Point X="-24.1254609375" Y="-3.030805908203" />
                  <Point X="-24.178189453125" Y="-3.273396240234" />
                  <Point X="-24.180275390625" Y="-3.289627197266" />
                  <Point X="-24.1802578125" Y="-3.323118652344" />
                  <Point X="-23.97793359375" Y="-4.859915527344" />
                  <Point X="-24.024310546875" Y="-4.870081542969" />
                  <Point X="-24.0253984375" Y="-4.870279296875" />
                  <Point X="-24.07068359375" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.058421875" Y="-4.752637695312" />
                  <Point X="-26.141244140625" Y="-4.731327636719" />
                  <Point X="-26.121748046875" Y="-4.583230957031" />
                  <Point X="-26.121078125" Y="-4.57605078125" />
                  <Point X="-26.11958203125" Y="-4.548860839844" />
                  <Point X="-26.119728515625" Y="-4.536236328125" />
                  <Point X="-26.121482421875" Y="-4.5138046875" />
                  <Point X="-26.124556640625" Y="-4.491547851562" />
                  <Point X="-26.18386328125" Y="-4.1933984375" />
                  <Point X="-26.186859375" Y="-4.18205078125" />
                  <Point X="-26.196853515625" Y="-4.151889648438" />
                  <Point X="-26.205853515625" Y="-4.132424316406" />
                  <Point X="-26.224138671875" Y="-4.102344238281" />
                  <Point X="-26.2346875" Y="-4.088157226562" />
                  <Point X="-26.249732421875" Y="-4.071432128906" />
                  <Point X="-26.265712890625" Y="-4.055649902344" />
                  <Point X="-26.494265625" Y="-3.855214355469" />
                  <Point X="-26.503556640625" Y="-3.848032714844" />
                  <Point X="-26.529857421875" Y="-3.830183105469" />
                  <Point X="-26.548849609375" Y="-3.82021875" />
                  <Point X="-26.58166796875" Y="-3.807489257812" />
                  <Point X="-26.598671875" Y="-3.802658203125" />
                  <Point X="-26.620787109375" Y="-3.798549560547" />
                  <Point X="-26.643064453125" Y="-3.795760009766" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9581484375" Y="-3.7758359375" />
                  <Point X="-26.989884765625" Y="-3.777686767578" />
                  <Point X="-27.011017578125" Y="-3.781343994141" />
                  <Point X="-27.044796875" Y="-3.791221679688" />
                  <Point X="-27.061224609375" Y="-3.797738525391" />
                  <Point X="-27.08126953125" Y="-3.807940673828" />
                  <Point X="-27.10064453125" Y="-3.819291259766" />
                  <Point X="-27.35340234375" Y="-3.988180175781" />
                  <Point X="-27.3596796875" Y="-3.992755859375" />
                  <Point X="-27.3804453125" Y="-4.010134277344" />
                  <Point X="-27.40276171875" Y="-4.032774902344" />
                  <Point X="-27.41047265625" Y="-4.041631347656" />
                  <Point X="-27.503203125" Y="-4.162478027344" />
                  <Point X="-27.747603515625" Y="-4.011150878906" />
                  <Point X="-27.980865234375" Y="-3.831547363281" />
                  <Point X="-27.356517578125" Y="-2.750147216797" />
                  <Point X="-27.341490234375" Y="-2.724120849609" />
                  <Point X="-27.334857421875" Y="-2.710101806641" />
                  <Point X="-27.323958984375" Y="-2.681162353516" />
                  <Point X="-27.319693359375" Y="-2.666241943359" />
                  <Point X="-27.31341796875" Y="-2.634741210938" />
                  <Point X="-27.31163671875" Y="-2.619197753906" />
                  <Point X="-27.310646484375" Y="-2.588034179688" />
                  <Point X="-27.314783203125" Y="-2.557131347656" />
                  <Point X="-27.323935546875" Y="-2.527324951172" />
                  <Point X="-27.3297421875" Y="-2.512801269531" />
                  <Point X="-27.3443125" Y="-2.483595458984" />
                  <Point X="-27.3522265625" Y="-2.470493652344" />
                  <Point X="-27.37005859375" Y="-2.445728515625" />
                  <Point X="-27.3799765625" Y="-2.434065185547" />
                  <Point X="-27.396974609375" Y="-2.417066894531" />
                  <Point X="-27.408814453125" Y="-2.407021972656" />
                  <Point X="-27.433970703125" Y="-2.388994628906" />
                  <Point X="-27.447287109375" Y="-2.381012207031" />
                  <Point X="-27.476115234375" Y="-2.366794921875" />
                  <Point X="-27.4905546875" Y="-2.361088134766" />
                  <Point X="-27.520171875" Y="-2.352103027344" />
                  <Point X="-27.550857421875" Y="-2.3480625" />
                  <Point X="-27.581791015625" Y="-2.349074462891" />
                  <Point X="-27.597216796875" Y="-2.350848632812" />
                  <Point X="-27.628744140625" Y="-2.357119140625" />
                  <Point X="-27.64367578125" Y="-2.361382568359" />
                  <Point X="-27.67264453125" Y="-2.37228515625" />
                  <Point X="-27.686681640625" Y="-2.378924316406" />
                  <Point X="-28.793087890625" Y="-3.017708251953" />
                  <Point X="-29.00401171875" Y="-2.740599365234" />
                  <Point X="-29.181265625" Y="-2.443372558594" />
                  <Point X="-28.0744140625" Y="-1.594055664062" />
                  <Point X="-28.048123046875" Y="-1.573881469727" />
                  <Point X="-28.036287109375" Y="-1.563099121094" />
                  <Point X="-28.014591796875" Y="-1.539696655273" />
                  <Point X="-28.004732421875" Y="-1.527076660156" />
                  <Point X="-27.986611328125" Y="-1.499344848633" />
                  <Point X="-27.979244140625" Y="-1.485774902344" />
                  <Point X="-27.966806640625" Y="-1.457627319336" />
                  <Point X="-27.961736328125" Y="-1.443049438477" />
                  <Point X="-27.9541875" Y="-1.413902587891" />
                  <Point X="-27.951552734375" Y="-1.398801147461" />
                  <Point X="-27.948748046875" Y="-1.368363647461" />
                  <Point X="-27.948578125" Y="-1.353027587891" />
                  <Point X="-27.950791015625" Y="-1.321344482422" />
                  <Point X="-27.953125" Y="-1.306035888672" />
                  <Point X="-27.960248046875" Y="-1.276009277344" />
                  <Point X="-27.972150390625" Y="-1.247544433594" />
                  <Point X="-27.98851953125" Y="-1.221384521484" />
                  <Point X="-27.997775390625" Y="-1.208971679688" />
                  <Point X="-28.019162109375" Y="-1.184700195312" />
                  <Point X="-28.029951171875" Y="-1.174251831055" />
                  <Point X="-28.053041015625" Y="-1.155185546875" />
                  <Point X="-28.065341796875" Y="-1.146567504883" />
                  <Point X="-28.091265625" Y="-1.131309570312" />
                  <Point X="-28.105431640625" Y="-1.124482177734" />
                  <Point X="-28.1346953125" Y="-1.1132578125" />
                  <Point X="-28.14979296875" Y="-1.108860839844" />
                  <Point X="-28.181681640625" Y="-1.10237878418" />
                  <Point X="-28.197298828125" Y="-1.100532592773" />
                  <Point X="-28.228623046875" Y="-1.09944152832" />
                  <Point X="-28.244330078125" Y="-1.100196533203" />
                  <Point X="-29.66091796875" Y="-1.286694091797" />
                  <Point X="-29.740763671875" Y="-0.974102600098" />
                  <Point X="-29.786451171875" Y="-0.654654602051" />
                  <Point X="-28.538236328125" Y="-0.320196258545" />
                  <Point X="-28.508287109375" Y="-0.312171234131" />
                  <Point X="-28.50018359375" Y="-0.309605987549" />
                  <Point X="-28.473078125" Y="-0.2988387146" />
                  <Point X="-28.443322265625" Y="-0.283357543945" />
                  <Point X="-28.43300390625" Y="-0.277126556396" />
                  <Point X="-28.405810546875" Y="-0.258253356934" />
                  <Point X="-28.39343359375" Y="-0.248010284424" />
                  <Point X="-28.37058203125" Y="-0.225583236694" />
                  <Point X="-28.360107421875" Y="-0.213399291992" />
                  <Point X="-28.340650390625" Y="-0.186456192017" />
                  <Point X="-28.33267578125" Y="-0.173281509399" />
                  <Point X="-28.318962890625" Y="-0.14582194519" />
                  <Point X="-28.313224609375" Y="-0.131536911011" />
                  <Point X="-28.3041875" Y="-0.102417976379" />
                  <Point X="-28.300947265625" Y="-0.08821245575" />
                  <Point X="-28.2966796875" Y="-0.059471172333" />
                  <Point X="-28.29565234375" Y="-0.044935554504" />
                  <Point X="-28.295837890625" Y="-0.014691693306" />
                  <Point X="-28.296953125" Y="-0.000746112287" />
                  <Point X="-28.301220703125" Y="0.026831010818" />
                  <Point X="-28.304373046875" Y="0.040462551117" />
                  <Point X="-28.3134375" Y="0.069667984009" />
                  <Point X="-28.31947265625" Y="0.084538444519" />
                  <Point X="-28.333958984375" Y="0.113051445007" />
                  <Point X="-28.34241015625" Y="0.126693984985" />
                  <Point X="-28.36221484375" Y="0.153337905884" />
                  <Point X="-28.372384765625" Y="0.164923065186" />
                  <Point X="-28.394462890625" Y="0.186294876099" />
                  <Point X="-28.40637109375" Y="0.19608140564" />
                  <Point X="-28.433564453125" Y="0.214954864502" />
                  <Point X="-28.440482421875" Y="0.219327941895" />
                  <Point X="-28.465615234375" Y="0.232834350586" />
                  <Point X="-28.49656640625" Y="0.245635681152" />
                  <Point X="-28.508287109375" Y="0.249611053467" />
                  <Point X="-29.785443359375" Y="0.591824707031" />
                  <Point X="-29.731330078125" Y="0.957531066895" />
                  <Point X="-29.6335859375" Y="1.318237060547" />
                  <Point X="-28.799533203125" Y="1.208432128906" />
                  <Point X="-28.778064453125" Y="1.205605712891" />
                  <Point X="-28.7677265625" Y="1.204815307617" />
                  <Point X="-28.74702734375" Y="1.204365844727" />
                  <Point X="-28.736666015625" Y="1.204706787109" />
                  <Point X="-28.7150859375" Y="1.206599365234" />
                  <Point X="-28.70482421875" Y="1.208066894531" />
                  <Point X="-28.6845234375" Y="1.212111083984" />
                  <Point X="-28.67333203125" Y="1.21505090332" />
                  <Point X="-28.61314453125" Y="1.234027587891" />
                  <Point X="-28.603439453125" Y="1.237681274414" />
                  <Point X="-28.584486328125" Y="1.246023803711" />
                  <Point X="-28.57523828125" Y="1.250712768555" />
                  <Point X="-28.556474609375" Y="1.261551879883" />
                  <Point X="-28.54779296875" Y="1.267220458984" />
                  <Point X="-28.53109375" Y="1.279473754883" />
                  <Point X="-28.515826171875" Y="1.293478759766" />
                  <Point X="-28.502181640625" Y="1.309059570312" />
                  <Point X="-28.495787109375" Y="1.317220825195" />
                  <Point X="-28.483373046875" Y="1.334980834961" />
                  <Point X="-28.477908203125" Y="1.343783569336" />
                  <Point X="-28.46797265625" Y="1.361930297852" />
                  <Point X="-28.4630703125" Y="1.37231640625" />
                  <Point X="-28.4389375" Y="1.430578491211" />
                  <Point X="-28.43550390625" Y="1.440337768555" />
                  <Point X="-28.429716796875" Y="1.460182495117" />
                  <Point X="-28.42736328125" Y="1.470267700195" />
                  <Point X="-28.423603515625" Y="1.491571044922" />
                  <Point X="-28.42236328125" Y="1.501854125977" />
                  <Point X="-28.4210078125" Y="1.522485595703" />
                  <Point X="-28.421904296875" Y="1.543134887695" />
                  <Point X="-28.42504296875" Y="1.563571044922" />
                  <Point X="-28.427169921875" Y="1.573706176758" />
                  <Point X="-28.43276171875" Y="1.594603393555" />
                  <Point X="-28.43598046875" Y="1.604448486328" />
                  <Point X="-28.44347265625" Y="1.623734008789" />
                  <Point X="-28.448384765625" Y="1.634400756836" />
                  <Point X="-28.477525390625" Y="1.690377319336" />
                  <Point X="-28.482802734375" Y="1.699291748047" />
                  <Point X="-28.494294921875" Y="1.716489624023" />
                  <Point X="-28.500509765625" Y="1.724772827148" />
                  <Point X="-28.514421875" Y="1.741352539062" />
                  <Point X="-28.521501953125" Y="1.748911865234" />
                  <Point X="-28.536443359375" Y="1.763215087891" />
                  <Point X="-28.5443046875" Y="1.769958862305" />
                  <Point X="-29.22761328125" Y="2.294280761719" />
                  <Point X="-29.00228515625" Y="2.680323486328" />
                  <Point X="-28.726337890625" Y="3.035013183594" />
                  <Point X="-28.267341796875" Y="2.770011474609" />
                  <Point X="-28.24491796875" Y="2.757716796875" />
                  <Point X="-28.225986328125" Y="2.749386474609" />
                  <Point X="-28.21629296875" Y="2.745738037109" />
                  <Point X="-28.195650390625" Y="2.739229248047" />
                  <Point X="-28.1856171875" Y="2.736657714844" />
                  <Point X="-28.16533203125" Y="2.732622314453" />
                  <Point X="-28.153353515625" Y="2.731007324219" />
                  <Point X="-28.06953125" Y="2.723673828125" />
                  <Point X="-28.059181640625" Y="2.723334472656" />
                  <Point X="-28.0385078125" Y="2.723784179688" />
                  <Point X="-28.02818359375" Y="2.724573242188" />
                  <Point X="-28.006734375" Y="2.727395019531" />
                  <Point X="-27.99655859375" Y="2.729302490234" />
                  <Point X="-27.976474609375" Y="2.734213623047" />
                  <Point X="-27.957044921875" Y="2.741281005859" />
                  <Point X="-27.9385" Y="2.750420410156" />
                  <Point X="-27.9294765625" Y="2.75549609375" />
                  <Point X="-27.9112265625" Y="2.767114746094" />
                  <Point X="-27.902806640625" Y="2.773145263672" />
                  <Point X="-27.886669921875" Y="2.786088134766" />
                  <Point X="-27.877673828125" Y="2.794279541016" />
                  <Point X="-27.81817578125" Y="2.85377734375" />
                  <Point X="-27.811263671875" Y="2.861488525391" />
                  <Point X="-27.7983203125" Y="2.877614990234" />
                  <Point X="-27.7922890625" Y="2.886030273438" />
                  <Point X="-27.7806640625" Y="2.904275146484" />
                  <Point X="-27.77558203125" Y="2.913302001953" />
                  <Point X="-27.7664375" Y="2.931842773438" />
                  <Point X="-27.7593671875" Y="2.951257568359" />
                  <Point X="-27.754447265625" Y="2.971338134766" />
                  <Point X="-27.75253515625" Y="2.981517822266" />
                  <Point X="-27.749705078125" Y="3.002966064453" />
                  <Point X="-27.7489140625" Y="3.013238525391" />
                  <Point X="-27.748447265625" Y="3.033811279297" />
                  <Point X="-27.7489453125" Y="3.046129150391" />
                  <Point X="-27.756279296875" Y="3.129951660156" />
                  <Point X="-27.757744140625" Y="3.140211181641" />
                  <Point X="-27.76178125" Y="3.1605" />
                  <Point X="-27.764353515625" Y="3.170529296875" />
                  <Point X="-27.770861328125" Y="3.191167724609" />
                  <Point X="-27.7745078125" Y="3.200860107422" />
                  <Point X="-27.782837890625" Y="3.219791259766" />
                  <Point X="-27.787521484375" Y="3.229030029297" />
                  <Point X="-28.059388671875" Y="3.699914794922" />
                  <Point X="-27.648369140625" Y="4.015039306641" />
                  <Point X="-27.1925234375" Y="4.268295898437" />
                  <Point X="-27.122599609375" Y="4.17716796875" />
                  <Point X="-27.11181640625" Y="4.164044433594" />
                  <Point X="-27.09751171875" Y="4.149102050781" />
                  <Point X="-27.089953125" Y="4.142022460938" />
                  <Point X="-27.073375" Y="4.128111816406" />
                  <Point X="-27.065091796875" Y="4.121897460938" />
                  <Point X="-27.04789453125" Y="4.11040625" />
                  <Point X="-27.03705859375" Y="4.10412890625" />
                  <Point X="-26.943763671875" Y="4.0555625" />
                  <Point X="-26.93432421875" Y="4.051286132812" />
                  <Point X="-26.915041015625" Y="4.043788085938" />
                  <Point X="-26.905197265625" Y="4.040566162109" />
                  <Point X="-26.884287109375" Y="4.034964111328" />
                  <Point X="-26.874150390625" Y="4.032833007812" />
                  <Point X="-26.853703125" Y="4.029687011719" />
                  <Point X="-26.833033203125" Y="4.028785888672" />
                  <Point X="-26.812388671875" Y="4.030140869141" />
                  <Point X="-26.802103515625" Y="4.031382324219" />
                  <Point X="-26.78078515625" Y="4.035144042969" />
                  <Point X="-26.77070703125" Y="4.037494384766" />
                  <Point X="-26.750865234375" Y="4.043278076172" />
                  <Point X="-26.73910546875" Y="4.047538330078" />
                  <Point X="-26.64193359375" Y="4.087788330078" />
                  <Point X="-26.6325859375" Y="4.092271728516" />
                  <Point X="-26.614435546875" Y="4.102229980469" />
                  <Point X="-26.6056328125" Y="4.107705078125" />
                  <Point X="-26.58789453125" Y="4.12012890625" />
                  <Point X="-26.5797421875" Y="4.126528808594" />
                  <Point X="-26.56418359375" Y="4.140178710937" />
                  <Point X="-26.550203125" Y="4.1554453125" />
                  <Point X="-26.537970703125" Y="4.172141113281" />
                  <Point X="-26.5323125" Y="4.1808203125" />
                  <Point X="-26.5214921875" Y="4.199579101562" />
                  <Point X="-26.5168125" Y="4.208823242188" />
                  <Point X="-26.508486328125" Y="4.227768554687" />
                  <Point X="-26.50421484375" Y="4.239452148438" />
                  <Point X="-26.47259375" Y="4.339737792969" />
                  <Point X="-26.470025390625" Y="4.349763183594" />
                  <Point X="-26.465990234375" Y="4.370049316406" />
                  <Point X="-26.464525390625" Y="4.380301269531" />
                  <Point X="-26.462638671875" Y="4.401861816406" />
                  <Point X="-26.46230078125" Y="4.412215332031" />
                  <Point X="-26.462751953125" Y="4.432896972656" />
                  <Point X="-26.463541015625" Y="4.443225585938" />
                  <Point X="-26.479265625" Y="4.562654785156" />
                  <Point X="-25.931177734375" Y="4.716319335938" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.229568359375" Y="4.27628515625" />
                  <Point X="-25.22043359375" Y="4.247104980469" />
                  <Point X="-25.207658203125" Y="4.218912597656" />
                  <Point X="-25.2001171875" Y="4.20534375" />
                  <Point X="-25.1822578125" Y="4.178616210938" />
                  <Point X="-25.172607421875" Y="4.166455566406" />
                  <Point X="-25.151451171875" Y="4.1438671875" />
                  <Point X="-25.126896484375" Y="4.125025390625" />
                  <Point X="-25.0996015625" Y="4.110436035156" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.78002734375" Y="4.218912109375" />
                  <Point X="-24.767251953125" Y="4.247105957031" />
                  <Point X="-24.76201953125" Y="4.261727539062" />
                  <Point X="-24.62180859375" Y="4.785006347656" />
                  <Point X="-24.17212109375" Y="4.737911621094" />
                  <Point X="-23.546396484375" Y="4.586841796875" />
                  <Point X="-23.141755859375" Y="4.440075683594" />
                  <Point X="-22.74955078125" Y="4.25665625" />
                  <Point X="-22.370578125" Y="4.035866699219" />
                  <Point X="-22.18221875" Y="3.901916259766" />
                  <Point X="-22.917298828125" Y="2.628720214844" />
                  <Point X="-22.934689453125" Y="2.598596435547" />
                  <Point X="-22.937931640625" Y="2.592476074219" />
                  <Point X="-22.947611328125" Y="2.571427001953" />
                  <Point X="-22.956509765625" Y="2.547248046875" />
                  <Point X="-22.959130859375" Y="2.538979980469" />
                  <Point X="-22.98016796875" Y="2.460314208984" />
                  <Point X="-22.982189453125" Y="2.450838378906" />
                  <Point X="-22.98630859375" Y="2.422086181641" />
                  <Point X="-22.98747265625" Y="2.401749023438" />
                  <Point X="-22.987580078125" Y="2.393328857422" />
                  <Point X="-22.9864140625" Y="2.368148925781" />
                  <Point X="-22.978216796875" Y="2.300159179688" />
                  <Point X="-22.97619921875" Y="2.289037841797" />
                  <Point X="-22.97085546875" Y="2.267109619141" />
                  <Point X="-22.967529296875" Y="2.256302734375" />
                  <Point X="-22.95926171875" Y="2.234212646484" />
                  <Point X="-22.95467578125" Y="2.223879394531" />
                  <Point X="-22.944310546875" Y="2.203832763672" />
                  <Point X="-22.9376640625" Y="2.192841552734" />
                  <Point X="-22.895572265625" Y="2.130810791016" />
                  <Point X="-22.88975" Y="2.123104980469" />
                  <Point X="-22.870779296875" Y="2.101253173828" />
                  <Point X="-22.856203125" Y="2.087086425781" />
                  <Point X="-22.849998046875" Y="2.081561279297" />
                  <Point X="-22.8304609375" Y="2.06611328125" />
                  <Point X="-22.7684296875" Y="2.024022949219" />
                  <Point X="-22.75872265625" Y="2.018247802734" />
                  <Point X="-22.738689453125" Y="2.007889282227" />
                  <Point X="-22.72836328125" Y="2.003306152344" />
                  <Point X="-22.706287109375" Y="1.995041748047" />
                  <Point X="-22.6954921875" Y="1.991717529297" />
                  <Point X="-22.673583984375" Y="1.986373779297" />
                  <Point X="-22.6610078125" Y="1.984177490234" />
                  <Point X="-22.592984375" Y="1.975974975586" />
                  <Point X="-22.58322265625" Y="1.975305419922" />
                  <Point X="-22.55390234375" Y="1.975312255859" />
                  <Point X="-22.53337890625" Y="1.977083496094" />
                  <Point X="-22.52512890625" Y="1.978161132812" />
                  <Point X="-22.5006328125" Y="1.982829223633" />
                  <Point X="-22.421966796875" Y="2.003865356445" />
                  <Point X="-22.416005859375" Y="2.005670776367" />
                  <Point X="-22.39559765625" Y="2.013066650391" />
                  <Point X="-22.372345703125" Y="2.023573852539" />
                  <Point X="-22.36396484375" Y="2.027872802734" />
                  <Point X="-21.059595703125" Y="2.780950195312" />
                  <Point X="-20.956029296875" Y="2.637014648438" />
                  <Point X="-20.863115234375" Y="2.483471679688" />
                  <Point X="-21.8041484375" Y="1.761390380859" />
                  <Point X="-21.827046875" Y="1.743819946289" />
                  <Point X="-21.83245703125" Y="1.739342163086" />
                  <Point X="-21.84955859375" Y="1.723474731445" />
                  <Point X="-21.867064453125" Y="1.704471679688" />
                  <Point X="-21.872591796875" Y="1.697899780273" />
                  <Point X="-21.92920703125" Y="1.624040039062" />
                  <Point X="-21.93473828125" Y="1.616000366211" />
                  <Point X="-21.94962109375" Y="1.590835327148" />
                  <Point X="-21.958525390625" Y="1.572330200195" />
                  <Point X="-21.961810546875" Y="1.564658569336" />
                  <Point X="-21.970294921875" Y="1.541119628906" />
                  <Point X="-21.991384765625" Y="1.465708618164" />
                  <Point X="-21.99377734375" Y="1.454649047852" />
                  <Point X="-21.99723046875" Y="1.432332519531" />
                  <Point X="-21.998291015625" Y="1.421075561523" />
                  <Point X="-21.999103515625" Y="1.39748425293" />
                  <Point X="-21.998818359375" Y="1.386172485352" />
                  <Point X="-21.99690625" Y="1.363665527344" />
                  <Point X="-21.994921875" Y="1.350741943359" />
                  <Point X="-21.97762890625" Y="1.266936279297" />
                  <Point X="-21.975173828125" Y="1.257506591797" />
                  <Point X="-21.965890625" Y="1.229794433594" />
                  <Point X="-21.95772265625" Y="1.210943115234" />
                  <Point X="-21.9541328125" Y="1.203551757813" />
                  <Point X="-21.94211328125" Y="1.182054321289" />
                  <Point X="-21.89502734375" Y="1.110483520508" />
                  <Point X="-21.888259765625" Y="1.101421875" />
                  <Point X="-21.87369921875" Y="1.084170776367" />
                  <Point X="-21.86590625" Y="1.075981811523" />
                  <Point X="-21.848654296875" Y="1.05988684082" />
                  <Point X="-21.83994140625" Y="1.052677978516" />
                  <Point X="-21.821716796875" Y="1.039345581055" />
                  <Point X="-21.810841796875" Y="1.032455322266" />
                  <Point X="-21.74261328125" Y="0.994048461914" />
                  <Point X="-21.73376171875" Y="0.989654724121" />
                  <Point X="-21.70639453125" Y="0.978364135742" />
                  <Point X="-21.686517578125" Y="0.972114990234" />
                  <Point X="-21.678572265625" Y="0.96999029541" />
                  <Point X="-21.654427734375" Y="0.965006347656" />
                  <Point X="-21.562166015625" Y="0.952812866211" />
                  <Point X="-21.555966796875" Y="0.952199645996" />
                  <Point X="-21.53428125" Y="0.95122277832" />
                  <Point X="-21.50878125" Y="0.952032287598" />
                  <Point X="-21.49939453125" Y="0.952797302246" />
                  <Point X="-20.295296875" Y="1.111319824219" />
                  <Point X="-20.247310546875" Y="0.914196228027" />
                  <Point X="-20.21612890625" Y="0.713921325684" />
                  <Point X="-21.281765625" Y="0.428384307861" />
                  <Point X="-21.3080078125" Y="0.421352813721" />
                  <Point X="-21.314798828125" Y="0.419257507324" />
                  <Point X="-21.33684375" Y="0.411058898926" />
                  <Point X="-21.36033203125" Y="0.400151306152" />
                  <Point X="-21.367861328125" Y="0.396237182617" />
                  <Point X="-21.45850390625" Y="0.343844055176" />
                  <Point X="-21.46675390625" Y="0.338486785889" />
                  <Point X="-21.4903125" Y="0.320764251709" />
                  <Point X="-21.50574609375" Y="0.306916168213" />
                  <Point X="-21.511654296875" Y="0.301131622314" />
                  <Point X="-21.52832421875" Y="0.282797790527" />
                  <Point X="-21.582708984375" Y="0.213497619629" />
                  <Point X="-21.589146484375" Y="0.204203643799" />
                  <Point X="-21.600875" Y="0.184914321899" />
                  <Point X="-21.606166015625" Y="0.174918991089" />
                  <Point X="-21.61594140625" Y="0.153445358276" />
                  <Point X="-21.62000390625" Y="0.142895965576" />
                  <Point X="-21.626849609375" Y="0.121386222839" />
                  <Point X="-21.63000390625" Y="0.108484405518" />
                  <Point X="-21.648125" Y="0.013862772942" />
                  <Point X="-21.64947265625" Y="0.004115490437" />
                  <Point X="-21.651484375" Y="-0.025288801193" />
                  <Point X="-21.651111328125" Y="-0.046048591614" />
                  <Point X="-21.6506171875" Y="-0.054161914825" />
                  <Point X="-21.647751953125" Y="-0.078373321533" />
                  <Point X="-21.629623046875" Y="-0.173033752441" />
                  <Point X="-21.6268359375" Y="-0.184000335693" />
                  <Point X="-21.619982421875" Y="-0.205514251709" />
                  <Point X="-21.61591796875" Y="-0.216061569214" />
                  <Point X="-21.606134765625" Y="-0.237537277222" />
                  <Point X="-21.600841796875" Y="-0.247531265259" />
                  <Point X="-21.589107421875" Y="-0.266818817139" />
                  <Point X="-21.581587890625" Y="-0.277485321045" />
                  <Point X="-21.527203125" Y="-0.346785491943" />
                  <Point X="-21.520640625" Y="-0.354299102783" />
                  <Point X="-21.49945703125" Y="-0.375373840332" />
                  <Point X="-21.48327734375" Y="-0.388873687744" />
                  <Point X="-21.476818359375" Y="-0.393810516357" />
                  <Point X="-21.45663671875" Y="-0.40748336792" />
                  <Point X="-21.365994140625" Y="-0.459876647949" />
                  <Point X="-21.36050390625" Y="-0.462814910889" />
                  <Point X="-21.340845703125" Y="-0.472014831543" />
                  <Point X="-21.316974609375" Y="-0.481027008057" />
                  <Point X="-21.3080078125" Y="-0.483912963867" />
                  <Point X="-20.215119140625" Y="-0.776751220703" />
                  <Point X="-20.23837890625" Y="-0.931024780273" />
                  <Point X="-20.272197265625" Y="-1.079219726563" />
                  <Point X="-21.532783203125" Y="-0.913260253906" />
                  <Point X="-21.563216796875" Y="-0.909253662109" />
                  <Point X="-21.57283984375" Y="-0.908481384277" />
                  <Point X="-21.601763671875" Y="-0.908126342773" />
                  <Point X="-21.62257421875" Y="-0.909636047363" />
                  <Point X="-21.629259765625" Y="-0.910359436035" />
                  <Point X="-21.64918359375" Y="-0.913472900391" />
                  <Point X="-21.82708203125" Y="-0.952140258789" />
                  <Point X="-21.843509765625" Y="-0.95730847168" />
                  <Point X="-21.87516015625" Y="-0.970524597168" />
                  <Point X="-21.8903828125" Y="-0.978572570801" />
                  <Point X="-21.920423828125" Y="-0.99812878418" />
                  <Point X="-21.931744140625" Y="-1.006770751953" />
                  <Point X="-21.952962890625" Y="-1.025649536133" />
                  <Point X="-21.962861328125" Y="-1.03588671875" />
                  <Point X="-22.070390625" Y="-1.165210571289" />
                  <Point X="-22.079423828125" Y="-1.178116699219" />
                  <Point X="-22.095234375" Y="-1.205248291016" />
                  <Point X="-22.10201171875" Y="-1.219473754883" />
                  <Point X="-22.113673828125" Y="-1.250309814453" />
                  <Point X="-22.11738671875" Y="-1.262568969727" />
                  <Point X="-22.12312890625" Y="-1.287470336914" />
                  <Point X="-22.125158203125" Y="-1.300112426758" />
                  <Point X="-22.140568359375" Y="-1.467592529297" />
                  <Point X="-22.140916015625" Y="-1.479478271484" />
                  <Point X="-22.139837890625" Y="-1.511653198242" />
                  <Point X="-22.136818359375" Y="-1.532434204102" />
                  <Point X="-22.1281875" Y="-1.565544921875" />
                  <Point X="-22.122369140625" Y="-1.5817109375" />
                  <Point X="-22.113830078125" Y="-1.600034179688" />
                  <Point X="-22.101431640625" Y="-1.62252734375" />
                  <Point X="-22.002978515625" Y="-1.775662963867" />
                  <Point X="-21.998255859375" Y="-1.78235534668" />
                  <Point X="-21.980205078125" Y="-1.804449829102" />
                  <Point X="-21.956509765625" Y="-1.828119750977" />
                  <Point X="-21.947203125" Y="-1.836277099609" />
                  <Point X="-20.912826171875" Y="-2.629981933594" />
                  <Point X="-20.9545078125" Y="-2.697428466797" />
                  <Point X="-20.998724609375" Y="-2.760252441406" />
                  <Point X="-22.124439453125" Y="-2.110319580078" />
                  <Point X="-22.151544921875" Y="-2.094670410156" />
                  <Point X="-22.160333984375" Y="-2.0901875" />
                  <Point X="-22.187533203125" Y="-2.078612304688" />
                  <Point X="-22.2080078125" Y="-2.071922119141" />
                  <Point X="-22.23325" Y="-2.065550048828" />
                  <Point X="-22.444978515625" Y="-2.027311889648" />
                  <Point X="-22.456798828125" Y="-2.025934692383" />
                  <Point X="-22.4889609375" Y="-2.024217407227" />
                  <Point X="-22.51003515625" Y="-2.025441040039" />
                  <Point X="-22.543974609375" Y="-2.031243774414" />
                  <Point X="-22.5606796875" Y="-2.035695678711" />
                  <Point X="-22.579974609375" Y="-2.042773071289" />
                  <Point X="-22.603033203125" Y="-2.053015625" />
                  <Point X="-22.778927734375" Y="-2.145587646484" />
                  <Point X="-22.792404296875" Y="-2.154201416016" />
                  <Point X="-22.81771875" Y="-2.173566162109" />
                  <Point X="-22.829556640625" Y="-2.184317138672" />
                  <Point X="-22.852423828125" Y="-2.208900390625" />
                  <Point X="-22.86035546875" Y="-2.218646728516" />
                  <Point X="-22.874865234375" Y="-2.239105712891" />
                  <Point X="-22.881443359375" Y="-2.249818359375" />
                  <Point X="-22.974015625" Y="-2.425712890625" />
                  <Point X="-22.978888671875" Y="-2.436572509766" />
                  <Point X="-22.9902109375" Y="-2.466736328125" />
                  <Point X="-22.99540234375" Y="-2.487312011719" />
                  <Point X="-23.00007421875" Y="-2.521644775391" />
                  <Point X="-23.00083203125" Y="-2.539027099609" />
                  <Point X="-22.999826171875" Y="-2.559897216797" />
                  <Point X="-22.997021484375" Y="-2.584519287109" />
                  <Point X="-22.95878515625" Y="-2.796233398438" />
                  <Point X="-22.95698046875" Y="-2.804228759766" />
                  <Point X="-22.94876171875" Y="-2.831541748047" />
                  <Point X="-22.935927734375" Y="-2.862479492188" />
                  <Point X="-22.93044921875" Y="-2.873578125" />
                  <Point X="-22.264103515625" Y="-4.027721679687" />
                  <Point X="-22.276244140625" Y="-4.036083496094" />
                  <Point X="-23.145400390625" Y="-2.903379394531" />
                  <Point X="-23.16608203125" Y="-2.876424316406" />
                  <Point X="-23.172625" Y="-2.868774902344" />
                  <Point X="-23.186478515625" Y="-2.854213623047" />
                  <Point X="-23.1937890625" Y="-2.847301757812" />
                  <Point X="-23.210720703125" Y="-2.832914306641" />
                  <Point X="-23.231001953125" Y="-2.817881103516" />
                  <Point X="-23.43982421875" Y="-2.683628173828" />
                  <Point X="-23.450216796875" Y="-2.677832519531" />
                  <Point X="-23.4792734375" Y="-2.663938476562" />
                  <Point X="-23.499396484375" Y="-2.656962646484" />
                  <Point X="-23.53334765625" Y="-2.649321289062" />
                  <Point X="-23.55067578125" Y="-2.647068603516" />
                  <Point X="-23.571837890625" Y="-2.646281494141" />
                  <Point X="-23.596310546875" Y="-2.646949462891" />
                  <Point X="-23.82469140625" Y="-2.667965332031" />
                  <Point X="-23.84066015625" Y="-2.670825439453" />
                  <Point X="-23.871875" Y="-2.679220458984" />
                  <Point X="-23.88712109375" Y="-2.684755371094" />
                  <Point X="-23.918138671875" Y="-2.699119140625" />
                  <Point X="-23.9289765625" Y="-2.705020507813" />
                  <Point X="-23.94979296875" Y="-2.718177734375" />
                  <Point X="-23.959771484375" Y="-2.72543359375" />
                  <Point X="-24.136123046875" Y="-2.872063476562" />
                  <Point X="-24.144779296875" Y="-2.880228759766" />
                  <Point X="-24.16677734375" Y="-2.903755615234" />
                  <Point X="-24.17954296875" Y="-2.92094140625" />
                  <Point X="-24.19712109375" Y="-2.951219726562" />
                  <Point X="-24.204501953125" Y="-2.967173828125" />
                  <Point X="-24.21170703125" Y="-2.987497802734" />
                  <Point X="-24.21829296875" Y="-3.010628173828" />
                  <Point X="-24.271021484375" Y="-3.253218505859" />
                  <Point X="-24.2724140625" Y="-3.261286865234" />
                  <Point X="-24.275275390625" Y="-3.289677001953" />
                  <Point X="-24.2752578125" Y="-3.323168457031" />
                  <Point X="-24.2744453125" Y="-3.335518798828" />
                  <Point X="-24.16691015625" Y="-4.152325195312" />
                  <Point X="-24.336845703125" Y="-3.518112792969" />
                  <Point X="-24.344931640625" Y="-3.487936523438" />
                  <Point X="-24.3481171875" Y="-3.478188232422" />
                  <Point X="-24.355517578125" Y="-3.459097167969" />
                  <Point X="-24.359732421875" Y="-3.449754394531" />
                  <Point X="-24.3702578125" Y="-3.429425048828" />
                  <Point X="-24.38289453125" Y="-3.408447998047" />
                  <Point X="-24.543318359375" Y="-3.177309326172" />
                  <Point X="-24.550703125" Y="-3.167976806641" />
                  <Point X="-24.57223046875" Y="-3.144022216797" />
                  <Point X="-24.58829296875" Y="-3.129778808594" />
                  <Point X="-24.6170546875" Y="-3.109579589844" />
                  <Point X="-24.63237890625" Y="-3.1008203125" />
                  <Point X="-24.65223046875" Y="-3.091807617188" />
                  <Point X="-24.67445703125" Y="-3.083351074219" />
                  <Point X="-24.922703125" Y="-3.006305175781" />
                  <Point X="-24.933185546875" Y="-3.0036953125" />
                  <Point X="-24.961923828125" Y="-2.998252441406" />
                  <Point X="-24.98148046875" Y="-2.996611572266" />
                  <Point X="-25.013666015625" Y="-2.997248291016" />
                  <Point X="-25.02973046875" Y="-2.998939941406" />
                  <Point X="-25.0496640625" Y="-3.002774169922" />
                  <Point X="-25.07009765625" Y="-3.007892333984" />
                  <Point X="-25.31834375" Y="-3.084938476562" />
                  <Point X="-25.32946484375" Y="-3.089170654297" />
                  <Point X="-25.358787109375" Y="-3.102486572266" />
                  <Point X="-25.37730078125" Y="-3.113588134766" />
                  <Point X="-25.405380859375" Y="-3.135112792969" />
                  <Point X="-25.41841015625" Y="-3.147194091797" />
                  <Point X="-25.433232421875" Y="-3.163774169922" />
                  <Point X="-25.447673828125" Y="-3.182072021484" />
                  <Point X="-25.608095703125" Y="-3.4132109375" />
                  <Point X="-25.61246875" Y="-3.420129150391" />
                  <Point X="-25.625974609375" Y="-3.445260498047" />
                  <Point X="-25.63877734375" Y="-3.476213623047" />
                  <Point X="-25.64275390625" Y="-3.487936035156" />
                  <Point X="-25.985427734375" Y="-4.766806152344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.908588669515" Y="-3.887197836988" />
                  <Point X="-27.44346733416" Y="-4.084630130328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.129532372467" Y="-4.642362431704" />
                  <Point X="-25.970204636041" Y="-4.709993043341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.94458769491" Y="-3.768712921446" />
                  <Point X="-27.378893714427" Y="-4.008835769843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.119645919399" Y="-4.5433547462" />
                  <Point X="-25.945375083338" Y="-4.617328327307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.896730917218" Y="-3.685822682508" />
                  <Point X="-27.287756831589" Y="-3.944316845582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.136176108447" Y="-4.433133861388" />
                  <Point X="-25.920545530635" Y="-4.524663611274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.848874139525" Y="-3.60293244357" />
                  <Point X="-27.193304507235" Y="-3.881205242749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.158598275953" Y="-4.320411980104" />
                  <Point X="-25.895715977932" Y="-4.431998895241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.801017361833" Y="-3.520042204632" />
                  <Point X="-27.098706092882" Y="-3.818155651442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.181020443459" Y="-4.207690098819" />
                  <Point X="-25.870886425229" Y="-4.339334179208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.81780277734" Y="-2.985238166406" />
                  <Point X="-28.771148692771" Y="-3.005041650379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.75316058414" Y="-3.437151965694" />
                  <Point X="-26.955246561672" Y="-3.775846373732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.2440758215" Y="-4.07772044296" />
                  <Point X="-25.846056872526" Y="-4.246669463174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.93385233892" Y="-2.83277381423" />
                  <Point X="-28.668132469325" Y="-2.94556520704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.705303806447" Y="-3.354261726756" />
                  <Point X="-26.669240940268" Y="-3.794044321459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.465309800669" Y="-3.88060795446" />
                  <Point X="-25.821227319822" Y="-4.154004747141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.036598547152" Y="-2.685956400521" />
                  <Point X="-28.565116245878" Y="-2.8860887637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.657447028755" Y="-3.271371487818" />
                  <Point X="-25.796397767119" Y="-4.061340031108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.119005872752" Y="-2.547772330279" />
                  <Point X="-28.462100022431" Y="-2.826612320361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.609590251062" Y="-3.18848124888" />
                  <Point X="-25.771568214416" Y="-3.968675315075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.160094181531" Y="-2.427127142109" />
                  <Point X="-28.359083798985" Y="-2.767135877022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.56173347337" Y="-3.105591009942" />
                  <Point X="-25.746738661713" Y="-3.876010599041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.073499036552" Y="-2.360680364504" />
                  <Point X="-28.256067575538" Y="-2.707659433682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.513876695677" Y="-3.022700771004" />
                  <Point X="-25.72190910901" Y="-3.783345883008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.986903891574" Y="-2.2942335869" />
                  <Point X="-28.153051352091" Y="-2.648182990343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.466019917985" Y="-2.939810532066" />
                  <Point X="-25.697079556306" Y="-3.690681166975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.900308746596" Y="-2.227786809296" />
                  <Point X="-28.050035128645" Y="-2.588706547004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.418163140292" Y="-2.856920293128" />
                  <Point X="-25.672250003603" Y="-3.598016450941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.813713601618" Y="-2.161340031692" />
                  <Point X="-27.947018905198" Y="-2.529230103664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.3703063626" Y="-2.774030054189" />
                  <Point X="-25.6474204509" Y="-3.505351734908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.172513650532" Y="-4.131412527921" />
                  <Point X="-24.16949466392" Y="-4.132694011708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.72711845664" Y="-2.094893254087" />
                  <Point X="-27.844002681751" Y="-2.469753660325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.326990546353" Y="-2.689212291476" />
                  <Point X="-25.610901067421" Y="-3.417649057645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.203715789269" Y="-4.014963769962" />
                  <Point X="-24.183886070928" Y="-4.02338098601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.640523311662" Y="-2.028446476483" />
                  <Point X="-27.740986458305" Y="-2.410277216986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.310800472768" Y="-2.592880334132" />
                  <Point X="-25.55578474899" Y="-3.337840310928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.234917928006" Y="-3.898515012002" />
                  <Point X="-24.198277477936" Y="-3.914067960312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.553928166683" Y="-1.961999698879" />
                  <Point X="-27.624890669822" Y="-2.356352719611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.350928374912" Y="-2.472642814391" />
                  <Point X="-25.500456079352" Y="-3.25812170195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.266120066743" Y="-3.782066254043" />
                  <Point X="-24.212668884944" Y="-3.804754934613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.467333021705" Y="-1.895552921275" />
                  <Point X="-25.444865802689" Y="-3.178514138566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.29732220548" Y="-3.665617496084" />
                  <Point X="-24.227060291952" Y="-3.695441908915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.66120221629" Y="-1.285581278472" />
                  <Point X="-29.659133922012" Y="-1.286459217305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.380737876727" Y="-1.82910614367" />
                  <Point X="-25.367928228931" Y="-3.107967965193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.328524344217" Y="-3.549168738125" />
                  <Point X="-24.24145169896" Y="-3.586128883217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.69076965188" Y="-1.16982641083" />
                  <Point X="-29.473557339363" Y="-1.262027567264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.294142731749" Y="-1.762659366066" />
                  <Point X="-25.237880459256" Y="-3.059965732471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.371209104712" Y="-3.427845896405" />
                  <Point X="-24.255843105968" Y="-3.476815857519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.720337087471" Y="-1.054071543188" />
                  <Point X="-29.287980756715" Y="-1.237595917223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.207547586771" Y="-1.696212588462" />
                  <Point X="-25.097435342568" Y="-3.016376911711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.470235174908" Y="-3.282607587605" />
                  <Point X="-24.270234512976" Y="-3.36750283182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.745621781498" Y="-0.940134591484" />
                  <Point X="-29.102404174067" Y="-1.213164267182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.120952441793" Y="-1.629765810858" />
                  <Point X="-24.756526323465" Y="-3.057879969086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.590049560808" Y="-3.128545162317" />
                  <Point X="-24.272615731584" Y="-3.263287828636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.761336055854" Y="-0.830260041912" />
                  <Point X="-28.916827591418" Y="-1.188732617141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.035888453419" Y="-1.562669095835" />
                  <Point X="-24.252622639858" Y="-3.168570156718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.308779571634" Y="-3.993682585843" />
                  <Point X="-22.275632674615" Y="-4.007752608863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.777050330209" Y="-0.72038549234" />
                  <Point X="-28.73125100877" Y="-1.1643009671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.978401023877" Y="-1.483866826071" />
                  <Point X="-24.23208537236" Y="-3.074083473711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.426224227783" Y="-3.840626051156" />
                  <Point X="-22.354560677207" Y="-3.871045423616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.726569138971" Y="-0.638609250859" />
                  <Point X="-28.545674426122" Y="-1.139869317059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.95095486006" Y="-1.392312795559" />
                  <Point X="-24.209251612175" Y="-2.980571594016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.543668883932" Y="-3.687569516469" />
                  <Point X="-22.433488679799" Y="-3.734338238368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.577521427486" Y="-0.598672014945" />
                  <Point X="-28.360097843473" Y="-1.115437667018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.957833196244" Y="-1.286188879218" />
                  <Point X="-24.161182193245" Y="-2.897771615928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.661113540082" Y="-3.534512981781" />
                  <Point X="-22.512416682391" Y="-3.59763105312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.428473716001" Y="-0.55873477903" />
                  <Point X="-24.082888647975" Y="-2.827801018313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.778558196231" Y="-3.381456447094" />
                  <Point X="-22.591344684983" Y="-3.460923867873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.279426004516" Y="-0.518797543116" />
                  <Point X="-24.000715633064" Y="-2.759477157862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.89600285238" Y="-3.228399912407" />
                  <Point X="-22.670272687575" Y="-3.324216682625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.130378293031" Y="-0.478860307202" />
                  <Point X="-23.909356863995" Y="-2.695052418718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.013447508529" Y="-3.07534337772" />
                  <Point X="-22.749200690168" Y="-3.187509497378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.981330581545" Y="-0.438923071287" />
                  <Point X="-23.746900296076" Y="-2.660806904674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.130892164678" Y="-2.922286843033" />
                  <Point X="-22.82812869276" Y="-3.05080231213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.83228287006" Y="-0.398985835373" />
                  <Point X="-23.52797718663" Y="-2.650530015466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.420044206798" Y="-2.696344847244" />
                  <Point X="-22.907056695352" Y="-2.914095126883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.683235158575" Y="-0.359048599458" />
                  <Point X="-22.960213012821" Y="-2.788327372941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.534187476575" Y="-0.319111351028" />
                  <Point X="-22.980399606162" Y="-2.676554436589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.416670093413" Y="-0.265790284794" />
                  <Point X="-22.999202921791" Y="-2.56536866679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.345292330065" Y="-0.192884111919" />
                  <Point X="-22.989966480858" Y="-2.466085067504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.305479067765" Y="-0.106579603263" />
                  <Point X="-22.949898590347" Y="-2.379888642111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.780847903168" Y="0.622881547839" />
                  <Point X="-29.57456666265" Y="0.535320356183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.296435887541" Y="-0.007213969673" />
                  <Point X="-22.905501133644" Y="-2.295530008532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.766479332379" Y="0.719986687248" />
                  <Point X="-28.915220428435" Y="0.35864872045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.333224709882" Y="0.111606194782" />
                  <Point X="-22.85602600588" Y="-2.213326718443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.75211076159" Y="0.817091826657" />
                  <Point X="-22.776047020639" Y="-2.14407154765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.737742190801" Y="0.914196966066" />
                  <Point X="-22.667498689287" Y="-2.086943344797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.717441531422" Y="1.00878408326" />
                  <Point X="-22.551106845044" Y="-2.033144515637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.444579893973" Y="-2.502837339824" />
                  <Point X="-20.961786948495" Y="-2.707770786622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.692360199548" Y="1.101341925376" />
                  <Point X="-22.229253527123" Y="-2.066558907754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.119662637624" Y="-2.113077480432" />
                  <Point X="-20.926338622179" Y="-2.619613472567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.667278867674" Y="1.193899767492" />
                  <Point X="-21.227355780782" Y="-2.38863503364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.6421975358" Y="1.286457609608" />
                  <Point X="-21.528372939385" Y="-2.157656594712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.402150967626" Y="1.287768122554" />
                  <Point X="-21.829390097988" Y="-1.926678155785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.049704629596" Y="1.241367763849" />
                  <Point X="-22.025855870677" Y="-1.740079147178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.723021843937" Y="1.205903384301" />
                  <Point X="-22.11412177184" Y="-1.59940825915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.579892669885" Y="1.248352890304" />
                  <Point X="-22.140734082798" Y="-1.484907767494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.496589333204" Y="1.31619695763" />
                  <Point X="-22.132968837683" Y="-1.384999682633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.451495722974" Y="1.400260091569" />
                  <Point X="-22.122814250792" Y="-1.286105813184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.423597349417" Y="1.491622170435" />
                  <Point X="-22.09026373348" Y="-1.196718452184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.434323128236" Y="1.599379229281" />
                  <Point X="-22.031512619629" Y="-1.118452584585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.508206122074" Y="1.733944935365" />
                  <Point X="-21.968086614053" Y="-1.042171090791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.784971000606" Y="1.954628892167" />
                  <Point X="-21.883434957222" Y="-0.974899351413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.085986724718" Y="2.185606722189" />
                  <Point X="-21.739014098979" Y="-0.932998132819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.202050037451" Y="2.338076911383" />
                  <Point X="-21.546694144561" Y="-0.91142887427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.153772363003" Y="2.420788490249" />
                  <Point X="-21.194247460769" Y="-0.957829379743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.105494688556" Y="2.503500069114" />
                  <Point X="-20.841800665661" Y="-1.004229932467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.057217014108" Y="2.586211647979" />
                  <Point X="-20.489353870553" Y="-1.05063048519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.008939339661" Y="2.668923226845" />
                  <Point X="-20.263955204011" Y="-1.043102306891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.950245159869" Y="2.747213261519" />
                  <Point X="-20.242483667923" Y="-0.949012197372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.889885734435" Y="2.824796441355" />
                  <Point X="-21.581681829626" Y="-0.277352067962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.730511523428" Y="-0.638652427248" />
                  <Point X="-20.226550489126" Y="-0.85257119466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.829526309001" Y="2.902379621191" />
                  <Point X="-21.63363296599" Y="-0.152095883048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.769166883568" Y="2.979962801027" />
                  <Point X="-28.226620559805" Y="2.749665549962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.22578129452" Y="2.749309302985" />
                  <Point X="-21.651194190688" Y="-0.041437349569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.960647092384" Y="2.739970747115" />
                  <Point X="-21.639871287544" Y="0.056960599054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.868073503785" Y="2.803879825962" />
                  <Point X="-21.617079397859" Y="0.150490251722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.798403766416" Y="2.877511012851" />
                  <Point X="-21.567634508208" Y="0.232706377129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.756507053727" Y="2.962931149286" />
                  <Point X="-21.503684898823" Y="0.308765614292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.750471590626" Y="3.063573483049" />
                  <Point X="-21.409678064822" Y="0.37206631656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.765132933775" Y="3.173001089841" />
                  <Point X="-21.29246628944" Y="0.425517105601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.830868221813" Y="3.304108300003" />
                  <Point X="-21.143418597214" Y="0.46545434969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.909796798165" Y="3.440815728797" />
                  <Point X="-20.994370958932" Y="0.505391616678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.988725374517" Y="3.577523157592" />
                  <Point X="-20.845323320651" Y="0.545328883665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.050315712619" Y="3.706870940891" />
                  <Point X="-21.874859291331" Y="1.085545211454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.562160104172" Y="0.952812281456" />
                  <Point X="-20.696275682369" Y="0.585266150653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.963674262538" Y="3.773298063145" />
                  <Point X="-21.964614011907" Y="1.226848065828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.361697997598" Y="0.970925401465" />
                  <Point X="-20.547228044088" Y="0.62520341764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.877032812457" Y="3.8397251854" />
                  <Point X="-21.993152278806" Y="1.342166077279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.176121378135" Y="0.995357035879" />
                  <Point X="-20.398180405806" Y="0.665140684628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.790391362377" Y="3.906152307654" />
                  <Point X="-21.995086066709" Y="1.446191157397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.990544758672" Y="1.019788670293" />
                  <Point X="-20.249132767525" Y="0.705077951615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.703749912296" Y="3.972579429908" />
                  <Point X="-21.97085676002" Y="1.539110662747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.804968139209" Y="1.044220304707" />
                  <Point X="-20.22952446647" Y="0.799958957482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.610374534616" Y="4.036148169483" />
                  <Point X="-21.928862568668" Y="1.624489421944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.619391519746" Y="1.068651939121" />
                  <Point X="-20.246729840733" Y="0.910466441414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.505069470001" Y="4.094653057388" />
                  <Point X="-22.902459990623" Y="2.140961244544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.520848581227" Y="1.978976811677" />
                  <Point X="-21.868929870235" Y="1.702253736645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.433814900283" Y="1.093083573535" />
                  <Point X="-20.27438334572" Y="1.025408893714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.399764405386" Y="4.153157945293" />
                  <Point X="-22.972510247993" Y="2.27390005052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.377399036549" Y="2.021290328418" />
                  <Point X="-21.790245548446" Y="1.772058459469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.294459340771" Y="4.211662833198" />
                  <Point X="-27.019094809967" Y="4.094777524595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.872601105568" Y="4.032594636344" />
                  <Point X="-22.987115853569" Y="2.383303998116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.27321115843" Y="2.080269433856" />
                  <Point X="-21.703650502255" Y="1.838505279006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.701433585423" Y="4.063142570543" />
                  <Point X="-22.974585445552" Y="2.48118939133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.170194897991" Y="2.139745861493" />
                  <Point X="-21.617055456063" Y="1.904952098542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.589651682645" Y="4.118898203759" />
                  <Point X="-22.947027730314" Y="2.572696071073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.067178637552" Y="2.19922228913" />
                  <Point X="-21.530460409872" Y="1.971398918079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.524461713976" Y="4.194430939643" />
                  <Point X="-22.90127262312" Y="2.656478416209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.964162377113" Y="2.258698716767" />
                  <Point X="-21.44386536368" Y="2.037845737616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.490424695482" Y="4.283187318327" />
                  <Point X="-22.85341590647" Y="2.739368681058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.861146116674" Y="2.318175144404" />
                  <Point X="-21.357270317489" Y="2.104292557153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.465185935494" Y="4.375678336173" />
                  <Point X="-22.80555918982" Y="2.822258945907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.758129856236" Y="2.377651572042" />
                  <Point X="-21.270675271297" Y="2.170739376689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.468416322926" Y="4.480253790138" />
                  <Point X="-22.757702473171" Y="2.905149210757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.655113595797" Y="2.437127999679" />
                  <Point X="-21.184080225106" Y="2.237186196226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.443217003691" Y="4.57276154959" />
                  <Point X="-22.709845756521" Y="2.988039475606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.552097335358" Y="2.496604427316" />
                  <Point X="-21.097485178914" Y="2.303633015763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.296794705962" Y="4.613813207527" />
                  <Point X="-22.661989039872" Y="3.070929740455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.449081074919" Y="2.556080854953" />
                  <Point X="-21.010890132722" Y="2.370079835299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.150372408233" Y="4.654864865463" />
                  <Point X="-25.225135611077" Y="4.26212514604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.87909764575" Y="4.115240744306" />
                  <Point X="-22.614132323222" Y="3.153820005305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.34606481448" Y="2.61555728259" />
                  <Point X="-20.924295086531" Y="2.436526654836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.003950110504" Y="4.695916523399" />
                  <Point X="-25.257058357297" Y="4.378879783728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.800972833694" Y="4.18528296492" />
                  <Point X="-22.566275606572" Y="3.236710270154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.243048554041" Y="2.675033710227" />
                  <Point X="-20.887780238347" Y="2.524231257218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.835313812095" Y="4.727538897479" />
                  <Point X="-25.288260543481" Y="4.495328561827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.759559571085" Y="4.270908313739" />
                  <Point X="-22.518418889923" Y="3.319600535003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.140032293602" Y="2.734510137864" />
                  <Point X="-20.976116884074" Y="2.664932174531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.644728194831" Y="4.749844338472" />
                  <Point X="-25.319462729665" Y="4.611777339926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.734730313594" Y="4.363573155083" />
                  <Point X="-22.470562173273" Y="3.402490799852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.454142577566" Y="4.772149779465" />
                  <Point X="-25.350664915849" Y="4.728226118025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.709901056103" Y="4.456237996426" />
                  <Point X="-22.422705456624" Y="3.485381064702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.685071798612" Y="4.54890283777" />
                  <Point X="-22.374848739974" Y="3.568271329551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.660242541122" Y="4.641567679113" />
                  <Point X="-22.326992023324" Y="3.6511615944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.635413283631" Y="4.734232520457" />
                  <Point X="-22.279135306675" Y="3.734051859249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.475894668424" Y="4.769725121438" />
                  <Point X="-22.231278590025" Y="3.816942124099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.138940345668" Y="4.729900733069" />
                  <Point X="-22.183421873376" Y="3.899832388948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.575115279176" Y="4.593775427449" />
                  <Point X="-22.665363413311" Y="4.207608671389" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001626220703" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978484863281" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.520373046875" Y="-3.567288085938" />
                  <Point X="-24.528458984375" Y="-3.537111816406" />
                  <Point X="-24.538984375" Y="-3.516782470703" />
                  <Point X="-24.699408203125" Y="-3.285643798828" />
                  <Point X="-24.71092578125" Y="-3.273825195312" />
                  <Point X="-24.73077734375" Y="-3.2648125" />
                  <Point X="-24.9790234375" Y="-3.187766601562" />
                  <Point X="-24.99384375" Y="-3.18551953125" />
                  <Point X="-25.01377734375" Y="-3.189353759766" />
                  <Point X="-25.2620234375" Y="-3.266399902344" />
                  <Point X="-25.27676171875" Y="-3.273825439453" />
                  <Point X="-25.291583984375" Y="-3.290405517578" />
                  <Point X="-25.452005859375" Y="-3.521544433594" />
                  <Point X="-25.4592265625" Y="-3.537111816406" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.10475390625" Y="-4.936905273438" />
                  <Point X="-26.351587890625" Y="-4.873396484375" />
                  <Point X="-26.310123046875" Y="-4.558431640625" />
                  <Point X="-26.309150390625" Y="-4.551046875" />
                  <Point X="-26.310904296875" Y="-4.528615234375" />
                  <Point X="-26.3702109375" Y="-4.230465820313" />
                  <Point X="-26.3759453125" Y="-4.215224609375" />
                  <Point X="-26.390990234375" Y="-4.198499511719" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.633375" Y="-3.989461914063" />
                  <Point X="-26.655490234375" Y="-3.985353271484" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.975041015625" Y="-3.967068115234" />
                  <Point X="-26.9950859375" Y="-3.977270263672" />
                  <Point X="-27.24784375" Y="-4.146159179688" />
                  <Point X="-27.259734375" Y="-4.157296386719" />
                  <Point X="-27.457095703125" Y="-4.4145" />
                  <Point X="-27.855833984375" Y="-4.167609863281" />
                  <Point X="-27.862080078125" Y="-4.162801269531" />
                  <Point X="-28.228580078125" Y="-3.880608398438" />
                  <Point X="-27.521060546875" Y="-2.655147216797" />
                  <Point X="-27.506033203125" Y="-2.629120849609" />
                  <Point X="-27.4997578125" Y="-2.597620117188" />
                  <Point X="-27.514328125" Y="-2.568414306641" />
                  <Point X="-27.531326171875" Y="-2.551416015625" />
                  <Point X="-27.560154296875" Y="-2.537198730469" />
                  <Point X="-27.591681640625" Y="-2.543469238281" />
                  <Point X="-28.842958984375" Y="-3.265894287109" />
                  <Point X="-29.16169921875" Y="-2.847135253906" />
                  <Point X="-29.1661796875" Y="-2.839621826172" />
                  <Point X="-29.43101953125" Y="-2.395526611328" />
                  <Point X="-28.190078125" Y="-1.443318603516" />
                  <Point X="-28.163787109375" Y="-1.42314453125" />
                  <Point X="-28.145666015625" Y="-1.395412719727" />
                  <Point X="-28.1381171875" Y="-1.366265869141" />
                  <Point X="-28.140330078125" Y="-1.334582763672" />
                  <Point X="-28.161716796875" Y="-1.310311157227" />
                  <Point X="-28.187640625" Y="-1.295053100586" />
                  <Point X="-28.219529296875" Y="-1.288571044922" />
                  <Point X="-29.80328125" Y="-1.497076049805" />
                  <Point X="-29.927392578125" Y="-1.011186523437" />
                  <Point X="-29.928576171875" Y="-1.002910766602" />
                  <Point X="-29.99839453125" Y="-0.5147421875" />
                  <Point X="-28.587412109375" Y="-0.136670379639" />
                  <Point X="-28.557462890625" Y="-0.128645339966" />
                  <Point X="-28.5413359375" Y="-0.121036483765" />
                  <Point X="-28.514142578125" Y="-0.102163269043" />
                  <Point X="-28.494685546875" Y="-0.075220100403" />
                  <Point X="-28.4856484375" Y="-0.04610112381" />
                  <Point X="-28.485833984375" Y="-0.015857367516" />
                  <Point X="-28.4948984375" Y="0.013348042488" />
                  <Point X="-28.514703125" Y="0.039991966248" />
                  <Point X="-28.541896484375" Y="0.058865337372" />
                  <Point X="-28.557462890625" Y="0.066085227966" />
                  <Point X="-29.998185546875" Y="0.452125915527" />
                  <Point X="-29.91764453125" Y="0.996413513184" />
                  <Point X="-29.9152578125" Y="1.005222412109" />
                  <Point X="-29.773515625" Y="1.528298706055" />
                  <Point X="-28.774732421875" Y="1.396806640625" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.73168359375" Y="1.395872802734" />
                  <Point X="-28.73046484375" Y="1.396257568359" />
                  <Point X="-28.67027734375" Y="1.415234130859" />
                  <Point X="-28.651513671875" Y="1.426073486328" />
                  <Point X="-28.639099609375" Y="1.443833374023" />
                  <Point X="-28.638623046875" Y="1.444986694336" />
                  <Point X="-28.61447265625" Y="1.503289672852" />
                  <Point X="-28.610712890625" Y="1.524593139648" />
                  <Point X="-28.6163046875" Y="1.545490478516" />
                  <Point X="-28.616916015625" Y="1.546665649414" />
                  <Point X="-28.646056640625" Y="1.602642211914" />
                  <Point X="-28.65996875" Y="1.619221801758" />
                  <Point X="-29.47610546875" Y="2.245465820312" />
                  <Point X="-29.160013671875" Y="2.787005859375" />
                  <Point X="-29.153693359375" Y="2.795131347656" />
                  <Point X="-28.774669921875" Y="3.282311035156" />
                  <Point X="-28.172341796875" Y="2.934556396484" />
                  <Point X="-28.15915625" Y="2.926943603516" />
                  <Point X="-28.138513671875" Y="2.920434814453" />
                  <Point X="-28.136787109375" Y="2.920283691406" />
                  <Point X="-28.05296484375" Y="2.912950195312" />
                  <Point X="-28.031515625" Y="2.915771972656" />
                  <Point X="-28.013265625" Y="2.927390625" />
                  <Point X="-28.012025390625" Y="2.928630126953" />
                  <Point X="-27.95252734375" Y="2.988127929688" />
                  <Point X="-27.94090234375" Y="3.006372802734" />
                  <Point X="-27.938072265625" Y="3.027821044922" />
                  <Point X="-27.93822265625" Y="3.029568603516" />
                  <Point X="-27.945556640625" Y="3.113391113281" />
                  <Point X="-27.952064453125" Y="3.134029541016" />
                  <Point X="-28.307279296875" Y="3.749276611328" />
                  <Point X="-27.752876953125" Y="4.174330566406" />
                  <Point X="-27.74292578125" Y="4.179859375" />
                  <Point X="-27.141546875" Y="4.513972167969" />
                  <Point X="-26.971861328125" Y="4.29283203125" />
                  <Point X="-26.96782421875" Y="4.287571289062" />
                  <Point X="-26.95124609375" Y="4.273660644531" />
                  <Point X="-26.94932421875" Y="4.27266015625" />
                  <Point X="-26.856029296875" Y="4.22409375" />
                  <Point X="-26.835119140625" Y="4.218491699219" />
                  <Point X="-26.81380078125" Y="4.222253417969" />
                  <Point X="-26.8118046875" Y="4.223080078125" />
                  <Point X="-26.7146328125" Y="4.263330078125" />
                  <Point X="-26.69689453125" Y="4.27575390625" />
                  <Point X="-26.68607421875" Y="4.294512695312" />
                  <Point X="-26.685423828125" Y="4.296579101562" />
                  <Point X="-26.653802734375" Y="4.396864746094" />
                  <Point X="-26.651916015625" Y="4.418425292969" />
                  <Point X="-26.68913671875" Y="4.701141113281" />
                  <Point X="-25.96809375" Y="4.903296386719" />
                  <Point X="-25.956033203125" Y="4.904707519531" />
                  <Point X="-25.22419921875" Y="4.990358398438" />
                  <Point X="-25.046041015625" Y="4.3254609375" />
                  <Point X="-25.042140625" Y="4.310904296875" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.7633515625" Y="4.990868652344" />
                  <Point X="-24.139794921875" Y="4.925565429688" />
                  <Point X="-24.129814453125" Y="4.92315625" />
                  <Point X="-23.4915390625" Y="4.769057128906" />
                  <Point X="-23.485529296875" Y="4.766877441406" />
                  <Point X="-23.068970703125" Y="4.615788085938" />
                  <Point X="-23.06268359375" Y="4.61284765625" />
                  <Point X="-22.66130078125" Y="4.425135253906" />
                  <Point X="-22.65522265625" Y="4.421593261719" />
                  <Point X="-22.26747265625" Y="4.195689941406" />
                  <Point X="-22.26175" Y="4.191620117188" />
                  <Point X="-21.931259765625" Y="3.956593261719" />
                  <Point X="-22.752755859375" Y="2.533720214844" />
                  <Point X="-22.770146484375" Y="2.503596435547" />
                  <Point X="-22.775580078125" Y="2.48989453125" />
                  <Point X="-22.7966171875" Y="2.411228759766" />
                  <Point X="-22.79778125" Y="2.390891601562" />
                  <Point X="-22.789583984375" Y="2.322901855469" />
                  <Point X="-22.78131640625" Y="2.300811767578" />
                  <Point X="-22.78044921875" Y="2.299533935547" />
                  <Point X="-22.738357421875" Y="2.237503173828" />
                  <Point X="-22.72378125" Y="2.223336425781" />
                  <Point X="-22.66175" Y="2.18124609375" />
                  <Point X="-22.639673828125" Y="2.172981689453" />
                  <Point X="-22.63826171875" Y="2.172811035156" />
                  <Point X="-22.57023828125" Y="2.164608642578" />
                  <Point X="-22.54971484375" Y="2.166379882812" />
                  <Point X="-22.471048828125" Y="2.187416015625" />
                  <Point X="-22.45896484375" Y="2.192417724609" />
                  <Point X="-21.005751953125" Y="3.031430419922" />
                  <Point X="-20.797404296875" Y="2.741875244141" />
                  <Point X="-20.79421484375" Y="2.73660546875" />
                  <Point X="-20.612486328125" Y="2.436295898438" />
                  <Point X="-21.688484375" Y="1.610653320312" />
                  <Point X="-21.7113828125" Y="1.593082885742" />
                  <Point X="-21.721794921875" Y="1.582311401367" />
                  <Point X="-21.77841015625" Y="1.508451660156" />
                  <Point X="-21.787314453125" Y="1.489946655273" />
                  <Point X="-21.808404296875" Y="1.414535766602" />
                  <Point X="-21.809216796875" Y="1.390944458008" />
                  <Point X="-21.808859375" Y="1.389215942383" />
                  <Point X="-21.79155078125" Y="1.305332519531" />
                  <Point X="-21.7833828125" Y="1.286481201172" />
                  <Point X="-21.736296875" Y="1.21491027832" />
                  <Point X="-21.719044921875" Y="1.198815429688" />
                  <Point X="-21.717638671875" Y="1.198024291992" />
                  <Point X="-21.64941015625" Y="1.159617553711" />
                  <Point X="-21.629533203125" Y="1.153368408203" />
                  <Point X="-21.537271484375" Y="1.141174926758" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-20.1510234375" Y="1.321953491211" />
                  <Point X="-20.06080859375" Y="0.951367980957" />
                  <Point X="-20.0598046875" Y="0.944924621582" />
                  <Point X="-20.002140625" Y="0.57455645752" />
                  <Point X="-21.23258984375" Y="0.244858352661" />
                  <Point X="-21.25883203125" Y="0.237826919556" />
                  <Point X="-21.272779296875" Y="0.231739990234" />
                  <Point X="-21.363421875" Y="0.179346893311" />
                  <Point X="-21.37885546875" Y="0.165498687744" />
                  <Point X="-21.433240234375" Y="0.096198623657" />
                  <Point X="-21.443015625" Y="0.074724975586" />
                  <Point X="-21.44338671875" Y="0.072785186768" />
                  <Point X="-21.461515625" Y="-0.021875238419" />
                  <Point X="-21.461142578125" Y="-0.042635002136" />
                  <Point X="-21.443013671875" Y="-0.137295425415" />
                  <Point X="-21.43323046875" Y="-0.158771057129" />
                  <Point X="-21.432119140625" Y="-0.160186340332" />
                  <Point X="-21.377734375" Y="-0.229486404419" />
                  <Point X="-21.3615546875" Y="-0.24298626709" />
                  <Point X="-21.270912109375" Y="-0.295379364014" />
                  <Point X="-21.25883203125" Y="-0.300387023926" />
                  <Point X="-20.001931640625" Y="-0.637172424316" />
                  <Point X="-20.05156640625" Y="-0.966394897461" />
                  <Point X="-20.05285546875" Y="-0.972047119141" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-21.557583984375" Y="-1.101634765625" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.608828125" Y="-1.099137939453" />
                  <Point X="-21.7867265625" Y="-1.137805053711" />
                  <Point X="-21.816767578125" Y="-1.157361328125" />
                  <Point X="-21.924296875" Y="-1.286685180664" />
                  <Point X="-21.935958984375" Y="-1.317521240234" />
                  <Point X="-21.951369140625" Y="-1.485001220703" />
                  <Point X="-21.950150390625" Y="-1.501453613281" />
                  <Point X="-21.941611328125" Y="-1.519776855469" />
                  <Point X="-21.843158203125" Y="-1.672912597656" />
                  <Point X="-21.8315390625" Y="-1.685540039062" />
                  <Point X="-20.660923828125" Y="-2.583784179688" />
                  <Point X="-20.795869140625" Y="-2.802144042969" />
                  <Point X="-20.798533203125" Y="-2.805930175781" />
                  <Point X="-20.943310546875" Y="-3.011638183594" />
                  <Point X="-22.219439453125" Y="-2.274864501953" />
                  <Point X="-22.246544921875" Y="-2.259215332031" />
                  <Point X="-22.26701953125" Y="-2.252525146484" />
                  <Point X="-22.478748046875" Y="-2.214287109375" />
                  <Point X="-22.49525" Y="-2.214074462891" />
                  <Point X="-22.514544921875" Y="-2.221151855469" />
                  <Point X="-22.690439453125" Y="-2.313723876953" />
                  <Point X="-22.713306640625" Y="-2.338307128906" />
                  <Point X="-22.80587890625" Y="-2.514201660156" />
                  <Point X="-22.81105078125" Y="-2.529880615234" />
                  <Point X="-22.810044921875" Y="-2.550750732422" />
                  <Point X="-22.77180859375" Y="-2.76246484375" />
                  <Point X="-22.76590625" Y="-2.778578125" />
                  <Point X="-22.01332421875" Y="-4.082087158203" />
                  <Point X="-22.1647109375" Y="-4.190218261719" />
                  <Point X="-22.1676796875" Y="-4.192140625" />
                  <Point X="-22.320224609375" Y="-4.290879394531" />
                  <Point X="-23.296138671875" Y="-3.019043945312" />
                  <Point X="-23.3168203125" Y="-2.992088867188" />
                  <Point X="-23.333751953125" Y="-2.977701416016" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.55773828125" Y="-2.836937255859" />
                  <Point X="-23.578900390625" Y="-2.836150146484" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.838298828125" Y="-2.871529785156" />
                  <Point X="-24.014650390625" Y="-3.018159667969" />
                  <Point X="-24.025423828125" Y="-3.030659667969" />
                  <Point X="-24.03262890625" Y="-3.050983642578" />
                  <Point X="-24.085357421875" Y="-3.293573974609" />
                  <Point X="-24.0860703125" Y="-3.310718505859" />
                  <Point X="-23.87235546875" Y="-4.934028808594" />
                  <Point X="-24.0056484375" Y="-4.96324609375" />
                  <Point X="-24.00841015625" Y="-4.963748046875" />
                  <Point X="-24.139798828125" Y="-4.987616210938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#118" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.00390064914" Y="4.369681895088" Z="0.05" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.05" />
                  <Point X="-0.968003380622" Y="4.985648828303" Z="0.05" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.05" />
                  <Point X="-1.735049949642" Y="4.773202724265" Z="0.05" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.05" />
                  <Point X="-1.749657495632" Y="4.76229068577" Z="0.05" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.05" />
                  <Point X="-1.739273588101" Y="4.342870572963" Z="0.05" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.05" />
                  <Point X="-1.837097220393" Y="4.300553770622" Z="0.05" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.05" />
                  <Point X="-1.932393262754" Y="4.348290817886" Z="0.05" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.05" />
                  <Point X="-1.93835169858" Y="4.354551788394" Z="0.05" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.05" />
                  <Point X="-2.773365456428" Y="4.254846840066" Z="0.05" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.05" />
                  <Point X="-3.366716927053" Y="3.802710162212" Z="0.05" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.05" />
                  <Point X="-3.371056583724" Y="3.780360893541" Z="0.05" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.05" />
                  <Point X="-2.99419109081" Y="3.056490372642" Z="0.05" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.05" />
                  <Point X="-3.053537848399" Y="2.995265719487" Z="0.05" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.05" />
                  <Point X="-3.138586092965" Y="3.001373608363" Z="0.05" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.05" />
                  <Point X="-3.153498454756" Y="3.009137365022" Z="0.05" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.05" />
                  <Point X="-4.19931624633" Y="2.857109324516" Z="0.05" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.05" />
                  <Point X="-4.540791668474" Y="2.275656880946" Z="0.05" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.05" />
                  <Point X="-4.530474828928" Y="2.250717646477" Z="0.05" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.05" />
                  <Point X="-3.667422733659" Y="1.554857547071" Z="0.05" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.05" />
                  <Point X="-3.690972397514" Y="1.495401206497" Z="0.05" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.05" />
                  <Point X="-3.751656180949" Y="1.475223470073" Z="0.05" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.05" />
                  <Point X="-3.774364875135" Y="1.477658957397" Z="0.05" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.05" />
                  <Point X="-4.969675146706" Y="1.04957976763" Z="0.05" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.05" />
                  <Point X="-5.058560937174" Y="0.458578267408" Z="0.05" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.05" />
                  <Point X="-5.030377171099" Y="0.438617977872" Z="0.05" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.05" />
                  <Point X="-3.549368052329" Y="0.03019575693" Z="0.05" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.05" />
                  <Point X="-3.539743475892" Y="0.00060165449" Z="0.05" />
                  <Point X="-3.539556741714" Y="0" Z="0.05" />
                  <Point X="-3.548621125243" Y="-0.029205296571" Z="0.05" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.05" />
                  <Point X="-3.576000542794" Y="-0.048680226204" Z="0.05" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.05" />
                  <Point X="-3.606510617673" Y="-0.057094078884" Z="0.05" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.05" />
                  <Point X="-4.984229401837" Y="-0.978709775635" Z="0.05" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.05" />
                  <Point X="-4.849643612815" Y="-1.510431759269" Z="0.05" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.05" />
                  <Point X="-4.814047238116" Y="-1.516834302359" Z="0.05" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.05" />
                  <Point X="-3.193210857741" Y="-1.322135189324" Z="0.05" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.05" />
                  <Point X="-3.200225286589" Y="-1.351596718477" Z="0.05" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.05" />
                  <Point X="-3.226672215948" Y="-1.372371280762" Z="0.05" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.05" />
                  <Point X="-4.215280109534" Y="-2.833951380975" Z="0.05" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.05" />
                  <Point X="-3.86924543407" Y="-3.290720878224" Z="0.05" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.05" />
                  <Point X="-3.836212306232" Y="-3.284899591101" Z="0.05" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.05" />
                  <Point X="-2.555841151336" Y="-2.572489317796" Z="0.05" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.05" />
                  <Point X="-2.570517422067" Y="-2.59886607626" Z="0.05" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.05" />
                  <Point X="-2.89874028676" Y="-4.171138553667" Z="0.05" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.05" />
                  <Point X="-2.459986207747" Y="-4.444050442405" Z="0.05" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.05" />
                  <Point X="-2.446578229853" Y="-4.443625547797" Z="0.05" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.05" />
                  <Point X="-1.973463056298" Y="-3.987563618858" Z="0.05" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.05" />
                  <Point X="-1.66491591836" Y="-4.003966348519" Z="0.05" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.05" />
                  <Point X="-1.430114359069" Y="-4.204811361994" Z="0.05" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.05" />
                  <Point X="-1.366100284126" Y="-4.507090090598" Z="0.05" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.05" />
                  <Point X="-1.365851868284" Y="-4.520625426187" Z="0.05" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.05" />
                  <Point X="-1.12337049826" Y="-4.954047888065" Z="0.05" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.05" />
                  <Point X="-0.823718218124" Y="-5.012588597834" Z="0.05" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.05" />
                  <Point X="-0.809582330895" Y="-4.983586521031" Z="0.05" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.05" />
                  <Point X="-0.256663929494" Y="-3.287634335822" Z="0.05" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.05" />
                  <Point X="-0.005114063067" Y="-3.205826584888" Z="0.05" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.05" />
                  <Point X="0.248245016294" Y="-3.2812854604" Z="0.05" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.05" />
                  <Point X="0.413781929938" Y="-3.514011419946" Z="0.05" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.05" />
                  <Point X="0.425172529506" Y="-3.548949508784" Z="0.05" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.05" />
                  <Point X="0.994369946537" Y="-4.981662670772" Z="0.05" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.05" />
                  <Point X="1.173438988355" Y="-4.942547478528" Z="0.05" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.05" />
                  <Point X="1.172618176336" Y="-4.9080696609" Z="0.05" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.05" />
                  <Point X="1.010073773645" Y="-3.030323580723" Z="0.05" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.05" />
                  <Point X="1.187510772313" Y="-2.878696142601" Z="0.05" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.05" />
                  <Point X="1.419525591962" Y="-2.854659535104" Z="0.05" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.05" />
                  <Point X="1.63305199199" Y="-2.988479542273" Z="0.05" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.05" />
                  <Point X="1.658037361379" Y="-3.01820047973" Z="0.05" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.05" />
                  <Point X="2.853332915767" Y="-4.202834719354" Z="0.05" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.05" />
                  <Point X="3.042692806531" Y="-4.067843367192" Z="0.05" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.05" />
                  <Point X="3.030863632857" Y="-4.03801017363" Z="0.05" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.05" />
                  <Point X="2.232998849965" Y="-2.510569975499" Z="0.05" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.05" />
                  <Point X="2.324783132933" Y="-2.330313774627" Z="0.05" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.05" />
                  <Point X="2.502584603746" Y="-2.234118177034" Z="0.05" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.05" />
                  <Point X="2.717936828013" Y="-2.270449159526" Z="0.05" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.05" />
                  <Point X="2.749403423477" Y="-2.286885867417" Z="0.05" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.05" />
                  <Point X="4.236197797082" Y="-2.803427478795" Z="0.05" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.05" />
                  <Point X="4.395989308518" Y="-2.545556071662" Z="0.05" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.05" />
                  <Point X="4.374855987248" Y="-2.521660483069" Z="0.05" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.05" />
                  <Point X="3.094290705691" Y="-1.461457228435" Z="0.05" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.05" />
                  <Point X="3.107673967496" Y="-1.29082246569" Z="0.05" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.05" />
                  <Point X="3.2155203405" Y="-1.158048366925" Z="0.05" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.05" />
                  <Point X="3.395634976776" Y="-1.116717001991" Z="0.05" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.05" />
                  <Point X="3.429732983189" Y="-1.119927020836" Z="0.05" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.05" />
                  <Point X="4.989734132655" Y="-0.951891064569" Z="0.05" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.05" />
                  <Point X="5.04687311413" Y="-0.576735963862" Z="0.05" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.05" />
                  <Point X="5.02177330617" Y="-0.562129824021" Z="0.05" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.05" />
                  <Point X="3.657310032407" Y="-0.168417550475" Z="0.05" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.05" />
                  <Point X="3.601057114339" Y="-0.09803817216" Z="0.05" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.05" />
                  <Point X="3.581808190258" Y="-0.001950087443" Z="0.05" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.05" />
                  <Point X="3.599563260166" Y="0.094660443805" Z="0.05" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.05" />
                  <Point X="3.654322324062" Y="0.165910566415" Z="0.05" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.05" />
                  <Point X="3.746085381945" Y="0.219731293767" Z="0.05" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.05" />
                  <Point X="3.774194511136" Y="0.227842108748" Z="0.05" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.05" />
                  <Point X="4.983443123711" Y="0.983896672156" Z="0.05" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.05" />
                  <Point X="4.882830763218" Y="1.400261684516" Z="0.05" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.05" />
                  <Point X="4.852169860137" Y="1.404895836607" Z="0.05" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.05" />
                  <Point X="3.370861258359" Y="1.234217386644" Z="0.05" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.05" />
                  <Point X="3.300724279901" Y="1.272879727956" Z="0.05" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.05" />
                  <Point X="3.252231039542" Y="1.345241980211" Z="0.05" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.05" />
                  <Point X="3.233948682095" Y="1.430620703531" Z="0.05" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.05" />
                  <Point X="3.254681524036" Y="1.507760171147" Z="0.05" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.05" />
                  <Point X="3.31173182357" Y="1.583173524272" Z="0.05" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.05" />
                  <Point X="3.335796344946" Y="1.602265496211" Z="0.05" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.05" />
                  <Point X="4.242405230237" Y="2.793771726203" Z="0.05" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.05" />
                  <Point X="4.007023273151" Y="3.122008330542" Z="0.05" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.05" />
                  <Point X="3.972137346875" Y="3.11123459334" Z="0.05" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.05" />
                  <Point X="2.431212237163" Y="2.245962539392" Z="0.05" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.05" />
                  <Point X="2.361568131808" Y="2.253731551859" Z="0.05" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.05" />
                  <Point X="2.298135961221" Y="2.295990972545" Z="0.05" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.05" />
                  <Point X="2.25476762769" Y="2.358888899161" Z="0.05" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.05" />
                  <Point X="2.2456981509" Y="2.428190312024" Z="0.05" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.05" />
                  <Point X="2.266565403266" Y="2.508257338719" Z="0.05" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.05" />
                  <Point X="2.284390759519" Y="2.54000173508" Z="0.05" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.05" />
                  <Point X="2.761069827592" Y="4.263647221515" Z="0.05" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.05" />
                  <Point X="2.363791019259" Y="4.496076097035" Z="0.05" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.05" />
                  <Point X="1.952342054843" Y="4.689419755257" Z="0.05" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.05" />
                  <Point X="1.52536251974" Y="4.84516068365" Z="0.05" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.05" />
                  <Point X="0.875763290288" Y="5.003039966007" Z="0.05" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.05" />
                  <Point X="0.206754693237" Y="5.074908160767" Z="0.05" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.05" />
                  <Point X="0.189343912413" Y="5.061765610863" Z="0.05" />
                  <Point X="0" Y="4.355124473572" Z="0.05" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>