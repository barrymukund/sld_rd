<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#211" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3419" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="-25" Y="0" />
          <Width Value="10.108341217041" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000473632812" Y="0.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.4366953125" Y="-3.512525634766" />
                  <Point X="-24.44227734375" Y="-3.497142333984" />
                  <Point X="-24.457634765625" Y="-3.467375976562" />
                  <Point X="-24.609197265625" Y="-3.249003662109" />
                  <Point X="-24.62136328125" Y="-3.231475585938" />
                  <Point X="-24.64325" Y="-3.209018798828" />
                  <Point X="-24.669505859375" Y="-3.189775634766" />
                  <Point X="-24.69750390625" Y="-3.175669189453" />
                  <Point X="-24.932037109375" Y="-3.102878417969" />
                  <Point X="-24.95086328125" Y="-3.097035888672" />
                  <Point X="-24.9790234375" Y="-3.092766357422" />
                  <Point X="-25.0086640625" Y="-3.092766357422" />
                  <Point X="-25.03682421875" Y="-3.097035644531" />
                  <Point X="-25.271357421875" Y="-3.169826171875" />
                  <Point X="-25.29018359375" Y="-3.175668945312" />
                  <Point X="-25.31818359375" Y="-3.189776367188" />
                  <Point X="-25.344439453125" Y="-3.209020019531" />
                  <Point X="-25.36632421875" Y="-3.2314765625" />
                  <Point X="-25.517884765625" Y="-3.449848632812" />
                  <Point X="-25.53005078125" Y="-3.467376953125" />
                  <Point X="-25.5381875" Y="-3.481572753906" />
                  <Point X="-25.550990234375" Y="-3.512525146484" />
                  <Point X="-25.58075390625" Y="-3.623607421875" />
                  <Point X="-25.9165859375" Y="-4.87694140625" />
                  <Point X="-26.061345703125" Y="-4.848843261719" />
                  <Point X="-26.07933203125" Y="-4.845352050781" />
                  <Point X="-26.246416015625" Y="-4.802362792969" />
                  <Point X="-26.2149609375" Y="-4.563438476562" />
                  <Point X="-26.21419921875" Y="-4.547929199219" />
                  <Point X="-26.2165078125" Y="-4.516222167969" />
                  <Point X="-26.2725390625" Y="-4.234540527344" />
                  <Point X="-26.277037109375" Y="-4.211930664062" />
                  <Point X="-26.287939453125" Y="-4.182962890625" />
                  <Point X="-26.30401171875" Y="-4.155126464844" />
                  <Point X="-26.32364453125" Y="-4.131203613281" />
                  <Point X="-26.539572265625" Y="-3.941839111328" />
                  <Point X="-26.556904296875" Y="-3.926639404297" />
                  <Point X="-26.5831875" Y="-3.910295410156" />
                  <Point X="-26.612884765625" Y="-3.897994384766" />
                  <Point X="-26.64302734375" Y="-3.890966308594" />
                  <Point X="-26.92961328125" Y="-3.872182373047" />
                  <Point X="-26.9526171875" Y="-3.870674804688" />
                  <Point X="-26.983419921875" Y="-3.873708984375" />
                  <Point X="-27.014470703125" Y="-3.882029785156" />
                  <Point X="-27.04266015625" Y="-3.894802734375" />
                  <Point X="-27.281458984375" Y="-4.054363037109" />
                  <Point X="-27.300625" Y="-4.067170654297" />
                  <Point X="-27.312791015625" Y="-4.076826416016" />
                  <Point X="-27.335099609375" Y="-4.0994609375" />
                  <Point X="-27.351810546875" Y="-4.12123828125" />
                  <Point X="-27.480146484375" Y="-4.288490234375" />
                  <Point X="-27.77533984375" Y="-4.10571484375" />
                  <Point X="-27.8017109375" Y="-4.089386230469" />
                  <Point X="-28.104720703125" Y="-3.856078125" />
                  <Point X="-27.423759765625" Y="-2.676619384766" />
                  <Point X="-27.412859375" Y="-2.647654541016" />
                  <Point X="-27.406587890625" Y="-2.616128173828" />
                  <Point X="-27.40557421875" Y="-2.585194091797" />
                  <Point X="-27.41455859375" Y="-2.555575927734" />
                  <Point X="-27.428775390625" Y="-2.526747070312" />
                  <Point X="-27.44680078125" Y="-2.501591308594" />
                  <Point X="-27.462859375" Y="-2.485531738281" />
                  <Point X="-27.4641484375" Y="-2.484242675781" />
                  <Point X="-27.489310546875" Y="-2.466212402344" />
                  <Point X="-27.518140625" Y="-2.451995361328" />
                  <Point X="-27.5477578125" Y="-2.443011230469" />
                  <Point X="-27.578689453125" Y="-2.444024169922" />
                  <Point X="-27.61021484375" Y="-2.450295166016" />
                  <Point X="-27.639181640625" Y="-2.461197265625" />
                  <Point X="-27.73498828125" Y="-2.516511230469" />
                  <Point X="-28.818021484375" Y="-3.14180078125" />
                  <Point X="-29.062025390625" Y="-2.821231201172" />
                  <Point X="-29.08286328125" Y="-2.793853515625" />
                  <Point X="-29.306142578125" Y="-2.419448730469" />
                  <Point X="-28.105955078125" Y="-1.498513061523" />
                  <Point X="-28.084578125" Y="-1.475594726562" />
                  <Point X="-28.06661328125" Y="-1.448464599609" />
                  <Point X="-28.053857421875" Y="-1.419836181641" />
                  <Point X="-28.0467265625" Y="-1.392310302734" />
                  <Point X="-28.04615234375" Y="-1.390094848633" />
                  <Point X="-28.043345703125" Y="-1.359645874023" />
                  <Point X="-28.045556640625" Y="-1.327977050781" />
                  <Point X="-28.05255859375" Y="-1.298234130859" />
                  <Point X="-28.068642578125" Y="-1.272252807617" />
                  <Point X="-28.089474609375" Y="-1.248298339844" />
                  <Point X="-28.11297265625" Y="-1.228765625" />
                  <Point X="-28.13748828125" Y="-1.214337402344" />
                  <Point X="-28.1394453125" Y="-1.213185668945" />
                  <Point X="-28.1686953125" Y="-1.20196484375" />
                  <Point X="-28.200591796875" Y="-1.195477172852" />
                  <Point X="-28.231927734375" Y="-1.194383789062" />
                  <Point X="-28.352875" Y="-1.210306762695" />
                  <Point X="-29.732099609375" Y="-1.391885375977" />
                  <Point X="-29.825927734375" Y="-1.024558105469" />
                  <Point X="-29.83407421875" Y="-0.992656982422" />
                  <Point X="-29.892421875" Y="-0.584697998047" />
                  <Point X="-28.532875" Y="-0.220408218384" />
                  <Point X="-28.51749609375" Y="-0.21482901001" />
                  <Point X="-28.487732421875" Y="-0.199472229004" />
                  <Point X="-28.462048828125" Y="-0.181647064209" />
                  <Point X="-28.459986328125" Y="-0.180216552734" />
                  <Point X="-28.437525390625" Y="-0.158330200195" />
                  <Point X="-28.418279296875" Y="-0.132074874878" />
                  <Point X="-28.404166015625" Y="-0.104066215515" />
                  <Point X="-28.395603515625" Y="-0.076474067688" />
                  <Point X="-28.394916015625" Y="-0.074259292603" />
                  <Point X="-28.3906484375" Y="-0.046103248596" />
                  <Point X="-28.3906484375" Y="-0.016456920624" />
                  <Point X="-28.394916015625" Y="0.011699280739" />
                  <Point X="-28.403478515625" Y="0.039291427612" />
                  <Point X="-28.404166015625" Y="0.041506202698" />
                  <Point X="-28.418279296875" Y="0.069514862061" />
                  <Point X="-28.437525390625" Y="0.095770042419" />
                  <Point X="-28.459978515625" Y="0.117650314331" />
                  <Point X="-28.485669921875" Y="0.135481124878" />
                  <Point X="-28.49798828125" Y="0.142722290039" />
                  <Point X="-28.532875" Y="0.157848205566" />
                  <Point X="-28.643123046875" Y="0.187389175415" />
                  <Point X="-29.89181640625" Y="0.521975524902" />
                  <Point X="-29.82973828125" Y="0.941493713379" />
                  <Point X="-29.82448828125" Y="0.976969665527" />
                  <Point X="-29.70355078125" Y="1.423267944336" />
                  <Point X="-28.7656640625" Y="1.29979284668" />
                  <Point X="-28.744984375" Y="1.299341796875" />
                  <Point X="-28.723423828125" Y="1.301227905273" />
                  <Point X="-28.70313671875" Y="1.305263183594" />
                  <Point X="-28.646275390625" Y="1.323191650391" />
                  <Point X="-28.6417109375" Y="1.324630981445" />
                  <Point X="-28.62277734375" Y="1.332961303711" />
                  <Point X="-28.604033203125" Y="1.343783081055" />
                  <Point X="-28.5873515625" Y="1.356014892578" />
                  <Point X="-28.573712890625" Y="1.371567504883" />
                  <Point X="-28.561298828125" Y="1.389297363281" />
                  <Point X="-28.5513515625" Y="1.407430175781" />
                  <Point X="-28.52853515625" Y="1.462512939453" />
                  <Point X="-28.52669921875" Y="1.466946411133" />
                  <Point X="-28.520908203125" Y="1.4868203125" />
                  <Point X="-28.517154296875" Y="1.508129638672" />
                  <Point X="-28.5158046875" Y="1.528765869141" />
                  <Point X="-28.518951171875" Y="1.549205078125" />
                  <Point X="-28.524552734375" Y="1.570104980469" />
                  <Point X="-28.532048828125" Y="1.589376342773" />
                  <Point X="-28.559578125" Y="1.642261230469" />
                  <Point X="-28.56180859375" Y="1.646544189453" />
                  <Point X="-28.573294921875" Y="1.663725830078" />
                  <Point X="-28.587203125" Y="1.680296508789" />
                  <Point X="-28.60213671875" Y="1.694590209961" />
                  <Point X="-28.665375" Y="1.743114990234" />
                  <Point X="-29.351859375" Y="2.269873291016" />
                  <Point X="-29.10155078125" Y="2.6987109375" />
                  <Point X="-29.081150390625" Y="2.733663574219" />
                  <Point X="-28.75050390625" Y="3.158662109375" />
                  <Point X="-28.20665625" Y="2.844670898438" />
                  <Point X="-28.187724609375" Y="2.836340576172" />
                  <Point X="-28.167083984375" Y="2.829832275391" />
                  <Point X="-28.146794921875" Y="2.825796630859" />
                  <Point X="-28.0676015625" Y="2.818868164062" />
                  <Point X="-28.06124609375" Y="2.818312011719" />
                  <Point X="-28.040568359375" Y="2.818762939453" />
                  <Point X="-28.019109375" Y="2.821587646484" />
                  <Point X="-27.999017578125" Y="2.826503662109" />
                  <Point X="-27.980466796875" Y="2.835651611328" />
                  <Point X="-27.9622109375" Y="2.84728125" />
                  <Point X="-27.946078125" Y="2.860229003906" />
                  <Point X="-27.889865234375" Y="2.916440673828" />
                  <Point X="-27.885353515625" Y="2.920952392578" />
                  <Point X="-27.872404296875" Y="2.937086425781" />
                  <Point X="-27.860775390625" Y="2.955341064453" />
                  <Point X="-27.85162890625" Y="2.973889892578" />
                  <Point X="-27.8467109375" Y="2.993978515625" />
                  <Point X="-27.843884765625" Y="3.015437744141" />
                  <Point X="-27.84343359375" Y="3.036123291016" />
                  <Point X="-27.85036328125" Y="3.115316162109" />
                  <Point X="-27.85091796875" Y="3.121660644531" />
                  <Point X="-27.854951171875" Y="3.141937255859" />
                  <Point X="-27.8614609375" Y="3.162590820312" />
                  <Point X="-27.869794921875" Y="3.181533691406" />
                  <Point X="-27.897818359375" Y="3.230070800781" />
                  <Point X="-28.18333203125" Y="3.724595214844" />
                  <Point X="-27.73614453125" Y="4.067448486328" />
                  <Point X="-27.70061328125" Y="4.094691650391" />
                  <Point X="-27.16703515625" Y="4.391133789062" />
                  <Point X="-27.0431953125" Y="4.229741210938" />
                  <Point X="-27.02888671875" Y="4.214795898438" />
                  <Point X="-27.012302734375" Y="4.200881835938" />
                  <Point X="-26.995109375" Y="4.189393554688" />
                  <Point X="-26.90696875" Y="4.143509765625" />
                  <Point X="-26.899892578125" Y="4.139826660156" />
                  <Point X="-26.880609375" Y="4.132328613281" />
                  <Point X="-26.859705078125" Y="4.126728027344" />
                  <Point X="-26.83926171875" Y="4.12358203125" />
                  <Point X="-26.818623046875" Y="4.124935058594" />
                  <Point X="-26.797310546875" Y="4.128693359375" />
                  <Point X="-26.777453125" Y="4.134481445312" />
                  <Point X="-26.6856484375" Y="4.172508789063" />
                  <Point X="-26.678279296875" Y="4.175561035156" />
                  <Point X="-26.660146484375" Y="4.185509277344" />
                  <Point X="-26.642416015625" Y="4.197923828125" />
                  <Point X="-26.62686328125" Y="4.2115625" />
                  <Point X="-26.6146328125" Y="4.228244140625" />
                  <Point X="-26.603810546875" Y="4.246988769531" />
                  <Point X="-26.595478515625" Y="4.265921386719" />
                  <Point X="-26.56559765625" Y="4.360690917969" />
                  <Point X="-26.56319921875" Y="4.368297851562" />
                  <Point X="-26.5591640625" Y="4.388583984375" />
                  <Point X="-26.55727734375" Y="4.410145019531" />
                  <Point X="-26.55773046875" Y="4.43083203125" />
                  <Point X="-26.56091796875" Y="4.455031738281" />
                  <Point X="-26.584203125" Y="4.631897460938" />
                  <Point X="-25.99562109375" Y="4.796915527344" />
                  <Point X="-25.94963671875" Y="4.809808105469" />
                  <Point X="-25.2947109375" Y="4.886457519531" />
                  <Point X="-25.133904296875" Y="4.286315429688" />
                  <Point X="-25.12112890625" Y="4.258121582031" />
                  <Point X="-25.10326953125" Y="4.231395019531" />
                  <Point X="-25.08211328125" Y="4.208807617188" />
                  <Point X="-25.054818359375" Y="4.19421875" />
                  <Point X="-25.024380859375" Y="4.183886230469" />
                  <Point X="-24.99384375" Y="4.178844238281" />
                  <Point X="-24.963306640625" Y="4.183886230469" />
                  <Point X="-24.932869140625" Y="4.19421875" />
                  <Point X="-24.90557421875" Y="4.208807617188" />
                  <Point X="-24.88441796875" Y="4.231395019531" />
                  <Point X="-24.86655859375" Y="4.258121582031" />
                  <Point X="-24.85378515625" Y="4.286314453125" />
                  <Point X="-24.83942578125" Y="4.339901855469" />
                  <Point X="-24.692580078125" Y="4.8879375" />
                  <Point X="-24.196119140625" Y="4.835944824219" />
                  <Point X="-24.15595703125" Y="4.831738769531" />
                  <Point X="-23.5597421875" Y="4.687793945312" />
                  <Point X="-23.51897265625" Y="4.677950683594" />
                  <Point X="-23.131001953125" Y="4.537231445312" />
                  <Point X="-23.10535546875" Y="4.527929199219" />
                  <Point X="-22.730099609375" Y="4.352434570312" />
                  <Point X="-22.705423828125" Y="4.34089453125" />
                  <Point X="-22.342890625" Y="4.129682128906" />
                  <Point X="-22.319021484375" Y="4.115776367188" />
                  <Point X="-22.05673828125" Y="3.929254394531" />
                  <Point X="-22.852416015625" Y="2.551098144531" />
                  <Point X="-22.85791796875" Y="2.539941894531" />
                  <Point X="-22.866921875" Y="2.516060546875" />
                  <Point X="-22.886796875" Y="2.441739746094" />
                  <Point X="-22.8893671875" Y="2.428324951172" />
                  <Point X="-22.89230078125" Y="2.403453125" />
                  <Point X="-22.892271484375" Y="2.380951416016" />
                  <Point X="-22.884521484375" Y="2.316685058594" />
                  <Point X="-22.883900390625" Y="2.311535888672" />
                  <Point X="-22.8785625" Y="2.289626464844" />
                  <Point X="-22.870296875" Y="2.267529296875" />
                  <Point X="-22.8599296875" Y="2.247471679688" />
                  <Point X="-22.8201640625" Y="2.188866943359" />
                  <Point X="-22.811599609375" Y="2.178033447266" />
                  <Point X="-22.79510546875" Y="2.160029785156" />
                  <Point X="-22.778400390625" Y="2.145593505859" />
                  <Point X="-22.719796875" Y="2.105827636719" />
                  <Point X="-22.715076171875" Y="2.102625244141" />
                  <Point X="-22.69503515625" Y="2.092266601563" />
                  <Point X="-22.672953125" Y="2.084004394531" />
                  <Point X="-22.65103515625" Y="2.078663574219" />
                  <Point X="-22.58676953125" Y="2.0709140625" />
                  <Point X="-22.57257421875" Y="2.070272705078" />
                  <Point X="-22.548515625" Y="2.070987548828" />
                  <Point X="-22.526794921875" Y="2.074170410156" />
                  <Point X="-22.452474609375" Y="2.094044921875" />
                  <Point X="-22.444408203125" Y="2.096591796875" />
                  <Point X="-22.41146484375" Y="2.110145019531" />
                  <Point X="-22.300576171875" Y="2.174166748047" />
                  <Point X="-21.032673828125" Y="2.906190917969" />
                  <Point X="-20.89088671875" Y="2.709139404297" />
                  <Point X="-20.876724609375" Y="2.689458496094" />
                  <Point X="-20.73780078125" Y="2.459883789063" />
                  <Point X="-21.76921484375" Y="1.668451416016" />
                  <Point X="-21.778578125" Y="1.66023840332" />
                  <Point X="-21.79602734375" Y="1.641625488281" />
                  <Point X="-21.849515625" Y="1.571845214844" />
                  <Point X="-21.85679296875" Y="1.56084375" />
                  <Point X="-21.8695546875" Y="1.538295410156" />
                  <Point X="-21.8783671875" Y="1.51708996582" />
                  <Point X="-21.89829296875" Y="1.445844360352" />
                  <Point X="-21.899892578125" Y="1.440125610352" />
                  <Point X="-21.90334765625" Y="1.417826660156" />
                  <Point X="-21.9041640625" Y="1.394256591797" />
                  <Point X="-21.902259765625" Y="1.371769165039" />
                  <Point X="-21.88590234375" Y="1.292499389648" />
                  <Point X="-21.882291015625" Y="1.279642333984" />
                  <Point X="-21.87378125" Y="1.255902099609" />
                  <Point X="-21.863716796875" Y="1.235743530273" />
                  <Point X="-21.81923046875" Y="1.168125610352" />
                  <Point X="-21.81566015625" Y="1.162698242188" />
                  <Point X="-21.80111328125" Y="1.145458862305" />
                  <Point X="-21.78387109375" Y="1.129367797852" />
                  <Point X="-21.765654296875" Y="1.116035888672" />
                  <Point X="-21.701185546875" Y="1.079746337891" />
                  <Point X="-21.6887109375" Y="1.073872802734" />
                  <Point X="-21.665556640625" Y="1.064960449219" />
                  <Point X="-21.64387890625" Y="1.059438476562" />
                  <Point X="-21.55671484375" Y="1.047918457031" />
                  <Point X="-21.548654296875" Y="1.047200805664" />
                  <Point X="-21.52858203125" Y="1.046273071289" />
                  <Point X="-21.511794921875" Y="1.04698449707" />
                  <Point X="-21.406458984375" Y="1.060852416992" />
                  <Point X="-20.223158203125" Y="1.21663671875" />
                  <Point X="-20.16014453125" Y="0.957789306641" />
                  <Point X="-20.1540625" Y="0.93280645752" />
                  <Point X="-20.109134765625" Y="0.64423828125" />
                  <Point X="-21.283419921875" Y="0.32958972168" />
                  <Point X="-21.295212890625" Y="0.325584747314" />
                  <Point X="-21.318455078125" Y="0.315066894531" />
                  <Point X="-21.404091796875" Y="0.265567749023" />
                  <Point X="-21.414689453125" Y="0.258451965332" />
                  <Point X="-21.435869140625" Y="0.242063156128" />
                  <Point X="-21.45246484375" Y="0.225579483032" />
                  <Point X="-21.503845703125" Y="0.160106994629" />
                  <Point X="-21.507970703125" Y="0.154851699829" />
                  <Point X="-21.519689453125" Y="0.135581161499" />
                  <Point X="-21.529466796875" Y="0.114115577698" />
                  <Point X="-21.536318359375" Y="0.092605209351" />
                  <Point X="-21.5534453125" Y="0.003173215866" />
                  <Point X="-21.5550078125" Y="-0.009676596642" />
                  <Point X="-21.5563828125" Y="-0.035667507172" />
                  <Point X="-21.5548203125" Y="-0.058554763794" />
                  <Point X="-21.537693359375" Y="-0.147986907959" />
                  <Point X="-21.536318359375" Y="-0.155165374756" />
                  <Point X="-21.529466796875" Y="-0.176675735474" />
                  <Point X="-21.519689453125" Y="-0.198141326904" />
                  <Point X="-21.507970703125" Y="-0.217411865234" />
                  <Point X="-21.45658984375" Y="-0.282884216309" />
                  <Point X="-21.4476015625" Y="-0.292809936523" />
                  <Point X="-21.429169921875" Y="-0.310480926514" />
                  <Point X="-21.41096484375" Y="-0.32415447998" />
                  <Point X="-21.325328125" Y="-0.373653778076" />
                  <Point X="-21.318466796875" Y="-0.377254394531" />
                  <Point X="-21.29951171875" Y="-0.386236450195" />
                  <Point X="-21.283419921875" Y="-0.392150054932" />
                  <Point X="-21.1868203125" Y="-0.418033630371" />
                  <Point X="-20.108525390625" Y="-0.706961914062" />
                  <Point X="-20.141580078125" Y="-0.926204406738" />
                  <Point X="-20.144974609375" Y="-0.948726074219" />
                  <Point X="-20.19882421875" Y="-1.18469921875" />
                  <Point X="-21.5756171875" Y="-1.003440795898" />
                  <Point X="-21.59196484375" Y="-1.002710205078" />
                  <Point X="-21.62533984375" Y="-1.005508789063" />
                  <Point X="-21.7934140625" Y="-1.042040283203" />
                  <Point X="-21.806904296875" Y="-1.04497265625" />
                  <Point X="-21.836021484375" Y="-1.056595947266" />
                  <Point X="-21.863849609375" Y="-1.07348828125" />
                  <Point X="-21.887603515625" Y="-1.093960205078" />
                  <Point X="-21.989193359375" Y="-1.216141235352" />
                  <Point X="-21.99734765625" Y="-1.225948242188" />
                  <Point X="-22.0120703125" Y="-1.250337768555" />
                  <Point X="-22.023412109375" Y="-1.277725952148" />
                  <Point X="-22.030240234375" Y="-1.305368164062" />
                  <Point X="-22.04480078125" Y="-1.46359777832" />
                  <Point X="-22.04596875" Y="-1.476298461914" />
                  <Point X="-22.043650390625" Y="-1.507570068359" />
                  <Point X="-22.03591796875" Y="-1.539190429688" />
                  <Point X="-22.023546875" Y="-1.567998657227" />
                  <Point X="-21.930533203125" Y="-1.712676269531" />
                  <Point X="-21.92526171875" Y="-1.720072631836" />
                  <Point X="-21.90617578125" Y="-1.744311767578" />
                  <Point X="-21.889369140625" Y="-1.760909179688" />
                  <Point X="-21.7997265625" Y="-1.8296953125" />
                  <Point X="-20.786875" Y="-2.606882324219" />
                  <Point X="-20.865615234375" Y="-2.734295166016" />
                  <Point X="-20.87517578125" Y="-2.749763427734" />
                  <Point X="-20.97101953125" Y="-2.885944580078" />
                  <Point X="-22.199044921875" Y="-2.176942871094" />
                  <Point X="-22.21387109375" Y="-2.170011474609" />
                  <Point X="-22.245775390625" Y="-2.159825195312" />
                  <Point X="-22.44580859375" Y="-2.12369921875" />
                  <Point X="-22.461865234375" Y="-2.120799560547" />
                  <Point X="-22.493220703125" Y="-2.120395751953" />
                  <Point X="-22.525392578125" Y="-2.125354003906" />
                  <Point X="-22.555166015625" Y="-2.135177246094" />
                  <Point X="-22.721345703125" Y="-2.222636230469" />
                  <Point X="-22.73468359375" Y="-2.22965625" />
                  <Point X="-22.75761328125" Y="-2.246547363281" />
                  <Point X="-22.7785703125" Y="-2.267503173828" />
                  <Point X="-22.79546484375" Y="-2.290435791016" />
                  <Point X="-22.882923828125" Y="-2.456614990234" />
                  <Point X="-22.8899453125" Y="-2.469953857422" />
                  <Point X="-22.899767578125" Y="-2.499728515625" />
                  <Point X="-22.9047265625" Y="-2.531906738281" />
                  <Point X="-22.90432421875" Y="-2.563260742188" />
                  <Point X="-22.868197265625" Y="-2.763294921875" />
                  <Point X="-22.86633984375" Y="-2.771485351562" />
                  <Point X="-22.857537109375" Y="-2.803653320313" />
                  <Point X="-22.848177734375" Y="-2.826078613281" />
                  <Point X="-22.790572265625" Y="-2.925854248047" />
                  <Point X="-22.138712890625" Y="-4.054905273438" />
                  <Point X="-22.20678515625" Y="-4.103526855469" />
                  <Point X="-22.218146484375" Y="-4.111641601562" />
                  <Point X="-22.298234375" Y="-4.16348046875" />
                  <Point X="-23.241451171875" Y="-2.934256347656" />
                  <Point X="-23.25249609375" Y="-2.922178222656" />
                  <Point X="-23.27807421875" Y="-2.900557128906" />
                  <Point X="-23.47536328125" Y="-2.773719238281" />
                  <Point X="-23.49119921875" Y="-2.763538330078" />
                  <Point X="-23.52001171875" Y="-2.751166748047" />
                  <Point X="-23.5516328125" Y="-2.743435302734" />
                  <Point X="-23.582900390625" Y="-2.741116699219" />
                  <Point X="-23.79866796875" Y="-2.760971923828" />
                  <Point X="-23.815986328125" Y="-2.762565673828" />
                  <Point X="-23.84363671875" Y="-2.769397949219" />
                  <Point X="-23.871021484375" Y="-2.780741455078" />
                  <Point X="-23.89540234375" Y="-2.795461425781" />
                  <Point X="-24.062013671875" Y="-2.933992431641" />
                  <Point X="-24.07538671875" Y="-2.945111816406" />
                  <Point X="-24.09585546875" Y="-2.968856689453" />
                  <Point X="-24.112748046875" Y="-2.996679443359" />
                  <Point X="-24.124375" Y="-3.025805664062" />
                  <Point X="-24.174189453125" Y="-3.254996826172" />
                  <Point X="-24.175533203125" Y="-3.2626875" />
                  <Point X="-24.18024609375" Y="-3.298233886719" />
                  <Point X="-24.1802578125" Y="-3.32312109375" />
                  <Point X="-24.163931640625" Y="-3.44712109375" />
                  <Point X="-23.977935546875" Y="-4.859915527344" />
                  <Point X="-24.013576171875" Y="-4.867728027344" />
                  <Point X="-24.024326171875" Y="-4.870084960938" />
                  <Point X="-24.070681640625" Y="-4.878505859375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-26.043244140625" Y="-4.755583984375" />
                  <Point X="-26.058423828125" Y="-4.752637207031" />
                  <Point X="-26.141244140625" Y="-4.731328613281" />
                  <Point X="-26.1207734375" Y="-4.575838378906" />
                  <Point X="-26.120076171875" Y="-4.568098632812" />
                  <Point X="-26.11944921875" Y="-4.541030273438" />
                  <Point X="-26.1217578125" Y="-4.509323242188" />
                  <Point X="-26.123333984375" Y="-4.497687988281" />
                  <Point X="-26.179365234375" Y="-4.216006347656" />
                  <Point X="-26.18386328125" Y="-4.193396484375" />
                  <Point X="-26.188125" Y="-4.178467773438" />
                  <Point X="-26.19902734375" Y="-4.1495" />
                  <Point X="-26.20566796875" Y="-4.1354609375" />
                  <Point X="-26.221740234375" Y="-4.107624511719" />
                  <Point X="-26.230576171875" Y="-4.094859375" />
                  <Point X="-26.250208984375" Y="-4.070936523438" />
                  <Point X="-26.261005859375" Y="-4.059779052734" />
                  <Point X="-26.47693359375" Y="-3.870414550781" />
                  <Point X="-26.494265625" Y="-3.85521484375" />
                  <Point X="-26.50673828125" Y="-3.845965332031" />
                  <Point X="-26.533021484375" Y="-3.829621337891" />
                  <Point X="-26.54683203125" Y="-3.822526855469" />
                  <Point X="-26.576529296875" Y="-3.810225830078" />
                  <Point X="-26.5913125" Y="-3.805475830078" />
                  <Point X="-26.621455078125" Y="-3.798447753906" />
                  <Point X="-26.636814453125" Y="-3.796169677734" />
                  <Point X="-26.923400390625" Y="-3.777385742188" />
                  <Point X="-26.946404296875" Y="-3.775878173828" />
                  <Point X="-26.9619296875" Y="-3.776132324219" />
                  <Point X="-26.992732421875" Y="-3.779166503906" />
                  <Point X="-27.008009765625" Y="-3.781946533203" />
                  <Point X="-27.039060546875" Y="-3.790267333984" />
                  <Point X="-27.0536796875" Y="-3.795498291016" />
                  <Point X="-27.081869140625" Y="-3.808271240234" />
                  <Point X="-27.095439453125" Y="-3.815813232422" />
                  <Point X="-27.33423828125" Y="-3.975373535156" />
                  <Point X="-27.353404296875" Y="-3.988181152344" />
                  <Point X="-27.35968359375" Y="-3.992758789062" />
                  <Point X="-27.380451171875" Y="-4.010140136719" />
                  <Point X="-27.402759765625" Y="-4.032774658203" />
                  <Point X="-27.410466796875" Y="-4.041627197266" />
                  <Point X="-27.427177734375" Y="-4.063404541016" />
                  <Point X="-27.50319921875" Y="-4.162479492188" />
                  <Point X="-27.725328125" Y="-4.024944091797" />
                  <Point X="-27.747591796875" Y="-4.011158935547" />
                  <Point X="-27.98086328125" Y="-3.831547607422" />
                  <Point X="-27.34148828125" Y="-2.724119384766" />
                  <Point X="-27.33484765625" Y="-2.710079833984" />
                  <Point X="-27.323947265625" Y="-2.681114990234" />
                  <Point X="-27.319685546875" Y="-2.666189453125" />
                  <Point X="-27.3134140625" Y="-2.634663085938" />
                  <Point X="-27.311638671875" Y="-2.619239501953" />
                  <Point X="-27.310625" Y="-2.588305419922" />
                  <Point X="-27.3146640625" Y="-2.557617675781" />
                  <Point X="-27.3236484375" Y="-2.527999511719" />
                  <Point X="-27.32935546875" Y="-2.51355859375" />
                  <Point X="-27.343572265625" Y="-2.484729736328" />
                  <Point X="-27.351552734375" Y="-2.471413574219" />
                  <Point X="-27.369578125" Y="-2.4462578125" />
                  <Point X="-27.379623046875" Y="-2.434418212891" />
                  <Point X="-27.395681640625" Y="-2.418358642578" />
                  <Point X="-27.408814453125" Y="-2.407021240234" />
                  <Point X="-27.4339765625" Y="-2.388990966797" />
                  <Point X="-27.447294921875" Y="-2.381009033203" />
                  <Point X="-27.476125" Y="-2.366791992188" />
                  <Point X="-27.490564453125" Y="-2.3610859375" />
                  <Point X="-27.520181640625" Y="-2.352101806641" />
                  <Point X="-27.5508671875" Y="-2.348062011719" />
                  <Point X="-27.581798828125" Y="-2.349074951172" />
                  <Point X="-27.59722265625" Y="-2.350849609375" />
                  <Point X="-27.628748046875" Y="-2.357120605469" />
                  <Point X="-27.643677734375" Y="-2.361383789062" />
                  <Point X="-27.67264453125" Y="-2.372285888672" />
                  <Point X="-27.686681640625" Y="-2.378924804688" />
                  <Point X="-27.78248828125" Y="-2.434238769531" />
                  <Point X="-28.7930859375" Y="-3.017707763672" />
                  <Point X="-28.986431640625" Y="-2.763692871094" />
                  <Point X="-29.004025390625" Y="-2.740578613281" />
                  <Point X="-29.181265625" Y="-2.443372314453" />
                  <Point X="-28.048123046875" Y="-1.573881591797" />
                  <Point X="-28.036484375" Y="-1.563311401367" />
                  <Point X="-28.015107421875" Y="-1.540393066406" />
                  <Point X="-28.005369140625" Y="-1.528044555664" />
                  <Point X="-27.987404296875" Y="-1.500914428711" />
                  <Point X="-27.979837890625" Y="-1.48712890625" />
                  <Point X="-27.96708203125" Y="-1.458500610352" />
                  <Point X="-27.961892578125" Y="-1.44366027832" />
                  <Point X="-27.95476171875" Y="-1.416134521484" />
                  <Point X="-27.951552734375" Y="-1.398814575195" />
                  <Point X="-27.94874609375" Y="-1.368365600586" />
                  <Point X="-27.948576171875" Y="-1.353029663086" />
                  <Point X="-27.950787109375" Y="-1.321360839844" />
                  <Point X="-27.953083984375" Y="-1.306207641602" />
                  <Point X="-27.9600859375" Y="-1.276464599609" />
                  <Point X="-27.971783203125" Y="-1.248229736328" />
                  <Point X="-27.9878671875" Y="-1.222248413086" />
                  <Point X="-27.996958984375" Y="-1.209912353516" />
                  <Point X="-28.017791015625" Y="-1.185957885742" />
                  <Point X="-28.02874609375" Y="-1.175242431641" />
                  <Point X="-28.052244140625" Y="-1.155709716797" />
                  <Point X="-28.064787109375" Y="-1.146892456055" />
                  <Point X="-28.089302734375" Y="-1.132464233398" />
                  <Point X="-28.105419921875" Y="-1.12448828125" />
                  <Point X="-28.134669921875" Y="-1.113267578125" />
                  <Point X="-28.149759765625" Y="-1.10887097168" />
                  <Point X="-28.18165625" Y="-1.102383300781" />
                  <Point X="-28.197279296875" Y="-1.100534912109" />
                  <Point X="-28.228615234375" Y="-1.09944152832" />
                  <Point X="-28.244328125" Y="-1.100196533203" />
                  <Point X="-28.365275390625" Y="-1.116119506836" />
                  <Point X="-29.66091796875" Y="-1.286694335938" />
                  <Point X="-29.7338828125" Y="-1.001046691895" />
                  <Point X="-29.7407578125" Y="-0.974121032715" />
                  <Point X="-29.78644921875" Y="-0.654654052734" />
                  <Point X="-28.508287109375" Y="-0.312171234131" />
                  <Point X="-28.5004765625" Y="-0.309713012695" />
                  <Point X="-28.473935546875" Y="-0.299253814697" />
                  <Point X="-28.444171875" Y="-0.283897064209" />
                  <Point X="-28.43356640625" Y="-0.277517578125" />
                  <Point X="-28.40790625" Y="-0.259708709717" />
                  <Point X="-28.3936875" Y="-0.248256408691" />
                  <Point X="-28.3712265625" Y="-0.226370040894" />
                  <Point X="-28.36090625" Y="-0.214494934082" />
                  <Point X="-28.34166015625" Y="-0.188239654541" />
                  <Point X="-28.33344140625" Y="-0.174823913574" />
                  <Point X="-28.319328125" Y="-0.146815353394" />
                  <Point X="-28.31343359375" Y="-0.132222366333" />
                  <Point X="-28.30487109375" Y="-0.104630233765" />
                  <Point X="-28.30098828125" Y="-0.088495727539" />
                  <Point X="-28.296720703125" Y="-0.06033972168" />
                  <Point X="-28.2956484375" Y="-0.046103286743" />
                  <Point X="-28.2956484375" Y="-0.016456888199" />
                  <Point X="-28.296720703125" Y="-0.00222059989" />
                  <Point X="-28.30098828125" Y="0.025935705185" />
                  <Point X="-28.30418359375" Y="0.039855426788" />
                  <Point X="-28.31274609375" Y="0.067447563171" />
                  <Point X="-28.319328125" Y="0.084255317688" />
                  <Point X="-28.33344140625" Y="0.112263893127" />
                  <Point X="-28.34166015625" Y="0.125679786682" />
                  <Point X="-28.36090625" Y="0.151934906006" />
                  <Point X="-28.371224609375" Y="0.163807495117" />
                  <Point X="-28.393677734375" Y="0.185687759399" />
                  <Point X="-28.4058125" Y="0.195695281982" />
                  <Point X="-28.43150390625" Y="0.213526153564" />
                  <Point X="-28.43752734375" Y="0.217379211426" />
                  <Point X="-28.460197265625" Y="0.229882553101" />
                  <Point X="-28.495083984375" Y="0.245008346558" />
                  <Point X="-28.508287109375" Y="0.249611190796" />
                  <Point X="-28.61853515625" Y="0.279152069092" />
                  <Point X="-29.7854453125" Y="0.591824890137" />
                  <Point X="-29.73576171875" Y="0.927587585449" />
                  <Point X="-29.73133203125" Y="0.957521911621" />
                  <Point X="-29.6335859375" Y="1.318237060547" />
                  <Point X="-28.778064453125" Y="1.20560546875" />
                  <Point X="-28.767736328125" Y="1.204815429688" />
                  <Point X="-28.747056640625" Y="1.204364379883" />
                  <Point X="-28.736705078125" Y="1.20470324707" />
                  <Point X="-28.71514453125" Y="1.206589355469" />
                  <Point X="-28.704890625" Y="1.208053222656" />
                  <Point X="-28.684603515625" Y="1.212088500977" />
                  <Point X="-28.6745703125" Y="1.21466027832" />
                  <Point X="-28.617708984375" Y="1.232588623047" />
                  <Point X="-28.603453125" Y="1.237675170898" />
                  <Point X="-28.58451953125" Y="1.246005737305" />
                  <Point X="-28.57527734375" Y="1.250688598633" />
                  <Point X="-28.556533203125" Y="1.261510375977" />
                  <Point X="-28.547857421875" Y="1.267171508789" />
                  <Point X="-28.53117578125" Y="1.279403320312" />
                  <Point X="-28.51592578125" Y="1.293378662109" />
                  <Point X="-28.502287109375" Y="1.308931274414" />
                  <Point X="-28.495892578125" Y="1.317079223633" />
                  <Point X="-28.483478515625" Y="1.334809082031" />
                  <Point X="-28.4780078125" Y="1.343606079102" />
                  <Point X="-28.468060546875" Y="1.361738891602" />
                  <Point X="-28.463583984375" Y="1.371074707031" />
                  <Point X="-28.440767578125" Y="1.426157470703" />
                  <Point X="-28.4354921875" Y="1.440369873047" />
                  <Point X="-28.429701171875" Y="1.460243774414" />
                  <Point X="-28.427349609375" Y="1.470338623047" />
                  <Point X="-28.423595703125" Y="1.491647949219" />
                  <Point X="-28.422357421875" Y="1.501929931641" />
                  <Point X="-28.4210078125" Y="1.522566040039" />
                  <Point X="-28.42191015625" Y="1.543220214844" />
                  <Point X="-28.425056640625" Y="1.563659423828" />
                  <Point X="-28.427189453125" Y="1.573798828125" />
                  <Point X="-28.432791015625" Y="1.594698730469" />
                  <Point X="-28.436015625" Y="1.604544067383" />
                  <Point X="-28.44351171875" Y="1.623815429688" />
                  <Point X="-28.447783203125" Y="1.633241333008" />
                  <Point X="-28.4753125" Y="1.686126342773" />
                  <Point X="-28.48283203125" Y="1.699342163086" />
                  <Point X="-28.494318359375" Y="1.716523803711" />
                  <Point X="-28.500529296875" Y="1.724800415039" />
                  <Point X="-28.5144375" Y="1.74137097168" />
                  <Point X="-28.521513671875" Y="1.74892590332" />
                  <Point X="-28.536447265625" Y="1.763219726562" />
                  <Point X="-28.5443046875" Y="1.769958618164" />
                  <Point X="-28.60754296875" Y="1.818483398438" />
                  <Point X="-29.22761328125" Y="2.294280517578" />
                  <Point X="-29.01950390625" Y="2.650821289063" />
                  <Point X="-29.00228515625" Y="2.680321533203" />
                  <Point X="-28.726337890625" Y="3.035013183594" />
                  <Point X="-28.25415625" Y="2.7623984375" />
                  <Point X="-28.24491796875" Y="2.757716552734" />
                  <Point X="-28.225986328125" Y="2.749386230469" />
                  <Point X="-28.21629296875" Y="2.745737792969" />
                  <Point X="-28.19565234375" Y="2.739229492188" />
                  <Point X="-28.1856171875" Y="2.736657470703" />
                  <Point X="-28.165328125" Y="2.732621826172" />
                  <Point X="-28.15507421875" Y="2.731158203125" />
                  <Point X="-28.075880859375" Y="2.724229736328" />
                  <Point X="-28.05917578125" Y="2.723334472656" />
                  <Point X="-28.038498046875" Y="2.723785400391" />
                  <Point X="-28.028169921875" Y="2.724575439453" />
                  <Point X="-28.0067109375" Y="2.727400146484" />
                  <Point X="-27.99653125" Y="2.729309814453" />
                  <Point X="-27.976439453125" Y="2.734225830078" />
                  <Point X="-27.957001953125" Y="2.741300292969" />
                  <Point X="-27.938451171875" Y="2.750448242188" />
                  <Point X="-27.92942578125" Y="2.755528076172" />
                  <Point X="-27.911169921875" Y="2.767157714844" />
                  <Point X="-27.902748046875" Y="2.773191894531" />
                  <Point X="-27.886615234375" Y="2.786139648438" />
                  <Point X="-27.878904296875" Y="2.793053222656" />
                  <Point X="-27.82269140625" Y="2.849264892578" />
                  <Point X="-27.811265625" Y="2.861488769531" />
                  <Point X="-27.79831640625" Y="2.877622802734" />
                  <Point X="-27.79228125" Y="2.886044677734" />
                  <Point X="-27.78065234375" Y="2.904299316406" />
                  <Point X="-27.7755703125" Y="2.913326660156" />
                  <Point X="-27.766423828125" Y="2.931875488281" />
                  <Point X="-27.759353515625" Y="2.951299804688" />
                  <Point X="-27.754435546875" Y="2.971388427734" />
                  <Point X="-27.7525234375" Y="2.98157421875" />
                  <Point X="-27.749697265625" Y="3.003033447266" />
                  <Point X="-27.748908203125" Y="3.013366210938" />
                  <Point X="-27.74845703125" Y="3.034051757812" />
                  <Point X="-27.748794921875" Y="3.044404541016" />
                  <Point X="-27.755724609375" Y="3.123597412109" />
                  <Point X="-27.757744140625" Y="3.140193847656" />
                  <Point X="-27.76177734375" Y="3.160470458984" />
                  <Point X="-27.764345703125" Y="3.170495117188" />
                  <Point X="-27.77085546875" Y="3.191148681641" />
                  <Point X="-27.77450390625" Y="3.20084765625" />
                  <Point X="-27.782837890625" Y="3.219790527344" />
                  <Point X="-27.7875234375" Y="3.229034423828" />
                  <Point X="-27.815546875" Y="3.277571533203" />
                  <Point X="-28.05938671875" Y="3.699914306641" />
                  <Point X="-27.678341796875" Y="3.992056640625" />
                  <Point X="-27.648357421875" Y="4.015047607422" />
                  <Point X="-27.1925234375" Y="4.268295898437" />
                  <Point X="-27.118564453125" Y="4.171909179688" />
                  <Point X="-27.11181640625" Y="4.164043945312" />
                  <Point X="-27.0975078125" Y="4.149098632812" />
                  <Point X="-27.089947265625" Y="4.142018554687" />
                  <Point X="-27.07336328125" Y="4.128104492188" />
                  <Point X="-27.06508203125" Y="4.121892089844" />
                  <Point X="-27.047888671875" Y="4.110403808594" />
                  <Point X="-27.0389765625" Y="4.105127929688" />
                  <Point X="-26.9508359375" Y="4.059243896484" />
                  <Point X="-26.9343203125" Y="4.051284667969" />
                  <Point X="-26.915037109375" Y="4.043786621094" />
                  <Point X="-26.905193359375" Y="4.040564941406" />
                  <Point X="-26.8842890625" Y="4.034964355469" />
                  <Point X="-26.874154296875" Y="4.032833251953" />
                  <Point X="-26.8537109375" Y="4.029687255859" />
                  <Point X="-26.833046875" Y="4.028785644531" />
                  <Point X="-26.812408203125" Y="4.030138671875" />
                  <Point X="-26.802125" Y="4.031378662109" />
                  <Point X="-26.7808125" Y="4.035136962891" />
                  <Point X="-26.7707265625" Y="4.037488769531" />
                  <Point X="-26.750869140625" Y="4.043276855469" />
                  <Point X="-26.74109765625" Y="4.046713134766" />
                  <Point X="-26.64929296875" Y="4.084740478516" />
                  <Point X="-26.632583984375" Y="4.092272460938" />
                  <Point X="-26.614451171875" Y="4.102220703125" />
                  <Point X="-26.605658203125" Y="4.107688964844" />
                  <Point X="-26.587927734375" Y="4.120103515625" />
                  <Point X="-26.579779296875" Y="4.126497558594" />
                  <Point X="-26.5642265625" Y="4.140136230469" />
                  <Point X="-26.550248046875" Y="4.155391113281" />
                  <Point X="-26.538017578125" Y="4.172072753906" />
                  <Point X="-26.532361328125" Y="4.180744140625" />
                  <Point X="-26.5215390625" Y="4.199488769531" />
                  <Point X="-26.516859375" Y="4.208722167969" />
                  <Point X="-26.50852734375" Y="4.227654785156" />
                  <Point X="-26.504875" Y="4.237354003906" />
                  <Point X="-26.474994140625" Y="4.332123535156" />
                  <Point X="-26.470025390625" Y="4.349764160156" />
                  <Point X="-26.465990234375" Y="4.370050292969" />
                  <Point X="-26.464525390625" Y="4.380302734375" />
                  <Point X="-26.462638671875" Y="4.401863769531" />
                  <Point X="-26.46230078125" Y="4.412225585938" />
                  <Point X="-26.46275390625" Y="4.432912597656" />
                  <Point X="-26.463544921875" Y="4.443237792969" />
                  <Point X="-26.466732421875" Y="4.4674375" />
                  <Point X="-26.479267578125" Y="4.562654785156" />
                  <Point X="-25.969974609375" Y="4.705442871094" />
                  <Point X="-25.9311796875" Y="4.716319824219" />
                  <Point X="-25.36522265625" Y="4.782556640625" />
                  <Point X="-25.22566796875" Y="4.261727539062" />
                  <Point X="-25.220435546875" Y="4.247105957031" />
                  <Point X="-25.20766015625" Y="4.218912109375" />
                  <Point X="-25.2001171875" Y="4.20533984375" />
                  <Point X="-25.1822578125" Y="4.17861328125" />
                  <Point X="-25.17260546875" Y="4.166452148438" />
                  <Point X="-25.15144921875" Y="4.143864746094" />
                  <Point X="-25.12689453125" Y="4.125024414062" />
                  <Point X="-25.099599609375" Y="4.110435546875" />
                  <Point X="-25.08535546875" Y="4.104260742188" />
                  <Point X="-25.05491796875" Y="4.093928222656" />
                  <Point X="-25.039857421875" Y="4.090155273438" />
                  <Point X="-25.0093203125" Y="4.08511328125" />
                  <Point X="-24.9783671875" Y="4.08511328125" />
                  <Point X="-24.947830078125" Y="4.090155273438" />
                  <Point X="-24.93276953125" Y="4.093928222656" />
                  <Point X="-24.90233203125" Y="4.104260742188" />
                  <Point X="-24.888087890625" Y="4.110435546875" />
                  <Point X="-24.86079296875" Y="4.125024414062" />
                  <Point X="-24.83623828125" Y="4.143864746094" />
                  <Point X="-24.81508203125" Y="4.166452148438" />
                  <Point X="-24.8054296875" Y="4.17861328125" />
                  <Point X="-24.7875703125" Y="4.20533984375" />
                  <Point X="-24.780025390625" Y="4.218916015625" />
                  <Point X="-24.767251953125" Y="4.247108886719" />
                  <Point X="-24.7620234375" Y="4.261725585938" />
                  <Point X="-24.7476640625" Y="4.315312988281" />
                  <Point X="-24.621810546875" Y="4.785006347656" />
                  <Point X="-24.206013671875" Y="4.741461425781" />
                  <Point X="-24.17212109375" Y="4.737912109375" />
                  <Point X="-23.582037109375" Y="4.595447265625" />
                  <Point X="-23.546404296875" Y="4.586844238281" />
                  <Point X="-23.16339453125" Y="4.447924316406" />
                  <Point X="-23.141740234375" Y="4.440069824219" />
                  <Point X="-22.77034375" Y="4.266380371094" />
                  <Point X="-22.749546875" Y="4.256654296875" />
                  <Point X="-22.390712890625" Y="4.047596923828" />
                  <Point X="-22.3705703125" Y="4.035861572266" />
                  <Point X="-22.182216796875" Y="3.901916015625" />
                  <Point X="-22.9346875" Y="2.598598144531" />
                  <Point X="-22.9376171875" Y="2.593117431641" />
                  <Point X="-22.946810546875" Y="2.573456542969" />
                  <Point X="-22.955814453125" Y="2.549575195312" />
                  <Point X="-22.958697265625" Y="2.540603271484" />
                  <Point X="-22.978572265625" Y="2.466282470703" />
                  <Point X="-22.980099609375" Y="2.459616943359" />
                  <Point X="-22.983712890625" Y="2.439452880859" />
                  <Point X="-22.986646484375" Y="2.414581054688" />
                  <Point X="-22.98730078125" Y="2.403329345703" />
                  <Point X="-22.987271484375" Y="2.380827636719" />
                  <Point X="-22.986587890625" Y="2.369577636719" />
                  <Point X="-22.978837890625" Y="2.305311279297" />
                  <Point X="-22.976201171875" Y="2.289048339844" />
                  <Point X="-22.97086328125" Y="2.267138916016" />
                  <Point X="-22.967541015625" Y="2.256343261719" />
                  <Point X="-22.959275390625" Y="2.23424609375" />
                  <Point X="-22.954689453125" Y="2.223908935547" />
                  <Point X="-22.944322265625" Y="2.203851318359" />
                  <Point X="-22.938541015625" Y="2.194130859375" />
                  <Point X="-22.898775390625" Y="2.135526123047" />
                  <Point X="-22.894689453125" Y="2.129951171875" />
                  <Point X="-22.881646484375" Y="2.113859130859" />
                  <Point X="-22.86515234375" Y="2.09585546875" />
                  <Point X="-22.85722265625" Y="2.088151123047" />
                  <Point X="-22.840517578125" Y="2.07371484375" />
                  <Point X="-22.8317421875" Y="2.066982910156" />
                  <Point X="-22.773138671875" Y="2.027217041016" />
                  <Point X="-22.758697265625" Y="2.018231811523" />
                  <Point X="-22.73865625" Y="2.007873168945" />
                  <Point X="-22.728326171875" Y="2.003290771484" />
                  <Point X="-22.706244140625" Y="1.995028564453" />
                  <Point X="-22.695443359375" Y="1.991705078125" />
                  <Point X="-22.673525390625" Y="1.986364257812" />
                  <Point X="-22.662408203125" Y="1.984346923828" />
                  <Point X="-22.598142578125" Y="1.976597167969" />
                  <Point X="-22.591056640625" Y="1.976010864258" />
                  <Point X="-22.569751953125" Y="1.975314575195" />
                  <Point X="-22.545693359375" Y="1.976029541016" />
                  <Point X="-22.5347421875" Y="1.976991333008" />
                  <Point X="-22.513021484375" Y="1.980174194336" />
                  <Point X="-22.502251953125" Y="1.982395263672" />
                  <Point X="-22.427931640625" Y="2.002269775391" />
                  <Point X="-22.408263671875" Y="2.008736450195" />
                  <Point X="-22.3753203125" Y="2.022289672852" />
                  <Point X="-22.36396484375" Y="2.027872680664" />
                  <Point X="-22.253076171875" Y="2.091894287109" />
                  <Point X="-21.059595703125" Y="2.780950927734" />
                  <Point X="-20.968" Y="2.653653564453" />
                  <Point X="-20.956041015625" Y="2.637034423828" />
                  <Point X="-20.863115234375" Y="2.483471679688" />
                  <Point X="-21.827046875" Y="1.743819946289" />
                  <Point X="-21.831859375" Y="1.739869995117" />
                  <Point X="-21.847884765625" Y="1.725212036133" />
                  <Point X="-21.865333984375" Y="1.706599121094" />
                  <Point X="-21.87142578125" Y="1.699419677734" />
                  <Point X="-21.9249140625" Y="1.629639404297" />
                  <Point X="-21.93946875" Y="1.607636474609" />
                  <Point X="-21.95223046875" Y="1.585088134766" />
                  <Point X="-21.95728125" Y="1.574752441406" />
                  <Point X="-21.96609375" Y="1.55354699707" />
                  <Point X="-21.96985546875" Y="1.542677490234" />
                  <Point X="-21.98978125" Y="1.471432006836" />
                  <Point X="-21.993771484375" Y="1.454671630859" />
                  <Point X="-21.9972265625" Y="1.432372680664" />
                  <Point X="-21.998291015625" Y="1.421115234375" />
                  <Point X="-21.999107421875" Y="1.397545166016" />
                  <Point X="-21.998826171875" Y="1.386240478516" />
                  <Point X="-21.996921875" Y="1.363753051758" />
                  <Point X="-21.995298828125" Y="1.3525703125" />
                  <Point X="-21.97894140625" Y="1.273300537109" />
                  <Point X="-21.97736328125" Y="1.266809814453" />
                  <Point X="-21.97171875" Y="1.247586425781" />
                  <Point X="-21.963208984375" Y="1.223846191406" />
                  <Point X="-21.95877734375" Y="1.213466796875" />
                  <Point X="-21.948712890625" Y="1.193308227539" />
                  <Point X="-21.943080078125" Y="1.183529174805" />
                  <Point X="-21.89859375" Y="1.115911376953" />
                  <Point X="-21.888265625" Y="1.101432617188" />
                  <Point X="-21.87371875" Y="1.084193237305" />
                  <Point X="-21.8659296875" Y="1.076005249023" />
                  <Point X="-21.8486875" Y="1.05991418457" />
                  <Point X="-21.8399765625" Y="1.052705200195" />
                  <Point X="-21.821759765625" Y="1.039373168945" />
                  <Point X="-21.81225390625" Y="1.033250366211" />
                  <Point X="-21.74778515625" Y="0.996960876465" />
                  <Point X="-21.741654296875" Y="0.993796813965" />
                  <Point X="-21.7228359375" Y="0.985213684082" />
                  <Point X="-21.699681640625" Y="0.976301391602" />
                  <Point X="-21.6890078125" Y="0.97290032959" />
                  <Point X="-21.667330078125" Y="0.967378234863" />
                  <Point X="-21.656326171875" Y="0.965257385254" />
                  <Point X="-21.569162109375" Y="0.953737487793" />
                  <Point X="-21.553041015625" Y="0.952302062988" />
                  <Point X="-21.53296875" Y="0.951374389648" />
                  <Point X="-21.524560546875" Y="0.951358337402" />
                  <Point X="-21.49939453125" Y="0.952797302246" />
                  <Point X="-21.39405859375" Y="0.966665161133" />
                  <Point X="-20.295294921875" Y="1.111319946289" />
                  <Point X="-20.25244921875" Y="0.935318786621" />
                  <Point X="-20.247314453125" Y="0.914225280762" />
                  <Point X="-20.21612890625" Y="0.713920776367" />
                  <Point X="-21.3080078125" Y="0.421352661133" />
                  <Point X="-21.31396875" Y="0.419543914795" />
                  <Point X="-21.33437890625" Y="0.412135070801" />
                  <Point X="-21.35762109375" Y="0.401617156982" />
                  <Point X="-21.36599609375" Y="0.397315734863" />
                  <Point X="-21.4516328125" Y="0.347816589355" />
                  <Point X="-21.472828125" Y="0.33358505249" />
                  <Point X="-21.4940078125" Y="0.317196258545" />
                  <Point X="-21.50281640625" Y="0.309465484619" />
                  <Point X="-21.519412109375" Y="0.292981719971" />
                  <Point X="-21.52719921875" Y="0.284228881836" />
                  <Point X="-21.578580078125" Y="0.218756347656" />
                  <Point X="-21.589140625" Y="0.204212402344" />
                  <Point X="-21.600859375" Y="0.184941818237" />
                  <Point X="-21.606142578125" Y="0.174960006714" />
                  <Point X="-21.615919921875" Y="0.153494400024" />
                  <Point X="-21.619986328125" Y="0.142947982788" />
                  <Point X="-21.626837890625" Y="0.121437644958" />
                  <Point X="-21.629623046875" Y="0.110473731995" />
                  <Point X="-21.64675" Y="0.021041704178" />
                  <Point X="-21.649875" Y="-0.004657866478" />
                  <Point X="-21.65125" Y="-0.030648736954" />
                  <Point X="-21.651162109375" Y="-0.042138027191" />
                  <Point X="-21.649599609375" Y="-0.065025352478" />
                  <Point X="-21.648125" Y="-0.076423240662" />
                  <Point X="-21.630998046875" Y="-0.165855422974" />
                  <Point X="-21.626837890625" Y="-0.183997817993" />
                  <Point X="-21.619986328125" Y="-0.205508148193" />
                  <Point X="-21.615919921875" Y="-0.216054580688" />
                  <Point X="-21.606142578125" Y="-0.237520187378" />
                  <Point X="-21.600859375" Y="-0.247501998901" />
                  <Point X="-21.589140625" Y="-0.266772583008" />
                  <Point X="-21.582705078125" Y="-0.27606137085" />
                  <Point X="-21.53132421875" Y="-0.341533752441" />
                  <Point X="-21.5270078125" Y="-0.346651733398" />
                  <Point X="-21.51334765625" Y="-0.361385162354" />
                  <Point X="-21.494916015625" Y="-0.379056121826" />
                  <Point X="-21.48622265625" Y="-0.386441345215" />
                  <Point X="-21.468017578125" Y="-0.400114929199" />
                  <Point X="-21.458505859375" Y="-0.406403167725" />
                  <Point X="-21.372869140625" Y="-0.45590246582" />
                  <Point X="-21.359146484375" Y="-0.46310369873" />
                  <Point X="-21.34019140625" Y="-0.472085723877" />
                  <Point X="-21.33228125" Y="-0.475405944824" />
                  <Point X="-21.3080078125" Y="-0.483913116455" />
                  <Point X="-21.211408203125" Y="-0.509796691895" />
                  <Point X="-20.21512109375" Y="-0.776751220703" />
                  <Point X="-20.235517578125" Y="-0.912041503906" />
                  <Point X="-20.238380859375" Y="-0.931040100098" />
                  <Point X="-20.2721953125" Y="-1.079219848633" />
                  <Point X="-21.563216796875" Y="-0.909253479004" />
                  <Point X="-21.571375" Y="-0.908535522461" />
                  <Point X="-21.59990234375" Y="-0.908042358398" />
                  <Point X="-21.63327734375" Y="-0.910840942383" />
                  <Point X="-21.645517578125" Y="-0.912676269531" />
                  <Point X="-21.813591796875" Y="-0.949207763672" />
                  <Point X="-21.842125" Y="-0.956742675781" />
                  <Point X="-21.8712421875" Y="-0.968366027832" />
                  <Point X="-21.88531640625" Y="-0.97538684082" />
                  <Point X="-21.91314453125" Y="-0.992279174805" />
                  <Point X="-21.925869140625" Y="-1.001526000977" />
                  <Point X="-21.949623046875" Y="-1.021997924805" />
                  <Point X="-21.96065234375" Y="-1.033223022461" />
                  <Point X="-22.0622421875" Y="-1.155404052734" />
                  <Point X="-22.078677734375" Y="-1.176853271484" />
                  <Point X="-22.093400390625" Y="-1.201242797852" />
                  <Point X="-22.099841796875" Y="-1.213990234375" />
                  <Point X="-22.11118359375" Y="-1.241378540039" />
                  <Point X="-22.115640625" Y="-1.254943969727" />
                  <Point X="-22.12246875" Y="-1.282586181641" />
                  <Point X="-22.12483984375" Y="-1.296662841797" />
                  <Point X="-22.139400390625" Y="-1.454892456055" />
                  <Point X="-22.140708984375" Y="-1.483322143555" />
                  <Point X="-22.138390625" Y="-1.51459375" />
                  <Point X="-22.135931640625" Y="-1.530136352539" />
                  <Point X="-22.12819921875" Y="-1.561756713867" />
                  <Point X="-22.123208984375" Y="-1.576676025391" />
                  <Point X="-22.110837890625" Y="-1.605484130859" />
                  <Point X="-22.10345703125" Y="-1.619373168945" />
                  <Point X="-22.010443359375" Y="-1.76405078125" />
                  <Point X="-21.999900390625" Y="-1.778843505859" />
                  <Point X="-21.980814453125" Y="-1.803082641602" />
                  <Point X="-21.9729296875" Y="-1.81190637207" />
                  <Point X="-21.956123046875" Y="-1.82850378418" />
                  <Point X="-21.947201171875" Y="-1.83627734375" />
                  <Point X="-21.85755859375" Y="-1.905063476562" />
                  <Point X="-20.912826171875" Y="-2.629980957031" />
                  <Point X="-20.946427734375" Y="-2.684353027344" />
                  <Point X="-20.954494140625" Y="-2.697403076172" />
                  <Point X="-20.9987265625" Y="-2.760251464844" />
                  <Point X="-22.151544921875" Y="-2.094670410156" />
                  <Point X="-22.158810546875" Y="-2.090883300781" />
                  <Point X="-22.1849765625" Y="-2.079512207031" />
                  <Point X="-22.216880859375" Y="-2.069325927734" />
                  <Point X="-22.228890625" Y="-2.066337646484" />
                  <Point X="-22.428923828125" Y="-2.030211547852" />
                  <Point X="-22.460642578125" Y="-2.025807495117" />
                  <Point X="-22.491998046875" Y="-2.025403686523" />
                  <Point X="-22.50769140625" Y="-2.026504272461" />
                  <Point X="-22.53986328125" Y="-2.031462402344" />
                  <Point X="-22.555158203125" Y="-2.035137451172" />
                  <Point X="-22.584931640625" Y="-2.044960693359" />
                  <Point X="-22.59941015625" Y="-2.051109130859" />
                  <Point X="-22.76558984375" Y="-2.138568115234" />
                  <Point X="-22.79102734375" Y="-2.153168945313" />
                  <Point X="-22.81395703125" Y="-2.170060058594" />
                  <Point X="-22.824787109375" Y="-2.179370361328" />
                  <Point X="-22.845744140625" Y="-2.200326171875" />
                  <Point X="-22.855056640625" Y="-2.21115625" />
                  <Point X="-22.871951171875" Y="-2.234088867188" />
                  <Point X="-22.879533203125" Y="-2.24619140625" />
                  <Point X="-22.9669921875" Y="-2.412370605469" />
                  <Point X="-22.980162109375" Y="-2.440192138672" />
                  <Point X="-22.989984375" Y="-2.469966796875" />
                  <Point X="-22.993658203125" Y="-2.485258789062" />
                  <Point X="-22.9986171875" Y="-2.517437011719" />
                  <Point X="-22.99971875" Y="-2.533125732422" />
                  <Point X="-22.99931640625" Y="-2.564479736328" />
                  <Point X="-22.9978125" Y="-2.580145019531" />
                  <Point X="-22.961685546875" Y="-2.780179199219" />
                  <Point X="-22.957970703125" Y="-2.796560058594" />
                  <Point X="-22.94916796875" Y="-2.828728027344" />
                  <Point X="-22.94520703125" Y="-2.840243408203" />
                  <Point X="-22.93584765625" Y="-2.862668701172" />
                  <Point X="-22.93044921875" Y="-2.873578613281" />
                  <Point X="-22.87284375" Y="-2.973354248047" />
                  <Point X="-22.264103515625" Y="-4.027721923828" />
                  <Point X="-22.276244140625" Y="-4.036082763672" />
                  <Point X="-23.16608203125" Y="-2.876424072266" />
                  <Point X="-23.17134375" Y="-2.870146728516" />
                  <Point X="-23.19116796875" Y="-2.849625732422" />
                  <Point X="-23.21674609375" Y="-2.828004638672" />
                  <Point X="-23.22669921875" Y="-2.820646972656" />
                  <Point X="-23.42398828125" Y="-2.693809082031" />
                  <Point X="-23.453716796875" Y="-2.676245117188" />
                  <Point X="-23.482529296875" Y="-2.663873535156" />
                  <Point X="-23.49744921875" Y="-2.658885009766" />
                  <Point X="-23.5290703125" Y="-2.651153564453" />
                  <Point X="-23.544607421875" Y="-2.6486953125" />
                  <Point X="-23.575875" Y="-2.646376708984" />
                  <Point X="-23.59160546875" Y="-2.646516357422" />
                  <Point X="-23.807373046875" Y="-2.666371582031" />
                  <Point X="-23.838775390625" Y="-2.670339355469" />
                  <Point X="-23.86642578125" Y="-2.677171630859" />
                  <Point X="-23.8799921875" Y="-2.681629882812" />
                  <Point X="-23.907376953125" Y="-2.692973388672" />
                  <Point X="-23.920123046875" Y="-2.699414550781" />
                  <Point X="-23.94450390625" Y="-2.714134521484" />
                  <Point X="-23.956138671875" Y="-2.722413330078" />
                  <Point X="-24.12275" Y="-2.860944335938" />
                  <Point X="-24.147341796875" Y="-2.883084228516" />
                  <Point X="-24.167810546875" Y="-2.906829101562" />
                  <Point X="-24.177060546875" Y="-2.919553466797" />
                  <Point X="-24.193953125" Y="-2.947376220703" />
                  <Point X="-24.200978515625" Y="-2.961458740234" />
                  <Point X="-24.21260546875" Y="-2.990584960938" />
                  <Point X="-24.21720703125" Y="-3.005628662109" />
                  <Point X="-24.267021484375" Y="-3.234819824219" />
                  <Point X="-24.269708984375" Y="-3.250201171875" />
                  <Point X="-24.274421875" Y="-3.285747558594" />
                  <Point X="-24.27524609375" Y="-3.298189208984" />
                  <Point X="-24.2752578125" Y="-3.323076416016" />
                  <Point X="-24.2744453125" Y="-3.335521972656" />
                  <Point X="-24.258119140625" Y="-3.459521972656" />
                  <Point X="-24.166912109375" Y="-4.152314941406" />
                  <Point X="-24.344931640625" Y="-3.487937744141" />
                  <Point X="-24.347392578125" Y="-3.48012109375" />
                  <Point X="-24.3578515625" Y="-3.453584472656" />
                  <Point X="-24.373208984375" Y="-3.423818115234" />
                  <Point X="-24.37958984375" Y="-3.413208740234" />
                  <Point X="-24.53115234375" Y="-3.194836425781" />
                  <Point X="-24.543318359375" Y="-3.177308349609" />
                  <Point X="-24.553330078125" Y="-3.165169433594" />
                  <Point X="-24.575216796875" Y="-3.142712646484" />
                  <Point X="-24.587091796875" Y="-3.132394775391" />
                  <Point X="-24.61334765625" Y="-3.113151611328" />
                  <Point X="-24.626759765625" Y="-3.104935546875" />
                  <Point X="-24.6547578125" Y="-3.090829101562" />
                  <Point X="-24.66934375" Y="-3.084938720703" />
                  <Point X="-24.903876953125" Y="-3.012147949219" />
                  <Point X="-24.936623046875" Y="-3.003109375" />
                  <Point X="-24.964783203125" Y="-2.99883984375" />
                  <Point X="-24.9790234375" Y="-2.997766357422" />
                  <Point X="-25.0086640625" Y="-2.997766357422" />
                  <Point X="-25.022904296875" Y="-2.998839599609" />
                  <Point X="-25.051064453125" Y="-3.003108886719" />
                  <Point X="-25.064984375" Y="-3.006304931641" />
                  <Point X="-25.299517578125" Y="-3.079095458984" />
                  <Point X="-25.3329296875" Y="-3.090828857422" />
                  <Point X="-25.3609296875" Y="-3.104936279297" />
                  <Point X="-25.37434375" Y="-3.113153076172" />
                  <Point X="-25.400599609375" Y="-3.132396728516" />
                  <Point X="-25.412474609375" Y="-3.142716552734" />
                  <Point X="-25.434359375" Y="-3.165173095703" />
                  <Point X="-25.444369140625" Y="-3.177309814453" />
                  <Point X="-25.5959296875" Y="-3.395681884766" />
                  <Point X="-25.608095703125" Y="-3.413210205078" />
                  <Point X="-25.61247265625" Y="-3.420135009766" />
                  <Point X="-25.625974609375" Y="-3.44526171875" />
                  <Point X="-25.63877734375" Y="-3.476214111328" />
                  <Point X="-25.64275390625" Y="-3.487937988281" />
                  <Point X="-25.672517578125" Y="-3.599020263672" />
                  <Point X="-25.985427734375" Y="-4.766806640625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.923123475113" Y="4.67779624829" />
                  <Point X="-24.647150989916" Y="4.690434195571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.343796944927" Y="4.702594195947" />
                  <Point X="-25.942840284594" Y="4.713050536336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.515591577045" Y="4.575668281385" />
                  <Point X="-24.672491432958" Y="4.595862043485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.318218454091" Y="4.607133250565" />
                  <Point X="-26.261872609131" Y="4.623604795111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.240387444131" Y="4.475850104212" />
                  <Point X="-24.697831875999" Y="4.5012898914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.292639963255" Y="4.511672305182" />
                  <Point X="-26.475273441751" Y="4.532315249336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.006345689343" Y="4.376750419022" />
                  <Point X="-24.723172319041" Y="4.406717739314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.26706147242" Y="4.4162113598" />
                  <Point X="-26.463073771939" Y="4.437087832143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.795301276394" Y="4.278052153927" />
                  <Point X="-24.748512804682" Y="4.312145587972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.241482981584" Y="4.320750414417" />
                  <Point X="-26.472147012596" Y="4.342231734984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.61787267787" Y="4.179940655056" />
                  <Point X="-24.780705059064" Y="4.217693034698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.210507220476" Y="4.225195259332" />
                  <Point X="-26.501602997854" Y="4.247731419955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.185909282772" Y="4.259676030589" />
                  <Point X="-27.207364669051" Y="4.260050535749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.449749174983" Y="4.081991577233" />
                  <Point X="-24.862510368993" Y="4.12410648053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.131824891289" Y="4.128807383003" />
                  <Point X="-26.551894079352" Y="4.153594782883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.111158516353" Y="4.163356779943" />
                  <Point X="-27.373176353664" Y="4.167930318306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.298107154742" Y="3.984330184759" />
                  <Point X="-26.705956095913" Y="4.061269474221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-26.963357681391" Y="4.065762435608" />
                  <Point X="-27.538988038277" Y="4.075810100862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.190575254961" Y="3.887438737303" />
                  <Point X="-27.689600697659" Y="3.983424583447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.244884557423" Y="3.793372238539" />
                  <Point X="-27.810770609333" Y="3.890525140958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.299193859885" Y="3.699305739776" />
                  <Point X="-27.931940521008" Y="3.79762569847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.353503162347" Y="3.605239241012" />
                  <Point X="-28.053110432682" Y="3.704726255981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.407812464808" Y="3.511172742249" />
                  <Point X="-28.006841961374" Y="3.608904165646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.46212176727" Y="3.417106243485" />
                  <Point X="-27.951426845204" Y="3.512922420031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.516431069732" Y="3.323039744722" />
                  <Point X="-27.896011729034" Y="3.416940674416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.570740372194" Y="3.228973245958" />
                  <Point X="-27.840596612864" Y="3.320958928801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.625049674656" Y="3.134906747195" />
                  <Point X="-27.785469450799" Y="3.224982209444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.679358977117" Y="3.040840248431" />
                  <Point X="-27.756438117271" Y="3.129460994468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.733668279579" Y="2.946773749668" />
                  <Point X="-27.74846537346" Y="3.034307358543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.787977582041" Y="2.852707250904" />
                  <Point X="-27.763627614222" Y="2.939557545276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.585862844444" Y="2.953909714606" />
                  <Point X="-28.786708279387" Y="2.957415484713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.020663681564" Y="2.726844200859" />
                  <Point X="-21.149418599703" Y="2.729091626315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.842286884503" Y="2.758640752141" />
                  <Point X="-27.826319020918" Y="2.845637356686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.416162741256" Y="2.855933117122" />
                  <Point X="-28.859638393888" Y="2.863674013433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.952167992719" Y="2.630634132998" />
                  <Point X="-21.309158965325" Y="2.636865433604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.896596186964" Y="2.664574253377" />
                  <Point X="-27.934777309546" Y="2.752516031992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.24535326127" Y="2.757937155394" />
                  <Point X="-28.932568508389" Y="2.769932542153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.894057868951" Y="2.534605345851" />
                  <Point X="-21.468899330946" Y="2.544639240893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.947941822852" Y="2.570456023621" />
                  <Point X="-29.004704125651" Y="2.676177202871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.919718024303" Y="2.440038774364" />
                  <Point X="-21.628639696568" Y="2.452413048183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.975991993392" Y="2.475931170005" />
                  <Point X="-29.059603592527" Y="2.582121005466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.040789025934" Y="2.347137605395" />
                  <Point X="-21.788380062189" Y="2.360186855472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.987271856681" Y="2.381113589587" />
                  <Point X="-29.114503276473" Y="2.48806481185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.161860027566" Y="2.254236436425" />
                  <Point X="-21.94812042781" Y="2.267960662762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.975432290314" Y="2.285892458023" />
                  <Point X="-29.169402960419" Y="2.394008618233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.282931029197" Y="2.161335267455" />
                  <Point X="-22.107860793432" Y="2.175734470051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.935865182481" Y="2.190187340423" />
                  <Point X="-29.224302644366" Y="2.299952424617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.404002030829" Y="2.068434098485" />
                  <Point X="-22.267601166222" Y="2.083508277466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.863143286482" Y="2.093903503842" />
                  <Point X="-29.108546690439" Y="2.202917425761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.52507303246" Y="1.975532929516" />
                  <Point X="-22.466466103303" Y="1.99196500669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.709388992742" Y="1.996205241498" />
                  <Point X="-28.981839209367" Y="2.105691267288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.646144034092" Y="1.882631760546" />
                  <Point X="-28.855131728295" Y="2.008465108815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.767215035723" Y="1.789730591576" />
                  <Point X="-28.728424247223" Y="1.911238950342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.873607667638" Y="1.69657321071" />
                  <Point X="-28.601716802535" Y="1.814012792504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.942230643916" Y="1.602756558052" />
                  <Point X="-28.494774489328" Y="1.717131636319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.979444455904" Y="1.508391656392" />
                  <Point X="-28.442496186266" Y="1.62120464398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.998547491984" Y="1.413710629963" />
                  <Point X="-28.421149866058" Y="1.525817571411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.988271840568" Y="1.318516796637" />
                  <Point X="-28.438928018365" Y="1.43111341905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.962872874552" Y="1.223058984871" />
                  <Point X="-28.482206580785" Y="1.336854378002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.292267513706" Y="1.098883988664" />
                  <Point X="-20.378343217094" Y="1.100386445656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.905922232681" Y="1.127050436556" />
                  <Point X="-28.589722250581" Y="1.243716599835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.140582537936" Y="1.253331901918" />
                  <Point X="-29.648770146215" Y="1.262202349616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.269038495084" Y="1.003464053472" />
                  <Point X="-21.015564988564" Y="1.016494721886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.807029941566" Y="1.030309794029" />
                  <Point X="-29.674395828437" Y="1.167635176399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.246353582319" Y="0.908053615682" />
                  <Point X="-29.700021510659" Y="1.073068003181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.231520403049" Y="0.812780230411" />
                  <Point X="-29.72564719288" Y="0.978500829964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.216687223778" Y="0.71750684514" />
                  <Point X="-29.742244611798" Y="0.883776067825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.536510023809" Y="0.628074901715" />
                  <Point X="-29.756267905705" Y="0.789006374166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.869421868242" Y="0.538871428411" />
                  <Point X="-29.770291199613" Y="0.694236680508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.202333712676" Y="0.449667955107" />
                  <Point X="-29.78431449352" Y="0.599466986849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.432843518344" Y="0.358677047567" />
                  <Point X="-29.436724243033" Y="0.498385305295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.541845894617" Y="0.265565219958" />
                  <Point X="-29.057416307498" Y="0.396749989488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.607627939925" Y="0.171698978666" />
                  <Point X="-28.678108371962" Y="0.295114673681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.635999116564" Y="0.077179728232" />
                  <Point X="-28.405381376512" Y="0.195339735104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.650558654125" Y="-0.017580605258" />
                  <Point X="-28.326733892648" Y="0.098952467002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.641166418112" Y="-0.112759018512" />
                  <Point X="-28.297577007498" Y="0.003429060515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.618963446497" Y="-0.208161043987" />
                  <Point X="-28.301714442092" Y="-0.09151319146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.560627571649" Y="-0.304193771635" />
                  <Point X="-28.340199819453" Y="-0.185855897864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.466914761608" Y="-0.400844005983" />
                  <Point X="-28.436350119166" Y="-0.279192059303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.249247070927" Y="-0.49965788082" />
                  <Point X="-28.721246719067" Y="-0.369233641818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.86993883832" Y="-0.601293201813" />
                  <Point X="-29.05415832439" Y="-0.458437119296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.490630967733" Y="-0.702928516486" />
                  <Point X="-29.387069929712" Y="-0.547640596774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.219030710808" Y="-0.802683787769" />
                  <Point X="-29.719981535035" Y="-0.636844074252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.233317581167" Y="-0.897448880683" />
                  <Point X="-29.775545848649" Y="-0.730888666715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.252321878408" Y="-0.992131630605" />
                  <Point X="-21.03784309607" Y="-0.978420306748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.860459068334" Y="-0.964061491542" />
                  <Point X="-29.761922499985" Y="-0.826140934314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.980407430473" Y="-1.056982256257" />
                  <Point X="-29.74829915132" Y="-0.921393201914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.05827892219" Y="-1.150637475477" />
                  <Point X="-29.729876918232" Y="-1.016729234353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.112277971261" Y="-1.244709389733" />
                  <Point X="-28.075441545843" Y="-1.140621982361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.495673753309" Y="-1.133286801895" />
                  <Point X="-29.70549805407" Y="-1.112169240174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.128775889204" Y="-1.339435888669" />
                  <Point X="-27.978532016625" Y="-1.23732801565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-29.132893943436" Y="-1.217178553267" />
                  <Point X="-29.681119189908" Y="-1.207609245995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.137505254143" Y="-1.434297988201" />
                  <Point X="-27.949985646116" Y="-1.332840765565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.136057991499" Y="-1.529337721429" />
                  <Point X="-27.957762934542" Y="-1.427719483655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.09984967538" Y="-1.624984211102" />
                  <Point X="-28.001348825136" Y="-1.521973160269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.038071314808" Y="-1.721077027561" />
                  <Point X="-28.102010110108" Y="-1.615230582168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.967443447778" Y="-1.81732431273" />
                  <Point X="-28.223081288756" Y="-1.708131748048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.845298673189" Y="-1.914470828865" />
                  <Point X="-28.344152467405" Y="-1.801032913928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.718590858661" Y="-2.011696993159" />
                  <Point X="-28.465223646053" Y="-1.893934079808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.591883044134" Y="-2.108923157452" />
                  <Point X="-22.143536627729" Y="-2.099294008333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.673392349138" Y="-2.090045342313" />
                  <Point X="-28.586294824702" Y="-1.986835245688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.465175229607" Y="-2.206149321746" />
                  <Point X="-21.973836452177" Y="-2.197270607079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.827781857076" Y="-2.182364934592" />
                  <Point X="-28.707366003351" Y="-2.079736411568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.33846741508" Y="-2.30337548604" />
                  <Point X="-21.804136276625" Y="-2.295247205826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.895326735709" Y="-2.276200405514" />
                  <Point X="-28.828437181999" Y="-2.172637577448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.211759600553" Y="-2.400601650333" />
                  <Point X="-21.634436101073" Y="-2.393223804573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.944877014091" Y="-2.370349973352" />
                  <Point X="-28.949508360648" Y="-2.265538743327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-21.085051786025" Y="-2.497827814627" />
                  <Point X="-21.464735925522" Y="-2.49120040332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.988216562312" Y="-2.464607949888" />
                  <Point X="-27.437386379433" Y="-2.386947401853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.692853632686" Y="-2.382488204361" />
                  <Point X="-29.070579539296" Y="-2.358439909207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.958343971498" Y="-2.59505397892" />
                  <Point X="-21.29503574997" Y="-2.589177002066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.999381237499" Y="-2.559427540922" />
                  <Point X="-27.344256842709" Y="-2.483587455128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.852594101637" Y="-2.474714395268" />
                  <Point X="-29.176354193191" Y="-2.45160807692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-20.950049997474" Y="-2.69021322194" />
                  <Point X="-21.125335574418" Y="-2.687153600813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.98434677791" Y="-2.654704439554" />
                  <Point X="-27.31182765247" Y="-2.579167979913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.012334506761" Y="-2.566940587289" />
                  <Point X="-29.119095886393" Y="-2.547621995547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.96713252668" Y="-2.750019386591" />
                  <Point X="-23.346866282336" Y="-2.743391109231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-23.968322298713" Y="-2.732543554115" />
                  <Point X="-27.321917516043" Y="-2.674006331853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.172074911885" Y="-2.65916677931" />
                  <Point X="-29.061837579594" Y="-2.643635914174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.943032127229" Y="-2.845454531792" />
                  <Point X="-23.201438571088" Y="-2.840944030537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.080246611656" Y="-2.825604379129" />
                  <Point X="-27.366958261193" Y="-2.768234613886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.331815317009" Y="-2.751392971331" />
                  <Point X="-29.004579272796" Y="-2.739649832801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.891308796921" Y="-2.941371837045" />
                  <Point X="-23.119299481799" Y="-2.937392244838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.176612246673" Y="-2.918936781878" />
                  <Point X="-27.421267664986" Y="-2.862301110881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.491555722133" Y="-2.843619163352" />
                  <Point X="-28.931439358274" Y="-2.835940965922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.835893669907" Y="-3.03735358285" />
                  <Point X="-23.045402517066" Y="-3.033696592319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.218855672674" Y="-3.013213891298" />
                  <Point X="-24.954692085966" Y="-3.000369818928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.024912849257" Y="-2.999144110945" />
                  <Point X="-27.475577068779" Y="-2.956367607876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-28.651296127257" Y="-2.935845355373" />
                  <Point X="-28.858144443342" Y="-2.932234804585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.78047851246" Y="-3.133335329185" />
                  <Point X="-22.971505552333" Y="-3.130000939801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.239428914436" Y="-3.107869255192" />
                  <Point X="-24.634628435136" Y="-3.100971021898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.327377384849" Y="-3.088879044002" />
                  <Point X="-27.529886472572" Y="-3.050434104871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.725063355014" Y="-3.22931707552" />
                  <Point X="-22.8976085876" Y="-3.226305287283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.260002156197" Y="-3.202524619085" />
                  <Point X="-24.529076083442" Y="-3.197827916214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.447483497947" Y="-3.181797055164" />
                  <Point X="-27.584195876365" Y="-3.144500601865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.669648197567" Y="-3.325298821856" />
                  <Point X="-22.823711622868" Y="-3.322609634764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.275185468417" Y="-3.297274064549" />
                  <Point X="-24.462322059174" Y="-3.294007583206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.512638703677" Y="-3.275674237981" />
                  <Point X="-27.638505280158" Y="-3.23856709886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.614233040121" Y="-3.421280568191" />
                  <Point X="-22.749814658135" Y="-3.418913982246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.266952353693" Y="-3.392432245265" />
                  <Point X="-24.395568034906" Y="-3.390187250198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.577793909407" Y="-3.369551420799" />
                  <Point X="-27.692814683951" Y="-3.332633595855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.558817882674" Y="-3.517262314527" />
                  <Point X="-22.675917693402" Y="-3.515218329727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.254414001486" Y="-3.487665574181" />
                  <Point X="-24.345517983835" Y="-3.486075348254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.63355672896" Y="-3.463592548327" />
                  <Point X="-27.747124087744" Y="-3.42670009285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.503402725228" Y="-3.613244060862" />
                  <Point X="-22.602020728669" Y="-3.611522677209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.241876420462" Y="-3.582898889636" />
                  <Point X="-24.319851545676" Y="-3.581537828762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.661558187495" Y="-3.558118252215" />
                  <Point X="-27.801433491537" Y="-3.520766589844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.447987567781" Y="-3.709225807198" />
                  <Point X="-22.528123763936" Y="-3.707827024691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.229338839437" Y="-3.678132205091" />
                  <Point X="-24.294272832998" Y="-3.676998778017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.686898575133" Y="-3.652690405267" />
                  <Point X="-27.85574289533" Y="-3.614833086839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.392572410335" Y="-3.805207553533" />
                  <Point X="-22.454226799203" Y="-3.804131372172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.216801258413" Y="-3.773365520546" />
                  <Point X="-24.268694120319" Y="-3.772459727272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.71223932733" Y="-3.747262551957" />
                  <Point X="-27.910052299123" Y="-3.708899583834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.337157252888" Y="-3.901189299868" />
                  <Point X="-22.38032983447" Y="-3.900435719654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.204263677389" Y="-3.868598836001" />
                  <Point X="-24.243115407641" Y="-3.867920676527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.737580079527" Y="-3.841834698646" />
                  <Point X="-26.536389013995" Y="-3.82789143683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.098823099964" Y="-3.818074113341" />
                  <Point X="-27.964361702916" Y="-3.802966080829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-22.281742095442" Y="-3.997171046204" />
                  <Point X="-22.306432869737" Y="-3.996740067135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.191726096365" Y="-3.963832151456" />
                  <Point X="-24.217536694962" Y="-3.963381625781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.762920831724" Y="-3.936406845335" />
                  <Point X="-26.4146560538" Y="-3.925030764718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.237402132892" Y="-3.910669678488" />
                  <Point X="-27.892964456355" Y="-3.899226795567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-24.17918851534" Y="-4.059065466911" />
                  <Point X="-24.191957982284" Y="-4.058842575036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.788261583921" Y="-4.030978992024" />
                  <Point X="-26.30411316412" Y="-4.021974769199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.372313407094" Y="-4.003329264601" />
                  <Point X="-27.766701368854" Y="-3.996445197122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.813602336118" Y="-4.125551138714" />
                  <Point X="-26.215439528757" Y="-4.118537044426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-27.452907590054" Y="-4.096936959069" />
                  <Point X="-27.61358446875" Y="-4.094132333719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.838943088315" Y="-4.220123285403" />
                  <Point X="-26.17972959885" Y="-4.214174834735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.864283840512" Y="-4.314695432092" />
                  <Point X="-26.160763713861" Y="-4.309520356653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.889624592709" Y="-4.409267578781" />
                  <Point X="-26.141797874559" Y="-4.404865877774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.914965344906" Y="-4.503839725471" />
                  <Point X="-26.122992529576" Y="-4.500208597455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.940306097103" Y="-4.59841187216" />
                  <Point X="-26.123324720109" Y="-4.595217270212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-25.9656468493" Y="-4.692984018849" />
                  <Point X="-26.135804948441" Y="-4.690013898181" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-25.000163085938" Y="0.001625976562" />
                  <Width Value="9.996462890625" />
                  <Height Value="9.978485351562" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-24.157642578125" Y="-4.92101953125" />
                  <Point X="-24.528458984375" Y="-3.537112304688" />
                  <Point X="-24.5356796875" Y="-3.521543212891" />
                  <Point X="-24.6872421875" Y="-3.303170898438" />
                  <Point X="-24.699408203125" Y="-3.285642822266" />
                  <Point X="-24.7256640625" Y="-3.266399658203" />
                  <Point X="-24.960197265625" Y="-3.193608886719" />
                  <Point X="-24.9790234375" Y="-3.187766357422" />
                  <Point X="-25.0086640625" Y="-3.187766357422" />
                  <Point X="-25.243197265625" Y="-3.260556884766" />
                  <Point X="-25.2620234375" Y="-3.266399658203" />
                  <Point X="-25.288279296875" Y="-3.285643310547" />
                  <Point X="-25.43983984375" Y="-3.504015380859" />
                  <Point X="-25.452005859375" Y="-3.521543701172" />
                  <Point X="-25.4592265625" Y="-3.537112304688" />
                  <Point X="-25.488990234375" Y="-3.648194580078" />
                  <Point X="-25.847744140625" Y="-4.987076660156" />
                  <Point X="-26.079447265625" Y="-4.942102539062" />
                  <Point X="-26.1002421875" Y="-4.938065917969" />
                  <Point X="-26.351587890625" Y="-4.873396972656" />
                  <Point X="-26.3091484375" Y="-4.551038574219" />
                  <Point X="-26.309681640625" Y="-4.534756347656" />
                  <Point X="-26.365712890625" Y="-4.253074707031" />
                  <Point X="-26.3702109375" Y="-4.23046484375" />
                  <Point X="-26.386283203125" Y="-4.202628417969" />
                  <Point X="-26.6022109375" Y="-4.013263671875" />
                  <Point X="-26.61954296875" Y="-3.998063964844" />
                  <Point X="-26.649240234375" Y="-3.985762939453" />
                  <Point X="-26.935826171875" Y="-3.966979003906" />
                  <Point X="-26.958830078125" Y="-3.965471435547" />
                  <Point X="-26.989880859375" Y="-3.973792236328" />
                  <Point X="-27.2286796875" Y="-4.133352539062" />
                  <Point X="-27.247845703125" Y="-4.14616015625" />
                  <Point X="-27.259732421875" Y="-4.157294433594" />
                  <Point X="-27.276443359375" Y="-4.179071777344" />
                  <Point X="-27.45709375" Y="-4.414500488281" />
                  <Point X="-27.8253515625" Y="-4.186485351562" />
                  <Point X="-27.855830078125" Y="-4.167613769531" />
                  <Point X="-28.228580078125" Y="-3.880607910156" />
                  <Point X="-28.195154296875" Y="-3.822712402344" />
                  <Point X="-27.506033203125" Y="-2.629119628906" />
                  <Point X="-27.49976171875" Y="-2.597593261719" />
                  <Point X="-27.513978515625" Y="-2.568764404297" />
                  <Point X="-27.530037109375" Y="-2.552704833984" />
                  <Point X="-27.531326171875" Y="-2.551415771484" />
                  <Point X="-27.56015625" Y="-2.537198730469" />
                  <Point X="-27.591681640625" Y="-2.543469726562" />
                  <Point X="-27.68748828125" Y="-2.598783691406" />
                  <Point X="-28.84295703125" Y="-3.265893798828" />
                  <Point X="-29.137619140625" Y="-2.87876953125" />
                  <Point X="-29.161701171875" Y="-2.847130126953" />
                  <Point X="-29.431017578125" Y="-2.395525878906" />
                  <Point X="-29.369525390625" Y="-2.348340576172" />
                  <Point X="-28.163787109375" Y="-1.423144897461" />
                  <Point X="-28.145822265625" Y="-1.396014770508" />
                  <Point X="-28.138689453125" Y="-1.368477783203" />
                  <Point X="-28.138115234375" Y="-1.366262084961" />
                  <Point X="-28.140326171875" Y="-1.334593261719" />
                  <Point X="-28.161158203125" Y="-1.310638793945" />
                  <Point X="-28.185673828125" Y="-1.296210571289" />
                  <Point X="-28.187630859375" Y="-1.29505871582" />
                  <Point X="-28.21952734375" Y="-1.288571044922" />
                  <Point X="-28.340474609375" Y="-1.304494018555" />
                  <Point X="-29.80328125" Y="-1.497076293945" />
                  <Point X="-29.91797265625" Y="-1.048069458008" />
                  <Point X="-29.927390625" Y="-1.011192993164" />
                  <Point X="-29.99839453125" Y="-0.514741943359" />
                  <Point X="-29.93098046875" Y="-0.496678314209" />
                  <Point X="-28.557462890625" Y="-0.128645339966" />
                  <Point X="-28.5418984375" Y="-0.12142742157" />
                  <Point X="-28.51620703125" Y="-0.103596504211" />
                  <Point X="-28.51414453125" Y="-0.102165397644" />
                  <Point X="-28.4948984375" Y="-0.075910133362" />
                  <Point X="-28.4863359375" Y="-0.048317913055" />
                  <Point X="-28.4856484375" Y="-0.046103252411" />
                  <Point X="-28.4856484375" Y="-0.016456857681" />
                  <Point X="-28.4942109375" Y="0.01113535881" />
                  <Point X="-28.4948984375" Y="0.013350020409" />
                  <Point X="-28.51414453125" Y="0.039605285645" />
                  <Point X="-28.5398359375" Y="0.057436050415" />
                  <Point X="-28.557462890625" Y="0.066085227966" />
                  <Point X="-28.6677109375" Y="0.095626205444" />
                  <Point X="-29.998185546875" Y="0.452126068115" />
                  <Point X="-29.92371484375" Y="0.955399780273" />
                  <Point X="-29.91764453125" Y="0.996417358398" />
                  <Point X="-29.7736953125" Y="1.527634521484" />
                  <Point X="-29.773515625" Y="1.528298706055" />
                  <Point X="-29.7378359375" Y="1.5236015625" />
                  <Point X="-28.753263671875" Y="1.393980224609" />
                  <Point X="-28.731703125" Y="1.395866455078" />
                  <Point X="-28.674841796875" Y="1.413794799805" />
                  <Point X="-28.67027734375" Y="1.415234008789" />
                  <Point X="-28.651533203125" Y="1.426055786133" />
                  <Point X="-28.639119140625" Y="1.443785644531" />
                  <Point X="-28.616302734375" Y="1.498868408203" />
                  <Point X="-28.614466796875" Y="1.503301879883" />
                  <Point X="-28.610712890625" Y="1.524611328125" />
                  <Point X="-28.616314453125" Y="1.545511230469" />
                  <Point X="-28.64384375" Y="1.598396118164" />
                  <Point X="-28.646060546875" Y="1.602651245117" />
                  <Point X="-28.65996875" Y="1.619221801758" />
                  <Point X="-28.72320703125" Y="1.667746582031" />
                  <Point X="-29.47610546875" Y="2.245466064453" />
                  <Point X="-29.18359765625" Y="2.746600585938" />
                  <Point X="-29.160013671875" Y="2.787006103516" />
                  <Point X="-28.778697265625" Y="3.277134765625" />
                  <Point X="-28.7638515625" Y="3.276064941406" />
                  <Point X="-28.15915625" Y="2.926943359375" />
                  <Point X="-28.138515625" Y="2.920435058594" />
                  <Point X="-28.059322265625" Y="2.913506591797" />
                  <Point X="-28.052966796875" Y="2.912950439453" />
                  <Point X="-28.0315078125" Y="2.915775146484" />
                  <Point X="-28.013251953125" Y="2.927404785156" />
                  <Point X="-27.9570390625" Y="2.983616455078" />
                  <Point X="-27.95252734375" Y="2.988128173828" />
                  <Point X="-27.9408984375" Y="3.0063828125" />
                  <Point X="-27.938072265625" Y="3.027842041016" />
                  <Point X="-27.945001953125" Y="3.107034912109" />
                  <Point X="-27.945556640625" Y="3.113379394531" />
                  <Point X="-27.95206640625" Y="3.134032958984" />
                  <Point X="-27.98008984375" Y="3.182570068359" />
                  <Point X="-28.30727734375" Y="3.749276123047" />
                  <Point X="-27.793947265625" Y="4.142840332031" />
                  <Point X="-27.752869140625" Y="4.1743359375" />
                  <Point X="-27.1523125" Y="4.507990722656" />
                  <Point X="-27.141546875" Y="4.513971679688" />
                  <Point X="-26.967826171875" Y="4.287573242188" />
                  <Point X="-26.9512421875" Y="4.273659179688" />
                  <Point X="-26.8631015625" Y="4.227775390625" />
                  <Point X="-26.856025390625" Y="4.224092285156" />
                  <Point X="-26.83512109375" Y="4.218491699219" />
                  <Point X="-26.81380859375" Y="4.22225" />
                  <Point X="-26.72200390625" Y="4.26027734375" />
                  <Point X="-26.714634765625" Y="4.263329589844" />
                  <Point X="-26.696904296875" Y="4.275744140625" />
                  <Point X="-26.68608203125" Y="4.294488769531" />
                  <Point X="-26.656201171875" Y="4.389258300781" />
                  <Point X="-26.653802734375" Y="4.396865234375" />
                  <Point X="-26.651916015625" Y="4.418426269531" />
                  <Point X="-26.655103515625" Y="4.442625976562" />
                  <Point X="-26.68913671875" Y="4.701140625" />
                  <Point X="-26.021267578125" Y="4.888388183594" />
                  <Point X="-25.96809375" Y="4.903296386719" />
                  <Point X="-25.24002734375" Y="4.988505859375" />
                  <Point X="-25.221025390625" Y="4.978514160156" />
                  <Point X="-25.042140625" Y="4.310903320312" />
                  <Point X="-25.02428125" Y="4.284176757813" />
                  <Point X="-24.99384375" Y="4.273844238281" />
                  <Point X="-24.96340625" Y="4.284176757813" />
                  <Point X="-24.945546875" Y="4.310903320312" />
                  <Point X="-24.9311875" Y="4.364490722656" />
                  <Point X="-24.763349609375" Y="4.990868652344" />
                  <Point X="-24.186224609375" Y="4.930428222656" />
                  <Point X="-24.13979296875" Y="4.925565429688" />
                  <Point X="-23.537447265625" Y="4.780140625" />
                  <Point X="-23.491541015625" Y="4.769057128906" />
                  <Point X="-23.098609375" Y="4.626538574219" />
                  <Point X="-23.068970703125" Y="4.615788085938" />
                  <Point X="-22.68985546875" Y="4.438488769531" />
                  <Point X="-22.66130078125" Y="4.425134277344" />
                  <Point X="-22.295068359375" Y="4.211767089844" />
                  <Point X="-22.26747265625" Y="4.195689941406" />
                  <Point X="-21.9312578125" Y="3.956592773438" />
                  <Point X="-21.9725390625" Y="3.885092285156" />
                  <Point X="-22.77014453125" Y="2.503597900391" />
                  <Point X="-22.775146484375" Y="2.491517822266" />
                  <Point X="-22.795021484375" Y="2.417197021484" />
                  <Point X="-22.797955078125" Y="2.392325195312" />
                  <Point X="-22.790205078125" Y="2.328058837891" />
                  <Point X="-22.789583984375" Y="2.322909667969" />
                  <Point X="-22.781318359375" Y="2.3008125" />
                  <Point X="-22.741552734375" Y="2.242207763672" />
                  <Point X="-22.741544921875" Y="2.242198242188" />
                  <Point X="-22.72505859375" Y="2.224204101562" />
                  <Point X="-22.666455078125" Y="2.184438232422" />
                  <Point X="-22.661744140625" Y="2.181242431641" />
                  <Point X="-22.639662109375" Y="2.172980224609" />
                  <Point X="-22.575396484375" Y="2.165230712891" />
                  <Point X="-22.551337890625" Y="2.165945556641" />
                  <Point X="-22.477017578125" Y="2.185820068359" />
                  <Point X="-22.45896484375" Y="2.192417480469" />
                  <Point X="-22.348076171875" Y="2.256439208984" />
                  <Point X="-21.005751953125" Y="3.031430908203" />
                  <Point X="-20.8137734375" Y="2.764625244141" />
                  <Point X="-20.797408203125" Y="2.741883789062" />
                  <Point X="-20.612486328125" Y="2.436295654297" />
                  <Point X="-20.661259765625" Y="2.398870605469" />
                  <Point X="-21.7113828125" Y="1.593082763672" />
                  <Point X="-21.72062890625" Y="1.583831298828" />
                  <Point X="-21.7741171875" Y="1.514051025391" />
                  <Point X="-21.78687890625" Y="1.491502685547" />
                  <Point X="-21.8068046875" Y="1.420256958008" />
                  <Point X="-21.808404296875" Y="1.414538208008" />
                  <Point X="-21.809220703125" Y="1.390968017578" />
                  <Point X="-21.79286328125" Y="1.311698120117" />
                  <Point X="-21.784353515625" Y="1.287957763672" />
                  <Point X="-21.7398671875" Y="1.22033984375" />
                  <Point X="-21.736296875" Y="1.214912475586" />
                  <Point X="-21.7190546875" Y="1.198821411133" />
                  <Point X="-21.6545859375" Y="1.162531860352" />
                  <Point X="-21.631431640625" Y="1.153619506836" />
                  <Point X="-21.544267578125" Y="1.142099487305" />
                  <Point X="-21.5241953125" Y="1.14117175293" />
                  <Point X="-21.418859375" Y="1.155039672852" />
                  <Point X="-20.151021484375" Y="1.321953491211" />
                  <Point X="-20.06783984375" Y="0.98025970459" />
                  <Point X="-20.060810546875" Y="0.951386962891" />
                  <Point X="-20.002140625" Y="0.574556274414" />
                  <Point X="-20.0553671875" Y="0.560293945312" />
                  <Point X="-21.25883203125" Y="0.237826919556" />
                  <Point X="-21.2709140625" Y="0.232818191528" />
                  <Point X="-21.35655078125" Y="0.183318954468" />
                  <Point X="-21.37773046875" Y="0.166930099487" />
                  <Point X="-21.429111328125" Y="0.101457588196" />
                  <Point X="-21.433236328125" Y="0.096202270508" />
                  <Point X="-21.443013671875" Y="0.074736686707" />
                  <Point X="-21.460140625" Y="-0.014695351601" />
                  <Point X="-21.461515625" Y="-0.04068624115" />
                  <Point X="-21.444388671875" Y="-0.130118438721" />
                  <Point X="-21.443013671875" Y="-0.137296798706" />
                  <Point X="-21.433236328125" Y="-0.158762374878" />
                  <Point X="-21.38185546875" Y="-0.224234741211" />
                  <Point X="-21.363423828125" Y="-0.241905792236" />
                  <Point X="-21.277787109375" Y="-0.291405029297" />
                  <Point X="-21.25883203125" Y="-0.300387023926" />
                  <Point X="-21.162232421875" Y="-0.32627053833" />
                  <Point X="-20.001931640625" Y="-0.637172485352" />
                  <Point X="-20.047642578125" Y="-0.940366943359" />
                  <Point X="-20.051568359375" Y="-0.966411560059" />
                  <Point X="-20.125451171875" Y="-1.290178588867" />
                  <Point X="-20.192337890625" Y="-1.281372924805" />
                  <Point X="-21.588017578125" Y="-1.097628173828" />
                  <Point X="-21.605162109375" Y="-1.098341308594" />
                  <Point X="-21.773236328125" Y="-1.134872802734" />
                  <Point X="-21.7867265625" Y="-1.137805053711" />
                  <Point X="-21.8145546875" Y="-1.154697387695" />
                  <Point X="-21.91614453125" Y="-1.276878417969" />
                  <Point X="-21.924298828125" Y="-1.286685424805" />
                  <Point X="-21.935640625" Y="-1.314073486328" />
                  <Point X="-21.950201171875" Y="-1.472303100586" />
                  <Point X="-21.951369140625" Y="-1.48500378418" />
                  <Point X="-21.94363671875" Y="-1.516624145508" />
                  <Point X="-21.850623046875" Y="-1.661301757812" />
                  <Point X="-21.831537109375" Y="-1.685540893555" />
                  <Point X="-21.74189453125" Y="-1.754327148438" />
                  <Point X="-20.660923828125" Y="-2.583784423828" />
                  <Point X="-20.78480078125" Y="-2.784236816406" />
                  <Point X="-20.79585546875" Y="-2.802122070312" />
                  <Point X="-20.943310546875" Y="-3.011638183594" />
                  <Point X="-21.00350390625" Y="-2.976885986328" />
                  <Point X="-22.246544921875" Y="-2.259215576172" />
                  <Point X="-22.26266015625" Y="-2.253312744141" />
                  <Point X="-22.462693359375" Y="-2.217186767578" />
                  <Point X="-22.47875" Y="-2.214287109375" />
                  <Point X="-22.510921875" Y="-2.219245361328" />
                  <Point X="-22.6771015625" Y="-2.306704345703" />
                  <Point X="-22.690439453125" Y="-2.313724365234" />
                  <Point X="-22.711396484375" Y="-2.334680175781" />
                  <Point X="-22.79885546875" Y="-2.500859375" />
                  <Point X="-22.805876953125" Y="-2.514198242188" />
                  <Point X="-22.8108359375" Y="-2.546376464844" />
                  <Point X="-22.774708984375" Y="-2.746410644531" />
                  <Point X="-22.76590625" Y="-2.778578613281" />
                  <Point X="-22.70830078125" Y="-2.878354248047" />
                  <Point X="-22.01332421875" Y="-4.082087890625" />
                  <Point X="-22.151568359375" Y="-4.18083203125" />
                  <Point X="-22.164705078125" Y="-4.19021484375" />
                  <Point X="-22.320224609375" Y="-4.290878417969" />
                  <Point X="-22.368265625" Y="-4.228268066406" />
                  <Point X="-23.3168203125" Y="-2.992087402344" />
                  <Point X="-23.32944921875" Y="-2.980467285156" />
                  <Point X="-23.52673828125" Y="-2.853629394531" />
                  <Point X="-23.54257421875" Y="-2.843448486328" />
                  <Point X="-23.5741953125" Y="-2.835717041016" />
                  <Point X="-23.789962890625" Y="-2.855572265625" />
                  <Point X="-23.80728125" Y="-2.857166015625" />
                  <Point X="-23.834666015625" Y="-2.868509521484" />
                  <Point X="-24.00127734375" Y="-3.007040527344" />
                  <Point X="-24.014650390625" Y="-3.018159912109" />
                  <Point X="-24.03154296875" Y="-3.045982666016" />
                  <Point X="-24.081357421875" Y="-3.275173828125" />
                  <Point X="-24.0860703125" Y="-3.310720214844" />
                  <Point X="-24.069744140625" Y="-3.434720214844" />
                  <Point X="-23.872357421875" Y="-4.934028320312" />
                  <Point X="-23.993234375" Y="-4.960524902344" />
                  <Point X="-24.005654296875" Y="-4.963247558594" />
                  <Point X="-24.139798828125" Y="-4.987616699219" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#210" />
            <Label Value="Polygon" />
            <Translation X="-24.993844032288" Y="-0.031280040741" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.17888588467" Y="5.022735717989" Z="2.35" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.35" />
                  <Point X="-0.251997501656" Y="5.069446858853" Z="2.35" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.35" />
                  <Point X="-1.040921814484" Y="4.967811911153" Z="2.35" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.35" />
                  <Point X="-1.710831794447" Y="4.467379908761" Z="2.35" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.35" />
                  <Point X="-1.710044342814" Y="4.435573668988" Z="2.35" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.35" />
                  <Point X="-1.747294078111" Y="4.337751762799" Z="2.35" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.35" />
                  <Point X="-1.846173908459" Y="4.303407543494" Z="2.35" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.35" />
                  <Point X="-2.119431006362" Y="4.590539045953" Z="2.35" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.35" />
                  <Point X="-2.182753310146" Y="4.582978036368" Z="2.35" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.35" />
                  <Point X="-2.830528703279" Y="4.213799789576" Z="2.35" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.35" />
                  <Point X="-3.029547711361" Y="3.188850209572" Z="2.35" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.35" />
                  <Point X="-3.000968552032" Y="3.133956324779" Z="2.35" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.35" />
                  <Point X="-2.998551962028" Y="3.050251605858" Z="2.35" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.35" />
                  <Point X="-3.061120140828" Y="2.994596147141" Z="2.35" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.35" />
                  <Point X="-3.745009138725" Y="3.350646237385" Z="2.35" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.35" />
                  <Point X="-3.824317522902" Y="3.339117366544" Z="2.35" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.35" />
                  <Point X="-4.232936940108" Y="2.803085765564" Z="2.35" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.35" />
                  <Point X="-3.759801084296" Y="1.659358949662" Z="2.35" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.35" />
                  <Point X="-3.694352525582" Y="1.606589202827" Z="2.35" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.35" />
                  <Point X="-3.668653818099" Y="1.549283027099" Z="2.35" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.35" />
                  <Point X="-3.696034064265" Y="1.492761038834" Z="2.35" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.35" />
                  <Point X="-4.737467093253" Y="1.604453808156" Z="2.35" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.35" />
                  <Point X="-4.828112066969" Y="1.571990917284" Z="2.35" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.35" />
                  <Point X="-4.979332020417" Y="0.993999727868" Z="2.35" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.35" />
                  <Point X="-3.686809225746" Y="0.078610026401" Z="2.35" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.35" />
                  <Point X="-3.57449860997" Y="0.047637799053" Z="2.35" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.35" />
                  <Point X="-3.548120472366" Y="0.027592201131" Z="2.35" />
                  <Point X="-3.539556741714" Y="0" Z="2.35" />
                  <Point X="-3.540244128769" Y="-0.00221474993" Z="2.35" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.35" />
                  <Point X="-3.550869985154" Y="-0.03123818408" Z="2.35" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.35" />
                  <Point X="-4.950078563027" Y="-0.417102030355" Z="2.35" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.35" />
                  <Point X="-5.054556275591" Y="-0.48699169214" Z="2.35" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.35" />
                  <Point X="-4.972574449274" Y="-1.029162117895" Z="2.35" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.35" />
                  <Point X="-3.340105403614" Y="-1.322786215772" Z="2.35" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.35" />
                  <Point X="-3.217191148972" Y="-1.308021433436" Z="2.35" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.35" />
                  <Point X="-3.193248774011" Y="-1.324660125993" Z="2.35" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.35" />
                  <Point X="-4.406119295095" Y="-2.277392817281" Z="2.35" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.35" />
                  <Point X="-4.481089232743" Y="-2.388230055878" Z="2.35" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.35" />
                  <Point X="-4.183563070699" Y="-2.877772214814" Z="2.35" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.35" />
                  <Point X="-2.668645916043" Y="-2.610804830791" Z="2.35" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.35" />
                  <Point X="-2.571550448496" Y="-2.556780020635" Z="2.35" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.35" />
                  <Point X="-3.244612182378" Y="-3.766432466449" Z="2.35" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.35" />
                  <Point X="-3.269502584184" Y="-3.885663933607" Z="2.35" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.35" />
                  <Point X="-2.857829852465" Y="-4.19771553676" Z="2.35" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.35" />
                  <Point X="-2.242932715858" Y="-4.178229639661" Z="2.35" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.35" />
                  <Point X="-2.207054573177" Y="-4.143644710087" Z="2.35" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.35" />
                  <Point X="-1.945252552258" Y="-3.985592094797" Z="2.35" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.35" />
                  <Point X="-1.641334835058" Y="-4.01957572371" Z="2.35" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.35" />
                  <Point X="-1.420908600279" Y="-4.231550335332" Z="2.35" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.35" />
                  <Point X="-1.409516115033" Y="-4.852288164892" Z="2.35" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.35" />
                  <Point X="-1.391127820433" Y="-4.885156256632" Z="2.35" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.35" />
                  <Point X="-1.095130680207" Y="-4.959906482645" Z="2.35" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.35" />
                  <Point X="-0.446851226788" Y="-3.629855514263" Z="2.35" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.35" />
                  <Point X="-0.404921298506" Y="-3.501244939329" Z="2.35" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.35" />
                  <Point X="-0.234533705304" Y="-3.277029975463" Z="2.35" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.35" />
                  <Point X="0.018825374057" Y="-3.210082069825" Z="2.35" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.35" />
                  <Point X="0.265524560927" Y="-3.300400816439" Z="2.35" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.35" />
                  <Point X="0.787903633613" Y="-4.902680515553" Z="2.35" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.35" />
                  <Point X="0.831068062241" Y="-5.011328663234" Z="2.35" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.35" />
                  <Point X="1.011313601206" Y="-4.978085270126" Z="2.35" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.35" />
                  <Point X="0.973670716939" Y="-3.396913858298" Z="2.35" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.35" />
                  <Point X="0.96134434992" Y="-3.254517153334" Z="2.35" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.35" />
                  <Point X="1.024533371954" Y="-3.014206506676" Z="2.35" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.35" />
                  <Point X="1.208462824234" Y="-2.874081722975" Z="2.35" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.35" />
                  <Point X="1.440066196293" Y="-2.86440750682" Z="2.35" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.35" />
                  <Point X="2.585908784678" Y="-4.227425814576" Z="2.35" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.35" />
                  <Point X="2.676552642333" Y="-4.317261183741" Z="2.35" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.35" />
                  <Point X="2.871334631609" Y="-4.190240521488" Z="2.35" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.35" />
                  <Point X="2.328842312604" Y="-2.822074087832" Z="2.35" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.35" />
                  <Point X="2.268337158424" Y="-2.706242424786" Z="2.35" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.35" />
                  <Point X="2.239231205178" Y="-2.492869432224" Z="2.35" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.35" />
                  <Point X="2.340028946148" Y="-2.31967010479" Z="2.35" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.35" />
                  <Point X="2.522264378726" Y="-2.235110851067" Z="2.35" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.35" />
                  <Point X="3.965339509275" Y="-2.988907187671" Z="2.35" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.35" />
                  <Point X="4.078088843168" Y="-3.028078524101" Z="2.35" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.35" />
                  <Point X="4.251572583624" Y="-2.779244091748" Z="2.35" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.35" />
                  <Point X="3.282387010763" Y="-1.683379441626" Z="2.35" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.35" />
                  <Point X="3.185276821845" Y="-1.602980152475" Z="2.35" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.35" />
                  <Point X="3.093431216829" Y="-1.445601843084" Z="2.35" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.35" />
                  <Point X="3.116145785658" Y="-1.2775651056" Z="2.35" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.35" />
                  <Point X="3.231226328077" Y="-1.152451901929" Z="2.35" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.35" />
                  <Point X="4.79497939161" Y="-1.299665106759" Z="2.35" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.35" />
                  <Point X="4.913280276789" Y="-1.286922294118" Z="2.35" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.35" />
                  <Point X="4.995642217184" Y="-0.916539676377" Z="2.35" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.35" />
                  <Point X="3.844551413938" Y="-0.24669418317" Z="2.35" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.35" />
                  <Point X="3.741078917693" Y="-0.216837467691" Z="2.35" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.35" />
                  <Point X="3.65131844551" Y="-0.162082919244" Z="2.35" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.35" />
                  <Point X="3.598561967316" Y="-0.089432065802" Z="2.35" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.35" />
                  <Point X="3.582809483109" Y="0.007178465376" Z="2.35" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.35" />
                  <Point X="3.60406099289" Y="0.101865819331" Z="2.35" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.35" />
                  <Point X="3.662316496659" Y="0.171311376551" Z="2.35" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.35" />
                  <Point X="4.951416403369" Y="0.543277749599" Z="2.35" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.35" />
                  <Point X="5.043118375074" Y="0.60061227408" Z="2.35" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.35" />
                  <Point X="4.974583939535" Y="1.023367223599" Z="2.35" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.35" />
                  <Point X="3.568458298563" Y="1.235891951323" Z="2.35" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.35" />
                  <Point X="3.456124971839" Y="1.222948748224" Z="2.35" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.35" />
                  <Point X="3.363785995021" Y="1.237381444382" Z="2.35" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.35" />
                  <Point X="3.2957477249" Y="1.279098531259" Z="2.35" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.35" />
                  <Point X="3.249948136548" Y="1.353079189599" Z="2.35" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.35" />
                  <Point X="3.235191330549" Y="1.438067908525" Z="2.35" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.35" />
                  <Point X="3.259409382524" Y="1.514914776844" Z="2.35" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.35" />
                  <Point X="4.363021381676" Y="2.390482962493" Z="2.35" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.35" />
                  <Point X="4.431773019248" Y="2.48083946111" Z="2.35" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.35" />
                  <Point X="4.220654170397" Y="2.825109846546" Z="2.35" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.35" />
                  <Point X="2.620766674689" Y="2.331020416483" Z="2.35" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.35" />
                  <Point X="2.50391239895" Y="2.265403509767" Z="2.35" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.35" />
                  <Point X="2.42443317037" Y="2.246151192594" Z="2.35" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.35" />
                  <Point X="2.355462751998" Y="2.2570924082" Z="2.35" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.35" />
                  <Point X="2.293666192035" Y="2.301562108384" Z="2.35" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.35" />
                  <Point X="2.253278510165" Y="2.365325273462" Z="2.35" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.35" />
                  <Point X="2.247124432891" Y="2.435557176932" Z="2.35" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.35" />
                  <Point X="3.064604936376" Y="3.891372407266" Z="2.35" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.35" />
                  <Point X="3.100753342881" Y="4.02208307171" Z="2.35" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.35" />
                  <Point X="2.72394487246" Y="4.286249628217" Z="2.35" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.35" />
                  <Point X="2.325169779958" Y="4.515060697623" Z="2.35" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.35" />
                  <Point X="1.912281979878" Y="4.704822136772" Z="2.35" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.35" />
                  <Point X="1.46813003542" Y="4.860024372681" Z="2.35" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.35" />
                  <Point X="0.812826526386" Y="5.011436253047" Z="2.35" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.35" />
                  <Point X="0.014358676883" Y="4.408711787962" Z="2.35" />
                  <Point X="0" Y="4.355124473572" Z="2.35" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>