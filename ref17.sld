<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#149" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1342" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.689669677734" Y="21.015865234375" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557721618652" Y="21.502857421875" />
                  <Point X="0.542362854004" Y="21.532625" />
                  <Point X="0.490713317871" Y="21.607041015625" />
                  <Point X="0.378635223389" Y="21.768525390625" />
                  <Point X="0.356748565674" Y="21.790982421875" />
                  <Point X="0.330493225098" Y="21.810224609375" />
                  <Point X="0.302494262695" Y="21.824330078125" />
                  <Point X="0.222569488525" Y="21.849134765625" />
                  <Point X="0.049135276794" Y="21.902962890625" />
                  <Point X="0.020983165741" Y="21.907232421875" />
                  <Point X="-0.008658161163" Y="21.907234375" />
                  <Point X="-0.03682484436" Y="21.90296484375" />
                  <Point X="-0.11674962616" Y="21.878158203125" />
                  <Point X="-0.290183837891" Y="21.82433203125" />
                  <Point X="-0.318184143066" Y="21.810224609375" />
                  <Point X="-0.34443963623" Y="21.79098046875" />
                  <Point X="-0.366323577881" Y="21.7685234375" />
                  <Point X="-0.417973297119" Y="21.69410546875" />
                  <Point X="-0.530051391602" Y="21.532623046875" />
                  <Point X="-0.538189147949" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.825203613281" Y="20.464095703125" />
                  <Point X="-0.916584594727" Y="20.12305859375" />
                  <Point X="-1.079322143555" Y="20.154646484375" />
                  <Point X="-1.168396118164" Y="20.177564453125" />
                  <Point X="-1.246417602539" Y="20.197638671875" />
                  <Point X="-1.230174682617" Y="20.321015625" />
                  <Point X="-1.214963012695" Y="20.43655859375" />
                  <Point X="-1.214201171875" Y="20.452068359375" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.235602783203" Y="20.579765625" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287937866211" Y="20.817033203125" />
                  <Point X="-1.304009765625" Y="20.84487109375" />
                  <Point X="-1.323645141602" Y="20.868796875" />
                  <Point X="-1.397229736328" Y="20.933328125" />
                  <Point X="-1.55690612793" Y="21.073361328125" />
                  <Point X="-1.583189086914" Y="21.089705078125" />
                  <Point X="-1.612886108398" Y="21.102005859375" />
                  <Point X="-1.643028198242" Y="21.109033203125" />
                  <Point X="-1.74069128418" Y="21.11543359375" />
                  <Point X="-1.9526171875" Y="21.12932421875" />
                  <Point X="-1.98341418457" Y="21.126291015625" />
                  <Point X="-2.014463134766" Y="21.11797265625" />
                  <Point X="-2.042657348633" Y="21.10519921875" />
                  <Point X="-2.124035888672" Y="21.05082421875" />
                  <Point X="-2.300624023438" Y="20.93283203125" />
                  <Point X="-2.312790283203" Y="20.92317578125" />
                  <Point X="-2.335102539062" Y="20.900537109375" />
                  <Point X="-2.480147460938" Y="20.71151171875" />
                  <Point X="-2.507229003906" Y="20.728279296875" />
                  <Point X="-2.801725341797" Y="20.910623046875" />
                  <Point X="-2.925023681641" Y="21.00555859375" />
                  <Point X="-3.104722167969" Y="21.143919921875" />
                  <Point X="-2.658601318359" Y="21.916625" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858886719" Y="22.35234765625" />
                  <Point X="-2.406587890625" Y="22.383875" />
                  <Point X="-2.405575683594" Y="22.414810546875" />
                  <Point X="-2.414561523438" Y="22.4444296875" />
                  <Point X="-2.428779541016" Y="22.4732578125" />
                  <Point X="-2.446807373047" Y="22.4984140625" />
                  <Point X="-2.464156005859" Y="22.51576171875" />
                  <Point X="-2.489312011719" Y="22.533787109375" />
                  <Point X="-2.518140869141" Y="22.54800390625" />
                  <Point X="-2.547759277344" Y="22.55698828125" />
                  <Point X="-2.578693847656" Y="22.555974609375" />
                  <Point X="-2.610219238281" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.521828125" Y="22.029205078125" />
                  <Point X="-3.818022705078" Y="21.858197265625" />
                  <Point X="-3.850204833984" Y="21.900478515625" />
                  <Point X="-4.082864013672" Y="22.20614453125" />
                  <Point X="-4.171265625" Y="22.35437890625" />
                  <Point X="-4.306143066406" Y="22.580548828125" />
                  <Point X="-3.516848388672" Y="23.1861953125" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.083064941406" Y="23.5241171875" />
                  <Point X="-3.063553466797" Y="23.555642578125" />
                  <Point X="-3.054409423828" Y="23.580095703125" />
                  <Point X="-3.051426269531" Y="23.589548828125" />
                  <Point X="-3.046152099609" Y="23.609912109375" />
                  <Point X="-3.042037353516" Y="23.64139453125" />
                  <Point X="-3.042736083984" Y="23.66426171875" />
                  <Point X="-3.048881835938" Y="23.686298828125" />
                  <Point X="-3.060884765625" Y="23.715279296875" />
                  <Point X="-3.073869873047" Y="23.737513671875" />
                  <Point X="-3.092348388672" Y="23.755443359375" />
                  <Point X="-3.113209960938" Y="23.770794921875" />
                  <Point X="-3.121335205078" Y="23.776154296875" />
                  <Point X="-3.139463623047" Y="23.786822265625" />
                  <Point X="-3.168722900391" Y="23.79804296875" />
                  <Point X="-3.200607666016" Y="23.8045234375" />
                  <Point X="-3.231929199219" Y="23.805615234375" />
                  <Point X="-4.346184570313" Y="23.658921875" />
                  <Point X="-4.732102050781" Y="23.608115234375" />
                  <Point X="-4.743083984375" Y="23.651109375" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.857467285156" Y="24.1708828125" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-4.000945800781" Y="24.654171875" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.511893798828" Y="24.78799609375" />
                  <Point X="-3.487571044922" Y="24.80129296875" />
                  <Point X="-3.478974365234" Y="24.80660546875" />
                  <Point X="-3.459976318359" Y="24.819791015625" />
                  <Point X="-3.436020507812" Y="24.841318359375" />
                  <Point X="-3.415003173828" Y="24.8719375" />
                  <Point X="-3.404659423828" Y="24.896078125" />
                  <Point X="-3.401251464844" Y="24.90533203125" />
                  <Point X="-3.394918701172" Y="24.925734375" />
                  <Point X="-3.389474609375" Y="24.9544765625" />
                  <Point X="-3.390489501953" Y="24.988529296875" />
                  <Point X="-3.395654785156" Y="25.01275390625" />
                  <Point X="-3.397835449219" Y="25.021103515625" />
                  <Point X="-3.404168212891" Y="25.0415078125" />
                  <Point X="-3.417484130859" Y="25.070830078125" />
                  <Point X="-3.440581542969" Y="25.10028515625" />
                  <Point X="-3.461155029297" Y="25.11787890625" />
                  <Point X="-3.468731201172" Y="25.123724609375" />
                  <Point X="-3.487729248047" Y="25.13691015625" />
                  <Point X="-3.501922851562" Y="25.145046875" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.548572265625" Y="25.43000390625" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.883131347656" Y="25.58066796875" />
                  <Point X="-4.82448828125" Y="25.97697265625" />
                  <Point X="-4.777404785156" Y="26.150724609375" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.101189941406" Y="26.34396484375" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744984863281" Y="26.299341796875" />
                  <Point X="-3.723423583984" Y="26.301228515625" />
                  <Point X="-3.703139160156" Y="26.305263671875" />
                  <Point X="-3.68376171875" Y="26.311373046875" />
                  <Point X="-3.641713134766" Y="26.324630859375" />
                  <Point X="-3.622784179688" Y="26.332958984375" />
                  <Point X="-3.604040039063" Y="26.343779296875" />
                  <Point X="-3.587355224609" Y="26.35601171875" />
                  <Point X="-3.573714355469" Y="26.37156640625" />
                  <Point X="-3.561299804688" Y="26.389296875" />
                  <Point X="-3.5513515625" Y="26.4074296875" />
                  <Point X="-3.543576171875" Y="26.426201171875" />
                  <Point X="-3.526704101563" Y="26.46693359375" />
                  <Point X="-3.520915771484" Y="26.48679296875" />
                  <Point X="-3.517157226562" Y="26.508109375" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951171875" Y="26.549193359375" />
                  <Point X="-3.524552978516" Y="26.570099609375" />
                  <Point X="-3.53205078125" Y="26.58937890625" />
                  <Point X="-3.541432617188" Y="26.607400390625" />
                  <Point X="-3.561790527344" Y="26.6465078125" />
                  <Point X="-3.573281982422" Y="26.66370703125" />
                  <Point X="-3.587194580078" Y="26.680287109375" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-4.184741699219" Y="27.141640625" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.30901953125" Y="27.34326953125" />
                  <Point X="-4.081156738281" Y="27.73365234375" />
                  <Point X="-3.956433837891" Y="27.89396875" />
                  <Point X="-3.75050390625" Y="28.158662109375" />
                  <Point X="-3.412726318359" Y="27.963646484375" />
                  <Point X="-3.206656982422" Y="27.844671875" />
                  <Point X="-3.187728759766" Y="27.836341796875" />
                  <Point X="-3.167086181641" Y="27.82983203125" />
                  <Point X="-3.146794921875" Y="27.825794921875" />
                  <Point X="-3.119807373047" Y="27.82343359375" />
                  <Point X="-3.061245605469" Y="27.818310546875" />
                  <Point X="-3.040560302734" Y="27.81876171875" />
                  <Point X="-3.019102050781" Y="27.821587890625" />
                  <Point X="-2.999012695312" Y="27.826505859375" />
                  <Point X="-2.980463623047" Y="27.83565234375" />
                  <Point X="-2.962209716797" Y="27.84728125" />
                  <Point X="-2.946077392578" Y="27.86023046875" />
                  <Point X="-2.926921386719" Y="27.87938671875" />
                  <Point X="-2.885353759766" Y="27.920953125" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.845796875" Y="28.063107421875" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-3.127965332031" Y="28.628697265625" />
                  <Point X="-3.183333251953" Y="28.724595703125" />
                  <Point X="-3.097492431641" Y="28.79041015625" />
                  <Point X="-2.700629150391" Y="29.094681640625" />
                  <Point X="-2.504198242188" Y="29.203814453125" />
                  <Point X="-2.167036132812" Y="29.391134765625" />
                  <Point X="-2.106279541016" Y="29.311955078125" />
                  <Point X="-2.04319543457" Y="29.2297421875" />
                  <Point X="-2.028892456055" Y="29.21480078125" />
                  <Point X="-2.012312988281" Y="29.200888671875" />
                  <Point X="-1.995114868164" Y="29.189396484375" />
                  <Point X="-1.965077880859" Y="29.173759765625" />
                  <Point X="-1.899898681641" Y="29.139828125" />
                  <Point X="-1.880625244141" Y="29.13233203125" />
                  <Point X="-1.859718017578" Y="29.126728515625" />
                  <Point X="-1.839268554688" Y="29.123580078125" />
                  <Point X="-1.818622436523" Y="29.12493359375" />
                  <Point X="-1.797306762695" Y="29.128693359375" />
                  <Point X="-1.777453491211" Y="29.134482421875" />
                  <Point X="-1.74616796875" Y="29.14744140625" />
                  <Point X="-1.678279296875" Y="29.1755625" />
                  <Point X="-1.660145141602" Y="29.185509765625" />
                  <Point X="-1.642415771484" Y="29.197923828125" />
                  <Point X="-1.626863647461" Y="29.2115625" />
                  <Point X="-1.614632324219" Y="29.228244140625" />
                  <Point X="-1.603810546875" Y="29.24698828125" />
                  <Point X="-1.59548046875" Y="29.265919921875" />
                  <Point X="-1.585297607422" Y="29.29821484375" />
                  <Point X="-1.563201171875" Y="29.368296875" />
                  <Point X="-1.559165527344" Y="29.3885859375" />
                  <Point X="-1.557279174805" Y="29.410146484375" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.584201660156" Y="29.631896484375" />
                  <Point X="-1.463404296875" Y="29.665763671875" />
                  <Point X="-0.949623596191" Y="29.80980859375" />
                  <Point X="-0.711511657715" Y="29.837677734375" />
                  <Point X="-0.294711334229" Y="29.886458984375" />
                  <Point X="-0.194864471436" Y="29.51382421875" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113983154" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155906677" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425941467" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.278498779297" Y="29.78000390625" />
                  <Point X="0.307419433594" Y="29.8879375" />
                  <Point X="0.395440093994" Y="29.87871875" />
                  <Point X="0.844044555664" Y="29.83173828125" />
                  <Point X="1.041052001953" Y="29.784173828125" />
                  <Point X="1.481023803711" Y="29.677951171875" />
                  <Point X="1.608247314453" Y="29.631806640625" />
                  <Point X="1.894647216797" Y="29.527927734375" />
                  <Point X="2.018646484375" Y="29.4699375" />
                  <Point X="2.294555419922" Y="29.34090234375" />
                  <Point X="2.414395751953" Y="29.271083984375" />
                  <Point X="2.680975341797" Y="29.115775390625" />
                  <Point X="2.793958251953" Y="29.035427734375" />
                  <Point X="2.943259521484" Y="28.92925390625" />
                  <Point X="2.419390625" Y="28.021884765625" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133076660156" Y="27.5160546875" />
                  <Point X="2.126303710938" Y="27.4907265625" />
                  <Point X="2.111606933594" Y="27.435767578125" />
                  <Point X="2.108619384766" Y="27.417931640625" />
                  <Point X="2.107728027344" Y="27.380951171875" />
                  <Point X="2.110368896484" Y="27.35905078125" />
                  <Point X="2.116099365234" Y="27.31152734375" />
                  <Point X="2.121442871094" Y="27.2896015625" />
                  <Point X="2.129709228516" Y="27.267513671875" />
                  <Point X="2.140069580078" Y="27.24747265625" />
                  <Point X="2.15362109375" Y="27.2275" />
                  <Point X="2.18302734375" Y="27.1841640625" />
                  <Point X="2.194462646484" Y="27.17033203125" />
                  <Point X="2.221594970703" Y="27.14559375" />
                  <Point X="2.241566162109" Y="27.132041015625" />
                  <Point X="2.284903564453" Y="27.102634765625" />
                  <Point X="2.304954101562" Y="27.09226953125" />
                  <Point X="2.327042724609" Y="27.08400390625" />
                  <Point X="2.348965332031" Y="27.078662109375" />
                  <Point X="2.370866210938" Y="27.076021484375" />
                  <Point X="2.418390136719" Y="27.070291015625" />
                  <Point X="2.436468994141" Y="27.06984375" />
                  <Point X="2.473208740234" Y="27.074169921875" />
                  <Point X="2.498535888672" Y="27.080943359375" />
                  <Point X="2.553495117188" Y="27.095640625" />
                  <Point X="2.565288085938" Y="27.099640625" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.61012890625" Y="27.69996484375" />
                  <Point X="3.967326416016" Y="27.90619140625" />
                  <Point X="4.123268554688" Y="27.689466796875" />
                  <Point X="4.186256835938" Y="27.585376953125" />
                  <Point X="4.262198242188" Y="27.4598828125" />
                  <Point X="3.588646728516" Y="26.943048828125" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.221422363281" Y="26.66023828125" />
                  <Point X="3.203974609375" Y="26.641626953125" />
                  <Point X="3.185746582031" Y="26.61784765625" />
                  <Point X="3.146192382812" Y="26.56624609375" />
                  <Point X="3.136606689453" Y="26.550912109375" />
                  <Point X="3.121629882812" Y="26.5170859375" />
                  <Point X="3.11483984375" Y="26.492806640625" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.09665234375" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739257812" Y="26.371767578125" />
                  <Point X="3.103312988281" Y="26.34475390625" />
                  <Point X="3.115408203125" Y="26.286134765625" />
                  <Point X="3.120679443359" Y="26.268978515625" />
                  <Point X="3.136282714844" Y="26.23573828125" />
                  <Point X="3.151442871094" Y="26.2126953125" />
                  <Point X="3.184340332031" Y="26.162693359375" />
                  <Point X="3.198896972656" Y="26.1454453125" />
                  <Point X="3.216140380859" Y="26.129357421875" />
                  <Point X="3.234345703125" Y="26.11603515625" />
                  <Point X="3.256314941406" Y="26.10366796875" />
                  <Point X="3.303987792969" Y="26.07683203125" />
                  <Point X="3.320521728516" Y="26.069501953125" />
                  <Point X="3.356119140625" Y="26.0594375" />
                  <Point X="3.385822998047" Y="26.05551171875" />
                  <Point X="3.450279785156" Y="26.046994140625" />
                  <Point X="3.462698730469" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="4.458650390625" Y="26.17474609375" />
                  <Point X="4.776839355469" Y="26.21663671875" />
                  <Point X="4.778020996094" Y="26.211783203125" />
                  <Point X="4.845936035156" Y="25.9328125" />
                  <Point X="4.865784667969" Y="25.805326171875" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.126698242188" Y="25.439482421875" />
                  <Point X="3.716579833984" Y="25.32958984375" />
                  <Point X="3.704787841797" Y="25.325583984375" />
                  <Point X="3.681548828125" Y="25.315068359375" />
                  <Point X="3.652365478516" Y="25.298201171875" />
                  <Point X="3.589038818359" Y="25.26159765625" />
                  <Point X="3.574312988281" Y="25.25109765625" />
                  <Point X="3.547530029297" Y="25.225576171875" />
                  <Point X="3.530020019531" Y="25.203263671875" />
                  <Point X="3.492024169922" Y="25.15484765625" />
                  <Point X="3.480300292969" Y="25.135568359375" />
                  <Point X="3.470526367188" Y="25.114103515625" />
                  <Point X="3.463680664062" Y="25.092603515625" />
                  <Point X="3.457843994141" Y="25.062126953125" />
                  <Point X="3.445178466797" Y="24.9959921875" />
                  <Point X="3.443482910156" Y="24.97812109375" />
                  <Point X="3.445178710938" Y="24.9414453125" />
                  <Point X="3.451015380859" Y="24.91096875" />
                  <Point X="3.463680908203" Y="24.8448359375" />
                  <Point X="3.470526367188" Y="24.823337890625" />
                  <Point X="3.480300048828" Y="24.801873046875" />
                  <Point X="3.492023925781" Y="24.782591796875" />
                  <Point X="3.509533691406" Y="24.760279296875" />
                  <Point X="3.547529785156" Y="24.71186328125" />
                  <Point X="3.559998291016" Y="24.698763671875" />
                  <Point X="3.589037841797" Y="24.675841796875" />
                  <Point X="3.618221191406" Y="24.658974609375" />
                  <Point X="3.681547851562" Y="24.622369140625" />
                  <Point X="3.692709716797" Y="24.616859375" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="4.606523925781" Y="24.369388671875" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.855022460938" Y="24.051271484375" />
                  <Point X="4.829593261719" Y="23.939837890625" />
                  <Point X="4.801174316406" Y="23.81530078125" />
                  <Point X="3.900004150391" Y="23.93394140625" />
                  <Point X="3.424382080078" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374658935547" Y="23.994490234375" />
                  <Point X="3.317382568359" Y="23.982041015625" />
                  <Point X="3.193094726563" Y="23.95502734375" />
                  <Point X="3.163974365234" Y="23.94340234375" />
                  <Point X="3.136147460938" Y="23.926509765625" />
                  <Point X="3.112397705078" Y="23.906041015625" />
                  <Point X="3.077777587891" Y="23.864404296875" />
                  <Point X="3.002653564453" Y="23.774052734375" />
                  <Point X="2.987932861328" Y="23.749669921875" />
                  <Point X="2.976589599609" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.964795654297" Y="23.640712890625" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347412109" Y="23.49243359375" />
                  <Point X="2.964079101562" Y="23.4608125" />
                  <Point X="2.976450683594" Y="23.432001953125" />
                  <Point X="3.008148193359" Y="23.38269921875" />
                  <Point X="3.076930908203" Y="23.275712890625" />
                  <Point X="3.086930908203" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="3.936501708984" Y="22.605375" />
                  <Point X="4.213122558594" Y="22.3931171875" />
                  <Point X="4.124811035156" Y="22.25021484375" />
                  <Point X="4.072220214844" Y="22.1754921875" />
                  <Point X="4.028980224609" Y="22.1140546875" />
                  <Point X="3.224560302734" Y="22.578486328125" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.786129394531" Y="22.829986328125" />
                  <Point X="2.754223876953" Y="22.840173828125" />
                  <Point X="2.686055908203" Y="22.852484375" />
                  <Point X="2.538133544922" Y="22.87919921875" />
                  <Point X="2.506783935547" Y="22.879603515625" />
                  <Point X="2.474611328125" Y="22.874646484375" />
                  <Point X="2.444833251953" Y="22.864822265625" />
                  <Point X="2.388202392578" Y="22.835017578125" />
                  <Point X="2.265315185547" Y="22.77034375" />
                  <Point X="2.242386230469" Y="22.753451171875" />
                  <Point X="2.221426269531" Y="22.7324921875" />
                  <Point X="2.204531738281" Y="22.709560546875" />
                  <Point X="2.174727294922" Y="22.6529296875" />
                  <Point X="2.110052734375" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675537109" Y="22.4367421875" />
                  <Point X="2.107986572266" Y="22.36857421875" />
                  <Point X="2.134701171875" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.682525878906" Y="21.2547109375" />
                  <Point X="2.861283203125" Y="20.94509375" />
                  <Point X="2.78184375" Y="20.888353515625" />
                  <Point X="2.723051757812" Y="20.850296875" />
                  <Point X="2.701764648438" Y="20.836517578125" />
                  <Point X="2.081796386719" Y="21.6444765625" />
                  <Point X="1.758546020508" Y="22.065744140625" />
                  <Point X="1.747507446289" Y="22.07781640625" />
                  <Point X="1.721922973633" Y="22.099443359375" />
                  <Point X="1.654690917969" Y="22.142666015625" />
                  <Point X="1.508799560547" Y="22.2364609375" />
                  <Point X="1.479986450195" Y="22.248833984375" />
                  <Point X="1.448365478516" Y="22.256564453125" />
                  <Point X="1.417100708008" Y="22.258880859375" />
                  <Point X="1.343570800781" Y="22.252115234375" />
                  <Point X="1.184013916016" Y="22.23743359375" />
                  <Point X="1.156362792969" Y="22.2306015625" />
                  <Point X="1.128977050781" Y="22.2192578125" />
                  <Point X="1.104595336914" Y="22.2045390625" />
                  <Point X="1.047817626953" Y="22.157330078125" />
                  <Point X="0.92461151123" Y="22.05488671875" />
                  <Point X="0.90414074707" Y="22.031134765625" />
                  <Point X="0.887248779297" Y="22.00330859375" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.858648132324" Y="21.8960859375" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.970140441895" Y="20.5344921875" />
                  <Point X="1.022065368652" Y="20.140083984375" />
                  <Point X="0.975680480957" Y="20.12991796875" />
                  <Point X="0.92931628418" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058415405273" Y="20.247361328125" />
                  <Point X="-1.141245849609" Y="20.268673828125" />
                  <Point X="-1.135987426758" Y="20.308615234375" />
                  <Point X="-1.120775634766" Y="20.424158203125" />
                  <Point X="-1.120077514648" Y="20.4318984375" />
                  <Point X="-1.119451782227" Y="20.45896484375" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123334106445" Y="20.502306640625" />
                  <Point X="-1.142428222656" Y="20.598298828125" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188124511719" Y="20.821529296875" />
                  <Point X="-1.199026245117" Y="20.85049609375" />
                  <Point X="-1.205665039062" Y="20.864533203125" />
                  <Point X="-1.221736816406" Y="20.89237109375" />
                  <Point X="-1.230573730469" Y="20.905138671875" />
                  <Point X="-1.250209106445" Y="20.929064453125" />
                  <Point X="-1.26100769043" Y="20.94022265625" />
                  <Point X="-1.334592285156" Y="21.00475390625" />
                  <Point X="-1.494268554688" Y="21.144787109375" />
                  <Point X="-1.506739746094" Y="21.15403515625" />
                  <Point X="-1.533022827148" Y="21.17037890625" />
                  <Point X="-1.546834472656" Y="21.177474609375" />
                  <Point X="-1.576531494141" Y="21.189775390625" />
                  <Point X="-1.591316040039" Y="21.194525390625" />
                  <Point X="-1.621458251953" Y="21.201552734375" />
                  <Point X="-1.636815673828" Y="21.203830078125" />
                  <Point X="-1.734478759766" Y="21.21023046875" />
                  <Point X="-1.946404541016" Y="21.22412109375" />
                  <Point X="-1.961928710938" Y="21.2238671875" />
                  <Point X="-1.992725708008" Y="21.220833984375" />
                  <Point X="-2.007998779297" Y="21.2180546875" />
                  <Point X="-2.039047729492" Y="21.209736328125" />
                  <Point X="-2.053667236328" Y="21.204505859375" />
                  <Point X="-2.081861572266" Y="21.191732421875" />
                  <Point X="-2.095436279297" Y="21.184189453125" />
                  <Point X="-2.176814697266" Y="21.129814453125" />
                  <Point X="-2.353402832031" Y="21.011822265625" />
                  <Point X="-2.359683349609" Y="21.0072421875" />
                  <Point X="-2.380451416016" Y="20.989861328125" />
                  <Point X="-2.402763671875" Y="20.96722265625" />
                  <Point X="-2.410470947266" Y="20.958369140625" />
                  <Point X="-2.503201171875" Y="20.837521484375" />
                  <Point X="-2.747596435547" Y="20.98884375" />
                  <Point X="-2.86706640625" Y="21.08083203125" />
                  <Point X="-2.980862548828" Y="21.168451171875" />
                  <Point X="-2.576328857422" Y="21.869125" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334850097656" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684082031" Y="22.333814453125" />
                  <Point X="-2.313413085938" Y="22.365341796875" />
                  <Point X="-2.311638671875" Y="22.380767578125" />
                  <Point X="-2.310626464844" Y="22.411703125" />
                  <Point X="-2.314667236328" Y="22.442390625" />
                  <Point X="-2.323653076172" Y="22.472009765625" />
                  <Point X="-2.329360351562" Y="22.486451171875" />
                  <Point X="-2.343578369141" Y="22.515279296875" />
                  <Point X="-2.351560791016" Y="22.528595703125" />
                  <Point X="-2.369588623047" Y="22.553751953125" />
                  <Point X="-2.379634033203" Y="22.565591796875" />
                  <Point X="-2.396982666016" Y="22.582939453125" />
                  <Point X="-2.408822998047" Y="22.592984375" />
                  <Point X="-2.433979003906" Y="22.611009765625" />
                  <Point X="-2.447294677734" Y="22.618990234375" />
                  <Point X="-2.476123535156" Y="22.63320703125" />
                  <Point X="-2.490564697266" Y="22.6389140625" />
                  <Point X="-2.520183105469" Y="22.6478984375" />
                  <Point X="-2.550870605469" Y="22.6519375" />
                  <Point X="-2.581805175781" Y="22.650923828125" />
                  <Point X="-2.597229492188" Y="22.6491484375" />
                  <Point X="-2.628754882813" Y="22.642876953125" />
                  <Point X="-2.643685546875" Y="22.63861328125" />
                  <Point X="-2.672649902344" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.569328125" Y="22.1114765625" />
                  <Point X="-3.793087158203" Y="21.9822890625" />
                  <Point X="-4.004020019531" Y="22.259412109375" />
                  <Point X="-4.089673095703" Y="22.403037109375" />
                  <Point X="-4.181266113281" Y="22.556625" />
                  <Point X="-3.459016113281" Y="23.110826171875" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.039159423828" Y="23.433931640625" />
                  <Point X="-3.016269775391" Y="23.456564453125" />
                  <Point X="-3.002284912109" Y="23.47412109375" />
                  <Point X="-2.9827734375" Y="23.505646484375" />
                  <Point X="-2.974571289062" Y="23.522369140625" />
                  <Point X="-2.965427246094" Y="23.546822265625" />
                  <Point X="-2.9594609375" Y="23.565728515625" />
                  <Point X="-2.954186767578" Y="23.586091796875" />
                  <Point X="-2.951953369141" Y="23.597599609375" />
                  <Point X="-2.947838623047" Y="23.62908203125" />
                  <Point X="-2.947081787109" Y="23.644296875" />
                  <Point X="-2.947780517578" Y="23.6671640625" />
                  <Point X="-2.951228027344" Y="23.68978125" />
                  <Point X="-2.957373779297" Y="23.711818359375" />
                  <Point X="-2.961112060547" Y="23.722650390625" />
                  <Point X="-2.973114990234" Y="23.751630859375" />
                  <Point X="-2.978849853516" Y="23.763189453125" />
                  <Point X="-2.991834960938" Y="23.785423828125" />
                  <Point X="-3.00771484375" Y="23.805693359375" />
                  <Point X="-3.026193359375" Y="23.823623046875" />
                  <Point X="-3.036042236328" Y="23.831958984375" />
                  <Point X="-3.056903808594" Y="23.847310546875" />
                  <Point X="-3.073154296875" Y="23.858029296875" />
                  <Point X="-3.091282714844" Y="23.868697265625" />
                  <Point X="-3.105447509766" Y="23.8755234375" />
                  <Point X="-3.134706787109" Y="23.886744140625" />
                  <Point X="-3.149801269531" Y="23.891138671875" />
                  <Point X="-3.181686035156" Y="23.897619140625" />
                  <Point X="-3.197298095703" Y="23.89946484375" />
                  <Point X="-3.228619628906" Y="23.900556640625" />
                  <Point X="-3.244329101562" Y="23.899802734375" />
                  <Point X="-4.358584472656" Y="23.753109375" />
                  <Point X="-4.660920410156" Y="23.713306640625" />
                  <Point X="-4.740762695312" Y="24.025884765625" />
                  <Point X="-4.763424316406" Y="24.184333984375" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-3.976357910156" Y="24.562408203125" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.497545410156" Y="24.691404296875" />
                  <Point X="-3.476563232422" Y="24.699810546875" />
                  <Point X="-3.466323730469" Y="24.704638671875" />
                  <Point X="-3.442000976562" Y="24.717935546875" />
                  <Point X="-3.424807617188" Y="24.728560546875" />
                  <Point X="-3.405809570312" Y="24.74174609375" />
                  <Point X="-3.396478271484" Y="24.74912890625" />
                  <Point X="-3.372522460938" Y="24.77065625" />
                  <Point X="-3.357696777344" Y="24.787556640625" />
                  <Point X="-3.336679443359" Y="24.81817578125" />
                  <Point X="-3.327681396484" Y="24.834521484375" />
                  <Point X="-3.317337646484" Y="24.858662109375" />
                  <Point X="-3.310521728516" Y="24.877169921875" />
                  <Point X="-3.304188964844" Y="24.897572265625" />
                  <Point X="-3.301578369141" Y="24.9080546875" />
                  <Point X="-3.296134277344" Y="24.936796875" />
                  <Point X="-3.294516845703" Y="24.957306640625" />
                  <Point X="-3.295531738281" Y="24.991359375" />
                  <Point X="-3.297578125" Y="25.00833984375" />
                  <Point X="-3.302743408203" Y="25.032564453125" />
                  <Point X="-3.307104736328" Y="25.049263671875" />
                  <Point X="-3.3134375" Y="25.06966796875" />
                  <Point X="-3.317669677734" Y="25.0807890625" />
                  <Point X="-3.330985595703" Y="25.110111328125" />
                  <Point X="-3.342727294922" Y="25.129451171875" />
                  <Point X="-3.365824707031" Y="25.15890625" />
                  <Point X="-3.378838623047" Y="25.172484375" />
                  <Point X="-3.399412109375" Y="25.190078125" />
                  <Point X="-3.414564453125" Y="25.20176953125" />
                  <Point X="-3.4335625" Y="25.214955078125" />
                  <Point X="-3.440481933594" Y="25.219328125" />
                  <Point X="-3.465612548828" Y="25.232833984375" />
                  <Point X="-3.496565673828" Y="25.24563671875" />
                  <Point X="-3.508288330078" Y="25.24961328125" />
                  <Point X="-4.523984375" Y="25.521767578125" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.731331542969" Y="25.95752734375" />
                  <Point X="-4.685711914062" Y="26.125876953125" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-4.11358984375" Y="26.24977734375" />
                  <Point X="-3.77806640625" Y="26.20560546875" />
                  <Point X="-3.76773828125" Y="26.20481640625" />
                  <Point X="-3.747056884766" Y="26.204365234375" />
                  <Point X="-3.736703613281" Y="26.204703125" />
                  <Point X="-3.715142333984" Y="26.20658984375" />
                  <Point X="-3.704888427734" Y="26.2080546875" />
                  <Point X="-3.684604003906" Y="26.21208984375" />
                  <Point X="-3.674573486328" Y="26.21466015625" />
                  <Point X="-3.655196044922" Y="26.22076953125" />
                  <Point X="-3.613147460938" Y="26.23402734375" />
                  <Point X="-3.603455322266" Y="26.23767578125" />
                  <Point X="-3.584526367188" Y="26.24600390625" />
                  <Point X="-3.575289550781" Y="26.25068359375" />
                  <Point X="-3.556545410156" Y="26.26150390625" />
                  <Point X="-3.547869873047" Y="26.2671640625" />
                  <Point X="-3.531185058594" Y="26.279396484375" />
                  <Point X="-3.515929931641" Y="26.293375" />
                  <Point X="-3.5022890625" Y="26.3089296875" />
                  <Point X="-3.495894042969" Y="26.317078125" />
                  <Point X="-3.483479492188" Y="26.33480859375" />
                  <Point X="-3.478011230469" Y="26.3436015625" />
                  <Point X="-3.468062988281" Y="26.361734375" />
                  <Point X="-3.463583007813" Y="26.37107421875" />
                  <Point X="-3.455807617188" Y="26.389845703125" />
                  <Point X="-3.438935546875" Y="26.430578125" />
                  <Point X="-3.435499267578" Y="26.440349609375" />
                  <Point X="-3.4297109375" Y="26.460208984375" />
                  <Point X="-3.427358886719" Y="26.470296875" />
                  <Point X="-3.423600341797" Y="26.49161328125" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.5225390625" />
                  <Point X="-3.421910400391" Y="26.543201171875" />
                  <Point X="-3.425056884766" Y="26.56364453125" />
                  <Point X="-3.427188232422" Y="26.57378125" />
                  <Point X="-3.432790039062" Y="26.5946875" />
                  <Point X="-3.436012939453" Y="26.604533203125" />
                  <Point X="-3.443510742188" Y="26.6238125" />
                  <Point X="-3.447785644531" Y="26.63324609375" />
                  <Point X="-3.457167480469" Y="26.651267578125" />
                  <Point X="-3.477525390625" Y="26.690375" />
                  <Point X="-3.482799560547" Y="26.69928515625" />
                  <Point X="-3.494291015625" Y="26.716484375" />
                  <Point X="-3.500508300781" Y="26.7247734375" />
                  <Point X="-3.514420898438" Y="26.741353515625" />
                  <Point X="-3.521501708984" Y="26.748912109375" />
                  <Point X="-3.536442871094" Y="26.76321484375" />
                  <Point X="-3.544303222656" Y="26.769958984375" />
                  <Point X="-4.126909179688" Y="27.217009765625" />
                  <Point X="-4.227614257812" Y="27.294283203125" />
                  <Point X="-4.226973144531" Y="27.295380859375" />
                  <Point X="-4.002298339844" Y="27.680302734375" />
                  <Point X="-3.881452636719" Y="27.835634765625" />
                  <Point X="-3.726336914062" Y="28.035013671875" />
                  <Point X="-3.460226318359" Y="27.881375" />
                  <Point X="-3.254156982422" Y="27.762400390625" />
                  <Point X="-3.244923583984" Y="27.757720703125" />
                  <Point X="-3.225995361328" Y="27.749390625" />
                  <Point X="-3.216300537109" Y="27.745740234375" />
                  <Point X="-3.195657958984" Y="27.73923046875" />
                  <Point X="-3.185623779297" Y="27.736658203125" />
                  <Point X="-3.165332519531" Y="27.73262109375" />
                  <Point X="-3.155075439453" Y="27.73115625" />
                  <Point X="-3.128087890625" Y="27.728794921875" />
                  <Point X="-3.069526123047" Y="27.723671875" />
                  <Point X="-3.059174072266" Y="27.723333984375" />
                  <Point X="-3.038488769531" Y="27.72378515625" />
                  <Point X="-3.028155517578" Y="27.72457421875" />
                  <Point X="-3.006697265625" Y="27.727400390625" />
                  <Point X="-2.996512695312" Y="27.7293125" />
                  <Point X="-2.976423339844" Y="27.73423046875" />
                  <Point X="-2.956998535156" Y="27.74130078125" />
                  <Point X="-2.938449462891" Y="27.750447265625" />
                  <Point X="-2.929420410156" Y="27.755529296875" />
                  <Point X="-2.911166503906" Y="27.767158203125" />
                  <Point X="-2.902742431641" Y="27.7731953125" />
                  <Point X="-2.886610107422" Y="27.78614453125" />
                  <Point X="-2.878901855469" Y="27.793056640625" />
                  <Point X="-2.859745849609" Y="27.812212890625" />
                  <Point X="-2.818178222656" Y="27.853779296875" />
                  <Point X="-2.811268310547" Y="27.861486328125" />
                  <Point X="-2.798323486328" Y="27.87761328125" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.751158447266" Y="28.07138671875" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-3.045692871094" Y="28.676197265625" />
                  <Point X="-3.059387695312" Y="28.69991796875" />
                  <Point X="-3.039689697266" Y="28.71501953125" />
                  <Point X="-2.648384521484" Y="29.015029296875" />
                  <Point X="-2.458060791016" Y="29.12076953125" />
                  <Point X="-2.192523681641" Y="29.268296875" />
                  <Point X="-2.181648193359" Y="29.254123046875" />
                  <Point X="-2.118563964844" Y="29.17191015625" />
                  <Point X="-2.111820800781" Y="29.164048828125" />
                  <Point X="-2.097517822266" Y="29.149107421875" />
                  <Point X="-2.089958007813" Y="29.14202734375" />
                  <Point X="-2.073378662109" Y="29.128115234375" />
                  <Point X="-2.065094726563" Y="29.121900390625" />
                  <Point X="-2.047896484375" Y="29.110408203125" />
                  <Point X="-2.038981933594" Y="29.105130859375" />
                  <Point X="-2.008944946289" Y="29.089494140625" />
                  <Point X="-1.94376574707" Y="29.0555625" />
                  <Point X="-1.934334594727" Y="29.0512890625" />
                  <Point X="-1.915061157227" Y="29.04379296875" />
                  <Point X="-1.905218994141" Y="29.0405703125" />
                  <Point X="-1.884311767578" Y="29.034966796875" />
                  <Point X="-1.874174072266" Y="29.032833984375" />
                  <Point X="-1.853724487305" Y="29.029685546875" />
                  <Point X="-1.833053955078" Y="29.028783203125" />
                  <Point X="-1.812407836914" Y="29.03013671875" />
                  <Point X="-1.802120605469" Y="29.031376953125" />
                  <Point X="-1.780804931641" Y="29.03513671875" />
                  <Point X="-1.770713012695" Y="29.0374921875" />
                  <Point X="-1.750859863281" Y="29.04328125" />
                  <Point X="-1.741098388672" Y="29.04671484375" />
                  <Point X="-1.709812866211" Y="29.059673828125" />
                  <Point X="-1.641924072266" Y="29.087794921875" />
                  <Point X="-1.632590576172" Y="29.092271484375" />
                  <Point X="-1.614456420898" Y="29.10221875" />
                  <Point X="-1.605655883789" Y="29.107689453125" />
                  <Point X="-1.587926513672" Y="29.120103515625" />
                  <Point X="-1.579778320312" Y="29.126498046875" />
                  <Point X="-1.564226196289" Y="29.14013671875" />
                  <Point X="-1.550251098633" Y="29.155388671875" />
                  <Point X="-1.538019775391" Y="29.1720703125" />
                  <Point X="-1.532359619141" Y="29.180744140625" />
                  <Point X="-1.521537841797" Y="29.19948828125" />
                  <Point X="-1.516855834961" Y="29.2087265625" />
                  <Point X="-1.508525756836" Y="29.227658203125" />
                  <Point X="-1.504877563477" Y="29.2373515625" />
                  <Point X="-1.494694702148" Y="29.269646484375" />
                  <Point X="-1.472598144531" Y="29.339728515625" />
                  <Point X="-1.470026489258" Y="29.349763671875" />
                  <Point X="-1.465990844727" Y="29.370052734375" />
                  <Point X="-1.464527099609" Y="29.380306640625" />
                  <Point X="-1.46264074707" Y="29.4018671875" />
                  <Point X="-1.462301757812" Y="29.41221875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479265991211" Y="29.562654296875" />
                  <Point X="-1.437758666992" Y="29.574291015625" />
                  <Point X="-0.931164001465" Y="29.7163203125" />
                  <Point X="-0.700468078613" Y="29.743322265625" />
                  <Point X="-0.365222686768" Y="29.78255859375" />
                  <Point X="-0.286627441406" Y="29.489236328125" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451339722" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.37026171875" Y="29.755416015625" />
                  <Point X="0.378190460205" Y="29.785005859375" />
                  <Point X="0.385544464111" Y="29.784236328125" />
                  <Point X="0.827879577637" Y="29.737912109375" />
                  <Point X="1.018756347656" Y="29.691828125" />
                  <Point X="1.453591186523" Y="29.586845703125" />
                  <Point X="1.575855224609" Y="29.5425" />
                  <Point X="1.858249755859" Y="29.44007421875" />
                  <Point X="1.978401733398" Y="29.3838828125" />
                  <Point X="2.250429443359" Y="29.256662109375" />
                  <Point X="2.366573242188" Y="29.188998046875" />
                  <Point X="2.629430664063" Y="29.035857421875" />
                  <Point X="2.738901611328" Y="28.9580078125" />
                  <Point X="2.817779052734" Y="28.901916015625" />
                  <Point X="2.337118164062" Y="28.069384765625" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.06237109375" Y="27.5931015625" />
                  <Point X="2.053179931641" Y="27.57343359375" />
                  <Point X="2.044181884766" Y="27.549560546875" />
                  <Point X="2.041301269531" Y="27.540595703125" />
                  <Point X="2.034528320312" Y="27.515267578125" />
                  <Point X="2.019831665039" Y="27.46030859375" />
                  <Point X="2.017912231445" Y="27.4514609375" />
                  <Point X="2.013646972656" Y="27.420220703125" />
                  <Point X="2.012755615234" Y="27.383240234375" />
                  <Point X="2.013411254883" Y="27.369578125" />
                  <Point X="2.016052124023" Y="27.347677734375" />
                  <Point X="2.021782592773" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.029144287109" Y="27.267107421875" />
                  <Point X="2.032469604492" Y="27.256302734375" />
                  <Point X="2.040735961914" Y="27.23421484375" />
                  <Point X="2.045318847656" Y="27.22388671875" />
                  <Point X="2.055679199219" Y="27.203845703125" />
                  <Point X="2.061456787109" Y="27.1941328125" />
                  <Point X="2.075008300781" Y="27.17416015625" />
                  <Point X="2.104414550781" Y="27.13082421875" />
                  <Point X="2.109809082031" Y="27.1236328125" />
                  <Point X="2.130456054688" Y="27.100130859375" />
                  <Point X="2.157588378906" Y="27.075392578125" />
                  <Point X="2.16825" Y="27.066984375" />
                  <Point X="2.188221191406" Y="27.053431640625" />
                  <Point X="2.23155859375" Y="27.024025390625" />
                  <Point X="2.24127734375" Y="27.018244140625" />
                  <Point X="2.261327880859" Y="27.00787890625" />
                  <Point X="2.271659667969" Y="27.003294921875" />
                  <Point X="2.293748291016" Y="26.995029296875" />
                  <Point X="2.304552490234" Y="26.991705078125" />
                  <Point X="2.326475097656" Y="26.98636328125" />
                  <Point X="2.337593505859" Y="26.984345703125" />
                  <Point X="2.359494384766" Y="26.981705078125" />
                  <Point X="2.407018310547" Y="26.975974609375" />
                  <Point X="2.416040527344" Y="26.9753203125" />
                  <Point X="2.447578613281" Y="26.97549609375" />
                  <Point X="2.484318359375" Y="26.979822265625" />
                  <Point X="2.497752685547" Y="26.98239453125" />
                  <Point X="2.523079833984" Y="26.98916796875" />
                  <Point X="2.5780390625" Y="27.003865234375" />
                  <Point X="2.584010253906" Y="27.00567578125" />
                  <Point X="2.604412841797" Y="27.0130703125" />
                  <Point X="2.627658691406" Y="27.023576171875" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.65762890625" Y="27.617693359375" />
                  <Point X="3.940403808594" Y="27.780953125" />
                  <Point X="4.043952148438" Y="27.63704296875" />
                  <Point X="4.104979492188" Y="27.536193359375" />
                  <Point X="4.136883789062" Y="27.483470703125" />
                  <Point X="3.530814453125" Y="27.01841796875" />
                  <Point X="3.172951660156" Y="26.7438203125" />
                  <Point X="3.168133056641" Y="26.739865234375" />
                  <Point X="3.152115722656" Y="26.725212890625" />
                  <Point X="3.13466796875" Y="26.7066015625" />
                  <Point X="3.128577880859" Y="26.699421875" />
                  <Point X="3.110349853516" Y="26.675642578125" />
                  <Point X="3.070795654297" Y="26.624041015625" />
                  <Point X="3.065637207031" Y="26.616603515625" />
                  <Point X="3.049740234375" Y="26.589373046875" />
                  <Point X="3.034763427734" Y="26.555546875" />
                  <Point X="3.030140380859" Y="26.542671875" />
                  <Point X="3.023350341797" Y="26.518392578125" />
                  <Point X="3.008616455078" Y="26.46570703125" />
                  <Point X="3.006225341797" Y="26.454662109375" />
                  <Point X="3.002771728516" Y="26.43236328125" />
                  <Point X="3.001709228516" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077636719" Y="26.363755859375" />
                  <Point X="3.004698974609" Y="26.3525703125" />
                  <Point X="3.010272705078" Y="26.325556640625" />
                  <Point X="3.022367919922" Y="26.2669375" />
                  <Point X="3.024597900391" Y="26.258232421875" />
                  <Point X="3.034682617188" Y="26.228611328125" />
                  <Point X="3.050285888672" Y="26.19537109375" />
                  <Point X="3.056918701172" Y="26.1835234375" />
                  <Point X="3.072078857422" Y="26.16048046875" />
                  <Point X="3.104976318359" Y="26.110478515625" />
                  <Point X="3.111740234375" Y="26.101421875" />
                  <Point X="3.126296875" Y="26.084173828125" />
                  <Point X="3.134089599609" Y="26.075982421875" />
                  <Point X="3.151333007812" Y="26.05989453125" />
                  <Point X="3.160038330078" Y="26.05269140625" />
                  <Point X="3.178243652344" Y="26.039369140625" />
                  <Point X="3.187743652344" Y="26.03325" />
                  <Point X="3.209712890625" Y="26.0208828125" />
                  <Point X="3.257385742188" Y="25.994046875" />
                  <Point X="3.265485107422" Y="25.989984375" />
                  <Point X="3.294675537109" Y="25.9780859375" />
                  <Point X="3.330272949219" Y="25.968021484375" />
                  <Point X="3.343671875" Y="25.965255859375" />
                  <Point X="3.373375732422" Y="25.961330078125" />
                  <Point X="3.437832519531" Y="25.9528125" />
                  <Point X="3.444033203125" Y="25.95219921875" />
                  <Point X="3.465708984375" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="4.471050292969" Y="26.08055859375" />
                  <Point X="4.704704101562" Y="26.1113203125" />
                  <Point X="4.752683105469" Y="25.91423828125" />
                  <Point X="4.771915527344" Y="25.7907109375" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="4.102110351562" Y="25.53124609375" />
                  <Point X="3.691991943359" Y="25.421353515625" />
                  <Point X="3.686022460938" Y="25.419541015625" />
                  <Point X="3.665623535156" Y="25.412134765625" />
                  <Point X="3.642384521484" Y="25.401619140625" />
                  <Point X="3.634010498047" Y="25.397318359375" />
                  <Point X="3.604827148438" Y="25.380451171875" />
                  <Point X="3.541500488281" Y="25.34384765625" />
                  <Point X="3.533885498047" Y="25.338947265625" />
                  <Point X="3.508777099609" Y="25.319873046875" />
                  <Point X="3.481994140625" Y="25.2943515625" />
                  <Point X="3.472795166016" Y="25.284224609375" />
                  <Point X="3.45528515625" Y="25.261912109375" />
                  <Point X="3.417289306641" Y="25.21349609375" />
                  <Point X="3.410854003906" Y="25.20420703125" />
                  <Point X="3.399130126953" Y="25.184927734375" />
                  <Point X="3.393841552734" Y="25.1749375" />
                  <Point X="3.384067626953" Y="25.15347265625" />
                  <Point X="3.380004150391" Y="25.14292578125" />
                  <Point X="3.373158447266" Y="25.12142578125" />
                  <Point X="3.370376220703" Y="25.11047265625" />
                  <Point X="3.364539550781" Y="25.07999609375" />
                  <Point X="3.351874023438" Y="25.013861328125" />
                  <Point X="3.350603271484" Y="25.00496484375" />
                  <Point X="3.348584228516" Y="24.973732421875" />
                  <Point X="3.350280029297" Y="24.937056640625" />
                  <Point X="3.351874267578" Y="24.923576171875" />
                  <Point X="3.3577109375" Y="24.893099609375" />
                  <Point X="3.370376464844" Y="24.826966796875" />
                  <Point X="3.373159179688" Y="24.81601171875" />
                  <Point X="3.380004638672" Y="24.794513671875" />
                  <Point X="3.384067382812" Y="24.783970703125" />
                  <Point X="3.393841064453" Y="24.762505859375" />
                  <Point X="3.399127685547" Y="24.752515625" />
                  <Point X="3.4108515625" Y="24.733234375" />
                  <Point X="3.417288818359" Y="24.723943359375" />
                  <Point X="3.434798583984" Y="24.701630859375" />
                  <Point X="3.472794677734" Y="24.65321484375" />
                  <Point X="3.478717529297" Y="24.646365234375" />
                  <Point X="3.501138427734" Y="24.6241953125" />
                  <Point X="3.530177978516" Y="24.6012734375" />
                  <Point X="3.541499511719" Y="24.593591796875" />
                  <Point X="3.570682861328" Y="24.576724609375" />
                  <Point X="3.634009521484" Y="24.540119140625" />
                  <Point X="3.639497558594" Y="24.537181640625" />
                  <Point X="3.659156005859" Y="24.527982421875" />
                  <Point X="3.683026367188" Y="24.518970703125" />
                  <Point X="3.691992431641" Y="24.516083984375" />
                  <Point X="4.581936035156" Y="24.277625" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.76161328125" Y="24.0689453125" />
                  <Point X="4.736974121094" Y="23.96097265625" />
                  <Point X="4.727802246094" Y="23.92078125" />
                  <Point X="3.912404052734" Y="24.02812890625" />
                  <Point X="3.436781982422" Y="24.09074609375" />
                  <Point X="3.428623046875" Y="24.09146484375" />
                  <Point X="3.400097167969" Y="24.09195703125" />
                  <Point X="3.366720458984" Y="24.089158203125" />
                  <Point X="3.354481445312" Y="24.087322265625" />
                  <Point X="3.297205078125" Y="24.074873046875" />
                  <Point X="3.172917236328" Y="24.047859375" />
                  <Point X="3.157873046875" Y="24.043255859375" />
                  <Point X="3.128752685547" Y="24.031630859375" />
                  <Point X="3.114676513672" Y="24.024609375" />
                  <Point X="3.086849609375" Y="24.007716796875" />
                  <Point X="3.074127197266" Y="23.998470703125" />
                  <Point X="3.050377441406" Y="23.978001953125" />
                  <Point X="3.039350097656" Y="23.966779296875" />
                  <Point X="3.004729980469" Y="23.925142578125" />
                  <Point X="2.929605957031" Y="23.834791015625" />
                  <Point X="2.921325927734" Y="23.82315234375" />
                  <Point X="2.906605224609" Y="23.79876953125" />
                  <Point X="2.900164550781" Y="23.786025390625" />
                  <Point X="2.888821289062" Y="23.758640625" />
                  <Point X="2.88436328125" Y="23.745072265625" />
                  <Point X="2.87753125" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.8701953125" Y="23.64941796875" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607666016" Y="23.485408203125" />
                  <Point X="2.864065917969" Y="23.469869140625" />
                  <Point X="2.871797607422" Y="23.438248046875" />
                  <Point X="2.876786865234" Y="23.423328125" />
                  <Point X="2.889158447266" Y="23.394517578125" />
                  <Point X="2.896540771484" Y="23.380626953125" />
                  <Point X="2.92823828125" Y="23.33132421875" />
                  <Point X="2.997020996094" Y="23.224337890625" />
                  <Point X="3.001741455078" Y="23.2176484375" />
                  <Point X="3.019789306641" Y="23.1955546875" />
                  <Point X="3.043486816406" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="3.878669433594" Y="22.530005859375" />
                  <Point X="4.087170654297" Y="22.370017578125" />
                  <Point X="4.045491455078" Y="22.30257421875" />
                  <Point X="4.001273193359" Y="22.23974609375" />
                  <Point X="3.272060302734" Y="22.6607578125" />
                  <Point X="2.848454589844" Y="22.905326171875" />
                  <Point X="2.841191650391" Y="22.90911328125" />
                  <Point X="2.815025878906" Y="22.920484375" />
                  <Point X="2.783120361328" Y="22.930671875" />
                  <Point X="2.771106933594" Y="22.933662109375" />
                  <Point X="2.702938964844" Y="22.94597265625" />
                  <Point X="2.555016601562" Y="22.9726875" />
                  <Point X="2.539358642578" Y="22.97419140625" />
                  <Point X="2.508009033203" Y="22.974595703125" />
                  <Point X="2.492317382813" Y="22.97349609375" />
                  <Point X="2.460144775391" Y="22.9685390625" />
                  <Point X="2.444847412109" Y="22.96486328125" />
                  <Point X="2.415069335938" Y="22.9550390625" />
                  <Point X="2.400588623047" Y="22.948890625" />
                  <Point X="2.343957763672" Y="22.9190859375" />
                  <Point X="2.221070556641" Y="22.854412109375" />
                  <Point X="2.208966552734" Y="22.846828125" />
                  <Point X="2.186037597656" Y="22.829935546875" />
                  <Point X="2.175212646484" Y="22.820626953125" />
                  <Point X="2.154252685547" Y="22.79966796875" />
                  <Point X="2.144942138672" Y="22.78883984375" />
                  <Point X="2.128047607422" Y="22.765908203125" />
                  <Point X="2.120463623047" Y="22.7538046875" />
                  <Point X="2.090659179688" Y="22.697173828125" />
                  <Point X="2.025984741211" Y="22.574287109375" />
                  <Point X="2.01983605957" Y="22.55980859375" />
                  <Point X="2.010012329102" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.00068347168" Y="22.435517578125" />
                  <Point X="2.002187866211" Y="22.419859375" />
                  <Point X="2.014498901367" Y="22.35169140625" />
                  <Point X="2.041213500977" Y="22.203767578125" />
                  <Point X="2.043014648438" Y="22.195779296875" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.600253417969" Y="21.2072109375" />
                  <Point X="2.735892089844" Y="20.97227734375" />
                  <Point X="2.723752685547" Y="20.963916015625" />
                  <Point X="2.157165039062" Y="21.70230859375" />
                  <Point X="1.833914550781" Y="22.123576171875" />
                  <Point X="1.828655639648" Y="22.1298515625" />
                  <Point X="1.808836547852" Y="22.1503671875" />
                  <Point X="1.783252075195" Y="22.171994140625" />
                  <Point X="1.773296630859" Y="22.179353515625" />
                  <Point X="1.706064575195" Y="22.222576171875" />
                  <Point X="1.560173217773" Y="22.31637109375" />
                  <Point X="1.546284912109" Y="22.323751953125" />
                  <Point X="1.517471679688" Y="22.336125" />
                  <Point X="1.502547119141" Y="22.3411171875" />
                  <Point X="1.470926025391" Y="22.34884765625" />
                  <Point X="1.455384765625" Y="22.3513046875" />
                  <Point X="1.424119995117" Y="22.35362109375" />
                  <Point X="1.408396362305" Y="22.35348046875" />
                  <Point X="1.334866455078" Y="22.34671484375" />
                  <Point X="1.175309570312" Y="22.332033203125" />
                  <Point X="1.1612265625" Y="22.32966015625" />
                  <Point X="1.133575439453" Y="22.322828125" />
                  <Point X="1.120007324219" Y="22.318369140625" />
                  <Point X="1.092621582031" Y="22.307025390625" />
                  <Point X="1.079880126953" Y="22.300587890625" />
                  <Point X="1.055498413086" Y="22.285869140625" />
                  <Point X="1.043858154297" Y="22.277587890625" />
                  <Point X="0.987080505371" Y="22.23037890625" />
                  <Point X="0.863874328613" Y="22.127935546875" />
                  <Point X="0.852650024414" Y="22.11690625" />
                  <Point X="0.832179138184" Y="22.093154296875" />
                  <Point X="0.822932739258" Y="22.080431640625" />
                  <Point X="0.806040710449" Y="22.05260546875" />
                  <Point X="0.799019287109" Y="22.038529296875" />
                  <Point X="0.787394775391" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.765815612793" Y="21.916263671875" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.833091308594" Y="20.84766015625" />
                  <Point X="0.781432617188" Y="21.040453125" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605834961" Y="21.519876953125" />
                  <Point X="0.642146484375" Y="21.546416015625" />
                  <Point X="0.626787841797" Y="21.57618359375" />
                  <Point X="0.620406860352" Y="21.58679296875" />
                  <Point X="0.568757263184" Y="21.661208984375" />
                  <Point X="0.456679229736" Y="21.822693359375" />
                  <Point X="0.446668762207" Y="21.834830078125" />
                  <Point X="0.424782104492" Y="21.857287109375" />
                  <Point X="0.412905792236" Y="21.867607421875" />
                  <Point X="0.386650512695" Y="21.886849609375" />
                  <Point X="0.373235198975" Y="21.89506640625" />
                  <Point X="0.345236297607" Y="21.909171875" />
                  <Point X="0.330652679443" Y="21.915060546875" />
                  <Point X="0.250727874756" Y="21.939865234375" />
                  <Point X="0.07729372406" Y="21.993693359375" />
                  <Point X="0.063380096436" Y="21.996888671875" />
                  <Point X="0.03522794342" Y="22.001158203125" />
                  <Point X="0.020989425659" Y="22.002232421875" />
                  <Point X="-0.008651926041" Y="22.002234375" />
                  <Point X="-0.022895647049" Y="22.001162109375" />
                  <Point X="-0.051062362671" Y="21.996892578125" />
                  <Point X="-0.064985206604" Y="21.9936953125" />
                  <Point X="-0.144910003662" Y="21.968888671875" />
                  <Point X="-0.318344299316" Y="21.9150625" />
                  <Point X="-0.332928955078" Y="21.909171875" />
                  <Point X="-0.360929229736" Y="21.895064453125" />
                  <Point X="-0.374344970703" Y="21.88684765625" />
                  <Point X="-0.400600402832" Y="21.867603515625" />
                  <Point X="-0.412477294922" Y="21.85728125" />
                  <Point X="-0.43436126709" Y="21.83482421875" />
                  <Point X="-0.44436819458" Y="21.822689453125" />
                  <Point X="-0.496017913818" Y="21.748271484375" />
                  <Point X="-0.608095947266" Y="21.5867890625" />
                  <Point X="-0.612468994141" Y="21.57987109375" />
                  <Point X="-0.625977233887" Y="21.554736328125" />
                  <Point X="-0.638778076172" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.916966552734" Y="20.48868359375" />
                  <Point X="-0.985424987793" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.588260529853" Y="26.31226910758" />
                  <Point X="-4.692993733857" Y="25.567053638877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.49406968652" Y="26.299868611596" />
                  <Point X="-4.600541666933" Y="25.542281105878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.768068210424" Y="24.350267810626" />
                  <Point X="-4.777528898718" Y="24.282951515591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.399878843187" Y="26.287468115613" />
                  <Point X="-4.508089596879" Y="25.51750859515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.66838053213" Y="24.376979327697" />
                  <Point X="-4.725900807869" Y="23.967701299282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.141779346915" Y="27.441338285988" />
                  <Point X="-4.168790199522" Y="27.249146083166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.305687999853" Y="26.275067619629" />
                  <Point X="-4.415637511751" Y="25.492736191685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.568692853835" Y="24.403690844767" />
                  <Point X="-4.66401651796" Y="23.725427731226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.015421185794" Y="27.657820149043" />
                  <Point X="-4.082195065974" Y="27.182699303764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.21149715652" Y="26.262667123646" />
                  <Point X="-4.323185426622" Y="25.467963788219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.469005175541" Y="24.430402361838" />
                  <Point X="-4.568068431819" Y="23.725530667515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.897373400462" Y="27.815170615827" />
                  <Point X="-3.995599961298" Y="27.116252318919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.117306313187" Y="26.250266627662" />
                  <Point X="-4.230733341493" Y="25.443191384754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.369317497246" Y="24.457113878909" />
                  <Point X="-4.470326359513" Y="23.738398478644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.780289034668" Y="27.965665996397" />
                  <Point X="-3.909004856622" Y="27.049805334073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.023115441854" Y="26.237866330913" />
                  <Point X="-4.138281256365" Y="25.418418981289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.269629818952" Y="24.48382539598" />
                  <Point X="-4.372584287207" Y="23.751266289773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.678491467387" Y="28.007390153679" />
                  <Point X="-3.822409751947" Y="26.983358349228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.92892456937" Y="26.225466042347" />
                  <Point X="-4.045829171236" Y="25.393646577824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.169942140657" Y="24.510536913051" />
                  <Point X="-4.274842205512" Y="23.764134167707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.589757795949" Y="27.956159862024" />
                  <Point X="-3.735814647271" Y="26.916911364383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.834733696886" Y="26.213065753782" />
                  <Point X="-3.953377086107" Y="25.368874174358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.070254462362" Y="24.537248430121" />
                  <Point X="-4.177100122247" Y="23.777002056809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.501024124511" Y="27.90492957037" />
                  <Point X="-3.649219542595" Y="26.850464379537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.739990444059" Y="26.204595855592" />
                  <Point X="-3.860925000979" Y="25.344101770893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.970566788218" Y="24.563959917661" />
                  <Point X="-4.079358038983" Y="23.789869945911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.412290474105" Y="27.853699129064" />
                  <Point X="-3.56262443792" Y="26.784017394692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.641161878409" Y="26.225194468563" />
                  <Point X="-3.76847291585" Y="25.319329367428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.870879181366" Y="24.590670926391" />
                  <Point X="-3.981615955718" Y="23.802737835013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.153781764148" Y="22.577714454479" />
                  <Point X="-4.161422225554" Y="22.523349746728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.323556841599" Y="27.802468560393" />
                  <Point X="-3.479405101099" Y="26.69355057346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.538348792826" Y="26.274144414022" />
                  <Point X="-3.676020830721" Y="25.294556963963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.771191574514" Y="24.617381935121" />
                  <Point X="-3.883873872454" Y="23.815605724115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.046252075333" Y="22.660224775771" />
                  <Point X="-4.08378503658" Y="22.393163879722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.234553515221" Y="27.753156963331" />
                  <Point X="-3.583568745593" Y="25.269784560498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.671503967662" Y="24.644092943851" />
                  <Point X="-3.786131789189" Y="23.828473613216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.938722386518" Y="22.742735097062" />
                  <Point X="-4.006147610291" Y="22.2629797013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.999061150902" Y="28.746169031504" />
                  <Point X="-3.016098913547" Y="28.624939051044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.141874228341" Y="27.730001184333" />
                  <Point X="-3.491333056398" Y="25.243472419958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.571816360811" Y="24.670803952581" />
                  <Point X="-3.688389705925" Y="23.841341502318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.831192697703" Y="22.825245418353" />
                  <Point X="-3.925258147387" Y="22.155934965753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.89154223347" Y="28.828602710421" />
                  <Point X="-2.938946117465" Y="28.491306549518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.046839813753" Y="27.723603009712" />
                  <Point X="-3.40256194739" Y="25.192508510454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.471468515006" Y="24.702212805563" />
                  <Point X="-3.59064762266" Y="23.85420939142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.723663008888" Y="22.907755739645" />
                  <Point X="-3.844277053994" Y="22.049542215003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.784023316038" Y="28.911036389338" />
                  <Point X="-2.861793321383" Y="28.357674047993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.947780094966" Y="27.745846362719" />
                  <Point X="-3.321228192551" Y="25.088625076279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.364655435372" Y="24.779624187584" />
                  <Point X="-3.492905539396" Y="23.867077280522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.616133320073" Y="22.990266060936" />
                  <Point X="-3.754678736012" Y="22.00446420319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.676504398606" Y="28.993470068255" />
                  <Point X="-2.784717056973" Y="28.223496995333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.839702432501" Y="27.832255719124" />
                  <Point X="-3.395163456131" Y="23.879945169624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.508603631258" Y="23.072776382228" />
                  <Point X="-3.650273533303" Y="22.064742650642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.571540724719" Y="29.057722244624" />
                  <Point X="-3.297421372867" Y="23.892813058726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.401073951652" Y="23.155286637989" />
                  <Point X="-3.545868330567" Y="22.125021098285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.467482013085" Y="29.115535279971" />
                  <Point X="-3.200537038698" Y="23.899577745881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.293544279928" Y="23.237796837669" />
                  <Point X="-3.441463127739" Y="22.185299546586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.363423301957" Y="29.173348311718" />
                  <Point X="-3.107854314027" Y="23.876446428036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.186014608205" Y="23.32030703735" />
                  <Point X="-3.337057924911" Y="22.245577994887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.25936459088" Y="29.231161343107" />
                  <Point X="-3.020166411413" Y="23.817775104554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.078484936481" Y="23.40281723703" />
                  <Point X="-3.232652722083" Y="22.305856443188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.163523554633" Y="29.230502579821" />
                  <Point X="-2.947355346293" Y="23.653249581996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.959811956678" Y="23.564616193619" />
                  <Point X="-3.128247519255" Y="22.366134891489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.081072228015" Y="29.134571082047" />
                  <Point X="-3.023842316427" Y="22.42641333979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.992664877119" Y="29.08101889909" />
                  <Point X="-2.919437113599" Y="22.486691788091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.902517687301" Y="29.039846313311" />
                  <Point X="-2.815031910771" Y="22.546970236392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.807871805362" Y="29.03068358525" />
                  <Point X="-2.710626707943" Y="22.607248684693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.890949500811" Y="21.324185344061" />
                  <Point X="-2.919478933063" Y="21.121187885617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.707743398495" Y="29.060531049064" />
                  <Point X="-2.609137388101" Y="22.646779547482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.764149756951" Y="21.543809231573" />
                  <Point X="-2.832912796771" Y="21.054534780016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.605130385238" Y="29.108057405952" />
                  <Point X="-2.513338321103" Y="22.645822157468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.63735001309" Y="21.763433119084" />
                  <Point X="-2.746322321332" Y="20.98805485644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.443914449782" Y="29.572565221097" />
                  <Point X="-1.463031536452" Y="29.436540081427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.479886046375" Y="29.316614011836" />
                  <Point X="-2.423366030277" Y="22.603405100702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.510550250461" Y="21.983057140134" />
                  <Point X="-2.658068439766" Y="20.933410682456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.344045797829" Y="29.600564432661" />
                  <Point X="-2.340651669515" Y="22.509345188116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.383750470423" Y="22.202681285063" />
                  <Point X="-2.5698145582" Y="20.878766508473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.244177139175" Y="29.628563691898" />
                  <Point X="-2.474403014226" Y="20.875051748869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.144308480522" Y="29.656562951135" />
                  <Point X="-2.359918951529" Y="21.007045011521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.044439821868" Y="29.684562210373" />
                  <Point X="-2.253977254824" Y="21.078256181832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.944571163215" Y="29.71256146961" />
                  <Point X="-2.148101231075" Y="21.149000064778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.846720198298" Y="29.726204091921" />
                  <Point X="-2.043874378938" Y="21.208009481975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.749182107289" Y="29.737620500711" />
                  <Point X="-1.945683066202" Y="21.224073804855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.651644072155" Y="29.749036511918" />
                  <Point X="-1.850625093823" Y="21.217843252631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.554106092772" Y="29.760452126441" />
                  <Point X="-1.755567121444" Y="21.211612700408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.456568113389" Y="29.771867740963" />
                  <Point X="-1.660509052752" Y="21.205382833487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.3612274974" Y="29.767648302528" />
                  <Point X="-1.567305965041" Y="21.185954091057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.29829986267" Y="29.532798518621" />
                  <Point X="-1.47903548986" Y="21.131427986774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.235372165136" Y="29.297949181593" />
                  <Point X="-1.393628440672" Y="21.056527547889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.159835538691" Y="29.152817035569" />
                  <Point X="-1.308221341713" Y="20.981627463135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.071392517584" Y="29.09951865935" />
                  <Point X="-1.224309495546" Y="20.896088101936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.022537626526" Y="29.08526319201" />
                  <Point X="-1.158717906475" Y="20.680193338095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.122828746021" Y="29.11626841633" />
                  <Point X="-1.121339429353" Y="20.263551851718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.240558544824" Y="29.271356291389" />
                  <Point X="-1.028497402599" Y="20.24155402709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.408238730453" Y="29.781859636503" />
                  <Point X="-0.876725065168" Y="20.638867150777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.502780845613" Y="29.771958569439" />
                  <Point X="-0.674969157414" Y="21.39183185736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.597322960773" Y="29.762057502375" />
                  <Point X="-0.53730062216" Y="21.688791214071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.691865075932" Y="29.752156435311" />
                  <Point X="-0.418565289599" Y="21.851033833596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.786407191092" Y="29.742255368247" />
                  <Point X="-0.313418162062" Y="21.916591350505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.879963146242" Y="29.725337408108" />
                  <Point X="-0.213109318971" Y="21.947722684757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.972748447194" Y="29.702935958425" />
                  <Point X="-0.112800381801" Y="21.978854688423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.065533764718" Y="29.680534626657" />
                  <Point X="-0.013633668064" Y="22.001859350052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.158319098542" Y="29.658133410865" />
                  <Point X="0.080991023149" Y="21.992545842137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.251104432365" Y="29.635732195073" />
                  <Point X="0.17291499725" Y="21.964015733456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.343889766189" Y="29.613330979281" />
                  <Point X="0.264839001849" Y="21.93548584178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.436675100012" Y="29.59092976349" />
                  <Point X="0.356291684507" Y="21.903602320238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.528230043048" Y="29.559773862338" />
                  <Point X="0.443077821374" Y="21.838514600063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.619510652684" Y="29.526665977625" />
                  <Point X="0.523297997926" Y="21.72670764466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.710791261293" Y="29.493558085598" />
                  <Point X="0.603077002208" Y="21.61176158545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.802071869901" Y="29.460450193571" />
                  <Point X="0.674700256005" Y="21.438784346176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.892866584551" Y="29.42388498638" />
                  <Point X="0.867965886607" Y="22.131337591781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.762700266927" Y="21.382333788696" />
                  <Point X="0.737627916968" Y="21.203934748929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.98288367798" Y="29.381786716698" />
                  <Point X="0.976593361269" Y="22.221659065246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.809100718087" Y="21.029886983228" />
                  <Point X="0.800555584599" Y="20.969085199134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.072900702558" Y="29.33968795712" />
                  <Point X="1.083905535572" Y="22.302621690362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.162917727136" Y="29.297589197542" />
                  <Point X="1.184086171442" Y="22.332840782841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.252897353113" Y="29.255224333088" />
                  <Point X="1.281276646107" Y="22.341783772818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.341570626602" Y="29.203564287696" />
                  <Point X="2.132319235735" Y="27.714663276756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.059601738082" Y="27.197251395662" />
                  <Point X="1.378467100864" Y="22.350726621138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.430243809895" Y="29.151903600527" />
                  <Point X="2.259118850012" Y="27.934286242225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.140587687773" Y="27.090893199277" />
                  <Point X="1.47403000166" Y="22.348088821285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.51891695777" Y="29.100242661341" />
                  <Point X="2.385918534434" Y="28.153909706812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.227509758095" Y="27.026772695887" />
                  <Point X="1.56506407192" Y="22.313226717752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.607590105644" Y="29.048581722154" />
                  <Point X="2.512718330975" Y="28.373533969159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.318052632293" Y="26.988415550777" />
                  <Point X="1.653047909819" Y="22.256661083242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.695165579941" Y="28.989110429625" />
                  <Point X="2.639518127515" Y="28.593158231506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.412185130594" Y="26.975599908314" />
                  <Point X="1.741031845785" Y="22.200096146509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.782382440244" Y="28.927087465947" />
                  <Point X="2.766317924055" Y="28.812782493853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.509515799605" Y="26.985540432893" />
                  <Point X="1.827291663992" Y="22.131263474476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.609651216085" Y="27.015437772694" />
                  <Point X="1.908486581528" Y="22.026392161557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.713628934425" Y="27.07267451081" />
                  <Point X="2.101619157594" Y="22.717998674948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.034541134677" Y="22.240713741648" />
                  <Point X="1.989569390308" Y="21.920723153401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.818034129141" Y="27.132952901389" />
                  <Point X="2.216304767003" Y="22.851426016969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.105628610618" Y="22.063924244838" />
                  <Point X="2.070652199089" Y="21.815054145245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.922439323856" Y="27.193231291968" />
                  <Point X="2.319973369927" Y="22.906463284612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.182781434857" Y="21.930291943663" />
                  <Point X="2.15173500787" Y="21.709385137089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.026844518572" Y="27.253509682547" />
                  <Point X="2.423106525705" Y="22.957690647846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.259934259096" Y="21.796659642489" />
                  <Point X="2.232817779567" Y="21.603715865069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.131249713288" Y="27.313788073126" />
                  <Point X="3.01553819596" Y="26.490457846202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.00113212541" Y="26.387953327994" />
                  <Point X="2.521391739922" Y="22.974423114483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.337087083335" Y="21.663027341314" />
                  <Point X="2.313900548603" Y="21.498046574111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.235654908004" Y="27.374066463705" />
                  <Point X="3.143115184188" Y="26.715612114753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.066325531643" Y="26.169225346041" />
                  <Point X="2.615545115807" Y="22.961756023754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.414239907574" Y="21.52939504014" />
                  <Point X="2.394983317638" Y="21.392377283153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.340060102719" Y="27.434344854284" />
                  <Point X="3.251481973309" Y="26.804078714211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.147408323081" Y="26.063556214483" />
                  <Point X="2.709104054724" Y="22.944859294222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.491392731813" Y="21.395762738965" />
                  <Point X="2.476066086674" Y="21.286707992195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.444465297435" Y="27.494623244863" />
                  <Point X="3.359011673789" Y="26.886589118504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.235318823306" Y="26.006468755305" />
                  <Point X="2.930211830878" Y="23.83551969929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.87361509669" Y="23.432813010462" />
                  <Point X="2.802188097902" Y="22.924583505931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.568545556052" Y="21.26213043779" />
                  <Point X="2.55714885571" Y="21.181038701237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.548870492151" Y="27.554901635441" />
                  <Point X="3.466541374269" Y="26.969099522797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.326018098073" Y="25.969224458057" />
                  <Point X="3.045467798738" Y="23.973005352563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.950433505572" Y="23.296801220385" />
                  <Point X="2.89189081185" Y="22.880248310009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.645698339839" Y="21.128497848783" />
                  <Point X="2.638231624745" Y="21.075369410278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.653275686866" Y="27.61518002602" />
                  <Point X="3.574071070075" Y="27.051609893831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.419976777397" Y="25.955172029313" />
                  <Point X="3.150882267899" Y="24.040465103976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.030617561531" Y="23.184737253613" />
                  <Point X="2.980624481991" Y="22.829018009125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.722851095402" Y="20.99486505895" />
                  <Point X="2.719314393781" Y="20.96970011932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.7576808565" Y="27.675458238127" />
                  <Point X="3.681600758936" Y="27.13412021545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.515858853241" Y="25.954805277933" />
                  <Point X="3.410223411712" Y="25.203170055669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.362864215539" Y="24.866191865144" />
                  <Point X="3.250216277989" Y="24.064660141008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.116705466551" Y="23.11468135569" />
                  <Point X="3.069358152132" Y="22.77778770824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.862086025041" Y="27.735736442469" />
                  <Point X="3.789130447798" Y="27.216630537068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.613600950529" Y="25.967673266817" />
                  <Point X="3.524205819364" Y="25.331593857199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.435537896114" Y="24.700688800756" />
                  <Point X="3.349172684343" Y="24.086168387859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.203300599924" Y="23.048234575037" />
                  <Point X="3.158091822273" Y="22.726557407356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.960457508865" Y="27.753082749253" />
                  <Point X="3.896660136659" Y="27.299140858687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.711343047817" Y="25.980541255702" />
                  <Point X="3.628966714652" Y="25.394403188863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.518765611372" Y="24.610281595205" />
                  <Point X="3.445586746808" Y="24.089586917974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.289895733297" Y="22.981787794385" />
                  <Point X="3.246825492414" Y="22.675327106472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.040715072341" Y="27.641541815641" />
                  <Point X="4.00418982552" Y="27.381651180306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.809085145105" Y="25.993409244586" />
                  <Point X="3.730123944843" Y="25.431571111003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.607030292991" Y="24.555714267592" />
                  <Point X="3.539777585463" Y="24.077186388703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.37649086667" Y="22.915341013732" />
                  <Point X="3.335559147351" Y="22.624096697407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.118664602198" Y="27.513578369499" />
                  <Point X="4.111719514381" Y="27.464161501924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.906827242393" Y="26.006277233471" />
                  <Point X="3.829811641339" Y="25.458282757582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.697198201986" Y="24.514689106425" />
                  <Point X="3.633968424118" Y="24.064785859432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.463086000043" Y="22.848894233079" />
                  <Point X="3.424292796246" Y="22.57286624535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.004569339681" Y="26.019145222355" />
                  <Point X="3.929499337835" Y="25.48499440416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.789650288893" Y="24.489916715613" />
                  <Point X="3.728159262773" Y="24.052385330161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.549681133416" Y="22.782447452427" />
                  <Point X="3.513026445141" Y="22.521635793293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.102311436969" Y="26.032013211239" />
                  <Point X="4.029187034331" Y="25.511706050739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.8821023758" Y="24.4651443248" />
                  <Point X="3.822350101428" Y="24.03998480089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.636276266789" Y="22.716000671774" />
                  <Point X="3.601760094036" Y="22.470405341237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.200053534257" Y="26.044881200124" />
                  <Point X="4.12887471838" Y="25.538417608757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.974554462707" Y="24.440371933988" />
                  <Point X="3.916540941594" Y="24.027584282376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.722871400162" Y="22.649553891121" />
                  <Point X="3.690493742931" Y="22.41917488918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.297795631545" Y="26.057749189008" />
                  <Point X="4.228562368518" Y="25.565128925482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.067006549613" Y="24.415599543176" />
                  <Point X="4.010731814669" Y="24.015183998015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.809466533535" Y="22.583107110469" />
                  <Point X="3.779227391826" Y="22.367944437123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.395537728833" Y="26.070617177892" />
                  <Point X="4.328250018655" Y="25.591840242206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.15945863652" Y="24.390827152364" />
                  <Point X="4.104922687743" Y="24.002783713655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.896061670369" Y="22.516660354445" />
                  <Point X="3.867961040721" Y="22.316713985067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.493279834525" Y="26.083485226572" />
                  <Point X="4.427937668793" Y="25.618551558931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.251910723427" Y="24.366054761551" />
                  <Point X="4.199113560818" Y="23.990383429294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.982656820976" Y="22.450213696418" />
                  <Point X="3.956694689616" Y="22.26548353301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.591021968763" Y="26.096353478374" />
                  <Point X="4.527625318931" Y="25.645262875656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.344362810334" Y="24.341282370739" />
                  <Point X="4.293304433893" Y="23.977983144934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.069251971583" Y="22.383767038392" />
                  <Point X="4.061476281427" Y="22.328440128081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.688764103002" Y="26.109221730176" />
                  <Point X="4.627312969069" Y="25.67197419238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.436814897241" Y="24.316509979927" />
                  <Point X="4.387495306968" Y="23.965582860573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.755106787416" Y="25.898671287398" />
                  <Point X="4.727000619206" Y="25.698685509105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.529266984148" Y="24.291737589114" />
                  <Point X="4.481686180042" Y="23.953182576212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.621719090852" Y="24.266965339167" />
                  <Point X="4.575877053117" Y="23.940782291852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.714171223766" Y="24.242193275711" />
                  <Point X="4.670067926192" Y="23.928382007491" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.597906738281" Y="20.99127734375" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.412669250488" Y="21.552873046875" />
                  <Point X="0.300591186523" Y="21.714357421875" />
                  <Point X="0.274335906982" Y="21.733599609375" />
                  <Point X="0.194411117554" Y="21.758404296875" />
                  <Point X="0.020976930618" Y="21.812232421875" />
                  <Point X="-0.008664452553" Y="21.812234375" />
                  <Point X="-0.088589248657" Y="21.787427734375" />
                  <Point X="-0.2620234375" Y="21.7336015625" />
                  <Point X="-0.288278991699" Y="21.714357421875" />
                  <Point X="-0.339928710938" Y="21.639939453125" />
                  <Point X="-0.45200680542" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.733440673828" Y="20.4395078125" />
                  <Point X="-0.847744018555" Y="20.012923828125" />
                  <Point X="-0.896538818359" Y="20.02239453125" />
                  <Point X="-1.100230224609" Y="20.061931640625" />
                  <Point X="-1.192067749023" Y="20.085560546875" />
                  <Point X="-1.351589355469" Y="20.126603515625" />
                  <Point X="-1.324361938477" Y="20.333416015625" />
                  <Point X="-1.309150146484" Y="20.448958984375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.32877734375" Y="20.561232421875" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282592773" Y="20.79737109375" />
                  <Point X="-1.4598671875" Y="20.86190234375" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649240600586" Y="21.014236328125" />
                  <Point X="-1.746903808594" Y="21.02063671875" />
                  <Point X="-1.958829589844" Y="21.03452734375" />
                  <Point X="-1.989878417969" Y="21.026208984375" />
                  <Point X="-2.071256835938" Y="20.971833984375" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.413683837891" Y="20.64207421875" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.557240234375" Y="20.6475078125" />
                  <Point X="-2.855838134766" Y="20.832390625" />
                  <Point X="-2.982981201172" Y="20.930287109375" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.740873779297" Y="21.964125" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762695312" Y="22.402408203125" />
                  <Point X="-2.513980712891" Y="22.431236328125" />
                  <Point X="-2.531329345703" Y="22.448583984375" />
                  <Point X="-2.560158203125" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.474328125" Y="21.94693359375" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-3.925798339844" Y="21.842939453125" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.252857910156" Y="22.305720703125" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.574680664062" Y="23.261564453125" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.152535644531" Y="23.588916015625" />
                  <Point X="-3.143391601562" Y="23.613369140625" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.136651611328" Y="23.649947265625" />
                  <Point X="-3.148654541016" Y="23.678927734375" />
                  <Point X="-3.169516113281" Y="23.694279296875" />
                  <Point X="-3.18764453125" Y="23.704947265625" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-4.333784667969" Y="23.564734375" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.83512890625" Y="23.62759765625" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.951510253906" Y="24.157431640625" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-4.025533691406" Y="24.745935546875" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.533141113281" Y="24.884650390625" />
                  <Point X="-3.514143066406" Y="24.8978359375" />
                  <Point X="-3.502324951172" Y="24.909353515625" />
                  <Point X="-3.491981201172" Y="24.933494140625" />
                  <Point X="-3.4856484375" Y="24.953896484375" />
                  <Point X="-3.483400878906" Y="24.96871875" />
                  <Point X="-3.488566162109" Y="24.992943359375" />
                  <Point X="-3.494898925781" Y="25.01334765625" />
                  <Point X="-3.502324462891" Y="25.0280859375" />
                  <Point X="-3.522897949219" Y="25.0456796875" />
                  <Point X="-3.541895996094" Y="25.058865234375" />
                  <Point X="-3.557463623047" Y="25.0660859375" />
                  <Point X="-4.57316015625" Y="25.338240234375" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.977107910156" Y="25.59457421875" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.86909765625" Y="26.175572265625" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.088790039063" Y="26.43815234375" />
                  <Point X="-3.753266113281" Y="26.39398046875" />
                  <Point X="-3.731704833984" Y="26.3958671875" />
                  <Point X="-3.712327392578" Y="26.4019765625" />
                  <Point X="-3.670278808594" Y="26.415234375" />
                  <Point X="-3.651534667969" Y="26.4260546875" />
                  <Point X="-3.639120117188" Y="26.44378515625" />
                  <Point X="-3.631344726563" Y="26.462556640625" />
                  <Point X="-3.61447265625" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616315917969" Y="26.54551171875" />
                  <Point X="-3.625697753906" Y="26.563533203125" />
                  <Point X="-3.646055664062" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-4.24257421875" Y="27.066271484375" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.391065917969" Y="27.391158203125" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-4.031414794922" Y="27.952302734375" />
                  <Point X="-3.774670654297" Y="28.282310546875" />
                  <Point X="-3.365226318359" Y="28.04591796875" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138514404297" Y="27.92043359375" />
                  <Point X="-3.111526855469" Y="27.918072265625" />
                  <Point X="-3.052965087891" Y="27.91294921875" />
                  <Point X="-3.031506835938" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.994096923828" Y="27.946560546875" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.940435302734" Y="28.054828125" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.210237792969" Y="28.581197265625" />
                  <Point X="-3.307278564453" Y="28.749275390625" />
                  <Point X="-3.155294921875" Y="28.86580078125" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.550335693359" Y="29.286859375" />
                  <Point X="-2.141548583984" Y="29.51397265625" />
                  <Point X="-2.030911010742" Y="29.369787109375" />
                  <Point X="-1.967826904297" Y="29.28757421875" />
                  <Point X="-1.951247436523" Y="29.273662109375" />
                  <Point X="-1.921210571289" Y="29.258025390625" />
                  <Point X="-1.85603137207" Y="29.22409375" />
                  <Point X="-1.835124267578" Y="29.218490234375" />
                  <Point X="-1.81380847168" Y="29.22225" />
                  <Point X="-1.782522949219" Y="29.235208984375" />
                  <Point X="-1.714634399414" Y="29.263330078125" />
                  <Point X="-1.696905029297" Y="29.275744140625" />
                  <Point X="-1.686083251953" Y="29.29448828125" />
                  <Point X="-1.675900268555" Y="29.326783203125" />
                  <Point X="-1.653803955078" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.681268676758" Y="29.641369140625" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-1.489050048828" Y="29.757236328125" />
                  <Point X="-0.968082946777" Y="29.903296875" />
                  <Point X="-0.722555053711" Y="29.932033203125" />
                  <Point X="-0.224200012207" Y="29.990359375" />
                  <Point X="-0.103101448059" Y="29.538412109375" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282115936" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594036102" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.186735778809" Y="29.804591796875" />
                  <Point X="0.236648361206" Y="29.9908671875" />
                  <Point X="0.405334960938" Y="29.973201171875" />
                  <Point X="0.860210144043" Y="29.925564453125" />
                  <Point X="1.063347900391" Y="29.87651953125" />
                  <Point X="1.508455932617" Y="29.769056640625" />
                  <Point X="1.640639282227" Y="29.72111328125" />
                  <Point X="1.931043945312" Y="29.61578125" />
                  <Point X="2.058890869141" Y="29.5559921875" />
                  <Point X="2.338684570312" Y="29.425140625" />
                  <Point X="2.462218505859" Y="29.353169921875" />
                  <Point X="2.732520019531" Y="29.195693359375" />
                  <Point X="2.849015136719" Y="29.11284765625" />
                  <Point X="3.068739990234" Y="28.956591796875" />
                  <Point X="2.501663085938" Y="27.974384765625" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.218079101562" Y="27.466185546875" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.204685546875" Y="27.370423828125" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682373047" Y="27.3008125" />
                  <Point X="2.232233886719" Y="27.28083984375" />
                  <Point X="2.261640136719" Y="27.23750390625" />
                  <Point X="2.274939941406" Y="27.224203125" />
                  <Point X="2.294911132812" Y="27.210650390625" />
                  <Point X="2.338248535156" Y="27.181244140625" />
                  <Point X="2.360337158203" Y="27.172978515625" />
                  <Point X="2.382238037109" Y="27.170337890625" />
                  <Point X="2.429761962891" Y="27.164607421875" />
                  <Point X="2.448664794922" Y="27.1659453125" />
                  <Point X="2.473991943359" Y="27.17271875" />
                  <Point X="2.528951171875" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.56262890625" Y="27.782236328125" />
                  <Point X="3.994247802734" Y="28.0314296875" />
                  <Point X="4.042255859375" Y="27.9647109375" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.267534179688" Y="27.634560546875" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.646479003906" Y="26.8676796875" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.279371337891" Y="26.58383203125" />
                  <Point X="3.261143310547" Y="26.560052734375" />
                  <Point X="3.221589111328" Y="26.508451171875" />
                  <Point X="3.213119384766" Y="26.4915" />
                  <Point X="3.206329345703" Y="26.467220703125" />
                  <Point X="3.191595458984" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.196353271484" Y="26.363951171875" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646728516" Y="26.287953125" />
                  <Point X="3.230806884766" Y="26.26491015625" />
                  <Point X="3.263704345703" Y="26.214908203125" />
                  <Point X="3.280947753906" Y="26.1988203125" />
                  <Point X="3.302916992188" Y="26.186453125" />
                  <Point X="3.35058984375" Y="26.1596171875" />
                  <Point X="3.36856640625" Y="26.153619140625" />
                  <Point X="3.398270263672" Y="26.149693359375" />
                  <Point X="3.462727050781" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.446250488281" Y="26.26893359375" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.870325195312" Y="26.23425390625" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.959653808594" Y="25.81994140625" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.151286132813" Y="25.34771875" />
                  <Point X="3.741167480469" Y="25.237826171875" />
                  <Point X="3.729087158203" Y="25.232818359375" />
                  <Point X="3.699903808594" Y="25.215951171875" />
                  <Point X="3.636577148438" Y="25.17934765625" />
                  <Point X="3.622264892578" Y="25.166927734375" />
                  <Point X="3.604754882812" Y="25.144615234375" />
                  <Point X="3.566759033203" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.5511484375" Y="25.0442578125" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.538483154297" Y="24.959314453125" />
                  <Point X="3.544319824219" Y="24.928837890625" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566759033203" Y="24.841240234375" />
                  <Point X="3.584268798828" Y="24.818927734375" />
                  <Point X="3.622264892578" Y="24.77051171875" />
                  <Point X="3.636576171875" Y="24.758091796875" />
                  <Point X="3.665759521484" Y="24.741224609375" />
                  <Point X="3.729086181641" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.631111816406" Y="24.46115234375" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.986881835938" Y="24.288630859375" />
                  <Point X="4.948431640625" Y="24.033599609375" />
                  <Point X="4.922212402344" Y="23.918703125" />
                  <Point X="4.874545898438" Y="23.709822265625" />
                  <Point X="3.887604248047" Y="23.83975390625" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394836425781" Y="23.901658203125" />
                  <Point X="3.337560058594" Y="23.889208984375" />
                  <Point X="3.213272216797" Y="23.8621953125" />
                  <Point X="3.1854453125" Y="23.845302734375" />
                  <Point X="3.150825195312" Y="23.803666015625" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.059395996094" Y="23.6320078125" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360595703" Y="23.483376953125" />
                  <Point X="3.088058105469" Y="23.43407421875" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.994333984375" Y="22.680744140625" />
                  <Point X="4.33907421875" Y="22.416216796875" />
                  <Point X="4.312519042969" Y="22.37324609375" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.149907714844" Y="22.120814453125" />
                  <Point X="4.056687988281" Y="21.988361328125" />
                  <Point X="3.177060302734" Y="22.49621484375" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340820313" Y="22.746685546875" />
                  <Point X="2.669172851562" Y="22.75899609375" />
                  <Point X="2.521250488281" Y="22.7857109375" />
                  <Point X="2.489077880859" Y="22.78075390625" />
                  <Point X="2.432447021484" Y="22.75094921875" />
                  <Point X="2.309559814453" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.258795410156" Y="22.608685546875" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.201474121094" Y="22.38545703125" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.764798339844" Y="21.3022109375" />
                  <Point X="2.986673339844" Y="20.917912109375" />
                  <Point X="2.963909423828" Y="20.90165234375" />
                  <Point X="2.835295898438" Y="20.809787109375" />
                  <Point X="2.774673828125" Y="20.770546875" />
                  <Point X="2.679775146484" Y="20.70912109375" />
                  <Point X="2.006427734375" Y="21.58664453125" />
                  <Point X="1.683177490234" Y="22.007912109375" />
                  <Point X="1.670549316406" Y="22.019533203125" />
                  <Point X="1.603317260742" Y="22.062755859375" />
                  <Point X="1.45742590332" Y="22.15655078125" />
                  <Point X="1.425805053711" Y="22.16428125" />
                  <Point X="1.352275146484" Y="22.157515625" />
                  <Point X="1.192718261719" Y="22.142833984375" />
                  <Point X="1.165332519531" Y="22.131490234375" />
                  <Point X="1.10855480957" Y="22.08428125" />
                  <Point X="0.985348754883" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.951480529785" Y="21.875908203125" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.064327758789" Y="20.546892578125" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.116022338867" Y="20.063423828125" />
                  <Point X="0.994364868164" Y="20.0367578125" />
                  <Point X="0.938338745117" Y="20.026578125" />
                  <Point X="0.860200561523" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#148" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.06096105203" Y="4.582634228643" Z="0.8" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.8" />
                  <Point X="-0.734523202698" Y="5.012974273047" Z="0.8" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.8" />
                  <Point X="-1.508703818612" Y="4.836662241728" Z="0.8" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.8" />
                  <Point X="-1.736996940898" Y="4.66612412805" Z="0.8" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.8" />
                  <Point X="-1.729742312464" Y="4.373099843406" Z="0.8" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.8" />
                  <Point X="-1.80781358704" Y="4.31268355068" Z="0.8" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.8" />
                  <Point X="-1.904278255919" Y="4.333654967541" Z="0.8" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.8" />
                  <Point X="-1.997399298944" Y="4.431504154989" Z="0.8" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.8" />
                  <Point X="-2.580774539162" Y="4.361846143208" Z="0.8" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.8" />
                  <Point X="-3.19187294104" Y="3.936761127657" Z="0.8" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.8" />
                  <Point X="-3.25969499491" Y="3.587476974855" Z="0.8" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.8" />
                  <Point X="-2.996401132513" Y="3.081751009208" Z="0.8" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.8" />
                  <Point X="-3.035607668061" Y="3.013195899825" Z="0.8" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.8" />
                  <Point X="-3.113325456399" Y="2.99916356666" Z="0.8" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.8" />
                  <Point X="-3.346382373442" Y="3.120498953836" Z="0.8" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.8" />
                  <Point X="-4.077034053908" Y="3.01428585996" Z="0.8" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.8" />
                  <Point X="-4.44040425705" Y="2.447644560713" Z="0.8" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.8" />
                  <Point X="-4.27916817307" Y="2.05788328882" Z="0.8" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.8" />
                  <Point X="-3.676204187547" Y="1.571726565253" Z="0.8" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.8" />
                  <Point X="-3.683694599879" Y="1.512971365389" Z="0.8" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.8" />
                  <Point X="-3.733518534205" Y="1.480942242495" Z="0.8" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.8" />
                  <Point X="-4.08841994626" Y="1.519005104384" Z="0.8" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.8" />
                  <Point X="-4.923513272879" Y="1.219931229473" Z="0.8" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.8" />
                  <Point X="-5.03272542084" Y="0.633172221906" Z="0.8" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.8" />
                  <Point X="-4.592257188919" Y="0.321224080653" Z="0.8" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.8" />
                  <Point X="-3.557562799386" Y="0.035883379362" Z="0.8" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.8" />
                  <Point X="-3.542475105177" Y="0.009402919699" Z="0.8" />
                  <Point X="-3.539556741714" Y="0" Z="0.8" />
                  <Point X="-3.545889495958" Y="-0.020404031362" Z="0.8" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.8" />
                  <Point X="-3.567805795737" Y="-0.042992603772" Z="0.8" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.8" />
                  <Point X="-4.044630599854" Y="-0.174487976103" Z="0.8" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.8" />
                  <Point X="-5.007162078061" Y="-0.818366922321" Z="0.8" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.8" />
                  <Point X="-4.889729755139" Y="-1.353496006647" Z="0.8" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.8" />
                  <Point X="-4.333414031213" Y="-1.453557752385" Z="0.8" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.8" />
                  <Point X="-3.201030517925" Y="-1.317532877622" Z="0.8" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.8" />
                  <Point X="-3.197950336836" Y="-1.342813047014" Z="0.8" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.8" />
                  <Point X="-3.611274524366" Y="-1.667486999192" Z="0.8" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.8" />
                  <Point X="-4.301956997537" Y="-2.688607470617" Z="0.8" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.8" />
                  <Point X="-3.971740315579" Y="-3.156063705373" Z="0.8" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.8" />
                  <Point X="-3.455484135518" Y="-3.065086082304" Z="0.8" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.8" />
                  <Point X="-2.560963748236" Y="-2.567366720896" Z="0.8" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.8" />
                  <Point X="-2.790330930864" Y="-2.979594246974" Z="0.8" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.8" />
                  <Point X="-3.01964103592" Y="-4.078049003647" Z="0.8" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.8" />
                  <Point X="-2.589717831024" Y="-4.363723842738" Z="0.8" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.8" />
                  <Point X="-2.380172083985" Y="-4.35708340384" Z="0.8" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.8" />
                  <Point X="-2.049634203106" Y="-4.038459626868" Z="0.8" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.8" />
                  <Point X="-1.756330038109" Y="-3.997974744045" Z="0.8" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.8" />
                  <Point X="-1.49899060124" Y="-4.144408436467" Z="0.8" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.8" />
                  <Point X="-1.383972561133" Y="-4.417240170402" Z="0.8" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.8" />
                  <Point X="-1.380090209615" Y="-4.628776319243" Z="0.8" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.8" />
                  <Point X="-1.210682668534" Y="-4.931583225641" Z="0.8" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.8" />
                  <Point X="-0.912222281846" Y="-4.995409647229" Z="0.8" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.8" />
                  <Point X="-0.691300449121" Y="-4.542152497085" Z="0.8" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.8" />
                  <Point X="-0.305008723737" Y="-3.357289967401" Z="0.8" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.8" />
                  <Point X="-0.07992481597" Y="-3.229045081815" Z="0.8" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.8" />
                  <Point X="0.173434263391" Y="-3.258066963473" Z="0.8" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.8" />
                  <Point X="0.365437135695" Y="-3.444355788367" Z="0.8" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.8" />
                  <Point X="0.54345441128" Y="-3.99038353273" Z="0.8" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.8" />
                  <Point X="0.941119332093" Y="-4.991336363966" Z="0.8" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.8" />
                  <Point X="1.120572014285" Y="-4.954135888832" Z="0.8" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.8" />
                  <Point X="1.107744004794" Y="-4.415301464399" Z="0.8" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.8" />
                  <Point X="0.99418374417" Y="-3.103430180487" Z="0.8" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.8" />
                  <Point X="1.134365967848" Y="-2.9228843048" Z="0.8" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.8" />
                  <Point X="1.350700776399" Y="-2.860992857236" Z="0.8" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.8" />
                  <Point X="1.570121841219" Y="-2.948021269843" Z="0.8" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.8" />
                  <Point X="1.960604129846" Y="-3.412513088919" Z="0.8" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.8" />
                  <Point X="2.79568717443" Y="-4.240147696871" Z="0.8" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.8" />
                  <Point X="2.986815140796" Y="-4.107755482724" Z="0.8" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.8" />
                  <Point X="2.801943637122" Y="-3.641509276087" Z="0.8" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.8" />
                  <Point X="2.244522211419" Y="-2.574376208962" Z="0.8" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.8" />
                  <Point X="2.296885765186" Y="-2.383321054278" Z="0.8" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.8" />
                  <Point X="2.449577324094" Y="-2.262015544781" Z="0.8" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.8" />
                  <Point X="2.65413059455" Y="-2.258925798072" Z="0.8" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.8" />
                  <Point X="3.14590432102" Y="-2.515805863152" Z="0.8" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.8" />
                  <Point X="4.184640529501" Y="-2.876683254438" Z="0.8" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.8" />
                  <Point X="4.348896898227" Y="-2.621758686907" Z="0.8" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.8" />
                  <Point X="4.018616103612" Y="-2.248307969555" Z="0.8" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.8" />
                  <Point X="3.123960091393" Y="-1.507606008013" Z="0.8" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.8" />
                  <Point X="3.103029592278" Y="-1.341294001797" Z="0.8" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.8" />
                  <Point X="3.183115594356" Y="-1.197021216493" Z="0.8" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.8" />
                  <Point X="3.342023460896" Y="-1.128369686754" Z="0.8" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.8" />
                  <Point X="3.874922029413" Y="-1.178537266246" Z="0.8" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.8" />
                  <Point X="4.964803527481" Y="-1.061140378552" Z="0.8" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.8" />
                  <Point X="5.030167386865" Y="-0.687541522291" Z="0.8" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.8" />
                  <Point X="4.637896602181" Y="-0.459270375917" Z="0.8" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.8" />
                  <Point X="3.684625973261" Y="-0.184206653915" Z="0.8" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.8" />
                  <Point X="3.617446678851" Y="-0.118922328818" Z="0.8" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.8" />
                  <Point X="3.587271378429" Y="-0.030476819516" Z="0.8" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.8" />
                  <Point X="3.594100071995" Y="0.066133711709" Z="0.8" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.8" />
                  <Point X="3.637932759549" Y="0.145026409757" Z="0.8" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.8" />
                  <Point X="3.718769441091" Y="0.203942190327" Z="0.8" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.8" />
                  <Point X="4.158071215125" Y="0.330701556851" Z="0.8" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.8" />
                  <Point X="5.002902444808" Y="0.858912629305" Z="0.8" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.8" />
                  <Point X="4.912750277234" Y="1.277361316826" Z="0.8" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.8" />
                  <Point X="4.433568263972" Y="1.349785874014" Z="0.8" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.8" />
                  <Point X="3.39866464319" Y="1.230542830637" Z="0.8" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.8" />
                  <Point X="3.321287882657" Y="1.261304200703" Z="0.8" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.8" />
                  <Point X="3.266421263028" Y="1.323673464248" Z="0.8" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.8" />
                  <Point X="3.239165895503" Y="1.405335427249" Z="0.8" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.8" />
                  <Point X="3.24832602616" Y="1.485034433335" Z="0.8" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.8" />
                  <Point X="3.294670158012" Y="1.560915237067" Z="0.8" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.8" />
                  <Point X="3.670761030836" Y="1.859292930868" Z="0.8" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.8" />
                  <Point X="4.304155596219" Y="2.691728596281" Z="0.8" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.8" />
                  <Point X="4.076685522253" Y="3.0251936075" Z="0.8" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.8" />
                  <Point X="3.531472997249" Y="2.856816926974" Z="0.8" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.8" />
                  <Point X="2.454918811659" Y="2.252301986253" Z="0.8" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.8" />
                  <Point X="2.382067600904" Y="2.251259695577" Z="0.8" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.8" />
                  <Point X="2.316829479953" Y="2.283306658084" Z="0.8" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.8" />
                  <Point X="2.26745194215" Y="2.34019538043" Z="0.8" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.8" />
                  <Point X="2.248170007182" Y="2.407690842928" Z="0.8" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.8" />
                  <Point X="2.260225956404" Y="2.484550764223" Z="0.8" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.8" />
                  <Point X="2.538808425886" Y="2.980666084706" Z="0.8" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.8" />
                  <Point X="2.871836191273" Y="4.1848763031" Z="0.8" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.8" />
                  <Point X="2.481232493129" Y="4.42765442242" Z="0.8" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.8" />
                  <Point X="2.073916313032" Y="4.632563540811" Z="0.8" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.8" />
                  <Point X="1.651531908916" Y="4.799398114016" Z="0.8" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.8" />
                  <Point X="1.068926359353" Y="4.956404446444" Z="0.8" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.8" />
                  <Point X="0.404386812742" Y="5.054210799554" Z="0.8" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.8" />
                  <Point X="0.132283509523" Y="4.848813277308" Z="0.8" />
                  <Point X="0" Y="4.355124473572" Z="0.8" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>