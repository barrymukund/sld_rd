<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers>
      <Layer Name="0">
        <Visible Value="True" />
        <Locked Value="False" />
        <Printable Value="True" />
        <Markable Value="True" />
        <Foreground Value="-16776961" />
        <NormalVector X="0" Y="0" Z="1" />
      </Layer>
      <Layer Name="03_20220112_slice_spline_DXFexport$03_00_slice_spline_B00_01_Bezier">
        <Visible Value="True" />
        <Locked Value="False" />
        <Printable Value="True" />
        <Markable Value="True" />
        <Foreground Value="-16711936" />
        <NormalVector X="0" Y="0" Z="1" />
      </Layer>
    </Layers>
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#166" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <MarkMode Value="HatchlinesAfterOutlines" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement>
      <ShapeElement Name="*Model_Space">
        <UID Value="S#1" />
        <Label Value="*Model_Space" />
        <Area Value="False" />
        <MarkMode Value="HatchlinesAfterOutlines" />
        <GraphicResolution Value="0.02" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="0" />
          <Height Value="0" />
        </Box>
      </ShapeElement>
      <ShapeElement Name="*Paper_Space">
        <UID Value="S#65" />
        <Label Value="*Paper_Space" />
        <Area Value="False" />
        <MarkMode Value="HatchlinesAfterOutlines" />
        <GraphicResolution Value="0.02" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="0" />
          <Height Value="0" />
        </Box>
      </ShapeElement>
      <ShapeElement Name="*Paper_Space0">
        <UID Value="S#66" />
        <Label Value="*Paper_Space0" />
        <Area Value="False" />
        <MarkMode Value="HatchlinesAfterOutlines" />
        <GraphicResolution Value="0.02" />
        <Box>
          <Center X="0" Y="0" />
          <Width Value="0" />
          <Height Value="0" />
        </Box>
      </ShapeElement>
    </ShapesElement>
  </LibraryElement>
  <JobElement>
    <UID Value="G#1" />
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <ChildElements>
      <GroupElement>
        <UID Value="G#160" />
        <Label Value="Vector Graphics (20220112_01_forgedUClamp1_watertight_degenCP_spline_G00_0102.dxf)" />
        <Translation X="-5.532285E-05" Y="-0.000130103479" Z="0" />
        <Area Value="True" />
        <MarkMode Value="HatchlinesAfterOutlines" />
        <GraphicResolution Value="0.02" />
        <Box>
          <Center X="5.532285E-05" Y="0.000130103479" />
          <Width Value="14.287500000002" />
          <Height Value="18.756600856909" />
        </Box>
        <ChildElements>
          <GraphicsPath>
            <UID Value="G#159" />
            <Label Value="Graphics Path" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <MarkMode Value="HatchlinesAfterOutlines" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="5.532285E-05" Y="0.000130103479" />
              <Width Value="14.287500000002" />
              <Height Value="18.756600856909" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePathElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <MarkMode Value="HatchlinesAfterOutlines" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="5.532285E-05" Y="0.000130103479" />
                  <Width Value="14.287500000002" />
                  <Height Value="18.756600856909" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="3.713146192197" Y="-6.694540302341" Z="16.6" />
                  <Point X="3.853826941284" Y="-6.531834825711" Z="16.6" />
                  <Point X="3.998005359782" Y="-6.372224874918" Z="16.6" />
                  <Point X="4.15131823975" Y="-6.208723958027" Z="16.6" />
                  <Point X="4.646018820208" Y="-5.672356781999" Z="16.6" />
                  <Point X="5.092161901609" Y="-5.150987171571" Z="16.6" />
                  <Point X="5.110126543622" Y="-5.128925634696" Z="16.6" />
                  <Point X="5.540782109409" Y="-4.56834723877" Z="16.599999999999" />
                  <Point X="5.93609304022" Y="-3.98613541312" Z="16.6" />
                  <Point X="6.276406430854" Y="-3.411695676495" Z="16.6" />
                  <Point X="6.293330022104" Y="-3.380377605333" Z="16.6" />
                  <BezierPoint X="6.403787419412" Y="-3.174748688434" Z="16.6" OnCorner="False" />
                  <BezierPoint X="6.503925623633" Y="-2.964211631004" Z="16.6" OnCorner="False" />
                  <Point X="6.593744634767" Y="-2.748766433044" Z="16.6" />
                  <BezierPoint X="6.683901908434" Y="-2.53250350587" Z="16.6" OnCorner="False" />
                  <BezierPoint X="6.763229962313" Y="-2.312338679626" Z="16.6" OnCorner="False" />
                  <Point X="6.831728796406" Y="-2.088271954312" Z="16.6" />
                  <BezierPoint X="6.900457969378" Y="-1.863433733605" Z="16.6" OnCorner="False" />
                  <BezierPoint X="6.95794931361" Y="-1.635757804568" Z="16.6" OnCorner="False" />
                  <Point X="7.004202829103" Y="-1.4052441672" Z="16.6" />
                  <BezierPoint X="7.050587138668" Y="-1.174073211206" Z="16.6" OnCorner="False" />
                  <BezierPoint X="7.085441377683" Y="-0.941176716852" Z="16.6" OnCorner="False" />
                  <Point X="7.108765546147" Y="-0.706554684139" Z="16.6" />
                  <Point X="7.135034203597" Y="-0.353712797694" Z="16.6" />
                  <Point X="7.143805322852" Y="0.000130269229" Z="16.6" />
                  <Point X="7.13503358018" Y="0.353873048012" Z="16.6" />
                  <Point X="7.108764300988" Y="0.706714888175" Z="16.6" />
                  <BezierPoint X="7.085439719098" Y="0.941336879794" Z="16.6" OnCorner="False" />
                  <BezierPoint X="7.050585069697" Y="1.174233312735" Z="16.6" OnCorner="False" />
                  <Point X="7.004200352786" Y="1.405404187" Z="16.6" />
                  <BezierPoint X="6.957946431107" Y="1.635917742869" Z="16.6" OnCorner="False" />
                  <BezierPoint X="6.900454685688" Y="1.863593570605" Z="16.6" OnCorner="False" />
                  <Point X="6.831725116528" Y="2.088431670208" Z="16.6" />
                  <BezierPoint X="6.76322588761" Y="2.312498274825" Z="16.6" OnCorner="False" />
                  <BezierPoint X="6.683897445779" Y="2.532662961289" Z="16.6" OnCorner="False" />
                  <Point X="6.593739791036" Y="2.748925729601" Z="16.6" />
                  <BezierPoint X="6.503920400267" Y="2.964370769296" Z="16.6" OnCorner="False" />
                  <BezierPoint X="6.40378182506" Y="3.174907650276" Z="16.6" OnCorner="False" />
                  <Point X="6.293324065414" Y="3.380536372542" Z="16.6" />
                  <Point X="6.276335682461" Y="3.411973418127" Z="16.6" />
                  <Point X="5.936083627659" Y="3.98629063653" Z="16.6" />
                  <Point X="5.540767168236" Y="4.568497899837" Z="16.599999999999" />
                  <Point X="5.110105081513" Y="5.129069345386" Z="16.6" />
                  <Point X="5.092032037082" Y="5.151263008852" Z="16.6" />
                  <Point X="4.645989022365" Y="5.672489272102" Z="16.6" />
                  <Point X="4.151275854081" Y="6.208835594439" Z="16.6" />
                  <Point X="3.997783759674" Y="6.372511436404" Z="16.6" />
                  <Point X="3.853586709128" Y="6.532124446867" Z="16.6" />
                  <Point X="3.712886072539" Y="6.694833527864" Z="16.6" />
                  <Point X="3.713781829185" Y="8.763077110813" Z="16.6" />
                  <BezierPoint X="3.693703471589" Y="8.858784528488" Z="16.600000000001" OnCorner="False" />
                  <BezierPoint X="3.638691717919" Y="8.925827559092" Z="16.600000000002" OnCorner="False" />
                  <Point X="3.548746568174" Y="8.964206202624" Z="16.600000000003" />
                  <Point X="3.31033047724" Y="9.068520253089" Z="16.6" />
                  <BezierPoint X="3.159557505572" Y="9.117857910315" Z="16.6" OnCorner="False" />
                  <BezierPoint X="3.00616932968" Y="9.156928464504" Z="16.600000000001" OnCorner="False" />
                  <Point X="2.850165949563" Y="9.185731915655" Z="16.600000000001" />
                  <BezierPoint X="2.534423412718" Y="9.243446259733" Z="16.600000000009" OnCorner="False" />
                  <BezierPoint X="2.216538351926" Y="9.284618464167" Z="16.600000000017" OnCorner="False" />
                  <Point X="1.896510767188" Y="9.309248528959" Z="16.600000000026" />
                  <Point X="0.948977945265" Y="9.362888521522" Z="16.6" />
                  <Point X="5.5322848E-05" Y="9.378430531933" Z="16.6" />
                  <Point X="-0.948867299567" Y="9.362888521522" Z="16.6" />
                  <Point X="-1.896400121488" Y="9.309248528957" Z="16.600000000026" />
                  <BezierPoint X="-2.216427728249" Y="9.284618634209" Z="16.600000000017" OnCorner="False" />
                  <BezierPoint X="-2.53431278904" Y="9.243446429773" Z="16.600000000009" OnCorner="False" />
                  <Point X="-2.85005530386" Y="9.185731915651" Z="16.6" />
                  <BezierPoint X="-3.01138441413" Y="9.156079815462" Z="16.6" OnCorner="False" />
                  <BezierPoint X="-3.169836038125" Y="9.115258493975" Z="16.6" OnCorner="False" />
                  <Point X="-3.325410175844" Y="9.063267951191" Z="16.6" />
                  <Point X="-3.555774484096" Y="8.959983070791" Z="16.600000000002" />
                  <BezierPoint X="-3.642444183229" Y="8.921642016234" Z="16.600000000001" OnCorner="False" />
                  <BezierPoint X="-3.695076416358" Y="8.85600669624" Z="16.600000000001" OnCorner="False" />
                  <Point X="-3.713671183481" Y="8.763077110811" Z="16.6" />
                  <Point X="-3.712775424623" Y="6.69482795887" Z="16.6" />
                  <Point X="-3.853476063423" Y="6.532124446869" Z="16.6" />
                  <Point X="-3.99767311397" Y="6.372511436406" Z="16.6" />
                  <Point X="-4.151165208381" Y="6.208835594436" Z="16.6" />
                  <Point X="-4.645878376665" Y="5.672489272099" Z="16.6" />
                  <Point X="-5.091921391378" Y="5.151263008856" Z="16.6" />
                  <Point X="-5.109994435814" Y="5.129069345383" Z="16.6" />
                  <Point X="-5.540656522536" Y="4.568497899835" Z="16.599999999999" />
                  <Point X="-5.93597298196" Y="3.986290636527" Z="16.6" />
                  <Point X="-6.276225036756" Y="3.411973418134" Z="16.6" />
                  <Point X="-6.293213419715" Y="3.380536372539" Z="16.6" />
                  <BezierPoint X="-6.40367117936" Y="3.174907650273" Z="16.6" OnCorner="False" />
                  <BezierPoint X="-6.503809754568" Y="2.964370769293" Z="16.6" OnCorner="False" />
                  <Point X="-6.593629145336" Y="2.748925729599" Z="16.6" />
                  <BezierPoint X="-6.683786800079" Y="2.532662961286" Z="16.6" OnCorner="False" />
                  <BezierPoint X="-6.76311524191" Y="2.312498274822" Z="16.6" OnCorner="False" />
                  <Point X="-6.831614470828" Y="2.088431670205" Z="16.6" />
                  <BezierPoint X="-6.900344039988" Y="1.863593570602" Z="16.6" OnCorner="False" />
                  <BezierPoint X="-6.957835785407" Y="1.635917742866" Z="16.6" OnCorner="False" />
                  <Point X="-7.004089707086" Y="1.405404186997" Z="16.6" />
                  <BezierPoint X="-7.050474423997" Y="1.174233312732" Z="16.6" OnCorner="False" />
                  <BezierPoint X="-7.085329073397" Y="0.941336879791" Z="16.6" OnCorner="False" />
                  <Point X="-7.108653655287" Y="0.706714888172" Z="16.6" />
                  <Point X="-7.13492293448" Y="0.353873048009" Z="16.6" />
                  <Point X="-7.143694677151" Y="0.000130025534" Z="16.6" />
                  <Point X="-7.134923557896" Y="-0.353712797697" Z="16.6" />
                  <Point X="-7.108654900445" Y="-0.706554684142" Z="16.6" />
                  <BezierPoint X="-7.085330731982" Y="-0.941176716855" Z="16.6" OnCorner="False" />
                  <BezierPoint X="-7.050476492967" Y="-1.174073211209" Z="16.6" OnCorner="False" />
                  <Point X="-7.004092183401" Y="-1.405244167203" Z="16.6" />
                  <BezierPoint X="-6.957838667908" Y="-1.635757804571" Z="16.6" OnCorner="False" />
                  <BezierPoint X="-6.900347323676" Y="-1.863433733608" Z="16.6" OnCorner="False" />
                  <Point X="-6.831618150704" Y="-2.088271954315" Z="16.6" />
                  <BezierPoint X="-6.763119316611" Y="-2.312338679629" Z="16.6" OnCorner="False" />
                  <BezierPoint X="-6.683791262732" Y="-2.532503505873" Z="16.6" OnCorner="False" />
                  <Point X="-6.593633989065" Y="-2.748766433047" Z="16.6" />
                  <BezierPoint X="-6.503814977931" Y="-2.964211631007" Z="16.6" OnCorner="False" />
                  <BezierPoint X="-6.40367677371" Y="-3.174748688436" Z="16.6" OnCorner="False" />
                  <Point X="-6.293219376402" Y="-3.380377605335" Z="16.6" />
                  <Point X="-6.276295785147" Y="-3.411695676508" Z="16.6" />
                  <Point X="-5.935982394517" Y="-3.986135413122" Z="16.6" />
                  <Point X="-5.540671463706" Y="-4.568347238772" Z="16.599999999999" />
                  <Point X="-5.110015897918" Y="-5.128925634698" Z="16.6" />
                  <Point X="-5.0920512559" Y="-5.150987171578" Z="16.6" />
                  <Point X="-4.645908174504" Y="-5.672356782" Z="16.6" />
                  <Point X="-4.151207594046" Y="-6.208723958027" Z="16.6" />
                  <Point X="-3.997894714072" Y="-6.372224874924" Z="16.6" />
                  <Point X="-3.853716295574" Y="-6.531834825716" Z="16.6" />
                  <Point X="-3.713035544562" Y="-6.694536021556" Z="16.6" />
                  <Point X="-3.713841589783" Y="-8.762869896072" Z="16.6" />
                  <BezierPoint X="-3.695093430884" Y="-8.855741070903" Z="16.600000000001" OnCorner="False" />
                  <BezierPoint X="-3.642404395649" Y="-8.921358726755" Z="16.600000000001" OnCorner="False" />
                  <Point X="-3.555774484077" Y="-8.959722863626" Z="16.600000000002" />
                  <Point X="-3.325410175827" Y="-9.063007744025" Z="16.6" />
                  <BezierPoint X="-3.169836038108" Y="-9.114998286809" Z="16.6" OnCorner="False" />
                  <BezierPoint X="-3.011384414114" Y="-9.155819608295" Z="16.6" OnCorner="False" />
                  <Point X="-2.850055303844" Y="-9.185471708484" Z="16.6" />
                  <BezierPoint X="-2.534312789026" Y="-9.243186222606" Z="16.600000000009" OnCorner="False" />
                  <BezierPoint X="-2.216427728236" Y="-9.284358427042" Z="16.600000000017" OnCorner="False" />
                  <Point X="-1.896400121476" Y="-9.30898832179" Z="16.600000000026" />
                  <Point X="-0.948867299559" Y="-9.362628314354" Z="16.6" />
                  <Point X="7.2862844E-05" Y="-9.378170324976" Z="16.6" />
                  <Point X="0.948977945266" Y="-9.362628314354" Z="16.6" />
                  <Point X="1.896510767184" Y="-9.30898832179" Z="16.600000000026" />
                  <BezierPoint X="2.216538351921" Y="-9.284358256998" Z="16.600000000017" OnCorner="False" />
                  <BezierPoint X="2.534423412712" Y="-9.243186052564" Z="16.600000000009" OnCorner="False" />
                  <Point X="2.850165949555" Y="-9.185471708486" Z="16.600000000001" />
                  <BezierPoint X="3.006169329671" Y="-9.156668257335" Z="16.600000000001" OnCorner="False" />
                  <BezierPoint X="3.159557505563" Y="-9.117597703146" Z="16.6" OnCorner="False" />
                  <Point X="3.31033047723" Y="-9.068260045921" Z="16.6" />
                  <Point X="3.548746568163" Y="-8.963945995456" Z="16.600000000003" />
                  <BezierPoint X="3.638652566474" Y="-8.925543339317" Z="16.600000000002" OnCorner="False" />
                  <BezierPoint X="3.693721122251" Y="-8.858517972868" Z="16.600000000001" OnCorner="False" />
                  <Point X="3.713952235494" Y="-8.762869896109" Z="16.6" />
                </Points>
              </OutlinePathElement>
            </ChildElements>
          </GraphicsPath>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>