from pkgutil import get_data
from tkinter import CENTER
from turtle import clear
from lxml import etree
import math
import os
import re
import xml.etree.ElementTree as ET


class Point(object):
    """
    A simple minded 3D point data type, that can also handle 2D point
    """

    def __init__(self, x=None, y=None, z=None):
        self._x = x
        self._y = y
        self._z = z

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        self._x = value

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, value):
        self._y = value

    @property
    def z(self):
        return self._z

    @z.setter
    def z(self, value):
        self._z = value

    def __eq__(self, other):
        
        xyclose = math.isclose(self.x, other.x) and \
               math.isclose(self.y, other.y)
        if(self.z and other.z):
            is_equal = xyclose and math.isclose(self.z, other.z)
            #print(f"XYZ:is equal:{is_equal}, {self},  other:{other}")
            return is_equal
        else:
            is_equal = xyclose
            #print(f"XY:is equal:{is_equal}, {self},  other:{other}")
            return is_equal

    def writeXML_attrib(self, element):
        # sets attributes
        element.set("X",str(self.x))
        element.set("Y",str(self.y))
        if self.z is not None:
            element.set("Z",str(self.z))
  
    def __str__(self):
        return f"({self._x}, {self._y}, {self._z})" if self.z else f"({self._x}, {self._y})"
    
    def __repr__(self):
        return f"Point({self.x}, {self.y}, {self.z})"

class Segment(object):
    """
    A segment is made up of 2 Point data (3D coordinates) as (start_x, start_y, end_x, end_y)
    """

    def __init__(self, st_pt, end_pt):
        self._start = st_pt
        self._end = end_pt

    @property
    def start(self):
        return self._start

    @property
    def end(self):
        return self._end

    @property
    def points(self):
        return [self._start, self._end]
    
    def show(self):
        print(self.start, self.end)  
        
    def __str__(self):
        return f"[{self._start} -- {self._end}]"

    def __repr__(self):
        return f"Segment({self._start}, {self._end})\n"
    
    def rhino_segment(self, rg, rc, tab):
        if self.start == self.end:
            return None, None
        
        ps = f"{tab} {rg}.Point3d({self.start.x}, {self.start.y}, {self.start.z})" if self.start.z is not None \
            else f"{tab} {rg}.Point2d({self.start.x}, {self.start.y})"
        pe = f"{tab} {rg}.Point3d({self.end.x}, {self.end.y}, {self.end.z})" if self.end.z is not None \
            else f"{tab} {rg}.Point2d({self.end.x}, {self.end.y})"
        rhino_pt_start = f"{tab} this_st_pt = {ps}"
        rhino_pt_end = f"{tab} this_end_pt = {pe}"
        this_seg = f"{tab} this_seg = {rg}.LineCurve(this_st_pt, this_end_pt)"
        return f"{rhino_pt_start}\n {rhino_pt_end}\n {this_seg}\n", "this_seg"

    def writeXML(self, element_root, prev_segment):
        if prev_segment is None:
            this_element = etree.SubElement(element_root, 'Point')
            self.start.writeXML_attrib(this_element)
            this_element = etree.SubElement(element_root, 'Point')
            self.end.writeXML_attrib(this_element)
        else:
            this_element = etree.SubElement(element_root, 'Point')
            self.end.writeXML_attrib(this_element)



class CubicBezier(object):
    """
    A cubic bezier is made up of 4 Point data (3D coordinates) as list of points[4]
    """
    def __init__(self, points):
        if len(points) != 4:
            raise ValueError("Need 4 points to define cubic bezier")
        self._points = points

    @property
    def start(self):
        return self._points[0]

    @property
    def end(self):
        return self._points[3]

    @property
    def points(self):
        return self._points
    
    def show(self):
        for pt in self._points:
            print(pt)
        
    def __str__(self):
        return f"[{self._points[0]}, {self._points[1]}, {self._points[2]}, {self._points[3]}]"

    def __repr__(self):
        return f"CubicBezier([{self._points[0]}, {self._points[1]}, {self._points[2]}, {self._points[3]}])\n"
    
    def rhino_segment(self, rg, rc, tab):
        ps = []
        rpt = []
        for i in range(len(self.points)):
            ps.append(f"{tab} {rg}.Point3d({self.points[i].x}, {self.points[i].y}, {self.points[i].z})" \
                if self.points[i].z is not None else f"{tab} {rg}.Point2d({self.points[i].x}, {self.points[i].y})")
        rpt.append(f"{tab} pt0 = {ps[0]}")
        rpt.append(f"{tab} pt1 = {ps[1]}")
        rpt.append(f"{tab} pt2 = {ps[2]}")
        rpt.append(f"{tab} pt3 = {ps[3]}")
        pt_list = f"{tab} this_point_set = {rc}.Point3dList()"
        pt0 =f"{tab} this_point_set.Add(pt0)" 
        pt1 =f"{tab} this_point_set.Add(pt1)" 
        pt2 =f"{tab} this_point_set.Add(pt2)" 
        pt3 =f"{tab} this_point_set.Add(pt3)"
        this_curve = f"{tab} this_bz_piece = ({rg}.BezierCurve(this_point_set)).ToNurbsCurve()" 
        return f"{rpt[0]}\n {rpt[1]}\n {rpt[2]}\n {rpt[3]}\n {pt_list}\n {pt0}\n {pt1}\n {pt2}\n {pt3}\n {this_curve}\n", "this_bz_piece" 


    def writeXML(self, element_root, prev_segment):
        if not prev_segment:
            this_point_element = etree.SubElement(element_root, 'Point')
            self.start.writeXML_attribute(this_point_element)
        
        this_element = etree.SubElement(element_root, 'BezierPoint')
        self.points[1].writeXML_attribute(this_point_element)
        
        this_element = etree.SubElement(element_root, 'BezierPoint')
        self.points[2].writeXML_attribute(this_point_element)
        
        this_point_element = etree.SubElement(element_root, 'Point')
        self.end.writeXML_attribute(this_point_element)



class GeometryInfo(object):
    """
    A collection of various geometries and flags associated with them, which is the data type that gets collected 
    from the XML elements. Users will interact with this information. 
    Typically, a list of this GeometryInfo will be created and key-ed by the root-element type under which the geometry is found in XML file
    """
    def __init__(self):
        self.geometry = []
        # Is the geometry a closed geometry or not ?
        self.closure = False
        
    def __len__(self):
        return len(self.geometry)    
    
    def __repr__(self):
        return (f"GeometryInfo({self.geometry}, {self.closure})")



################################ WRITING XML ###############################
class XMLGeometry(object):
    def __init__(self, _data, _root, element_tag_name, geometry_tag_name):
        self.geometry_list = _data
        self.root = _root
        self.closed = False
        self.geometry_tag_name = geometry_tag_name
        if self.root:
            self.element = etree.SubElement(self.root, element_tag_name)
        else:
            self.element = etree.Element(element_tag_name)

    def _check_closure(self, current_geom_set):
        # let us check for closure. We just need to check the first geometry
        if len(current_geom_set) < 2:
            return False
        first_seg = current_geom_set[0]
        last_seg = current_geom_set[-1]
        return (first_seg.start == last_seg.end)

    def _write_closure_tag(self, geometry_tag):
        closed_tag  = etree.SubElement(geometry_tag, "IsClosed")
        if self.closed:
            closed_tag.set('Value','True')
        else:
            closed_tag.set('Value','False')

    def write(self, fp):
        # for each set of self contained geometry, write 
        for geom_set in self.geometry_list:
            geom_element_tag  = etree.SubElement(self.element, self.geometry_tag_name)
            
            # check for closure automatically
            self.closed = self._check_closure(geom_set)
  
            # this closure tag is a direct child of geom_element_tag
            self._write_closure_tag(geom_element_tag)

            # adjust number of segments for closure
            g_set = geom_set[:-1] if self.closed else geom_set
            
            # each geometry consists of set of segments, write them all
            prev_segment = None
            for each_segment in g_set:
                each_segment.writeXML(geom_element_tag, prev_segment)
                prev_segment = each_segment

        # create a new XML file with the results
        all_data = etree.tostring(self.element, encoding='unicode', pretty_print=True)
        fp.write(all_data)

################################ WRITING XML ###############################


##############################################################################

def layer_z_position(layer_id, scale=1.0):
    return layer_id*scale            


def read_xml(filename, layer_id, extract_elements):
    '''
    User facing function to process XML file.
    Simply use XPath protocol to parse XML.
    It takes in a dictionary that maps the outermost element (called "root-element")
    to the required child element, such that the child element also has the 
    IsClosed flag.
    
    For example: One map: extract_elements['PolygonElement'] = ['OutlinePolygonElement']
    is one such mapping
    '''
    
    # what is the layer id ? We will use this as Z value, if not given ...
    # the layer_id comes in in the format <dirname>_<layerid>, with "_" as the seperator
    # If that is not the format, then the z_pos is automatically given a value of 0
    l_split = layer_id.split("_")
    try: 
        l_id = float(l_split[1])
    except KeyError:
        z_pos = 0
    else:        
        z_pos = layer_z_position(l_id) 
    
    ##
    z_pos = z_pos - 1
    #print(f"Z pos is:{z_pos}, filename is {filename}")
    
    
    # obtain XML file data tree context
    root = etree.parse(filename)
       
    # XPath syntax to reach Point and BezierPoint, the real data    
    actual_geom = ".//*[self::Point or self::BezierPoint]"
    #print(f"XPath of Geometry element:{actual_geom}")
    
    # Initialize output dictionary that will hold all parsed data as
    # Segment() and CubicBezier() objects
    out = {}
    
    # For all root-elements (high level elements that contain polygonal data)
    # get the polygonal data    
    for rl, (geometry_elements_list, form_polycurve) in extract_elements.items():
        out[rl] = [[], form_polycurve]
        # Construct XPath search string: //[self::element or self::other_element] ..
        gpath =""
        delim = ".//*["
        geometry_elements = geometry_elements_list[0]
        for pel in geometry_elements:
            gpath += f"{delim}self::{pel}"
            delim = " or "
        gpath += "]"
    
        # First find the root element
        find_rl = f"//{rl}"
        #print(f"XPath of root ele: {rl} is: {find_rl}")
        #print(f"XPath of geometry outline {geometry_elements} is:{gpath}")
     
        for this_root in root.xpath(find_rl):
            # Find that element that contains the flag IsClosed to determine
            # if the parse polygon needs to be closed or not
            this_geom_parent = (this_root.xpath(gpath))
            for this_geom in this_geom_parent:
                # For the high level child of root-element, IsClosed is a 
                # *direct* child of that element (upon inspection of XML file)
                is_closed_ele = this_geom.xpath("./child::IsClosed")
                is_closed = False
                if len(is_closed_ele) > 1:
                    raise ValueError(f"IsClosed data can only appear once, but we got {len(is_closed_ele)} IsClosed elements")
                if len(is_closed_ele) == 1:
                    is_closed_attr = is_closed_ele[0].attrib['Value']
                    is_closed = True if is_closed_attr == "True" else False
                
                # Where is the actual geometry found, get to that using XPath
                this_point = this_geom.xpath(actual_geom)
                
                # Parse data embedded as attribute in the element
                # could be 2D or 3D, 
                # if 2D, we need to get the z-pos of layer, which will be supplied to us
                # further, sometimes it is assumed that we get the Z from previously initialized
                # z. All this is incorporated here
                pts = [], is_closed
                prev_z = None
                for x in this_point:
                    tagname = str(x.tag).strip()
                    _x = float(x.attrib['X'])
                    _y = float(x.attrib['Y'])
                    try:
                        # Is there Z attrib present ?
                        _z = float(x.attrib['Z'])
                    except KeyError:
                        # no Z attribute and there is no previous value initialized, use layer z_pos input 
                        if prev_z is None:
                            _z = z_pos
                        else:
                            # no Z attribute, but there *is* previous Z available, use it ...
                            _z = prev_z
                    else:
                        # this is the else for the try clause which says z value is available, 
                        # make that the prev_z!
                        prev_z = _z
                                 
                    #print(f"Appending point:{_x}, {_y}, {_z}")
                    pts[0].append({tagname:[_x, _y, _z]})       
                
                # Fill the parsed raw data for further processing
                out[rl][0].append(pts)
        
        #print(out[rl])

    return out


def process_raw_data(raw_data):
    changed_info = {}
    # Loop thru the raw data, make segments and Beziers and re-fill the output
    for key, (geom_info, form_polycurve) in raw_data.items():
        #print(f"Geometry for: {key}:")
        changed_info[key] = [[], form_polycurve]
        for g in geom_info:
            #print(f"Geometry:{g}")
            g_data = g[0]
            closure = g[1]
            if g_data is None or len(g_data) < 2:
                raise ValueError(f"Geometry data is {g_data} => degenerate")
            geom_mod = []
            prev_pt = None
            prev_tag = None
            bzpoints = []
            for pt_data in g_data:
                etag = None
                for _tag, v in pt_data.items():
                    this_pt = Point(v[0], v[1], v[2]) if v[2] is not None else Point(v[0], v[1])
                    etag = _tag
                
                #print(f"this pt: {this_pt}")    
                # backward and forward data reqd for bezier points
                start_bezier = (prev_tag == "Point" and etag == "BezierPoint")
                end_bezier = (etag == "Point" and prev_tag == "BezierPoint")
                is_segment = (etag == "Point" and prev_tag == "Point")
         
                #print(f"start bz:{start_bezier}, end_bezier:{end_bezier}, is_segment:{is_segment}, etag:{etag}, prev_tag:{prev_tag}")
        
                # order is important here ...
                if start_bezier:
                    bzpoints.append(prev_pt)

                if etag == "BezierPoint":
                    bzpoints.append(this_pt)

                if is_segment:
                    geom_mod.append(Segment(prev_pt, this_pt))

                if end_bezier:
                    bzpoints.append(this_pt)
                    # lists are mutable, so copy bzpoints instead of passing self.bzpoints
                    bzpts = [bzpoints[0], bzpoints[1], bzpoints[2], bzpoints[3]]
                    geom_mod.append(CubicBezier(bzpts))
                    # clear the local list of bezier points
                    bzpoints.clear()
                       
                prev_pt = this_pt 
                prev_tag = etag
            
            if closure:
                geom_mod.append(Segment(geom_mod[-1].end, geom_mod[0].start))

            changed_info[key][0].append(geom_mod), form_polycurve  
            #print(f"local info changed for key: {key}:{changed_info[key]}")  
    
    #print(f"changed info:\n {changed_info}")        
    return changed_info        
    


def dir_walk_with_match(regex, directory):
    '''Yield path/filenames matching some regular expression
    '''
    sep = os.path.sep
    pattern = re.compile(regex)
    for p,_,f in os.walk(directory):
        for each in f:
            if pattern.search(each):
                yield f'{p},{sep},{each}'
 
 
def extract_filename_for_parsing(regex, directory):      
    '''
    Walk a given directory, collect SLD files, process the filename to get
    layer information and pass all of this information fro writing data into Rhino
    '''           
    process_these = []
    ## now get the files and feedback reqd. info
    all_files = dir_walk_with_match(regex, directory)
    for file_data in all_files:
        p, sep, filename = file_data.split(',')
        parent_dir = p.split(sep)[-1]
        layer_id = filename.split(" ")[1].split(".")[0]
        process_file = sep.join([p, filename])
        layer_name = f"{parent_dir}_{layer_id}"
        #print(f"processing file:{process_file}, layer-name:{layer_name}") 
        process_these.append((process_file, layer_name))
    return process_these                    



############################## TESTS ######################################
def test_read_write_XML():
    """
    Change the user directory to location of sld files
    
    """
    dir_root = "C:\\Users\\Barry\\Honeywell\\sld_files"
    dir_name = "center"
    dir = f"{dir_root}\\{dir_name}"
    file_type = r'sld|SLD'
    
    ## definition of what needs to be extracted ...
    # Geometry info is stored as dependent child of these tags
    polygon_geom_ele = ['OutlinePolygonElement'] 
    fill_hatchlines_geom_ele = ['GraphicPath'] 
    hatch_vector_geom_ele = ['GraphicPath'] 
    fill_outline_geom_ele = ['GraphicPath'] 
    outline_geom_ele = ['Points'] 
    
    # flag that tells if polycurve is to be formed from ordered set of data 
    # that is read 
    form_polycurve = True
    
    # All geometry extracted is consolidated into these root elements, which
    # specify how the geometry is stored either as GraphicPath, OutlinePolygonElement or Points
    # root-element and its children that contain the geometry information
    # Each child element's geometry creates its own geometry added as a separate
    # list organized under root-element. 
    extract_elements = {}
    #extract_elements['HatchVector'] = [hatch_vector_geom_ele], not form_polycurve
    extract_elements['PolygonElement'] = [polygon_geom_ele], form_polycurve
    #extract_elements['FillHatchlinesElement'] = [fill_hatchlines_geom_ele], not form_polycurve
    #extract_elements['FillOutlinesElement'] = [fill_outline_geom_ele], not form_polycurve
    #extract_elements['OutlinePathElement'] = [outline_geom_ele], form_polycurve
    
    # extract all files with SLD suffix under all sub-directories of dir ...
    process_these_files = extract_filename_for_parsing(file_type, dir)                 
    all_data = []
    for to_process in process_these_files:
        fname = to_process[0]
        layer_name = to_process[1] 
        print(f"Process:{fname}, with layer id:{layer_name}") 
        raw_data = read_xml(fname, layer_name, extract_elements)
        segs_bzs = process_raw_data(raw_data)
        # geometry data & layer info stored as a pair
        all_data.append((segs_bzs, layer_name))      
         
    #input_to_xml_write = all_data[0][0]['FillHatchlinesElement'][0]
    for data, layer_name in all_data:
        filename ="".join([".\\out\\", layer_name,'.xml'])
        for root_element_name, element_data in data.items():
            # the index 1 is for grasshopper to create one curve or multiple curves as the case maybe
            element_geom_data = element_data[0]
            with open(filename, 'w+') as fp:
                # indexing using three zeros is weird ... 
                geom_elem_name = extract_elements[root_element_name][0][0][0]
                print(f"Write to file:{filename}, Element_tag[{root_element_name}] ==> Child[{geom_elem_name}]")
                xml_obj = XMLGeometry(element_geom_data, None, root_element_name, geom_elem_name)
                xml_obj.write(fp)
 


if __name__ == '__main__':
    test_read_write_XML()

