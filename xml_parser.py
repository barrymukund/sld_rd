from asyncore import read
from re import I
from lxml import etree

class EchoTarget(object):
     def start(self, tag, attrib):
         print("start %s %r" % (tag, dict(attrib)))
     def end(self, tag):
         print("end %s" % tag)
     def data(self, data):
         print("data %r" % data)
     def comment(self, text):
         print("comment %s" % text)
     def close(self):
         print("close")
         return "closed!"

def read_xml_parser(filename):
    parser = etree.XMLParser(ns_clean=True, 
                             recover=True, 
                             remove_blank_text=True, 
                             target=EchoTarget())
    emtree = etree.parse(filename, parser)
    return emtree


class CollectorTarget(object):
    def __init__(self):
        self.events = []
    
    #def start(self, tag, attrib):
    #    self.events.append(f"start {tag}, {dict(attrib)}")
    
    def end(self, tag):
        self.events.append(f"end {tag}")
    
    def data(self, data):
        self.events.append(f"data {data}")
    
    def comment(self, text):
        self.events.append(f"comment {text}")
    
    def close(self):
        self.events.append("close")
        return "closed!"

def read_xml_parser_event_collector(filename):
    parser = etree.XMLParser(ns_clean=True, 
                             recover=True, 
                             remove_blank_text=True, 
                             target=CollectorTarget())
    emtree = etree.parse(filename, parser)
    for event in parser.target.events:
        print(f"collected event: {event}")
    
    return emtree


def read_xml_iterwalk(filename):
    context = etree.iterparse(filename, 
                    events=("start", "end"))
                    #, tag="Description")
    _, root = next(context)
    walk_context = etree.iterwalk(root, events=("start", "end"))
    
    for action, elem in walk_context:
        if action == 'start' and elem.tag == 'Instructions':
            print("\nSKIP Instructions ...\n")
            walk_context.skip_subtree()
        
        txt = str(elem.text).strip()
        print(f"ACTION: {action}, TAG:{elem.tag}, TEXT:{txt}, ATTRIB:{elem.attrib}")
   
    return root


def read_xml_iterparse(filename):
    context = etree.iterparse(filename, 
                    events=("start", "end")
                    , tag="Ingredients")
    
    for action, elem in context:
         print(f"x=>{action}:{elem.tag}")

    root = context
    for action, elem in root:
         print(f"root=>{action}:{elem.tag}")

    print(f"root is {root}")
    
    return root


def read_xml_iterparse_elements_of_root(filename):
    context = etree.iterparse(filename, 
                    events=("start", "end")
                    , tag="Ingredients")
    
    for action, elem in context:
         esz = len(elem)
         print(f"x=>{action}:{elem.tag}, elem size:{esz}")
         for child in elem:
             tx =str(child.text).strip()
             print(f"Child:{child.tag}, child text:{tx}")
             
    return None



def iterate_xml(filename, start_tag = None):
    doc = etree.iterparse(filename, events=('start', 'end'))
    event, root = next(doc)
    for event, element in doc:
        if event == 'start' and start_tag is None:
            start_tag = element.tag
        if event == 'end' and element.tag == start_tag:
            yield element
            start_tag = None
            root.clear()


if __name__ == '__main__':
    
    fname = 'ingredients.xml'
    
    #read_xml_parser_event_collector(fname)
    
    #read_xml_iterparse_elements_of_root(fname)
    #
    root = read_xml_iterwalk(fname)
    
    #for ele in iterate_xml(fname):
    #    print(f"element:{ele.tag}, text:{ele.text}")
    
    
        